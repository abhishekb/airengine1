﻿using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class PassengerFare
    {
        public int Id { get; set; }

        public int FlightFareId { get; set; }

        public int Quantity { get; set; }

        public Money BaseAmount { get; set; }

        public int PassengerType { get; set; }

        public virtual FlightFare FlightFare { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FareComponent> FareComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PassengerFare()
        {
            BaseAmount = new Money();

            FareComponents = new List<FareComponent>();
        }
    }
}
