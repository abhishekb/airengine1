﻿using System;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class Money
    {
        public string DisplayCurrency { get; set; }

        public Decimal DisplayAmount { get; set; }

        public string BaseCurrency { get; set; }

        public Decimal BaseAmount { get; set; }

        public string NativeCurrency { get; set; }

        public Decimal NativeAmount { get; set; }
    }
}
