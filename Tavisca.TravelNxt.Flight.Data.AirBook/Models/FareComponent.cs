﻿namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class FareComponent
    {
        public int Id { get; set; }

        public int PassengerFareId { get; set; }

        public int Type { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string OwnerId { get; set; }

        public long FareComponentRuleId { get; set; }

        public Money Value { get; set; }

        public virtual PassengerFare PassengerFare { get; set; }

        public FareComponent()
        {
            Value = new Money();
        }
    }
}
