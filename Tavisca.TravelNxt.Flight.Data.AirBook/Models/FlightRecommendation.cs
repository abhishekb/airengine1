﻿using System;
using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class FlightRecommendation
    {
        public int Id { get; set; }

        public Guid RecordLocator { get; set; }

        public int ItineraryType { get; set; }

        public int ProviderSpaceId { get; set; }

        public long AccountId { get; set; }

        public long? ContractId { get; set; }

        public string AdditionalInfo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightLeg> FlightLegs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaxFareQualifier> PaxFareQualifiers { get; set; } 

        public virtual FlightFare FlightFare { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlightRecommendation()
        {
            FlightLegs = new List<FlightLeg>();
            PaxFareQualifiers = new List<PaxFareQualifier>();
        }
    }
}
