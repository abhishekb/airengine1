﻿using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class FlightLeg
    {
        public int Id { get; set; }

        public int FlightRecommendationId { get; set; }

        public int LegIndex { get; set; }

        public int LayoverDurationInMin { get; set; }

        public string AdditionalInfo { get; set; }

        public virtual FlightRecommendation FlightRecommendation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FlightSegment> FlightSegments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlightLeg()
        {
            FlightSegments = new List<FlightSegment>();
        }
    }
}
