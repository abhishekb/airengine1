﻿using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class FlightFare
    {
        public int Id { get; set; }

        public int FlightRecommendationId { get; set; }

        public int FareType { get; set; }

        public virtual FlightRecommendation FlightRecommendation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PassengerFare> PassengerFares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlightFare()
        {
            PassengerFares = new List<PassengerFare>();
        }
    }
}
