﻿using System;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class FlightSegment
    {
        public int Id { get; set; }

        public int FlightLegId { get; set; }

        public int SegmentIndex { get; set; }

        public string FlightNumber { get; set; }

        public string MarketingAirlineCode { get; set; }

        public string OperatingAirlineCode { get; set; }

        public string CodeShareText { get; set; }

        public string AircraftType { get; set; }

        public string DepartureAirportCode { get; set; }

        public string ArrivalAirportCode { get; set; }

        public string DepartureTerminal { get; set; }

        public string ArrivalTerminal { get; set; }

        public DateTime DepartureDateTime { get; set; }

        public DateTime ArrivalDateTime { get; set; }

        public int NumberOfStops { get; set; }

        public bool CanETicket { get; set; }

        public int DurationMinutes { get; set; }

        public int DistanceTraveledKms { get; set; }

        public string AdditionalInfo { get; set; }

        public virtual FlightLeg FlightLeg { get; set; }
    }
}
