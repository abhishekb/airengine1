﻿namespace Tavisca.TravelNxt.Flight.Data.AirBook.Models
{
    public class PaxFareQualifier
    {
        public int Id { get; set; }

        public int FlightRecommendationId { get; set; }

        public string ClassOfService { get; set; }

        public string FareBasisCode { get; set; }

        public int LegIndex { get; set; }

        public string SegmentRef { get; set; }

        public int PassengerType { get; set; }

        public int CabinClass { get; set; }

        public virtual FlightRecommendation FlightRecommendation { get; set; }
    }
}
