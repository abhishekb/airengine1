﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database
{
    public class AirBookContext : DbContext
    {
        static AirBookContext()
        {
            System.Data.Entity.Database.SetInitializer<AirBookContext>(null);
        }

        public AirBookContext() : base(SettingManager.AirBookConnectionString) { }

        public DbSet<FareComponent> FareComponents { get; set; }
        public DbSet<FlightFare> FlightFares { get; set; }
        public DbSet<FlightLeg> FlightLegs { get; set; }
        public DbSet<FlightRecommendation> FlightRecommendations { get; set; }
        public DbSet<FlightSegment> FlightSegments { get; set; }
        public DbSet<PassengerFare> PassengerFares { get; set; }
        public DbSet<PaxFareQualifier> PaxFareQualifiers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new FareComponentMap());
            modelBuilder.Configurations.Add(new FlightFareMap());
            modelBuilder.Configurations.Add(new FlightLegMap());
            modelBuilder.Configurations.Add(new FlightRecommendationMap());
            modelBuilder.Configurations.Add(new FlightSegmentMap());
            modelBuilder.Configurations.Add(new PassengerFareMap());
            modelBuilder.Configurations.Add(new PaxFareQualifierMap());
        }
    }
}
