﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class PaxFareQualifierMap : EntityTypeConfiguration<PaxFareQualifier>
    {
        public PaxFareQualifierMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.FlightRecommendationId).IsRequired();

            this.Property(x => x.ClassOfService).IsRequired().HasMaxLength(50);

            this.Property(x => x.FareBasisCode).HasMaxLength(50);

            this.Property(x => x.LegIndex).IsRequired();

            this.Property(x => x.SegmentRef).IsRequired().HasMaxLength(100);

            this.Property(x => x.PassengerType).IsRequired();

            this.Property(x => x.CabinClass).IsRequired();
            
            // Relationships
            this.HasRequired(t => t.FlightRecommendation)
                .WithMany(t => t.PaxFareQualifiers)
                .HasForeignKey(t => t.FlightRecommendationId);
        }
    }
}
