﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class FlightLegMap : EntityTypeConfiguration<FlightLeg>
    {
        public FlightLegMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.FlightRecommendationId).IsRequired();

            this.Property(x => x.LayoverDurationInMin).IsRequired();

            this.Property(x => x.LegIndex).IsRequired();

            this.Property(x => x.AdditionalInfo).HasMaxLength(510);

            // Relationships
            this.HasRequired(t => t.FlightRecommendation)
                .WithMany(x => x.FlightLegs)
                .HasForeignKey(x => x.FlightRecommendationId);
        }
    }
}
