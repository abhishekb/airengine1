﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class FareComponentMap : EntityTypeConfiguration<FareComponent>
    {
        public FareComponentMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.OwnerId).IsRequired();

            this.Property(x => x.PassengerFareId).IsRequired();

            this.Property(x => x.Code).HasMaxLength(50);

            this.Property(x => x.Description).HasMaxLength(510);

            this.Property(x => x.OwnerId).HasMaxLength(100);

            this.Property(x => x.OwnerId).HasMaxLength(100);

            this.Property(x => x.Type).IsRequired();

            this.Property(x => x.Value.BaseAmount).IsRequired().HasColumnType("money").HasColumnName("BaseAmount");
            this.Property(x => x.Value.BaseCurrency).IsRequired().HasMaxLength(5).HasColumnName("BaseCurrency");
            this.Property(x => x.Value.DisplayAmount).HasColumnType("money").HasColumnName("DisplayAmount");
            this.Property(x => x.Value.DisplayCurrency).HasMaxLength(5).HasColumnName("DisplayCurrency");
            this.Property(x => x.Value.NativeAmount).IsRequired().HasColumnType("money").HasColumnName("NativeAmount");
            this.Property(x => x.Value.NativeCurrency).IsRequired().HasMaxLength(5).HasColumnName("NativeCurrency");

            // Relationships
            this.HasRequired(t => t.PassengerFare)
                .WithMany(t => t.FareComponents)
                .HasForeignKey(t => t.PassengerFareId);
        }
    }
}
