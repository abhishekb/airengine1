﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class FlightRecommendationMap : EntityTypeConfiguration<FlightRecommendation>
    {
        public FlightRecommendationMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.RecordLocator).IsRequired();

            this.Property(x => x.ItineraryType).IsRequired();

            this.Property(x => x.ProviderSpaceId).IsRequired();

            this.Property(x => x.AccountId).IsRequired();

            this.Property(x => x.AdditionalInfo).HasMaxLength(510);
        }
    }
}
