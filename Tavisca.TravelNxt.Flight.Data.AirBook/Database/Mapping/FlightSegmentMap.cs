﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class FlightSegmentMap : EntityTypeConfiguration<FlightSegment>
    {
        public FlightSegmentMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.FlightLegId).IsRequired();

            this.Property(x => x.SegmentIndex).IsRequired();

            this.Property(x => x.FlightNumber).IsRequired().HasMaxLength(50);

            this.Property(x => x.MarketingAirlineCode).IsRequired().HasMaxLength(5);

            this.Property(x => x.OperatingAirlineCode).HasMaxLength(5);

            this.Property(x => x.CodeShareText).HasMaxLength(255);

            this.Property(x => x.AircraftType).HasMaxLength(50);

            this.Property(x => x.DepartureAirportCode).IsRequired().HasMaxLength(5);

            this.Property(x => x.ArrivalAirportCode).IsRequired().HasMaxLength(5);

            this.Property(x => x.DepartureTerminal).HasMaxLength(10);

            this.Property(x => x.ArrivalTerminal).HasMaxLength(10);

            this.Property(x => x.DepartureDateTime).IsRequired();

            this.Property(x => x.ArrivalDateTime).IsRequired();

            this.Property(x => x.NumberOfStops).IsRequired();

            this.Property(x => x.AdditionalInfo).HasMaxLength(510);

            // Relationships
            this.HasRequired(t => t.FlightLeg)
                .WithMany(t => t.FlightSegments)
                .HasForeignKey(t => t.FlightLegId);
        }
    }
}
