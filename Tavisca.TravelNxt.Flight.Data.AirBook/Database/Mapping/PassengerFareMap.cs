﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class PassengerFareMap : EntityTypeConfiguration<PassengerFare>
    {
        public PassengerFareMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.FlightFareId).IsRequired();

            this.Property(x => x.PassengerType).IsRequired();

            this.Property(x => x.Quantity).IsRequired();

            this.Property(x => x.BaseAmount.BaseAmount).IsRequired().HasColumnType("money").HasColumnName("BaseAmount");
            this.Property(x => x.BaseAmount.BaseCurrency).IsRequired().HasMaxLength(5).HasColumnName("BaseCurrency");
            this.Property(x => x.BaseAmount.DisplayAmount).HasColumnType("money").HasColumnName("DisplayAmount");
            this.Property(x => x.BaseAmount.DisplayCurrency).HasMaxLength(5).HasColumnName("DisplayCurrency");
            this.Property(x => x.BaseAmount.NativeAmount).IsRequired().HasColumnType("money").HasColumnName("NativeAmount");
            this.Property(x => x.BaseAmount.NativeCurrency).IsRequired().HasMaxLength(5).HasColumnName("NativeCurrency");

            // Relationships
            this.HasRequired(t => t.FlightFare)
                .WithMany(t => t.PassengerFares)
                .HasForeignKey(t => t.FlightFareId);
        }
    }
}
