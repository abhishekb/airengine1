﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirBook.Database.Mapping
{
    public class FlightFareMap : EntityTypeConfiguration<FlightFare>
    {
        public FlightFareMap()
        {
            this.HasKey(x => x.Id);

            this.Property(x => x.FlightRecommendationId).IsRequired();

            this.Property(x => x.FareType).IsRequired();

            // Relationships
            this.HasRequired(t => t.FlightRecommendation)
                .WithOptional(x => x.FlightFare);
        }
    }
}
