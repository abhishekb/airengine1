﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_CallStatus")]
    public enum CallStatus
    {
        [EnumMember]
        Success = 0,
        [EnumMember]
        Failure = 1,
        [EnumMember]
        Warning = 2,
        [EnumMember]
        InProgress = 3
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "B_ItineraryTypeOptions")]
    public enum ItineraryTypeOptions
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        MultiCity = 1,
        [EnumMember]
        OneWay = 2,
        [EnumMember]
        RoundTrip = 3
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "B_PassengerType")]
    public enum PassengerType
    {
        [EnumMember]
        Infant = 0,
        [EnumMember]
        Child = 1,
        [EnumMember]
        Adult = 2,
        [EnumMember]
        Senior = 3
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "B_FareComponentType")]
    public enum FareComponentType
    {
        [EnumMember]
        Tax,
        [EnumMember]
        Fees,
        [EnumMember]
        Markup,
        [EnumMember]
        Discount,
        [EnumMember]
        Commision
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "B_FareType")]
    public enum FareType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Published,
        [EnumMember]
        Negotiated
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "B_CabinType")]
    public enum CabinType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        First = 1,
        [EnumMember]
        Business = 2,
        [EnumMember]
        PremiumEconomy = 3,
        [EnumMember]
        Economy = 4
    }

    public static class EnumParsers
    {
        public static CallStatus ToDataContract(Entities.CallStatus entity)
        {
            switch (entity)
            {
                case Entities.CallStatus.Success:
                    return CallStatus.Success;
                case Entities.CallStatus.InProgress:
                    return CallStatus.InProgress;
                case Entities.CallStatus.Failure:
                    return CallStatus.Failure;
                case Entities.CallStatus.Warning:
                    return CallStatus.Warning;
            }
            throw new InvalidDataException("this conversion is not handled.");
        }

        public static Entities.CallStatus ToEntity(CallStatus contract)
        {
            switch (contract)
            {
                case CallStatus.Success:
                    return Entities.CallStatus.Success;
                case CallStatus.InProgress:
                    return Entities.CallStatus.InProgress;
                case CallStatus.Failure:
                    return Entities.CallStatus.Failure;
                case CallStatus.Warning:
                    return Entities.CallStatus.Warning;
            }
            throw new InvalidDataException("this conversion is not handled.");
        }
    }
}
