﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_PricedRecommendation")]
    public class PricedRecommendation
    {
        public int RefId { get; set; }

        [DataMember]
        public List<FlightLeg> Legs { get; set; }
        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }
        [DataMember]
        public ProviderSpace ProviderSpace { get; set; }

        [DataMember]
        public ItineraryTypeOptions ItineraryType { get; set; }

        //TODO1: Decide on FlightFare in Pricing response
        //public FlightFare FlightFare { get; set; }

        public static PricedRecommendation ToDataContract(FlightRecommendation recommendation)
        {
            if (recommendation == null)
                return null;

            var datacontract = new PricedRecommendation()
                                   {
                                       Legs = recommendation.Legs.Select(FlightLeg.ToDataContract).ToListBuffered(),
                                       AdditionalInfo = AdditionalInfoDictionary.ToDictionary(recommendation.AdditionalInfo),
                                       RefId = recommendation.RefId,
                                       ProviderSpace = ProviderSpace.ToDataContract(recommendation.ProviderSpace),
                                       ItineraryType = (ItineraryTypeOptions)((int)recommendation.ItineraryType)
                                       //TODO1: Fare Translation here
                                   };

            return datacontract;
        }

        //public static ItineraryTypeOptions
    }
}
