﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightProduct")]
    public class FlightProduct
    {
        [DataMember]
        public PricedRecommendation Recommendation { get; set; }

        #region Translation

        public static FlightProduct ToDataContract(Entities.Booking.FlightProduct entity)
        {
            if (entity == null)
                return null;

            var contract = new FlightProduct()
            {
                Recommendation = PricedRecommendation.ToDataContract(entity.Recommendation)
            };

            return contract;
        }

        #endregion
    }
}
