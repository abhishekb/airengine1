﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{

    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightSegment")]
    public class FlightSegment
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public Airline MarketingAirline { get; set; }
        //[DataMember]
        //public string AirlineFlightNumber { get; set; }
        [DataMember]
        public Airline OperatingAirline { get; set; }
        [DataMember]
        public string CodeShareText { get; set; }
        [DataMember]
        public string AircraftType { get; set; }

        // Travel dates and points
        [DataMember]
        public Airport DepartureAirport { get; set; }
        [DataMember]
        public Airport ArrivalAirport { get; set; }
        [DataMember]
        public string DepartureTerminal { get; set; }
        [DataMember]
        public string ArrivalTerminal { get; set; }
        [DataMember]
        public DateTime DepartureDateTime { get; set; }
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }

        /// <summary>
        /// Number of no plane change stops
        /// </summary>
        [DataMember]
        public List<SegmentStop> InSegmentStops { get; set; }

        /// <summary>
        /// Whether the particular flight in the pricing recommendation can be eticketed
        /// </summary>
        [DataMember]
        public bool CanETicket { get; set; }

        [DataMember]
        public int DurationMinutes { get; set; }
        [DataMember]
        public int DistanceTraveledKms { get; set; }

        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        #region Translation

        public static FlightSegment ToDataContract(Entities.FlightSegment flightSegment)
        {
            return new FlightSegment()
                       {
                           Key = flightSegment.Key,
                           AdditionalInfo = AdditionalInfoDictionary.ToDictionary(flightSegment.AdditionalInfo),
                           FlightNumber = flightSegment.FlightNumber,
                           MarketingAirline = Airline.ToDataContract(flightSegment.MarketingAirline),
                           OperatingAirline = Airline.ToDataContract(flightSegment.OperatingAirline),
                           CodeShareText = flightSegment.CodeShareText,
                           ArrivalAirport = Airport.ToDataContract(flightSegment.ArrivalAirport),
                           DepartureAirport = Airport.ToDataContract(flightSegment.DepartureAirport),
                           CanETicket = flightSegment.CanETicket,
                           DurationMinutes = flightSegment.DurationMinutes,
                           DistanceTraveledKms = flightSegment.DistanceTraveledKms,
                           InSegmentStops = flightSegment.InSegmentStops == null ? null : flightSegment.InSegmentStops.Select(SegmentStop.ToDataContract).ToListBuffered(flightSegment.InSegmentStops.Count),
                           DepartureDateTime = flightSegment.DepartureDateTime,
                           ArrivalDateTime = flightSegment.ArrivalDateTime,
                           DepartureTerminal = flightSegment.DepartureTerminal,
                           ArrivalTerminal = flightSegment.ArrivalTerminal,
                           Index = flightSegment.SegmentIndex
                       };
        }

        public static Entities.FlightSegment ToEntity(FlightSegment flightSegment)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

}
