﻿using System.Runtime.Serialization;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_PaxFareQualifier")]
    public class PaxTypeFareBasis
    {
        [DataMember]
        public string FareBasisCode { get; set; }

        [DataMember]
        public PassengerType PassengerType { get; set; }

        public static PaxTypeFareBasis ToDataContract(Entities.PaxTypeFareBasis paxFareQualifier)
        {
            return new PaxTypeFareBasis()
                                   {
                                       FareBasisCode = paxFareQualifier.FareBasisCode,
                                       PassengerType = PassengerFare.ToDataContract(paxFareQualifier.PassengerType)
                                   };
        }


        public static CabinType ToDataContract(Entities.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.CabinType.Business:
                    return CabinType.Business;
                case Entities.CabinType.Economy:
                    return CabinType.Economy;
                case Entities.CabinType.First:
                    return CabinType.First;
                case Entities.CabinType.PremiumEconomy:
                    return CabinType.PremiumEconomy;
                case Entities.CabinType.Unknown:
                    return CabinType.Unknown;
            }

            return default(CabinType);
        }

        public static Entities.CabinType ToEntity(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Business:
                    return Entities.CabinType.Business;
                case CabinType.Economy:
                    return Entities.CabinType.Economy;
                case CabinType.First:
                    return Entities.CabinType.First;
                case CabinType.PremiumEconomy:
                    return Entities.CabinType.PremiumEconomy;
                case CabinType.Unknown:
                    return Entities.CabinType.Unknown;
            }

            return default(Entities.CabinType);
        }
    }





}
