﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Book.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightRecommendationInfo")]
    [Validator(typeof(FlightRecommendationInfoValidator))]
    public class FlightRecommendationInfo
    {
        [DataMember]
        public int RecommendationRefID { get; set; }

        #region Translation

        public static FlightRecommendationInfo ToDataContract(Entities.Booking.FlightRecommendationInfo entity)
        {
            var contract = new FlightRecommendationInfo()
                {
                    RecommendationRefID = entity.FlightRecommendationRefId
                };

            return contract;
        }

        public static Entities.Booking.FlightRecommendationInfo ToEntity(FlightRecommendationInfo contract)
        {
            var entity = new Entities.Booking.FlightRecommendationInfo()
                {
                    FlightRecommendationRefId = contract.RecommendationRefID
                };

            return entity;
        }

        #endregion
    }
}
