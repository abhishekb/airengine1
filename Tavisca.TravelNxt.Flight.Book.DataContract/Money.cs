﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_Money")]
    public class Money
    {
        [DataMember]
        public string DisplayCurrency { get; set; }
        [DataMember]
        public Decimal DisplayAmount { get; set; }

        private static string GetRequestedCurrency()
        {
            return Utility.GetCurrentContext().DisplayCurrency;
        }

        public override string ToString()
        {
            return this.DisplayAmount.ToString(CultureInfo.InvariantCulture) + " " + this.DisplayCurrency;
        }

        #region Translation

        public static Money ToDataContract(Entities.Money money)
        {
            if (string.IsNullOrEmpty(money.NativeCurrency))
            {
                return new Money()
                {
                    DisplayAmount = 0,
                    DisplayCurrency = GetRequestedCurrency()

                };
            }
            Money contract = null;
            if (money.DisplayAmount != 0 && !string.IsNullOrEmpty(money.DisplayCurrency))
                contract = new Money()
                {
                    DisplayAmount = money.DisplayAmount,
                    DisplayCurrency = money.DisplayCurrency
                };
            else
            {
                contract = new Money()
                {
                    DisplayAmount = money.ConvertToValue(GetRequestedCurrency()),
                    DisplayCurrency = GetRequestedCurrency()
                };
            }
            return contract;
        }

        public static Entities.Money ToEntity(Money money)
        {
            throw new NotSupportedException();
            //var entity = new Entities.Money()
        }

        #endregion
    }
}
