﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightSaveRS")]
    public class FlightSaveRS
    {
        [DataMember]
        public CallStatus Status { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public Guid RecordLocator { get; set; }

        #region Translation

        public static FlightSaveRS ToDataContract(Entities.Booking.FlightSaveRS entity)
        {
            var contract = new FlightSaveRS()
                {
                    Status = EnumParsers.ToDataContract(entity.Status),
                    RecordLocator = entity.RecordLocator,
                    Message = entity.Message
                };

            return contract;
        }

        public static Entities.Booking.FlightSaveRS ToEntity(FlightSaveRS contract)
        {
            var entity = new Entities.Booking.FlightSaveRS()
                {
                    Status = EnumParsers.ToEntity(contract.Status),
                    RecordLocator = contract.RecordLocator,
                    Message = contract.Message
                };

            return entity;
        }

        

        #endregion
    }
}
