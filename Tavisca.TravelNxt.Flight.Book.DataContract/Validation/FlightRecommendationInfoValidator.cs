﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract.Validation
{
    public class FlightRecommendationInfoValidator : AbstractValidator<FlightRecommendationInfo>
    {
        public FlightRecommendationInfoValidator()
        {
            RuleFor(x => x.RecommendationRefID).GreaterThan(0);
        }
    }
}
