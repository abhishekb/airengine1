﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract.Validation
{
    public class FlightSaveRQValidator : AbstractValidator<FlightSaveRQ>
    {
        public FlightSaveRQValidator()
        {
            RuleFor(x => x.FlightRecommendationInfo).NotNull().SetValidator(new FlightRecommendationInfoValidator());
        }
    }
}
