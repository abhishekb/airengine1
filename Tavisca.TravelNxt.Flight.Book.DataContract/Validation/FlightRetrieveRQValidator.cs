﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract.Validation
{
    public class FlightRetrieveRQValidator : AbstractValidator<FlightRetrieveRQ>
    {
        public FlightRetrieveRQValidator()
        {
            RuleFor(x => x.RecordLocator).NotEqual(Guid.Empty).WithMessage("Record Locator cannot be empty.");
        }
    }
}
