﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Flight.Entities;

//using FluentValidation.Attributes;


namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_PaxTypeQuantity")]
    //[Validator(typeof(PaxTypeQuantityValidator))]
    public class PaxTypeQuantity
    {
        [DataMember]
        public List<int> Ages { get; set; }
        [DataMember]
        public PassengerType PassengerType { get; set; }
        [DataMember]
        public int Quantity { get; set; }

        #region Translation

        public static PaxTypeQuantity ToDataContract(Entities.PaxTypeQuantity paxTypeQuantity)
        {
            var contract = new PaxTypeQuantity()
                {
                    Ages = new List<int>(paxTypeQuantity.Ages),
                    Quantity = paxTypeQuantity.Quantity,
                    PassengerType = PassengerFare.ToDataContract(paxTypeQuantity.PassengerType)
                };

            return contract;
        }

        public static Entities.PaxTypeQuantity ToEntity(PaxTypeQuantity paxTypeQuantity)
        {
            var entity = new Entities.PaxTypeQuantity()
            {
                Ages = paxTypeQuantity.Ages,
                Quantity = paxTypeQuantity.Quantity,
                PassengerType = PassengerFare.ToEntity(paxTypeQuantity.PassengerType)
            };

            return entity;
        }

        #endregion
    }
}
