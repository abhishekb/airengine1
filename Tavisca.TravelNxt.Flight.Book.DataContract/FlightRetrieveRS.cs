﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightRetrieveRS")]
    public class FlightRetrieveRS
    {
        [DataMember]
        public CallStatus Status { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public FlightProduct Product { get; set; }

        #region Translation

        public static FlightRetrieveRS ToDataContract(Entities.Booking.FlightRetrieveRS entity)
        {
            var contract = new FlightRetrieveRS()
            {
                Status = EnumParsers.ToDataContract(entity.Status),
                Message = entity.Message,
                Product = FlightProduct.ToDataContract(entity.Product)
            };

            return contract;
        }

        #endregion
    }
}
