﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Book.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightSaveRQ")]
    [Validator(typeof(FlightSaveRQValidator))]
    public class FlightSaveRQ
    {
        [DataMember]
        public FlightRecommendationInfo FlightRecommendationInfo { get; set; }

        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        [DataMember]
        public Requester Requester { get; set; }

        #region Translation

        public static FlightSaveRQ ToDataContract(Entities.Booking.FlightSaveRQ entity)
        {
            var contract = new FlightSaveRQ()
                {
                    AdditionalInfo = AdditionalInfoDictionary.ToDictionary(entity.AdditionalInfo),
                    FlightRecommendationInfo = FlightRecommendationInfo.ToDataContract(entity.FlightRecommendationInfo),
                    Requester = Requester.ToDataContract(entity.Requester)
                };

            return contract;
        }

        public static Entities.Booking.FlightSaveRQ ToEntity(FlightSaveRQ contract)
        {
            var entity = new Entities.Booking.FlightSaveRQ()
                {
                    AdditionalInfo = new AdditionalInfoDictionary(contract.AdditionalInfo),
                    FlightRecommendationInfo = FlightRecommendationInfo.ToEntity(contract.FlightRecommendationInfo),
                    Requester = Requester.ToEntity(contract.Requester)
                };

            return entity;
        }

        #endregion
    }
}
