﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightLeg")]
    public class FlightLeg
    {

        [DataMember]
        public List<FlightSegment> Segments { get; set; }
        [DataMember]
        public int LegIndex { get; set; }
        [DataMember]
        public int LayoverDurationInMin { get; set; }

        public static FlightLeg ToDataContract(Entities.FlightLeg leg)
        {
            var dataContract = new FlightLeg()
                                   {
                                       Segments = leg.Segments.Select(FlightSegment.ToDataContract).ToListBuffered(),
                                       LegIndex = leg.LegIndex
                                   };

            return dataContract;
        }
    }
}
