﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Book.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightRetrieveRQ")]
    [Validator(typeof(FlightRetrieveRQValidator))]
    public class FlightRetrieveRQ
    {
        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        [DataMember]
        public Requester Requester { get; set; }

        [DataMember]
        public Guid RecordLocator { get; set; }

        #region Translation

        public static Entities.Booking.FlightRetrieveRQ ToEntity(FlightRetrieveRQ contract)
        {
            var entity = new Entities.Booking.FlightRetrieveRQ()
            {
                AdditionalInfo = new AdditionalInfoDictionary(contract.AdditionalInfo),
                Requester = Requester.ToEntity(contract.Requester),
                RecordLocator = contract.RecordLocator
            };

            return entity;
        }

        #endregion
    }
}
