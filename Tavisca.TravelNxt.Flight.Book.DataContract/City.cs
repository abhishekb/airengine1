﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_City")]
    public class City
    {
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }

        #region Translation

        public static City ToDataContract(Entities.City city)
        {
            if (city == null)
                return null;

            var contract = new City()
                {
                    Code = city.Code,
                    Country = city.CountryCode,
                    State = city.StateCode,
                    Name = city.Name
                };

            return contract;
        }

        public static Entities.City ToEntity(City city)
        {
            var entity = new Entities.City()
            {
                Code = city.Code,
                CountryCode = city.Country,
                StateCode = city.State,
                Name = city.Name
            };

            return entity;
        }

        #endregion
    }
}
