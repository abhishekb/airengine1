﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_Airport")]
    public class Airport
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public City City { get; set; }

        #region Translation

        public static Airport ToDataContract(Entities.Airport airport)
        {
            var contract = new Airport()
                {
                    FullName = airport.FullName,
                    Code = airport.Code,
                    City = City.ToDataContract(airport.City)
                };

            return contract;
        }

        public static Entities.Airport ToEntity(Airport airport)
        {
            var entity = new Entities.Airport(airport.Code);

            return entity;
        }

        #endregion
    }
}
