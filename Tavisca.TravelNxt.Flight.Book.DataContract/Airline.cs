﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_Airline")]
    public class Airline
    {
        [DataMember]
        public string Code { get; set; }

        
        #region Translation

        public static Airline ToDataContract(Entities.Airline airline)
        {
            if (airline == null)
            {
                return null;
            }
            else
                return new Airline()
                    {
                        Code = airline.Code
                        
                    };


        }

        public static Entities.Airline ToEntity(Airline airline)
        {
            var entity = new Entities.Airline(airline.Code);

            return entity;
        }

        #endregion
    }
}
