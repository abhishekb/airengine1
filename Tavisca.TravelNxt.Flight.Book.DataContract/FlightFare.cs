﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FlightFare")]
    public class FlightFare
    {
        [DataMember]
        public Money TotalMarkup { get; set; }
        [DataMember]
        public Money TotalFees { get; set; }
        [DataMember]
        public Money TotalCommission { get; set; }
        [DataMember]
        public Money TotalTaxes { get; set; }
        [DataMember]
        public Money BaseAmount { get; set; }
        [DataMember]
        public Money TotalFare { get; set; }
        [DataMember]
        public List<PassengerFare> PassengerFares { get; set; }
        [DataMember]
        public FareType FareType { get; set; }

        #region Translation

        public static FlightFare ToDataContract(Entities.FlightFare flightFare)
        {
            var contract = new FlightFare()
                {
                    BaseAmount = Money.ToDataContract(flightFare.BaseAmount),
                    FareType = ToDataContract(flightFare.FareType),
                    PassengerFares = flightFare.PassengerFares.Select(PassengerFare.ToDataContract).ToListBuffered(),
                    TotalFare = Money.ToDataContract(flightFare.TotalFare),
                    TotalCommission = Money.ToDataContract(flightFare.PassengerFares.Select(x => x.TotalCommission).Aggregate((x, y) => x + y)),
                    TotalFees = Money.ToDataContract(flightFare.PassengerFares.Select(x => x.TotalFees).Aggregate((x, y) => x + y)),
                    TotalMarkup = Money.ToDataContract(flightFare.PassengerFares.Select(x => x.TotalMarkup).Aggregate((x, y) => x + y)),
                    TotalTaxes = Money.ToDataContract(flightFare.PassengerFares.Select(x => x.TotalTaxes).Aggregate((x, y) => x + y))
                };
            
            return contract;
        }

        public static Entities.FlightFare ToEntity(FlightFare flightFare)
        {
            var entity = new Entities.FlightFare()
                {
                    FareType = ToEntity(flightFare.FareType),
                    PassengerFares = flightFare.PassengerFares.Select(PassengerFare.ToEntity).ToListBuffered()  
                };

            return entity;
        }

        public static FareType ToDataContract(Entities.FareType fareType)
        {
            switch (fareType)
            {
                case Entities.FareType.Negotiated:
                    return FareType.Negotiated;
                case Entities.FareType.Published:
                    return FareType.Published;
                case Entities.FareType.Unknown:
                    return FareType.Unknown;
            }

            return default(FareType);
        }

        public static Entities.FareType ToEntity(FareType fareType)
        {
            switch (fareType)
            {
                case FareType.Negotiated:
                    return Entities.FareType.Negotiated;
                case FareType.Published:
                    return Entities.FareType.Published;
                case FareType.Unknown:
                    return Entities.FareType.Unknown;
            }

            return default(Entities.FareType);
        }

        #endregion
    }
}
