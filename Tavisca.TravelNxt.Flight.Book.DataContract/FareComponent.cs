﻿using System.Runtime.Serialization;
using Tavisca.TravelNxt.Flight.Entities;
using Money = Tavisca.TravelNxt.Flight.Book.DataContract.Money;

namespace Tavisca.TravelNxt.Flight.Book.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "B_FareComponent")]
    public class FareComponent
    {
        [DataMember]
        public FareComponentType FareComponentType{ get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Money Value { get; set; }

        #region Translation

        public static FareComponent ToDataContract(Entities.FareComponent fareComponent)
        {
            var contract = new FareComponent()
                {
                    Code = fareComponent.Code,
                    Description = fareComponent.Description,
                    FareComponentType = ToDataContract(fareComponent.FareComponentType),
                    Value = Money.ToDataContract(fareComponent.Value)
                };

            return contract;
        }

        public static Entities.FareComponent ToEntity(FareComponent fareComponent)
        {
            var entity = new Entities.FareComponent()
            {
                Code = fareComponent.Code,
                Description = fareComponent.Description,
                FareComponentType = ToEntity(fareComponent.FareComponentType),
                Value = Money.ToEntity(fareComponent.Value)
            };

            return entity;
        }

        public static FareComponentType ToDataContract(Entities.FareComponentType fareComponentType)
        {
            switch (fareComponentType)
            {
                case Entities.FareComponentType.Commision:
                    return FareComponentType.Commision;
                case Entities.FareComponentType.Discount:
                    return FareComponentType.Discount;
                case Entities.FareComponentType.Fees:
                    return FareComponentType.Fees;
                case Entities.FareComponentType.Markup:
                    return FareComponentType.Markup;
                case Entities.FareComponentType.Tax:
                    return FareComponentType.Tax;
            }

            return default(FareComponentType);
        }

        public static Entities.FareComponentType ToEntity(FareComponentType fareComponentType)
        {
            switch (fareComponentType)
            {
                case FareComponentType.Commision:
                    return Entities.FareComponentType.Commision;
                case FareComponentType.Discount:
                    return Entities.FareComponentType.Discount;
                case FareComponentType.Fees:
                    return Entities.FareComponentType.Fees;
                case FareComponentType.Markup:
                    return Entities.FareComponentType.Markup;
                case FareComponentType.Tax:
                    return Entities.FareComponentType.Tax;
            }

            return default(Entities.FareComponentType);
        }

        #endregion
    }
}
