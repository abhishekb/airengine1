using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class Fare
    {
        [DataMember]
        public Money BaseFare { get; set; }

        [DataMember]
        public List<Tax> Taxes { get; set; }

        [DataMember]
        public Money TotalFare { get; set; }
    }
}