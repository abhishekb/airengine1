using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;
using System.Linq;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class Seat
    {
        [DataMember]
        public char ColumnCharacter { get; set; }

        [DataMember]
        public int RowNumber { get; set; }

        [DataMember]
        public List<SeatCharacteristic> SeatCharacteristics { get; set; }

        [DataMember]
        public Fare Fare { get; set; }

        #region Translation
        
        public static Seat ToDataContract(Entities.SeatMap.Seat seat)
        {
            return new Seat()
                       {
                           RowNumber = seat.RowNumber,
                           ColumnCharacter = seat.ColumnCharacter,
                           SeatCharacteristics =
                               seat.SeatCharacteristics == null
                                   ? new List<SeatCharacteristic>()
                                   : seat.SeatCharacteristics.Select(EnumerationTranslations.ToDataContract).
                                         ToListBuffered()
                            //Todo : Fare Parsing
                       };
        }

        #endregion
    }
}