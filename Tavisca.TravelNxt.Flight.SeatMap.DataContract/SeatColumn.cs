using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class SeatColumn
    {
        [DataMember]
        public char ColumnCharacter { get; set; }

        [DataMember]
        public SeatColumnType ColumnType { get; set; }

        #region Translation

        public static SeatColumn ToDataContract(Entities.SeatMap.SeatColumn seatColumn)
        {
            return new SeatColumn()
                       {
                           ColumnCharacter = seatColumn.ColumnCharacter,
                           ColumnType = EnumerationTranslations.ToDataContract(seatColumn.ColumnType)
                       };
        }

        #endregion
    }
}