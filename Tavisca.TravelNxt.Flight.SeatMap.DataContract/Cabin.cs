﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class Cabin
    {
        [DataMember]
        public List<CabinFacility> CabinFacilities { get; set; }

        [DataMember]
        public int Capacity { get; set; }

        [DataMember]
        public List<SeatColumn> Columns {get; set; }

        [DataMember]
        public List<SeatRow> Rows { get; set; }

        [DataMember]
        public List<Seat> Seats { get; set; }

        [DataMember]
        public CabinLocation CabinLocation { get; set; }

        #region Translation

        public static Cabin ToDataContract(Entities.SeatMap.Cabin cabin)
        {
            return new Cabin()
                       {
                           Capacity = cabin.Capacity,
                           CabinLocation = EnumerationTranslations.ToDataContract(cabin.CabinLocation),
                           CabinFacilities =
                               cabin.CabinFacilities == null
                                   ? new List<CabinFacility>()
                                   : cabin.CabinFacilities.Select(CabinFacility.ToDataContract).ToListBuffered(),
                           Columns =
                               cabin.Columns == null
                                   ? new List<SeatColumn>()
                                   : cabin.Columns.Select(SeatColumn.ToDataContract).ToListBuffered(),
                           Rows =
                               cabin.Rows == null
                                   ? new List<SeatRow>()
                                   : cabin.Rows.Select(SeatRow.ToDataContract).ToListBuffered(),
                           Seats =
                               cabin.Seats == null
                                   ? new List<Seat>()
                                   : cabin.Seats.Select(Seat.ToDataContract).ToListBuffered()
                       };
        }

        #endregion
    }
}