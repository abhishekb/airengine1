using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class CabinFacility
    {
        [DataMember]
        public CabinFacilityLocation CabinFacilityLocation { get; set; }

        [DataMember]
        public CabinFacilityType CabinFacilityType { get; set; }

        #region Translation

        public static CabinFacility ToDataContract(Entities.SeatMap.CabinFacility cabinFacility)
        {
            return new CabinFacility()
                       {
                           CabinFacilityLocation = EnumerationTranslations.ToDataContract(cabinFacility.CabinFacilityLocation),
                           CabinFacilityType = EnumerationTranslations.ToDataContract(cabinFacility.CabinFacilityType)
                       };
        }

        #endregion
    }
}