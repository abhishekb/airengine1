﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1/Air/V1")]
    public class ServiceStatus
    {
        [DataMember]
        public string StatusCode { get; set; }

        [DataMember]
        public CallStatus Status { get; set; }

        [DataMember]
        public List<string> Messages { get; set; }

        [DataMember]
        public string StatusMessage { get; set; }

        #region Translation

        public static ServiceStatus ToDataContract(Entities.SeatMap.ServiceStatus serviceStatus)
        {
            return new ServiceStatus()
                       {
                           Messages =
                               serviceStatus.Messages == null
                                   ? new List<string>()
                                   : serviceStatus.Messages.ToListBuffered(),
                           StatusMessage = serviceStatus.StatusMessage,
                           StatusCode = serviceStatus.StatusCode,
                           Status = ToDataContract(serviceStatus.Status)
                       };
        }

        private static CallStatus ToDataContract(Entities.SeatMap.CallStatus callStatus)
        {
            switch (callStatus)
            {
                case Entities.SeatMap.CallStatus.Success:
                    return CallStatus.Success;
                case Entities.SeatMap.CallStatus.Failure:
                    return CallStatus.Failure;
                case Entities.SeatMap.CallStatus.Warning:
                    return CallStatus.Warning;
                default : 
                    throw new ArgumentException("Call status not supported", "callStatus");
            }
        }

        #endregion
    }
}
