﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class SeatMapRQ
    {
        [DataMember]
        public int RecommendationRefId { get; set; }
        [DataMember]
        public string SegmentKey { get; set; }
        //[DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        #region Translation

        public static Entities.SeatMap.SeatMapRQ ToEntity(SeatMapRQ request)
        {
            return new Entities.SeatMap.SeatMapRQ()
                       {
                           RecommendationRefID = request.RecommendationRefId,
                           SegmentKey = request.SegmentKey
                       };
        }

        #endregion
    }
}
