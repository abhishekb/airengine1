using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class Money
    {
        [DataMember]
        public string DisplayCurrency { get; set; }
        [DataMember]
        public Decimal DisplayAmount { get; set; }

        //private static string GetRequestedCurrency()
        //{
        //    return Utility.GetCurrentContext().DisplayCurrency;
        //}

        public override string ToString()
        {
            return this.DisplayAmount.ToString(CultureInfo.InvariantCulture) + " " + this.DisplayCurrency;
        }
    }
}