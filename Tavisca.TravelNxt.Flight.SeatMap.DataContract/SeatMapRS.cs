﻿using System;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class SeatMapRS
    {
        [DataMember]
        public Guid SessionId { get; set; }
        [DataMember]
        public SeatMap SeatMap { get; set; }
        [DataMember]
        public ServiceStatus ServiceStatus { get; set; }

        #region Translation

        public static SeatMapRS ToDataContract(Entities.SeatMap.SeatMapRS response)
        {
            return new SeatMapRS()
                       {
                           SessionId = response.SessionId,
                           ServiceStatus = ServiceStatus.ToDataContract(response.ServiceStatus),
                           SeatMap = SeatMap.ToDataContract(response.SeatMap)
                       };
        }

        #endregion
    }
}
