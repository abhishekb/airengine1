﻿using System;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum CabinType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        First,
        [EnumMember]
        Business,
        [EnumMember]
        PremiumEconomy,
        [EnumMember]
        Economy
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum CabinLocation : int
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Maindeck,
        [EnumMember]
        Lowerdeck,
        [EnumMember]
        Upperdeck
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum SeatCharacteristic
    {
        [EnumMember]
        None,
        [EnumMember]
        AirportBlock,
        [EnumMember]
        Aisle,
        [EnumMember]
        AisleToLeft,
        [EnumMember]
        AisleToRight,
        [EnumMember]
        BehindGallery,
        [EnumMember]
        BehindLavatory,
        [EnumMember]
        Blocked,
        [EnumMember]
        BlockedForPreferredPassenger,
        [EnumMember]
        Buffer,
        [EnumMember]
        BufferZone,
        [EnumMember]
        Bulkhead,
        [EnumMember]
        BulkheadSeatWithMovieScreen,
        [EnumMember]
        Middle,
        [EnumMember]
        MiddleSection,
        [EnumMember]
        Crew,
        [EnumMember]
        Deportee,
        [EnumMember]
        EconomyComfort,
        [EnumMember]
        ElectronicConnectionAvailable,
        [EnumMember]
        ExitRowSeats,
        [EnumMember]
        FrontOfCabinClass,
        [EnumMember]
        FullyRecline,
        [EnumMember]
        FrontOfGalley,
        [EnumMember]
        FrontOfLavatory,
        [EnumMember]
        IndividualAirphone,
        [EnumMember]
        MovieScreen,
        [EnumMember]
        Infant,
        [EnumMember]
        LegRestAvailable,
        [EnumMember]
        LegSpace,
        [EnumMember]
        MedicallyOkayForTravel,
        [EnumMember]
        NearGalley,
        [EnumMember]
        NearLavatory,
        [EnumMember]
        NoSeatBar,
        [EnumMember]
        NoSeatCloset,
        [EnumMember]
        NoSmoking,
        [EnumMember]
        NotForInfant,
        [EnumMember]
        NotForUnaccompaniedMinor,
        [EnumMember]
        NotForChildren,
        [EnumMember]
        OverWing,
        [EnumMember]
        Paid,
        [EnumMember]
        Premium,
        [EnumMember]
        PetInCabinOrSeat,
        [EnumMember]
        Preferred,
        [EnumMember]
        RearFacing,
        [EnumMember]
        RestrictedRecline,
        [EnumMember]
        Restricted,
        [EnumMember]
        RightOfAircraft,
        [EnumMember]
        LeftOfAircraft,
        [EnumMember]
        AdjacentToTable,
        [EnumMember]
        AdjacentToBar,
        [EnumMember]
        AdjacentToCloset,
        [EnumMember]
        AdjacentToStairsToUpperdeck,
        [EnumMember]
        AngledLeft,
        [EnumMember]
        AngledRight,
        [EnumMember]
        ForwardEndCabin,
        [EnumMember]
        Available,
        [EnumMember]
        QuietZone,
        [EnumMember]
        NotAllowedForMedical,
        [EnumMember]
        UpperDeck,
        [EnumMember]
        Occupied,
        [EnumMember]
        AdultWithInfant,
        [EnumMember]
        UnaccompaniedMinor,
        [EnumMember]
        BassinetFacility,
        [EnumMember]
        HandicappedOrIncappacited,
        [EnumMember]
        NoEarphone,
        [EnumMember]
        IndividualEarphone,
        [EnumMember]
        MovieView,
        [EnumMember]
        NoMovieView,
        [EnumMember]
        HandleCabinBaggage,
        [EnumMember]
        Smoking,
        [EnumMember]
        WindowAndAisle,
        [EnumMember]
        Window,
        [EnumMember]
        WindowSeatWithNoWindow,
        [EnumMember]
        BehindGalley,
        [EnumMember]
        NoSeat
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum CallStatus
    {
        [EnumMember]
        Success = 0,
        [EnumMember]
        Failure = 1,
        [EnumMember]
        Warning = 2,
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum CabinFacilityLocation
    {
        [EnumMember]
        None,
        [EnumMember]
        Center,
        [EnumMember]
        LeftCenter,
        [EnumMember]
        Left,
        [EnumMember]
        RightCenter,
        [EnumMember]
        Right,
        [EnumMember]
        Rear,
        [EnumMember]
        Front
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum RowCharacteristic
    {
        [EnumMember]
        None,
        [EnumMember]
        BufferRow,
        [EnumMember]
        ExitLeft,
        [EnumMember]
        ExitRight,
        [EnumMember]
        ExitRow,
        [EnumMember]
        FirstRow,
        [EnumMember]
        LowerdeckRow,
        [EnumMember]
        MaindeckRow,
        [EnumMember]
        MiddleRow,
        [EnumMember]
        NonSmokingRow,
        [EnumMember]
        NotOverwingRow,
        [EnumMember]
        OverwingRow,
        [EnumMember]
        RearRow,
        [EnumMember]
        RowWithMovieScreen,
        [EnumMember]
        SmokingRow,
        [EnumMember]
        UpperDeckRow,
        [EnumMember]
        BulkheadRow,
        [EnumMember]
        LeftOfAisle,
        [EnumMember]
        RightOfAisle
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum SeatColumnType
    {
        [EnumMember]
        NotApplicable,
        [EnumMember]
        Window,
        [EnumMember]
        Aisle,
        [EnumMember]
        Middle
    }

    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public enum CabinFacilityType
    {
        [EnumMember]
        None,
        [EnumMember]
        Airphone,
        [EnumMember]
        Bar,
        [EnumMember]
        Closet,
        [EnumMember]
        EmergencyExit,
        [EnumMember]
        ExitDoor,
        [EnumMember]
        Gallery,
        [EnumMember]
        Galley,
        [EnumMember]
        LuggageStorage,
        [EnumMember]
        MovieScreen,
        [EnumMember]
        QuietArea,
        [EnumMember]
        SmokingArea,
        [EnumMember]
        StairsToUpperDeck,
        [EnumMember]
        StorageSpace,
        [EnumMember]
        Table,
        [EnumMember]
        Lavatory,
        [EnumMember]
        Bulkhead
    }

    #region Translation

    public class EnumerationTranslations
    {
        public static CabinType ToDataContract(Entities.SeatMap.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.SeatMap.CabinType.Unknown:
                    return CabinType.Unknown;
                case Entities.SeatMap.CabinType.Economy:
                    return CabinType.Economy;
                case Entities.SeatMap.CabinType.First:
                    return CabinType.First;
                case Entities.SeatMap.CabinType.Business:
                    return CabinType.Business;
                case Entities.SeatMap.CabinType.PremiumEconomy:
                    return CabinType.PremiumEconomy;
                default :
                    throw new ArgumentException(string.Format("Unrecognized cabinType - {0}", cabinType), "cabinType");
            }
        }

        public static CabinLocation ToDataContract(Entities.SeatMap.CabinLocation cabinLocation)
        {
            switch (cabinLocation)
            {
                case Entities.SeatMap.CabinLocation.Upperdeck:
                    return CabinLocation.Upperdeck;
                case Entities.SeatMap.CabinLocation.Unknown:
                    return CabinLocation.Unknown;
                case Entities.SeatMap.CabinLocation.Maindeck:
                    return CabinLocation.Maindeck;
                case Entities.SeatMap.CabinLocation.Lowerdeck:
                    return CabinLocation.Lowerdeck;
                default :
                    throw new ArgumentException(string.Format("Unrecognized cabin location - {0}", cabinLocation),
                                                "cabinLocation");
            }
        }

        public static CabinFacilityType ToDataContract(Entities.SeatMap.CabinFacilityType cabinFacilityType)
        {
            switch (cabinFacilityType)
            {
                case Entities.SeatMap.CabinFacilityType.Bulkhead:
                    return CabinFacilityType.Bulkhead;
                case Entities.SeatMap.CabinFacilityType.Airphone:
                    return CabinFacilityType.Airphone;
                case Entities.SeatMap.CabinFacilityType.Bar:
                    return CabinFacilityType.Bar;
                case Entities.SeatMap.CabinFacilityType.Closet:
                    return CabinFacilityType.Closet;
                case Entities.SeatMap.CabinFacilityType.EmergencyExit:
                    return CabinFacilityType.EmergencyExit;
                case Entities.SeatMap.CabinFacilityType.Lavatory:
                    return CabinFacilityType.Lavatory;
                case Entities.SeatMap.CabinFacilityType.ExitDoor:
                    return CabinFacilityType.ExitDoor;
                case Entities.SeatMap.CabinFacilityType.Gallery:
                    return CabinFacilityType.Gallery;
                case Entities.SeatMap.CabinFacilityType.Galley:
                    return CabinFacilityType.Galley;
                case Entities.SeatMap.CabinFacilityType.MovieScreen:
                    return CabinFacilityType.MovieScreen;
                case Entities.SeatMap.CabinFacilityType.QuietArea:
                    return CabinFacilityType.QuietArea;
                case Entities.SeatMap.CabinFacilityType.SmokingArea:
                    return CabinFacilityType.SmokingArea;
                case Entities.SeatMap.CabinFacilityType.StairsToUpperDeck:
                    return CabinFacilityType.StairsToUpperDeck;
                case Entities.SeatMap.CabinFacilityType.StorageSpace:
                    return CabinFacilityType.StorageSpace;
                case Entities.SeatMap.CabinFacilityType.Table:
                    return CabinFacilityType.Table;
                case Entities.SeatMap.CabinFacilityType.LuggageStorage:
                    return CabinFacilityType.LuggageStorage;
                case Entities.SeatMap.CabinFacilityType.None:
                    return CabinFacilityType.None;
                default:
                    throw new ArgumentException(
                        string.Format("CabinFacilityType not recognized - {0}", cabinFacilityType), "cabinFacilityType");
            }
        }

        public static CabinFacilityLocation ToDataContract(Entities.SeatMap.CabinFacilityLocation cabinFacilityLocation)
        {
            switch (cabinFacilityLocation)
            {
                case Entities.SeatMap.CabinFacilityLocation.Right:
                    return CabinFacilityLocation.Right;
                case Entities.SeatMap.CabinFacilityLocation.RightCenter:
                    return CabinFacilityLocation.RightCenter;
                case Entities.SeatMap.CabinFacilityLocation.Left:
                    return CabinFacilityLocation.Left;
                case Entities.SeatMap.CabinFacilityLocation.LeftCenter:
                    return CabinFacilityLocation.LeftCenter;
                case Entities.SeatMap.CabinFacilityLocation.Center:
                    return CabinFacilityLocation.Center;
                case Entities.SeatMap.CabinFacilityLocation.Rear:
                    return CabinFacilityLocation.Rear;
                case Entities.SeatMap.CabinFacilityLocation.Front:
                    return CabinFacilityLocation.Front;
                case Entities.SeatMap.CabinFacilityLocation.None:
                    return CabinFacilityLocation.None;
                default:
                    throw new ArgumentException(
                        string.Format("CabinFacilityLocation not recognized - {0}", cabinFacilityLocation),
                        "cabinFacilityLocation");
            }
        }

        public static SeatColumnType ToDataContract(Entities.SeatMap.SeatColumnType seatColumnType)
        {
            switch (seatColumnType)
            {
                case Entities.SeatMap.SeatColumnType.Middle:
                    return SeatColumnType.Middle;
                case Entities.SeatMap.SeatColumnType.Aisle:
                    return SeatColumnType.Aisle;
                case Entities.SeatMap.SeatColumnType.Window:
                    return SeatColumnType.Window;
                case Entities.SeatMap.SeatColumnType.NotApplicable:
                    return SeatColumnType.NotApplicable;
                default:
                    throw new ArgumentException(string.Format("SeatColumnType not recognized - {0}", seatColumnType),
                                                "seatColumnType");
            }
        }

        public static RowCharacteristic ToDataContract(Entities.SeatMap.RowCharacteristic rowCharacteristic)
        {
            switch (rowCharacteristic)
            {
                case Entities.SeatMap.RowCharacteristic.RightOfAisle:
                    return RowCharacteristic.RightOfAisle;
                case Entities.SeatMap.RowCharacteristic.ExitRow:
                    return RowCharacteristic.ExitRow;
                case Entities.SeatMap.RowCharacteristic.FirstRow:
                    return RowCharacteristic.FirstRow;
                case Entities.SeatMap.RowCharacteristic.LowerdeckRow:
                    return RowCharacteristic.LowerdeckRow;
                case Entities.SeatMap.RowCharacteristic.MaindeckRow:
                    return RowCharacteristic.MaindeckRow;
                case Entities.SeatMap.RowCharacteristic.LeftOfAisle:
                    return RowCharacteristic.LeftOfAisle;
                case Entities.SeatMap.RowCharacteristic.MiddleRow:
                    return RowCharacteristic.MiddleRow;
                case Entities.SeatMap.RowCharacteristic.ExitRight:
                    return RowCharacteristic.ExitRight;
                case Entities.SeatMap.RowCharacteristic.NonSmokingRow:
                    return RowCharacteristic.NonSmokingRow;
                case Entities.SeatMap.RowCharacteristic.OverwingRow:
                    return RowCharacteristic.OverwingRow;
                case Entities.SeatMap.RowCharacteristic.RearRow:
                    return RowCharacteristic.RearRow;
                case Entities.SeatMap.RowCharacteristic.RowWithMovieScreen:
                    return RowCharacteristic.RowWithMovieScreen;
                case Entities.SeatMap.RowCharacteristic.SmokingRow:
                    return RowCharacteristic.SmokingRow;
                case Entities.SeatMap.RowCharacteristic.UpperDeckRow:
                    return RowCharacteristic.UpperDeckRow;
                case Entities.SeatMap.RowCharacteristic.BulkheadRow:
                    return RowCharacteristic.BulkheadRow;
                case Entities.SeatMap.RowCharacteristic.BufferRow:
                    return RowCharacteristic.BufferRow;
                case Entities.SeatMap.RowCharacteristic.NotOverwingRow:
                    return RowCharacteristic.NotOverwingRow;
                case Entities.SeatMap.RowCharacteristic.ExitLeft:
                    return RowCharacteristic.ExitLeft;
                case Entities.SeatMap.RowCharacteristic.None:
                    return RowCharacteristic.None;
                default:
                    throw new ArgumentException(
                        string.Format("RowCharacteristic not recognized - {0}", rowCharacteristic), "rowCharacteristic");
            }
        }

        public static SeatCharacteristic ToDataContract(Entities.SeatMap.SeatCharacteristic seatCharacteristic)
        {
            switch (seatCharacteristic)
            {
                case Entities.SeatMap.SeatCharacteristic.BehindGalley:
                    return SeatCharacteristic.BehindGalley;
                case Entities.SeatMap.SeatCharacteristic.Infant:
                    return SeatCharacteristic.Infant;
                case Entities.SeatMap.SeatCharacteristic.LegRestAvailable:
                    return SeatCharacteristic.LegRestAvailable;
                case Entities.SeatMap.SeatCharacteristic.LegSpace:
                    return SeatCharacteristic.LegSpace;
                case Entities.SeatMap.SeatCharacteristic.MedicallyOkayForTravel:
                    return SeatCharacteristic.MedicallyOkayForTravel;
                case Entities.SeatMap.SeatCharacteristic.NearGalley:
                    return SeatCharacteristic.NearGalley;
                case Entities.SeatMap.SeatCharacteristic.Aisle:
                    return SeatCharacteristic.Aisle;
                case Entities.SeatMap.SeatCharacteristic.NearLavatory:
                    return SeatCharacteristic.NearLavatory;
                case Entities.SeatMap.SeatCharacteristic.NoSeatBar:
                    return SeatCharacteristic.NoSeatBar;
                case Entities.SeatMap.SeatCharacteristic.ExitRowSeats:
                    return SeatCharacteristic.ExitRowSeats;
                case Entities.SeatMap.SeatCharacteristic.AirportBlock:
                    return SeatCharacteristic.AirportBlock;
                case Entities.SeatMap.SeatCharacteristic.NoSmoking:
                    return SeatCharacteristic.NoSmoking;
                case Entities.SeatMap.SeatCharacteristic.NotForInfant:
                    return SeatCharacteristic.NotForInfant;
                case Entities.SeatMap.SeatCharacteristic.NotForUnaccompaniedMinor:
                    return SeatCharacteristic.NotForUnaccompaniedMinor;
                case Entities.SeatMap.SeatCharacteristic.NotForChildren:
                    return SeatCharacteristic.NotForChildren;
                case Entities.SeatMap.SeatCharacteristic.OverWing:
                    return SeatCharacteristic.OverWing;
                case Entities.SeatMap.SeatCharacteristic.Paid:
                    return SeatCharacteristic.Paid;
                case Entities.SeatMap.SeatCharacteristic.MovieScreen:
                    return SeatCharacteristic.MovieScreen;
                case Entities.SeatMap.SeatCharacteristic.IndividualAirphone:
                    return SeatCharacteristic.IndividualAirphone;
                case Entities.SeatMap.SeatCharacteristic.AisleToLeft:
                    return SeatCharacteristic.AisleToLeft;
                case Entities.SeatMap.SeatCharacteristic.FrontOfLavatory:
                    return SeatCharacteristic.FrontOfLavatory;
                case Entities.SeatMap.SeatCharacteristic.ElectronicConnectionAvailable:
                    return SeatCharacteristic.ElectronicConnectionAvailable;
                case Entities.SeatMap.SeatCharacteristic.EconomyComfort:
                    return SeatCharacteristic.EconomyComfort;
                case Entities.SeatMap.SeatCharacteristic.Deportee:
                    return SeatCharacteristic.Deportee;
                case Entities.SeatMap.SeatCharacteristic.Crew:
                    return SeatCharacteristic.Crew;
                case Entities.SeatMap.SeatCharacteristic.MiddleSection:
                    return SeatCharacteristic.MiddleSection;
                case Entities.SeatMap.SeatCharacteristic.Middle:
                    return SeatCharacteristic.Middle;
                case Entities.SeatMap.SeatCharacteristic.BulkheadSeatWithMovieScreen:
                    return SeatCharacteristic.BulkheadSeatWithMovieScreen;
                case Entities.SeatMap.SeatCharacteristic.FrontOfCabinClass:
                    return SeatCharacteristic.FrontOfCabinClass;
                case Entities.SeatMap.SeatCharacteristic.Premium:
                    return SeatCharacteristic.Premium;
                case Entities.SeatMap.SeatCharacteristic.Bulkhead:
                    return SeatCharacteristic.Bulkhead;
                case Entities.SeatMap.SeatCharacteristic.Buffer:
                    return SeatCharacteristic.Buffer;
                case Entities.SeatMap.SeatCharacteristic.BlockedForPreferredPassenger:
                    return SeatCharacteristic.BlockedForPreferredPassenger;
                case Entities.SeatMap.SeatCharacteristic.Blocked:
                    return SeatCharacteristic.Blocked;
                case Entities.SeatMap.SeatCharacteristic.BehindLavatory:
                    return SeatCharacteristic.BehindLavatory;
                case Entities.SeatMap.SeatCharacteristic.BehindGallery:
                    return SeatCharacteristic.BehindGallery;
                case Entities.SeatMap.SeatCharacteristic.AisleToRight:
                    return SeatCharacteristic.AisleToRight;
                case Entities.SeatMap.SeatCharacteristic.FullyRecline:
                    return SeatCharacteristic.FullyRecline;
                case Entities.SeatMap.SeatCharacteristic.FrontOfGalley:
                    return SeatCharacteristic.FrontOfGalley;
                case Entities.SeatMap.SeatCharacteristic.BufferZone:
                    return SeatCharacteristic.BufferZone;
                case Entities.SeatMap.SeatCharacteristic.PetInCabinOrSeat:
                    return SeatCharacteristic.PetInCabinOrSeat;
                case Entities.SeatMap.SeatCharacteristic.NoSeatCloset:
                    return SeatCharacteristic.NoSeatCloset;
                case Entities.SeatMap.SeatCharacteristic.QuietZone:
                    return SeatCharacteristic.QuietZone;
                case Entities.SeatMap.SeatCharacteristic.Window:
                    return SeatCharacteristic.Window;
                case Entities.SeatMap.SeatCharacteristic.Available:
                    return SeatCharacteristic.Available;
                case Entities.SeatMap.SeatCharacteristic.WindowAndAisle:
                    return SeatCharacteristic.WindowAndAisle;
                case Entities.SeatMap.SeatCharacteristic.HandicappedOrIncappacited:
                    return SeatCharacteristic.HandicappedOrIncappacited;
                case Entities.SeatMap.SeatCharacteristic.NotAllowedForMedical:
                    return SeatCharacteristic.NotAllowedForMedical;
                case Entities.SeatMap.SeatCharacteristic.UpperDeck:
                    return SeatCharacteristic.UpperDeck;
                case Entities.SeatMap.SeatCharacteristic.Preferred:
                    return SeatCharacteristic.Preferred;
                case Entities.SeatMap.SeatCharacteristic.ForwardEndCabin:
                    return SeatCharacteristic.ForwardEndCabin;
                case Entities.SeatMap.SeatCharacteristic.Occupied:
                    return SeatCharacteristic.Occupied;
                case Entities.SeatMap.SeatCharacteristic.HandleCabinBaggage:
                    return SeatCharacteristic.HandleCabinBaggage;
                case Entities.SeatMap.SeatCharacteristic.NoMovieView:
                    return SeatCharacteristic.NoMovieView;
                case Entities.SeatMap.SeatCharacteristic.MovieView:
                    return SeatCharacteristic.MovieView;
                case Entities.SeatMap.SeatCharacteristic.IndividualEarphone:
                    return SeatCharacteristic.IndividualEarphone;
                case Entities.SeatMap.SeatCharacteristic.NoEarphone:
                    return SeatCharacteristic.NoEarphone;
                case Entities.SeatMap.SeatCharacteristic.AdultWithInfant:
                    return SeatCharacteristic.AdultWithInfant;
                case Entities.SeatMap.SeatCharacteristic.UnaccompaniedMinor:
                    return SeatCharacteristic.UnaccompaniedMinor;
                case Entities.SeatMap.SeatCharacteristic.Smoking:
                    return SeatCharacteristic.Smoking;
                case Entities.SeatMap.SeatCharacteristic.AngledRight:
                    return SeatCharacteristic.AngledRight;
                case Entities.SeatMap.SeatCharacteristic.BassinetFacility:
                    return SeatCharacteristic.BassinetFacility;
                case Entities.SeatMap.SeatCharacteristic.LeftOfAircraft:
                    return SeatCharacteristic.LeftOfAircraft;
                case Entities.SeatMap.SeatCharacteristic.RestrictedRecline:
                    return SeatCharacteristic.RestrictedRecline;
                case Entities.SeatMap.SeatCharacteristic.RightOfAircraft:
                    return SeatCharacteristic.RightOfAircraft;
                case Entities.SeatMap.SeatCharacteristic.RearFacing:
                    return SeatCharacteristic.RearFacing;
                case Entities.SeatMap.SeatCharacteristic.WindowSeatWithNoWindow:
                    return SeatCharacteristic.WindowSeatWithNoWindow;
                case Entities.SeatMap.SeatCharacteristic.AdjacentToTable:
                    return SeatCharacteristic.AdjacentToTable;
                case Entities.SeatMap.SeatCharacteristic.Restricted:
                    return SeatCharacteristic.Restricted;
                case Entities.SeatMap.SeatCharacteristic.AdjacentToCloset:
                    return SeatCharacteristic.AdjacentToCloset;
                case Entities.SeatMap.SeatCharacteristic.AdjacentToStairsToUpperdeck:
                    return SeatCharacteristic.AdjacentToStairsToUpperdeck;
                case Entities.SeatMap.SeatCharacteristic.AngledLeft:
                    return SeatCharacteristic.AngledLeft;
                case Entities.SeatMap.SeatCharacteristic.AdjacentToBar:
                    return SeatCharacteristic.AdjacentToBar;
                case Entities.SeatMap.SeatCharacteristic.None:
                    return SeatCharacteristic.None;
                case Entities.SeatMap.SeatCharacteristic.NoSeat:
                    return SeatCharacteristic.NoSeat;
                default:
                    throw new ArgumentException(
                        string.Format("Seat characteristic not recognized - {0}", seatCharacteristic),
                        "seatCharacteristic");
            }
        }
        
    }

    #endregion
}
