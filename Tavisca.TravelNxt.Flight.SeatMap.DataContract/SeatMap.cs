﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class SeatMap
    {
        [DataMember]
        public CabinType CabinType { get; set; }

        [DataMember]
        public string ClassOfService { get; set; }

        [DataMember]
        public List<Cabin> Cabins { get; set; }

        #region Translation

        public static SeatMap ToDataContract(Entities.SeatMap.SeatMap seatMap)
        {
            if(seatMap == null)
                return null;

            return new SeatMap()
                       {
                           ClassOfService = seatMap.ClassOfService,
                           CabinType = EnumerationTranslations.ToDataContract(seatMap.CabinType),
                           Cabins =
                               seatMap.Cabins == null
                                   ? new List<Cabin>()
                                   : seatMap.Cabins.Select(Cabin.ToDataContract).ToListBuffered()
                       };
        }

        

        #endregion
    }
}
