using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.SeatMap.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public class SeatRow
    {
        [DataMember]
        public List<RowCharacteristic> RowCharacteristics { get; set; }

        [DataMember]
        public int RowNumber { get; set; }

        #region Translation

        public static SeatRow ToDataContract(Entities.SeatMap.SeatRow seatRow)
        {
            return new SeatRow()
                       {
                           RowNumber = seatRow.RowNumber,
                           RowCharacteristics =
                               seatRow.RowCharacteristics == null
                                   ? new List<RowCharacteristic>()
                                   : seatRow.RowCharacteristics.Select(EnumerationTranslations.ToDataContract).
                                         ToListBuffered()
                       };
        }

        #endregion
    }
}