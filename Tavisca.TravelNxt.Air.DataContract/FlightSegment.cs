﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightSegment
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public int RefId { get; set; }

        [DataMember]
        public string FlightNumber { get; set; }

        [DataMember]
        public string ClassOfService { get; set; }

        [DataMember]
        public CabinType CabinType { get; set; }

        [DataMember]
        public List<PaxTypeFareBasis> PaxTypeFareBasisCodes { get; set; }

        [DataMember]
        public string MarketingAirlineCode { get; set; }
        //[DataMember]
        //public string AirlineFlightNumber { get; set; }
        [DataMember]
        public string OperatingAirlineCode { get; set; }
        [DataMember]
        public string CodeShareText { get; set; }
        [DataMember]
        public string AircraftType { get; set; }

        // Travel dates and points
        [DataMember]
        public string DepartureAirportCode { get; set; }
        [DataMember]
        public string ArrivalAirportCode { get; set; }
        [DataMember]
        public string DepartureTerminal { get; set; }
        [DataMember]
        public string ArrivalTerminal { get; set; }
        [DataMember]
        public DateTime DepartureDateTime { get; set; }
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }

        /// <summary>
        /// Number of plane change stops
        /// </summary>
        [DataMember]
        public List<SegmentStop> InSegmentStops { get; set; }

        /// <summary>
        /// Whether the particular flight in the pricing recommendation can be eticketed
        /// </summary>
        [DataMember]
        public bool CanETicket { get; set; }

        [DataMember]
        public int DurationMinutes { get; set; }

        [DataMember]
        public int DistanceTraveledKms { get; set; }

        [DataMember]
        public List<PaxTypeBaggage> PaxTypeBaggages { get; set; }

        //[DataMember]
        //public Dictionary<string, string> AdditionalInfo { get; set; }

        #region Translation

        public static FlightSegment ToDataContract(Entities.FlightSegment flightSegment)
        {
            var paxTypeFareBasis = flightSegment.PaxTypeFareBasisCodes.First();

            return new FlightSegment()
                       {
                           Key = flightSegment.Key,
                           //AdditionalInfo = AdditionalInfoDictionary.ToDictionary(flightSegment.AdditionalInfo),
                           FlightNumber = flightSegment.FlightNumber,
                           ClassOfService = paxTypeFareBasis.ClassOfService,
                           CabinType = SearchSegment.ToDataContract(paxTypeFareBasis.CabinType),
                           PaxTypeFareBasisCodes = flightSegment.PaxTypeFareBasisCodes == null ? null : flightSegment.PaxTypeFareBasisCodes.Select(PaxTypeFareBasis.ToDataContract).ToListBuffered(),
                           MarketingAirlineCode = Airline.ToDataContract(flightSegment.MarketingAirline),
                           OperatingAirlineCode = Airline.ToDataContract(flightSegment.OperatingAirline),
                           CodeShareText = flightSegment.CodeShareText,
                           ArrivalAirportCode = Airport.ToDataContract(flightSegment.ArrivalAirport),
                           DepartureAirportCode = Airport.ToDataContract(flightSegment.DepartureAirport),
                           CanETicket = flightSegment.CanETicket,
                           DurationMinutes = flightSegment.DurationMinutes,
                           DistanceTraveledKms = flightSegment.DistanceTraveledKms,
                           InSegmentStops = flightSegment.InSegmentStops == null ? null : flightSegment.InSegmentStops.Select(SegmentStop.ToDataContract).ToListBuffered(flightSegment.InSegmentStops.Count),
                           DepartureDateTime = DateTime.SpecifyKind(flightSegment.DepartureDateTime,DateTimeKind.Utc),
                           ArrivalDateTime = DateTime.SpecifyKind(flightSegment.ArrivalDateTime,DateTimeKind.Utc),
                           DepartureTerminal = flightSegment.DepartureTerminal,
                           ArrivalTerminal = flightSegment.ArrivalTerminal,
                           AircraftType = flightSegment.AircraftType,
                           PaxTypeBaggages = flightSegment.BaggageDetails == null ? null : flightSegment.BaggageDetails.Select(PaxTypeBaggage.ToDataContract).ToListBuffered(flightSegment.BaggageDetails.Count)
                       };
        }

        public static Entities.FlightSegment ToEntity(FlightSegment flightSegment)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

}
