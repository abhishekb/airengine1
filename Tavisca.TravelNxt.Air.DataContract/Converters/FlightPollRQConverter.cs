﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Serialization.Specialized;
using Tavisca.TravelNxt.Common.Analytics;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Converters
{
	public class FlightPollRQConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightPollRQ>
	{
		public string Tag { get; set; }

        //public FlightPollRQConverter(){ }

        //public FlightPollRQConverter(string tag)
        //{ 
        //    Tag = tag;
        //}

		public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightPollRQ value, string parentId = null)
		{
			if (value == null)
				return new List<InfluxDbTable>();

			var table = new InfluxDbTable();

            table.Name = "flightPollRQ";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"getAsSupplierResponds",
			"previousTimeStamp",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.GetAsSupplierResponds,
									value.PreviousTimeStamp,
                
                                }
                        }
                };

			if (!string.IsNullOrWhiteSpace(parentId))
			{
				((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
				((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
			}

			if (!string.IsNullOrWhiteSpace(Tag))
			{
				((List<string>)table.Columns).Add("tag");
				((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
			}

			var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);


			
            return retVal;
		}
	}
}
