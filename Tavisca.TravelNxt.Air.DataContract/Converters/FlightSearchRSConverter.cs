using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Serialization.Specialized;
using Tavisca.TravelNxt.Common.Analytics;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Converters
{
    public class FlightSearchRSConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS>
    {
        public string Tag { get; set; }

        //public FlightSearchRSConverter() { }

        //public FlightSearchRSConverter(string tag)
        //{
        //    Tag = tag;
        //}

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightSearchRS";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"sessionID",
			"inProgress",
			"timeStamp",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.SessionID,
									value.InProgress,
									value.TimeStamp,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var serviceStatusConverter = new ServiceStatusConverter("ServiceStatus");

            retVal.AddRange(serviceStatusConverter.GetTables(value.ServiceStatus, uid));
            if (value.Airports != null)
            {
                var airportConverter = new AirportConverter("Airports");
                foreach (var item in value.Airports)
                {
                    retVal.AddRange(airportConverter.GetTables(item, uid));
                }
            }
            if (value.Airlines != null)
            {
                var airlineConverter = new AirlineConverter("Airlines");
                foreach (var item in value.Airlines)
                {
                    retVal.AddRange(airlineConverter.GetTables(item, uid));
                }
            }
            if (value.ProviderSpaces != null)
            {
                var providerSpaceConverter = new ProviderSpaceConverter("ProviderSpaces");
                foreach (var item in value.ProviderSpaces)
                {
                    retVal.AddRange(providerSpaceConverter.GetTables(item, uid));
                }
            }
            if (value.LegAlternates != null)
            {
                var flightLegAlternatesConverter = new FlightLegAlternatesConverter("LegAlternates");
                foreach (var item in value.LegAlternates)
                {
                    retVal.AddRange(flightLegAlternatesConverter.GetTables(item, uid));
                }
            }
            if (value.LegRecommendations != null)
            {
                var flightRecommendationConverter = new FlightRecommendationConverter("LegRecommendations");
                foreach (var item in value.LegRecommendations)
                {
                    retVal.AddRange(flightRecommendationConverter.GetTables(item, uid));
                }
            }
            if (value.ItineraryRecommendations != null)
            {
                var flightRecommendationConverter = new FlightRecommendationConverter("ItineraryRecommendations");
                foreach (var item in value.ItineraryRecommendations)
                {
                    retVal.AddRange(flightRecommendationConverter.GetTables(item, uid));
                }
            }
            if (value.Segments != null)
            {
                var flightSegmentConverter = new FlightSegmentConverter("Segments");
                foreach (var item in value.Segments)
                {
                    retVal.AddRange(flightSegmentConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class ServiceStatusConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.ServiceStatus>
    {
        public string Tag { get; set; }

        public ServiceStatusConverter() { }

        public ServiceStatusConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.ServiceStatus value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "serviceStatus";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"statusCode",
			"statusMessage",
			"status",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.StatusCode,
									value.StatusMessage,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.CallStatus), value.Status),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var messagesConverter = new StringCollectionConverter<string>("serviceStatus_Messages");
            retVal.AddRange(messagesConverter.GetTables(value.Messages, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class AirportConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.Airport>
    {
        public string Tag { get; set; }

        public AirportConverter() { }

        public AirportConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.Airport value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "airport";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"code",
			"fullName",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Code,
									value.FullName,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var cityConverter = new CityConverter("City");

            retVal.AddRange(cityConverter.GetTables(value.City, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class AirlineConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.Airline>
    {
        public string Tag { get; set; }

        public AirlineConverter() { }

        public AirlineConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.Airline value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "airline";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"code",
			"fullName",
			"websiteURL",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Code,
									value.FullName,
									value.WebsiteURL,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class ProviderSpaceConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.ProviderSpace>
    {
        public string Tag { get; set; }

        public ProviderSpaceConverter() { }

        public ProviderSpaceConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.ProviderSpace value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "providerSpace";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"iD",
			"name",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.ID,
									value.Name,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightLegAlternatesConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightLegAlternates>
    {
        public string Tag { get; set; }

        public FlightLegAlternatesConverter() { }

        public FlightLegAlternatesConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightLegAlternates value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightLegAlternates";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"legIndex",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.LegIndex,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            if (value.Legs != null)
            {
                var flightLegConverter = new FlightLegConverter("Legs");
                foreach (var item in value.Legs)
                {
                    retVal.AddRange(flightLegConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightRecommendationConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightRecommendation>
    {
        public string Tag { get; set; }

        public FlightRecommendationConverter() { }

        public FlightRecommendationConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightRecommendation value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightRecommendation";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"rateCode",
			"refID",
			"itineraryType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.RateCode,
									value.RefID,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.ItineraryTypeOptions), value.ItineraryType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var legRefsConverter = new StringCollectionConverter<string>("flightRecommendation_LegRefs");
            retVal.AddRange(legRefsConverter.GetTables(value.LegRefs, uid));
            var fareConverter = new FlightFareConverter("Fare");

            retVal.AddRange(fareConverter.GetTables(value.Fare, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightSegmentConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSegment>
    {
        public string Tag { get; set; }

        public FlightSegmentConverter() { }

        public FlightSegmentConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSegment value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightSegment";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"key",
			"refId",
			"flightNumber",
			"classOfService",
			"marketingAirlineCode",
			"operatingAirlineCode",
			"codeShareText",
			"aircraftType",
			"departureAirportCode",
			"arrivalAirportCode",
			"departureTerminal",
			"arrivalTerminal",
			"departureDateTime",
			"arrivalDateTime",
			"canETicket",
			"durationMinutes",
			"distanceTraveledKms",
			"cabinType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Key,
									value.RefId,
									value.FlightNumber,
									value.ClassOfService,
									value.MarketingAirlineCode,
									value.OperatingAirlineCode,
									value.CodeShareText,
									value.AircraftType,
									value.DepartureAirportCode,
									value.ArrivalAirportCode,
									value.DepartureTerminal,
									value.ArrivalTerminal,
									value.DepartureDateTime,
									value.ArrivalDateTime,
									value.CanETicket,
									value.DurationMinutes,
									value.DistanceTraveledKms,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.CabinType), value.CabinType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            if (value.PaxTypeFareBasisCodes != null)
            {
                var paxTypeFareBasisConverter = new PaxTypeFareBasisConverter("PaxTypeFareBasisCodes");
                foreach (var item in value.PaxTypeFareBasisCodes)
                {
                    retVal.AddRange(paxTypeFareBasisConverter.GetTables(item, uid));
                }
            }
            if (value.InSegmentStops != null)
            {
                var segmentStopConverter = new SegmentStopConverter("InSegmentStops");
                foreach (var item in value.InSegmentStops)
                {
                    retVal.AddRange(segmentStopConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class CityConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.City>
    {
        public string Tag { get; set; }

        public CityConverter() { }

        public CityConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.City value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "city";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"countryCode",
			"stateCode",
			"code",
			"name",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.CountryCode,
									value.StateCode,
									value.Code,
									value.Name,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightLegConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightLeg>
    {
        public string Tag { get; set; }

        public FlightLegConverter() { }

        public FlightLegConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightLeg value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightLeg";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"refID",
			"numberOfStops",
			"providerSpaceRefId",
			"legType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.RefID,
									value.NumberOfStops,
									value.ProviderSpaceRefId,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.LegType), value.LegType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var flightSegmentRefsConverter = new StringCollectionConverter<int>("flightLeg_FlightSegmentRefs");
            retVal.AddRange(flightSegmentRefsConverter.GetTables(value.FlightSegmentRefs, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightFareConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FlightFare>
    {
        public string Tag { get; set; }

        public FlightFareConverter() { }

        public FlightFareConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FlightFare value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightFare";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"fareType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.FareType), value.FareType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();
            var totalMarkupConverter = new MoneyConverter("TotalMarkup");

            retVal.AddRange(totalMarkupConverter.GetTables(value.TotalMarkup, uid));
            var totalFeesConverter = new MoneyConverter("TotalFees");

            retVal.AddRange(totalFeesConverter.GetTables(value.TotalFees, uid));
            var totalCommissionConverter = new MoneyConverter("TotalCommission");

            retVal.AddRange(totalCommissionConverter.GetTables(value.TotalCommission, uid));
            var totalTaxesConverter = new MoneyConverter("TotalTaxes");

            retVal.AddRange(totalTaxesConverter.GetTables(value.TotalTaxes, uid));
            var baseAmountConverter = new MoneyConverter("BaseAmount");

            retVal.AddRange(baseAmountConverter.GetTables(value.BaseAmount, uid));
            var totalFareConverter = new MoneyConverter("TotalFare");

            retVal.AddRange(totalFareConverter.GetTables(value.TotalFare, uid));
            if (value.PassengerFares != null)
            {
                var passengerFareConverter = new PassengerFareConverter("PassengerFares");
                foreach (var item in value.PassengerFares)
                {
                    retVal.AddRange(passengerFareConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class PaxTypeFareBasisConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.PaxTypeFareBasis>
    {
        public string Tag { get; set; }

        public PaxTypeFareBasisConverter() { }

        public PaxTypeFareBasisConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.PaxTypeFareBasis value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "paxTypeFareBasis";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"fareBasisCode",
			"passengerType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.FareBasisCode,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerType), value.PassengerType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class SegmentStopConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.SegmentStop>
    {
        public string Tag { get; set; }

        public SegmentStopConverter() { }

        public SegmentStopConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.SegmentStop value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "segmentStop";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"airportCode",
			"duration",
			"arrivalTerminal",
			"departureTerminal",
			"arrivalDateTime",
			"departureDateTime",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.AirportCode,
									value.Duration,
									value.ArrivalTerminal,
									value.DepartureTerminal,
									value.ArrivalDateTime,
									value.DepartureDateTime,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class MoneyConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.Money>
    {
        public string Tag { get; set; }

        public MoneyConverter() { }

        public MoneyConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.Money value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "money";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"displayCurrency",
			"displayAmount",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.DisplayCurrency,
									value.DisplayAmount,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class PassengerFareConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerFare>
    {
        public string Tag { get; set; }

        public PassengerFareConverter() { }

        public PassengerFareConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerFare value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "passengerFare";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"quantity",
			"passengerType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Quantity,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerType), value.PassengerType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var totalMarkupConverter = new MoneyConverter("TotalMarkup");

            retVal.AddRange(totalMarkupConverter.GetTables(value.TotalMarkup, uid));
            var totalFeesConverter = new MoneyConverter("TotalFees");

            retVal.AddRange(totalFeesConverter.GetTables(value.TotalFees, uid));
            var totalCommissionConverter = new MoneyConverter("TotalCommission");

            retVal.AddRange(totalCommissionConverter.GetTables(value.TotalCommission, uid));
            var totalTaxesConverter = new MoneyConverter("TotalTaxes");

            retVal.AddRange(totalTaxesConverter.GetTables(value.TotalTaxes, uid));
            var totalDiscountConverter = new MoneyConverter("TotalDiscount");

            retVal.AddRange(totalDiscountConverter.GetTables(value.TotalDiscount, uid));
            var baseAmountConverter = new MoneyConverter("BaseAmount");

            retVal.AddRange(baseAmountConverter.GetTables(value.BaseAmount, uid));
            var totalAmountConverter = new MoneyConverter("TotalAmount");

            retVal.AddRange(totalAmountConverter.GetTables(value.TotalAmount, uid));
            if (value.Markups != null)
            {
                var fareComponentConverter = new FareComponentConverter("Markups");
                foreach (var item in value.Markups)
                {
                    retVal.AddRange(fareComponentConverter.GetTables(item, uid));
                }
            }
            if (value.Taxes != null)
            {
                var fareComponentConverter = new FareComponentConverter("Taxes");
                foreach (var item in value.Taxes)
                {
                    retVal.AddRange(fareComponentConverter.GetTables(item, uid));
                }
            }
            if (value.Commissions != null)
            {
                var fareComponentConverter = new FareComponentConverter("Commissions");
                foreach (var item in value.Commissions)
                {
                    retVal.AddRange(fareComponentConverter.GetTables(item, uid));
                }
            }
            if (value.Fees != null)
            {
                var fareComponentConverter = new FareComponentConverter("Fees");
                foreach (var item in value.Fees)
                {
                    retVal.AddRange(fareComponentConverter.GetTables(item, uid));
                }
            }
            if (value.Discounts != null)
            {
                var fareComponentConverter = new FareComponentConverter("Discounts");
                foreach (var item in value.Discounts)
                {
                    retVal.AddRange(fareComponentConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FareComponentConverter : IInfluxDbTableConverter<Tavisca.TravelNxt.Flight.Avail.DataContract.FareComponent>
    {
        public string Tag { get; set; }

        public FareComponentConverter() { }

        public FareComponentConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Tavisca.TravelNxt.Flight.Avail.DataContract.FareComponent value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "fareComponent";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"code",
			"fareComponentType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Code,
									Enum.GetName(typeof(Tavisca.TravelNxt.Flight.Avail.DataContract.FareComponentType), value.FareComponentType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var valueConverter = new MoneyConverter("Value");

            retVal.AddRange(valueConverter.GetTables(value.Value, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
}
