﻿using System;
using System.Collections.Generic;
using Tavisca.Frameworks.Serialization.Specialized;
using Tavisca.TravelNxt.Common.Analytics;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Converters
{
    public class FlightSearchRQConverter : IInfluxDbTableConverter<FlightSearchRQ>
    {
        public string Tag { get; set; }

        public FlightSearchRQConverter() { }

        //public FlightSearchRQConverter(string tag)
        //{
        //    Tag = tag;
        //}

        public ICollection<InfluxDbTable> GetTables(FlightSearchRQ value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightSearchRQ";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"doDeferredSearch",
			"allowSupplierStreaming",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.DoDeferredSearch,
									value.AllowSupplierStreaming,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var searchCriterionConverter = new FlightSearchCriteriaConverter("SearchCriterion");

            retVal.AddRange(searchCriterionConverter.GetTables(value.SearchCriterion, uid));
            var requesterConverter = new RequesterConverter("Requester");

            retVal.AddRange(requesterConverter.GetTables(value.Requester, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightSearchCriteriaConverter : IInfluxDbTableConverter<FlightSearchCriteria>
    {
        public string Tag { get; set; }

        public FlightSearchCriteriaConverter() { }

        public FlightSearchCriteriaConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(FlightSearchCriteria value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightSearchCriteria";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"sortingOrder",
			"maxPreferredResults",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									Enum.GetName(typeof(SortingOrder), value.SortingOrder),
									      value.MaxPreferredResults.ToJSCompatibleNullableString(),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var flightTravelPreferenceConverter = new FlightTravelPreferenceConverter("FlightTravelPreference");

            retVal.AddRange(flightTravelPreferenceConverter.GetTables(value.FlightTravelPreference, uid));
            if (value.PassengerInfoSummary != null)
            {
                var paxTypeQuantityConverter = new PaxTypeQuantityConverter("PassengerInfoSummary");
                foreach (var item in value.PassengerInfoSummary)
                {
                    retVal.AddRange(paxTypeQuantityConverter.GetTables(item, uid));
                }
            }
            if (value.SearchSegments != null)
            {
                var searchSegmentConverter = new SearchSegmentConverter("SearchSegments");
                foreach (var item in value.SearchSegments)
                {
                    retVal.AddRange(searchSegmentConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class RequesterConverter : IInfluxDbTableConverter<Requester>
    {
        public string Tag { get; set; }

        public RequesterConverter() { }

        public RequesterConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(Requester value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "requester";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"id",
			"posId",
			"customerType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Id,
									value.PosId,
									value.CustomerType.ToString(),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class FlightTravelPreferenceConverter : IInfluxDbTableConverter<FlightTravelPreference>
    {
        public string Tag { get; set; }

        public FlightTravelPreferenceConverter() { }

        public FlightTravelPreferenceConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(FlightTravelPreference value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "flightTravelPreference";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"includeNonStopOnly",
			"includeDirectOnly",
			"jetFlightsOnly",
			"allowMixedAirlines",
			"unRestrictedFare",
			"noPenalty",
			"refundable",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.IncludeNonStopOnly,
									value.IncludeDirectOnly,
									value.JetFlightsOnly,
									value.AllowMixedAirlines,
									value.UnRestrictedFare,
									value.NoPenalty,
									value.Refundable,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var airlinesPreferenceConverter = new AirlinesPreferenceConverter("AirlinesPreference");

            retVal.AddRange(airlinesPreferenceConverter.GetTables(value.AirlinesPreference, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class PaxTypeQuantityConverter : IInfluxDbTableConverter<PaxTypeQuantity>
    {
        public string Tag { get; set; }

        public PaxTypeQuantityConverter() { }

        public PaxTypeQuantityConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(PaxTypeQuantity value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "paxTypeQuantity";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"quantity",
			"passengerType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.Quantity,
									Enum.GetName(typeof(PassengerType), value.PassengerType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var agesConverter = new StringCollectionConverter<int>("paxTypeQuantity_Ages");
            retVal.AddRange(agesConverter.GetTables(value.Ages, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class SearchSegmentConverter : IInfluxDbTableConverter<SearchSegment>
    {
        public string Tag { get; set; }

        public SearchSegmentConverter() { }

        public SearchSegmentConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(SearchSegment value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "searchSegment";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"departureAirportCode",
			"arrivalAirportCode",
            "departureCityCode",
			"arrivalCityCode",
			"cabin",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.DepartureAirportCode,
									value.ArrivalAirportCode,
                                    value.DepartureCityCode,
									value.ArrivalCityCode,
									Enum.GetName(typeof(CabinType), value.Cabin),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var includeServiceClassConverter = new StringCollectionConverter<string>("searchSegment_IncludeServiceClass");
            retVal.AddRange(includeServiceClassConverter.GetTables(value.IncludeServiceClass, uid));
            var travelDateConverter = new TravelDateConverter("TravelDate");

            retVal.AddRange(travelDateConverter.GetTables(value.TravelDate, uid));
            var arrivalAlternateAirportInformationConverter = new AlternateAirportInformationConverter("ArrivalAlternateAirportInformation");

            retVal.AddRange(arrivalAlternateAirportInformationConverter.GetTables(value.ArrivalAlternateAirportInformation, uid));
            var departureAlternateAirportInformationConverter = new AlternateAirportInformationConverter("DepartureAlternateAirportInformation");

            retVal.AddRange(departureAlternateAirportInformationConverter.GetTables(value.DepartureAlternateAirportInformation, uid));
            if (value.ConnectionPreferences != null)
            {
                var connectionPreferenceConverter = new ConnectionPreferenceConverter("ConnectionPreferences");
                foreach (var item in value.ConnectionPreferences)
                {
                    retVal.AddRange(connectionPreferenceConverter.GetTables(item, uid));
                }
            }

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class AirlinesPreferenceConverter : IInfluxDbTableConverter<AirlinesPreference>
    {
        public string Tag { get; set; }

        public AirlinesPreferenceConverter() { }

        public AirlinesPreferenceConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(AirlinesPreference value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "airlinesPreference";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var preferredAirlinesConverter = new StringCollectionConverter<string>("airlinesPreference_PreferredAirlines");
            retVal.AddRange(preferredAirlinesConverter.GetTables(value.PreferredAirlines, uid));
            var prohibitedAirlinesConverter = new StringCollectionConverter<string>("airlinesPreference_ProhibitedAirlines");
            retVal.AddRange(prohibitedAirlinesConverter.GetTables(value.ProhibitedAirlines, uid));
            var permittedAirlinesConverter = new StringCollectionConverter<string>("airlinesPreference_PermittedAirlines");
            retVal.AddRange(permittedAirlinesConverter.GetTables(value.PermittedAirlines, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class TravelDateConverter : IInfluxDbTableConverter<TravelDate>
    {
        public string Tag { get; set; }

        public TravelDateConverter() { }

        public TravelDateConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(TravelDate value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "travelDate";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"dateTime",
			"timeWindow",
			"anyTime",
			"minDate",
			"maxDate",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.DateTime,
									value.TimeWindow,
									value.AnyTime,
									      value.MinDate.ToJSCompatibleNullableString(),
									      value.MaxDate.ToJSCompatibleNullableString(),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class AlternateAirportInformationConverter : IInfluxDbTableConverter<AlternateAirportInformation>
    {
        public string Tag { get; set; }

        public AlternateAirportInformationConverter() { }

        public AlternateAirportInformationConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(AlternateAirportInformation value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "alternateAirportInformation";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"includeNearByAirports",
			"radiusKm",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									      value.IncludeNearByAirports.ToJSCompatibleNullableString(),
									      value.RadiusKm.ToJSCompatibleNullableString(),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();

            var airportCodesConverter = new StringCollectionConverter<string>("alternateAirportInformation_AirportCodes");
            retVal.AddRange(airportCodesConverter.GetTables(value.AirportCodes, uid));

            retVal.Insert(0, table);



            return retVal;
        }
    }
    public class ConnectionPreferenceConverter : IInfluxDbTableConverter<ConnectionPreference>
    {
        public string Tag { get; set; }

        public ConnectionPreferenceConverter() { }

        public ConnectionPreferenceConverter(string tag)
        {
            Tag = tag;
        }

        public ICollection<InfluxDbTable> GetTables(ConnectionPreference value, string parentId = null)
        {
            if (value == null)
                return new List<InfluxDbTable>();

            var table = new InfluxDbTable();

            table.Name = "connectionPreference";

            var uid = TableHelpers.GenerateUid();

            table.Columns = new List<string>()
                {
                    TableHelpers.GetIdColumn(),
			"airportCode",
			"preferenceType",
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    uid,
									value.AirportCode,
									Enum.GetName(typeof(ConnectionPreferenceType), value.PreferenceType),
                
                                }
                        }
                };

            if (!string.IsNullOrWhiteSpace(parentId))
            {
                ((List<string>)table.Columns).Add(TableHelpers.GetParentIdColumn());
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(parentId);
            }

            if (!string.IsNullOrWhiteSpace(Tag))
            {
                ((List<string>)table.Columns).Add("tag");
                ((List<InfluxDbTableRow>)table.Rows)[0].Items.Add(Tag);
            }

            var retVal = new List<InfluxDbTable>();


            retVal.Insert(0, table);



            return retVal;
        }
    }
}
