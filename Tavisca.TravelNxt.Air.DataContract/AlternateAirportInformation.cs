﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(AlternateAirportInformationValidator))]
    public class AlternateAirportInformation
    {
        [DataMember]
        public List<string> AirportCodes { get; set; }
        [DataMember]
        public bool? IncludeNearByAirports { get; set; }
        [DataMember]
        public int? RadiusKm { get; set; }

        #region Translation

        public static AlternateAirportInformation ToDataContract(
            Entities.Avail.AlternateAirportInformation alternateAirportInformation)
        {
            if (alternateAirportInformation == null)
                return null;

            return new AlternateAirportInformation()
                {
                    AirportCodes = alternateAirportInformation.AirportCodes == null ? 
                                    null : new List<string>(alternateAirportInformation.AirportCodes),
                    IncludeNearByAirports = alternateAirportInformation.IncludeNearByAirports,
                    RadiusKm = alternateAirportInformation.RadiusKm
                };
        }

        public static Entities.Avail.AlternateAirportInformation ToEntity(
            AlternateAirportInformation alternateAirportInformation)
        {
            if (alternateAirportInformation == null)
                return null;

            return new Entities.Avail.AlternateAirportInformation()
            {
                AirportCodes = alternateAirportInformation.AirportCodes == null ?
                                null : new List<string>(alternateAirportInformation.AirportCodes),
                IncludeNearByAirports = alternateAirportInformation.IncludeNearByAirports,
                RadiusKm = alternateAirportInformation.RadiusKm
            };
        }

        #endregion
    }
}