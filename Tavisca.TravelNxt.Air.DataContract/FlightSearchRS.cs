﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;
using System.Linq;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightSearchRS
    {

        private DateTime _timeStamp;

        [DataMember]
        public Guid SessionID { get; set; }

        [DataMember]
        public ServiceStatus ServiceStatus { get; set; }
        
        [DataMember]
        public bool InProgress { get; set; }
        [DataMember]
        public List<Airport> Airports { get; set; }
        [DataMember]
        public List<Airline> Airlines { get; set; }
        [DataMember]
        public List<ProviderSpace> ProviderSpaces { get; set; }
        [DataMember]
        public List<FlightLegAlternates> LegAlternates { get; set; }
        [DataMember]
        public List<FlightRecommendation> LegRecommendations { get; set; }
        [DataMember]
        public List<FlightRecommendation> ItineraryRecommendations { get; set; }
        [DataMember]
        public List<FlightSegment> Segments { get; set; }
        [DataMember]
        public DateTime TimeStamp {
            get
            {
                /* in case of default dateTime Json serialization will throw error
                 * which returns "unknown error" or "gateway timeout" or "underlying connection timeout" exceptions at client side
                 * to resolve that issue, we need to set the time to unix default value 
                 */ 
                 
                if (_timeStamp == DateTime.MinValue)
                {
                    _timeStamp = new DateTime(1970,1,1);
                }

                return _timeStamp;

            } 
            set
            {
                _timeStamp = value;
            }
        }

        [DataMember]
        public List<Baggage> Baggages { get; set; } 

        #region Translation

        public static FlightSearchRS ToDataContract(FlightAvailResult availResult)
        {
            var response = new FlightSearchRS
                               {
                                   LegRecommendations = new List<FlightRecommendation>(),
                                   ItineraryRecommendations = new List<FlightRecommendation>(),
                                   ServiceStatus =
                                       new ServiceStatus()
                                           {
                                               StatusCode = availResult.StatusCode,
                                               Status = EnumParsers.ToDataContract(availResult.Status),
                                               StatusMessage = availResult.StatusMessage
                                           },
                                    ProviderSpaces = new List<ProviderSpace>(),
                                    InProgress = availResult.Status == Entities.CallStatus.InProgress,
                                    Baggages = new List<Baggage>()
                               };

            if (availResult.Messages != null)
            {
                response.ServiceStatus.Messages = new List<string>();
                availResult.Messages.ForEach(response.ServiceStatus.Messages.Add);
            }

            var legAlternatesIndexer = new Dictionary<int, FlightLegAlternates>();
            var segmentIndexer = new Dictionary<string, FlightSegment>();
            var legRefs = new Dictionary<string, int>();
            var legNo = 0;
            var segNo = 0;
            var providerIndexer = new Dictionary<int, int>();
            var providerIndexNo = 0;
            var airlines = new HashSet<Entities.Airline>();
            var airports = new HashSet<Entities.Airport>();
            var baggageIndexer = new Dictionary<string, Baggage>();

            if (availResult.FlightRecommendations != null)
            {
                foreach (var recommendation in availResult.FlightRecommendations)
                {
                    if(!providerIndexer.ContainsKey(recommendation.ProviderSpace.ID))
                    {
                        response.ProviderSpaces.Add(ProviderSpace.ToDataContract(recommendation.ProviderSpace));
                        providerIndexer[recommendation.ProviderSpace.ID] = providerIndexNo;
                        providerIndexNo++;
                    }

                    var flightRecommendation = new FlightRecommendation
                        {
                            RefID = recommendation.RefId,
                            LegRefs = new List<string>(),
                            RateCode = recommendation.RateCode,
                            PlatingCarrier=recommendation.PlatingCarrier,
                            Contract = AirContract.ToDataContract(recommendation.Contract)
                        };

                    foreach (var leg in recommendation.Legs)
                    {
                        FlightLegAlternates legAlternate;

                        var index = leg.LegIndex;
                        if (index == 0 && leg.LegType == Entities.LegType.Return)
                            index++;

                        if (!legAlternatesIndexer.TryGetValue(index, out legAlternate))
                        {
                            legAlternate = new FlightLegAlternates
                                {
                                    LegIndex = index,
                                    Legs = new List<FlightLeg>()
                                };
                            legAlternatesIndexer.Add(index, legAlternate);
                        }
                        if (!legRefs.ContainsKey(leg.Key))
                        {
                            legNo++;
                            legRefs.Add(leg.Key, legNo);
                            flightRecommendation.LegRefs.Add(string.Format(LegRefFormat, index, legNo));
                            var flightLeg = new FlightLeg
                                                {
                                                    LegType = EnumParsers.ToDataContract(leg.LegType),
                                                    //AdditionalInfo =
                                                    //    AdditionalInfoDictionary.ToDictionary(leg.AdditionalInfo),
                                                    FlightSegmentRefs = new List<int>(),
                                                    RefID = legNo,
                                                    ProviderSpaceRefId = providerIndexer[recommendation.ProviderSpace.ID]
                                                };
                            legAlternate.Legs.Add(flightLeg);
                            foreach (var segment in leg.Segments)
                            {
                                airports.Add(segment.ArrivalAirport);
                                airports.Add(segment.DepartureAirport);

                                airlines.Add(segment.MarketingAirline);
                                airlines.Add(segment.OperatingAirline);

                                FlightSegment flightSegment;
                                if (!segmentIndexer.TryGetValue(segment.Key, out flightSegment))
                                {
                                    segNo++;
                                    flightSegment = FlightSegment.ToDataContract(segment);
                                    flightSegment.RefId = segNo;
                                    segmentIndexer.Add(segment.Key, flightSegment);
                                }

                                flightLeg.FlightSegmentRefs.Add(flightSegment.RefId);

                                if (segment.InSegmentStops != null)
                                {
                                    flightLeg.NumberOfStops += segment.InSegmentStops.Count;

                                    foreach (var inSegmentStop in segment.InSegmentStops)
                                        airports.Add(inSegmentStop.Airport);
                                }

                                if (segment.BaggageDetails != null)
                                {
                                    foreach (var baggage in segment.BaggageDetails)
                                    {
                                        Baggage flightBaggage;
                                        if(!baggageIndexer.TryGetValue(baggage.BaggageCode, out flightBaggage))
                                        {
                                            flightBaggage = Baggage.ToDataContract(baggage);
                                            baggageIndexer.Add(baggage.BaggageCode, flightBaggage);
                                        }
                                    }
                                }
                            }
                            flightLeg.NumberOfStops += (flightLeg.FlightSegmentRefs.Count - 1);
                        }
                        else
                            flightRecommendation.LegRefs.Add(string.Format(LegRefFormat, index, legRefs[leg.Key]));
                    }

                    flightRecommendation.Fare = FlightFare.ToDataContract(recommendation.Fare);
                    //flightRecommendation.AdditionalInfo = AdditionalInfoDictionary.ToDictionary(recommendation.AdditionalInfo);
                    flightRecommendation.ItineraryType = EnumParsers.ToDataContract(recommendation.ItineraryType);

                    if (flightRecommendation.LegRefs.Count == 1)
                        response.LegRecommendations.Add(flightRecommendation);
                    else
                        response.ItineraryRecommendations.Add(flightRecommendation);
                }
            }

            response.LegAlternates = legAlternatesIndexer.Values.ToListBuffered();
            response.Segments = segmentIndexer.Values.ToListBuffered();
            response.Baggages = baggageIndexer.Values.ToListBuffered();

            response.Airlines = airlines.Where(x => x != null).Select(Airline.ToDataContractObject).ToListBuffered(airlines.Count);
            response.Airports = airports.Select(Airport.ToDataContractObject).ToListBuffered(airports.Count);

            switch (availResult.Status)
            {
                case Entities.CallStatus.InProgress:
                    response.InProgress = true;
                    break;
                default:
                    response.InProgress = false;
                    break;
            }

            response.TimeStamp = DateTime.SpecifyKind(availResult.TimeStamp, DateTimeKind.Utc); 

            response.SessionID = Utility.GetCurrentContext().SessionId;
            return response;
        }


        private const string LegRefFormat = "{0}/{1}";

        #endregion
    }
}