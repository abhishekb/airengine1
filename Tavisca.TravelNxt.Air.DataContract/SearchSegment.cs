﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(SearchSegmentValidator))]
    public class SearchSegment
    {
        [DataMember]
        public string DepartureAirportCode { get; set; }
        [DataMember]
        public string ArrivalAirportCode { get; set; }

        [DataMember]
        public string DepartureCityCode { get; set; }
        [DataMember]
        public string ArrivalCityCode { get; set; }

        private TravelDate _travelDate;
        [DataMember]
        public TravelDate TravelDate
        {
            get
            {
                if (_travelDate != null)
                {
                    if (!string.IsNullOrWhiteSpace(this.DepartureCityCode))
                    {
                        _travelDate.IsDepartureCity = true;
                        _travelDate.DepartureLocation = this.DepartureCityCode;
                    }
                    else
                    {
                        _travelDate.DepartureLocation = this.DepartureAirportCode;
                        _travelDate.IsDepartureCity = false;
                    }
                }

                return _travelDate;
            }
            set { _travelDate = value; }
        }

        [DataMember]
        public CabinType Cabin { get; set; }
        [DataMember]
        public AlternateAirportInformation ArrivalAlternateAirportInformation { get; set; }
        [DataMember]
        public AlternateAirportInformation DepartureAlternateAirportInformation { get; set; }
        [DataMember]
        public List<ConnectionPreference> ConnectionPreferences { get; set; }
        [DataMember]
        public List<string> IncludeServiceClass { get; set; }
        #region Translation

        public static SearchSegment ToDataContract(Entities.Avail.SearchSegment searchSegment)
        {
            var contract = new SearchSegment()
                               {
                                   TravelDate = searchSegment.TravelDate == null
                                                    ? null
                                                    : TravelDate.ToDataContract(searchSegment.TravelDate),
                                   ArrivalAirportCode = searchSegment.ArrivalAirportCode,
                                   Cabin = ToDataContract(searchSegment.Cabin),
                                   ConnectionPreferences = searchSegment.ConnectionPreferences == null
                                                               ? null
                                                               : searchSegment.ConnectionPreferences.Select(
                                                                   ConnectionPreference.ToDataContract).ToListBuffered(2),
                                   DepartureAirportCode = searchSegment.DepartureAirportCode,
                                   ArrivalAlternateAirportInformation =
                                       searchSegment.ArrivalAlternateAirportInformation == null
                                           ? null
                                           : AlternateAirportInformation.ToDataContract(
                                               searchSegment.ArrivalAlternateAirportInformation),
                                   DepartureAlternateAirportInformation =
                                       searchSegment.DepartureAlternateAirportInformation == null
                                           ? null
                                           : AlternateAirportInformation.ToDataContract(
                                               searchSegment.DepartureAlternateAirportInformation),
                                   IncludeServiceClass =
                                       searchSegment.IncludeServiceClass == null
                                           ? null
                                           : new List<string>(searchSegment.IncludeServiceClass)
                               };

            return contract;
        }

        public static Entities.Avail.SearchSegment ToEntity(SearchSegment searchSegment)
        {
            var entity = new Entities.Avail.SearchSegment()
            {
                TravelDate = TravelDate.ToEntity(searchSegment.TravelDate),
                ArrivalAirportCode = searchSegment.ArrivalAirportCode,
                Cabin = ToEntity(searchSegment.Cabin),

                ConnectionPreferences = searchSegment.ConnectionPreferences == null
                    ? null
                    : searchSegment.ConnectionPreferences.Select(ConnectionPreference.ToEntity).ToListBuffered(2),
                DepartureAirportCode = searchSegment.DepartureAirportCode,
                ArrivalAlternateAirportInformation =
                    AlternateAirportInformation.ToEntity(searchSegment.ArrivalAlternateAirportInformation),
                DepartureAlternateAirportInformation =
                    AlternateAirportInformation.ToEntity(searchSegment.DepartureAlternateAirportInformation),
                IncludeServiceClass =
                    searchSegment.IncludeServiceClass == null
                        ? null
                        : new List<string>(searchSegment.IncludeServiceClass),
                ArrivalCityCode = searchSegment.ArrivalCityCode,
                DepartureCityCode = searchSegment.DepartureCityCode
            };

            return entity;
        }

        public static CabinType ToDataContract(Entities.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.CabinType.Business:
                    return CabinType.Business;
                case Entities.CabinType.Economy:
                    return CabinType.Economy;
                case Entities.CabinType.First:
                    return CabinType.First;
                case Entities.CabinType.PremiumEconomy:
                    return CabinType.PremiumEconomy;
                case Entities.CabinType.Unknown:
                    return CabinType.Unknown;
            }

            return default(CabinType);
        }

        public static Entities.CabinType ToEntity(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Business:
                    return Entities.CabinType.Business;
                case CabinType.Economy:
                    return Entities.CabinType.Economy;
                case CabinType.First:
                    return Entities.CabinType.First;
                case CabinType.PremiumEconomy:
                    return Entities.CabinType.PremiumEconomy;
                case CabinType.Unknown:
                    return Entities.CabinType.Unknown;
            }

            return default(Entities.CabinType);
        }

        #endregion
    }
}
