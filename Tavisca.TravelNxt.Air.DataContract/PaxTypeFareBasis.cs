﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class PaxTypeFareBasis
    {
        [DataMember]
        public string FareBasisCode { get; set; }

        [DataMember]
        public PassengerType PassengerType { get; set; }

        public static PaxTypeFareBasis ToDataContract(Entities.PaxTypeFareBasis paxFareQualifier)
        {
            return new PaxTypeFareBasis()
                                   {
                                       FareBasisCode = paxFareQualifier.FareBasisCode,
                                       PassengerType = PassengerFare.ToDataContract(paxFareQualifier.PassengerType)
                                   };
        }
    };



}
