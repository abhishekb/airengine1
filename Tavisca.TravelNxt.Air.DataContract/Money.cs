﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Money
    {
        [DataMember]
        public string DisplayCurrency { get; set; }
        [DataMember]
        public Decimal DisplayAmount { get; set; }
        
        private static string GetRequestedCurrency()
        {
            return Utility.GetCurrentContext().DisplayCurrency;
        }

        public override string ToString()
        {
            return this.DisplayAmount.ToString(CultureInfo.InvariantCulture) + " " + this.DisplayCurrency;
        }

        #region Translation

        public static Money ToDataContract(Entities.Money money)
        {
            if (string.IsNullOrEmpty(money.NativeCurrency))
            {
                return new Money()
                           {
                               DisplayAmount = 0,
                               DisplayCurrency = GetRequestedCurrency()

                           };
            }
            Money contract;
            if (money.DisplayAmount != 0 && !string.IsNullOrEmpty(money.DisplayCurrency))
            {
                contract = new Money()
                    {
                        DisplayAmount = Math.Round(money.DisplayAmount, 2),
                        DisplayCurrency = money.DisplayCurrency
                    };
            }
            else
            {
                var displayCurrency = GetRequestedCurrency();

                contract = new Money()
                               {
                                   DisplayAmount = Math.Round(money.ConvertToValue(displayCurrency), 2),
                                   DisplayCurrency = displayCurrency
                               };
            }

            return contract;
        }

        public static Entities.Money ToEntity(Money money)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
