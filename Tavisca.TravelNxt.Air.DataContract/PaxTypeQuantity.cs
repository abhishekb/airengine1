﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(PaxTypeQuantityValidator))]
    public class PaxTypeQuantity
    {
        [DataMember]
        public List<int> Ages { get; set; }
        [DataMember]
        public PassengerType PassengerType { get; set; }
        [DataMember]
        public int Quantity { get; set; }

        #region Translation

        public static PaxTypeQuantity ToDataContract(Entities.PaxTypeQuantity paxTypeQuantity)
        {
            var contract = new PaxTypeQuantity()
                {
                    Ages = paxTypeQuantity.Ages == null ? null : new List<int>(paxTypeQuantity.Ages),
                    Quantity = paxTypeQuantity.Quantity,
                    PassengerType = PassengerFare.ToDataContract(paxTypeQuantity.PassengerType)
                };

            return contract;
        }

        public static Entities.PaxTypeQuantity ToEntity(PaxTypeQuantity paxTypeQuantity)
        {
            var entity = new Entities.PaxTypeQuantity()
            {
                Ages = paxTypeQuantity.Ages,
                Quantity = paxTypeQuantity.Quantity,
                PassengerType = PassengerFare.ToEntity(paxTypeQuantity.PassengerType)
            };

            return entity;
        }

        #endregion
    }
}
