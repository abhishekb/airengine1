﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class SegmentStop
    {
        [DataMember]
        public string AirportCode { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public string ArrivalTerminal { get; set; }
        [DataMember]
        public string DepartureTerminal { get; set; }
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }
        [DataMember]
        public DateTime DepartureDateTime { get; set; }

        #region Translation

        public static SegmentStop ToDataContract(Entities.SegmentStop flightSegment)
        {
            if (flightSegment == null)
                return null;

            return new SegmentStop()
                {
                    AirportCode = Airport.ToDataContract(flightSegment.Airport),
                    ArrivalDateTime = DateTime.SpecifyKind(flightSegment.ArrivalDateTime,DateTimeKind.Utc),
                    ArrivalTerminal = flightSegment.ArrivalTerminal,
                    DepartureDateTime = DateTime.SpecifyKind(flightSegment.DepartureDateTime,DateTimeKind.Utc),
                    DepartureTerminal = flightSegment.DepartureTerminal,
                    Duration = flightSegment.Duration
                };
        }

        #endregion
    }
}
