﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(ConnectionPreferenceValidator))]
    public class ConnectionPreference
    {
        [DataMember]
        public string AirportCode { get; set; }
        [DataMember]
        public ConnectionPreferenceType PreferenceType { get; set; }

        #region Translation

        public static ConnectionPreference ToDataContract(Entities.Avail.ConnectionPreference connectionPreference)
        {
            var contract = new ConnectionPreference()
            {
                AirportCode = connectionPreference.AirportCode,
                PreferenceType = ToDataContract(connectionPreference.PreferenceType)
            };

            return contract;
        }

        public static Entities.Avail.ConnectionPreference ToEntity(ConnectionPreference connectionPreference)
        {
            var entity = new Entities.Avail.ConnectionPreference()
            {
                AirportCode = connectionPreference.AirportCode,
                PreferenceType = ToEntity(connectionPreference.PreferenceType)
            };

            return entity;
        }

        public static ConnectionPreferenceType ToDataContract(Entities.Avail.ConnectionPreferenceType connectionPreferenceType)
        {
            switch (connectionPreferenceType)
            {
                case Entities.Avail.ConnectionPreferenceType.Avoid:
                    return ConnectionPreferenceType.Avoid;
                case Entities.Avail.ConnectionPreferenceType.Via:
                    return ConnectionPreferenceType.Via;
            }

            return default(ConnectionPreferenceType);
        }

        public static Entities.Avail.ConnectionPreferenceType ToEntity(ConnectionPreferenceType connectionPreferenceType)
        {
            switch (connectionPreferenceType)
            {
                case ConnectionPreferenceType.Avoid:
                    return Entities.Avail.ConnectionPreferenceType.Avoid;
                case ConnectionPreferenceType.Via:
                    return Entities.Avail.ConnectionPreferenceType.Via;
            }

            return default(Entities.Avail.ConnectionPreferenceType);
        }

        #endregion
    }
}
