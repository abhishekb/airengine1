﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Baggage
    {
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? Quantity { get; set; }

        [DataMember]
        public decimal? WeightInPounds { get; set; }

        [DataMember]
        public string BaggageCode { get; set; }

        public static Baggage ToDataContract(Entities.Baggage baggage)
        {
            if (baggage == null)
                return null;

            return new Baggage()
                       {
                           Description = baggage.Description,
                           Quantity = baggage.Quantity,
                           WeightInPounds = baggage.WeightInPounds,
                           BaggageCode = baggage.BaggageCode
                       };
        }
    }
}
