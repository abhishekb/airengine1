﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class City
    {
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string StateCode { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }

        #region Translation

        public static City ToDataContract(Entities.City city)
        {
            if (city == null)
                return null;

            var contract = new City()
                {
                    Code = city.Code,
                    CountryCode = city.CountryCode,
                    StateCode = city.StateCode,
                    Name = city.Name
                };

            return contract;
        }

        public static Entities.City ToEntity(City city)
        {
            var entity = new Entities.City()
            {
                Code = city.Code,
                CountryCode = city.CountryCode,
                StateCode = city.StateCode,
                Name = city.Name
            };

            return entity;
        }

        #endregion
    }
}
