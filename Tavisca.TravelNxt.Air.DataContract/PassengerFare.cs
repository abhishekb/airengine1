﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class PassengerFare
    {
        [DataMember]
        public List<FareComponent> Markups { get; set; }
        [DataMember]
        public List<FareComponent> Taxes { get; set; }
        [DataMember]
        public List<FareComponent> Commissions { get; set; }
        [DataMember]
        public List<FareComponent> Fees { get; set; }
        [DataMember]
        public List<FareComponent> Discounts { get; set; }
        [DataMember]
        public Money TotalMarkup { get; set; }
        [DataMember]
        public Money TotalFees { get; set; }
        [DataMember]
        public Money TotalCommission { get; set; }
        [DataMember]
        public Money TotalTaxes { get; set; }
        [DataMember]
        public Money TotalDiscount { get; set; }
        [DataMember]
        public Money BaseAmount { get; set; }
        [DataMember]
        public Money TotalAmount { get; set; }
        [DataMember]
        public PassengerType PassengerType { get; set; }
        [DataMember]
        public int Quantity { get; set; }

        #region Translation

        public static PassengerFare ToDataContract(Entities.PassengerFare passengerFare)
        {
            var contract = new PassengerFare()
                {
                    Fees = passengerFare.Fees.Select(FareComponent.ToDataContract).ToListBuffered(2),
                    Commissions = passengerFare.Commissions.Select(FareComponent.ToDataContract).ToListBuffered(),
                    BaseAmount = Money.ToDataContract(passengerFare.BaseAmount),
                    Markups = passengerFare.Markups.Select(FareComponent.ToDataContract).ToListBuffered(),
                    Taxes = passengerFare.Taxes.Select(FareComponent.ToDataContract).ToListBuffered(),
                    TotalAmount = Money.ToDataContract(passengerFare.TotalAmount),
                    TotalCommission = Money.ToDataContract(passengerFare.TotalCommission),
                    TotalFees = Money.ToDataContract(passengerFare.TotalFees),
                    TotalMarkup = Money.ToDataContract(passengerFare.TotalMarkup),
                    PassengerType = ToDataContract(passengerFare.PassengerType),
                    Quantity = passengerFare.Quantity,
                    TotalTaxes = Money.ToDataContract(passengerFare.TotalTaxes),
                    TotalDiscount = Money.ToDataContract(passengerFare.TotalDiscount),
                    Discounts = passengerFare.Discounts.Select(FareComponent.ToDataContract).ToListBuffered()
                };

            return contract;
        }

        public static Entities.PassengerFare ToEntity(PassengerFare passengerFare)
        {
            var entity = new Entities.PassengerFare()
            {
                Fees = passengerFare.Fees.Select(FareComponent.ToEntity).ToListBuffered(),
                Commissions = passengerFare.Commissions.Select(FareComponent.ToEntity).ToListBuffered(),
                BaseAmount = Money.ToEntity(passengerFare.BaseAmount),
                Markups = passengerFare.Markups.Select(FareComponent.ToEntity).ToListBuffered(),
                Taxes = passengerFare.Taxes.Select(FareComponent.ToEntity).ToListBuffered(),
                PassengerType = ToEntity(passengerFare.PassengerType),
                Discounts = passengerFare.Discounts.Select(FareComponent.ToEntity).ToListBuffered()
            };

            return entity;
        }

        public static PassengerType ToDataContract(Entities.PassengerType passengerType)
        {
            switch (passengerType)
            {
                case Entities.PassengerType.Adult:
                    return PassengerType.Adult;
                case Entities.PassengerType.Child:
                    return PassengerType.Child;
                case Entities.PassengerType.Infant:
                    return PassengerType.Infant;
                case Entities.PassengerType.Senior:
                    return PassengerType.Senior;
                case Entities.PassengerType.MilitaryAdult:
                    return PassengerType.MilitaryAdult;
                case Entities.PassengerType.MilitaryChild:
                    return PassengerType.MilitaryChild;
                case Entities.PassengerType.MilitaryInfant:
                    return PassengerType.MilitaryInfant;
                case Entities.PassengerType.MilitarySenior:
                    return PassengerType.MilitarySenior;
            }

            return default(PassengerType);
        }

        public static Entities.PassengerType ToEntity(PassengerType passengerType)
        {
            switch (passengerType)
            {
                case PassengerType.Adult:
                    return Entities.PassengerType.Adult;
                case PassengerType.Child:
                    return Entities.PassengerType.Child;
                case PassengerType.Infant:
                    return Entities.PassengerType.Infant;
                case PassengerType.Senior:
                    return Entities.PassengerType.Senior;
                case PassengerType.MilitaryAdult:
                    return Entities.PassengerType.MilitaryAdult;
                case PassengerType.MilitaryChild:
                    return Entities.PassengerType.MilitaryChild;
                case PassengerType.MilitaryInfant:
                    return Entities.PassengerType.MilitaryInfant;
                case PassengerType.MilitarySenior:
                    return Entities.PassengerType.Senior;
            }

            return default(Entities.PassengerType);
        }

        #endregion
    }
}
