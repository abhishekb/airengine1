﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightLegAlternates
    {
        [DataMember]
        public List<FlightLeg> Legs { get; set; }
        [DataMember]
        public int LegIndex { get; set; }
    }
}