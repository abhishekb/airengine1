﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightRecommendation
    {
        [DataMember]
        public List<string> LegRefs { get; set; }
        [DataMember]
        public FlightFare Fare { get; set; }
        [DataMember]
        public string RateCode { get; set; }
        [DataMember]
        public AirContract Contract { get; set; }
        //[DataMember]
        //public Dictionary<string, string> AdditionalInfo { get; set; }
        [DataMember]
        public int RefID { get; set; }
        [DataMember]
        public ItineraryTypeOptions ItineraryType { get; set; }
        [DataMember]
        [ProtoIgnore]
        public string PlatingCarrier { get; set; }
    }
}