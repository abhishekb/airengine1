﻿using System;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(TravelDateValidator))]
    public class TravelDate
    {
        [DataMember]
        public DateTime DateTime { get; set; }
        [DataMember]
        public int TimeWindow { get; set; }
        [DataMember]
        public bool AnyTime { get; set; }
        [DataMember]
        public int? MinDate { get; set; }
        [DataMember]
        public int? MaxDate { get; set; }

        public string DepartureLocation { get; set; } //should not be a data member
        
        public bool IsDepartureCity { get; set; }

        #region Translation

        public static TravelDate ToDataContract(Entities.TravelDate travelDate)
        {
            var contract = new TravelDate()
                {
                    AnyTime = travelDate.AnyTime,
                    DateTime = DateTime.SpecifyKind(travelDate.DateTime,DateTimeKind.Utc),
                    TimeWindow = travelDate.TimeWindow,
                    MaxDate = travelDate.MaxDate,
                    MinDate = travelDate.MinDate
                };

            return contract;
        }

        public static Entities.TravelDate ToEntity(TravelDate travelDate)
        {
            var entity = new Entities.TravelDate()
            {
                AnyTime = travelDate.AnyTime,
                DateTime = travelDate.DateTime,
                TimeWindow = travelDate.TimeWindow,
                MinDate = travelDate.MinDate,
                MaxDate = travelDate.MaxDate
            };

            return entity;
        }

        #endregion
    }
}