﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(FlightSearchCriteriaValidator))]
    public class FlightSearchCriteria
    {
        [DataMember]
        public List<PaxTypeQuantity> PassengerInfoSummary { get; set; }
        [DataMember]
        public List<SearchSegment> SearchSegments { get; set; }
        [DataMember]
        public SortingOrder SortingOrder { get; set; }
        [DataMember]
        public FlightTravelPreference FlightTravelPreference { get; set; }
        //[DataMember]
        //public Dictionary<string, string> AdditionalInfo { get; set; }
        [DataMember]
        public int? MaxPreferredResults { get; set; }

        #region Translation

        public static FlightSearchCriteria ToDataContract(Entities.Avail.FlightSearchCriteria flightSearchCriteria)
        {
            if (flightSearchCriteria == null)
                return null;

            var contract = new FlightSearchCriteria()
                               {
                                   //AdditionalInfo = AdditionalInfoDictionary.ToDictionary(flightSearchCriteria.AdditionalInfo),
                                   FlightTravelPreference = flightSearchCriteria.FlightTravelPreference == null
                                                                ? null
                                                                : FlightTravelPreference.ToDataContract(
                                                                    flightSearchCriteria.FlightTravelPreference),
                                   PassengerInfoSummary = flightSearchCriteria.PassengerInfoSummary == null
                                                              ? null
                                                              : flightSearchCriteria.PassengerInfoSummary.Select(
                                                                  PaxTypeQuantity.ToDataContract).ToListBuffered(2),
                                   SearchSegments = flightSearchCriteria.SearchSegments == null
                                                        ? null
                                                        : flightSearchCriteria.SearchSegments.Select(
                                                            SearchSegment.ToDataContract).ToListBuffered(2),
                                   SortingOrder = ToDataContract(flightSearchCriteria.SortingOrder),
                                   MaxPreferredResults = flightSearchCriteria.MaxPreferredResults
                               };

            return contract;
        }

        public static Entities.Avail.FlightSearchCriteria ToEntity(FlightSearchCriteria flightSearchCriteria)
        {
            var contract = new Entities.Avail.FlightSearchCriteria()
            {
                //AdditionalInfo = new AdditionalInfoDictionary(flightSearchCriteria.AdditionalInfo),
                FlightTravelPreference =
                    FlightTravelPreference.ToEntity(flightSearchCriteria.FlightTravelPreference),
                PassengerInfoSummary =
                    flightSearchCriteria.PassengerInfoSummary.Select(PaxTypeQuantity.ToEntity).ToListBuffered(2),
                SearchSegments = flightSearchCriteria.SearchSegments.Select(SearchSegment.ToEntity).ToListBuffered(2),
                SortingOrder = ToEntity(flightSearchCriteria.SortingOrder),
                MaxPreferredResults = flightSearchCriteria.MaxPreferredResults
            };

            return contract;
        }

        public static SortingOrder ToDataContract(Entities.Avail.SortingOrder sortingOrder)
        {
            switch (sortingOrder)
            {
                case Entities.Avail.SortingOrder.ByAirline:
                    return SortingOrder.ByAirLine;
                case Entities.Avail.SortingOrder.ByArrivalTime:
                    return SortingOrder.ByArrivalTime;
                case Entities.Avail.SortingOrder.ByDepartureTime:
                    return SortingOrder.ByDepartureTime;
                case Entities.Avail.SortingOrder.ByJourneyTime:
                    return SortingOrder.ByJourneyTime;
                case Entities.Avail.SortingOrder.ByPreference:
                    return SortingOrder.ByPreference;
                case Entities.Avail.SortingOrder.ByPriceHighToLow:
                    return SortingOrder.ByPriceHighToLow;
                case Entities.Avail.SortingOrder.ByPriceLowToHigh:
                    return SortingOrder.ByPriceLowToHigh;
            }

            return default(SortingOrder);
        }

        public static Entities.Avail.SortingOrder ToEntity(SortingOrder sortingOrder)
        {
            switch (sortingOrder)
            {
                case SortingOrder.ByAirLine:
                    return Entities.Avail.SortingOrder.ByAirline;
                case SortingOrder.ByArrivalTime:
                    return Entities.Avail.SortingOrder.ByArrivalTime;
                case SortingOrder.ByDepartureTime:
                    return Entities.Avail.SortingOrder.ByDepartureTime;
                case SortingOrder.ByJourneyTime:
                    return Entities.Avail.SortingOrder.ByJourneyTime;
                case SortingOrder.ByPreference:
                    return Entities.Avail.SortingOrder.ByPreference;
                case SortingOrder.ByPriceHighToLow:
                    return Entities.Avail.SortingOrder.ByPriceHighToLow;
                case SortingOrder.ByPriceLowToHigh:
                    return Entities.Avail.SortingOrder.ByPriceLowToHigh;
            }

            return default(Entities.Avail.SortingOrder);
        }

        #endregion
    }
}