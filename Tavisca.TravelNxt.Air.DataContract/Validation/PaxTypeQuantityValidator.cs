﻿using System.Linq;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class PaxTypeQuantityValidator : AbstractValidator<PaxTypeQuantity>
    {
        public PaxTypeQuantityValidator()
        {
            RuleFor(x => x.Ages)
                .Must(x => x == null || x.All(y => y > 0))
                .WithMessage(ValidationResources.PaxTypeQuantity_Ages_NotEmpty);

            RuleFor(x => x.Quantity)
                .GreaterThan(0)
                .WithMessage(ValidationResources.PaxTypeQuantity_Qnty);
        }
    }
}
