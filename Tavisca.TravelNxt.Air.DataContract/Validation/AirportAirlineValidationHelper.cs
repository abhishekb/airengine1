﻿using Tavisca.Singularity;
using Tavisca.TravelNxt.Flight.Contracts;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public static class AirportAirlineValidationHelper
    {
        private readonly static IContentManagerFactory ContentManagerFactory = RuntimeContext.Resolver.Resolve<IContentManagerFactory>();
        public static bool IsAirportCodeValid(string airportCode)
        {
            return !string.IsNullOrEmpty(airportCode) &&
                   ContentManagerFactory.GetContentManager().GetAirportByCode(airportCode) != null;
        }

        public static bool IsCityCodeValid(string cityCode)
        {
            if(string.IsNullOrEmpty(cityCode))
                return false;

            var cities = ContentManagerFactory.GetContentManager().GetCitiesByCode(cityCode);

            return cities != null && cities.Count > 0;
        }

        public static bool IsAirlineCodeValid(string airlineCode)
        {
            return !string.IsNullOrEmpty(airlineCode) &&
                   ContentManagerFactory.GetContentManager().GetAirlineByCode(airlineCode) != null;
        }
    }
}
