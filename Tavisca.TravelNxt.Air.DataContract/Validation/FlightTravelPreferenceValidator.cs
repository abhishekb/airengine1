﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class FlightTravelPreferenceValidator: AbstractValidator<FlightTravelPreference>
    {
        public FlightTravelPreferenceValidator()
        {
            RuleFor(x => x.AirlinesPreference)
                .SetValidator(new AirlinesPreferenceValidator());
        }
    }
}
