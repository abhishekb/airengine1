using System;
using System.Globalization;
using System.IO;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class TravelDateValidator : AbstractValidator<TravelDate>
    {
        private readonly static IContentManagerFactory ContentManagerFactory = RuntimeContext.Resolver.Resolve<IContentManagerFactory>();

        public TravelDateValidator()
        {
            RuleFor(x => x.MaxDate)
                .InclusiveBetween(0, 3)
                .WithMessage(ValidationResources.TravelDate_MaxDate);

            RuleFor(x => x.MinDate)
                .InclusiveBetween(0, 3)
                .WithMessage(ValidationResources.TravelDate_MinDate);

            RuleFor(x => x.TimeWindow)
                .GreaterThan(-1)
                .LessThanOrEqualTo(12)
                .WithMessage(ValidationResources.TravelDate_TimeWindow);

            Custom(DateValidation);
        }

        public ValidationFailure DateValidation(TravelDate instance)
        {
            if (instance == null)
                return null;

            if (string.IsNullOrWhiteSpace(instance.DepartureLocation))
                return null;

            var airport = instance.IsDepartureCity
                ? ContentManagerFactory.GetContentManager()
                    .GetAirportsForCity(instance.DepartureLocation).FirstOrDefault()
                : ContentManagerFactory.GetContentManager().GetAirportByCode(instance.DepartureLocation);

            if (airport == null)
                return null; //this should be validated elsewhere.

            var timeConverter = RuntimeContext.Resolver.Resolve<ITimeConverter>();

            var currentTime = timeConverter.GetLocalTimeAtLocation(airport.ToTimeZoneInfo(), DateTime.UtcNow);

            var minTravelDateOffsetHours = Settings.SettingManager.MinTravelDateOffsetHours;

            var hourDiff = (instance.DateTime - currentTime).TotalHours;

            if (hourDiff > 7920) //7920 hours = 330 days * 24 hours
            {
                return new ValidationFailure("DateTime", string.Format(ValidationResources.TravelDate_Date, minTravelDateOffsetHours.ToString(CultureInfo.InvariantCulture)));
            }

            if (instance.AnyTime == false)
            {
                if (hourDiff < minTravelDateOffsetHours) //7920 hours = 330 days * 24 hours
                {
                    return new ValidationFailure("DateTime", string.Format(ValidationResources.TravelDate_Date, minTravelDateOffsetHours.ToString(CultureInfo.InvariantCulture)));
                }
            }
            else
            {
                if (instance.DateTime.Date < currentTime.Date)
                {
                    return new ValidationFailure("Date", string.Format(ValidationResources.TravelDate_Invalid_Date, instance.DateTime.Date.ToString(CultureInfo.InvariantCulture)));
                }
                else
                {
                    if (hourDiff < minTravelDateOffsetHours)
                    {
                        instance.DateTime = currentTime.AddHours(minTravelDateOffsetHours);
                        instance.AnyTime = false;
                    }
                }
            }

            return null;
        }
    }
}