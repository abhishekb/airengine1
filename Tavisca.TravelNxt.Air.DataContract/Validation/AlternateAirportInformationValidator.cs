﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class AlternateAirportInformationValidator : AbstractValidator<AlternateAirportInformation>
    {
        public override ValidationResult Validate(ValidationContext<AlternateAirportInformation> context)
        {
            var result = base.Validate(context);

            AddMutuallyExclusiveValidation(context.InstanceToValidate, ref result);

            return result;
        }

        public override ValidationResult Validate(AlternateAirportInformation instance)
        {
            var result = base.Validate(instance);

            AddMutuallyExclusiveValidation(instance, ref result);

            return result;
        }

        private static void AddMutuallyExclusiveValidation(AlternateAirportInformation instance, ref ValidationResult result)
        {
            if (instance != null)
            {
                if (instance.AirportCodes != null && instance.AirportCodes.Count > 0)
                {
                    foreach (var airportCode in instance.AirportCodes)
                    {
                        if (airportCode == null)
                        {
                            result.Errors.Add(
                            new ValidationFailure("AirportCodes",
                                                  ValidationResources.AlternateAirportInformation_Arprt_With_NullValues)
                            );
                        }
                        else if (!AirportAirlineValidationHelper.IsAirportCodeValid(airportCode))
                        {
                            result.Errors.Add(
                            new ValidationFailure("AirportCodes",
                                                 string.Format(
                                                    ValidationResources.AlternateAirportInformation_Arprt_With_InvalidValues,
                                                    airportCode))
                            );
                        }
                    }

                    if (instance.IncludeNearByAirports.HasValue || instance.RadiusKm.HasValue)
                    {
                        result.Errors.Add(
                            new ValidationFailure("AirportCodes",
                                                  ValidationResources.AlternateAirportInformation_Arprt_With_Nearby_Info)
                            );
                    }
                }
            }
        }
    }
}
