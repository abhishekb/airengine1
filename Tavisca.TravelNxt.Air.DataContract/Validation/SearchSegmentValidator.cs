﻿using System;
using FluentValidation;
using FluentValidation.Results;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class SearchSegmentValidator : AbstractValidator<SearchSegment>
    {
        public SearchSegmentValidator()
        {
            RuleFor(x => x.ConnectionPreferences).SetCollectionValidator(new ConnectionPreferenceValidator());

            RuleFor(x => x.ArrivalAlternateAirportInformation).SetValidator(new AlternateAirportInformationValidator());
            RuleFor(x => x.DepartureAlternateAirportInformation).SetValidator(new AlternateAirportInformationValidator());
            RuleFor(x => x.TravelDate)
                .NotNull()
                .WithMessage(ValidationResources.SearchSegment_TravelDate_NotEmpty)
                .SetValidator(new TravelDateValidator());

            //Validation for arrival spec
            Custom(x =>
            {
                if (IsArrivalLocationMissing(x))
                    return new ValidationFailure("SearchSegment.ArrivalAirportCode/ArrivalCityCode",
                        ValidationResources.ArrivalLocationMissing);

                if (AreMultipleArrivalValuesPresent(x))
                    return new ValidationFailure("SearchSegment.ArrivalAirportCode/ArrivalCityCode",
                        ValidationResources.ArrivalAirportAndCitySpecified);

                ValidationFailure failure;
                return !IsArrivalLocationCodeValid(x, out failure) ? failure : null;
            });

            //Validation for departure spec
            Custom(x =>
            {
                if (IsDepartureLocationMissing(x))
                    return new ValidationFailure("SearchSegment.DepartureAirportCode/DepartureCityCode",
                        ValidationResources.DepartureLocationMissing);

                if (AreMultipleDepartureValuesPresent(x))
                    return new ValidationFailure("SearchSegment.DepartureAirportCode/DepartureCityCode",
                        ValidationResources.DepartureAirportAndCitySpecified);

                ValidationFailure failure;
                return !IsDepartureLocationCodeValid(x, out failure) ? failure : null;
            });

            //Validation for diferent arrival-departure spec
            Custom(x =>
            {
                var arrivalDepartSame = AreArrivalDepartureLocSame(x);
                if (arrivalDepartSame.HasValue && arrivalDepartSame.Value)
                    return new ValidationFailure("SearchSegment.ArriveDepartCode",
                        ValidationResources.
                            SearchSegment_ArrivalCode_SameAs_DepartureCode);

                return null;
            });
        }

        private static bool IsArrivalLocationMissing(SearchSegment searchSegment)
        {
            return string.IsNullOrWhiteSpace(searchSegment.ArrivalCityCode) &&
                   string.IsNullOrWhiteSpace(searchSegment.ArrivalAirportCode);
        }

        private static bool IsDepartureLocationMissing(SearchSegment searchSegment)
        {
            return string.IsNullOrWhiteSpace(searchSegment.DepartureCityCode) &&
                   string.IsNullOrWhiteSpace(searchSegment.DepartureAirportCode);
        }

        private static bool AreMultipleArrivalValuesPresent(SearchSegment searchSegment)
        {
            return !string.IsNullOrWhiteSpace(searchSegment.ArrivalCityCode) &&
                   !string.IsNullOrWhiteSpace(searchSegment.ArrivalAirportCode);
        }

        private static bool AreMultipleDepartureValuesPresent(SearchSegment searchSegment)
        {
            return !string.IsNullOrWhiteSpace(searchSegment.DepartureCityCode) &&
                   !string.IsNullOrWhiteSpace(searchSegment.DepartureAirportCode);
        }

        private static bool? AreArrivalDepartureLocSame(SearchSegment searchSegment)
        {
            //In case of multiple values for any, earlier validations will fail...so skip
            if (AreMultipleArrivalValuesPresent(searchSegment) || AreMultipleDepartureValuesPresent(searchSegment))
                return null;

            var arrivalLoc = GetApplicableArrivalCode(searchSegment);

            var departureLoc = GetApplicableDepartureCode(searchSegment);

            //This validation is for cases having one value each on either side
            if (string.IsNullOrWhiteSpace(arrivalLoc) || string.IsNullOrWhiteSpace(departureLoc))
                return null;

            return string.Equals(arrivalLoc, departureLoc, StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsArrivalLocationCodeValid(SearchSegment searchSegment, out ValidationFailure validationFailure)
        {
            validationFailure = null;

            var arrivalLoc = GetApplicableArrivalCode(searchSegment);

            var citySpecified = arrivalLoc.Equals(searchSegment.ArrivalCityCode,
                StringComparison.InvariantCultureIgnoreCase);

            var isCodeValid = citySpecified
                ? AirportAirlineValidationHelper.IsCityCodeValid(arrivalLoc)
                : AirportAirlineValidationHelper.IsAirportCodeValid(arrivalLoc);

            if (!isCodeValid)
                validationFailure =
                    new ValidationFailure(
                        citySpecified ? "SearchSegment.ArrivalCityCode" : "SearchSegment.ArrivalAirportCode",
                        citySpecified
                            ? string.Format(ValidationResources.SearchSegment_ArrivalCityCode_Invalid, arrivalLoc)
                            : string.Format(ValidationResources.SearchSegment_ArrivalArprtCode_Invalid, arrivalLoc));

            return isCodeValid;
        }

        private static bool IsDepartureLocationCodeValid(SearchSegment searchSegment, out ValidationFailure validationFailure)
        {
            validationFailure = null;

            var departureLoc = GetApplicableDepartureCode(searchSegment);

            var citySpecified = departureLoc.Equals(searchSegment.DepartureCityCode,
                StringComparison.InvariantCultureIgnoreCase);

            var isCodeValid = citySpecified
                ? AirportAirlineValidationHelper.IsCityCodeValid(departureLoc)
                : AirportAirlineValidationHelper.IsAirportCodeValid(departureLoc);

            if (!isCodeValid)
                validationFailure =
                    new ValidationFailure(
                        citySpecified ? "SearchSegment.DepartureCityCode" : "SearchSegment.DepartureAirportCode",
                        citySpecified
                            ? string.Format(ValidationResources.SearchSegment_DepartureCityCode_Invalid, departureLoc)
                            : string.Format(ValidationResources.SearchSegment_DepartureArprtCode_Invalid, departureLoc));

            return isCodeValid;
        }

        private static string GetApplicableArrivalCode(SearchSegment searchSegment)
        {
            return !string.IsNullOrWhiteSpace(searchSegment.ArrivalCityCode)
                ? searchSegment.ArrivalCityCode
                : searchSegment.ArrivalAirportCode;
        }

        private static string GetApplicableDepartureCode(SearchSegment searchSegment)
        {
            return !string.IsNullOrWhiteSpace(searchSegment.DepartureCityCode)
                ? searchSegment.DepartureCityCode
                : searchSegment.DepartureAirportCode;
        }
    }
}
