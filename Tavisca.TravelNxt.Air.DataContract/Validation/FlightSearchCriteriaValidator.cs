﻿using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class FlightSearchCriteriaValidator: AbstractValidator<FlightSearchCriteria>
    {
        public FlightSearchCriteriaValidator()
        {
            RuleFor(x => x.PassengerInfoSummary)
                .Must(x => x.Count > 0)
                .WithMessage(ValidationResources.FlightSearchCriteria_Psngr_NotEmpty).When(
                    request => request.PassengerInfoSummary != null)
                .Must(x => x != null).WithMessage(ValidationResources.FlightSearchCriteria_Psngr_NotNull)
                .SetCollectionValidator(new PaxTypeQuantityValidator());

            RuleFor(x => x.SearchSegments)
                .Must(x => x.Count > 0).When(
                    request => request.SearchSegments != null)
                .WithMessage(ValidationResources.FlightSearchCriteria_SearchSegs_NotEmpty)
                .Must(x => x != null)
                .WithMessage(ValidationResources.FlightSearchCriteria_SearchSegs_NotNull)
                .SetCollectionValidator(new SearchSegmentValidator());

            RuleFor(x => x.SearchSegments).Must(
                x => x.Zip(x.Skip(1), (a, b) => new { Date1 = a.TravelDate.DateTime, Date2 = b.TravelDate.DateTime })
                         .All(p => p.Date1 <= p.Date2))
                .WithMessage(ValidationResources.TravelDate_InvalidOrder).When(request => request.SearchSegments != null);

            RuleFor(x => x.FlightTravelPreference).SetValidator(new FlightTravelPreferenceValidator());

            RuleFor(x => x.MaxPreferredResults).InclusiveBetween(1, 5000)
                .WithMessage(ValidationResources.FlightSearchCriteria_MaxPreferredResults_Range);

            Custom(x =>
            {
                var passengerInfoSummary = x.PassengerInfoSummary;
                if (passengerInfoSummary != null)
                {
                    var adultPassengerCount =
                        x.PassengerInfoSummary.Where(
                            passenger =>
                            passenger.PassengerType == PassengerType.Adult ||
                            passenger.PassengerType == PassengerType.Senior ||
                            passenger.PassengerType == PassengerType.MilitaryAdult ||
                            passenger.PassengerType == PassengerType.MilitarySenior).Sum(y => y.Quantity);

                    var infantChildPassengerCount = x.PassengerInfoSummary.Where(
                        passenger =>
                        passenger.PassengerType == PassengerType.Infant ||
                        passenger.PassengerType == PassengerType.MilitaryInfant).Sum(y => y.Quantity);

                    if (infantChildPassengerCount > adultPassengerCount)
                        return new ValidationFailure("FlightSearchCriteria.PassengerInfoSummary",
                                                     ValidationResources.
                                                         FlightSearchCriteria_Psngr_InfantOrChildAcompanyInvalid);
                }
                return null;
            });
        }
    }
}
