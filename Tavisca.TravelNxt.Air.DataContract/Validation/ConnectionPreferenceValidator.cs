﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class ConnectionPreferenceValidator : AbstractValidator<ConnectionPreference>
    {
        public ConnectionPreferenceValidator()
        {
            RuleFor(x => x.AirportCode)
                .NotEmpty()
                .WithMessage(ValidationResources.ConnectionPreference_ArprtCode_NotEmpty)
                .Must(AirportAirlineValidationHelper.IsAirportCodeValid)
                .WithMessage(ValidationResources.ConnectionPreference_ArprtCode_InvalidValue, x => x.AirportCode);
        }
    }
}
