using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class AirlinesPreferenceValidator : AbstractValidator<AirlinesPreference>
    {
        public override ValidationResult Validate(AirlinesPreference instance)
        {
            var result = base.Validate(instance);

            AddAirlineValidation(instance, ref result);

            return result;
        }

        public override ValidationResult Validate(ValidationContext<AirlinesPreference> context)
        {
            var result = base.Validate(context);

            AddAirlineValidation(context.InstanceToValidate, ref result);

            return result;
        }

        private static void AddAirlineValidation(AirlinesPreference instance, ref ValidationResult result)
        {
            const string airlinesPreferencePropName = "AirlinesPreference";
            const string permittedAirlinesPropName = "PermittedAirlines";
            const string preferredAirlinesPropName = "PreferredAirlines";
            const string prohibitedAirlinesPropName = "ProhibitedAirlines";

            if (instance != null)
            {
                var permittedAirlines = instance.PermittedAirlines;
                var prohibitedAirlines = instance.ProhibitedAirlines;
                var preferredAirlines = instance.PreferredAirlines;

                ValidateAirlines(permittedAirlines, permittedAirlinesPropName, ref result);
                ValidateAirlines(preferredAirlines, preferredAirlinesPropName, ref result);
                ValidateAirlines(prohibitedAirlines, prohibitedAirlinesPropName, ref result);

                if (prohibitedAirlines != null && prohibitedAirlines.Count != 0)
                {
                    var havePermittedAirlines = permittedAirlines != null && permittedAirlines.Count != 0;
                    var havePreferredAirlines = preferredAirlines != null && preferredAirlines.Count != 0;

                    if (havePermittedAirlines || havePreferredAirlines)
                    {
                        foreach (var prohibitedAirline in prohibitedAirlines)
                        {
                            if (havePermittedAirlines && permittedAirlines.Any(x => x.Equals(prohibitedAirline, StringComparison.OrdinalIgnoreCase)))
                            {
                                result.Errors.Add(new ValidationFailure(airlinesPreferencePropName,
                                    string.Format(ValidationResources.AirlinePreference_ConflictingRequest,
                                    prohibitedAirline, prohibitedAirlinesPropName, permittedAirlines)));
                            }

                            if (havePreferredAirlines && preferredAirlines.Any(x => x.Equals(prohibitedAirline, StringComparison.OrdinalIgnoreCase)))
                            {
                                result.Errors.Add(new ValidationFailure(airlinesPreferencePropName,
                                    string.Format(ValidationResources.AirlinePreference_ConflictingRequest,
                                    prohibitedAirline, prohibitedAirlinesPropName, preferredAirlinesPropName)));
                            }
                        }
                    }
                }
            }
        }

        private static void ValidateAirlines(IEnumerable<string> airlineCodes, string propertyName, ref ValidationResult result)
        {
            if (airlineCodes != null)
            {
                foreach (var airlineCode in airlineCodes)
                {
                    if (!AirportAirlineValidationHelper.IsAirlineCodeValid(airlineCode))
                    {
                        result.Errors.Add(new ValidationFailure(propertyName,
                            string.Format(ValidationResources.AirlinePreference_UnsupportedAirlineCode,
                            airlineCode, propertyName)));
                    }
                }
            }
        }
    }
}