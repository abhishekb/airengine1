﻿using FluentValidation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract.Validation
{
    public class FlightSearchRQValidator : AbstractValidator<FlightSearchRQ>
    {
        public FlightSearchRQValidator()
        {
            RuleFor(x => x.SearchCriterion)
                .NotNull()
                .WithMessage(ValidationResources.FlightSearchRQ_SearchCriterion_NotNull)
                .SetValidator(new FlightSearchCriteriaValidator());
        }
    }
}
