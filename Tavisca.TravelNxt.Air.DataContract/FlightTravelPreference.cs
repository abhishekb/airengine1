﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(FlightTravelPreferenceValidator))]
    public class FlightTravelPreference
    {
        [DataMember]
        public bool IncludeNonStopOnly { get; set; }
        [DataMember]
        public bool IncludeDirectOnly { get; set; }
        [DataMember]
        public bool JetFlightsOnly { get; set; }
        [DataMember]
        public bool AllowMixedAirlines { get; set; }
        [DataMember]
        public bool UnRestrictedFare { get; set; }
        [DataMember]
        public bool NoPenalty { get; set; }
        [DataMember]
        public bool Refundable { get; set; }
        [DataMember]
        public AirlinesPreference AirlinesPreference { get; set; }

        #region Translation

        public static FlightTravelPreference ToDataContract(Entities.Avail.FlightTravelPreference flightTravelPreference)
        {
            var contract = new FlightTravelPreference()
                               {
                                   AllowMixedAirlines = flightTravelPreference.AllowMixedAirlines,
                                   JetFlightsOnly = flightTravelPreference.JetFlightsOnly,
                                   UnRestrictedFare = flightTravelPreference.UnRestrictedFare,
                                   AirlinesPreference = flightTravelPreference.AirlinesPreference == null ? null : AirlinesPreference.ToDataContract(flightTravelPreference.AirlinesPreference),
                                   Refundable = flightTravelPreference.Refundable != null && flightTravelPreference.Refundable.Value,
                                   IncludeDirectOnly = flightTravelPreference.IncludeDirectOnly,
                                   IncludeNonStopOnly = flightTravelPreference.IncludeNonStopOnly,
                                   NoPenalty = flightTravelPreference.NoPenalty
                               };

            return contract;
        }

        public static Entities.Avail.FlightTravelPreference ToEntity(FlightTravelPreference flightTravelPreference)
        {
            if (flightTravelPreference == null)
                return null;

            var entity = new Entities.Avail.FlightTravelPreference()
                             {
                                 AllowMixedAirlines = flightTravelPreference.AllowMixedAirlines,
                                 JetFlightsOnly = flightTravelPreference.JetFlightsOnly,
                                 UnRestrictedFare = flightTravelPreference.UnRestrictedFare,
                                 Refundable = flightTravelPreference.Refundable,
                                 AirlinesPreference = AirlinesPreference.ToEntity(flightTravelPreference.AirlinesPreference),
                                 IncludeDirectOnly = flightTravelPreference.IncludeDirectOnly,
                                 IncludeNonStopOnly = flightTravelPreference.IncludeNonStopOnly,
                                 NoPenalty = flightTravelPreference.NoPenalty
                             };

            return entity;
        }

        #endregion
    }
}
