﻿using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Requester
    {
        /// <summary>
        /// This is requester ID. Like Affiliate ID or Agency ID from a Hierarchy.
        /// </summary>
        public long Id
        {
            get
            {
                return Utility.GetCurrentContext().AccountId;
            }
        }

        public int PosId
        {
            get
            {
                return Utility.GetCurrentContext().PosId;
            }
        }

        /// <summary>
        /// Customer type for whom the search will be performed
        /// </summary>
        [DataMember]
        public CustomerType CustomerType { get; set; }

        #region Translation

        public static Requester ToDataContract(Entities.Requester requester)
        {
            var contract = new Requester()
                {
                    //AdditionalInfo = new Dictionary<string, string>(),
                   
                    // Don't have exact support in engines
                   //RequesterOrigin = requester.
                };

            return contract;
        }

        public static Entities.Requester ToEntity(Requester requester)
        {
            var entity = new Entities.Requester(requester.Id, requester.PosId,
                EnumParsers.ToEntity(requester.CustomerType));

            return entity;
        }

        #endregion
    }
}
