﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class PaxTypeBaggage
    {
        [DataMember]
        public string BaggageCode { get; set; }

        [DataMember]
        public PassengerType PassengerType { get; set; }

        public static PaxTypeBaggage ToDataContract(Entities.Baggage baggage)
        {
            return new PaxTypeBaggage()
                       {
                           BaggageCode = baggage.BaggageCode,
                           PassengerType = ToDataContract(baggage.PassengerType)
                       };
        }

        public static PassengerType ToDataContract(Entities.PassengerType passengerType)
        {
            switch (passengerType)
            {
                case Entities.PassengerType.Adult:
                    return PassengerType.Adult;
                case Entities.PassengerType.Child:
                    return PassengerType.Child;
                case Entities.PassengerType.Infant:
                    return PassengerType.Infant;
                case Entities.PassengerType.Senior:
                    return PassengerType.Senior;
                case Entities.PassengerType.MilitaryAdult:
                    return PassengerType.MilitaryAdult;
                case Entities.PassengerType.MilitaryChild:
                    return PassengerType.MilitaryChild;
                case Entities.PassengerType.MilitaryInfant:
                    return PassengerType.MilitaryInfant;
                case Entities.PassengerType.MilitarySenior:
                    return PassengerType.MilitarySenior;
            }

            return default(PassengerType);
        }
    }
}
