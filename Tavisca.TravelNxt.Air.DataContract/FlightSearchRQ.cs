﻿using System.Runtime.Serialization;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(FlightSearchRQValidator))]
    public class FlightSearchRQ 
    {
        [DataMember]
        public bool DoDeferredSearch { get; set; }
        [DataMember]
        public bool AllowSupplierStreaming { get; set; }
        [DataMember]
        public FlightSearchCriteria SearchCriterion { get; set; }
        [DataMember]
        public Requester Requester { get; set; }
    }
}
