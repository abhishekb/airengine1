﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirContract
    {
        [DataMember]
        public long ContractId { get; set; }

        [DataMember]
        public string ContractName { get; set; }

        [DataMember]
        public long AgencyId { get; set; }

        [DataMember]
        public string ContractType { get; set; }

        [DataMember]
        public string ContractRateCode { get; set; }

        public string ContractRateCodeType { get; set; }

        #region Transation

        public static AirContract ToDataContract(Entities.AirContract airContract)
        {
            if(airContract == null)
                return null;

            return new AirContract()
                       {
                           AgencyId = airContract.AgencyId,
                           ContractId = airContract.ContractId,
                           ContractName = airContract.ContractName,
                           ContractRateCode = airContract.ContractRateCode,
                           ContractRateCodeType = airContract.ContractRateCodeType,
                           ContractType = airContract.ContractType
                       };
        }

        #endregion
    }
}
