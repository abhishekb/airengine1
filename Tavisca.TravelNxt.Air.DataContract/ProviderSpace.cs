﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class ProviderSpace
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool? IsSeatmapAvailable { get; set; }


        #region Translation

        public static ProviderSpace ToDataContract(Entities.ProviderSpace providerSpace)
        {
            bool? available=null;
            string seatmapAvialble;

            if (providerSpace.AdditionalInfo.TryGetValue("SeatMapProvided", out seatmapAvialble))
            {
                bool x;
                if (Boolean.TryParse(seatmapAvialble, out x))
                    available = x;
            }

            var contract = new ProviderSpace()
                {
                    ID = providerSpace.ID,
                    Name = providerSpace.Name,
                    IsSeatmapAvailable = available
                };

            return contract;
        }

        //public static Entities.ProviderSpace ToEntity(ProviderSpace providerSpace, Requester requester)
        //{
        //    var entity = new Entities.ProviderSpace(providerSpace.ID, providerSpace.Name, Requester.ToEntity(requester), null, null, null);

        //    return entity;
        //}

        #endregion
    }
}
