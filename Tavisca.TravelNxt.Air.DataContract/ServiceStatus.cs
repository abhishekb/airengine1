﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class ServiceStatus
    {
        [DataMember]
        public string StatusCode { get; set; }

        [DataMember]
        public CallStatus Status { get; set; }

        [DataMember]
        public List<string> Messages { get; set; }

        [DataMember]
        public string StatusMessage { get; set; }
    }
}
