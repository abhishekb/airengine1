﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightPollRQ
    {
        [DataMember]
        public bool GetAsSupplierResponds { get; set; }

        [DataMember]
        public DateTime PreviousTimeStamp { get; set; }
    }
}
