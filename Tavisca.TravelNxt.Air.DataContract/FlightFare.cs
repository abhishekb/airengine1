﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightFare
    {
        [DataMember]
        public Money TotalMarkup { get; set; }
        [DataMember]
        public Money TotalFees { get; set; }
        [DataMember]
        public Money TotalCommission { get; set; }
        [DataMember]
        public Money TotalTaxes { get; set; }
        [DataMember]
        public Money TotalDiscount { get; set; }
        [DataMember]
        public Money BaseAmount { get; set; }
        [DataMember]
        public Money TotalFare { get; set; }
        [DataMember]
        public Money Savings { get; set; }
        [DataMember]
        public List<PassengerFare> PassengerFares { get; set; }
        [DataMember]
        public FareType FareType { get; set; }
        [DataMember]
        public List<FareAttribute> FareAttributes { get; set; }

        #region Translation

        public static FlightFare ToDataContract(Entities.FlightFare flightFare)
        {
            var contract = new FlightFare()
            {
                BaseAmount = Money.ToDataContract(flightFare.BaseAmount),
                FareType = ToDataContract(flightFare.FareType),
                PassengerFares =
                    flightFare.PassengerFares.Select(PassengerFare.ToDataContract).ToListBuffered(2),
                TotalFare = Money.ToDataContract(flightFare.TotalFare),
                TotalCommission =
                    Money.ToDataContract(flightFare.TotalCommission),
                TotalFees =
                    Money.ToDataContract(flightFare.TotalFees),
                TotalMarkup =
                    Money.ToDataContract(flightFare.TotalMarkup),
                TotalTaxes =
                    Money.ToDataContract(flightFare.TotalTaxes),
                TotalDiscount =
                    Money.ToDataContract(flightFare.TotalDiscount),
                Savings =
                    flightFare.Savings == null ? null : Money.ToDataContract(flightFare.Savings),
                FareAttributes =
                    flightFare.FareAttributes == null
                        ? null
                        : flightFare.FareAttributes.Select(ToDataContract).ToListBuffered()
            };

            return contract;
        }

        public static Entities.FlightFare ToEntity(FlightFare flightFare)
        {
            var entity = new Entities.FlightFare()
            {
                FareType = ToEntity(flightFare.FareType),
                PassengerFares = flightFare.PassengerFares.Select(PassengerFare.ToEntity).ToListBuffered(2),


            };

            return entity;
        }

        public static FareType ToDataContract(Entities.FareType fareType)
        {
            switch (fareType)
            {
                case Entities.FareType.Negotiated:
                    return FareType.Negotiated;
                case Entities.FareType.Published:
                    return FareType.Published;
                case Entities.FareType.Unknown:
                    return FareType.Unknown;
            }

            return default(FareType);
        }

        public static Entities.FareType ToEntity(FareType fareType)
        {
            switch (fareType)
            {
                case FareType.Negotiated:
                    return Entities.FareType.Negotiated;
                case FareType.Published:
                    return Entities.FareType.Published;
                case FareType.Unknown:
                    return Entities.FareType.Unknown;
            }

            return default(Entities.FareType);
        }

        public static FareAttribute ToDataContract(Entities.FareAttribute fareRestrictionType)
        {
            switch (fareRestrictionType)
            {
                case Entities.FareAttribute.WithPenalty:
                    return FareAttribute.WithPenalty;
                case Entities.FareAttribute.NoPenalty:
                    return FareAttribute.NoPenalty;
                case Entities.FareAttribute.NoMinimumStayRestriction:
                    return FareAttribute.NoMinimumStayRestriction;
                case Entities.FareAttribute.NoRestriction:
                    return FareAttribute.NoRestriction;
                case Entities.FareAttribute.ExcludeBookingClasses:
                    return FareAttribute.ExcludeBookingClasses;
                case Entities.FareAttribute.NonRefundableFares:
                    return FareAttribute.NonRefundableFares;
                case Entities.FareAttribute.NoMaximumStayRestriction:
                    return FareAttribute.NoMaximumStayRestriction;
                case Entities.FareAttribute.NoAdvancePurchase:
                    return FareAttribute.NoAdvancePurchase;
                case Entities.FareAttribute.IncludeBookingClasses:
                    return FareAttribute.IncludeBookingClasses;
                case Entities.FareAttribute.NoDayTimeRestriction:
                    return FareAttribute.NoDayTimeRestriction;
                case Entities.FareAttribute.RefundableFares:
                    return FareAttribute.RefundableFares;
                case Entities.FareAttribute.NoMinMaxFare:
                    return FareAttribute.NoMinMaxFare;
                case Entities.FareAttribute.EconomyUnrestricted:
                    return FareAttribute.EconomyUnrestricted;
                case Entities.FareAttribute.CouponFare:
                    return FareAttribute.CouponFare;
                case Entities.FareAttribute.FrozenFares:
                    return FareAttribute.FrozenFares;
                default:
                    return FareAttribute.Unknown;
            }
        }

        #endregion
    }
}
