﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum ConnectionPreferenceType
    {
        [EnumMember]
        Via,
        [EnumMember]
        Avoid
    }
}