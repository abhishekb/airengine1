﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightLeg
    {
        [DataMember]
        public int RefID { get; set; }
        [DataMember]
        public LegType LegType { get; set; }
        [DataMember]
        public List<int> FlightSegmentRefs { get; set; }
        [DataMember]
        public int NumberOfStops { get; set; }
        //[DataMember]
        //public Dictionary<string, string> AdditionalInfo { get; set; }

        // <summary>
        // this is kept on leg level for full schedule impl.
        // </summary>
        [DataMember]
        public int ProviderSpaceRefId { get; set; }
    }
}
