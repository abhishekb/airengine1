﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    [Validator(typeof(AirlinesPreferenceValidator))]
    public class AirlinesPreference
    {
        [DataMember]
        public List<string> PreferredAirlines { get; set; }

        [DataMember]
        public List<string> ProhibitedAirlines { get; set; }

        [DataMember]
        public List<string> PermittedAirlines { get; set; }

        #region translation

        public static Entities.Avail.AirlinesPreference ToEntity(AirlinesPreference airlinesPreference)
        {
            if(airlinesPreference == null)
                return null;

            return new Entities.Avail.AirlinesPreference()
                       {
                           PermittedAirlines =
                               airlinesPreference.PermittedAirlines == null
                                   ? null
                                   : airlinesPreference.PermittedAirlines.Select(x => x).ToListBuffered(),
                           ProhibitedAirlines =
                               airlinesPreference.ProhibitedAirlines == null
                                   ? null
                                   : airlinesPreference.ProhibitedAirlines.Select(x => x).ToListBuffered(),
                           PreferredAirlines =
                               airlinesPreference.PreferredAirlines == null
                                   ? null
                                   : airlinesPreference.PreferredAirlines.Select(x => x).ToListBuffered()
                       };
        }

        public static AirlinesPreference ToDataContract(Entities.Avail.AirlinesPreference airlinesPreference)
        {
            if (airlinesPreference == null)
                return null;

            return new AirlinesPreference()
                       {
                           PermittedAirlines =
                               airlinesPreference.PermittedAirlines == null
                                   ? null
                                   : airlinesPreference.PermittedAirlines.Select(x => x).ToListBuffered(),
                           ProhibitedAirlines =
                               airlinesPreference.ProhibitedAirlines == null
                                   ? null
                                   : airlinesPreference.ProhibitedAirlines.Select(x => x).ToListBuffered(),
                           PreferredAirlines =
                               airlinesPreference.PreferredAirlines == null
                                   ? null
                                   : airlinesPreference.PreferredAirlines.Select(x => x).ToListBuffered()
                       };
        }

        #endregion
    }
}
