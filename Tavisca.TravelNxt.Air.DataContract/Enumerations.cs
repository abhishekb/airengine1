﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum FareType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Published,
        [EnumMember]
        Negotiated
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum CustomerType
    {
        [EnumMember]
        Agent,
        [EnumMember]
        Admin,
        [EnumMember]
        Traveller
    }
   
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum CabinType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        First,
        [EnumMember]
        Business,
        [EnumMember]
        PremiumEconomy,
        [EnumMember]
        Economy

    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum FareAttribute
    {
        [EnumMember]
        EconomyUnrestricted,
        [EnumMember]
        NoAdvancePurchase,
        [EnumMember]
        NoDayTimeRestriction,
        [EnumMember]
        NoMaximumStayRestriction,
        [EnumMember]
        NoMinimumStayRestriction,
        [EnumMember]
        NoPenalty,
        [EnumMember]
        WithPenalty,
        [EnumMember]
        NoRestriction,
        [EnumMember]
        NoMinMaxFare,
        [EnumMember]
        NonRefundableFares,
        [EnumMember]
        RefundableFares,
        [EnumMember]
        IncludeBookingClasses,
        [EnumMember]
        ExcludeBookingClasses,
        [EnumMember]
        Unknown,
        [EnumMember]
        CouponFare,
        [EnumMember]
        FrozenFares
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum FareComponentType
    {
        [EnumMember]
        Tax,
        [EnumMember]
        Fees,
        [EnumMember]
        Markup,
        [EnumMember]
        Discount,
        [EnumMember]
        Commision
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum PassengerType
    {
        [EnumMember]
        Infant,
        [EnumMember]
        Child,
        [EnumMember]
        Adult,
        [EnumMember]
        Senior,
        [EnumMember]
        Mapped,
        [EnumMember]
        MilitaryAdult,
        [EnumMember]
        MilitaryChild,
        [EnumMember]
        MilitaryInfant,
        [EnumMember]
        MilitarySenior
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum ItineraryTypeOptions
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        MultiCity = 1,
        [EnumMember]
        OneWay = 2,
        [EnumMember]
        RoundTrip = 3

    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum LegType
    {
        [EnumMember]
        NotApplicable = 0,
        [EnumMember]
        Onward = 1,
        [EnumMember]
        Return = 2
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum CallStatus
    {
        [EnumMember]
        Success = 0,
        [EnumMember]
        Failure = 1,
        [EnumMember]
        Warning = 2,
        [EnumMember]
        InProgress = 3
    }

    public class EnumParsers
    {
        public static ItineraryTypeOptions ToDataContract(Entities.ItineraryTypeOptions options)
        {
            switch (options)
            {
                case Entities.ItineraryTypeOptions.NotSet:
                    return ItineraryTypeOptions.NotSet;

                case Entities.ItineraryTypeOptions.MultiCity:
                    return ItineraryTypeOptions.MultiCity;

                case Entities.ItineraryTypeOptions.OneWay:
                    return ItineraryTypeOptions.OneWay;

                case Entities.ItineraryTypeOptions.RoundTrip:
                    return ItineraryTypeOptions.RoundTrip;
                 default:
                    return ItineraryTypeOptions.NotSet;
            }
        }

        public static LegType ToDataContract(Entities.LegType legType)
        {
            switch (legType)
            {
                case Entities.LegType.NotApplicable:
                    return LegType.NotApplicable;

                case Entities.LegType.Onward:
                    return LegType.Onward;

                case Entities.LegType.Return:
                    return LegType.Return;

                default:
                    return LegType.NotApplicable;
            }
        }


        public static CallStatus ToDataContract(Entities.CallStatus callStatus)
        {
            switch (callStatus)
            {
                case Entities.CallStatus.Failure:
                    return CallStatus.Failure;

                case Entities.CallStatus.InProgress:
                    return CallStatus.InProgress;

                case Entities.CallStatus.Success:
                    return CallStatus.Success;

                case Entities.CallStatus.Warning:
                    return CallStatus.Warning;

                default:
                    return CallStatus.Failure;
            }
        }

        public static Entities.CustomerType ToEntity(CustomerType customerType)
        {
            switch (customerType)
            {
                case CustomerType.Admin:
                    return Entities.CustomerType.Admin;
                case CustomerType.Agent:
                    return Entities.CustomerType.Agent;
                case CustomerType.Traveller:
                    return Entities.CustomerType.Traveller;
            }

            throw new ArgumentException("Unrecognized customer type");
        }
    }

}
