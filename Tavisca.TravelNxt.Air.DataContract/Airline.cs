﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Avail.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Airline
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string WebsiteURL { get; set; }

        #region Translation

        public static string ToDataContract(Entities.Airline airline)
        {
            if (airline == null)
            {
                return null;
            }
            
            return airline.Code;
        }

        public static Airline ToDataContractObject(Entities.Airline airline)
        {
            if (airline == null)
            {
                return null;
            }
            else
                return new Airline()
                {
                    Code = airline.Code,
                    FullName = airline.FullName,
                    WebsiteURL = airline.WebsiteURL
                };
        }

        public static Entities.Airline ToEntity(Airline airline)
        {
            var entity = new Entities.Airline(airline.Code);

            return entity;
        }

        #endregion
    }
}
