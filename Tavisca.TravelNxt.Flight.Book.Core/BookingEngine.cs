﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Repositories;
using Tavisca.Frameworks.Session;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.Frameworks.Parallel;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.AirBook.Models;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Entities.Booking;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using Tavisca.TravelNxt.Flight.Settings;
using FlightRecommendation = Tavisca.TravelNxt.Flight.Entities.FlightRecommendation;

namespace Tavisca.TravelNxt.Flight.Book.Core
{
    public class BookingEngine : IBookingEngine
    {
        #region IBookingEngine Members

        public virtual FlightSaveRS Save(FlightSaveRQ request)
        {
            var result = new FlightSaveRS();
            FlightRecommendation recommendation = null;
            try
            {
                recommendation = GetFromSession(request.FlightRecommendationInfo.FlightRecommendationRefId);

                if (recommendation != null)
                {
                    var locator = SaveRecommendationInfo(recommendation, request);

                    result.Status = CallStatus.Success;
                    result.RecordLocator = locator;
                }
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                //result.Code = 
                result.Status = CallStatus.Failure;
                result.Message = "Some error occured while saving.";
            }

            if (recommendation == null && result.Status != CallStatus.Failure)
            {
                result.Status = CallStatus.Failure;
                result.Message = "The recommendation was not found.";
            }

            return result;
        }

        public virtual FlightRetrieveRS Retrieve(FlightRetrieveRQ request)
        {
            var result = new FlightRetrieveRS();
            try
            {
                var product = GetFromStore(request.RecordLocator);

                if (product == null)
                {
                    result.Status = CallStatus.Failure;
                    result.Message = "Record not found!";
                }
                else
                {
                    result.Product = product;
                    result.Status = CallStatus.Success;
                }
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                //result.Code = 
                result.Status = CallStatus.Failure;
                result.Message = "Some error occured while retrieving.";
            }

            return result;
        }

        #endregion

        #region Protected Members

        protected FlightRecommendation GetFromSession(int refNumber)
        {
            var result = GetSessionStore()
                .Get<FlightPriceResult>(KeyStore.APISessionKeys.FlightPriceCategory + refNumber, GetSessionId());

            if (result != null)
            {
                var recommendation = result.Recommendation;
                return recommendation;
            }

            return null;
        }

        protected string GetSessionId()
        {
            var context = Utility.GetCurrentContext();

            return context.SessionId.ToString();
        }

        protected virtual Guid SaveRecommendationInfo(FlightRecommendation recommendation, FlightSaveRQ request)
        {
            var locator = Guid.NewGuid();

            var model = new Data.AirBook.Models.FlightRecommendation
                {
                    AccountId = request.Requester.ID,
                    //ContractId = recommendation.ProviderSpace.Contract == null ? null : (long?)recommendation.ProviderSpace.Contract.ContractId,
                    ItineraryType = (int)recommendation.ItineraryType,
                    ProviderSpaceId = recommendation.ProviderSpace.ID,
                    RecordLocator = locator,
                    AdditionalInfo = recommendation.AdditionalInfo == null ? null : AdditionalInfoDictionary.ToDictionary(recommendation.AdditionalInfo).ToReconvertibleString(),
                    FlightFare = new Data.AirBook.Models.FlightFare()
                        {
                            FareType = (int)recommendation.Fare.FareType
                        }
                };

            foreach (var passengerFare in recommendation.Fare.PassengerFares)
            {
                var pFare = new Data.AirBook.Models.PassengerFare()
                    {
                        Quantity = passengerFare.Quantity,
                        PassengerType = (int)passengerFare.PassengerType,
                        BaseAmount = ToDataMoney(passengerFare.BaseAmount),
                    };

                passengerFare.Commissions.ForEach(x =>
                        pFare.FareComponents.Add(ToDataFareComponent(x))
                    );

                passengerFare.Discounts.ForEach(x =>
                        pFare.FareComponents.Add(ToDataFareComponent(x))
                    );

                passengerFare.Fees.ForEach(x =>
                        pFare.FareComponents.Add(ToDataFareComponent(x))
                    );

                passengerFare.Markups.ForEach(x =>
                        pFare.FareComponents.Add(ToDataFareComponent(x))
                    );

                passengerFare.Taxes.ForEach(x =>
                        pFare.FareComponents.Add(ToDataFareComponent(x))
                    );

                model.FlightFare.PassengerFares.Add(pFare);
            }

            foreach (var leg in recommendation.Legs)
            {
                var mLeg = new Data.AirBook.Models.FlightLeg()
                    {
                        AdditionalInfo = AdditionalInfoDictionary.ToDictionary(leg.AdditionalInfo).ToReconvertibleString(),
                        LegIndex = leg.LegIndex
                    };

                foreach (var segment in leg.Segments)
                {
                    var mSegment = new Data.AirBook.Models.FlightSegment()
                        {
                            AdditionalInfo = AdditionalInfoDictionary.ToDictionary(segment.AdditionalInfo).ToReconvertibleString(),
                            AircraftType = segment.AircraftType,
                            ArrivalAirportCode = segment.ArrivalAirport.Code,
                            ArrivalDateTime = segment.ArrivalDateTime,
                            DepartureDateTime = segment.DepartureDateTime,
                            DurationMinutes = segment.DurationMinutes,
                            FlightNumber = segment.FlightNumber,
                            DistanceTraveledKms = segment.DistanceTraveledKms,
                            DepartureTerminal = segment.DepartureTerminal,
                            DepartureAirportCode = segment.DepartureAirport.Code,
                            CodeShareText = segment.CodeShareText,
                            CanETicket = segment.CanETicket,
                            ArrivalTerminal = segment.ArrivalTerminal,
                            MarketingAirlineCode = segment.MarketingAirline.Code,
                            //NumberOfStops = segment.NumberOfStops, //TODO: change to segment stop details.
                            OperatingAirlineCode = segment.OperatingAirline == null ? null : segment.OperatingAirline.Code,
                            SegmentIndex = segment.SegmentIndex
                        };

                    mLeg.FlightSegments.Add(mSegment);
                }

                model.FlightLegs.Add(mLeg);
            }

            //foreach (var paxFareQualifier in recommendation.PaxFareQualifiers)
            //{
            //    var qualifier = new Data.AirBook.Models.PaxFareQualifier()
            //        {
            //            PassengerType = (int)paxFareQualifier.PassengerType,
            //            FareBasisCode = paxFareQualifier.FareBasisCode,
            //            CabinClass = (int)paxFareQualifier.CabinClass,
            //            ClassOfService = paxFareQualifier.ClassOfService,
            //            LegIndex = paxFareQualifier.LegIndex,
            //            SegmentRef = paxFareQualifier.SegmentRef,
            //        };

            //    model.PaxFareQualifiers.Add(qualifier);
            //}
            
            using (var repository = GetRepository(KeyStore.Repositories.AirBook))
            {
                repository.Create(model);

                repository.SaveChanges();
            }

            return locator;
        }

        protected FlightProduct GetFromStore(Guid recordLocator)
        {
            Data.AirBook.Models.FlightRecommendation model;
            using (var repository = GetRepository(KeyStore.Repositories.AirBookReadOnly))
            {
                model = repository.Find<Data.AirBook.Models.FlightRecommendation>
                    (x => x.RecordLocator == recordLocator);
            }

            if (model == null)
                return null;

            var product = new FlightProduct();

            var recommendation = new FlightRecommendation()
                {
                    AdditionalInfo = new AdditionalInfoDictionary(model.AdditionalInfo == null ? null : model.AdditionalInfo.ToDictionary()),
                    ItineraryType = (ItineraryTypeOptions)model.ItineraryType,
                    Legs = new FlightLegCollection()
                };

            if (model.FlightFare != null)
            {
                recommendation.Fare = new Entities.FlightFare()
                    {
                        FareType = (FareType)model.FlightFare.FareType,
                        PassengerFares = model.FlightFare.PassengerFares.Select(x =>
                            {
                                var fare = new Entities.PassengerFare()
                                    {
                                        PassengerType = (PassengerType)x.PassengerType,
                                        Quantity = x.Quantity,
                                        BaseAmount = ToEntityMoney(x.BaseAmount)
                                    };

                                var components = x.FareComponents.ToLookup(y => (FareComponentType)y.Type,
                                                                           z => new Entities.FareComponent()
                                                                               {
                                                                                   Code = z.Code,
                                                                                   Description = z.Description,
                                                                                   FareComponentRuleId = z.FareComponentRuleId,
                                                                                   //OwnerId = z.OwnerId,
                                                                                   FareComponentType = (FareComponentType)z.Type,
                                                                                   Value = ToEntityMoney(z.Value)
                                                                               });

                                fare.Commissions = components[FareComponentType.Commision].ToListBuffered();

                                fare.Discounts = components[FareComponentType.Discount].ToListBuffered();

                                fare.Fees = components[FareComponentType.Fees].ToListBuffered();

                                fare.Markups = components[FareComponentType.Markup].ToListBuffered();

                                fare.Taxes = components[FareComponentType.Tax].ToListBuffered();

                                return fare;
                            }).ToListBuffered()
                    };
            }

            foreach (var flightLeg in model.FlightLegs)
            {
                var leg = new Entities.FlightLeg()
                    {
                        AdditionalInfo = new AdditionalInfoDictionary(flightLeg.AdditionalInfo == null
                                             ? null
                                             : flightLeg.AdditionalInfo.ToDictionary()),
                        LegIndex = flightLeg.LegIndex,
                        Segments = new FlightSegmentCollection()
                    };

                leg.Segments.AddRange(
                    flightLeg.FlightSegments.Select(x =>
                        new Entities.FlightSegment(x.DepartureAirportCode, x.ArrivalAirportCode,
                            x.DepartureDateTime, x.ArrivalDateTime, x.FlightNumber,
                            x.MarketingAirlineCode, x.SegmentIndex)
                                {
                                    AdditionalInfo = new AdditionalInfoDictionary(x.AdditionalInfo == null ? null : x.AdditionalInfo.ToDictionary()),
                                    OperatingAirline = ToAirline(x.OperatingAirlineCode),
                                    //NumberOfStops = x.NumberOfStops, //TODO: enter stop details
                                    DurationMinutes = x.DurationMinutes,
                                    DistanceTraveledKms = x.DistanceTraveledKms,
                                    DepartureTerminal = x.DepartureTerminal,
                                    ArrivalTerminal = x.ArrivalTerminal,
                                    CodeShareText = x.CodeShareText,
                                    CanETicket = x.CanETicket,
                                    AircraftType = x.AircraftType
                                }
                        )
                );
                recommendation.Legs.Add(leg);
            }

            //var requester = new Requester(model.AccountId, Utility.GetCurrentContext().PosId);

            var providerSpaceManager = RuntimeContext.Resolver.Resolve<IProviderSpaceManager>();

            recommendation.ProviderSpace = providerSpaceManager.Get(null, model.ProviderSpaceId);
            
            product.Recommendation = recommendation;

            return product;
        }

        private static Airline ToAirline(string airlineCode)
        {
            if (string.IsNullOrEmpty(airlineCode))
                return null;

            return new Airline(airlineCode);
        }

        private static Entities.Money ToEntityMoney(Data.AirBook.Models.Money money)
        {
            return new Entities.Money(money.NativeAmount, money.NativeCurrency, money.BaseAmount,
                                      money.BaseCurrency, money.DisplayAmount,
                                      money.DisplayCurrency);
        }

        private static SessionStore GetSessionStore()
        {
            return Utility.GetSessionProvider();
        }

        private static Data.AirBook.Models.Money ToDataMoney(Entities.Money money)
        {
            return new Data.AirBook.Models.Money()
            {
                BaseAmount = money.BaseAmount,
                BaseCurrency = money.BaseCurrency,
                DisplayAmount = money.DisplayAmount,
                DisplayCurrency = money.DisplayCurrency,
                NativeAmount = money.NativeAmount,
                NativeCurrency = money.NativeCurrency
            };
        }

        private static Data.AirBook.Models.FareComponent ToDataFareComponent(Entities.FareComponent fareComponent)
        {
            return new Data.AirBook.Models.FareComponent()
            {
                Code = fareComponent.Code,
                //OwnerId = fareComponent.OwnerId,
                Description = fareComponent.Description,
                FareComponentRuleId = fareComponent.FareComponentRuleId,
                Type = (int)fareComponent.FareComponentType,
                Value = ToDataMoney(fareComponent.Value),
            };
        }

        protected IRepository GetRepository(string name)
        {
            return GetRepositoryFactory().GetRepository(name);
        }

        protected IRepositoryFactory GetRepositoryFactory()
        {
            return RuntimeContext.Resolver.Resolve<IRepositoryFactory>();
        }

        #endregion
    }
}
