﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Tavisca.TravelNxt.Flight.FareRules.DataContract;

namespace Tavisca.TravelNxt.Flight.FareRules.ServiceContract
{
    [ServiceContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public interface IAirFareRules
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "getFareRules", BodyStyle = WebMessageBodyStyle.Bare)]
        FareRulesRS GetFareRules(FareRulesRQ request);
    }
}
