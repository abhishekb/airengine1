﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class TravelDates
    {
        public TravelDaySpan AdvancePurchase { get; set; }
        public TravelDateSpan TravelDateSpan { get; set; }
        public TravelDateSpan BookingDateSpan { get; set; }
        public TravelDaySpan TravelDuration { get; set; }
    }
}