﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Common.FareComponent;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    internal static class InclusiveExclusiveHandler
    {
        public static void Process(List<FareComponentRule> fareComponentRules, Func<FareComponentRule, bool> processor)
        {
            var groups = fareComponentRules.GroupBy(x =>
            {
                if (x.FareComponentType == FareComponentTypeOptions.Markup ||
                    x.FareComponentType ==
                    FareComponentTypeOptions.DynamicMarkup)
                    return FareComponentTypeOptions.Markup;
                return x.FareComponentType;
            });
            foreach (var group in groups)
                ProcessForComponentType(group.ToList(), processor);
        }

        private static void ProcessForComponentType(List<FareComponentRule> fareComponentRules, Func<FareComponentRule, bool> processor)
        {
            var exclusiveMapPerGroup = GetExclusiveStatusMapPerGroup(fareComponentRules);

            foreach (var fareComponentRule in fareComponentRules.OrderBy(x => x.Preference))
            {
                if (fareComponentRule.IsExclusive && exclusiveMapPerGroup[fareComponentRule.FareComponentGroup])
                    continue;

                var componentApplied = processor(fareComponentRule);

                if (fareComponentRule.IsExclusive && componentApplied)
                    exclusiveMapPerGroup[fareComponentRule.FareComponentGroup] = true;
            }
        }

        private static IDictionary<string, bool> GetExclusiveStatusMapPerGroup(IEnumerable<FareComponentRule> fareComponentRules)
        {
            return fareComponentRules.Select(x => x.FareComponentGroup).Distinct().ToDictionary(x => x, x => false);
        }
    }
}
