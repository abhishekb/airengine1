﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    public class FlightFareDetails
    {
        public FlightFare Fare { get; set; }

        public ProviderSpace Provider { get; set; }

        public ItineraryTypeOptions ItineraryType { get; set; }

        public List<CabinType> CabinTypes { get; set; }

        public string OriginCountry { get; set; }

        public string OriginAirportCode { get; set; }

        public string DestinationCountry { get; set; }

        public string DestinationAirportCode { get; set; }

        public List<string> MarketingCarriers { get; set; }

        public List<string> ClassOfServices { get; set; }

        public IList<string> FareBasisCodes { get; set; }

        public DateTime DepartureTime { get; set; }

        public DateTime ArrivalTime { get; set; }

        public int LegCount { get; set; }

        public int SegmentCount { get; set; }

        public bool JumpNegoRate { get; set; }
    }
}
