﻿using System;
using System.Xml.Linq;
using ProtoBuf;
using Tavisca.Frameworks.Logging;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class AirFareRule : Rule
    {
        public TravelType TravelType { get; set; }
        public FlightItinerary Itinerary { get; set; }
        public TravelDates TravelDates { get; set; }
        public AirFareRange AirFareRange { get; set; }

        internal static AirFareRule FromXml(XElement ruleXml)
        {
            try
            {
                var rule = new AirFareRule();
                var itineraryXml = ruleXml.Element("Itinerary") ?? new XElement("Itinerary");

                rule.Itinerary = new FlightItinerary()
                    {
                        ClassesOfService = itineraryXml.GetElementValue("ClassesOfService", string.Empty, false),
                        DestinationAirports = itineraryXml.GetElementValue("DestinationAirports", string.Empty, false),
                        DestinationCountries = itineraryXml.GetElementValue("DestinationCountries", string.Empty, false),
                        FareBasisCodes = itineraryXml.GetElementValue("FareBasisCodes", string.Empty, false),
                        FareType = itineraryXml.GetElementValue("FareType", null, false).ToEnum<FareType>(),
                        MarketingCarriers = itineraryXml.GetElementValue("MarketingCarriers", string.Empty, false),
                        OriginAirports = itineraryXml.GetElementValue("OriginAirports", string.Empty, false),
                        OriginCountries = itineraryXml.GetElementValue("OriginCountries", string.Empty, false)
                    };

                var travelDatesXml = ruleXml.Element("TravelDates") ?? new XElement("TravelDates");
                rule.TravelDates = new TravelDates();

                var advancePurchaseXml = travelDatesXml.Element("AdvancePurchase") ?? new XElement("AdvancePurchase");
                rule.TravelDates.AdvancePurchase = new TravelDaySpan
                    {
                        MaximumDays = advancePurchaseXml.GetElementValue("MaximumDays", null, false).ToNullableInt(),
                        MinimumDays = advancePurchaseXml.GetElementValue("MinimumDays", null, false).ToNullableInt()
                    };

                var bookingDateSpanXml = travelDatesXml.Element("BookingDateSpan") ?? new XElement("BookingDateSpan");
                rule.TravelDates.BookingDateSpan = new TravelDateSpan
                    {
                        From = bookingDateSpanXml.GetElementValue("From", null, false).ToNullableDateTime(),
                        To = bookingDateSpanXml.GetElementValue("To", null, false).ToNullableDateTime()
                    };

                var travelDateSpanXml = travelDatesXml.Element("TravelDateSpan") ?? new XElement("TravelDateSpan");
                rule.TravelDates.TravelDateSpan = new TravelDateSpan
                    {
                        From = travelDateSpanXml.GetElementValue("From", null, false).ToNullableDateTime(),
                        To = travelDateSpanXml.GetElementValue("To", null, false).ToNullableDateTime()
                    };

                var travelDurationXml = travelDatesXml.Element("TravelDuration") ?? new XElement("TravelDuration");
                rule.TravelDates.TravelDuration = new TravelDaySpan
                    {
                        MaximumDays = travelDurationXml.GetElementValue("MaximumDays", null, false).ToNullableInt(),
                        MinimumDays = travelDurationXml.GetElementValue("MinimumDays", null, false).ToNullableInt()
                    };

                var travelTypeXml = ruleXml.Element("TravelType") ?? new XElement("TravelType");
                rule.TravelType = new TravelType
                    {
                        CabinType =
                            travelTypeXml.GetElementValue("CabinType", CabinType.Unknown.ToString(), false)
                                         .ToEnum<CabinType>(),
                        ItineraryType =
                            travelTypeXml.GetElementValue("ItineraryType", ItineraryTypeOptions.NotSet.ToString(), false)
                                         .ToEnum<ItineraryTypeOptions>(),
                    };

                var supplierFareRangeXml = ruleXml.Element("AirFareRange");
                if (supplierFareRangeXml != null)
                    rule.AirFareRange = DeserializeAirFareRange(supplierFareRangeXml);

                rule.RateCode = ruleXml.GetElementValue("RateCode", string.Empty, false);
                rule.CustomerType = ruleXml.GetElementValue("CustomerType", string.Empty, false);

                return rule;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);

                throw;
            }
        }

        private static AirFareRange DeserializeAirFareRange(XElement supplierFareRangeXml)
        {
            return new AirFareRange()
            {
                MinValue =
                    supplierFareRangeXml.GetElementValue("MinValue", null, false) == null
                        ? (decimal?)null
                        : Convert.ToDecimal(supplierFareRangeXml.GetElementValue("MinValue", null, false)),
                MaxValue =
                    supplierFareRangeXml.GetElementValue("MaxValue", null, false) == null
                        ? (decimal?)null
                        : Convert.ToDecimal(supplierFareRangeXml.GetElementValue("MaxValue", null, false)),
                Currency = supplierFareRangeXml.GetElementValue("Currency", null, false),
                FareRangeFareType =
                    supplierFareRangeXml.GetElementValue("FareRangeFareType",
                                                         FareRangeFareType.PerPersonAverageFare.ToString(),
                                                         false).ToEnum<FareRangeFareType>()
            };
        }
    }
}