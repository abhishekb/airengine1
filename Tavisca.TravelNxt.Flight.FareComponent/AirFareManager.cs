﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Tavisca.Frameworks.Caching;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    public class AirFareManager : FareManager<FlightFareDetails, ICollection<ProviderSpace>>
    {
        #region private fields

        private readonly Dictionary<string, Dictionary<FareComponentTypeOptions, List<FareComponentRule>>>
            _providerAndUplineBasedComponents =
                new Dictionary<string, Dictionary<FareComponentTypeOptions, List<FareComponentRule>>>();

        private static readonly List<FareComponentTypeOptions> MarkupAndFeesType = new List<FareComponentTypeOptions>()
                                                                                       {
                                                                                           FareComponentTypeOptions.
                                                                                               Markup,
                                                                                           FareComponentTypeOptions.
                                                                                               DynamicMarkup,
                                                                                           FareComponentTypeOptions.Fee
                                                                                       };

        private static readonly IDictionary<string, AccountTypeOptions> AccountTypeMapping =
            new ConcurrentDictionary<string, AccountTypeOptions>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region constructors

        static AirFareManager()
        {
            Enum.GetNames(typeof(AccountTypeOptions)).ForEach(
                name => AccountTypeMapping[name] = (AccountTypeOptions)Enum.Parse(typeof(AccountTypeOptions), name));
        }

        #endregion

        #region Initialization/Precaching

        public override void Initialize(ICollection<ProviderSpace> data)
        {
            if (_initialized)
                throw new InvalidOperationException("Initialization already done");

            var currentContext = Utility.GetCurrentContext();

            var posInfo = new PosInfo()
            {
                RequesterDK = currentContext.AccountId.ToString(CultureInfo.InvariantCulture),
                PosId = currentContext.PosId
            };

            foreach (var providerSpace in data)
            {
                var providerBasedComponents = FetchApplicableFareComponents(posInfo, providerSpace);

                //Following call filters based upon inclusion/exclusion and caches the ones applicable
                //at instance level against provider id and upline
                SplitApplicableComponentsForEachComponentType(providerSpace.ID, providerBasedComponents);
            }

            _initialized = true;
        }

        #endregion

        #region FareManager Methods

        public override ProductTypeOptions ProductType
        {
            get { return ProductTypeOptions.Air; }
        }

        public override void ApplyFareComponents(PosInfo posInfo, FlightFareDetails flightFareDetails)
        {
            if (!_initialized)
                throw new InvalidOperationException(KeyStore.ErrorMessages.FareManagerInitializationPending);

            try
            {
                if (flightFareDetails.Fare == null || flightFareDetails.Fare.TotalFare.BaseAmount == decimal.Zero)
                    return;

                var uplineNodes = HierarchyFacade.GetUplineHierarchy(Utility.GetCurrentContext().AccountId);

                var passengerCount =
                    flightFareDetails.Fare.PassengerFares.Where(x => x.TotalAmount.BaseAmount > 0).Sum(x => x.Quantity);

                foreach (var upline in uplineNodes)
                    AddFareComponentsToFare(posInfo, flightFareDetails, upline.Id, passengerCount);

                SetOwnerIdForCommissions(flightFareDetails.Fare.PassengerFares.SelectMany(x => x.Commissions),
                                         posInfo.RequesterDK);
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);
                throw;
            }
        }

        private static void SetOwnerIdForCommissions(IEnumerable<Entities.FareComponent> commissions, string requesterDk)
        {
            var uplineMap =
                GetUplineMapFromHierarchy(new AccountInfo() { AccountType = AccountTypeOptions.Company, DK = requesterDk });

            foreach (var commission in commissions.Distinct())
            {
                var definerLevel = uplineMap[commission.OwnerId];
                var applicableLevel = definerLevel + 1;

                commission.OwnerId = uplineMap.First(x => x.Value == applicableLevel).Key;
            }
        }

        private void SplitApplicableComponentsForEachComponentType(int providerId, ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>> applicableFareComponents)
        {
            foreach (var uplineSpecificComponents in applicableFareComponents)
            {
                var key = GetKeyForUplineMarkups(providerId, uplineSpecificComponents.Key);

                var filteredUplineComponents = new List<FareComponentRule>();
                foreach (var groupSpecificComponents in uplineSpecificComponents.Value)
                    filteredUplineComponents.AddRange(
                        FilterBasedOnInclusivityAndExclusivity(groupSpecificComponents.Value));

                _providerAndUplineBasedComponents.Add(key, GetComponentTypeMap(filteredUplineComponents));
            }
        }

        private static IEnumerable<FareComponentRule> FilterBasedOnInclusivityAndExclusivity(IEnumerable<FareComponentRule> fareComponentRules)
        {
            return fareComponentRules;
            //var applicableComponents = new List<FareComponentRule>();

            //var componentTypeGroups = fareComponentRules.GroupBy(x => x.FareComponentType);

            //foreach (var componentTypeGroup in componentTypeGroups)
            //{
            //    var inclusiveOnes = componentTypeGroup.Where(x => !x.IsExclusive);
            //    var exclusiveOne =
            //        componentTypeGroup.Where(x => x.IsExclusive).OrderBy(y => y.Preference).FirstOrDefault();

            //    applicableComponents.AddRange(inclusiveOnes);
            //    if (exclusiveOne != null)
            //        applicableComponents.Add(exclusiveOne);
            //}

            //return applicableComponents;
        }

        private static Dictionary<FareComponentTypeOptions, List<FareComponentRule>> GetComponentTypeMap(IEnumerable<FareComponentRule> fareComponentRules)
        {
            var componentTypeMap = new Dictionary<FareComponentTypeOptions, List<FareComponentRule>>();

            //Empty container to club Markup/Fee/DynamicMarkup
            componentTypeMap.Add(FareComponentTypeOptions.Markup, new List<FareComponentRule>());

            foreach (var group in fareComponentRules.GroupBy(x => x.FareComponentType))
            {
                //Accumulation in case of (Markup/DynamicMarkup/Fee)
                //thereby the addrange function for clubbing 
                //Same not needed for Commission/Discount
                if (MarkupAndFeesType.Contains(group.Key))
                    componentTypeMap[FareComponentTypeOptions.Markup].AddRange(group);
                else
                    componentTypeMap[group.Key] = group.ToList();
            }

            return componentTypeMap;
        }

        protected override string GetProviderSpaceFromValue(FlightFareDetails value)
        {
            return value.Provider.ID.ToString(CultureInfo.InvariantCulture);
        }

        protected override Rule GetRuleFromXmlString(string ruleXml)
        {
            var xml = XElement.Parse(ruleXml);

            return AirFareRule.FromXml(xml);
        }

        #endregion

        #region Helpers

        protected void ApplyFareComponent(FlightFare flightFare, FareComponentRule fareComponentRule, FareComponentType fareComponentType, Money currentFareComponentValue, int passengerCount)
        {
            var avgComponent = new Entities.FareComponent()
            {
                Value = currentFareComponentValue / passengerCount,
                FareComponentType = fareComponentType,
                Description = fareComponentRule.FriendlyName,
                OwnerId = long.Parse(fareComponentRule.OwnerId),
                FareComponentRuleId = fareComponentRule.Id,
                Code = fareComponentRule.Id.ToString(CultureInfo.InvariantCulture)
            };

            foreach (var pf in flightFare.PassengerFares)
            {
                if (pf.TotalAmount.BaseAmount <= 0)
                    continue;

                switch (fareComponentType)
                {
                    case FareComponentType.Commision:
                        pf.Commissions.Collate(avgComponent);
                        break;
                    case FareComponentType.Discount:
                        pf.Discounts.Collate(avgComponent);
                        break;
                    case FareComponentType.Fees:
                        pf.Fees.Collate(avgComponent);
                        break;
                    case FareComponentType.Markup:
                        pf.Markups.Collate(avgComponent);
                        break;
                    case FareComponentType.Tax:
                        pf.Taxes.Collate(avgComponent);
                        break;
                }
            }
        }

        protected Money GetCurrentFareComponentValue(FlightFareDetails flightFareDetails, FareComponentRule fareComponentRule, int passengerCount)
        {
            if (fareComponentRule.FareComponentType == FareComponentTypeOptions.DynamicMarkup)
            {
                if (flightFareDetails.Fare.Savings != null && flightFareDetails.Fare.FareType == FareType.Negotiated)
                    return GetCalculatedDynamicMarkup(fareComponentRule, flightFareDetails);
                return Money.Zero;
            }

            return GetCalculatedMarkup(flightFareDetails, fareComponentRule, passengerCount);
        }

        protected virtual int GetCabinClassRank(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Economy:
                    return 4;
                case CabinType.PremiumEconomy:
                    return 3;
                case CabinType.Business:
                    return 2;
                case CabinType.First:
                    return 1;
            }
            return 5;
        }

        protected CabinType GetHighestCabinClass(List<CabinType> cabinTypes)
        {
            var highestRank = int.MaxValue;
            var highestClass = CabinType.Unknown;

            foreach (var cabinType in cabinTypes)
            {
                int currentRank = GetCabinClassRank(cabinType);
                if (currentRank < highestRank)
                {
                    highestRank = currentRank;
                    highestClass = cabinType;
                }
            }

            return highestClass;
        }

        private void AddFareComponentsToFare(PosInfo posInfo, FlightFareDetails flightFareDetails, long upline, int passengerCount)
        {
            var key = GetKeyForUplineMarkups(flightFareDetails.Provider.ID, upline);

            var applicableProviderComponents = _providerAndUplineBasedComponents[key];

            List<FareComponentRule> dictRetrievedValues = null;
            List<FareComponentRule> markupAndFeesComponents = null;

            //Markup/DynamicMarkup/Fees are all keyed against => Markup
            if (applicableProviderComponents.TryGetValue(FareComponentTypeOptions.Markup, out dictRetrievedValues))
                markupAndFeesComponents = dictRetrievedValues ?? new List<FareComponentRule>();

            List<FareComponentRule> discountComponents = null;
            if (applicableProviderComponents.TryGetValue(FareComponentTypeOptions.Discount, out dictRetrievedValues))
                discountComponents = dictRetrievedValues ?? new List<FareComponentRule>();

            List<FareComponentRule> commissionComponents = null;
            if (applicableProviderComponents.TryGetValue(FareComponentTypeOptions.Commission, out dictRetrievedValues))
                commissionComponents = dictRetrievedValues ?? new List<FareComponentRule>();

            //Markup and fees need to be calculated on the current base fare
            ApplyComponents(flightFareDetails, posInfo, markupAndFeesComponents, passengerCount);
            //Discounts are applied after markups and fees
            ApplyComponents(flightFareDetails, posInfo, discountComponents, passengerCount);
            //Commissions are calculated at the end
            ApplyComponents(flightFareDetails, posInfo, commissionComponents, passengerCount);
        }

        private static string GetKeyForUplineMarkups(int providerId, long upline)
        {
            return string.Format("{0}-{1}", providerId, upline);
        }

        private void ApplyComponents(FlightFareDetails flightFareDetails, PosInfo posInfo, List<FareComponentRule> fareComponentRules, int passengerCount)
        {
            if (fareComponentRules == null || fareComponentRules.Count == 0)
                return;

            //Calculate final amount for all markups and fees beforehand
            //as their application should not affect the subsequent ones
            fareComponentRules.ForEach(
                fareComponentRule =>
                    fareComponentRule.FareComponentValue.FinalCalculatedAmount =
                        GetCurrentFareComponentValue(flightFareDetails, fareComponentRule,
                            passengerCount).ConvertTo(fareComponentRule.FareComponentValue.Currency).
                            NativeAmount);

            InclusiveExclusiveHandler.Process(fareComponentRules, (fareComponentRule) =>
            {
                if (fareComponentRule.FareComponentValue.FinalCalculatedAmount == 0 ||
                    !IsApplicable(flightFareDetails, posInfo.AdditionalInfo, fareComponentRule))
                    return false;

                ApplyFareComponent(flightFareDetails.Fare, fareComponentRule,
                    GetFareComponentType(fareComponentRule.FareComponentType),
                    new Money(
                        fareComponentRule.FareComponentValue.FinalCalculatedAmount,
                        fareComponentRule.FareComponentValue.Currency), passengerCount);
                return true;
            });
        }


        private static FareComponentType GetFareComponentType(FareComponentTypeOptions fareComponentTypeOptions)
        {
            switch (fareComponentTypeOptions)
            {
                case FareComponentTypeOptions.Markup:
                case FareComponentTypeOptions.DynamicMarkup:
                    return FareComponentType.Markup;
                case FareComponentTypeOptions.Commission:
                    return FareComponentType.Commision;
                case FareComponentTypeOptions.Fee:
                    return FareComponentType.Fees;
                case FareComponentTypeOptions.Discount:
                    return FareComponentType.Discount;
            }
            return FareComponentType.Markup;
        }

        private readonly
            ConcurrentDictionary
                <string, ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>>>
            _applicableFareComponents =
                new ConcurrentDictionary
                    <string, ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>>>();

        private bool _initialized;

        private ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>> FetchApplicableFareComponents(PosInfo posInfo, ProviderSpace providerSpace)
        {
            var accountType = GetAccountType(posInfo.RequesterDK);
            var key = GetCacheKey(posInfo.RequesterDK, accountType, posInfo.PosId, providerSpace.ID.ToString(CultureInfo.InvariantCulture));

            ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>> applicableFareComponents;

            if (_applicableFareComponents.TryGetValue(key, out applicableFareComponents))
                return applicableFareComponents;

            var cacheProvider =
                RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.Markup);

            applicableFareComponents = cacheProvider.Get<ConcurrentDictionary<long, ConcurrentDictionary<string, List<FareComponentRule>>>>(
                KeyStore.CacheBucket.AirMarkup, key);

            if (applicableFareComponents != null)
            {
                _applicableFareComponents[key] = applicableFareComponents;

                return applicableFareComponents;
            }

            applicableFareComponents =
                this.GetApplicableFareComponents(
                    new AccountInfo() { AccountType = AccountTypeOptions.Company, DK = posInfo.RequesterDK },
                    posInfo.PosId.ToString(CultureInfo.InvariantCulture),
                    providerSpace.ID.ToString(CultureInfo.InvariantCulture));

            cacheProvider.Insert(KeyStore.CacheBucket.AirMarkup, key, applicableFareComponents,
                              new ExpirationSettings()
                              {
                                  ExpirationType = ExpirationType.SlidingExpiration,
                                  SlidingExpirationInterval =
                                      new TimeSpan(
                                      TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                              });

            _applicableFareComponents[key] = applicableFareComponents;

            return applicableFareComponents;
        }

        private static AccountTypeOptions GetAccountType(string accountId)
        {
            var accountType = HierarchyFacade.GetNodeDetails(long.Parse(accountId)).Type.Trim();
            return AccountTypeMapping[accountType];
        }

        protected virtual Money GetCalculatedMarkup(FlightFareDetails flightFareDetails, FareComponentRule fareComponentRule, int passengerCount)
        {
            var currentFareComponentValue = fareComponentRule.FareComponentValue.Amount;

            if (fareComponentRule.FareComponentValue.FareComponentValueType == FareComponentValueType.Absolute)
            {
                switch (fareComponentRule.FareComponentValue.CalculationType)
                {
                    case CalculationTypeOptions.PerPerson:
                        currentFareComponentValue *= passengerCount;
                        break;
                    case CalculationTypeOptions.PerLeg:
                        currentFareComponentValue *= flightFareDetails.LegCount;
                        break;
                    case CalculationTypeOptions.PerSegment:
                        currentFareComponentValue *= flightFareDetails.SegmentCount;
                        break;
                }
                return new Money(currentFareComponentValue,
                                   fareComponentRule.FareComponentValue.Currency);
            }

            return MarkupCalculator.GetPercentCalculatedAmount(flightFareDetails.Fare, fareComponentRule);
        }

        private static Money GetCalculatedDynamicMarkup(FareComponentRule fareComponentRule, FlightFareDetails flightFareDetails)
        {
            var dynamicCurrency = fareComponentRule.DynamicValue.Currency;
            var dynamicAmount = fareComponentRule.DynamicValue.Amount;
            var dynamicAmountMoney = new Money(dynamicAmount, dynamicCurrency);

            var currentNetRateSavings = (flightFareDetails.Fare.Savings - flightFareDetails.Fare.TotalMarkup +
                                 flightFareDetails.Fare.TotalDiscount).ConvertTo(dynamicCurrency);

            var currentbasefare = GetCurrentBaseFare(flightFareDetails.Fare, dynamicCurrency);

            var maxLimit = Money.Zero;

            //Calculate the max limit to which the markup can go as per the combination of valuetype and calculation level

            switch (fareComponentRule.DynamicValue.FareComponentValueType)
            {
                case FareComponentValueType.Absolute:
                    if (fareComponentRule.DynamicValue.CalculationLevel == CalculationLevel.Below)
                        maxLimit = (currentbasefare + currentNetRateSavings) - dynamicAmountMoney;
                    else
                        maxLimit = currentbasefare + dynamicAmountMoney;
                    break;
                case FareComponentValueType.Percent:
                    if (fareComponentRule.DynamicValue.CalculationLevel == CalculationLevel.Above)
                        maxLimit = GetPercentMoney(currentbasefare, 100 + dynamicAmount);
                    else
                        maxLimit = GetPercentMoney(currentbasefare + currentNetRateSavings, 100 - dynamicAmount);
                    break;
            }

            var calculatedMarkupAmount = maxLimit - currentbasefare;

            var minimumDynamicAmountMoney = new Money(fareComponentRule.DynamicValue.MinimumValue,
                                                      fareComponentRule.DynamicValue.Currency);
            if (calculatedMarkupAmount.BaseAmount < minimumDynamicAmountMoney.BaseAmount)
                calculatedMarkupAmount = minimumDynamicAmountMoney;

            //If the calculated amount exceeds the current netrate savings then check the jump nego flag
            //to decide whether to allow bypassing the published rate

            if (calculatedMarkupAmount.BaseAmount > currentNetRateSavings.BaseAmount && !flightFareDetails.JumpNegoRate)
                calculatedMarkupAmount = currentNetRateSavings;

            return calculatedMarkupAmount;
        }

        private static Money GetCurrentBaseFare(FlightFare flightFare, string currency)
        {
            var currentBaseFare = flightFare.BaseAmount + flightFare.TotalMarkup - flightFare.TotalDiscount;

            return currentBaseFare.ConvertTo(currency);
        }

        private static Money GetPercentMoney(Money money, decimal percent)
        {
            return new Money(percent / 100 * money.BaseAmount, money.BaseCurrency);
        }

        protected bool IsApplicable(FlightFareDetails flightFareDetails, IDictionary<string, string> additionalInfo, FareComponentRule fareComponentRule)
        {
            var airFareRule = fareComponentRule.Rule as AirFareRule;

            if (fareComponentRule.Distribution == null || airFareRule == null)
                return false;

            var isApplicable = true;

            if (!string.IsNullOrEmpty(airFareRule.CustomerType))
                isApplicable =
                    airFareRule.CustomerType.ContainsValue(flightFareDetails.Provider.Requester.CustomerType.ToString());

            isApplicable = isApplicable && fareComponentRule.Distribution.ProviderSpace == flightFareDetails.Provider.ID.ToString();

            if (!string.IsNullOrEmpty(airFareRule.RateCode))
            {
                var rateCodeFound = flightFareDetails.Provider.Contracts != null &&
                                    flightFareDetails.Provider.Contracts.Any(
                                        x =>
                                        string.Compare(x.ContractRateCode, airFareRule.RateCode,
                                                       StringComparison.OrdinalIgnoreCase) == 0);

                isApplicable = isApplicable && rateCodeFound;
            }

            isApplicable = isApplicable && IsTravelTypeCriteriaApplicable(flightFareDetails, airFareRule.TravelType);

            isApplicable = isApplicable && IsItineraryCriteriaApplicable(airFareRule, flightFareDetails);

            isApplicable = isApplicable && IsTravelDateCriteriaApplicable(airFareRule, flightFareDetails);

            if (isApplicable && airFareRule.AirFareRange != null)
                isApplicable = IsFareRangeApplicable(airFareRule.AirFareRange, flightFareDetails.Fare);
            return isApplicable;
        }

        private bool IsTravelTypeCriteriaApplicable(FlightFareDetails flightFareDetails, TravelType travelType)
        {
            if (travelType == null)
                return true;

            var isApplicable = true;

            isApplicable = travelType.ItineraryType == ItineraryTypeOptions.NotSet ||
                               travelType.ItineraryType == flightFareDetails.ItineraryType;

            isApplicable = isApplicable &&
                           (travelType.CabinType == CabinType.Unknown ||
                            travelType.CabinType == GetHighestCabinClass(flightFareDetails.CabinTypes));

            return isApplicable;
        }

        private static bool IsItineraryCriteriaApplicable(AirFareRule airFareRule, FlightFareDetails flightFareDetails)
        {
            if (airFareRule.Itinerary == null)
                return true;

            var isApplicable = airFareRule.Itinerary.Supplier.ContainsValue(flightFareDetails.Provider.Family);

            isApplicable = isApplicable && airFareRule.Itinerary.OriginCountries.ContainsValue(flightFareDetails.OriginCountry);

            isApplicable = isApplicable && airFareRule.Itinerary.OriginAirports.ContainsValue(flightFareDetails.OriginAirportCode);

            isApplicable = isApplicable && airFareRule.Itinerary.DestinationCountries.ContainsValue(flightFareDetails.DestinationCountry);
            isApplicable = isApplicable && airFareRule.Itinerary.DestinationAirports.ContainsValue(flightFareDetails.DestinationAirportCode);

            isApplicable = isApplicable && (airFareRule.Itinerary.FareType == FareType.Unknown || airFareRule.Itinerary.FareType == flightFareDetails.Fare.FareType);

            isApplicable = isApplicable &&
                           IsMarketingCarriersCriteriaApplicable(flightFareDetails,
                                                                 airFareRule.Itinerary.MarketingCarriers);

            isApplicable = isApplicable &&
                           IsClassOfServiceCriteriaApplicable(flightFareDetails, airFareRule.Itinerary.ClassesOfService);

            isApplicable = isApplicable &&
                           IsFareBasisCriteriaApplicable(flightFareDetails, airFareRule.Itinerary.FareBasisCodes);

            return isApplicable;
        }

        private static bool IsMarketingCarriersCriteriaApplicable(FlightFareDetails flightFareDetails, string marketingCarriers)
        {
            if (string.IsNullOrEmpty(marketingCarriers))
                return true;

            var marketingCarriersMatched = false;

            var marketingAirlines = flightFareDetails.MarketingCarriers.Select(x => x.ToLower()).Distinct();

            var ruleCarriers = new List<string>();

            ruleCarriers.AddRange(marketingCarriers.Split(',')
                .Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.ToLower().Trim()));

            if (marketingAirlines.All(x => ruleCarriers.Any(y => y.Equals(x, StringComparison.Ordinal))))
                marketingCarriersMatched = true;

            return marketingCarriersMatched;
        }

        private static bool IsClassOfServiceCriteriaApplicable(FlightFareDetails flightFareDetails, string classesOfService)
        {
            if (string.IsNullOrEmpty(classesOfService))
                return true;

            var classOfServiceMatched = false;

            var ruleClassesOfService = classesOfService.Split(',')
                .Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.ToLower().Trim()).ToListBuffered();

            if (flightFareDetails.ClassOfServices.Select(x => x.ToLower()).All(x => ruleClassesOfService.Any(y => y.Equals(x, StringComparison.Ordinal))))
                classOfServiceMatched = true;

            return classOfServiceMatched;
        }

        private static bool IsFareBasisCriteriaApplicable(FlightFareDetails flightFareDetails, string fareBasisCodes)
        {
            if (string.IsNullOrEmpty(fareBasisCodes))
                return true;

            var fareBasisCodeMatched = false;

            var ruleFareBasisCodes = fareBasisCodes.Split(',').Select(x => x.ToLower().Trim()).ToListBuffered();

            if (flightFareDetails.FareBasisCodes.All(x => ruleFareBasisCodes.Any(y => y.Equals(x.ToLower(), StringComparison.Ordinal))))
                fareBasisCodeMatched = true;

            return fareBasisCodeMatched;
        }

        private static bool IsTravelDateCriteriaApplicable(AirFareRule airFareRule, FlightFareDetails flightFareDetails)
        {
            if (airFareRule.TravelDates == null)
                return true;

            var isApplicable = true;

            var currentDateTime = DateTime.UtcNow;

            if (airFareRule.TravelDates.AdvancePurchase != null)
            {
                var advancedPurchasedSpan = flightFareDetails.DepartureTime - currentDateTime;
                isApplicable =
                    !((airFareRule.TravelDates.AdvancePurchase.MinimumDays.HasValue &&
                        airFareRule.TravelDates.AdvancePurchase.MinimumDays > advancedPurchasedSpan.Days) ||
                        (airFareRule.TravelDates.AdvancePurchase.MaximumDays.HasValue &&
                        airFareRule.TravelDates.AdvancePurchase.MaximumDays < advancedPurchasedSpan.Days));
            }

            var completionTime = flightFareDetails.ArrivalTime;
            var starTime = flightFareDetails.DepartureTime;

            if (isApplicable && airFareRule.TravelDates.TravelDateSpan != null && airFareRule.TravelDates.TravelDateSpan.From.HasValue && airFareRule.TravelDates.TravelDateSpan.To.HasValue)
                isApplicable = ((airFareRule.TravelDates.TravelDateSpan.From.Value.Date <= starTime.Date) &&
                                (airFareRule.TravelDates.TravelDateSpan.To.Value.Date >= completionTime.Date));

            if (isApplicable && airFareRule.TravelDates.BookingDateSpan != null && airFareRule.TravelDates.BookingDateSpan.From.HasValue && airFareRule.TravelDates.BookingDateSpan.To.HasValue)
                isApplicable = ((airFareRule.TravelDates.BookingDateSpan.From.Value.Date <= currentDateTime.Date) &&
                                (airFareRule.TravelDates.BookingDateSpan.To.Value.Date >= currentDateTime.Date));

            if (airFareRule.TravelDates.TravelDuration != null)
            {
                var travelDuration = completionTime - starTime;
                isApplicable = !((airFareRule.TravelDates.TravelDuration.MinimumDays.HasValue && airFareRule.TravelDates.TravelDuration.MinimumDays > travelDuration.Days) ||
                                    (airFareRule.TravelDates.TravelDuration.MaximumDays.HasValue && airFareRule.TravelDates.TravelDuration.MaximumDays < travelDuration.Days)) && isApplicable;
            }

            return isApplicable;
        }

        private static bool IsFareRangeApplicable(AirFareRange airFareRange, FlightFare flightFare)
        {
            var applicablePassengerTypes =
                flightFare.PassengerFares.Where(x => x.TotalAmount != null && x.TotalAmount.BaseAmount > 0).Select(
                    x => x.PassengerType).ToListBuffered();

            var passengerCount =
                flightFare.PassengerFares.Where(x => x.TotalAmount != null && x.TotalAmount.BaseAmount > 0).Sum(
                    y => y.Quantity);

            var fareRangeFareType = airFareRange.FareRangeFareType;
            Money totalFare;
            var isApplicable = false;
            if (fareRangeFareType == FareRangeFareType.PerPersonAverageFare)
            {
                totalFare = flightFare.TotalFare / passengerCount;
                return airFareRange.CheckFareRange(totalFare.ConvertTo(airFareRange.Currency).NativeAmount);
            }
            totalFare = Money.Zero;
            foreach (var passengerFare in flightFare.PassengerFares)
            {
                if (!applicablePassengerTypes.Contains(passengerFare.PassengerType))
                    continue;
                if (fareRangeFareType == FareRangeFareType.PerPersonTotalFare)
                    totalFare = passengerFare.TotalAmount;
                else if (fareRangeFareType == FareRangeFareType.PerPersonBaseFare)
                    totalFare = passengerFare.BaseAmount + passengerFare.TotalMarkup -
                                passengerFare.TotalDiscount;
                if (totalFare.NativeAmount > 0 && airFareRange.CheckFareRange(totalFare.ConvertTo(airFareRange.Currency).NativeAmount))
                    isApplicable = true;
                else
                    applicablePassengerTypes.Remove(passengerFare.PassengerType);
            }
            return isApplicable;
        }

        #endregion
    }
}
