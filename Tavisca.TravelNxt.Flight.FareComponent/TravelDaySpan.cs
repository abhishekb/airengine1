﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class TravelDaySpan
    {
        public int? MinimumDays { get; set; }
        public int? MaximumDays { get; set; }
    }
}