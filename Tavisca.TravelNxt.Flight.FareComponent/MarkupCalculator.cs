﻿using System;
using System.Linq;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    public class MarkupCalculator
    {
        public static Money GetPercentCalculatedAmount(FlightFare flightFare, FareComponentRule fareComponentRule)
        {
            switch (fareComponentRule.FareComponentValue.FareApplicableOn)
            {
                case FareApplicableOnOptions.BaseFare:
                    return GetPercentCalculatedAmountOnBaseFare(flightFare,
                        fareComponentRule);
                case FareApplicableOnOptions.TotalFare:
                    return GetPercentCalculatedAmountOnTotalFare(flightFare,
                        fareComponentRule);
                default:
                    throw new InvalidOperationException(string.Format("Unknown FareApplicableOnOptions - {0}",
                        fareComponentRule.FareComponentValue.FareApplicableOn));
            }
        }

        #region private methods

        private static Money GetPercentCalculatedAmountOnBaseFare(FlightFare flightFare, FareComponentRule fareComponentRule)
        {
            var currency = fareComponentRule.FareComponentValue.Currency;

            var amount = fareComponentRule.FareComponentValue.CalculationType == CalculationTypeOptions.PerPerson
                ? flightFare.PassengerFares.Select(x => fareComponentRule.GetApplicableValue(
                    x.BaseAmount.ConvertTo(currency)
                        .ToNativeCurrencyValue()) * x.Quantity).Aggregate((x, y) => (x + y))
                : fareComponentRule.GetApplicableValue(
                    flightFare.BaseAmount.ConvertTo(currency)
                        .ToNativeCurrencyValue());

            return new Money(amount, currency);
        }

        private static Money GetPercentCalculatedAmountOnTotalFare(FlightFare flightFare, FareComponentRule fareComponentRule)
        {
            var currency = fareComponentRule.FareComponentValue.Currency;

            var amount = fareComponentRule.FareComponentValue.CalculationType == CalculationTypeOptions.PerPerson
                ? flightFare.PassengerFares.Select(x => fareComponentRule.GetApplicableValue(
                    x.TotalAmount.ConvertTo(currency)
                        .ToNativeCurrencyValue())*x.Quantity).Aggregate((x, y) => (x + y))
                : fareComponentRule.GetApplicableValue(
                    flightFare.TotalFare.ConvertTo(currency)
                        .ToNativeCurrencyValue());

            return new Money(amount, currency);
        }

        #endregion
    }
}
