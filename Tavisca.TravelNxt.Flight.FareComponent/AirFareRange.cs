﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Common.FareComponent;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class AirFareRange : FareRange
    {
        public static readonly List<FareRangeFareType> ValidFareRangeFareType = new List<FareRangeFareType>()
                                                                             {
                                                                                 FareRangeFareType.PerPersonBaseFare,
                                                                                 FareRangeFareType.PerPersonTotalFare,
                                                                                 FareRangeFareType.PerPersonAverageFare, 
                                                                             };

        private FareRangeFareType _fareRangeFareType;
        public override FareRangeFareType FareRangeFareType
        {
            get { return _fareRangeFareType; }
            set
            {
                if (ValidFareRangeFareType.Contains(value))
                {
                    _fareRangeFareType = value;
                }
                else
                {
                    throw new InvalidDataException("Invalid FareRangeFareType");
                }
            }
        }

        public bool CheckFareRange(decimal fare)
        {
            var isApplicable = false;
            if (MinValue.HasValue && MaxValue.HasValue)
            {
                if (fare >= MinValue && fare <= MaxValue)
                    isApplicable = true;
            }
            else if (MinValue.HasValue)
            {
                if (fare >= MinValue)
                    isApplicable = true;
            }
            else if (MaxValue.HasValue)
            {
                if (fare <= MaxValue)
                    isApplicable = true;
            }
            return isApplicable; 
        }
    }
}
