﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class TravelDateSpan
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}