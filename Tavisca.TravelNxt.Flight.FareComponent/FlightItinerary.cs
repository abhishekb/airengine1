﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightItinerary
    {
        public string OriginCountries { get; set; }
        public string OriginAirports { get; set; }
        public string DestinationCountries { get; set; }
        public string DestinationAirports { get; set; }
        public string MarketingCarriers { get; set; }
        public string ClassesOfService { get; set; }
        public FareType FareType { get; set; }
        public string FareBasisCodes { get; set; }
        public string Supplier { get; set; }
    }
}
