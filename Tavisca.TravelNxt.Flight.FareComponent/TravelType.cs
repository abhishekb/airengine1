﻿using System;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.FareComponent
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class TravelType
    {
        public ItineraryTypeOptions ItineraryType { get; set; }
        public CabinType CabinType { get; set; }
    }
}