﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.FaultContract;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace Tavisca.TravelNxt.Flight.Avail.ServiceContract
{
    [ServiceContract(Namespace = "http://tavisca.com/Air/V1")]
    public interface IAirEngine
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "search", BodyStyle = WebMessageBodyStyle.Bare)]
        FlightSearchRS Search(FlightSearchRQ request);

        [OperationContract]
        [WebInvoke(UriTemplate = "tryGetFromSession", BodyStyle = WebMessageBodyStyle.Bare)]
        FlightSearchRS TryGetFromSession(FlightPollRQ request);

        [OperationContract(Name = "WebAPISearch")]
        [FaultContract(typeof(ApplicationFault))]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/avail?uid={uid}&cul={cul}&cid={cid}&curr={curr}&ip={ip}&numRes={numRes}" +
                    "&adt={adt}&chld={chld}&inf={inf}&senr={senr}&frm1={frm1}&frm2={frm2}&frm3={frm3}&frm4={frm4}&frm5={frm5}" +
                    "&isfrm1city={isfrm1city}&isfrm2city={isfrm2city}&isfrm3city={isfrm3city}&isfrm4city={isfrm4city}&isfrm5city={isfrm5city}"+
                    "&to1={to1}&to2={to2}&to3={to3}&to4={to4}&to5={to5}"+
                    "&isto1city={isto1city}&isto2city={isto2city}&isto3city={isto3city}&isto4city={isto4city}&isto5city={isto5city}" +
                    "&date1={date1}&date2={date2}&date3={date3}&date4={date4}&date5={date5}" +
                    "&trip={trip}&nonstop={nonstop}&cabin={cabin}&window={window}&providebaggageinfo={providebaggageinfo}")]
        AirAvailRs Search(string uid, string cul, string cid, string curr, string ip, int numRes, int adt, int chld, int inf, int senr,
            string frm1, bool isfrm1city, string to1, bool isto1city, string frm2, bool isfrm2city, string to2, bool isto2city, string frm3, 
            bool isfrm3city, string to3, bool isto3city, string frm4, bool isfrm4city, string to4, bool isto4city, string frm5, bool isfrm5city, string to5, bool isto5city,
            string date1, string date2, string date3, string date4, string date5, string trip, bool nonstop, string cabin, int window, bool providebaggageinfo);     
    }
}
