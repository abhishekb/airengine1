﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Parallel.Ambience.Wcf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Validation;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Price.DataContract;
using Tavisca.TravelNxt.Flight.Price.ServiceContract;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Price.ServiceImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AmbientOperation(ApplicationName = "Air Price")]
    public class AirPriceService : IAirPrice
    {
        static AirPriceService()
        {
            Utility.SetApplicationName("Air Engine");
        }

        #region IAirPrice Members

        public FlightPriceRS Price(FlightPriceRQ request)
        {
            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            ILogger logFactory = null;
            IEventEntry entry = null;

            try
            {
                logFactory = Utility.GetLogFactory();

                entry = Utility.GetEventEntryContextual();

                entry.Title = "Pricing - Root Level Pricing Log";

                entry.CallType = KeyStore.CallTypes.AirPrice;

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var validationResult = GetValidationFactory().Validate(request);

                if (!validationResult.IsValid)
                {
                    throw validationResult.ToException();
                }

                var priceEngine = RuntimeContext.Resolver.Resolve<IFlightPriceEngine>();

                var entity = FlightPriceRQ.ToEntity(request);

                var result = priceEngine.Pricing(entity);

                var retVal = FlightPriceRS.ToDataContract(result);

                stopwatch.Stop();
                entry.TimeTaken = stopwatch.ElapsedSeconds;

                if (retVal != null)
                    entry.ResponseObject = retVal;

                return retVal;
            }
            catch (Exception ex)
            {
                if (logFactory != null)
                    logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                var response = new FlightPriceRS()
                    {
                        CallStatus = CallStatus.Failure,
                        Message = "Some error occured.",
                        StatusCode = "PricingFailureCode" //TODO1

                    };
                return response;

            }
            finally
            {
                if (logFactory != null && entry != null)
                    logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
            }
        }

        public FlightPriceRS GetPricedResult(FlightGetPricedRQ request)
        {
            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            ILogger logFactory = null;
            IEventEntry entry = null;

            try
            {
                logFactory = Utility.GetLogFactory();

                entry = Utility.GetEventEntryContextual();

                entry.Title = "Pricing - Root Level Pricing Log";

                entry.CallType = KeyStore.CallTypes.AirPriceRetrieve;

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var validationResult = GetValidationFactory().Validate(request);

                if (!validationResult.IsValid)
                {
                    throw validationResult.ToException();
                }
                
                var priceEngine = RuntimeContext.Resolver.Resolve<IFlightPriceEngine>();

                var entity = FlightGetPricedRQ.ToEntity(request);

                var result = priceEngine.GetPrePricedResult(entity);

                var retVal = FlightPriceRS.ToDataContract(result);

                stopwatch.Stop();
                entry.TimeTaken = stopwatch.ElapsedSeconds;

                if (retVal != null)
                    entry.ResponseObject = retVal;

                return retVal;
            }
            catch (Exception ex)
            {
                if (logFactory != null)
                    logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                var response = new FlightPriceRS()
                {
                    CallStatus = CallStatus.Failure,
                    Message = "Some error occured.",
                    StatusCode = "PricingFailureCode" //TODO1

                };
                return response;

            }
            finally
            {
                if (logFactory != null && entry != null)
                    logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
            }
        }

        #endregion

        #region Protected Members

        protected IValidationFactory GetValidationFactory()
        {
            return RuntimeContext.Resolver.Resolve<IValidationFactory>();
        }

        #endregion
    }
}
