﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Price.DataContract;
using System.ServiceModel;

namespace Tavisca.TravelNxt.Flight.Price.ServiceContract
{
    [ServiceContract(Namespace = "http://tavisca.com")]
    public interface IAirPrice
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "price", BodyStyle = WebMessageBodyStyle.Wrapped)]
        FlightPriceRS Price(FlightPriceRQ request);

        [OperationContract]
        [WebInvoke(UriTemplate = "price", BodyStyle = WebMessageBodyStyle.Wrapped)]
        FlightPriceRS GetPricedResult(FlightGetPricedRQ request);
    }
}
