﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Session;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Common.Security.Encryption;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using Tavisca.TravelNxt.Flight.Settings;


namespace Tavisca.TravelNxt.Flight.Price.Core
{
    public class PricingEngine : IFlightPriceEngine
    {
        #region IFlightPriceEngine Members

        public virtual FlightPriceResult Pricing(FlightPriceRQ request)
        {
            var result = new FlightPriceResult();
            try
            {
                var recommendation = GetFromSession(request.RecommendationRefID);
                if (recommendation != null)
                {
                    var flightFare = recommendation.Fare;

                    var providerStrategy = RuntimeContext.Resolver.Resolve<IPricingProviderStrategy>();

                    recommendation = providerStrategy.Price(request, recommendation);

                    if (recommendation == null)
                    {
                        result.Status = CallStatus.Failure;
                        result.Message = "Some error occured while Pricing!";

                        return result;
                    }

                    var markupStrategy = RuntimeContext.Resolver.Resolve<IFareManager<FlightRecommendation, ICollection<ProviderSpace>>>(KeyStore.SingularityNameKeys.AirFareManager);

                    markupStrategy.ApplyFareComponents(new PosInfo()
                            {
                                PosId = request.Requester.POS.ID,
                                RequesterDK = request.Requester.ID.ToString(CultureInfo.InvariantCulture)
                            }, recommendation);

                    result.Recommendation = recommendation;

                    if (flightFare.TotalFare.BaseAmount == recommendation.Fare.TotalFare.BaseAmount)
                        result.Status = CallStatus.Success;
                    else if (flightFare.TotalFare.BaseAmount < recommendation.Fare.TotalFare.BaseAmount)
                    {
                        result.Status = CallStatus.Warning;
                        // TODO1: Put valida code here increased fare
                        result.StatusCode = "Increased";
                    }
                    else
                    {
                        result.Status = CallStatus.Warning;
                        result.StatusCode = "Decreased";
                    }

                    result.ExternalIdentifier = GetExternalIdentifier(result);

                    GetSessionStore().AddAsync(KeyStore.APISessionKeys.FlightPriceCategory + request.RecommendationRefID, GetSessionId(), result);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                //result.Code = 
                result.Status = CallStatus.Failure;
                result.Message = "Some error occured while Pricing!";
            }

            if (result.Recommendation == null && result.Status != CallStatus.Failure)
            {
                result.Status = CallStatus.Failure;
                result.Message = "The recommendation was not found.";
            }

            return result;
        }

        public virtual FlightPriceResult GetPrePricedResult(FlightGetPricedRQ request)
        {
            if (request == null)
                return null;

            var result = new FlightPriceResult();
            try
            {
                var id = ParseExternalIdentifier(request.ExternalIdentifier);

                var recommendation = GetFromSession(id.Item1, id.Item2);

                result.Recommendation = recommendation;

                result.ExternalIdentifier = request.ExternalIdentifier;

                result.Status = CallStatus.Success;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                //result.Code = 
                result.Status = CallStatus.Failure;
                result.Message = "Some error occured while fetching the pricing result!";
            }

            if (result.Recommendation == null && result.Status != CallStatus.Failure)
            {
                result.Status = CallStatus.Failure;
                result.Message = "The recommendation was not found.";
            }

            return result;
        }

        #endregion

        #region Protected Members

        protected FlightRecommendation GetFromSession(int refNumber)
        {
            return GetFromSession(refNumber, GetSessionId());
        }

        protected FlightRecommendation GetFromSession(int refNumber, string sessionId)
        {
            var result = GetSessionStore()
                .Get<FlightAvailResult>(KeyStore.APISessionKeys.FlightSeachCategory, sessionId);

            if (result != null)
            {
                var recommendation = result.FlightRecommendations.FirstOrDefault(x => x.RefId == refNumber);

                return recommendation;
            }
            return null;
        }

        protected string GetSessionId()
        {
            var context = Utility.GetCurrentContext();

            return context.SessionId.ToString();
        }

        private const string SessionIdKey = "SessionId";
        private const string RefIdKey = "RefId";
        private const char KeyValSeperator = '=';
        private const char ItemSeperator = '&';

        protected string GetExternalIdentifier(FlightPriceResult priceResult)
        {
            var builder = new StringBuilder();

            builder.Append(SessionIdKey).Append(KeyValSeperator).Append(GetSessionId()).Append(ItemSeperator)
                .Append(RefIdKey).Append(KeyValSeperator).Append(priceResult.Recommendation.RefId);

            return EncryptionManager.Encrypt(builder.ToString());
        }

        protected Tuple<int, string> ParseExternalIdentifier(string externalId)
        {
            if (string.IsNullOrEmpty(externalId))
                return null;

            var id = EncryptionManager.Decrypt(externalId);

            if (string.IsNullOrEmpty(id))
                return null;

            var items = id.Split(ItemSeperator);

            if (items.Length == 0)
                return null;

            int? refId = null;
            string sessionId = null;

            foreach (var item in items)
            {
                var pair = item.Split(KeyValSeperator);

                if (pair.Length != 2)
                    continue;

                switch (pair[0])
                {
                    case SessionIdKey:
                        sessionId = pair[1];
                        break;
                    case RefIdKey:
                        refId = int.Parse(pair[1]);
                        break;
                }
            }

            if (!refId.HasValue || string.IsNullOrEmpty(sessionId))
                return null;

            return new Tuple<int, string>(refId.Value, sessionId);
        }

        private static SessionStore GetSessionStore()
        {
            return Utility.GetSessionProvider();
        }

        #endregion
    }
}
