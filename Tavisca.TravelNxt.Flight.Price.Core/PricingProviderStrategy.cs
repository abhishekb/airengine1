﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Common;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Price.Core
{
    public class PricingProviderStrategy : IPricingProviderStrategy
    {
        #region IPricingProviderStrategy Members

        public virtual FlightRecommendation Price(FlightPriceRQ request, FlightRecommendation recommendation)
        {
            FlightRecommendation retVal = null;
            try
            {
                var configurationCollection = GetConfigurationCollection();

                var providerSpaceOriginal = recommendation.ProviderSpace;

                var providerSpaceManager = RuntimeContext.Resolver.Resolve<IProviderSpaceManager>();

                var providerSpaces = providerSpaceManager.GetAllProviderSpaceForRequest(request.Requester);

                if (providerSpaceOriginal == null || providerSpaceOriginal.Requester == null ||
                    providerSpaces.All(x => x.ID != providerSpaceOriginal.ID))
                    throw new InvalidDataException("The provider space is not valid for this pricing call.");

                var config = configurationCollection.GetConfiguration(providerSpaceOriginal.ID,
                                                                      providerSpaceOriginal.Requester.ID);

                var providerSpacePrimary = SwapProviderSpace(config.FareSourceReplacementPriceConfigurations,
                                                             providerSpaces, recommendation, providerSpaceOriginal);

                var failoverProviderSpaces = new List<ProviderSpace>();

                var requestedProviderSpaceIds = new List<int>();

                if (config.IsFailoverEnabled && config.FareSourcePriceConfigurations.Count > 0)
                {
                    var originalId = providerSpaceOriginal.ID;
                    var replacementConfigurations = config.FareSourceReplacementPriceConfigurations;

                    var failOverQuery =
                        config.FareSourcePriceConfigurations.Where(
                            x => providerSpaces.Any(p => p.ID == x.ProviderSpaceId)
                                 && x.ProviderSpaceId != originalId)
                              .OrderBy(x => x.PreferenceOrder)
                              .Select(x => SwapProviderSpace(replacementConfigurations,
                                                             providerSpaces, recommendation, 
                                                             providerSpaces.First(p => p.ID == x.ProviderSpaceId)));

                    failoverProviderSpaces = failOverQuery.ToListBuffered();
                }
                
                if (config.IsFailoverEnabled && config.FareSourcePriceConfigurations.Count == 0)
                {
                    failoverProviderSpaces.AddRange(providerSpaces);
                }

                var provider = GetProvider(request, providerSpacePrimary);

                try
                {
                    requestedProviderSpaceIds.Add(providerSpacePrimary.ID);
                    requestedProviderSpaceIds.Add(providerSpaceOriginal.ID);

                    retVal = provider.Price(request, recommendation, providerSpacePrimary);
                }
                catch (HandledException) { }

                while (retVal == null && failoverProviderSpaces.Count > 0)
                {
                    var current = failoverProviderSpaces[0];

                    failoverProviderSpaces.RemoveAt(0);

                    if (requestedProviderSpaceIds.Any(x => x == current.ID))
                        continue;

                    requestedProviderSpaceIds.Add(current.ID);

                    var newProviderSpace = SwapProviderSpace(config.FareSourceReplacementPriceConfigurations,
                                                             providerSpaces, recommendation, current);

                    requestedProviderSpaceIds.Add(newProviderSpace.ID);

                    provider = GetProvider(request, newProviderSpace);

                    try
                    {
                        retVal = provider.Price(request, recommendation, newProviderSpace);
                    }
                    catch (HandledException) { }
                }
            }

            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
            }

            return retVal;
        }

        #endregion

        #region Protected Members

        protected virtual IFareSourcePriceConfigurationCollection GetConfigurationCollection()
        {
            return RuntimeContext.Resolver.Resolve<IFareSourcePriceConfigurationCollection>();
        }

        protected virtual IPriceProvider GetProvider(FlightPriceRQ request, ProviderSpace providerSpace)
        {
            IPriceProvider provider;

            if (request.AdditionalInfo != null &&
                request.AdditionalInfo.ContainsKey(KeyStore.MockKeys.Mock))
            {
                provider = RuntimeContext.Resolver.Resolve<IPriceProvider>(
                    KeyStore.SingularityNameKeys.PriceMockDefaultProvider);
            }
            else
            {
                var configuration =
                    providerSpace.Configurations.FirstOrDefault(
                        x => x.ConfigType == ConfigurationType.Price);

                if (configuration == null)
                    throw new InvalidDataException("Fare source configuration not found for pricing, fare source: " +
                        providerSpace.ID.ToString(CultureInfo.InvariantCulture));


                var providerLocator = string.IsNullOrWhiteSpace(configuration.ContractVersion)
                                          ? KeyStore.SingularityNameKeys.PriceDefaultProvider
                                          : configuration.ContractVersion;

                provider = RuntimeContext.Resolver.Resolve<IPriceProvider>(providerLocator);
            }

            return provider;
        }

        protected virtual ProviderSpace SwapProviderSpace(
            ICollection<ProviderSpaceReplacementPriceConfiguration> providerSpaceReplacementPriceConfigurations,
            IList<ProviderSpace> validProviderSpaces, FlightRecommendation recommendation, ProviderSpace original)
        {
            if (providerSpaceReplacementPriceConfigurations.Count > 0)
            {
                var target =
                    providerSpaceReplacementPriceConfigurations.FirstOrDefault(
                        x =>
                        x.ProviderSpaceId == original.ID &&
                        validProviderSpaces.Any(p => p.ID == x.ReplacementProviderSpaceId) &&
                        AirRuleMatches(x.AirRuleSet, recommendation));

                if (target != null)
                {
                    return validProviderSpaces.First(p => p.ID == target.ReplacementProviderSpaceId);
                }
            }

            return original;
        }

        /// <summary>
        /// For most purposes override ApplyNegativeCustomRuleMatchCriteria or ApplyPostiveCustomRuleMatchCriteria
        /// </summary>
        protected virtual bool AirRuleMatches(AirRuleSet airRuleSet, FlightRecommendation flightRecommendation)
        {
            if (airRuleSet == null)
                return true; //if null, predicate defaults to true, otherwise defaults to false.

            var segments = flightRecommendation.Legs.SelectMany(x => x.Segments).ToListBuffered();

            //negative overrides positives

            #region Negative Section

            if (
                airRuleSet.MarketingAirlineRules.Where(x => x.Alignment == AlignmentType.Negative).Any(
                    ma =>
                    segments.Select(x => x.MarketingAirline == null ? string.Empty : x.MarketingAirline.Code)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return false;

            if (
                airRuleSet.OperatingAirlineRules.Where(x => x.Alignment == AlignmentType.Negative).Any(
                    ma =>
                    segments.Select(x => x.OperatingAirline == null ? string.Empty : x.OperatingAirline.Code)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return false;

            if (
                airRuleSet.DestinationCountryCodeRules.Where(x => x.Alignment == AlignmentType.Negative).Any(
                    ma =>
                    segments.Select(x => x.ArrivalAirport.City.CountryCode)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return false;

            if (
                airRuleSet.DepartureCountryCodeRules.Where(x => x.Alignment == AlignmentType.Negative).Any(
                    ma =>
                    segments.Select(x => x.DepartureAirport.City.CountryCode)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return false;

            //if (
            //    airRuleSet.ClassOfServiceRules.Where(x => x.Alignment == AlignmentType.Negative).Any(
            //        ma =>
            //        flightRecommendation.PaxFareQualifiers.Select(x => x.ClassOfService)
            //                .Any(
            //                    a =>
            //                    a.Equals(ma.Value, StringComparison.InvariantCultureIgnoreCase))))
            //    return false;

            var customResult = ApplyNegativeCustomRuleMatchCriteria(airRuleSet, flightRecommendation);

            if (customResult.HasValue)
                return customResult.Value;

            #endregion

            #region Positive Section

            if (
                airRuleSet.MarketingAirlineRules.Where(x => x.Alignment == AlignmentType.Positive).Any(
                    ma =>
                    segments.Select(x => x.MarketingAirline == null ? string.Empty : x.MarketingAirline.Code)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return true;

            if (
                airRuleSet.OperatingAirlineRules.Where(x => x.Alignment == AlignmentType.Positive).Any(
                    ma =>
                    segments.Select(x => x.OperatingAirline == null ? string.Empty : x.OperatingAirline.Code)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return true;

            if (
                airRuleSet.DestinationCountryCodeRules.Where(x => x.Alignment == AlignmentType.Positive).Any(
                    ma =>
                    segments.Select(x => x.ArrivalAirport.City.CountryCode)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return true;

            if (
                airRuleSet.DepartureCountryCodeRules.Where(x => x.Alignment == AlignmentType.Positive).Any(
                    ma =>
                    segments.Select(x => x.DepartureAirport.City.CountryCode)
                            .Any(
                                a =>
                                a.Equals(ma.Value, StringComparison.OrdinalIgnoreCase))))
                return true;

            //if (
            //    airRuleSet.ClassOfServiceRules.Where(x => x.Alignment == AlignmentType.Positive).Any(
            //        ma =>
            //        flightRecommendation.PaxFareQualifiers.Select(x => x.ClassOfService)
            //                .Any(
            //                    a =>
            //                    a.Equals(ma.Value, StringComparison.InvariantCultureIgnoreCase))))
            //    return true;

            customResult = ApplyPositiveCustomRuleMatchCriteria(airRuleSet, flightRecommendation);

            if (customResult.HasValue)
                return customResult.Value;

            #endregion

            return false; //default negative
        }

        /// <summary>
        /// For injection of custom code without overriding the main function for most purposes.
        /// </summary>
        protected virtual bool? ApplyNegativeCustomRuleMatchCriteria(AirRuleSet airRuleSet,
            FlightRecommendation flightRecommendation)
        {
            return null;
        }

        /// <summary>
        /// For injection of custom code without overriding the main function for most purposes.
        /// </summary>
        protected virtual bool? ApplyPositiveCustomRuleMatchCriteria(AirRuleSet airRuleSet,
            FlightRecommendation flightRecommendation)
        {
            return null;
        }

        #endregion
    }
}
