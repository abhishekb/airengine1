﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Logging;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.FareQuote;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using Tavisca.TravelNxt.Flight.Settings;
using PassengerType = Tavisca.TravelNxt.Flight.Entities.PassengerType;

namespace Tavisca.TravelNxt.Flight.Price.Core
{
    public class PriceProvider : IPriceProvider
    {
        protected virtual string LogEntryTitle { get { return "Price Call to supplier: {0}"; } }

        public virtual FlightRecommendation Price(FlightPriceRQ request, FlightRecommendation recommendation, ProviderSpace providerSpace)
        {
            var eventEntry = Utility.GetEventEntryContextual();

            eventEntry.Title = string.Format(LogEntryTitle, providerSpace.Family);

            eventEntry.CallType = KeyStore.CallTypes.SupplierAirPrice;

            var logFactory = Utility.GetLogFactory();

            var stopwatch = new WrappedTimer();
            try
            {
                eventEntry.AddAdditionalInfo(KeyStore.LogCategories.AdditionalInfoKeys.ProviderName,
                    providerSpace.Family);

                eventEntry.ProviderId = providerSpace.ID;

                var dataContract = ToDataContract(request, recommendation, providerSpace);

                eventEntry.RequestObject = dataContract;
                eventEntry.ReqResSerializerType = SerializerType.DataContractSerializer;

                stopwatch.Start();

                var result = GetResponse(dataContract, providerSpace);
                
                if (result != null)
                    eventEntry.ResponseObject = result;

                if (result == null || result.ResponseStatus == StatusType.Failure)
                    return null;

                return ToEntity(result, recommendation);
            }
            catch (Exception ex)
            {
                stopwatch.Stop();
                eventEntry.TimeTaken = stopwatch.ElapsedSeconds;

                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
            finally
            {
                logFactory.WriteAsync(eventEntry, KeyStore.LogCategories.Core);
            }
        }

        protected virtual FareQuoteResponse GetResponse(FareQuoteRequest request, ProviderSpace providerSpace)
        {
            var priceConfiguration =
                        providerSpace.Configurations.First(
                            x => x.ConfigType == ConfigurationType.Price);

            using (var client = new FareQuoteServiceClient(KeyStore.ClientEndpoints.FareQuoteService,
                priceConfiguration.ServiceUri.ToString()))
            {
                return client.GetFareQuote(request);
            }
        }

        #region Translations

        #region ToContract

        public static FareQuoteRequest ToDataContract(FlightPriceRQ request, FlightRecommendation recommendation,
            ProviderSpace providerSpace)
        {
            var context = Utility.GetCurrentContext();

            var tuple = ToDataContract(recommendation);

            var contract = new FareQuoteRequest()
                {
                    Culture = context.Culture.ToString(),
                    FareSpace = ToDataContract(providerSpace),
                    Requester = ToDataContract(request.Requester),
                    PassengerTypeQuantities = tuple.Item2, //TODO1
                    ItineraryRecommendation = tuple.Item1,
                    FareQuoteStrategy = string.Empty, //TODO1: what to do here?
                    FareRestrictions = null, //TODO1: what to do here?
                    PricingOverrides = null //TODO1: what to do here?
                };

            return contract;
        }

        public static FareSpace ToDataContract(ProviderSpace providerSpace)
        {
            return new FareSpace()
            {
                Code = "123",
                AdditionalParams = AdditionalInfoDictionary.ToDictionary(providerSpace.AdditionalInfo),
                Qualifiers = new FareSpaceQualifiers()
                {
                    //TODO1: fill this object.
                }
            };
        }

        public static Tuple<ItineraryRecommendation, PassengerTypeQuantity[]> ToDataContract(FlightRecommendation recommendation)
        {
            var pQuantity = recommendation.Fare.PassengerFares.Select(x => new PassengerTypeQuantity()
                {
                    Quantity = x.Quantity,
                    Passenger = new Passenger()
                        {
                            PassengerType = ToDataContract(x.PassengerType),
                            MappedPaxTypeCode = string.Empty, //TODO1
                            Age = x.PassengerType == PassengerType.Adult ? 25 :
                            x.PassengerType == PassengerType.Child ? 5 :
                            x.PassengerType == PassengerType.Infant ? 1 :
                            x.PassengerType == PassengerType.Senior ? 70 : 25

                        }
                });

            var iRecommendation = new ItineraryRecommendation()
                {
                    LegRecommendations = recommendation.Legs.Select(x => new LegRecommendation()
                        {
                            QuoteFlights = x.Segments.Select(y => new QuoteFlight()
                                {
                                    //Cabin = ToDataContract(recommendation.PaxFareQualifiers[0].CabinClass),
                                    //ClassOfService = recommendation.PaxFareQualifiers[0].ClassOfService,
                                    //FareBasis = recommendation.PaxFareQualifiers[0].FareBasisCode,
                                    Flight = new Data.Services.FareQuote.Flight()
                                        {
                                            AirlineCode = y.MarketingAirline.Code,
                                            ArrivalDateTime = y.ArrivalDateTime,
                                            ArrivalLocation = y.ArrivalAirport.Code,
                                            DepartureDateTime = y.DepartureDateTime,
                                            DepartureLocation = y.DepartureAirport.Code,
                                            BaggageAllowance = string.Empty, //TODO1
                                            FlightNumber = y.FlightNumber,
                                            OperatingAirlineCode = y.OperatingAirline == null ? string.Empty : y.OperatingAirline.Code
                                        }
                                }).ToArray()
                        }).ToArray()
                };

            return new Tuple<ItineraryRecommendation, PassengerTypeQuantity[]>(iRecommendation, pQuantity.ToArray());
        }

        public static Data.Services.FareQuote.CabinType ToDataContract(Entities.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.CabinType.Business:
                    return Data.Services.FareQuote.CabinType.Business;
                case Entities.CabinType.Economy:
                    return Data.Services.FareQuote.CabinType.Economy;
                case Entities.CabinType.First:
                    return Data.Services.FareQuote.CabinType.First;
                case Entities.CabinType.PremiumEconomy:
                    return Data.Services.FareQuote.CabinType.PremiumEconomy;
                case Entities.CabinType.Unknown:
                default:
                    return Data.Services.FareQuote.CabinType.Unknown;
            }
        }

        public static Data.Services.FareQuote.PassengerType ToDataContract(Entities.PassengerType passengerType)
        {
            switch (passengerType)
            {
                case Entities.PassengerType.Adult:
                    return Data.Services.FareQuote.PassengerType.Adult;
                case Entities.PassengerType.Child:
                    return Data.Services.FareQuote.PassengerType.Child;
                case Entities.PassengerType.Infant:
                    return Data.Services.FareQuote.PassengerType.InfantInLap;
                case Entities.PassengerType.Senior:
                    return Data.Services.FareQuote.PassengerType.Senior;
                default:
                    return Data.Services.FareQuote.PassengerType.Adult;
            } //TODO1: military and government, consider, also change in toentity function
        }

        public static Data.Services.FareQuote.Requester ToDataContract(Entities.Requester requester)
        {
            //TODO1: fill security info
            var contract = new Data.Services.FareQuote.Requester()
            {
                AffiliateId = requester.ID.ToString(CultureInfo.InvariantCulture),
                ParentId = null,
                SecurityToken = null,
                ApplicationIpAddress = null,
                TrackingId = null,
                UserId = null,
                UserIpAddress = null,
                UserSessionId = null
            };

            return contract;
        }

        #endregion

        #region ToEntity

        public static FlightRecommendation ToEntity(FareQuoteResponse response, FlightRecommendation recommendation)
        {
            recommendation.Fare.PassengerFares.Clear();

            recommendation.Fare.PassengerFares = response.Fares.Select(x => new PassengerFare()
                {
                    BaseAmount = ToEntity(x.BaseAmount),
                    PassengerType = ToEntity(x.PassengerTypeQuantity.Passenger.PassengerType),
                    Quantity = x.PassengerTypeQuantity.Quantity,
                    Taxes = x.Taxes == null ? new List<Entities.FareComponent>() : x.Taxes.Select(ToEntity).ToListBuffered(),
                    Discounts = x.Discounts == null ? new List<Entities.FareComponent>() : x.Discounts.Select(ToEntity).ToListBuffered(),
                    Fees = x.Fees == null ? new List<Entities.FareComponent>() : x.Fees.Select(ToEntity).ToListBuffered(),
                }).ToListBuffered();

            return recommendation;
        }

        public static Entities.Money ToEntity(Data.Services.FareQuote.Money money)
        {
            var entity = new Entities.Money(money.Amount, money.CurrencyCode);

            return entity;
        }

        public static Entities.FareComponent ToEntity(Data.Services.FareQuote.FareComponent fareComponent)
        {
            return new Entities.FareComponent()
            {
                Code = fareComponent.Code,
                Description = fareComponent.Description,
                Value = new Entities.Money(fareComponent.Amount, fareComponent.CurrencyCode),
                FareComponentType = ToEntity(fareComponent.Type)
            };
        }

        public static Entities.FareComponentType ToEntity(Data.Services.FareQuote.FareComponentType fareComponentType)
        {
            switch (fareComponentType)
            {
                case Data.Services.FareQuote.FareComponentType.Discount:
                    return Entities.FareComponentType.Discount;
                case Data.Services.FareQuote.FareComponentType.CommissionAmount:
                    return Entities.FareComponentType.Commision;
                case Data.Services.FareQuote.FareComponentType.Fee:
                    return Entities.FareComponentType.Fees;
                case Data.Services.FareQuote.FareComponentType.Tax:
                    return Entities.FareComponentType.Tax;
                case Data.Services.FareQuote.FareComponentType.Penalty:
                case Data.Services.FareQuote.FareComponentType.EquivalentAmount:
                default:
                    return default(Entities.FareComponentType);
            }
        }

        public static Entities.CabinType ToEntity(Data.Services.FareSearch.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Data.Services.FareSearch.CabinType.Business:
                    return Entities.CabinType.Business;
                case Data.Services.FareSearch.CabinType.Economy:
                    return Entities.CabinType.Economy;
                case Data.Services.FareSearch.CabinType.First:
                    return Entities.CabinType.First;
                case Data.Services.FareSearch.CabinType.PremiumEconomy:
                    return Entities.CabinType.PremiumEconomy;
                case Data.Services.FareSearch.CabinType.Unknown:
                    return Entities.CabinType.Unknown;
                default:
                    return Entities.CabinType.Unknown;
            }
        }

        public static Entities.PassengerType ToEntity(Data.Services.FareQuote.PassengerType passengerType)
        {
            switch (passengerType)
            {
                case Data.Services.FareQuote.PassengerType.Adult:
                    return Entities.PassengerType.Adult;
                case Data.Services.FareQuote.PassengerType.Child:
                    return Entities.PassengerType.Child;
                case Data.Services.FareQuote.PassengerType.InfantInLap:
                    return Entities.PassengerType.Infant;
                case Data.Services.FareQuote.PassengerType.InfantWithSeat:
                    return Entities.PassengerType.Child;
                case Data.Services.FareQuote.PassengerType.Military:
                    return Entities.PassengerType.Adult;
                case Data.Services.FareQuote.PassengerType.Senior:
                    return Entities.PassengerType.Senior;
                case Data.Services.FareQuote.PassengerType.Youth:
                    return Entities.PassengerType.Adult;
                case Data.Services.FareQuote.PassengerType.Government:
                    return Entities.PassengerType.Adult;
                default:
                    return Entities.PassengerType.Adult;
            }
        }

        #endregion

        #endregion
    }
}
