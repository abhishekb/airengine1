﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Repositories;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Price.Core.Configuration
{
    public class FareSourcePriceConfigurationCollection : IFareSourcePriceConfigurationCollection
    {
        #region Private Members

        private List<PriceConfiguration> _priceConfigurations;

        #region Static Members

        private ICacheProvider _cacheProvider;
        protected ICacheProvider CacheProvider
        {
            get
            {
                return _cacheProvider ??
                       (_cacheProvider =
                        RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.Settings));
            }
        }

        #endregion

        #endregion

        #region Constructors

        public FareSourcePriceConfigurationCollection()
        {
            LoadAll();
        }

        #endregion

        #region IFareSourcePriceConfigurationCollection Members

        public void LoadAll()
        {

            _priceConfigurations = CacheProvider.Get<List<PriceConfiguration>>(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.AirlineOperatingAirports);

            if (_priceConfigurations != null)
                return;

            //Getuncached data
            _priceConfigurations = GetUnCachedData();

            CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.PriceConfigurationsCacheKey,
                                     _priceConfigurations,
                                     new ExpirationSettings()
                                         {
                                             ExpirationType = ExpirationType.SlidingExpiration,
                                             SlidingExpirationInterval =
                                                 new TimeSpan(
                                                 TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                         });
        }

        public virtual PriceConfiguration GetConfiguration(int posId, long accountId)
        {
            var configuration = this.FirstOrDefault(x => x.PosId.HasValue && x.PosId == posId &&
                x.AccountId.HasValue && x.AccountId == accountId);

            if (configuration != null)
                return configuration;

            configuration = this.FirstOrDefault(x => x.PosId.HasValue && x.PosId == posId);

            if (configuration != null)
                return configuration;

            configuration = this.FirstOrDefault(x => x.AccountId.HasValue && x.AccountId == accountId);

            if (configuration != null)
                return configuration;

            configuration = this.FirstOrDefault(x => !x.AccountId.HasValue && !x.PosId.HasValue);

            if (configuration != null)
                return configuration;

            return GetDefault();
        }

        public virtual PriceConfiguration GetDefault()
        {
            return new PriceConfiguration()
                {
                    Id = -1,
                    IsFailoverEnabled = false
                };
        }

        #region ICollection<PriceConfiguration> Members

        public IEnumerator<PriceConfiguration> GetEnumerator()
        {
            return _priceConfigurations.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _priceConfigurations.GetEnumerator();
        }

        public void Add(PriceConfiguration item)
        {
            _priceConfigurations.Add(item);
        }

        public void Clear()
        {
            _priceConfigurations.Clear();
        }

        public bool Contains(PriceConfiguration item)
        {
            return _priceConfigurations.Contains(item);
        }

        public void CopyTo(PriceConfiguration[] array, int arrayIndex)
        {
            _priceConfigurations.CopyTo(array, arrayIndex);
        }

        public bool Remove(PriceConfiguration item)
        {
            return _priceConfigurations.Remove(item);
        }

        public int Count { get { return _priceConfigurations.Count; } }
        public bool IsReadOnly { get { return true; } }

        #endregion

        #endregion

        #region Protected Members

        protected virtual List<PriceConfiguration> GetUnCachedData()
        {
            var factory = RuntimeContext.Resolver.Resolve<IRepositoryFactory>();

            using (var repository = factory.GetRepository(KeyStore.Repositories.AirConfigurationReadOnly))
            {
                var configurations = repository.All<AirPriceConfiguration>()
                    .Include(x => x.AirPriceConfigurationFareSources)
                    .Include(x => x.AirPriceConfigurationFareSourceReplacements)
                    .Include(x => x.AirPriceConfigurationFareSourceReplacements.Select(y => y.AirRule)).ToListBuffered();

                return configurations.Select(ToPriceConfiguration).Where(x => x != null).ToListBuffered();
            }
        }

        protected virtual PriceConfiguration ToPriceConfiguration(AirPriceConfiguration airPriceConfiguration)
        {
            if (airPriceConfiguration == null)
                return null;

            var priceConfiguration = new PriceConfiguration()
                {
                    Id = airPriceConfiguration.Id,
                    PosId = airPriceConfiguration.PosId,
                    IsFailoverEnabled = airPriceConfiguration.IsFailoverEnabled,
                    FareSourcePriceConfigurations = airPriceConfiguration.AirPriceConfigurationFareSources
                                                    .Select(ToFareSourcePriceConfiguration).Where(x => x != null)
                                                    .ToListBuffered(),
                    FareSourceReplacementPriceConfigurations = airPriceConfiguration.AirPriceConfigurationFareSourceReplacements
                                                    .Select(ToFareSourcePriceConfigurationReplacement).Where(x => x != null)
                                                    .ToListBuffered()
                };

            long accountId;
            if (!string.IsNullOrWhiteSpace(airPriceConfiguration.AccountId) &&
                long.TryParse(airPriceConfiguration.AccountId, out accountId))
                priceConfiguration.AccountId = accountId;

            return priceConfiguration;
        }

        protected virtual ProviderSpacePriceConfiguration ToFareSourcePriceConfiguration(
            AirPriceConfigurationFareSource airPriceConfigurationFareSource)
        {
            if (airPriceConfigurationFareSource == null)
                return null;

            var providerspacePriceConfiguration = new ProviderSpacePriceConfiguration()
                {
                    Id = airPriceConfigurationFareSource.Id,
                    PriceConfigurationId = airPriceConfigurationFareSource.AirPriceConfigurationId,
                    ProviderSpaceId = airPriceConfigurationFareSource.FareSourceId,
                    PreferenceOrder = airPriceConfigurationFareSource.PreferenceOrder
                };

            return providerspacePriceConfiguration;
        }

        protected virtual ProviderSpaceReplacementPriceConfiguration ToFareSourcePriceConfigurationReplacement(
            AirPriceConfigurationFareSourceReplacement airPriceConfigurationFareSourceReplacement)
        {
            if (airPriceConfigurationFareSourceReplacement == null)
                return null;

            var providerspaceReplacement = new ProviderSpaceReplacementPriceConfiguration()
                {
                    Id = airPriceConfigurationFareSourceReplacement.Id,
                    PriceConfigurationId = airPriceConfigurationFareSourceReplacement.AirPriceConfigurationId,
                    ProviderSpaceId = airPriceConfigurationFareSourceReplacement.FareSourceId,
                    ReplacementProviderSpaceId = airPriceConfigurationFareSourceReplacement.ReplacementFaresourceId,
                    RuleId = airPriceConfigurationFareSourceReplacement.RuleId,
                    AirRuleSet = airPriceConfigurationFareSourceReplacement.AirRule == null
                                  ? null
                                  : Common.AirRuleSet.FromXML(airPriceConfigurationFareSourceReplacement.AirRule.RuleSet)
                };

            return providerspaceReplacement;
        }

        #endregion
    }
}
