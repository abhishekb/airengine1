﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.Services.FareQuote;

namespace Tavisca.TravelNxt.Flight.Price.Core
{
    public class MockPriceProvider : PriceProvider
    {
        protected override string LogEntryTitle
        {
            get { return "Price Call to mock supplier: {0}"; }
        }

        protected override FareQuoteResponse GetResponse(FareQuoteRequest request, Entities.ProviderSpace providerSpace)
        {
            //System.Threading.Thread.Sleep(new Random(Guid.NewGuid().ToString().GetHashCode()).Next(500, 4000));

            return new FareQuoteResponse() //TODO1: do better
                {
                    ResponseStatus = StatusType.Success,
                    Fares = new [] {
                        new Fare()
                        {
                          BaseAmount  = new Money(){ Amount = new Random(Guid.NewGuid().ToString().GetHashCode()).Next(50, 200), CurrencyCode = "USD"},
                          Discounts = null,
                          Fees = null,
                          Taxes = null,
                          PassengerTypeQuantity = request.PassengerTypeQuantities.First()
                        }
                    }
                };
        }
    }
}
