﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions;

namespace Tavisca.TravelNxt.Engines.WebAPI.FaultContract
{
    public class ApplicationFault
    {

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public static ApplicationFault UnknownError
        {
            get
            {
                return new ApplicationFault
                {
                    ErrorCode = ErrorCodes.UNKNOWN_ERROR,
                    ErrorMessage = "Unknown Error occurred. Please contact support."
                };
            }
        }
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("ErrorCode=");
            stringBuilder.Append(ErrorCode);
            stringBuilder.Append("\n");
            stringBuilder.Append("ErrorMessage=");
            stringBuilder.Append(ErrorMessage);
            return stringBuilder.ToString();
        }
    }
}
