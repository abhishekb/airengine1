﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
//using FluentValidation.Attributes;

using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_Requester")]
    public class Requester
    {
        /// <summary>
        /// This is requester ID. Like Affiliate ID or Agency ID from a Hierarchy.
        /// </summary>
        public long ID
        {
            get
            {
                return Utility.GetCurrentContext().AccountId;
            }
        }
        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        public int POSID
        {
            get
            {
                return Utility.GetCurrentContext().PosId;
            }
        }

        /// <summary>
        /// This is to indicate if requester is coming on current POS from different origin/site.
        /// </summary>
        [DataMember]
        public string CustomerType { get; set; }

        #region Translation

        public static Requester ToDataContract(Entities.Requester requester)
        {
            var contract = new Requester()
                {
                    AdditionalInfo = new Dictionary<string, string>(),
                   
                    // Don't have exact support in engines
                   //RequesterOrigin = requester.
                };

            return contract;
        }

        public static Entities.Requester ToEntity(Requester requester)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
