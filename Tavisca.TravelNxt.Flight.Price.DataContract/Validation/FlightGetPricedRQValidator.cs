﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Tavisca.TravelNxt.Common.Security.Encryption;

namespace Tavisca.TravelNxt.Flight.Price.DataContract.Validation
{
    public class FlightGetPricedRQValidator : AbstractValidator<FlightGetPricedRQ>
    {
        public FlightGetPricedRQValidator()
        {
            RuleFor(x => x.ExternalIdentifier).NotEmpty()
                .Must(EncryptionManager.IsEncrypted)
                .WithMessage("The provided string is not properly formatted.");
        }

        public override ValidationResult Validate(FlightGetPricedRQ instance)
        {
            if (instance == null)
                return new ValidationResult(new List<ValidationFailure>(1){ new ValidationFailure("instance", "request cannot be null") });

            return base.Validate(instance);
        }
    }
}
