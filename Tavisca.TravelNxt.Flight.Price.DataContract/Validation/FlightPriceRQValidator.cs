﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;

namespace Tavisca.TravelNxt.Flight.Price.DataContract.Validation
{
    public class FlightPriceRQValidator : AbstractValidator<FlightPriceRQ>
    {
        public FlightPriceRQValidator()
        {
            RuleFor(x => x.RecommendationRefID).GreaterThan(0);
        }

        public override ValidationResult Validate(FlightPriceRQ instance)
        {
            if (instance == null)
                return new ValidationResult(new List<ValidationFailure>(1){ new ValidationFailure("instance", "request cannot be null") });

            return base.Validate(instance);
        }
    }
}
