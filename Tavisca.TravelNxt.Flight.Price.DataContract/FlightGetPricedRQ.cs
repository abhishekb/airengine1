﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Price.DataContract.Validation;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_FlightGetPricedRQ")]
    [Validator(typeof(FlightGetPricedRQValidator))]
    public class FlightGetPricedRQ
    {
        [DataMember]
        public string ExternalIdentifier { get; set; }

        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        [DataMember]
        public Requester Requester { get; set; }

        public static Entities.Pricing.FlightGetPricedRQ ToEntity(FlightGetPricedRQ request)
        {
            return new Entities.Pricing.FlightGetPricedRQ()
            {
                ExternalIdentifier = request.ExternalIdentifier,
                AdditionalInfo = new AdditionalInfoDictionary(request.AdditionalInfo),
                Requester = Requester.ToEntity(request.Requester)
            };
        }
    }
}
