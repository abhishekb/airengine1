﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_PricedRecommendation")]
    public class PricedRecommendation
    {
        [DataMember]
        public int RefId { get; set; }

        [DataMember]
        public List<FlightLeg> Legs { get; set; }
        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }
        [DataMember]
        public ProviderSpace ProviderSpace { get; set; }

        [DataMember]
        public ItineraryTypeOptions ItineraryType { get; set; }

        //TODO1: Decide on FlightFare in Pricing response
        [DataMember]
        public FlightFare FlightFare { get; set; }

        public static PricedRecommendation ToDataContract(FlightRecommendation recommendation)
        {
            if (recommendation == null)
                return null;

            var datacontract = new PricedRecommendation()
                                   {
                                       Legs = recommendation.Legs.Select(FlightLeg.ToDataContract).ToListBuffered(),
                                       AdditionalInfo = AdditionalInfoDictionary.ToDictionary(recommendation.AdditionalInfo),
                                       RefId = recommendation.RefId,
                                       ProviderSpace = ProviderSpace.ToDataContract(recommendation.ProviderSpace),
                                       ItineraryType = EnumParsers.ToDataContract(recommendation.ItineraryType),
                                       FlightFare = FlightFare.ToDataContract(recommendation.Fare)
                                       //TODO1: Fare Translation here
                                   };

            return datacontract;
        }
    }
}
