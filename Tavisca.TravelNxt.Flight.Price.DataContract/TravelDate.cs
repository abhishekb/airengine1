﻿using System;
using System.Runtime.Serialization;
////using FluentValidation.Attributes;


namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_TravelDate")]
    
    public class TravelDate
    {
        [DataMember]
        public DateTime DateTime { get; set; }
        [DataMember]
        public int TimeWindow { get; set; }
        [DataMember]
        public bool AnyTime { get; set; }

        #region Translation

        public static TravelDate ToDataContract(Entities.TravelDate travelDate)
        {
            var contract = new TravelDate()
                {
                    AnyTime = travelDate.AnyTime,
                    DateTime = travelDate.DateTime,
                    TimeWindow = travelDate.TimeWindow
                };

            return contract;
        }

        public static Entities.TravelDate ToEntity(TravelDate travelDate)
        {
            var entity = new Entities.TravelDate()
            {
                AnyTime = travelDate.AnyTime,
                DateTime = travelDate.DateTime,
                TimeWindow = travelDate.TimeWindow
            };

            return entity;
        }

        #endregion
    }
}