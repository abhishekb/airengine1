﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_ProviderSpace")]
    public class ProviderSpace
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }

        #region Translation

        public static ProviderSpace ToDataContract(Entities.ProviderSpace providerSpace)
        {
            var contract = new ProviderSpace()
                {
                    ID = providerSpace.ID,
                    Name = providerSpace.Name
                };

            return contract;
        }

        //public static Entities.ProviderSpace ToEntity(ProviderSpace providerSpace, Requester requester)
        //{
        //    var entity = new Entities.ProviderSpace(providerSpace.ID, providerSpace.Name, Requester.ToEntity(requester), null, null, null);

        //    return entity;
        //}

        #endregion
    }
}
