﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com")]
    public class SegmentStop
    {
        [DataMember]
        public Airport Airport { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public string ArrivalTerminal { get; set; }
        [DataMember]
        public string DepartureTerminal { get; set; }
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }
        [DataMember]
        public DateTime DepartureDateTime { get; set; }

        #region Translation

        public static SegmentStop ToDataContract(Entities.SegmentStop flightSegment)
        {
            if (flightSegment == null)
                return null;

            return new SegmentStop()
            {
                Airport = Airport.ToDataContract(flightSegment.Airport),
                ArrivalDateTime = flightSegment.ArrivalDateTime,
                ArrivalTerminal = flightSegment.ArrivalTerminal,
                DepartureDateTime = flightSegment.DepartureDateTime,
                DepartureTerminal = flightSegment.DepartureTerminal,
                Duration = flightSegment.Duration
            };
        }

        #endregion
    }
}
