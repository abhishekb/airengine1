﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities.Pricing;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_FlightPriceRS")]
    public class FlightPriceRS
    {
        [DataMember]
        public PricedRecommendation PricedRecommendation { get; set; }
        [DataMember]
        public string ExternalIdentifier { get; set; }
        [DataMember]
        public CallStatus CallStatus { get; set; }
        [DataMember]
        public string StatusCode { get; set; }
        [DataMember]
        public string Message { get; set; }

        public static FlightPriceRS ToDataContract(FlightPriceResult result)
        {
            var flightPriceRS = new FlightPriceRS()
                                    {
                                        CallStatus = EnumParsers.ToDataContract(result.Status),
                                        ExternalIdentifier = result.ExternalIdentifier,
                                        Message = result.Message,
                                        PricedRecommendation = PricedRecommendation.ToDataContract(result.Recommendation),
                                        StatusCode = result.StatusCode
                                    };
            return flightPriceRS;
        }

    }
}
