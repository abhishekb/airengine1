﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_SortingOrder")]
    public enum SortingOrder
    {
        [EnumMember]
        ByDepartureTime,
        [EnumMember]
        ByArrivalTime,
        [EnumMember]
        ByJourneyTime,
        [EnumMember]
        ByPriceHighToLow,
        [EnumMember]
        ByPriceLowToHigh,
        [EnumMember]
        ByAirLine,
        [EnumMember]
        ByPreference
    }
}
