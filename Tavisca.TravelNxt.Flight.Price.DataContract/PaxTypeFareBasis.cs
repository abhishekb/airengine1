﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_PaxTypeFareBasis")]
    public class PaxTypeFareBasis
    {
        [DataMember]
        public string FareBasisCode { get; set; }

        [DataMember]
        public PassengerType PassengerType { get; set; }

        #region Translators

        public static PaxTypeFareBasis ToDataContract(Entities.PaxTypeFareBasis paxFareQualifier)
        {
            return new PaxTypeFareBasis()
            {
                FareBasisCode = paxFareQualifier.FareBasisCode,
                PassengerType = PassengerFare.ToDataContract(paxFareQualifier.PassengerType)
            };
        }

        public static Entities.PaxTypeFareBasis ToEntity(PaxTypeFareBasis paxTypeFareBasis)
        {
            if (paxTypeFareBasis == null)
                return null;

            return new Entities.PaxTypeFareBasis()
                       {
                           FareBasisCode = paxTypeFareBasis.FareBasisCode,
                           PassengerType = PassengerFare.ToEntity(paxTypeFareBasis.PassengerType)
                       };
        }

        #endregion
    }

}
