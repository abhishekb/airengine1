﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities.Pricing;
using FluentValidation.Attributes;
using Tavisca.TravelNxt.Flight.Price.DataContract.Validation;

//using FluentValidation.Attributes;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_FlightPriceRQ")]
    [Validator(typeof(FlightPriceRQValidator))]
    public class FlightPriceRQ
    {
        [DataMember]
        public int RecommendationRefID { get; set; }

        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        [DataMember]
        public Requester Requester { get; set; }

        public static Entities.Pricing.FlightPriceRQ ToEntity(FlightPriceRQ request)
        {
            return new Entities.Pricing.FlightPriceRQ()
                                       {
                                           RecommendationRefID = request.RecommendationRefID,
                                           AdditionalInfo = new AdditionalInfoDictionary(request.AdditionalInfo),
                                           Requester = Requester.ToEntity(request.Requester)
                                       };
        }
    }
}
