﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{

    [DataContract(Namespace = "http://tavisca.com", Name = "P_FareType")]
    public enum FareType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Published,
        [EnumMember]
        Negotiated
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "P_CabinType")]
    public enum CabinType
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        First,
        [EnumMember]
        Business,
        [EnumMember]
        PremiumEconomy,
        [EnumMember]
        Economy

    }

    [DataContract(Namespace = "http://tavisca.com", Name = "P_FareComponentType")]
    public enum FareComponentType
    {
        [EnumMember]
        Tax,
        [EnumMember]
        Fees,
        [EnumMember]
        Markup,
        [EnumMember]
        Discount,
        [EnumMember]
        Commision
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "P_PassengerType")]
    public enum PassengerType
    {
        [EnumMember]
        Infant,
        [EnumMember]
        Child,
        [EnumMember]
        Adult,
        [EnumMember]
        Senior
    }

    [DataContract(Namespace = "http://tavisca.com", Name = "P_ItineraryTypeOptions")]
    public enum ItineraryTypeOptions
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        MultiCity = 1,
        [EnumMember]
        OneWay = 2,
        [EnumMember]
        RoundTrip = 3

    }

    [DataContract(Namespace = "http://tavisca.com", Name = "P_CallStatus")]
    public enum CallStatus
    {
        [EnumMember]
        Success = 0,
        [EnumMember]
        Failure = 1,
        [EnumMember]
        Warning = 2,
        [EnumMember]
        InProgress = 3
    }

    internal class EnumParsers
    {
        internal static ItineraryTypeOptions ToDataContract(Entities.ItineraryTypeOptions options)
        {
            switch (options)
            {
                case Entities.ItineraryTypeOptions.NotSet:
                    return ItineraryTypeOptions.NotSet;

                case Entities.ItineraryTypeOptions.MultiCity:
                    return ItineraryTypeOptions.MultiCity;

                case Entities.ItineraryTypeOptions.OneWay:
                    return ItineraryTypeOptions.OneWay;

                case Entities.ItineraryTypeOptions.RoundTrip:
                    return ItineraryTypeOptions.RoundTrip;
                default:
                    return ItineraryTypeOptions.NotSet;
            }
        }


        internal static CallStatus ToDataContract(Entities.CallStatus callStatus)
        {
            switch (callStatus)
            {
                case Entities.CallStatus.Failure:
                    return CallStatus.Failure;
                case Entities.CallStatus.InProgress:
                    return CallStatus.InProgress;
                case Entities.CallStatus.Success:
                    return CallStatus.Success;
                case Entities.CallStatus.Warning:
                    return CallStatus.Warning;
                default:
                    return CallStatus.Failure;
            }
        }
    }
}
