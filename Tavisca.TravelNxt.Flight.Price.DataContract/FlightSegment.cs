﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Price.DataContract
{
    [DataContract(Namespace = "http://tavisca.com", Name = "P_FlightSegment")]
    public class FlightSegment
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public string FlightNumber { get; set; }

        [DataMember]
        public string ClassOfService { get; set; }

        [DataMember]
        public CabinType CabinType { get; set; }

        [DataMember]
        public IList<PaxTypeFareBasis> PaxTypeFareBasisCodes { get; set; }

        [DataMember]
        public Airline MarketingAirline { get; set; }
        //[DataMember]
        //public string AirlineFlightNumber { get; set; }
        [DataMember]
        public Airline OperatingAirline { get; set; }
        [DataMember]
        public string CodeShareText { get; set; }
        [DataMember]
        public string AircraftType { get; set; }

        // Travel dates and points
        [DataMember]
        public Airport DepartureAirport { get; set; }
        [DataMember]
        public Airport ArrivalAirport { get; set; }
        [DataMember]
        public string DepartureTerminal { get; set; }
        [DataMember]
        public string ArrivalTerminal { get; set; }
        [DataMember]
        public DateTime DepartureDateTime { get; set; }
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }

        /// <summary>
        /// Number of no plane change stops
        /// </summary>
        [DataMember]
        public List<SegmentStop> InSegmentStops { get; set; }

        /// <summary>
        /// Whether the particular flight in the pricing recommendation can be eticketed
        /// </summary>
        [DataMember]
        public bool CanETicket { get; set; }

        [DataMember]
        public int DurationMinutes { get; set; }
        [DataMember]
        public int DistanceTraveledKms { get; set; }

        [DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        #region Translation

        public static FlightSegment ToDataContract(Entities.FlightSegment flightSegment)
        {
            var paxTypeFareBasis = flightSegment.PaxTypeFareBasisCodes.First();
            
            return new FlightSegment()
                       {
                           Key = flightSegment.Key,
                           AdditionalInfo = AdditionalInfoDictionary.ToDictionary(flightSegment.AdditionalInfo),
                           FlightNumber = flightSegment.FlightNumber,
                           MarketingAirline = Airline.ToDataContract(flightSegment.MarketingAirline),
                           OperatingAirline = Airline.ToDataContract(flightSegment.OperatingAirline),
                           CodeShareText = flightSegment.CodeShareText,
                           ArrivalAirport = Airport.ToDataContract(flightSegment.ArrivalAirport),
                           DepartureAirport = Airport.ToDataContract(flightSegment.DepartureAirport),
                           CanETicket = flightSegment.CanETicket,
                           DurationMinutes = flightSegment.DurationMinutes,
                           DistanceTraveledKms = flightSegment.DistanceTraveledKms,
                           InSegmentStops = flightSegment.InSegmentStops == null ? null : flightSegment.InSegmentStops.Select(SegmentStop.ToDataContract).ToListBuffered(flightSegment.InSegmentStops.Count),
                           DepartureDateTime = flightSegment.DepartureDateTime,
                           ArrivalDateTime = flightSegment.ArrivalDateTime,
                           DepartureTerminal = flightSegment.DepartureTerminal,
                           ArrivalTerminal = flightSegment.ArrivalTerminal,
                           Index = flightSegment.SegmentIndex,
                           CabinType = ToDataContract(paxTypeFareBasis.CabinType),
                           ClassOfService = paxTypeFareBasis.ClassOfService,
                           AircraftType = flightSegment.AircraftType,
                           PaxTypeFareBasisCodes = flightSegment.PaxTypeFareBasisCodes.Select(PaxTypeFareBasis.ToDataContract).ToListBuffered(flightSegment.PaxTypeFareBasisCodes.Count)
                       };
        }

        public static CabinType ToDataContract(Entities.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.CabinType.Business:
                    return CabinType.Business;
                case Entities.CabinType.Economy:
                    return CabinType.Economy;
                case Entities.CabinType.First:
                    return CabinType.First;
                case Entities.CabinType.PremiumEconomy:
                    return CabinType.PremiumEconomy;
                case Entities.CabinType.Unknown:
                    return CabinType.Unknown;
            }

            return default(CabinType);
        }

        public static Entities.CabinType ToEntity(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Business:
                    return Entities.CabinType.Business;
                case CabinType.Economy:
                    return Entities.CabinType.Economy;
                case CabinType.First:
                    return Entities.CabinType.First;
                case CabinType.PremiumEconomy:
                    return Entities.CabinType.PremiumEconomy;
                case CabinType.Unknown:
                    return Entities.CabinType.Unknown;
            }

            return default(Entities.CabinType);
        }

        public static Entities.FlightSegment ToEntity(FlightSegment flightSegment)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
