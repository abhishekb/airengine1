﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Tavisca.TravelNxt.Flight.SeatMap.DataContract;

namespace Tavisca.TravelNxt.Flight.SeatMap.ServiceContract
{
    [ServiceContract(Namespace = "http://tavisca.com/AirSeatmap/V1")]
    public interface IAirSeatMap
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "getSeatMap", BodyStyle = WebMessageBodyStyle.Bare)]
        SeatMapRS GetSeatMap(SeatMapRQ request);
    }
}
