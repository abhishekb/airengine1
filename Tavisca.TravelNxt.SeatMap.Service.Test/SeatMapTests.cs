﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.SeatMap.Service.Test.AirEngine;
using Tavisca.TravelNxt.SeatMap.Service.Test.AirSeatMap;
using CallStatus = Tavisca.TravelNxt.SeatMap.Service.Test.AirEngine.CallStatus;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.SeatMap.Service.Test
{
    [TestClass]
    public class SeatMapTests
    {
        [TestMethod]
        [TestCategory("Service Test Seatmap")]
        public void TestTravelportSeatMap()
        {
            bool successStatus = false;
            FlightSearchRQ fareSearchRq = RequestProvider.GetFareSearchRequest(Constants.Travelport);
            using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                            Guid.NewGuid().ToString(), "170",
                            GetTestSetting("MockGetAccountId"), GetTestSetting("DisplayCurrency"),
                            GetTestSetting("Password"), false, string.Empty)))
            using (var client = new AirEngineClient("AirBasicHttpEndpoint"))
            {
                var fareSearchResponse = client.Search(fareSearchRq);
                ValidateSearchResponse(fareSearchResponse);
                foreach (var request in GetSeatMapRequest(fareSearchResponse))
                {
                    try
                    {
                        var seatMapResponse = new AirSeatMapClient().GetSeatMap(request);
                        ValidateResponse(seatMapResponse);
                        successStatus = true;
                        break;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            Assert.IsTrue(successStatus, "Seatmap failed for all segments");
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private static void ValidateSearchResponse(FlightSearchRS fareSearchRs)
        {
            Assert.IsNotNull(fareSearchRs);
            Assert.AreNotEqual(fareSearchRs.ServiceStatus.Status, CallStatus.Failure);
            Assert.IsTrue((fareSearchRs.ItineraryRecommendations != null && fareSearchRs.ItineraryRecommendations.Any()) ||
                          (fareSearchRs.LegRecommendations != null && fareSearchRs.LegRecommendations.Any()));
        }

        private static IEnumerable<SeatMapRQ> GetSeatMapRequest(FlightSearchRS fareSearchResponse)
        {
            var allRecommendations =
                (fareSearchResponse.LegRecommendations ?? new List<FlightRecommendation>()).Union(
                    fareSearchResponse.ItineraryRecommendations ?? new List<FlightRecommendation>()).ToArray();

            foreach (var segment in fareSearchResponse.Segments)
            {
                yield return GetSeatMapRQ(fareSearchResponse, allRecommendations, segment);
            }
        }

        private static SeatMapRQ GetSeatMapRQ(FlightSearchRS flightSearchRs,IEnumerable<FlightRecommendation> flightRecommendations, FlightSegment flightSegment)
        {
            var recommendation = flightRecommendations.First(
                flightRecommendation => DoesRecommendationContainSegment(flightSearchRs, flightRecommendation, flightSegment));

            return new SeatMapRQ()
                       {
                           RecommendationRefId = recommendation.RefID,
                           SegmentKey = flightSegment.Key
                       };
        }
        
        private static bool DoesRecommendationContainSegment(FlightSearchRS flightSearchRs, FlightRecommendation recommendation, FlightSegment flightSegment)
        {
            var flightLegs = new List<FlightLeg>();
            foreach (var legRef in recommendation.LegRefs)
            {
                var parts = legRef.Split('/').Select(int.Parse).ToArray();
                flightLegs.Add(
                    flightSearchRs.LegAlternates.First(x => x.LegIndex == parts[0]).Legs.First(x => x.RefID == parts[1]));
            }

            return flightLegs.Any(leg => leg.FlightSegmentRefs.Contains(flightSegment.RefId));
        }

        private static void ValidateResponse(SeatMapRS seatMapResponse)
        {
            Assert.IsNotNull(seatMapResponse);
            Assert.IsNotNull(seatMapResponse.SeatMap);
            Assert.IsNotNull(seatMapResponse.ServiceStatus);
            Assert.AreEqual(seatMapResponse.ServiceStatus.Status, AirSeatMap.CallStatus.Success);
        }
    }
}
