﻿namespace Tavisca.TravelNxt.SeatMap.Service.Test
{
    internal static class Constants
    {
        public const string RequesterNode = "Requester";
        public const string JourneyLegNode = "JourneyLeg";
        public const string SupplierNode = "Supplier";
        public const string Attribute = "attribute";
        public const string Code = "Code";
        public const string Name = "name";
        public const string Value = "value";
        public const string Culture = "en-us";
        public const string Curency = "INR";

        public const string ApplicableFor = "applicableFor";
        public const string UserId = "UserId";
        public const string UserSessionId = "UserSessionId";
        public const string TrackingId = "TrackingId";
        public const string AffiliateId = "AffiliateId";

        public const string RequesterXml = "Requester.xml";
        public const string JourneyLegXml = "JourneyLeg.xml";
        public const string SupplierAttributesXml = "FareSpaceAttributes.xml";
        public const string PreferredAirlineNode = "PreferredAirline";
        public const string DepartureCode = "DepartureCode";
        public const string ArrivalCode = "ArrivalCode";

        public const string Travelport = "travelport";
        public const string Adult = "Adult";

        public static class BindingNames
        {
            public const string AirFareSearchBasic = "BasicHttpBinding_IAirFareSearchV1";
        }
    }
}
