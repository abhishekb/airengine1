﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareComponent;
using FlightItinerary = Tavisca.TravelNxt.Flight.FareComponent.FlightItinerary;

namespace MarkupComponentTests
{
    [TestClass]
    public class ApplicabilityChecks : TestBase
    {
        [TestMethod]
        public void TestForCustomerType()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution(){ProviderSpace = "123"},
                Rule = new AirFareRule() {CustomerType = "Agent"}
            };

            var flightFareDetails = new FlightFareDetails() {Provider = GetProviderSpace(CustomerType.Agent)};

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsTrue(val, "Customer type applicability failed");

            flightFareDetails = new FlightFareDetails() {Provider = GetProviderSpace(CustomerType.Admin)};

            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsFalse(val, "Invalid customer type should not pass");
        }

        [TestMethod]
        public void TestForRateCode()
        {
            const string rateCode = "TestRateCode";
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule = new AirFareRule() {RateCode = rateCode}
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent,
                        new List<AirContract>() {new DbAirContract() {ContractRateCode = rateCode}})
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsTrue(val, "Rate code applicability failed");

            flightFareDetails = new FlightFareDetails() { Provider = GetProviderSpace(CustomerType.Admin) };

            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsFalse(val, "Invalid rate code should not pass");
        }

        [TestMethod]
        public void TestForTravelType()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelType =
                            new TravelType()
                            {
                                ItineraryType = ItineraryTypeOptions.OneWay,
                                CabinType = CabinType.First
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                CabinTypes = new List<CabinType>() {CabinType.First, CabinType.Business},
                ItineraryType = ItineraryTypeOptions.OneWay
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsTrue(val, "Travel type check failed");

            flightFareDetails.ItineraryType = ItineraryTypeOptions.RoundTrip;

            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);

            Assert.IsFalse(val, "Invalid traveltype(itinerary type) should not pass");

            flightFareDetails.ItineraryType = ItineraryTypeOptions.OneWay;
            flightFareDetails.CabinTypes = new List<CabinType>() {CabinType.Business, CabinType.Economy};
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid traveltype(cabin type) should not pass");
        }

        [TestMethod]
        public void TestOriginCountryCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() {OriginCountries = "US, CN"}
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                OriginCountry = "CN"
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Origin country check failed");

            flightFareDetails.OriginCountry = "LON";
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid origin country should not pass");
        }

        [TestMethod]
        public void TestDestinationCountryCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() {DestinationCountries = "US, CN"}
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DestinationCountry = "CN"
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Destination country check failed");

            flightFareDetails.DestinationCountry = "LON";
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid destination country should not pass");
        }

        [TestMethod]
        public void TestOriginAirportCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() { ProviderSpace = "123" },
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() { OriginAirports = "MCO, NYC" }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                OriginAirportCode = "NYC"
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Origin airport check failed");

            flightFareDetails.OriginAirportCode = "LON";
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid origin airport should not pass");
        }

        [TestMethod]
        public void TestDestinationAirportCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() { ProviderSpace = "123" },
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() { DestinationAirports = "MCO, NYC" }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DestinationAirportCode = "NYC"
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Destination airport check failed");

            flightFareDetails.DestinationAirportCode = "LON";
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid destination airport should not pass");
        }

        [TestMethod]
        public void TestFareCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() {FareType = FareType.Negotiated}
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                Fare = new FlightFare() {FareType = FareType.Negotiated}
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Fare type check failed");

            flightFareDetails.Fare.FareType = FareType.Published;
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid fare type should not pass");
        }

        [TestMethod]
        public void TestMarketingAirlineCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() {MarketingCarriers = "M1, m2"}
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                MarketingCarriers = new List<string>() {"m1", "m2"}
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Marketing airline check failed");

            flightFareDetails.MarketingCarriers = new List<string>() {"m1", "m2", "m3"};
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid marketing airline should not pass");
        }

        [TestMethod]
        public void TestClassOfServiceCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() { ProviderSpace = "123" },
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() { ClassesOfService = "C1, c2" }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                ClassOfServices = new List<string>() {"c1", "c2"}
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "COS airline check failed");

            flightFareDetails.ClassOfServices = new List<string>() { "c1", "c2", "m3" };
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid COS should not pass");
        }

        [TestMethod]
        public void TestFareBasisCheck()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() { ProviderSpace = "123" },
                Rule =
                    new AirFareRule()
                    {
                        Itinerary = new FlightItinerary() { FareBasisCodes = "FB1, fb2" }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                FareBasisCodes = new List<string>() {"fb1", "fb2"}
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "fare basis check failed");

            flightFareDetails.FareBasisCodes = new List<string>() { "fb1", "fb2", "m3" };
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid fare basis should not pass");
        }

        [TestMethod]
        public void TestAdvancedPurchaseCheckWithBothBounds()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                AdvancePurchase = new TravelDaySpan() {MinimumDays = 10, MaximumDays = 20}
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DepartureTime = DateTime.UtcNow.AddDays(15)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Advance purchase check with both bounds failed");

            flightFareDetails.DepartureTime = DateTime.UtcNow.AddDays(21);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid advance purchase should not pass");
        }

        [TestMethod]
        public void TestAdvancedPurchaseCheckWithMaxBound()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                AdvancePurchase = new TravelDaySpan() {MaximumDays = 20}
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DepartureTime = DateTime.UtcNow.AddDays(1)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Advance purchase check failed for max bound");

            flightFareDetails.DepartureTime = DateTime.UtcNow.AddDays(21);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid advance purchase check should not pass(max bound)");
        }

        [TestMethod]
        public void TestAdvancedPurchaseCheckWithMinBounds()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                AdvancePurchase = new TravelDaySpan() {MinimumDays = 10}
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DepartureTime = DateTime.UtcNow.AddDays(50)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Advance purchase check failed for min bound");

            flightFareDetails.DepartureTime = DateTime.UtcNow.AddDays(9);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid advance purchase check should not pass(min bound)");
        }

        [TestMethod]
        public void TestTravelSpan()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                TravelDateSpan =
                                    new TravelDateSpan()
                                    {
                                        From = DateTime.UtcNow.AddDays(10),
                                        To = DateTime.UtcNow.AddDays(20)
                                    }
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DepartureTime = DateTime.UtcNow.AddDays(11),
                ArrivalTime = DateTime.UtcNow.AddDays(17)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Valid travel span failed");

            flightFareDetails.ArrivalTime = DateTime.UtcNow.AddDays(21);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid maximum travelspan bound succeeded");

            flightFareDetails.DepartureTime = DateTime.UtcNow.AddDays(5);
            flightFareDetails.ArrivalTime = DateTime.UtcNow.AddDays(17);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid minimum travelspan bound succeeded");
        }

        [TestMethod]
        public void TestBookingSpan()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() { ProviderSpace = "123" },
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                BookingDateSpan = 
                                    new TravelDateSpan()
                                    {
                                        From = DateTime.UtcNow.AddDays(-10),
                                        To = DateTime.UtcNow.AddDays(10)
                                    }
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Valid booking span failed");

            (rule.Rule as AirFareRule).TravelDates.BookingDateSpan.From = DateTime.UtcNow.AddDays(1);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid maximum bookingspan bound succeeded");

            (rule.Rule as AirFareRule).TravelDates.BookingDateSpan.To = DateTime.UtcNow.AddDays(-1);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid minimum bookingspan bound succeeded");
        }

        [TestMethod]
        public void TestTravelDuration()
        {
            var rule = new FareComponentRule()
            {
                Distribution = new Distribution() {ProviderSpace = "123"},
                Rule =
                    new AirFareRule()
                    {
                        TravelDates =
                            new TravelDates()
                            {
                                TravelDuration = new TravelDaySpan() {MinimumDays = 10, MaximumDays = 20}
                            }
                    }
            };

            var flightFareDetails = new FlightFareDetails()
            {
                Provider =
                    GetProviderSpace(CustomerType.Agent),
                DepartureTime = DateTime.UtcNow.AddDays(5),
                ArrivalTime = DateTime.UtcNow.AddDays(17)
            };

            var val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsTrue(val, "Valid travel duration failed");

            flightFareDetails.ArrivalTime = DateTime.UtcNow.AddDays(7);
            val = new MockFareManager().IsComponentApplicable(flightFareDetails, new Dictionary<string, string>(),
                rule);
            Assert.IsFalse(val, "Invalid travel duration succeeded");
        }

        #region private methods

        private static ProviderSpace GetProviderSpace(CustomerType customerType, IList<AirContract> contracts = null)
        {
            var provider = new ProviderSpace(123, "test", new Requester(4, 789, customerType), "family", null, null,
                ProviderSpaceType.All, new Collection<ProviderSpaceConfiguration>(),
                new Collection<KeyValuePair<string, string>>()) {Contracts = contracts};

            return provider;
        }

        #endregion
    }
}
