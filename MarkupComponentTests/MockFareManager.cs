﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.FareComponent;

namespace MarkupComponentTests
{
    internal class MockFareManager : AirFareManager
    {
        public bool IsComponentApplicable(FlightFareDetails flightFareDetails, IDictionary<string, string> additionalInfo, FareComponentRule fareComponentRule)
        {
            return IsApplicable(flightFareDetails, additionalInfo, fareComponentRule);
        }
    }
}
