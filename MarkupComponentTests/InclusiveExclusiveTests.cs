﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.FareComponent;

namespace MarkupComponentTests
{
    [TestClass]
    public class InclusiveExclusiveTests
    {
        [TestMethod]
        public void TestSecondHighestPriorityExclusiveMarkupSelection()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 5,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                var isApplicable = rule.Rule.CustomerType == validCustomerType;

                if (isApplicable)
                    appliedRules.Add(rule);
                return isApplicable;
            });

            Assert.IsFalse(appliedRules.Any(x => x.Id == 1));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 2));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 3));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 5));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 4));
        }

        [TestMethod]
        public void TestFirstHighestPriorityExclusiveMarkupSelection()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 5,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                var isApplicable = rule.Rule.CustomerType == validCustomerType;

                if (isApplicable)
                    appliedRules.Add(rule);
                return isApplicable;
            });

            Assert.IsTrue(appliedRules.Any(x => x.Id == 1));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 2));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 3));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 5));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 4));
        }

        [TestMethod]
        public void TestSecondHighestPriorityExclusiveSelectionPerGroup()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "B",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "B",
                    IsExclusive = true,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                var isApplicable = rule.Rule.CustomerType == validCustomerType;

                if (isApplicable)
                    appliedRules.Add(rule);
                return isApplicable;
            });

            Assert.IsTrue(appliedRules.Any(x => x.Id == 2));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 3));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 1));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 4));
        }

        [TestMethod]
        public void TestFirstHighestPriorityExclusiveSelectionPerGroup()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "B",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "B",
                    IsExclusive = true,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                var isApplicable = rule.Rule.CustomerType == validCustomerType;

                if (isApplicable)
                    appliedRules.Add(rule);
                return isApplicable;
            });

            Assert.IsTrue(appliedRules.Any(x => x.Id == 1));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 4));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 2));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 3));
        }

        [TestMethod]
        public void TestInclusiveMarkupApplicability()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = false,
                    Preference = 3,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "B",
                    IsExclusive = false,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "B",
                    IsExclusive = false,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                var isApplicable = rule.Rule.CustomerType == validCustomerType;

                if (isApplicable)
                    appliedRules.Add(rule);
                return isApplicable;
            });

            Assert.IsTrue(appliedRules.Any(x => x.Id == 1));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 2));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 3));
        }

        [TestMethod]
        public void TestHighestExclusiveSelectionPerGroupPerComponentType()
        {
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 1,
                    FareComponentType = FareComponentTypeOptions.Fee
                },
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    FareComponentType = FareComponentTypeOptions.Fee
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 1,
                    FareComponentType = FareComponentTypeOptions.Markup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
                appliedRules.Add(rule);
                return true;
            });

            Assert.IsFalse(appliedRules.Any(x => x.Id == 1));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 2));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 3));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 4));
        }

        [TestMethod]
        public void TestHighestExclusiveSelectionWhenComponentTypePassedAreMarkupAndDynamicMarkup()
        {
            const string validCustomerType = "Valid";
            const string invalidCustomerType = "Invalid";
            var appliedRules = new List<FareComponentRule>();
            var rules = new List<FareComponentRule>()
            {
                new FareComponentRule()
                {
                    Id = 3,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 1,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 2,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 2,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 1,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 3,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.DynamicMarkup
                },
                new FareComponentRule()
                {
                    Id = 4,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 4,
                    Rule = new AirFareRule()
                    {
                        CustomerType = invalidCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.Markup
                },
                new FareComponentRule()
                {
                    Id = 5,
                    FareComponentGroup = "A",
                    IsExclusive = true,
                    Preference = 5,
                    Rule = new AirFareRule()
                    {
                        CustomerType = validCustomerType
                    },
                    FareComponentType = FareComponentTypeOptions.DynamicMarkup
                }
            };

            InclusiveExclusiveHandler.Process(rules, rule =>
            {
               appliedRules.Add(rule);
               return true;
            });

            Assert.IsFalse(appliedRules.Any(x => x.Id == 1));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 2));
            Assert.IsTrue(appliedRules.Any(x => x.Id == 3));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 5));
            Assert.IsFalse(appliedRules.Any(x => x.Id == 4));
        }
    }
}
