using System.Reflection;
using System.Runtime.InteropServices;

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyCompany("Tavisca Solutions Pvt. Ltd.")]
[assembly: AssemblyCopyright("Copyright � 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]


[assembly: AssemblyVersion("1.0.1492.0")]
[assembly: AssemblyFileVersion("1.0.1492.0")]
[assembly: AssemblyInformationalVersion("")]
