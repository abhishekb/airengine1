﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Avail.Service.Test.AirFareRules;
using Tavisca.TravelNxt.Avail.Service.Test.AirEngine;
using Tavisca.Frameworks.Parallel.Ambience;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Service.Test
{
    [TestClass]
    public class AirFareRulesTests
    {

        private static AirSearchTests test = new AirSearchTests();

        private static List<Tuple<string, CallContext,FlightSearchRS>> AirSearchResposes = new List<Tuple<string,CallContext, FlightSearchRS>>();
        private static string BaseAirFareRulesAddress = GetTestSetting("BaseAirFareRulesAddress");
        private static string[] accountDetailsRaw = GetTestSetting("AccountIdPosPair").Split(',');
        private static List<string> bindingAddress = new List<string>();
        private static readonly string[] bindings = GetTestSetting("Bindings").ToLower().Split(',');
        private static string sessionId;

        private static List<string> bindingTypes = new List<string>();
        private static List<string> webHttpBindingContentTypes = new List<string>();

        public static List<Tuple<string, string, string>> AccountDetails = new List<Tuple<string, string, string>>();



        [ClassInitialize]

        public static void ClassInitialize(TestContext context)
        {
            if (string.IsNullOrEmpty(BaseAirFareRulesAddress))
                throw new InternalTestFailureException("Aborting test..Invalid or empty Base Airline address....");
            else
                SetupBindings();

            if (accountDetailsRaw.Length == 0)
                throw new InternalTestFailureException("Aborting test..Invalid or empty Account details....");

            PrepareSearchTestResults();
        
            if (AirSearchResposes.Count == 0)
                throw new InternalTestFailureException("Could not start AirFareRules Test because 0 ItineraryRecommendations or LegRecommendations found in all the search requests made in ClassInitilize method....");
        }
             

        private static void PrepareSearchTestResults()
        {
            CallContext AirSearchCallContext;
            string[] accountIdPOSPair;
            foreach (string detail in accountDetailsRaw)
            {
                accountIdPOSPair = detail.Split(':');
                if (accountIdPOSPair.Length == 3)
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], accountIdPOSPair[2]));
                else
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], "ANY"));

                sessionId = Guid.NewGuid().ToString();
                AirSearchCallContext = new CallContext(GetTestSetting("MockCulture"), sessionId,
                accountIdPOSPair[1], accountIdPOSPair[0], GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty);
                AirEngine.FlightSearchRS rs;
                rs = test.AirSearch(SearchRequestFactory.TripType.RoundTrip,SearchRequestFactory.PassangerInfoSummaryType.OneAdult ,AirSearchCallContext,accountIdPOSPair[2]);
                                
                if (string.IsNullOrEmpty(rs.SessionID.ToString()) || rs.SessionID.Equals(sessionId))
                    throw new InternalTestFailureException("Null or Empty SessionId found in one of the AirSearch Response made in ClassInitialize method....");

                if ((rs.LegRecommendations != null && rs.LegRecommendations.Count > 0) || (rs.ItineraryRecommendations != null && rs.ItineraryRecommendations.Count > 0))
                {
                    AirSearchResposes.Add(Tuple.Create(sessionId,AirSearchCallContext,rs));
                }
            }
        }

        [TestMethod]
        public void AirFareRulesTest()
        {
            int responseNumber = StaticRandom.Instance.Next(AirSearchResposes.Count);
            AirFareRules.FareRulesRQ fareRuleRequest = new AirFareRules.FareRulesRQ();

            fareRuleRequest.RecommendationRefID = GetRecommendationRefId(AirSearchResposes[responseNumber].Item3);
            var accountData =AccountDetails[StaticRandom.Instance.Next(AirSearchTests.AccountDetails.Count)];
            AirFareRules.AirFareRulesClient client;
            FareRulesRS fareRulesResponse=null;
            int randomBinding = StaticRandom.Instance.Next(bindingAddress.Count);

            if (bindingTypes[randomBinding].ToLower().Equals("basichttpbinding_ifarerules") || bindingTypes[randomBinding].ToLower().Equals("probinding_ifarerules"))
            {
                using (client =new AirFareRulesClient(bindingTypes[randomBinding],bindingAddress[randomBinding]))
                {
                    using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                            AirSearchResposes[responseNumber].Item1, accountData.Item2.ToString(CultureInfo.InvariantCulture),
                            accountData.Item1, GetTestSetting("DisplayCurrency"),
                            GetTestSetting("Password"), false, string.Empty)))
                    {
                        fareRulesResponse = client.GetFareRules(fareRuleRequest);
                    }
                }
            }
            else if (bindingTypes[randomBinding].ToLower().Equals("webhttpbinding_iairfarerules"))
            {
                int randomContentType = StaticRandom.Instance.Next(webHttpBindingContentTypes.Count);
                using (var webClient = GetWebClient(webHttpBindingContentTypes[randomContentType], accountData.Item1, accountData.Item2))
                {
                    var serializedRequest = GetSerializedRequest(fareRuleRequest, webHttpBindingContentTypes[randomContentType]);
                    var bytesResponse = webClient.UploadData(bindingAddress[randomBinding] + "/getfarerules",
                        Encoding.UTF8.GetBytes(serializedRequest));
                    fareRulesResponse = GetResponseFromBytes(bytesResponse, webHttpBindingContentTypes[randomContentType]);

                }
            }
            ValidateResponse(fareRulesResponse);
        }

        private string GetSerializedRequest(FareRulesRQ flightSearchRequest, string contentType)
        {
            string response;
            response = contentType == "text/json" ? Serialize(flightSearchRequest) : XMLSerialize(flightSearchRequest);
            return response;
        }

        private FareRulesRS GetResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? Deserialize<FareRulesRS>(byteResponse) : XmlDeserialize<FareRulesRS>(byteResponse);
            return response;
        }

        private static T XmlDeserialize<T>(byte[] data)
        {
            var ser = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);
                return (T)obj;
            }
        }

        private string Serialize<T>(T obj)
        {
            var ser = new DataContractJsonSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);
                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private string XMLSerialize<T>(T obj)
        {
            var ser = new DataContractSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);
                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private T Deserialize<T>(byte[] data)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);
                return (T)obj;
            }
        }


        private WebClient GetWebClient(string contentType, string AccountId, string pos)
        {
            var client = new WebClient();
            client.Headers.Add("culture", GetTestSetting("MockCulture"));
            client.Headers.Add("posId", pos);
            client.Headers.Add("accountId", AccountId);
            client.Headers.Add("displayCurrency", GetTestSetting("DisplayCurrency"));
            client.Headers.Add("password", GetTestSetting("Password"));
            client.Headers.Add("sessionId", Guid.NewGuid().ToString());
            client.Headers.Add("Content-Type", contentType);
            client.Headers.Add("Accept-Type", contentType);
            return client;
        }
       public static int GetRecommendationRefId(FlightSearchRS searchResponse)
        {
            if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                return searchResponse.LegRecommendations[StaticRandom.Instance.Next(0, searchResponse.LegRecommendations.Count)].RefID;

            return searchResponse.ItineraryRecommendations[StaticRandom.Instance.Next(0, searchResponse.ItineraryRecommendations.Count)].RefID;
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private void ValidateResponse(FareRulesRS fareRulesResponse)
        {
            Assert.IsNotNull(fareRulesResponse, "Fare rule response is null...");
            Assert.IsNotNull(fareRulesResponse.FareRules, "Fare rules are null...");
            Assert.IsNotNull(fareRulesResponse.ServiceStatus, "Service status is null...");
            StringBuilder statusMessage = new StringBuilder();
            if (!string.Equals(Tavisca.TravelNxt.Avail.Service.Test.AirFareRules.CallStatus.Success, fareRulesResponse.ServiceStatus.Status))
            {
                if (!string.IsNullOrEmpty(fareRulesResponse.ServiceStatus.StatusMessage))
                    statusMessage.AppendLine(fareRulesResponse.ServiceStatus.StatusMessage);
                if (fareRulesResponse.ServiceStatus.Messages.Length > 0)
                {
                    foreach (string message in fareRulesResponse.ServiceStatus.Messages)
                        statusMessage.AppendLine(message);
                }
            }
            Assert.AreEqual(Tavisca.TravelNxt.Avail.Service.Test.AirFareRules.CallStatus.Success, fareRulesResponse.ServiceStatus.Status,
                statusMessage.ToString());
            Assert.IsTrue(fareRulesResponse.FareRules != null && fareRulesResponse.FareRules.Length > 0,"Fare Rules length is 0");
        }

        public static void SetupBindings()
        {
            foreach (string binding in bindings)
            {

                if (binding.Equals("protobufbinding"))
                {
                    bindingAddress.Add(BaseAirFareRulesAddress + "/proto");
                    bindingTypes.Add("proBinding_IFareRules");
                }
                else if (binding.Equals("restbindingwithjson"))
                {
                    bindingAddress.Add(BaseAirFareRulesAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/json");
                    bindingTypes.Add("webHttpBinding_IAirFareRules");
                }
                else if (binding.Equals("restbindingwithxml"))
                {
                    bindingAddress.Add(BaseAirFareRulesAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/xml");
                    bindingTypes.Add("webHttpBinding_IAirFareRules");
                }
                else if (binding.Equals("basichttpbinding"))
                {
                    bindingAddress.Add(BaseAirFareRulesAddress);
                    bindingTypes.Add("BasicHttpBinding_IFareRules");
                }
                else
                    throw new InternalTestFailureException("Invlid inputs provided for AirFareRules in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
            }
            if (bindingAddress.Count == 0)
                throw new InternalTestFailureException("Invlid inputs provided for AirFareRules in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
        }


    }
}
