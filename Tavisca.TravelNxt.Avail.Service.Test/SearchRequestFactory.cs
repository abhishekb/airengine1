﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Avail.Service.Test.AirEngine;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Service.Test
{

    public static class SearchRequestFactory
    {
        public enum PassangerInfoSummaryType
        {
            OneAdult,
            SeniorCouple,
            InfantWIthParents,
            Family
        }

        public enum TripType
        {
            OneWay,
            RoundTrip,
            MultiCity
        }

        public static FlightSearchRQ GetBaseSearchRequest()
        {
            var req = new FlightSearchRQ
            {
                Requester = new Requester(),
                SearchCriterion = new FlightSearchCriteria()
                {
                    MaxPreferredResults = 200
                }
            };

            return req;
        }

      private static readonly List<Tuple<string, string, string>> MulticityUSPairs = new List<Tuple<string, string, string>>()
        {
           Tuple.Create("PNQ","DEL","BLR"),
           Tuple.Create("PNQ","DEL","LAX"),
           Tuple.Create("PNQ","DEL","JFK"),
           Tuple.Create("PNQ","DEL","LHR"),
       
           Tuple.Create("BOM","DEL","LAS"),
           Tuple.Create("BOM","DEL","LAX"),
           Tuple.Create("BOM","DEL","JFK"),
           Tuple.Create("BOM","DEL","LHR"),
           
           Tuple.Create("DEL","BLR","LAS"),
           Tuple.Create("DEL","BLR","LAX"),
           Tuple.Create("DEL","BLR","JFK"),
           Tuple.Create("DEL","BLR","LHR"),
           Tuple.Create("DEL","BLR","LAS"),
           
        };

        private static readonly List<Tuple<string, string, string>> MulticityINPairs = new List<Tuple<string, string, string>>()
        {
           Tuple.Create("PNQ","DEL","BLR"),
           Tuple.Create("PNQ","DEL","LAX"),
           Tuple.Create("PNQ","DEL","JFK"),
           Tuple.Create("PNQ","DEL","LHR"),
       
           Tuple.Create("BOM","DEL","LAS"),
           Tuple.Create("BOM","DEL","LAX"),
           Tuple.Create("BOM","DEL","JFK"),
           Tuple.Create("BOM","DEL","LHR"),
           
           Tuple.Create("DEL","BLR","LAS"),
           Tuple.Create("DEL","BLR","LAX"),
           Tuple.Create("DEL","BLR","JFK"),
           Tuple.Create("DEL","BLR","LHR"),
           Tuple.Create("DEL","BLR","LAS"),
           
        };

        private static readonly List<KeyValuePair<string, string>> USPairs = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("LAS", "LAX"),
                new KeyValuePair<string, string>("LAS", "JFK"),
                new KeyValuePair<string, string>("LAS", "BKK"),
                new KeyValuePair<string, string>("LAX", "LAS"),
                new KeyValuePair<string, string>("JFK", "LAS"),
                new KeyValuePair<string, string>("JFK", "LAX"),    
                new KeyValuePair<string, string>("JFK", "LHR"),
                new KeyValuePair<string, string>("BKK", "LAS"),
                new KeyValuePair<string, string>("LHR", "LAS"),
                new KeyValuePair<string, string>("LHR", "LAX"),
                new KeyValuePair<string, string>("LHR", "JFK"),

                new KeyValuePair<string, string>("LAS", "DEL"),
                new KeyValuePair<string, string>("LAX", "DEL"),
                new KeyValuePair<string, string>("BKK", "DEL"),
                new KeyValuePair<string, string>("JFK", "DEL"),
                new KeyValuePair<string, string>("LHR", "DEL")

           }; //repeated by choice.

        private static readonly List<KeyValuePair<string, string>> INPairs = new List<KeyValuePair<string, string>>()
            {
                //International
                /*new KeyValuePair<string, string>("DEL", "LAS"),
                new KeyValuePair<string, string>("DEL", "LAX"),
                new KeyValuePair<string, string>("DEL", "BKK"),
                new KeyValuePair<string, string>("DEL", "JFK"),
                new KeyValuePair<string, string>("DEL", "LHR"),*/

                //Domestic
                new KeyValuePair<string, string>("DEL", "BOM"),
                new KeyValuePair<string, string>("DEL", "PNQ"),
                new KeyValuePair<string, string>("DEL", "BLR"),
                new KeyValuePair<string, string>("DEL", "LHR"),
                new KeyValuePair<string, string>("BOM", "DEL"),
                new KeyValuePair<string, string>("BOM", "PNQ"),
                new KeyValuePair<string, string>("BOM", "BLR"),
                new KeyValuePair<string, string>("BOM", "LHR"),
                new KeyValuePair<string, string>("PNQ", "BOM"),
                new KeyValuePair<string, string>("PNQ", "DEL"),
                new KeyValuePair<string, string>("PNQ", "BLR"),
                new KeyValuePair<string, string>("PNQ", "LHR"),
                 new KeyValuePair<string, string>("BLR", "BOM"),
                new KeyValuePair<string, string>("BLR", "PNQ"),
                new KeyValuePair<string, string>("BLR", "DEL"),
                new KeyValuePair<string, string>("BLR", "LHR"),
                 new KeyValuePair<string, string>("LHR", "BOM"),
                new KeyValuePair<string, string>("LHR", "PNQ"),
                new KeyValuePair<string, string>("LHR", "DEL")
            }; //repeated by choice.

       public static PaxTypeQuantity GetCustomerType(List<int> ages, int quantity, PassengerType type)
        {
            PaxTypeQuantity item = new PaxTypeQuantity();
            item.Ages = ages;
            item.Quantity = quantity;
            item.PassengerType = type;
            return item;
        }

        public static List<PaxTypeQuantity> CreatePassangerInfoSummaryList(PassangerInfoSummaryType infoType)
        {
            List<PaxTypeQuantity> passangers = new List<PaxTypeQuantity>();

            switch (infoType)
            {
                case PassangerInfoSummaryType.OneAdult:
                    passangers.Add(GetCustomerType(new List<int>() { 40 }, 1, PassengerType.Adult));
                    break;

                case PassangerInfoSummaryType.SeniorCouple:
                    passangers.Add(GetCustomerType(new List<int>() { 55, 60 }, 2, PassengerType.Senior));
                    break;

                case PassangerInfoSummaryType.InfantWIthParents:
                    passangers.Add(GetCustomerType(new List<int>() { 40, 35 }, 2, PassengerType.Adult));
                    passangers.Add(GetCustomerType(new List<int>() { 20 }, 1, PassengerType.Infant));
                    break;

                case PassangerInfoSummaryType.Family:
                    passangers.Add(GetCustomerType(new List<int>() { 45, 30 }, 2, PassengerType.Adult));
                    passangers.Add(GetCustomerType(new List<int>() { 10, 10 }, 2, PassengerType.Child));
                    break;

            }
            return passangers;
        }

        public static SearchSegment GetSingleSearchSegment(string origin, string destination, DateTime date)
        {
            SearchSegment segment = new SearchSegment();
            segment.ArrivalAirportCode = destination;
            segment.DepartureAirportCode = origin;
            segment.TravelDate =
                            new TravelDate
                            {
                                DateTime = date,
                                AnyTime = true
                            };
            segment.ArrivalAlternateAirportInformation = new AlternateAirportInformation()
                {
                    IncludeNearByAirports = true,
                    RadiusKm = 10
                };

            return segment;
       }
        public static List<SearchSegment> GetSeachSegmentForTrip(TripType tripType,string cityPair)
        {
            List<SearchSegment> segments = new List<SearchSegment>();

            switch (tripType)
            {
                case TripType.OneWay:
                    var pair = GetRandomCityPair(cityPair);
                    var time = Get50PercentDateTime(pair.Value + "-" + pair.Key);
                    segments.Add(GetSingleSearchSegment(pair.Value, pair.Key, time));
                    break;

                case TripType.RoundTrip:
                    pair = GetRandomCityPair(cityPair);
                    time = Get50PercentDateTime(pair.Value + "-" + pair.Key);
                    var random = new Random(Guid.NewGuid().GetHashCode()).Next(10, 30);
                    segments.Add(GetSingleSearchSegment(pair.Value, pair.Key, time));
                    segments.Add(GetSingleSearchSegment(pair.Key, pair.Value, time.AddDays(random)));

                    break;
                case TripType.MultiCity:
                    var tuple = GetRandomMultiCityPair(cityPair);
                    random = new Random(Guid.NewGuid().GetHashCode()).Next(10, 30);
                    time = Get50PercentDateTime(tuple.Item1 + "-" + tuple.Item2);
                    segments.Add(GetSingleSearchSegment(tuple.Item1, tuple.Item2, time));
                    segments.Add(GetSingleSearchSegment(tuple.Item2, tuple.Item3, time.AddDays(random)));
                    break;
            }
            return segments;
        }


        public static FlightSearchRQ GetTypicalSearchRequest(TripType type, PassangerInfoSummaryType passangerInfo,string cityPair)
        {
            var req = GetBaseSearchRequest();

            req.SearchCriterion.PassengerInfoSummary = new List<PaxTypeQuantity>();
            req.SearchCriterion.PassengerInfoSummary = CreatePassangerInfoSummaryList(passangerInfo);
            req.SearchCriterion.SortingOrder = SortingOrder.ByPriceLowToHigh;
            req.SearchCriterion.SearchSegments = GetSeachSegmentForTrip(type,cityPair);
            req.SearchCriterion.FlightTravelPreference = new FlightTravelPreference()
            {
                AllowMixedAirlines = true,
                IncludeDirectOnly=false,
                IncludeNonStopOnly=false,
                JetFlightsOnly=false,
                NoPenalty=false,
                Refundable=false,
                UnRestrictedFare=false,
                AirlinesPreference = new AirlinesPreference()
            };
            return req;
        }


        public static FlightSearchRQ GetTypicalSearchRequest(bool oneWay, string posHint)
        {
            var req = GetBaseSearchRequest();

            req.SearchCriterion.PassengerInfoSummary = new List<PaxTypeQuantity>();
            req.SearchCriterion.PassengerInfoSummary.Add(new PaxTypeQuantity { Ages = new List<int>() { 20 } });
            req.SearchCriterion.PassengerInfoSummary[0].Quantity = 1;
            req.SearchCriterion.PassengerInfoSummary[0].PassengerType = PassengerType.Adult;
            req.SearchCriterion.SortingOrder = SortingOrder.ByPriceLowToHigh;
            req.SearchCriterion.SearchSegments = new List<SearchSegment>();
            req.SearchCriterion.FlightTravelPreference = new FlightTravelPreference()
            {
                AllowMixedAirlines = true,
                AirlinesPreference = new AirlinesPreference()
            };

            var pair = GetRandomCityPair(posHint);

            var time = Get50PercentDateTime(pair.Key + "-" + pair.Value);

            var origin = pair.Key;
            var destination = pair.Value;

            Console.WriteLine(pair.Key + " " + pair.Value);
            req.SearchCriterion.SearchSegments.Add(new SearchSegment
            {
                ArrivalAirportCode = origin,
                ArrivalAlternateAirportInformation = new AlternateAirportInformation()
                 {
                    IncludeNearByAirports = false,
                    RadiusKm = 10
                },
                DepartureAirportCode = destination,
                TravelDate =
                    new TravelDate
                    {
                        DateTime = time,
                        AnyTime = true
                    }
            });

            if (!oneWay)
            {
                req.SearchCriterion.SearchSegments.Add(new SearchSegment
                {
                    ArrivalAirportCode = destination,
                    ArrivalAlternateAirportInformation = new AlternateAirportInformation()
                    {
                        IncludeNearByAirports = false,
                        RadiusKm = 10
                    },
                    DepartureAirportCode = origin,
                    TravelDate =
                        new TravelDate
                        {
                            DateTime = time.AddDays(1)
                        }
                });
            }

            return req;
        }


        private static KeyValuePair<string, string> GetRandomCityPair(string posHint)
        {

            if (string.IsNullOrWhiteSpace(posHint) || posHint.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
               return USPairs[StaticRandom.Instance.Next(0,USPairs.Count)];
            }
            else if (string.IsNullOrWhiteSpace(posHint) || posHint.Equals("IN", StringComparison.OrdinalIgnoreCase))
            {
                return INPairs[StaticRandom.Instance.Next(0, INPairs.Count)];
            }
            else
            {
                List<KeyValuePair<string, string>> combinedPairs = new List<KeyValuePair<string, string>>();
                combinedPairs=USPairs.Concat(INPairs).ToList();
                return combinedPairs[StaticRandom.Instance.Next(0, combinedPairs.Count)];
            }
        }

        private static Tuple<string, string,string> GetRandomMultiCityPair(string posHint)
        {

            if (string.IsNullOrWhiteSpace(posHint) || posHint.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                return MulticityUSPairs[StaticRandom.Instance.Next(0, MulticityUSPairs.Count)];
            }
            else if (string.IsNullOrWhiteSpace(posHint) || posHint.Equals("IN", StringComparison.OrdinalIgnoreCase))
            {
                return MulticityINPairs[StaticRandom.Instance.Next(0, MulticityINPairs.Count)];
            }
            else
            {
                List<Tuple<string, string,string>> combinedPairs = new List<Tuple<string,string, string>>();
                combinedPairs = MulticityUSPairs.Concat(MulticityINPairs).ToList();
                return combinedPairs[StaticRandom.Instance.Next(0, combinedPairs.Count)];
            }
        }

        private readonly static ConcurrentDictionary<string, ConcurrentBag<DateTime>> DateBags = new ConcurrentDictionary<string, ConcurrentBag<DateTime>>();
        private readonly static ConcurrentDictionary<string, IntCounter> Counters = new ConcurrentDictionary<string, IntCounter>();

        public static DateTime Get50PercentDateTime(string pair)
        {
            //var number = Environment.TickCount%2;

            //if (number == 0)
            //{
            //    return Dates[0];
            //}
            //return Dates[1];

            var dateBag = GetDateBag(pair);

            var counter = GetCounter(pair);

            var current = counter.Increment();

            if ((current % 2) == 0)
            {
                DateTime dateTime;

                while (!dateBag.TryTake(out dateTime))
                { }

                return dateTime;
            }
            var date = GetRandomDate();

            dateBag.Add(date);

            return date;
        }

        private static IntCounter GetCounter(string pair)
        {
            return Counters.GetOrAdd(pair, x => new IntCounter());
        }

        private static ConcurrentBag<DateTime> GetDateBag(string pair)
        {
            return DateBags.GetOrAdd(pair, x => new ConcurrentBag<DateTime>());
        }

        private static DateTime GetRandomDate()
        {
            var maxDate = DateTime.Now.AddMonths(5);

            var now = DateTime.Now.AddMonths(2).AddHours(12); //compensating for time differences

            var maxHourDiff = Convert.ToInt32((maxDate - now).TotalHours);

            var random = new Random(Guid.NewGuid().GetHashCode());

            DateTime retVal;

            do
            {
                var hourDiff = random.Next(0, maxHourDiff);

                retVal = now.AddHours(hourDiff);
            } while (retVal.Hour < 12 || retVal.Hour > 22);

            return retVal;
        }

        public static FlightSearchRQ GetDeferredSearchRequest(bool oneWay, string posHint)
        {
            var req = GetTypicalSearchRequest(oneWay, posHint);

            req.DoDeferredSearch = true;

            return req;
        }

        public static FlightSearchRQ GetDeferredSearchRequestWithSupplierStreaming(bool oneWay, string posHint)
        {
            var req = GetDeferredSearchRequest(oneWay, posHint);

            req.AllowSupplierStreaming = true;

            return req;
        }

        public static int WaitTimeOut
        {
            get { return _waitTimeOut; }
            set { _waitTimeOut = value; }
        }

        private static int _waitTimeOut = 1000;

        public static string SessionId
        {
            get
            {
                return Guid.NewGuid().ToString();
                //if (string.IsNullOrWhiteSpace(_sessionId))
                //    _sessionId = Guid.NewGuid().ToString();

                //return _sessionId;
            }
        }

        public static bool DoMock { get; set; }
    }

    public static class StaticRandom
    {
        private static int seed;

        private static ThreadLocal<Random> threadLocal = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref seed)));

        static StaticRandom()
        {
            seed = Environment.TickCount;
        }

        public static Random Instance { get { return threadLocal.Value; } }
    }
}
