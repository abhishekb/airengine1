﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="tavisca.singularity.configuration" type="Tavisca.Singularity.Configuration.SingularityConfigurationSection, Tavisca.Singularity.Core" />
    <section name="ApplicationLog" type="Tavisca.Frameworks.Logging.Configuration.ApplicationLogSection, Tavisca.Frameworks.Logging" />
  </configSections>
  <tavisca.singularity.configuration initializer="default" host="wcf.windows" interceptionProvider="InterfaceInterceptor" />

  <ApplicationLog exceptionSwitch="Off" eventSwitch="Off" maxThreads="5" minPriority="Medium" defaultLogger="RedisSink" compressionType="Zip" customLocatorAdapter="Tavisca.Frameworks.Logging.Extensions.DependencyInjection.Adapters.SingularityAdapter, Tavisca.Frameworks.Logging.Extensions" customFormatter="Tavisca.Frameworks.Logging.Extensions.Formatters.CreditCardMaskFormatter, Tavisca.Frameworks.Logging.Extensions">
    <categories>

    </categories>
  </ApplicationLog>

  <appSettings>
    <add key="MockCulture" value="en-US" />
    <add key="BaseAirEngineAddress" value="http://dev-apis.tavisca.com/Engines/Oski/AirEngine/AirEngine.svc" />
    <add key="BaseAirFareRulesAddress" value="http://dev-apis.tavisca.com/Engines/Oski/AirEngine/AirFareRules.svc" />
    <add key="BaseAirSeatMapAddress" value="http://dev-apis.tavisca.com/Engines/Oski/AirEngine/AirSeatMap.svc" />

    <!--Note: Possible inputs for Binding are:BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,RestBindingWithXML or mulitple comma(,) seperated bindings are also supported-->
    <add key="Bindings" value="BasicHttpBinding" />

    <!--Note: Input format for AccountIdPair is 'AccountId:POS:TypeOfCityPair'. Value for TypeOfCityPair = IN,US or ANY. Multiple comma(,) seperated values are also supported-->
    <add key="AccountIdPosPair" value="123456:1:US" />

    <add key="DisplayCurrency" value="INR" />
    <!--<add key="BaseCurrency" value="USD"/>-->
    <add key="Password" value="password" />
  </appSettings>
  <system.diagnostics>
    <sources>
      <source name="System.ServiceModel" switchValue="Information, ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="traceListener" type="System.Diagnostics.XmlWriterTraceListener" initializeData="d:\log\client.svclog" />
          <!--<add name="traceListener" 
                      type="Tavisca.Frameworks.Logging.Tracing.ApplicationTraceListener, Tavisca.Frameworks.Logging" />-->
        </listeners>
      </source>
    </sources>
  </system.diagnostics>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="BasicHttpBinding_IAirEngine" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="BasicHttpBinding_IFareRules" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="BasicHttpBinding_ISeatMap" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
      </basicHttpBinding>

      <webHttpBinding>
        <binding name="WebHttpBinding_IAirEngine" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="WebHttpBinding_IAirFareRules" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="WebHttpBinding_IAirSeatMap" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
      </webHttpBinding>

      <customBinding>
        <binding name="CustomBinding_IBinaryTestService" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00">
          <binaryMessageEncoding compressionFormat="Deflate" />
          <httpTransport maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        </binding>
      </customBinding>

      <protoBufBinding>
        <binding name="ProtoBufBinding_IAirEngine" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" />
        <binding name="ProtoBufBinding_IFareRules" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" />
        <binding name="ProtoBufBinding_ISeatMap" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" />
      </protoBufBinding>
    </bindings>

    <client>
      <endpoint binding="webHttpBinding" bindingConfiguration="WebHttpBinding_IAirEngine" contract="AirEngine.IAirEngine" name="webHttpBinding_IAirEngine" behaviorConfiguration="flightClient" />
      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IAirEngine" contract="AirEngine.IAirEngine" name="BasicHttpBinding_IAirEngine" behaviorConfiguration="flightClient" />
      <endpoint binding="protoBufBinding" bindingConfiguration="ProtoBufBinding_IAirEngine" contract="AirEngine.IAirEngine" name="protoBufBinding_IAirEngine" behaviorConfiguration="protoBinding" />

      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IFareRules" contract="AirFareRules.IAirFareRules" name="BasicHttpBinding_IFareRules" behaviorConfiguration="flightInspectors" />
      <endpoint binding="protoBufBinding" bindingConfiguration="ProtoBufBinding_IFareRules" contract="AirFareRules.IAirFareRules" name="proBinding_IFareRules" behaviorConfiguration="protoBinding" />
      <endpoint binding="webHttpBinding" bindingConfiguration="WebHttpBinding_IAirFareRules" contract="AirFareRules.IAirFareRules" name="webHttpBinding_IAirFareRules" behaviorConfiguration="flightClient" />

      <endpoint binding="webHttpBinding" bindingConfiguration="WebHttpBinding_IAirSeatMap" contract="AirSeatMaps.IAirSeatMap" name="webHttpBinding_IAirSeatMap" behaviorConfiguration="flightClient" />
      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_ISeatMap" contract="AirSeatMaps.IAirSeatMap" name="BasicHttpBinding_ISeatMap" behaviorConfiguration="flightClient" />
      <endpoint binding="protoBufBinding" bindingConfiguration="ProtoBufBinding_ISeatMap" contract="AirSeatMaps.IAirSeatMap" name="protoBufBinding_IAirSeatMap" behaviorConfiguration="protoBinding" />

    </client>
    <behaviors>
      <endpointBehaviors>
        <behavior name="flightInspectors">
          <flightEndpointBehaviourExtension />
        </behavior>
        <behavior name="flightClient">
          <flightEndpointBehaviourExtension />
        </behavior>
        <behavior name="protoBinding">
          <protoBufFormatterBehaviour />
          <flightEndpointBehaviourExtension />
        </behavior>
      </endpointBehaviors>
    </behaviors>
    <extensions>
      <bindingExtensions>
        <add name="protoBufBinding" type="ProtoBuf.Wcf.Channels.Bindings.ProtoBufBindingCollectionElement, ProtoBuf.Wcf.Channels, Version=1.0.0.0, Culture=neutral" />
      </bindingExtensions>
      <behaviorExtensions>
        <add name="protoBufFormatterBehaviour" type="ProtoBuf.Wcf.Channels.Bindings.ProtoBufBindingEndpointBehaviour, ProtoBuf.Wcf.Channels" />
        <add name="flightBehaviourExtension" type="Tavisca.TravelNxt.Common.Service.Inspectors.Header.ServiceBehaviour, Tavisca.TravelNxt.Common.Service.Inspectors" />
        <add name="flightEndpointBehaviourExtension" type="Tavisca.TravelNxt.Common.Service.Inspectors.Header.EndpointBehaviour, Tavisca.TravelNxt.Common.Service.Inspectors" />
      </behaviorExtensions>
    </extensions>
  </system.serviceModel>
  <connectionStrings>
    <add name="loadTest" connectionString="Data Source=cylon;Initial Catalog=dLoadTest_V_4_4_0_0;Integrated Security=True" />
  </connectionStrings>
  <system.net>
    <connectionManagement>
      <add address="*" maxconnection="999" />
    </connectionManagement>
  </system.net>
</configuration>
