﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Avail.Service.Test.AirEngine;
using System.Configuration;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Avail.Service.Test.AirSeatMaps;
using Tavisca.Frameworks.Parallel.Ambience;
using System.Globalization;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Service.Test
{
    /// <summary>
    /// Summary description for AirSeatMapTests
    /// </summary>
    [TestClass]
    public class AirSeatMapTests
    {
        private static AirSearchTests test = new AirSearchTests();

        private static List<Tuple<string, CallContext, FlightSearchRS>> AirSearchResposes = new List<Tuple<string, CallContext, FlightSearchRS>>();
        private static string BaseAirSeatMapAddress = GetTestSetting("BaseAirSeatMapAddress");
        private static string[] accountDetailsRaw = GetTestSetting("AccountIdPosPair").Split(',');
        private static List<string> bindingAddress = new List<string>();
        private static readonly string[] bindings = GetTestSetting("Bindings").ToLower().Split(',');
        private static string sessionId;

        private static List<string> bindingTypes = new List<string>();
        private static List<string> webHttpBindingContentTypes = new List<string>();

        public static List<Tuple<string, string, string>> AccountDetails = new List<Tuple<string, string, string>>();

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            if (string.IsNullOrEmpty(BaseAirSeatMapAddress))
                throw new InternalTestFailureException("Aborting test..Invalid or empty Base Airline address....");
            else
                SetupBindings();

            if (accountDetailsRaw.Length == 0)
                throw new InternalTestFailureException("Aborting test..Invalid or empty Account details....");

            PrepareSearchTestResults();

            if (AirSearchResposes.Count == 0)
                throw new InternalTestFailureException("Could not start AirSeatMap Test because 0 ItineraryRecommendations or LegRecommendations found in all the search requests made in ClassInitilize method....");
        }

        [TestMethod]
        public void AirSeatMap()
        {
            int responseNumber = StaticRandom.Instance.Next(AirSearchResposes.Count);
            var accountData = AccountDetails[StaticRandom.Instance.Next(AirSearchTests.AccountDetails.Count)];
            int randomBinding = StaticRandom.Instance.Next(bindingAddress.Count);
            SeatMapRQ seatMapRequest = new SeatMapRQ();
            
            CallContext context = new CallContext(GetTestSetting("MockCulture"),
                             AirSearchResposes[responseNumber].Item1, accountData.Item2.ToString(CultureInfo.InvariantCulture),
                             accountData.Item1, GetTestSetting("DisplayCurrency"),
                             GetTestSetting("Password"), false, string.Empty);
            SeatMapRS seatmapResponse = null;
            if (bindingTypes[randomBinding].ToLower().Equals("basichttpbinding_iseatmap") || bindingTypes[randomBinding].ToLower().Equals("protobufbinding_iairseatmap"))
            {
                using (var scope = new AmbientContextScope(context))
                {
                    AirSeatMaps.AirSeatMapClient client = new AirSeatMapClient(bindingTypes[randomBinding], bindingAddress[randomBinding]);
                    var request = GetSeatmapRequest(AirSearchResposes[responseNumber].Item3);
                    using (new AmbientContextScope(context))
                        seatmapResponse=client.GetSeatMap(request);
                    
                }
            }
            else if (bindingTypes[randomBinding].ToLower().Equals("webhttpbinding_iairseatmap"))
            {
                int randomContentType = StaticRandom.Instance.Next(webHttpBindingContentTypes.Count);
                using (var webClient = GetWebClient(webHttpBindingContentTypes[randomContentType], accountData.Item1, accountData.Item2))
                {
                    seatMapRequest = GetSeatmapRequest(AirSearchResposes[responseNumber].Item3);
                    var serializedRequest = GetSerializedRequest(seatMapRequest, webHttpBindingContentTypes[randomContentType]);
                    var bytesResponse = webClient.UploadData(bindingAddress[randomBinding] + "/getseatmap",
                        Encoding.UTF8.GetBytes(serializedRequest));
                    seatmapResponse = GetResponseFromBytes(bytesResponse, webHttpBindingContentTypes[randomContentType]);
                }
            }
            ValidateResponse(seatmapResponse);
        }

        private static void ValidateResponse(SeatMapRS seatmapResponse)
        {
            Assert.IsNotNull(seatmapResponse, "SeatMap response is null...");
            Assert.IsNotNull(seatmapResponse.SeatMap, "SeatMaps are null...");
            Assert.IsNotNull(seatmapResponse.ServiceStatus, "Service status is null...");
            StringBuilder statusMessage = new StringBuilder();
            if (!string.Equals(Tavisca.TravelNxt.Avail.Service.Test.AirSeatMaps.CallStatus.Success, seatmapResponse.ServiceStatus.Status))
            {
                if (!string.IsNullOrEmpty(seatmapResponse.ServiceStatus.StatusMessage))
                    statusMessage.AppendLine(seatmapResponse.ServiceStatus.StatusMessage);
                if (seatmapResponse.ServiceStatus.Messages.Length > 0)
                {
                    foreach (string message in seatmapResponse.ServiceStatus.Messages)
                        statusMessage.AppendLine(message);
                }
            }
            Assert.AreEqual(Tavisca.TravelNxt.Avail.Service.Test.AirSeatMaps.CallStatus.Success, seatmapResponse.ServiceStatus.Status,
                statusMessage.ToString());
            Assert.IsTrue(seatmapResponse.SeatMap.Cabins != null && seatmapResponse.SeatMap.Cabins.Length > 0,"SeatMap Cabins length is 0");
        }
        
        private static SeatMapRS GetSeatmapResponse(FlightSearchRS searchResponse, CallContext callContext,AirSeatMapClient client ,List<string> airlines = null, int flightNumberLength = 0)
        {
            var request = GetSeatmapRequest(searchResponse, airlines, flightNumberLength);
            
            using (new AmbientContextScope(callContext))
                return client.GetSeatMap(request);
        }

        private static SeatMapRQ GetSeatmapRequest(FlightSearchRS searchResponse, List<string> airlines = null, int flightNumberLength = 0)
        {
            int segmentRef = -1;
            var recommendation = GetSingleRecommendation(searchResponse, ref segmentRef, airlines, flightNumberLength);
            var recommendationLegRef = recommendation.LegRefs[0].Split('/');
            var legAlternateIndex = int.Parse(recommendationLegRef[0]);
            var lefRefId = int.Parse(recommendationLegRef[1]);

            var leg = searchResponse.LegAlternates[legAlternateIndex].Legs.First(l => l.RefID == lefRefId);

            var segment =
                searchResponse.Segments.First(
                    x => (segmentRef == -1) ? x.RefId == leg.FlightSegmentRefs.First() : x.RefId == segmentRef);

            return new SeatMapRQ()
            {
                RecommendationRefId = recommendation.RefID,
                SegmentKey = segment.Key
            };
        }

        private static FlightRecommendation GetSingleRecommendation(FlightSearchRS searchResponse, ref int segmentRefId, List<string> airlines = null, int flightNumberLength = 0)
        {
            if (airlines == null)
            {
                if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                    return searchResponse.LegRecommendations[0];

                return searchResponse.ItineraryRecommendations[0];
            }

            var segment =
                searchResponse.Segments.First(
                    x =>
                    (airlines.Contains(x.MarketingAirlineCode)) &&
                    (flightNumberLength == 0 || x.FlightNumber.Length <= flightNumberLength)).
                    RefId;
            var leg =
                searchResponse.LegAlternates.SelectMany(x => x.Legs).First(x => x.FlightSegmentRefs.Contains(segment)).
                    RefID;
            var allRecommendations = new List<FlightRecommendation>();
            if (searchResponse.LegRecommendations != null)
                allRecommendations.AddRange(searchResponse.LegRecommendations);

            if (searchResponse.ItineraryRecommendations != null)
                allRecommendations.AddRange(searchResponse.ItineraryRecommendations);

            segmentRefId = segment;

            return allRecommendations.First(x => x.LegRefs.Any(y => int.Parse(y.Split('/')[1]) == leg));
        }

       
        private static void PrepareSearchTestResults()
        {
            CallContext AirSearchCallContext;
            string[] accountIdPOSPair;
            foreach (string detail in accountDetailsRaw)
            {
                accountIdPOSPair = detail.Split(':');
                if (accountIdPOSPair.Length == 3)
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], accountIdPOSPair[2]));
                else
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], "ANY"));

                sessionId = Guid.NewGuid().ToString();
                AirSearchCallContext = new CallContext(GetTestSetting("MockCulture"), sessionId,
                accountIdPOSPair[1], accountIdPOSPair[0], GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty);
                AirEngine.FlightSearchRS rs;
                rs = test.AirSearch(SearchRequestFactory.TripType.RoundTrip, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, AirSearchCallContext, accountIdPOSPair[2]);

                if (string.IsNullOrEmpty(rs.SessionID.ToString()) || rs.SessionID.Equals(sessionId))
                    throw new InternalTestFailureException("Null or Empty SessionId found in one of the AirSearch Response made in ClassInitialize method....");

                if ((rs.LegRecommendations != null && rs.LegRecommendations.Count > 0) || (rs.ItineraryRecommendations != null && rs.ItineraryRecommendations.Count > 0))
                {
                    AirSearchResposes.Add(Tuple.Create(sessionId, AirSearchCallContext, rs));
                }
            }
        }


        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public static void SetupBindings()
        {
            foreach (string binding in bindings)
            {

                if (binding.Equals("protobufbinding"))
                {
                    bindingAddress.Add(BaseAirSeatMapAddress + "/proto");
                    bindingTypes.Add("protoBufBinding_IAirSeatMap");
                }
                else if (binding.Equals("restbindingwithjson"))
                {
                    bindingAddress.Add(BaseAirSeatMapAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/json");
                    bindingTypes.Add("webHttpBinding_IAirSeatMap");
                }
                else if (binding.Equals("restbindingwithxml"))
                {
                    bindingAddress.Add(BaseAirSeatMapAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/xml");
                    bindingTypes.Add("webHttpBinding_IAirSeatMap");
                }
                else if (binding.Equals("basichttpbinding"))
                {
                    bindingAddress.Add(BaseAirSeatMapAddress);
                    bindingTypes.Add("BasicHttpBinding_ISeatMap");
                }
                else
                    throw new InternalTestFailureException("Invlid inputs provided for AirSeatMap in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
            }
            if (bindingAddress.Count == 0)
                throw new InternalTestFailureException("Invlid inputs provided for AirSeatMap in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
        }

        private string GetSerializedRequest(SeatMapRQ flightSearchRequest, string contentType)
        {
            string response;
            response = contentType == "text/json" ? Serialize(flightSearchRequest) : XMLSerialize(flightSearchRequest);
            return response;
        }

        private SeatMapRS GetResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? Deserialize<SeatMapRS>(byteResponse) : XmlDeserialize<SeatMapRS>(byteResponse);
            return response;
        }

        private static T XmlDeserialize<T>(byte[] data)
        {
            var ser = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }

        private string Serialize<T>(T obj)
        {
            var ser = new DataContractJsonSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private string XMLSerialize<T>(T obj)
        {
            var ser = new DataContractSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private T Deserialize<T>(byte[] data)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }


        private WebClient GetWebClient(string contentType, string AccountId, string pos)
        {
            var client = new WebClient();
            client.Headers.Add("culture", GetTestSetting("MockCulture"));
            client.Headers.Add("posId", pos);
            client.Headers.Add("accountId", AccountId);
            client.Headers.Add("displayCurrency", GetTestSetting("DisplayCurrency"));
            client.Headers.Add("password", GetTestSetting("Password"));
            client.Headers.Add("sessionId", Guid.NewGuid().ToString());
            client.Headers.Add("Content-Type", contentType);
            client.Headers.Add("Accept-Type", contentType);
            return client;
        }
    }
}
