﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProtoBuf.Wcf.Channels.Bindings;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Avail.Service.Test.AirEngine;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Flight.Data.AirBook.Database;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Service.Test
{
    [TestClass]
    public class AirSearchTests
    {
        private static readonly ConcurrentStack<Guid> SessionIds = new ConcurrentStack<Guid>();
        private static string baseAirlineAddress = GetTestSetting("BaseAirEngineAddress");

        private static List<string> bindingAddress = new List<string>();
        private static List<string> bindingTypes = new List<string>();
        private static List<string> webHttpBindingContentTypes = new List<string>();
        private static readonly string[] bindings = GetTestSetting("Bindings").ToLower().Split(',');
        private static string[] accountDetailsRaw = GetTestSetting("AccountIdPosPair").Split(',');
        public static List<Tuple<string, string, string>> AccountDetails = new List<Tuple<string, string, string>>();


        static AirSearchTests()
        {
            MockClientInspector.SessionRetriever = obj => SearchRequestFactory.SessionId;
            SearchRequestFactory.DoMock = false;
        }

        [ClassInitialize]
        public static void InitilizeScenarioData(TestContext context)
        {
            if (string.IsNullOrEmpty(baseAirlineAddress))
                throw new InternalTestFailureException("Aborting test..Invalid or empty Base Airline address....");
            else
                SetupBindings();

            if (accountDetailsRaw.Length == 0)
                throw new InternalTestFailureException("Aborting test..Invalid or empty Account details....");
            else
                SetupAccountDetails();
        }

        private static int _count;

        # region LoadTestCases
        [TestMethod, TestCategory("Service Tests")]
        public void AvailOneWayTripForAdult()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailOneWayTripForSeniorCitizens()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.SeniorCouple, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailOneWayTripForInfantWithParents()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.InfantWIthParents, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailOneWayTripForFamily()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.Family, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailRoundTripForAdult()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.RoundTrip, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailRoundTripForSeniorCitizens()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.RoundTrip, SearchRequestFactory.PassangerInfoSummaryType.SeniorCouple, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailRoundTripForInfantWithParents()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.RoundTrip, SearchRequestFactory.PassangerInfoSummaryType.InfantWIthParents, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailRoundTripForFamily()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.RoundTrip, SearchRequestFactory.PassangerInfoSummaryType.Family, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailMulticityTripForAdult()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.MultiCity, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailMulticityTripForSeniorCitizens()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.MultiCity, SearchRequestFactory.PassangerInfoSummaryType.SeniorCouple, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailMulticityTripForInfantWithParents()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.MultiCity, SearchRequestFactory.PassangerInfoSummaryType.InfantWIthParents, out message);
            Assert.IsTrue(result, message);
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailMulticityTripForFamily()
        {
            string message = string.Empty;
            bool result = AirSearch(SearchRequestFactory.TripType.MultiCity, SearchRequestFactory.PassangerInfoSummaryType.Family, out message);
            Assert.IsTrue(result, message);
        }

        # endregion

        public bool AirSearch(SearchRequestFactory.TripType tripType, SearchRequestFactory.PassangerInfoSummaryType passangerInfo, out string response)
        {
            var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];
            var req = SearchRequestFactory.GetTypicalSearchRequest(tripType, passangerInfo, accountData.Item3);
            req.DoDeferredSearch = false;
            FlightSearchRS res = null;

            int randomBinding = StaticRandom.Instance.Next(bindingAddress.Count);


            if (bindingTypes[randomBinding].ToLower().Equals("basichttpbinding_iairengine") || bindingTypes[randomBinding].ToLower().Equals("protobufbinding_iairengine"))
            {
                using (var client = GetClient(randomBinding))
                {
                    using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                                Guid.NewGuid().ToString(), accountData.Item2,
                                accountData.Item1, GetTestSetting("DisplayCurrency"),
                                GetTestSetting("Password"), false, string.Empty)))
                    {
                        res = client.Search(req);
                    }
                }
            }
            else if (bindingTypes[randomBinding].ToLower().Equals("webhttpbinding_iairengine"))
            {
                int randomContentType = StaticRandom.Instance.Next(webHttpBindingContentTypes.Count);
                using (var client = GetWebClient(webHttpBindingContentTypes[randomContentType], accountData.Item1, accountData.Item2))
                {
                    var serializedRequest = GetSerializedRequest(req, webHttpBindingContentTypes[randomContentType]);
                    var bytesResponse = client.UploadData(bindingAddress[randomBinding] + "/search",
                        Encoding.UTF8.GetBytes(serializedRequest));
                    res = GetResponseFromBytes(bytesResponse, webHttpBindingContentTypes[randomContentType]);
                }
            }
            return ValidateResponse(res, out response);
        }


        public FlightSearchRS AirSearch(SearchRequestFactory.TripType tripType, SearchRequestFactory.PassangerInfoSummaryType passangerInfo, AmbientContextBase context, string cityPair)
        {
            var req = SearchRequestFactory.GetTypicalSearchRequest(tripType, passangerInfo, cityPair);
            req.DoDeferredSearch = false;
            FlightSearchRS res = null;
            using (var client = new AirEngineClient("BasicHttpBinding_IAirEngine", GetTestSetting("BaseAirEngineAddress")))
            {
                using (var scope = new AmbientContextScope(context))
                {
                    res = client.Search(req);
                }
            }
            return res;
        }


        private string GetSerializedRequest(FlightSearchRQ flightSearchRequest, string contentType)
        {
            string response;
            response = contentType == "text/json" ? Serialize(flightSearchRequest) : XMLSerialize(flightSearchRequest);
            return response;
        }

        private string GetSerializedRequest(FlightPollRQ flightSearchRequest, string contentType)
        {
            string response;
            response = contentType == "text/json" ? Serialize(flightSearchRequest) : XMLSerialize(flightSearchRequest);
            return response;
        }

        private FlightSearchRS GetResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? Deserialize<FlightSearchRS>(byteResponse) : XmlDeserialize<FlightSearchRS>(byteResponse);
            return response;
        }

        private static T XmlDeserialize<T>(byte[] data)
        {
            var ser = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }

        private WebClient GetWebClient(string contentType, string AccountId, string pos)
        {
            var client = new WebClient();
            client.Headers.Add("culture", GetTestSetting("MockCulture"));
            client.Headers.Add("posId", pos);
            client.Headers.Add("accountId", AccountId);
            client.Headers.Add("displayCurrency", GetTestSetting("DisplayCurrency"));
            client.Headers.Add("password", GetTestSetting("Password"));
            client.Headers.Add("sessionId", Guid.NewGuid().ToString());
            client.Headers.Add("Content-Type", contentType);
            client.Headers.Add("Accept-Type", contentType);
            return client;
        }

        public static void SetupAccountDetails()
        {
            foreach (string detail in accountDetailsRaw)
            {
                string[] accountIdPOSPair = detail.Split(':');
                if (accountIdPOSPair.Length == 3)
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], accountIdPOSPair[2]));
                else
                    AccountDetails.Add(Tuple.Create(accountIdPOSPair[0], accountIdPOSPair[1], "ANY"));
            }
        }

        public static void SetupBindings()
        {
            foreach (string binding in bindings)
            {

                if (binding.Equals("protobufbinding"))
                {
                    bindingAddress.Add(baseAirlineAddress + "/proto");
                    bindingTypes.Add("protoBufBinding_IAirEngine");
                }
                else if (binding.Equals("restbindingwithjson"))
                {
                    bindingAddress.Add(baseAirlineAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/json");
                    bindingTypes.Add("webHttpBinding_IAirEngine");
                }
                else if (binding.Equals("restbindingwithxml"))
                {
                    bindingAddress.Add(baseAirlineAddress + "/rest");
                    webHttpBindingContentTypes.Add("text/xml");
                    bindingTypes.Add("webHttpBinding_IAirEngine");
                }
                else if (binding.Equals("basichttpbinding"))
                {
                    bindingAddress.Add(baseAirlineAddress);
                    bindingTypes.Add("BasicHttpBinding_IAirEngine");
                }
                else
                    throw new InternalTestFailureException("Invlid inputs provided in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
            }
            if (bindingAddress.Count == 0)
                throw new InternalTestFailureException("Invlid inputs provided in app.config for Bindings tag...Possible inputs are:  BasicHttpBinding,ProtoBufBinding,RestBindingWithJSON,ResBindingWithXML or mulitple comma(,) seperated bindings are also supported");
        }

        private bool ValidateResponse(FlightSearchRS res, out string result)
        {
            StringBuilder message = new StringBuilder();
            bool isSuccesfull = false;

            if (res == null)
            {
                message.AppendLine("Response is NULL");
                isSuccesfull = false;
            }

            else if (CallStatus.Failure == res.ServiceStatus.Status)
            {
                var messages = string.Empty;// = res.ServiceStatus.Messages.Count==0 ?? new List<string>() { "error message was missing in the status" };
                if (res.ServiceStatus.Messages != null && res.ServiceStatus.Messages.Count > 0)
                    message.AppendLine(messages[0].ToString());
                else if (res.ServiceStatus.StatusMessage.Length > 0)
                    message.AppendLine(res.ServiceStatus.StatusMessage + " Failure Status Code: " + res.ServiceStatus.StatusCode);

                isSuccesfull = false;
            }
            else if (res.LegAlternates == null && res.ItineraryRecommendations == null)
            {
                message.AppendLine("LegAlternates and ItineraryRecommendations are NULL(Supplier Failure)");
                isSuccesfull = false;
            }
            else if ((res.LegRecommendations != null && res.LegRecommendations.Count == 0) && res.ItineraryRecommendations.Count == 0)
            {
                message.AppendLine("No Results Found");
                isSuccesfull = false;
            }
            else
                isSuccesfull = true;

            result = message.ToString();

            return isSuccesfull;
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailOneWay()
        {
            // var pos = GetTestPosWithHint();

            var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];

            var req = SearchRequestFactory.GetTypicalSearchRequest(true, accountData.Item3);

            //for (int i = 0; i < 2; i++)
            //{
            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            var res = GetSearchResult(req, accountData.Item1, accountData.Item2);

            Console.WriteLine(res.SessionID);

            SessionIds.Push(res.SessionID);

            stopwatch.Stop();
            var timetaken = stopwatch.ElapsedSeconds;

            Console.WriteLine(timetaken.ToString(CultureInfo.InvariantCulture));
            //}
        }

        [TestMethod, TestCategory("Service Tests")]
        public void AvailRoundTrip()
        {
            // var pos = GetTestPosWithHint();
            var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];

            var req = SearchRequestFactory.GetTypicalSearchRequest(false, accountData.Item3);

            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            var res = GetSearchResult(req, accountData.Item1, accountData.Item2);

            Console.WriteLine(res.SessionID);

            SessionIds.Push(res.SessionID);

            stopwatch.Stop();
            var timetaken = stopwatch.ElapsedSeconds;

            Console.WriteLine(timetaken.ToString(CultureInfo.InvariantCulture));
        }

       // [TestMethod, TestCategory("Service Tests")]
        public void GetFromSession()
        {
            Guid sessionId = Guid.Parse("9ad86b48-4185-4adc-968f-2ea5bf412d64");

            if (!SessionIds.TryPeek(out sessionId))
            {
               // AvailOneWay();
                string message;
              bool success=  AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, out message);

                SessionIds.TryPeek(out sessionId);
            }

            var req = new FlightPollRQ();

            // var posPair = GetTestPosWithHint();
            var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];
             int randomContentType = StaticRandom.Instance.Next(webHttpBindingContentTypes.Count);
            FlightSearchRS res;
            using (var client = GetWebClient(webHttpBindingContentTypes[randomContentType], accountData.Item1, accountData.Item2))
            {
                using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                    sessionId.ToString(), accountData.Item2.ToString(CultureInfo.InvariantCulture), accountData.Item1,
                    GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                {
                    //res = client.TryGetFromSession(req);
                    int randomBinding = StaticRandom.Instance.Next(bindingAddress.Count);
                   
                    var serializedRequest = GetSerializedRequest(req, webHttpBindingContentTypes[randomContentType]);
                    var bytesResponse = client.UploadData(bindingAddress[randomBinding] + "/search",
                        Encoding.UTF8.GetBytes(serializedRequest));


                    res = GetResponseFromBytes(bytesResponse, webHttpBindingContentTypes[randomContentType]);
                }
            }
            var messages = res.ServiceStatus.Messages ?? new List<string>() { "error message was missing in the status" };

            Assert.IsNotNull(res);
            Assert.AreNotEqual(CallStatus.Failure, res.ServiceStatus.Status,
                    string.Join(",", messages));

            Assert.IsFalse(res.LegAlternates == null && res.ItineraryRecommendations == null);
            Assert.IsTrue((res.LegRecommendations != null && res.LegRecommendations.Count > 0) || res.ItineraryRecommendations.Count > 0);
        }

        //[TestMethod, TestCategory("Service Tests")]
        public void AvailJsonTest()
        {
            //var pos = GetTestPosWithHint();
            var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];

            var req = SearchRequestFactory.GetTypicalSearchRequest(true, accountData.Item3);

            //var json = Serialize(req);

            var stopwatch = new WrappedTimer();
            stopwatch.Start();
            try
            {
                var res = GetWebHttpBindingResult(req, accountData.Item1, accountData.Item2);
                // Serialize(res);
                //Console.WriteLine(res.SessionID);
            }
            catch (Exception ex)
            {
                if (_count == 0)
                {
                    var message = new StringBuilder();

                    message.AppendLine(ex.Message);

                    var inner = ex.InnerException;
                    while (inner != null)
                    {
                        message.AppendLine("------------------------------");
                        message.AppendLine("------------------------------");
                        message.AppendLine("------------------------------");
                        message.AppendLine("------------------------------");

                        message.AppendLine(inner.Message);

                        inner = inner.InnerException;
                    }

                    _count++;
                    File.AppendAllText(@"d:\log\blah.txt", message.ToString());
                }
                throw;
            }
            stopwatch.Stop();
            var timetaken = stopwatch.ElapsedSeconds;
            Console.Write(timetaken.ToString(CultureInfo.InvariantCulture));
        }

        public FlightSearchRS GetWebHttpBindingResult(FlightSearchRQ request, string accountId, string pos)
        {
            byte[] response;

            using (var client = new WebClient())
            {
                client.Headers.Add("culture", GetTestSetting("MockCulture"));
                client.Headers.Add("accountId", accountId);
                client.Headers.Add("posId", pos.ToString(CultureInfo.InvariantCulture));
                client.Headers.Add("displayCurrency", GetTestSetting("DisplayCurrency"));
                client.Headers.Add("password", GetTestSetting("Password"));
                client.Headers.Add("Content-Type", "text/json");
                client.Headers.Add("Accept-Type", "text/json");

                var json = Serialize(request);

                response = client.UploadData("http://dev-apis.tavisca.com/Engines/Oski/AirEngine/AirEngine.svc/rest/search", Encoding.UTF8.GetBytes(json));
            }

            return null; //Deserialize<FlightSearchRS>(response);
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public FlightSearchRS GetSearchResult(FlightSearchRQ request, string accountId, string pos)
        {
            FlightSearchRS res;
            using (var client = GetClient(StaticRandom.Instance.Next(bindingAddress.Count)))
            {
                using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                            Guid.NewGuid().ToString(), pos.ToString(CultureInfo.InvariantCulture),
                            accountId, GetTestSetting("DisplayCurrency"),
                            GetTestSetting("Password"), false, string.Empty)))
                {
                    res = client.Search(request);
                }
            }

            Assert.IsNotNull(res);

            var messages = res.ServiceStatus.Messages ?? new List<string>() { "error message was missing in the status" };

            Assert.AreNotEqual(CallStatus.Failure, res.ServiceStatus.Status,
                string.Join(",", messages));

            Assert.IsFalse(res.LegAlternates == null && res.ItineraryRecommendations == null);
            Assert.IsTrue((res.LegRecommendations != null && res.LegRecommendations.Count > 0) || res.ItineraryRecommendations.Count > 0);

            return res;

        }


        private string Serialize<T>(T obj)
        {
            var ser = new DataContractJsonSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private string XMLSerialize<T>(T obj)
        {
            var ser = new DataContractSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }

        private T Deserialize<T>(byte[] data)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }

       // [TestMethod, TestCategory("Service Tests")]
        public void DeferredSearchTest()
        {
            try
            {
               // var sessionId = Guid.NewGuid();

                // var pos = GetTestPosWithHint();
                var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];

                //FlightSearchRS res;
                //using (var client = GetClient(StaticRandom.Instance.Next(bindingAddress.Count)))
                //{
                //    var req = //SearchRequestFactory.GetDeferredSearchRequest(true, accountData.Item3);

                //    using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                //    sessionId.ToString(), accountData.Item2.ToString(CultureInfo.InvariantCulture), accountData.Item1,
                //    GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                //    {
                //        res = client.Search(req);
                //    }
                //}

                //Assert.IsNotNull(res);

                //Assert.IsTrue(res.InProgress);
               Guid  sessionId = Guid.NewGuid();
                 CallContext AirSearchCallContext = new CallContext(GetTestSetting("MockCulture"), sessionId.ToString(),
                accountData.Item2, accountData.Item1, GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty);
                 var res = AirSearch(SearchRequestFactory.TripType.OneWay, SearchRequestFactory.PassangerInfoSummaryType.OneAdult, AirSearchCallContext, accountData.Item3);

                TryGetAvailTest(false, accountData, sessionId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[TestMethod, TestCategory("Service Tests")]
        public void DeferredSearchTestWithSupplierStreaming()
        {
            try
            {
                var sessionId = Guid.NewGuid();

                //  var pos = GetTestPosWithHint();
                var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];

                FlightSearchRS res;
                using (var client = GetClient(StaticRandom.Instance.Next(bindingAddress.Count)))
                {
                    var req = SearchRequestFactory.GetDeferredSearchRequestWithSupplierStreaming(true, accountData.Item3);

                    using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                    sessionId.ToString(), accountData.Item2.ToString(CultureInfo.InvariantCulture), accountData.Item1,
                    GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                    {
                        res = client.Search(req);
                    }
                }

                Assert.IsNotNull(res);

                Assert.IsTrue(res.InProgress);

                TryGetAvailTest(true, accountData, sessionId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void TryGetAvailTest(bool streamSuppliers, Tuple<string, string, string> accountData, Guid sessionId)
        {
            using (var client = GetClient(StaticRandom.Instance.Next(bindingAddress.Count)))
            {
                var flightReq = new FlightPollRQ() { GetAsSupplierResponds = streamSuppliers };
                FlightSearchRS res;
                using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                   sessionId.ToString(), accountData.Item2.ToString(CultureInfo.InvariantCulture), accountData.Item1,
                   GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), true, string.Empty)))
                {
                    res = client.TryGetFromSession(flightReq);
                }

                Assert.IsNotNull(res);

                Assert.IsTrue(res.InProgress ||
                              (res.LegRecommendations.Count > 0 || res.ItineraryRecommendations.Count > 0));

                var stopwatch = new WrappedTimer();
                stopwatch.Start();

                while (res.InProgress)
                {
                    using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                   sessionId.ToString(), accountData.Item2.ToString(CultureInfo.InvariantCulture), accountData.Item1,
                   GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                    {
                        res = client.TryGetFromSession(new FlightPollRQ()
                        {
                            GetAsSupplierResponds = streamSuppliers,
                            PreviousTimeStamp = res.TimeStamp
                        });
                    }

                    if (res.InProgress)
                        Thread.Sleep(1000);

                    if (stopwatch.ElapsedSeconds > SearchRequestFactory.WaitTimeOut)
                        break;
                }

                Assert.IsNotNull(res, "Search Response is Null");
                Assert.IsNotNull(res.LegRecommendations, "LegRecommendations are null...");

                Assert.IsTrue(res.LegRecommendations.Count > 0 || res.ItineraryRecommendations.Count > 0);
            }
        }
        /*  private Tuple<int, string> GetTestPosWithHint()
          {
              var accountData = AccountDetails[StaticRandom.Instance.Next(AccountDetails.Count)];
              var random = new Random(Guid.NewGuid().GetHashCode()).Next(0, PosConfigs.Length);

              var pair = PosConfigs[random].Split(':');

              if (pair.Length == 0)
                  throw new InternalTestFailureException("The test failed due to incorrect test configuration for MockPosId in app.config, check to see if it has an empty ',' in it.");

              var posId = int.Parse(pair[0]);

              if (pair.Length == 1)
                  return new Tuple<int, string>(posId, null);

              return new Tuple<int, string>(posId, pair[1]);
          }*/

        private static AirEngineClient GetClient(int number)
        {
            return new AirEngineClient(bindingTypes[number], bindingAddress[number]);
        }

        private static AirEngineClient GetClient()
        {
            return new AirEngineClient();
        }
    }
}
