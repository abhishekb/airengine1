﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Avail.Service.Test.Console.AirEngine;

namespace Tavisca.TravelNxt.Avail.Service.Test.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new AirEngineClient();
            var req = new FlightSearchRQ { Requester = new Requester(), SearchCriterion = new FlightSearchCriteria() };
            req.SearchCriterion.PassengerInfoSummary = new PaxTypeQuantity[1];
            req.SearchCriterion.PassengerInfoSummary[0] = new PaxTypeQuantity { Ages = new int[1] };
            req.SearchCriterion.PassengerInfoSummary[0].Ages[0] = 20;
            req.SearchCriterion.PassengerInfoSummary[0].Quantity = 1;
            req.SearchCriterion.PassengerInfoSummary[0].PassengerType = PassengerType.Adult;
            req.SearchCriterion.SearchSegments = new SearchSegment[1];
            req.SearchCriterion.SearchSegments[0] = new SearchSegment
            {
                ArrivalAirportCode = "LAS",
                DepartureAirportCode = "LAX",
                TravelDate =
                    new TravelDate
                    {
                        DateTime =
                            DateTime.Now
                            .AddMonths(3)

                    }
            };
            var res = client.Search(req);

            if (res == null)
                throw new Exception();
        }
    }
}
