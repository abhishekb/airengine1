﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Avail.Service.Test;
using Tavisca.TravelNxt.Avail.Service.Test.AirEngine;
using Tavisca.TravelNxt.Common.Service.Inspectors;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Price.Service.Test.AirPriceService;

namespace Tavisca.TravelNxt.Price.Service.Test
{
    public static class PriceRequestFactory
    {
        static PriceRequestFactory()
        {
            SearchRequestFactory.DoMock = true;

            if (MockClientInspector.SessionRetriever == null)
                MockClientInspector.SessionRetriever = obj => SearchRequestFactory.SessionId;
        }

        public static bool DoMock { get { return SearchRequestFactory.DoMock; } }

        public static P_AirPriceRQ GetBasePriceRequest()
        {
            var request = new P_AirPriceRQ()
                {
                    AdditionalInfo = new Dictionary<string, string>()
                };

            if (DoMock)
                request.AdditionalInfo.Add("mock", "true");

            return request;
        }

        public static P_AirPriceRQ GetTypicalPriceRequest()
        {
            var request = GetBasePriceRequest();

            var searchRequest = GetSearchRequest();

            GetSearchResponse(searchRequest);

            //request.Requester = new P_Requester()
            //    {
            //        RequesterOrigin = searchRequest.Requester.RequesterOrigin
            //    };




            //request.RecommendationRefID = searchRes.Recommendations
            //                                [random.Next(0, searchRes.Recommendations.Count - 1)].RefID;

            return request;
        }

        public static FlightSearchRS GetSearchResponse(FlightSearchRQ request)
        {
            var searchTest = new AirSearchTests();

            return searchTest.GetSearchResult(request, null,null); //TODO
        }

        public static FlightSearchRQ GetSearchRequest()
        {
            return SearchRequestFactory.GetTypicalSearchRequest(false, null);
        }

        public static List<int> PricedRecommendations { get; internal set; } 
    }
}
