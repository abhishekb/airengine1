﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Price.Service.Test.AirPriceService;

namespace Tavisca.TravelNxt.Price.Service.Test
{
    [TestClass]
    public class PriceTest
    {
        [TestMethod, TestCategory("Service Tests")]
        public void PriceRequestTest()
        {
            var request = PriceRequestFactory.GetTypicalPriceRequest();

            using (var client = new AirPriceClient())
            {
                client.Price(request);
                

                if (PriceRequestFactory.PricedRecommendations == null)
                    PriceRequestFactory.PricedRecommendations = new List<int>(1);

                PriceRequestFactory.PricedRecommendations.Add(request.RecommendationRefID);
                //Assert.IsTrue(res.);
            }
        }
    }
}
