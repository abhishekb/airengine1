﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace ComponentTest
{
    [TestClass]
    public class FlightSortTests : TestBase
    {
        #region Test Setup

        private static readonly List<PaxTypeFareBasis> PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                                                      {
                                                                                          new PaxTypeFareBasis()
                                                                                              {ClassOfService = "S"}
                                                                                      };

        private IFlightRecommendationComparer GetComparer()
        {
            return RuntimeContext.Resolver.Resolve<IFlightRecommendationComparer>();
        }

        private FlightRecommendation CreateRecommendation(decimal totalValue, double duration,
            DateTime departureDateTime, DateTime arrivalDateTime, string[] marketingAirlines = null)
        {
            var moneyparts = new Money[5];

            for (var i = 0; i < moneyparts.Length; i++)
            {
                var amt = totalValue / moneyparts.Length;

                moneyparts[i] = new Money(amt, "INR", amt, "INR", amt, "INR"); //only display currency should be considered in these tests.
            }

            if (marketingAirlines == null)
            {
                marketingAirlines = new[] { "9W", "9W", "9W", "9W" };
            }

            return new FlightRecommendation()
                {
                    Fare = new FlightFare()
                        {
                            PassengerFares = new List<PassengerFare>()
                                {
                                    new PassengerFare()
                                        {
                                            Quantity = 1,
                                            BaseAmount = moneyparts[0],
                                            Markups = new List<FareComponent>(){new FareComponent(){Value = moneyparts[1]}},
                                            Discounts = new List<FareComponent>(){new FareComponent(){Value = moneyparts[2]}},
                                            Fees = new List<FareComponent>(){new FareComponent(){Value = moneyparts[3]}},
                                            Taxes = new List<FareComponent>(){new FareComponent(){Value = moneyparts[4]}},
                                        }
                                }
                        },
                    Legs = new FlightLegCollection()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", departureDateTime, arrivalDateTime, 
                                                "9W-321", marketingAirlines[0], 0){DurationMinutes = Convert.ToInt32(Math.Ceiling(duration/4)), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", arrivalDateTime.AddHours(1), arrivalDateTime.AddHours(2), 
                                                "9W-322", marketingAirlines[1], 1){DurationMinutes = Convert.ToInt32(Math.Floor(duration/4)), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", departureDateTime.AddDays(1), arrivalDateTime.AddDays(1), 
                                                "9W-321", marketingAirlines[2], 0){DurationMinutes = Convert.ToInt32(Math.Ceiling(duration/4)), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", arrivalDateTime.AddDays(1).AddHours(1), arrivalDateTime.AddDays(1).AddHours(2), 
                                                "9W-322", marketingAirlines[3], 1){DurationMinutes = Convert.ToInt32(Math.Floor(duration/4)), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        }
                };
        }

        #endregion


        #region Tests

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDefaultBehaviourTest()
        {
            var comparer = GetComparer();

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);


            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result, "By Default sorting should be done on price low to high");
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDefaultInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);


            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result, "By Default sorting should be done on price low to high");
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDefaultEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareCustomBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPreference;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result,
                            "By Default sorting should be done on price low to high when custom is provided, 'custom' is provided for customizations.");
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareCustomInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPreference;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result,
                            "By Default sorting should be done on price low to high when custom is provided, 'custom' is provided for customizations.");
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareCustomEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPreference;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareLowToHighBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceLowToHigh;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareLowToHighInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceLowToHigh;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareLowToHighTimeEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceLowToHigh;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareHighToLowInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceHighToLow;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareHighToLowBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceHighToLow;
            
            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(110m, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareHighToLowTimeEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByPriceHighToLow;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareJourneyTimeBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByJourneyTime;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA + 10, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareJourneyTimeInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByJourneyTime;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA + 10, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareJourneyTimeEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByJourneyTime;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDepartureTimeInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByDepartureTime;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA.AddHours(1), arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDepartureTimeBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByDepartureTime;
            
            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA.AddHours(1), arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareDepartureEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByDepartureTime;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareArrivalTimeInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByArrivalTime;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA.AddHours(1));

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareArrivalTimeBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByArrivalTime;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA.AddHours(1));

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareArrivalEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByArrivalTime;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareAirlineBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByAirline;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA, new[] { "abc", "zzz", "zzz", "zzz" });

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA, new[] { "zzz", "abc", "abc", "abc" });

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(-1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareAirlineInvertedBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByAirline;
            comparer.SortByDesc = true;

            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA, new[] { "abc", "zzz", "zzz", "zzz" });

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA, new[] { "zzz", "abc", "abc", "abc" });

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(1, result);
        }

        [TestMethod, TestCategory("Sort Tests")]
        public void CompareAirlineEqualityBehaviourTest()
        {
            var comparer = GetComparer();

            comparer.SortingOrder = SortingOrder.ByAirline;
            
            var priceA = 100m;
            var durationA = 10;
            var departureA = DateTime.UtcNow.AddDays(3);
            var arrivalA = departureA.AddHours(2);

            var recommendationA = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            //B will be better in the tests target parameters and equal in every other case for apt testing.
            var recommendationB = CreateRecommendation(priceA, durationA, departureA, arrivalA);

            var result = comparer.Compare(recommendationA, recommendationB);

            Assert.AreEqual(0, result);
        }

        #endregion
    }
}
