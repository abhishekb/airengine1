﻿using System;
using System.Runtime.Caching;
using Tavisca.Frameworks.Session.Provider;
namespace ComponentTest.Mock
{
    public class MockSessionProvider : SessionDataProviderBase
    {
        public MockSessionProvider() : base(string.Empty)
        {
            
        }

        //public MockSessionProvider(string connStringNameOrValue) : base(connStringNameOrValue)
        //{
        //}


        public override void Add(string category, string key, object value, TimeSpan expireIn)
        {
            MemoryCache.Default.Add(key + category, value, new DateTimeOffset(DateTime.Now + expireIn));
        }

        public override T Get<T>(string category, string key)
        {
            return (T) MemoryCache.Default.Get(key + category);
        }

        public override bool Remove(string category, string key)
        {
            return true;
        }
    }
}
