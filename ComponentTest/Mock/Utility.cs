﻿using System.Runtime.Remoting.Messaging;

namespace ComponentTest.Mock
{
    internal static class TestUtility
    {
        public static void SetCurrentProviderName(string providerName)
        {
            CallContext.SetData(MockKeys.ContextKeys.ProviderName, providerName);
        }

        public static string GetCurrentProviderName()
        {
            return CallContext.GetData(MockKeys.ContextKeys.ProviderName) as string;
        }

        public static void SetCurrentCallType(string callType)
        {
            CallContext.SetData(MockKeys.ContextKeys.CallType, callType);
        }

        public static string GetCurrentCallType()
        {
            return CallContext.GetData(MockKeys.ContextKeys.CallType) as string;
        }
    }
}
