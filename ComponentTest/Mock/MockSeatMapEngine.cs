﻿using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.SeatMap.Core;
using KeyValueMap = Tavisca.TravelNxt.Flight.Data.Services.SeatMap;

namespace ComponentTest.Mock
{
    class MockSeatMapEngine : SeatMapEngine
    {
        
        #region Protected Members

        protected override FlightRecommendation GetRecommendationFromSession(int refNumber)
        {
            return MockDataProvider.GetRecommendationForCurrentProvider();

        }
        #endregion
    }
}
