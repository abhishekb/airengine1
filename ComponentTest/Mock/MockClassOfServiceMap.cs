﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Entities;

namespace ComponentTest.Mock
{
    public class MockClassOfServiceMap : ClassOfServiceMap
    {
        private static readonly IList<AirlineClassesOfService> OverrideAirlineClassesOfService = new List<AirlineClassesOfService>();
        private static readonly IList<AirlineCosZoneCountry> OverrideAirlineCosZoneCountries = new List<AirlineCosZoneCountry>();

        private static readonly ReaderWriterLockSlim AirlineClassesOfServiceLocker = new ReaderWriterLockSlim();
        private static readonly ReaderWriterLockSlim AirlineCosZoneCountryLocker = new ReaderWriterLockSlim();


        public static void AddOverrideAirlineClassesOfService(AirlineClassesOfService airlineClassesOfService)
        {
            AirlineClassesOfServiceLocker.EnterWriteLock();
            try
            {
                OverrideAirlineClassesOfService.Add(airlineClassesOfService);
            }
            finally
            {
                AirlineClassesOfServiceLocker.ExitWriteLock();
            }
        }

        public static void AddOverrideAirlineCosZoneCountry(AirlineCosZoneCountry airlineCosZoneCountry)
        {
            AirlineCosZoneCountryLocker.EnterWriteLock();
            try
            {
                OverrideAirlineCosZoneCountries.Add(airlineCosZoneCountry);
            }
            finally
            {
                AirlineCosZoneCountryLocker.ExitWriteLock();
            }
        }

        public static void ClearAllOverrides()
        {
            AirlineClassesOfServiceLocker.EnterWriteLock();
            try
            {
                OverrideAirlineClassesOfService.Clear();
            }
            finally
            {
                AirlineClassesOfServiceLocker.ExitWriteLock();
            }

            AirlineCosZoneCountryLocker.EnterWriteLock();
            try
            {
                OverrideAirlineCosZoneCountries.Clear();
            }
            finally
            {
                AirlineCosZoneCountryLocker.ExitWriteLock();
            }
        }

        protected override IList<AirlineClassesOfService> GetAirlineClassesOfServicesFromSlowStore()
        {
            var retVal = base.GetAirlineClassesOfServicesFromSlowStore();

            AirlineClassesOfServiceLocker.EnterReadLock();
            try
            {
                retVal = retVal.Concat(OverrideAirlineClassesOfService).ToListBuffered();
            }
            finally
            {
                AirlineClassesOfServiceLocker.ExitReadLock();
            }

            return retVal;
        }

        protected override IList<AirlineCosZoneCountry> GetAirlineCosZoneCountriesFromSlowStore()
        {
            var retVal = base.GetAirlineCosZoneCountriesFromSlowStore();

            AirlineCosZoneCountryLocker.EnterReadLock();
            try
            {
                retVal = retVal.Concat(OverrideAirlineCosZoneCountries).ToListBuffered();
            }
            finally
            {
                AirlineCosZoneCountryLocker.ExitReadLock();
            }

            return retVal;
        }

        public static CabinType InvokeCabinTypeParseMethod(string cabinType)
        {
            return ParseCabinClass(cabinType);
        }
    }
}
