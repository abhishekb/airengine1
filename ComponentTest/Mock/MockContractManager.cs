﻿using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;

namespace ComponentTest.Mock
{
    public class MockContractManager : ContractManager
    {
        protected override ICollection<AirContract> GetAllContracts()
        {
            var neededValue = CallContext.LogicalGetData("scenario");

            if(neededValue.Equals(MockKeys.TestScenario.ValidSearchContracts))
                return new List<AirContract>(ContractStore.ValidSearchContracts);

            if (neededValue.Equals(MockKeys.TestScenario.InvalidSearchValidPeriod))
                return new List<AirContract>(ContractStore.InvalidSearchValidPeriod);

            //reroute to test paper contracts
            if (neededValue.Equals(MockKeys.TestScenario.PaperContracts))
                return base.GetAllContracts();

            return new List<AirContract>(ContractStore.ValidSearchContracts);
        }

        protected override IList<AirContract> GetDbContracts()
        {
            return new List<AirContract>(ContractStore.ValidSearchContracts);
        }

        protected override IList<IPaperContract> GetPaperContracts()
        {
            return new List<IPaperContract>() {new TestPaperContract()};
        }
    }
}
