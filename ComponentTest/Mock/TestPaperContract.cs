﻿using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Contracts;
namespace ComponentTest.Mock
{
    public class TestPaperContract : DbAirContract, IPaperContract
    {
        public TestPaperContract()
        {
            this.ContractId = 123;
            this.ContractType = MockKeys.PaperContract;
        }

        public Tavisca.TravelNxt.Flight.Entities.AirContract GetContract()
        {
            return this;
        }

        public Tavisca.TravelNxt.Flight.Entities.AirContract GetOverriddenContract(Tavisca.TravelNxt.Flight.Entities.AirContract airContract)
        {
            this.SearchAirFareSourceId = airContract.SearchAirFareSourceId;
            return this;
        }

        public override bool IsContractApplicable(Tavisca.TravelNxt.Flight.Entities.Avail.FlightSearchCriteria searchCriteria)
        {
            return true;
        }

        public override bool CanSellRecommendation(Tavisca.TravelNxt.Flight.Entities.FlightRecommendation flightRecommendation)
        {
            return true;
        }
    }
}
