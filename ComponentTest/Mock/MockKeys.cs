﻿using System;
using System.Configuration;
using Tavisca.TravelNxt.Common.Settings;

namespace ComponentTest.Mock
{
    public static class MockKeys
    {
        public const string All = "*";
        public const string PaperContract = "PaperContract";

        public static class TestScenario
        {
            public const string ValidSearchContracts = "ValidSearchContracts";
            public const string InvalidSearchValidPeriod = "InvalidSearchValidPeriod";
            public const string PaperContracts = "PaperContracts";
            public const string ValidSaleContracts = "ValidSaleContracts";
        }

        public static class InvalidValues
        {
            public const string InvalidClassOfService = "I";
            public const string InvalidAirportPair = "ABC-DEF";
            public const string InvalidAirport1 = "ABC";
            public const string InvalidAirport2 = "DEF";
            public const int InvalidProvider = -1;
            public const string InvalidAirline = "I";
            public const string InvalidFareBasis = "I";
            public const string InvalidCabinType = "first";
            public const string InvalidInterceptLevel = "pre";
        }

        public static class ValidValues
        {
            public const string ValidClassOfService = "V";
            public const string ValidAirportPair = "LAS-LAX";
            public const string ValidAirport1 = "LAS";
            public const string ValidAirport2 = "LAX";
            public const int ValidProvider = 123;
            public const int ValidAgencyId = 123;
            public const string ValidAirline = "V";
            public const string ValidFareBasis = "V";
            public const string ValidCabinType = "economy";
            public const string ValidInterceptLevel = "post";
        }

        public static class AirlinePreference
        {
            public const string Include = "Include";
            public const string Exclude = "Exclude";
            public const string Only = "Only";
        }

        public static class SupplierCurrencyTestKeys
        {
            public const string PrimaryCurrencySupplier = "PrimaryCurrencySupplier";
            public const string SecondaryCurrencySupplier = "SecondaryCurrencySupplier";
            public const string TertiaryCurrencySupplier = "TertiaryCurrencySupplier";
            public const string DefaultCurrencySupplier = "DefaultCurrencySupplier";
            public const string NoContextTertiaryCurrencySupplier = "NoContextTertiaryCurrencySupplier";

            public const string PrimaryCurrency = "PrimaryCurrency";
            public const string SecondaryCurrency = "SecondaryCurrency";
            public const string TertiaryCurrency = "TertiaryCurrency";
            public const string DefaultCurrency = "DefaultCurrency";

            public const string Pos = "*";
        }

        public static class UriKeys
        {
            public const string SeatMapUri = "SeatMapUri";
            public const string FareRuleUri = "FareRuleUri";
        }

        public static class ContextKeys
        {
            public const string ProviderName = "ProviderName";
            public const string CallType = "CallType";
        }

        public static class ProviderNames
        {
            public const string Worldspan = "worldspan";
            public const string Sabre = "sabre";
            public const string TravelPort = "travelport";
            public const string Amadeus = "amadeusws";
        }

        public static class CallTypes
        {
            public const string SeatMap = "SeatMap";
            public const string FareRule = "FareRule";
        }
    }
}
