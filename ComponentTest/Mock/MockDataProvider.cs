﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;

namespace ComponentTest.Mock
{
    internal static class MockDataProvider
    {
        private static readonly XDocument AttributeDocument = GetAttributesDocument();

        private static XDocument FlightSegmentsDocument
        {
            get
            {
                return GetFlightSegmentsDocument();
            }
        }
        private static readonly XDocument FareDetailsDocument = GetFareDetailsDocument();

        private static readonly XDocument SeatMapData = XDocument.Load(Constants.SeatmapData);
        private static readonly XDocument FareRuleData = XDocument.Load(Constants.FareRulesData);

        public static FlightRecommendation GetRecommendationForCurrentProvider()
        {
            return GetFlightRecommendation(TestUtility.GetCurrentProviderName());
        }

        #region private members
        private static FlightRecommendation GetFlightRecommendation(string provider)
        {
            var recommendation = new FlightRecommendation
            {
                AdditionalInfo = new AdditionalInfoDictionary(),
                Legs = GetFlightLegCollection(),
                ProviderSpace = GetProviderSpace(provider)
            };
            return recommendation;
        }

        private static FlightLegCollection GetFlightLegCollection()
        {
            var leg = new FlightLeg();
            var fs = GetFlightSegment();
            leg.Segments.Add(fs);
            return new FlightLegCollection { leg };
        }

        private static FlightSegment GetFlightSegment()
        {
            var segmentNode = (from flightSegment in FlightSegmentsDocument.Descendants("FlightSegment")
                               let xAttribute = flightSegment.Attribute(Constants.ApplicableFor)
                               where xAttribute.Value.ToLower().Contains(TestUtility.GetCurrentProviderName())
                               select flightSegment).FirstOrDefault();

            return ToFlightSegment(segmentNode);
        }

        public static AdditionalInfoDictionary GetSupplierAttributes(string supplierIdentifier)
        {
            var supplierAttributes = new AdditionalInfoDictionary();

            var supplierNode = (from supplier in AttributeDocument.Descendants(Constants.SupplierNode)
                                let xAttribute = supplier.Attribute(Constants.Id)
                                where xAttribute != null && xAttribute.Value.Equals(supplierIdentifier)
                                select supplier).FirstOrDefault();

            if (supplierNode == null) return supplierAttributes;
            foreach (var attribute in supplierNode.Descendants(Constants.Attribute))
            {
                var nameAttribute = attribute.Attribute(Constants.Name);
                var valueAttribute = attribute.Attribute(Constants.Value);
                if (nameAttribute != null && !string.IsNullOrEmpty(nameAttribute.Value) && valueAttribute != null)
                    supplierAttributes.AddOrUpdate(nameAttribute.Value, valueAttribute.Value);
            }

            return supplierAttributes;
        }

        private static FlightSegment ToFlightSegment(XElement xElement)
        {
            string arrivalAirportCode =
                GetXElementValue(xElement.Descendants(Constants.ArrivalAirportCodeNode).FirstOrDefault());
            DateTime arrivalDateTime =
                DateTime.Parse(GetXElementValue(xElement.Descendants(Constants.ArrivalDateTimeNode).FirstOrDefault(),
                    DateTime.Now.AddDays(25).ToString()));
            DateTime departureDateTime =
                DateTime.Parse(GetXElementValue(xElement.Descendants(Constants.DepartureDateTimeNode).FirstOrDefault(),
                    DateTime.Now.AddDays(20).ToString()));
            string departureAirportCode =
                GetXElementValue(xElement.Descendants(Constants.DepartureAirportCodeNode).FirstOrDefault());
            string airlineCode = GetXElementValue(xElement.Descendants(Constants.AirlineCodeNode).FirstOrDefault());
            string flightNumber = GetXElementValue(xElement.Descendants(Constants.FlightNumberNode).FirstOrDefault());
            return new FlightSegment(departureAirportCode, arrivalAirportCode, departureDateTime, arrivalDateTime, flightNumber, airlineCode, 123) { PaxTypeFareBasisCodes = GetPaxTypeFareBasisCodes() };
        }

        private static string GetXElementValue(XElement xelement, string defaultValue = "")
        {
            return (xelement != null)
                       ? (string.IsNullOrEmpty(xelement.Value) ? defaultValue : xelement.Value)
                       : defaultValue;
        }

        private static XDocument GetAttributesDocument()
        {
            return XDocument.Load(Constants.FareSpaceData);
        }

        private static XDocument GetFlightSegmentsDocument()
        {
            return
                string.Equals(TestUtility.GetCurrentCallType(), MockKeys.CallTypes.FareRule)
                    ? FareRuleData
                    : SeatMapData;
        }

        private static XDocument GetFareDetailsDocument()
        {
            return XDocument.Load(Constants.FareDetailsDoc);
        }

        private static ProviderSpace GetProviderSpace(string provider)
        {
            var requester = new Requester(4, 101, CustomerType.Agent);
            var aid = GetSupplierAttributes(provider);
            var pscList = new List<ProviderSpaceConfiguration>();
            var providerSpaceConfigurationAirFareRule = new ProviderSpaceConfiguration
            {
                ConfigType = ConfigurationType.AirFareRules,
                ServiceUri = new Uri(System.Configuration.ConfigurationManager.AppSettings[MockKeys.UriKeys.FareRuleUri])
            };
            var providerSpaceConfigurationSeatMap = new ProviderSpaceConfiguration
            {
                ConfigType = ConfigurationType.SeatMap,
                ServiceUri = new Uri(System.Configuration.ConfigurationManager.AppSettings[MockKeys.UriKeys.SeatMapUri])
            };
            pscList.Add(providerSpaceConfigurationAirFareRule);
            pscList.Add(providerSpaceConfigurationSeatMap);
            return new ProviderSpace(110, provider, requester, provider, null, null, ProviderSpaceType.All, pscList, aid);
        }

        private static List<PaxTypeFareBasis> GetPaxTypeFareBasisCodes()
        {
            var segmentNode = (from fareDetail in FareDetailsDocument.Descendants("FareDetail")
                               let xAttribute = fareDetail.Attribute("id")
                               where xAttribute != null
                               select fareDetail).FirstOrDefault();
            List<PaxTypeFareBasis> paxTypeFraeBasisList = new List<PaxTypeFareBasis>() { ToPaxTypeFareBasis(segmentNode) };
            return paxTypeFraeBasisList;
        }

        private static PaxTypeFareBasis ToPaxTypeFareBasis(XElement xElement)
        {
            return new PaxTypeFareBasis()
            {
                FareBasisCode = GetXElementValue(xElement.Descendants("FareBasis").FirstOrDefault()),
                ClassOfService = GetXElementValue(xElement.Descendants("ClassOfService").FirstOrDefault()),
            };
        }
        #endregion
    }
}
