﻿using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.FareRules;
using Tavisca.TravelNxt.Flight.FareRules.Core;

namespace ComponentTest.Mock
{
    class MockFareRulesEngine : FareRulesEngine
    {
        protected override FlightRecommendation GetRecommendationFromSession(FareRulesRQ request)
        {
            return MockDataProvider.GetRecommendationForCurrentProvider();
        }
    }
}
