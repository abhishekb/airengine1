﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Data.Services.FareRules;
using Tavisca.TravelNxt.Flight.FareRules.Core;
namespace ComponentTest.Mock
{
    internal class MockFareRuleProvider : FareRulesProvider
    {
        public new List<FareDetail> GetFareDetails(Tavisca.TravelNxt.Flight.Entities.FlightRecommendation recommendation)
        {
            return FareRulesProvider.GetFareDetails(recommendation);
        }
    }
}
