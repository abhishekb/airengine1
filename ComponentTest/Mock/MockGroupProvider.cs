﻿namespace ComponentTest.Mock
{
    public sealed class MockGroupProvider : Tavisca.TravelNxt.Common.FareComponent.IGroupProvider
    {
        string Tavisca.TravelNxt.Common.FareComponent.IGroupProvider.GetGroupAssignedToNodeByParent(long node)
        {
            if (node == 123456)
            {
                return "GroupA";
            }
            else
            {
                return string.Empty;
            }
        }

        string Tavisca.TravelNxt.Common.FareComponent.IGroupProvider.GetSelfAssignedGroup(long node)
        {
            return string.Empty;
        }
    }
}
