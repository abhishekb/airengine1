﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProtoBuf;
using Entities = Tavisca.TravelNxt.Flight.Entities;
using Avail = Tavisca.TravelNxt.Flight.Entities.Avail;
using Booking = Tavisca.TravelNxt.Flight.Entities.Booking;
using FareRules = Tavisca.TravelNxt.Flight.Entities.FareRules;
using Pricing = Tavisca.TravelNxt.Flight.Entities.Pricing;
using SeatMap = Tavisca.TravelNxt.Flight.Entities.SeatMap;
using Tavisca.TravelNxt.Flight.Avail.Core;

namespace Tavisca.ComponentTest.Mock
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class MockCacheObject
    {
        public Avail.AirlinesPreference AirlinePreference { get; set; }

        public Avail.AlternateAirportInformation AlternateAirportInformation { get; set; }

        public Avail.ConnectionPreference ConnectionPreference { get; set; }

        public Avail.FlightAvailResult FlightAvailResult { get; set; }

        public Avail.FlightAvailRq FlightAvailRq { get; set; }

        public Avail.FlightSearchCriteria FlightSearchCriteria { get; set; }

        public Avail.FlightTravelPreference FlightTravelPreference { get; set; }

        public Avail.SearchSegment SearchSegment { get; set; }

        public Avail.ConnectionPreferenceType ConnectionPreferenceType { get; set; }

        public Avail.PreferenceTypeOptions PreferenceTypeOptions { get; set; }

        public Avail.SortingOrder SortingOrder { get; set; }

        public Booking.FlightProduct FlightProduct { get; set; }

        public Booking.FlightRecommendationInfo FlightRecommendationInfo { get; set; }

        public Booking.FlightRetrieveRQ FlightRetrieveRQ { get; set; }

        public Booking.FlightRetrieveRS FlightRetrieveRS { get; set; }

        public Booking.FlightSaveRQ FlightSaveRQ { get; set; }

        public Booking.FlightSaveRS FlightSaveRS { get; set; }

        public FareRules.FareRule FareRule { get; set; }

        public FareRules.FareRuleDetail FareRuleDetail { get; set; }

        public FareRules.FareRulesRQ FareRulesRQ { get; set; }

        public FareRules.FareRulesRS FareRulesRS { get; set; }

        public FareRules.ServiceStatus FareRulesServiceStatus { get; set; }

        public Pricing.FlightGetPricedRQ FlightGetPricedRQ { get; set; }

        public Pricing.FlightPriceResult FlightPriceResult { get; set; }

        public Pricing.FlightPriceRQ FlightPriceRQ { get; set; }

        public Pricing.PriceConfiguration PriceConfiguration { get; set; }

        public Pricing.ProviderSpacePriceConfiguration ProviderSpacePriceConfiguration { get; set; }

        public Pricing.ProviderSpaceReplacementPriceConfiguration ProviderSpaceReplacementPriceConfiguration { get; set; }

        public SeatMap.Cabin Cabin { get; set; }

        public SeatMap.CabinFacility CabinFacility { get; set; }

        public SeatMap.CabinFacilityLocation CabinFacilityLocation { get; set; }

        public SeatMap.CabinFacilityType CabinFacilityType { get; set; }

        public SeatMap.CabinLocation CabinLocation { get; set; }

        public SeatMap.CabinType SeatMapCabinType { get; set; }

        public SeatMap.CallStatus SeatMapCallStatus { get; set; }

        public SeatMap.RowCharacteristic RowCharacteristic { get; set; }

        public SeatMap.Seat Seat { get; set; }

        public SeatMap.SeatCharacteristic SeatCharacteristic { get; set; }

        public SeatMap.SeatColumn SeatColumn { get; set; }

        public SeatMap.SeatColumnType SeatColumnType { get; set; }

        public SeatMap.SeatMap SeatMap { get; set; }

        public SeatMap.SeatMapRQ SeatMapRQ { get; set; }

        public SeatMap.SeatMapRS SeatMapRS { get; set; }

        public SeatMap.SeatRow SeatRow { get; set; }

        public SeatMap.ServiceStatus SeatMapServiceStatus { get; set; }

        public Entities.Address Address { get; set; }

        public Entities.AirContract AirContract { get; set; }
        //public DbAirContract AirContract { get; set; }

        public Entities.Airline Airline { get; set; }

        public Entities.Airport Airport { get; set; }

        public Entities.CabinType CabinType { get; set; }

        public Entities.CallStatus CallStatus { get; set; }

        public Entities.City City { get; set; }

        public Entities.CompletionInfo CompletionInfo { get; set; }

        public Entities.ConfigurationType ConfigurationType { get; set; }

        public Entities.DateTimeSpan DateTimeSpan { get; set; }

        public Entities.FareAttribute FareAttribute { get; set; }

        public Entities.FareComponent FareComponent { get; set; }

        public Entities.FareComponentType FareComponentType { get; set; }

        public Entities.FareType FareType { get; set; }

        public Entities.FlightFare FlightFare { get; set; }

        public Entities.FlightLeg FlightLeg { get; set; }

        public Entities.FlightLegCollection FlightLegCollection { get; set; }

        public Entities.FlightRecommendation FlightRecommendation { get; set; }

        public Entities.FlightSegment FlightSegment { get; set; }

        public Entities.FlightSegmentCollection FlightSegmentCollection { get; set; }

        public Entities.ItineraryTypeOptions ItineraryTypeOptions { get; set; }

        public Entities.LegType LegType { get; set; }

        public Entities.Money Money { get; set; }

        public Entities.PassengerFare PassengerFare { get; set; }

        public Entities.PassengerType PassengerType { get; set; }

        public Entities.PaxTypeFareBasis PaxTypeFareBasis { get; set; }

        public Entities.PaxTypeQuantity PaxTypeQuantity { get; set; }

        public Entities.PointOfSale PointOfSale { get; set; }

        public Entities.ProviderSpace ProviderSpace { get; set; }

        public Entities.ProviderSpaceConfiguration ProviderSpaceConfiguration { get; set; }

        public Entities.ProviderSpaceType ProviderSpaceType { get; set; }

        public Entities.Requester Requester { get; set; }

        public Entities.RequesterType RequesterType { get; set; }

        public Entities.SegmentStop SegmentStop { get; set; }

        public Entities.TravelDate TravelDate { get; set; }
    }
}
