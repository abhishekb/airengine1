﻿namespace ComponentTest.Mock
{
    internal class Constants
    {
        public const string Invalid = "invalid";
        public const string DefaultCustomerType = "Agency";

        public const string FareRulesData = "Mock\\FareRuleMockData.xml";
        public const string SeatmapData = "Mock\\SeatMapMockData.xml";
        public const string FareSpaceData = "Mock\\FareSpaceMockData.xml";

        public const string FareDetailsDoc = "Mock\\FareDetails.xml";
        public const string FlightSegmentsDoc = "FlightSegments.xml";
        public const string AttributesDoc = "FareSpaceAttributes.xml";

        public const string SupplierNode = "Supplier";
        public const string Id = "id";
        public const string Attribute = "attribute";
        public const string Name = "name";
        public const string Value = "value";

        public const string FlightSegmentNode = "FlightSegment";
        public const string AirlineCodeNode = "AirlineCode";
        public const string FlightNumberNode = "FlightNumber";
        public const string AircraftTypeNode = "AircraftType";
        public const string ArrivalAirportCodeNode = "ArrivalAirportCode";
        public const string DepartureAirportCodeNode = "DepartureAirportCode";
        public const string ArrivalDateTimeNode = "ArrivalDateTime";
        public const string DepartureDateTimeNode = "DepartureDateTime";

        public const string Culture = "en-us";

        public const string FareDetailNode = "FareDetail";
        public const string ClassOfServiceNode = "ClassOfService";
        public const string CabinTypeNode = "CabinType";
        public const string FareBasisNode = "FareBasis";
        public const string FlightSegmentsNode = "FlightSegments";

        public const string Sabre = "sabre";
        public const string Worldspan = "worldspan";
        public const string Travelport = "travelport";
        public const string Amadeus = "amadeus";
        public const string SabreCode = "110";
        public const string AmadeusCode = "109";
        public const string TravelportCode = "170";
        public const string WorldspanCode = "101";

        public const string ApplicableFor = "applicableFor";
    }
}