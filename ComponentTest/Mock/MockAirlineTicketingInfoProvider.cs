﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;

namespace ComponentTest.Mock
{
    public class MockAirlineTicketingInfoProvider : IAirlineTicketingInfoProvider
    {
        private readonly IAirlineTicketingInfoProvider _internalAirlineTicketingInfoProvider =
            RuntimeContext.Resolver.Resolve<IAirlineTicketingInfoProvider>("actual");

        private static readonly IList<PosTicketingAirlineInfo> OverridePosTicketingAirlineInfo = new List<PosTicketingAirlineInfo>();
        private static readonly IList<PosInterAirlineAgreement> OverrideAirTicketingAgreements = new List<PosInterAirlineAgreement>();

        private static readonly ReaderWriterLockSlim PosTicketingAirlineInfoLocker = new ReaderWriterLockSlim();
        private static readonly ReaderWriterLockSlim AirTicketingAgreementsLocker = new ReaderWriterLockSlim();

        public static void AddOverridePosTicketingAirlineInfo(PosTicketingAirlineInfo posTicketingAirlineInfo)
        {
            PosTicketingAirlineInfoLocker.EnterWriteLock();
            try
            {
                OverridePosTicketingAirlineInfo.Add(posTicketingAirlineInfo);
            }
            finally
            {
                PosTicketingAirlineInfoLocker.ExitWriteLock();
            }
        }

        public static void AddOverrideAirTicketingAgreements(PosInterAirlineAgreement posInterAirlineAgreement)
        {
            AirTicketingAgreementsLocker.EnterWriteLock();
            try
            {
                OverrideAirTicketingAgreements.Add(posInterAirlineAgreement);
            }
            finally
            {
                AirTicketingAgreementsLocker.ExitWriteLock();
            }
        }

        public static void ClearAllOverrides()
        {
            PosTicketingAirlineInfoLocker.EnterWriteLock();
            try
            {
                OverridePosTicketingAirlineInfo.Clear();
            }
            finally
            {
                PosTicketingAirlineInfoLocker.ExitWriteLock();
            }
            AirTicketingAgreementsLocker.EnterWriteLock();
            try
            {
                OverrideAirTicketingAgreements.Clear();
            }
            finally
            {
                AirTicketingAgreementsLocker.ExitWriteLock();
            }
        }

        public IList<PosTicketingAirlineInfo> GetPosAirlineTicketingInfo()
        {
            var info = _internalAirlineTicketingInfoProvider.GetPosAirlineTicketingInfo();

            PosTicketingAirlineInfoLocker.EnterReadLock();
            try
            {
                return info.Concat(OverridePosTicketingAirlineInfo).ToListBuffered();
            }
            finally
            {
                PosTicketingAirlineInfoLocker.ExitReadLock();
            }
        }

        public IList<PosInterAirlineAgreement> GetAirTicketingAgreements()
        {
            var info = _internalAirlineTicketingInfoProvider.GetAirTicketingAgreements();

            AirTicketingAgreementsLocker.EnterReadLock();
            try
            {
                return info.Concat(OverrideAirTicketingAgreements).ToListBuffered();
            }
            finally
            {
                AirTicketingAgreementsLocker.ExitReadLock();
            }
        }
    }
}
