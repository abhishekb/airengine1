﻿using System;
using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Entities;

namespace ComponentTest.Mock
{
    internal static class ContractStore
    {
        public static List<AirContract> ValidSearchContracts = new List<AirContract>() {GetValidContract()};

        public static List<AirContract> InvalidSearchValidPeriod = new List<AirContract>() { GetInvalidSearchValidPeriod() };

        private static AirContract GetValidContract()
        {
            return new DbAirContract()
                       {
                           ContractRateCode = "123",
                           ContractId = 123,
                           InterceptLevel = "Pre",
                           ValidPeriod =
                               new DateTimeSpan() {Start = DateTime.UtcNow.AddHours(-2), End = DateTime.UtcNow.AddHours(2)},
                           AgencyId = 123,
                           TravelPeriod =
                               new DateTimeSpan() {Start = DateTime.UtcNow.AddHours(-2), End = DateTime.UtcNow.AddHours(2)},
                           SearchAirFareSourceId = 123
                       };
        }

        private static AirContract GetInvalidSearchValidPeriod()
        {
            return new DbAirContract()
            {
                ContractRateCode = "123",
                ContractId = 123,
                InterceptLevel = "Pre",
                ValidPeriod =
                    new DateTimeSpan() { Start = DateTime.UtcNow.AddHours(4), End = DateTime.UtcNow.AddHours(6) },
                AgencyId = 123,
                TravelPeriod =
                    new DateTimeSpan() { Start = DateTime.UtcNow.AddHours(-2), End = DateTime.UtcNow.AddHours(2) },
                SearchAirFareSourceId = 123
            };
        }

        public static AirContract GetValidSaleContract()
        {
            return new DbAirContract()
                       {
                           BookAirFareSourceId = MockKeys.ValidValues.ValidProvider,
                           InterceptLevel = MockKeys.ValidValues.ValidInterceptLevel,
                           ValidPeriod = new DateTimeSpan(){Start = DateTime.UtcNow.AddHours(-2), End = DateTime.Now.AddHours(2)},
                           FareBasisCodes = new List<string>(){MockKeys.ValidValues.ValidFareBasis},
                           TicketingPeriod = new DateTimeSpan() { Start = DateTime.UtcNow.AddHours(-2), End = DateTime.Now.AddHours(2) },
                           ApplicableCabinTypes = new List<string>(){MockKeys.ValidValues.ValidCabinType},
                           ApplicableClassOfService = new List<string>() { MockKeys.ValidValues.ValidClassOfService},
                           AirlinePreferenceLevel = MockKeys.AirlinePreference.Include,
                           TravelPeriod = new DateTimeSpan() { Start = DateTime.UtcNow.AddHours(-2), End = DateTime.Now.AddHours(2) },
                           Airlines = new List<string>(){ MockKeys.ValidValues.ValidAirline},
                           AirportPairs = new List<string>(){MockKeys.ValidValues.ValidAirportPair},
                           AgencyId = MockKeys.ValidValues.ValidAgencyId,
                           ContractId = 123
                       };
        }
    }
}
