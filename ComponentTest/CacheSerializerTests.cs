﻿using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Caching.Client;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Settings;
using Entities = Tavisca.TravelNxt.Flight.Entities;
using Avail = Tavisca.TravelNxt.Flight.Entities.Avail;
using Booking = Tavisca.TravelNxt.Flight.Entities.Booking;
using FareRules = Tavisca.TravelNxt.Flight.Entities.FareRules;
using Pricing = Tavisca.TravelNxt.Flight.Entities.Pricing;
using SeatMap = Tavisca.TravelNxt.Flight.Entities.SeatMap;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.TestHelpers;
using System.Reflection;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.ComponentTest.Mock;
using Tavisca.Frameworks.Serialization.Configuration;
using Tavisca.Frameworks.Serialization.Compression;
using System.Configuration;
using CabinType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.CabinType;
using FareAttribute = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareAttribute;
using FareComponent = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareComponent;
using FareComponentType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareComponentType;
using FareType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareType;
using Money = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Money;
using PassengerType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.PassengerType;

namespace ComponentTest
{
    [TestClass]
    public class CacheSerializerTests : TestBase
    {
        private static IApplicationSerializationConfiguration _config;

        private static readonly List<Entities.PaxTypeFareBasis> PaxTypeFareBasisCodes =
            new List<Entities.PaxTypeFareBasis>() {new Entities.PaxTypeFareBasis() {ClassOfService = "S"}};

        [ClassInitialize]
        public static void TestInitialize(TestContext testContext)
        {
            _config = SerializationConfigurationManager.GetConfiguration();
            SerializationConfigurationManager.SetConfiguration(GetMockConfiguration());
        }

        [ClassCleanup]
        public static void TestCleanUp()
        {
            SerializationConfigurationManager.SetConfiguration(_config);
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void FlightRecommendationCacheSerializationTest()
        {

            var expected = GetFlightRecommendationObject();
            var cacheProvider = GetCacheProvider();
            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                new ExpirationSettings()
                                                {
                                                    ExpirationType = ExpirationType.AbsoluteExpiration,
                                                    AbsoluteExpiration =
                                                        DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                });

            var actual = cacheProvider.Get<Entities.FlightRecommendation>("SerializationTestCategory", "ProtoBufSerialization");
            Assert.IsNotNull(actual);
            Assert.IsTrue(AreEqualFlightRecommendation(actual, expected));
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void ProviderSpaceCacheSerializationTest()
        {
            var expected = GetFlightRecommendationObject().ProviderSpace;
            var cacheProvider = GetCacheProvider();


            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                            new ExpirationSettings()
                                                            {
                                                                ExpirationType = ExpirationType.AbsoluteExpiration,
                                                                AbsoluteExpiration =
                                                                    DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                            });

            var actual = cacheProvider.Get<Entities.ProviderSpace>("SerializationTestCategory", "ProtoBufSerialization");
            Assert.IsNotNull(actual);
            Assert.IsTrue(AreEqualProviderSpace(actual, expected));
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void InterlineAgreementCacheSerializationTest()
        {
            var expected = new AirlineInterlineAgreementProvider.InterlineAgreement()
            {
                AgreementType = "Agreement",
                Airlines = new List<string>() { "Jet", "Indigo" }
            };
            var cacheProvider = GetCacheProvider();

            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                new ExpirationSettings()
                                                {
                                                    ExpirationType = ExpirationType.AbsoluteExpiration,
                                                    AbsoluteExpiration =
                                                        DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                });
            var actual = cacheProvider.Get<AirlineInterlineAgreementProvider.InterlineAgreement>(
                "SerializationTestCategory", "ProtoBufSerialization");

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.AgreementType, expected.AgreementType);
            Assert.AreEqual(actual.Airlines[0], expected.Airlines[0]);
            Assert.AreEqual(actual.Airlines[1], expected.Airlines[1]);
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void PosFareSourceStrategySettingCacheSerializationTest()
        {
            var cacheProvider = GetCacheProvider();
            var expected = new PosFareSourceStrategySetting()
            {
                AirLineCode = "A1001",
                CheckMarket = true,
                FareSourceID = 1001,
                ID = 12345,
                MarketType = "Air",
                PosID = 54321,
                RateCode = "R1001"
            };

            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                new ExpirationSettings()
                                                {
                                                    ExpirationType = ExpirationType.AbsoluteExpiration,
                                                    AbsoluteExpiration =
                                                        DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                });
            var actual = cacheProvider.Get<PosFareSourceStrategySetting>("SerializationTestCategory", "ProtoBufSerialization");
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.AirLineCode, expected.AirLineCode);
            Assert.AreEqual(actual.CheckMarket, expected.CheckMarket);
            Assert.AreEqual(actual.FareSourceID, expected.FareSourceID);
            Assert.AreEqual(actual.ID, expected.ID);
            Assert.AreEqual(actual.MarketType, expected.MarketType);
            Assert.AreEqual(actual.PosID, expected.PosID);
            Assert.AreEqual(actual.RateCode, expected.RateCode);
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void FareSearchRSCacheSerializationTest()
        {
            var cacheProvider = GetCacheProvider();
            var expected = GetFareSearchRSObject();

            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                            new ExpirationSettings()
                                                            {
                                                                ExpirationType = ExpirationType.AbsoluteExpiration,
                                                                AbsoluteExpiration =
                                                                    DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                            });
            var actual = cacheProvider.Get<FareSearchRS>("SerializationTestCategory", "ProtoBufSerialization");
            Assert.IsNotNull(actual);
            AreEqualFareSearchRSObjects(actual, expected);
        }

        [TestMethod, TestCategory("Cache Serialization")]
        public void MockCacheObjectCacheSerializationTest()
        {
            var cacheProvider = GetCacheProvider();
            var expected = GetMockCacheObject();

            cacheProvider.Insert("SerializationTestCategory", "ProtoBufSerialization", expected,
                                                            new ExpirationSettings()
                                                            {
                                                                ExpirationType = ExpirationType.AbsoluteExpiration,
                                                                AbsoluteExpiration =
                                                                    DateTime.Now.AddHours(SettingManager.ScepterCacheExpirationHours)
                                                            });

            var actual = cacheProvider.Get<MockCacheObject>("SerializationTestCategory", "ProtoBufSerialization");

            Assert.IsNotNull(actual);
            Assert.IsTrue(AreEqualMockCacheObjects(actual, expected));
        }

        #region private methods

        private static ICacheProvider GetCacheProvider()
        {
            //Do not return an in memory provider...serialization won't be utilized for in memory providers
            //return new CacheProviderFactory().GetCacheProvider(CacheProviderOptions.RedisCache);
            return new CacheProviderFactory().GetCacheProvider(ConfigurationManager.AppSettings["mockCacheType"]);
        }

        private static IApplicationSerializationConfiguration GetMockConfiguration()
        {
            var config = new SerializationConfiguration();
            config.DefaultSerializationProvider = SerializerType.Binary;
            config.TypeElements = new List<ITypeElement>();
            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.FlightRecommendation",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Avail.Core"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Avail.Core.AirlineInterlineAgreementProvider+InterlineAgreement",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Avail.Core"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.CabinType",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.Airport",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.Airline",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.ProviderSpace",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Avail.Core"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Entities.Pricing.PriceConfiguration",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models.PosFareSourceStrategySetting",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Data.AirConfiguration"}
                    }
            });

            config.TypeElements.Add(new TypeElementConfiguration()
            {
                Type = "Tavisca.ComponentTest.Mock.MockCacheObject",
                SerializationProvider = SerializerType.ProtoBuf,
                CompressionOptions = CompressionTypeOptions.Deflate,
                LookupAssemblies = new List<IAssemblyElement>()
                    {
                        new AssemblyElement(){Name = "ComponentTest"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Common.Extensions"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Entities"},
                        new AssemblyElement(){Name = "Tavisca.TravelNxt.Flight.Avail.Core"}
                    }
            });
            return config;
        }

        private static Entities.FlightRecommendation GetFlightRecommendationObject()
        {
            //MockCacheObject mObj = new MockCacheObject();
            var additionalInfoDictionary = new AdditionalInfoDictionary();
            additionalInfoDictionary.AddOrUpdate("Flight", "Jet Airways");
            additionalInfoDictionary.AddOrUpdate("Airport", "IGI-T3");
            var dateTimeSpan = new Entities.DateTimeSpan()
            {
                Start = new DateTime(2015, 4, 8),
                End = new DateTime(2015, 4, 20)
            };
            Entities.AirContract airContract = new DbAirContract()
            {
                AgencyId = 123456789,
                Airlines = new List<string>() { "Jet", "Indigo" },
                TicketingPeriod = dateTimeSpan
            };
            var money = new Entities.Money(1000, "Dollar", 2000, "Euro", 3000, "Pound");
            var fareComponent = new Entities.FareComponent()
            {
                FareComponentType = Entities.FareComponentType.Commision
            };
            var passengerFare = new Entities.PassengerFare()
            {
                BaseAmount = money,
                PassengerType = Entities.PassengerType.Adult,
                Commissions = new List<Entities.FareComponent>() { fareComponent }
            };
            var flightFare = new Entities.FlightFare()
            {
                Savings = money,
                FareAttributes = new List<Entities.FareAttribute>() { Entities.FareAttribute.ExcludeBookingClasses },
                FareType = Entities.FareType.Corporate,
                PassengerFares = new List<Entities.PassengerFare>() { passengerFare }
            };
            var flightSegment = new Entities.FlightSegment("LA", "NY", new DateTime(2015, 04, 8),
                                                                              new DateTime(2015, 4, 20), "F111", "M111",
                                                                              1)
                                                       {
                                                           PaxTypeFareBasisCodes = PaxTypeFareBasisCodes,
                                                           BaggageDetails =
                                                               new List<Entities.Baggage>()
                                                                   {
                                                                       new Entities.Baggage()
                                                                           {
                                                                               Description = "Baggage",
                                                                               Quantity = null,
                                                                               WeightInPounds = null
                                                                           },
                                                                       new Entities.Baggage()
                                                                           {
                                                                               Description = null,
                                                                               Quantity = 2,
                                                                               WeightInPounds = 15.0M
                                                                           }
                                                                   }
                                                       };
            var flightSegmentCollection = new Entities.FlightSegmentCollection();
            flightSegmentCollection.Add(flightSegment);
            var flightLeg = new Entities.FlightLeg() { LegType = Entities.LegType.Onward, Segments = flightSegmentCollection };
            var flightLegCollection = new Entities.FlightLegCollection();
            flightLegCollection.Add(flightLeg);
            var configurationType = Entities.ConfigurationType.Search;
            var providerSpaceConfiguration = new Entities.ProviderSpaceConfiguration()
            {
                ConfigType = configurationType,
                ServiceUri = new Uri("http://www.tavisca.com")
            };
            var requesterType = Entities.RequesterType.Affiliate;
            var requester = new Entities.Requester(long.Parse(TestCaseSettingManager.AccountId), 11, Entities.CustomerType.Agent)
            {
                //Type = requesterType
            };
            var providerSpaceType = Entities.ProviderSpaceType.Domestic;
            var providerSpace = new Entities.ProviderSpace(1, "PS", requester, "family", 5, 111111, providerSpaceType,
                new List<Entities.ProviderSpaceConfiguration>() { providerSpaceConfiguration }, additionalInfoDictionary);
            providerSpace.Contracts = new List<Entities.AirContract>() { airContract };
            var flightRecommendation = new Entities.FlightRecommendation()
            {
                AdditionalInfo = additionalInfoDictionary,
                Contract = airContract,
                Fare = flightFare,
                ItineraryType = Entities.ItineraryTypeOptions.MultiCity,
                Legs = flightLegCollection,
                ProviderSpace = providerSpace,
                PlatingCarrier="ABC"
            };
            return flightRecommendation;
        }

        private static MockCacheObject GetMockCacheObject()
        {
            var mObj = new MockCacheObject();
            var guid = new Guid("12ABCDEF-ABCD-EFAB-ABEF-1234506789AB");
            mObj.City = new Entities.City()
            {
                Code = "001"
            };
            mObj.Address = new Entities.Address()
            {
                AddressLine1 = "Tavisca",
                City = mObj.City
            };
            mObj.DateTimeSpan = new Entities.DateTimeSpan()
            {
                Start = new DateTime(2015, 4, 8),
                End = new DateTime(2015, 4, 20)
            };
            mObj.Airline = new Entities.Airline("A1001");
            mObj.AirlinePreference = new Avail.AirlinesPreference()
            {
                PermittedAirlines = new List<string>() { "Jet", "Indigo" }
            };
            mObj.CabinFacilityLocation = SeatMap.CabinFacilityLocation.Center;
            mObj.CabinFacilityType = SeatMap.CabinFacilityType.Bar;
            mObj.CabinLocation = SeatMap.CabinLocation.Lowerdeck;
            mObj.CallStatus = Entities.CallStatus.InProgress;
            mObj.Money = new Entities.Money(1000, "Dollar", 2000, "Euro", 3000, "Pound");
            mObj.FareComponentType = Entities.FareComponentType.Commision;
            mObj.AirContract = new DbAirContract()
            {
                AgencyId = 123456789,
                Airlines = new List<string>() { "Jet", "Indigo" },
                TicketingPeriod = mObj.DateTimeSpan
            };
            mObj.CabinFacility = new SeatMap.CabinFacility()
            {
                CabinFacilityLocation = mObj.CabinFacilityLocation,
                CabinFacilityType = mObj.CabinFacilityType
            };
            mObj.Cabin = new SeatMap.Cabin()
            {
                CabinFacilities = new List<SeatMap.CabinFacility>() { mObj.CabinFacility }
            };

            mObj.CompletionInfo = new Entities.CompletionInfo()
            {
                Code = "C1001",
                Status = mObj.CallStatus
            };

            mObj.FareComponent = new Entities.FareComponent()
            {
                FareComponentType = mObj.FareComponentType
            };
            mObj.FareRuleDetail = new FareRules.FareRuleDetail()
            {
                Title = "FareRule"
            };
            mObj.FareRule = new FareRules.FareRule()
            {
                FareBasisCode = "Fare",
                FareRuleDetails = new List<FareRules.FareRuleDetail>() { mObj.FareRuleDetail }
            };
            var additionalInfoDictionary = new AdditionalInfoDictionary();
            additionalInfoDictionary.AddOrUpdate("Flight", "Jet Airways");
            additionalInfoDictionary.AddOrUpdate("Airport", "IGI-T3");
            mObj.FareRulesRQ = new FareRules.FareRulesRQ()
            {
                AdditionalInfo = additionalInfoDictionary,
                RecommendationRefID = 1
            };
            mObj.FareRulesServiceStatus = new FareRules.ServiceStatus()
            {
                Status = mObj.CallStatus
            };
            mObj.FareRulesRS = new FareRules.FareRulesRS()
            {
                FareRules = new List<FareRules.FareRule>() { mObj.FareRule },
                ServiceStatus = mObj.FareRulesServiceStatus,
                SessionId = guid
            };
            mObj.ItineraryTypeOptions = Entities.ItineraryTypeOptions.MultiCity;
            mObj.FareAttribute = Entities.FareAttribute.ExcludeBookingClasses;
            mObj.FareType = Entities.FareType.Corporate;
            mObj.PassengerType = Entities.PassengerType.Adult;
            mObj.PassengerFare = new Entities.PassengerFare()
            {
                BaseAmount = mObj.Money,
                PassengerType = mObj.PassengerType,
                Commissions = new List<Entities.FareComponent>() { mObj.FareComponent }
            };
            mObj.FlightFare = new Entities.FlightFare()
            {
                Savings = mObj.Money,
                FareAttributes = new List<Entities.FareAttribute>() { mObj.FareAttribute },
                FareType = mObj.FareType,
                PassengerFares = new List<Entities.PassengerFare>() { mObj.PassengerFare }
            };

            mObj.FlightSegment = new Entities.FlightSegment("LA", "NY", new DateTime(2015, 04, 8),
                                                            new DateTime(2015, 4, 20), "F111", "M111", 1)
                                     {
                                         PaxTypeFareBasisCodes = PaxTypeFareBasisCodes,
                                         BaggageDetails =
                                             new List<Entities.Baggage>()
                                                 {
                                                     new Entities.Baggage()
                                                         {
                                                             Description = "Baggage",
                                                             Quantity = null,
                                                             WeightInPounds = null
                                                         },
                                                     new Entities.Baggage()
                                                         {
                                                             Description = null,
                                                             Quantity = 2,
                                                             WeightInPounds = 15.0M
                                                         }
                                                 }
                                     };
            mObj.FlightSegmentCollection = new Entities.FlightSegmentCollection();
            mObj.FlightSegmentCollection.Add(mObj.FlightSegment);
            mObj.LegType = Entities.LegType.Onward;
            mObj.FlightLeg = new Entities.FlightLeg() { LegType = mObj.LegType, Segments = mObj.FlightSegmentCollection };
            mObj.FlightLegCollection = new Entities.FlightLegCollection();
            mObj.FlightLegCollection.Add(mObj.FlightLeg);
            mObj.ConfigurationType = Entities.ConfigurationType.Search;
            mObj.ProviderSpaceConfiguration = new Entities.ProviderSpaceConfiguration()
            {
                ConfigType = mObj.ConfigurationType,
                ServiceUri = new Uri("http://www.tavisca.com")
            };
            mObj.RequesterType = Entities.RequesterType.Affiliate;
            mObj.Requester = new Entities.Requester(long.Parse(TestCaseSettingManager.AccountId), 11, Entities.CustomerType.Agent)
            {
                //Type = mObj.RequesterType
            };
            mObj.ProviderSpaceType = Entities.ProviderSpaceType.Domestic;
            mObj.ProviderSpace = new Entities.ProviderSpace(1, "PS", mObj.Requester, "family", 5, 111111, mObj.ProviderSpaceType,
                new List<Entities.ProviderSpaceConfiguration>() { mObj.ProviderSpaceConfiguration }, additionalInfoDictionary);
            mObj.ProviderSpace.Contracts = new List<Entities.AirContract>() { mObj.AirContract };
            mObj.FlightRecommendation = new Entities.FlightRecommendation()
            {
                AdditionalInfo = additionalInfoDictionary,
                Contract = mObj.AirContract,
                Fare = mObj.FlightFare,
                ItineraryType = mObj.ItineraryTypeOptions,
                Legs = mObj.FlightLegCollection,
                ProviderSpace = mObj.ProviderSpace
            };
            mObj.FlightAvailResult = new Avail.FlightAvailResult()
            {
                FlightRecommendations = new List<Entities.FlightRecommendation>() { mObj.FlightRecommendation },
                AdditionalInfo = additionalInfoDictionary,
                Status = mObj.CallStatus
            };

            mObj.FlightTravelPreference = new Avail.FlightTravelPreference()
            {
                AllowMixedAirlines = true
            };
            mObj.FlightTravelPreference.AirlinesPreference = mObj.AirlinePreference;
            mObj.PaxTypeQuantity = new Entities.PaxTypeQuantity()
            {
                PassengerType = mObj.PassengerType
            };
            mObj.AlternateAirportInformation = new Avail.AlternateAirportInformation()
            {
                IncludeNearByAirports = true
            };
            mObj.CabinType = Entities.CabinType.Business;
            mObj.ConnectionPreferenceType = Avail.ConnectionPreferenceType.Avoid;
            mObj.ConnectionPreference = new Avail.ConnectionPreference()
            {
                PreferenceType = mObj.ConnectionPreferenceType
            };
            mObj.TravelDate = new Entities.TravelDate()
            {
                DateTime = new DateTime(2015, 4, 8)
            };
            mObj.SearchSegment = new Avail.SearchSegment()
            {
                ArrivalAlternateAirportInformation = mObj.AlternateAirportInformation,
                Cabin = mObj.CabinType,
                ConnectionPreferences = new List<Avail.ConnectionPreference>() { mObj.ConnectionPreference },
                TravelDate = mObj.TravelDate
            };
            mObj.SortingOrder = Avail.SortingOrder.ByAirline;
            mObj.FlightSearchCriteria = new Avail.FlightSearchCriteria()
            {
                FlightTravelPreference = mObj.FlightTravelPreference,
                PassengerInfoSummary = new List<Entities.PaxTypeQuantity>() { mObj.PaxTypeQuantity },
                AdditionalInfo = additionalInfoDictionary,
                SearchSegments = new List<Avail.SearchSegment>() { mObj.SearchSegment },
                SortingOrder = mObj.SortingOrder,
                SessionID = guid
            };
            mObj.FlightAvailRq = new Avail.FlightAvailRq(mObj.ProviderSpace, mObj.FlightSearchCriteria);
            mObj.FlightGetPricedRQ = new Pricing.FlightGetPricedRQ()
            {
                Requester = mObj.Requester
            };
            mObj.FlightPriceResult = new Pricing.FlightPriceResult()
            {
                Status = mObj.CallStatus,
                Recommendation = mObj.FlightRecommendation
            };
            mObj.FlightPriceRQ = new Pricing.FlightPriceRQ()
            {
                Requester = mObj.Requester,
                AdditionalInfo = additionalInfoDictionary
            };
            mObj.FlightRetrieveRQ = new Booking.FlightRetrieveRQ()
            {
                AdditionalInfo = additionalInfoDictionary,
                Requester = mObj.Requester,
                RecordLocator = guid
            };
            mObj.FlightProduct = new Booking.FlightProduct()
            {
                Recommendation = mObj.FlightRecommendation
            };
            mObj.FlightRetrieveRS = new Booking.FlightRetrieveRS()
            {
                Product = mObj.FlightProduct
            };
            mObj.FlightRecommendationInfo = new Booking.FlightRecommendationInfo()
            {
                FlightRecommendationRefId = 1
            };
            mObj.FlightSaveRQ = new Booking.FlightSaveRQ()
            {
                FlightRecommendationInfo = mObj.FlightRecommendationInfo
            };
            mObj.FlightSaveRS = new Booking.FlightSaveRS()
            {
                Status = mObj.CallStatus,
                RecordLocator = guid
            };
            mObj.PaxTypeFareBasis = new Entities.PaxTypeFareBasis()
            {
                PassengerType = mObj.PassengerType
            };
            mObj.PreferenceTypeOptions = Avail.PreferenceTypeOptions.Acceptable;
            mObj.ProviderSpacePriceConfiguration = new Pricing.ProviderSpacePriceConfiguration()
            {
                Id = 1
            };
            mObj.PriceConfiguration = new Pricing.PriceConfiguration();
            mObj.PriceConfiguration.FareSourcePriceConfigurations.Add(mObj.ProviderSpacePriceConfiguration);
            mObj.ProviderSpaceReplacementPriceConfiguration = new Pricing.ProviderSpaceReplacementPriceConfiguration()
            {
                AirRuleSet = new Tavisca.TravelNxt.Flight.Common.AirRuleSet()
                {
                    ClassOfServiceRules = new List<Tavisca.TravelNxt.Flight.Common.AirRule>() 
                        { 
                            new Tavisca.TravelNxt.Flight.Common.AirRule()
                            {
                                Alignment = Tavisca.TravelNxt.Flight.Common.AlignmentType.Positive, 
                                RuleType = Tavisca.TravelNxt.Flight.Common.AirRuleType.Custom
                            }
                        }
                }
            };
            mObj.PriceConfiguration.FareSourceReplacementPriceConfigurations.Add(mObj.ProviderSpaceReplacementPriceConfiguration);
            mObj.SeatCharacteristic = SeatMap.SeatCharacteristic.AdjacentToBar;
            mObj.SeatColumnType = SeatMap.SeatColumnType.Aisle;
            mObj.SeatMapCabinType = SeatMap.CabinType.Economy;
            mObj.Seat = new SeatMap.Seat()    //70
            {
                SeatCharacteristics = new List<SeatMap.SeatCharacteristic>() { mObj.SeatCharacteristic }
            };
            mObj.SeatColumn = new SeatMap.SeatColumn()
            {
                ColumnType = mObj.SeatColumnType
            };
            mObj.SeatMap = new SeatMap.SeatMap()
            {
                Cabins = new List<SeatMap.Cabin>() { mObj.Cabin },
                CabinType = mObj.SeatMapCabinType
            };
            mObj.SeatMapRQ = new SeatMap.SeatMapRQ()
            {
                AdditionalInfo = new Dictionary<string, string>() { { "Company", "Tavisca" } }
            };
            mObj.SeatMapCallStatus = SeatMap.CallStatus.Warning;
            mObj.SeatMapServiceStatus = new SeatMap.ServiceStatus()
            {
                Status = mObj.SeatMapCallStatus
            };
            mObj.SeatMapRS = new SeatMap.SeatMapRS()
            {
                SeatMap = mObj.SeatMap,
                ServiceStatus = mObj.SeatMapServiceStatus,
                SessionId = guid
            };
            mObj.RowCharacteristic = SeatMap.RowCharacteristic.BufferRow;
            mObj.SeatRow = new SeatMap.SeatRow()
            {
                RowCharacteristics = new List<SeatMap.RowCharacteristic>() { mObj.RowCharacteristic }
            };
            mObj.Airport = new Entities.Airport("A1002")
             {
                 City = mObj.City
             };
            mObj.SegmentStop = new Entities.SegmentStop()
            {
                Airport = mObj.Airport
            };
            mObj.PointOfSale = new Entities.PointOfSale(123)
            {
                Name = "POS"
            };

            return mObj;
        }

        private static FareSearchRS GetFareSearchRSObject()
        {
            var fareSearchRS = new FareSearchRS();
            fareSearchRS.Flights = new List<Flight>();
            var flight = new Flight()
            {
                AircraftType = "Boeing",
                StopDetails = new List<StopDetail>() { new StopDetail() { ArrivalDateTime = new DateTime(2015, 04, 21) } }
            };
            fareSearchRS.Flights.Add(flight);
            fareSearchRS.ItineraryFares = new List<Fare>();
            var fare = new Fare()
            {
                BaseAmount = new Money() { Amount = 100.0M, CurrencyCode = "$" },
                Discounts = new List<FareComponent>() { new FareComponent() { Amount = 200.0M, Code = "C1001", Description = "FareComponentCode", Type = FareComponentType.Discount } },
                Penalties = new List<Penalty>() { new Penalty() { AmountType = AmountType.Amount, PenaltyType = PenaltyType.NonRefundable } },
                FareAttributes = new List<FareAttribute>() { FareAttribute.NonRefundableFares },
                FareType = FareType.Corporate,
                PassengerFareBreakups = new List<PassengerFareBreakup>()
            };
            var passengerFareBreakup = new PassengerFareBreakup()
            {
                FareDetails = new List<FareDetails>(),
                PassengerTypeQuantity = new PassengerTypeQuantity() { PassengerType = PassengerType.Government }
            };
            var fareDetails = new FareDetails()
            {
                Cabin = CabinType.First
            };
            passengerFareBreakup.FareDetails.Add(fareDetails);
            fare.PassengerFareBreakups.Add(passengerFareBreakup);
            fareSearchRS.ItineraryFares.Add(fare);
            fareSearchRS.LegGroups = new List<LegGroup>();
            var legGroup = new LegGroup()
            {
                LegGroupNumber = 123,
                LegRecommendations = new List<LegRecommendation>()
            };
            var legRecommendation = new LegRecommendation()
            {
                DurationMinutes = 500,
                FlightCodes = new List<string>() { "F1001", "F1002" },
                LegNumber = 111,
                Layovers = new List<Layover>()
            };
            var layover = new Layover()
            {
                ArrivingFlight = flight,
                ChangeOfAirport = true
            };
            legRecommendation.Layovers.Add(layover);
            legGroup.LegRecommendations.Add(legRecommendation);
            fareSearchRS.LegGroups.Add(legGroup);
            fareSearchRS.Status = new Status()
            {
                ErrorMessages = new List<StatusMessage>(),
                ResponseStatus = StatusType.Success
            };
            fareSearchRS.Status.ErrorMessages.Add(new StatusMessage() { Category = "error" });
            return fareSearchRS;
        }

        private static void AreEqualFareSearchRSObjects(FareSearchRS actual, FareSearchRS expected)
        {
            Assert.AreEqual(actual.Flights[0].AircraftType, expected.Flights[0].AircraftType);
            Assert.AreEqual(actual.Flights[0].StopDetails[0].ArrivalDateTime, expected.Flights[0].StopDetails[0].ArrivalDateTime);
            Assert.AreEqual(actual.ItineraryFares[0].BaseAmount.Amount, expected.ItineraryFares[0].BaseAmount.Amount);
            Assert.AreEqual(actual.ItineraryFares[0].BaseAmount.CurrencyCode, expected.ItineraryFares[0].BaseAmount.CurrencyCode);
            Assert.AreEqual(actual.ItineraryFares[0].Discounts[0].Amount, expected.ItineraryFares[0].Discounts[0].Amount);
            Assert.AreEqual(actual.ItineraryFares[0].Discounts[0].Code, expected.ItineraryFares[0].Discounts[0].Code);
            Assert.AreEqual(actual.ItineraryFares[0].Discounts[0].Description, expected.ItineraryFares[0].Discounts[0].Description);
            Assert.AreEqual(actual.ItineraryFares[0].Discounts[0].Type, expected.ItineraryFares[0].Discounts[0].Type);
            Assert.AreEqual(actual.ItineraryFares[0].Penalties[0].AmountType, expected.ItineraryFares[0].Penalties[0].AmountType);
            Assert.AreEqual(actual.ItineraryFares[0].Penalties[0].PenaltyType, expected.ItineraryFares[0].Penalties[0].PenaltyType);
            Assert.AreEqual(actual.ItineraryFares[0].FareAttributes[0], expected.ItineraryFares[0].FareAttributes[0]);
            Assert.AreEqual(actual.ItineraryFares[0].FareType, expected.ItineraryFares[0].FareType);
            Assert.AreEqual(actual.ItineraryFares[0].PassengerFareBreakups[0].PassengerTypeQuantity.PassengerType, expected.ItineraryFares[0].PassengerFareBreakups[0].PassengerTypeQuantity.PassengerType);
            Assert.AreEqual(actual.ItineraryFares[0].PassengerFareBreakups[0].FareDetails[0].Cabin, expected.ItineraryFares[0].PassengerFareBreakups[0].FareDetails[0].Cabin);
            Assert.AreEqual(actual.LegGroups[0].LegGroupNumber, expected.LegGroups[0].LegGroupNumber);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].DurationMinutes, actual.LegGroups[0].LegRecommendations[0].DurationMinutes);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].LegNumber, actual.LegGroups[0].LegRecommendations[0].LegNumber);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].FlightCodes[0], actual.LegGroups[0].LegRecommendations[0].FlightCodes[0]);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].FlightCodes[1], actual.LegGroups[0].LegRecommendations[0].FlightCodes[1]);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].Layovers[0].ArrivingFlight.AircraftType, actual.LegGroups[0].LegRecommendations[0].Layovers[0].ArrivingFlight.AircraftType);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].Layovers[0].ArrivingFlight.StopDetails[0].ArrivalDateTime, actual.LegGroups[0].LegRecommendations[0].Layovers[0].ArrivingFlight.StopDetails[0].ArrivalDateTime);
            Assert.AreEqual(actual.LegGroups[0].LegRecommendations[0].Layovers[0].ChangeOfAirport, actual.LegGroups[0].LegRecommendations[0].Layovers[0].ChangeOfAirport);
            Assert.AreEqual(actual.Status.ResponseStatus, expected.Status.ResponseStatus);
            Assert.AreEqual(actual.Status.ErrorMessages[0].Category, expected.Status.ErrorMessages[0].Category);
        }

        private static bool AreEqualMockCacheObjects(MockCacheObject actual, MockCacheObject expected)
        {
            if (actual.City.Code != expected.City.Code)
                return false;

            if (actual.Address.AddressLine1 != expected.Address.AddressLine1 || actual.Address.City.Code != actual.City.Code)
                return false;

            if (actual.DateTimeSpan.Start != expected.DateTimeSpan.Start || actual.DateTimeSpan.End != expected.DateTimeSpan.End)
                return false;

            if (actual.Airline.Code != expected.Airline.Code)
                return false;

            if (actual.AirlinePreference.PermittedAirlines[0] != expected.AirlinePreference.PermittedAirlines[0] || actual.AirlinePreference.PermittedAirlines[1] != expected.AirlinePreference.PermittedAirlines[1])
                return false;

            if (actual.CabinFacilityLocation != expected.CabinFacilityLocation)
                return false;

            if (actual.CabinFacilityType != expected.CabinFacilityType)
                return false;

            if (actual.CabinLocation != expected.CabinLocation)
                return false;

            if (actual.CallStatus != expected.CallStatus)
                return false;

            if (!AreEqualMoney(actual.Money, expected.Money))
                return false;

            if (actual.FareComponentType != expected.FareComponentType)
                return false;

            if (!AreEqualCabinFacility(actual.CabinFacility, expected.CabinFacility))
                return false;

            if (!AreEqualCabinFacility(actual.Cabin.CabinFacilities[0], expected.Cabin.CabinFacilities[0]))
                return false;

            if (actual.CompletionInfo.Code != expected.CompletionInfo.Code || actual.CompletionInfo.Status != expected.CompletionInfo.Status)
                return true;

            if (actual.FareComponent.FareComponentType != expected.FareComponent.FareComponentType)
                return false;

            if (actual.FareRuleDetail.Title != expected.FareRuleDetail.Title)
                return false;

            if (!AreEqualFareRule(actual.FareRule, expected.FareRule))
                return false;

            if (actual.FareRulesRQ.RecommendationRefID != expected.FareRulesRQ.RecommendationRefID || !AreAdditionalInfoDictionaryEqual(actual.FareRulesRQ.AdditionalInfo, expected.FareRulesRQ.AdditionalInfo))
                return false;

            if (actual.FareRulesServiceStatus.Status != expected.FareRulesServiceStatus.Status)
                return false;

            if (actual.FareRulesRS.ServiceStatus.Status != expected.FareRulesRS.ServiceStatus.Status || !AreEqualFareRule(actual.FareRulesRS.FareRules[0], expected.FareRulesRS.FareRules[0]) ||
                !actual.FareRulesRS.SessionId.Equals(expected.FareRulesRS.SessionId))
                return false;

            if (actual.ItineraryTypeOptions != expected.ItineraryTypeOptions || actual.FareAttribute != expected.FareAttribute || actual.FareType != expected.FareType || actual.PassengerType != actual.PassengerType)
                return false;

            if (!AreEqualPassengerFare(actual.PassengerFare, expected.PassengerFare))
                return false;

            if (!AreEqualFlightFare(actual.FlightFare, expected.FlightFare))
                return false;

            if (!AreEqualFlightSegment(actual.FlightSegment, expected.FlightSegment))
                return false;

            if (!AreEqualFlightSegment(actual.FlightSegmentCollection[0], expected.FlightSegmentCollection[0]))
                return false;

            if (actual.LegType != expected.LegType)
                return false;

            if (!AreEqualFlightLeg(actual.FlightLeg, expected.FlightLeg))
                return false;

            if (actual.ConfigurationType != expected.ConfigurationType || actual.RequesterType != expected.RequesterType || actual.ProviderSpaceType != expected.ProviderSpaceType)
                return false;

            if (actual.ProviderSpaceConfiguration.ConfigType != expected.ProviderSpaceConfiguration.ConfigType || actual.ProviderSpaceConfiguration.ServiceUri.AbsoluteUri != expected.ProviderSpaceConfiguration.ServiceUri.AbsoluteUri)
                return false;

            if (!AreEqualRequester(actual.Requester, expected.Requester))
                return false;

            if (!AreEqualProviderSpace(actual.ProviderSpace, expected.ProviderSpace))
                return false;

            if (!AreEqualFlightRecommendation(actual.FlightRecommendation, expected.FlightRecommendation))
                return false;

            if (!AreAdditionalInfoDictionaryEqual(actual.FlightAvailResult.AdditionalInfo, expected.FlightAvailResult.AdditionalInfo) ||
                !AreEqualFlightRecommendation(actual.FlightAvailResult.FlightRecommendations[0], expected.FlightAvailResult.FlightRecommendations[0]) || actual.FlightAvailResult.Status != expected.FlightAvailResult.Status)
                return false;

            if (!AreEqualFlightTravelPreference(actual.FlightTravelPreference, expected.FlightTravelPreference))
                return false;

            if (!AreEqualPaxTypeQuantity(actual.PaxTypeQuantity, expected.PaxTypeQuantity))
                return false;

            if (actual.AlternateAirportInformation.IncludeNearByAirports != expected.AlternateAirportInformation.IncludeNearByAirports || actual.CabinType != expected.CabinType
                || actual.ConnectionPreferenceType != expected.ConnectionPreferenceType || actual.ConnectionPreference.PreferenceType != expected.ConnectionPreference.PreferenceType
                || actual.TravelDate.DateTime != expected.TravelDate.DateTime || actual.SortingOrder != expected.SortingOrder)
                return false;

            if (!AreEqualSearchSegment(actual.SearchSegment, expected.SearchSegment))
                return false;

            if (!AreEqualFlightSearchCriteria(actual.FlightSearchCriteria, expected.FlightSearchCriteria))
                return false;

            if (!AreEqualProviderSpace(actual.FlightAvailRq.ProviderSpace, expected.FlightAvailRq.ProviderSpace) || !AreEqualFlightSearchCriteria(actual.FlightAvailRq.SearchCriteria, expected.FlightAvailRq.SearchCriteria))
                return false;

            if (!AreEqualRequester(actual.FlightGetPricedRQ.Requester, expected.FlightGetPricedRQ.Requester))
                return false;

            if (actual.FlightPriceResult.Status != expected.FlightPriceResult.Status || !AreEqualFlightRecommendation(actual.FlightPriceResult.Recommendation, expected.FlightPriceResult.Recommendation))
                return false;

            if (!AreAdditionalInfoDictionaryEqual(actual.FlightPriceRQ.AdditionalInfo, expected.FlightPriceRQ.AdditionalInfo) || !AreEqualRequester(actual.FlightPriceRQ.Requester, expected.FlightPriceRQ.Requester))
                return false;

            if (!AreAdditionalInfoDictionaryEqual(actual.FlightRetrieveRQ.AdditionalInfo, expected.FlightRetrieveRQ.AdditionalInfo) || !AreEqualRequester(actual.FlightRetrieveRQ.Requester, expected.FlightRetrieveRQ.Requester) ||
                !actual.FlightRetrieveRQ.RecordLocator.Equals(expected.FlightRetrieveRQ.RecordLocator))
                return false;

            if (!AreEqualFlightRecommendation(actual.FlightProduct.Recommendation, expected.FlightProduct.Recommendation))
                return false;

            if (!AreEqualFlightRecommendation(actual.FlightRetrieveRS.Product.Recommendation, expected.FlightRetrieveRS.Product.Recommendation))
                return false;

            if (actual.FlightRecommendationInfo.FlightRecommendationRefId != expected.FlightRecommendationInfo.FlightRecommendationRefId)
                return false;

            if (actual.FlightSaveRQ.FlightRecommendationInfo.FlightRecommendationRefId != expected.FlightSaveRQ.FlightRecommendationInfo.FlightRecommendationRefId)
                return false;

            if (actual.FlightSaveRS.Status != expected.FlightSaveRS.Status || !actual.FlightSaveRS.RecordLocator.Equals(expected.FlightSaveRS.RecordLocator))
                return false;

            if (actual.PaxTypeFareBasis.PassengerType != expected.PaxTypeFareBasis.PassengerType || actual.PreferenceTypeOptions != expected.PreferenceTypeOptions ||
                actual.ProviderSpacePriceConfiguration.Id != expected.ProviderSpacePriceConfiguration.Id)
                return false;

            if (!AreEqualProviderSpaceReplacementPriceConfiguration(actual.ProviderSpaceReplacementPriceConfiguration, expected.ProviderSpaceReplacementPriceConfiguration))
                return false;

            if (actual.PriceConfiguration.FareSourcePriceConfigurations.ToListBuffered()[0].Id != expected.PriceConfiguration.FareSourcePriceConfigurations.ToListBuffered()[0].Id)
                return false;

            if (!AreEqualProviderSpaceReplacementPriceConfiguration(actual.PriceConfiguration.FareSourceReplacementPriceConfigurations.ToListBuffered()[0], expected.PriceConfiguration.FareSourceReplacementPriceConfigurations.ToListBuffered()[0]))
                return false;

            if (actual.SeatCharacteristic != expected.SeatCharacteristic || actual.SeatColumnType != expected.SeatColumnType || actual.SeatMapCabinType != expected.SeatMapCabinType)
                return false;

            if (actual.Seat.SeatCharacteristics[0] != expected.Seat.SeatCharacteristics[0] || actual.SeatColumn.ColumnType != expected.SeatColumn.ColumnType)
                return false;

            if (!AreEqualSeatMap(actual.SeatMap, expected.SeatMap))
                return false;

            if (actual.SeatMapRQ.AdditionalInfo["Company"] != expected.SeatMapRQ.AdditionalInfo["Company"])
                return false;

            if (actual.SeatMapCallStatus != expected.SeatMapCallStatus || actual.SeatMapServiceStatus.Status != expected.SeatMapServiceStatus.Status)
                return false;

            if (actual.SeatMapRS.ServiceStatus.Status != expected.SeatMapRS.ServiceStatus.Status || !AreEqualSeatMap(actual.SeatMapRS.SeatMap, expected.SeatMapRS.SeatMap)
                || !actual.SeatMapRS.SessionId.Equals(expected.SeatMapRS.SessionId))
                return false;

            if (actual.RowCharacteristic != expected.RowCharacteristic || actual.SeatRow.RowCharacteristics[0] != expected.SeatRow.RowCharacteristics[0])
                return false;

            if (!AreEqualAirport(actual.Airport, expected.Airport))
                return false;

            if (!AreEqualAirport(actual.SegmentStop.Airport, expected.SegmentStop.Airport))
                return false;

            if (actual.PointOfSale.ID != expected.PointOfSale.ID || actual.PointOfSale.Name != expected.PointOfSale.Name)
                return false;

            return true;
        }

        private static bool AreEqualAirport(Entities.Airport actual, Entities.Airport expected)
        {
            if (actual.Code != expected.Code || actual.City.Code != expected.City.Code)
                return false;

            return true;
        }

        private static bool AreEqualSeatMap(SeatMap.SeatMap actual, SeatMap.SeatMap expected)
        {
            if (!AreEqualCabinFacility(actual.Cabins[0].CabinFacilities[0], expected.Cabins[0].CabinFacilities[0]) || actual.CabinType != expected.CabinType)
                return false;

            return true;
        }

        private static bool AreEqualProviderSpaceReplacementPriceConfiguration(Pricing.ProviderSpaceReplacementPriceConfiguration actual, Pricing.ProviderSpaceReplacementPriceConfiguration expected)
        {
            if (actual.AirRuleSet.ClassOfServiceRules[0].Alignment != expected.AirRuleSet.ClassOfServiceRules[0].Alignment || actual.AirRuleSet.ClassOfServiceRules[0].RuleType != expected.AirRuleSet.ClassOfServiceRules[0].RuleType)
                return false;

            return true;
        }

        private static bool AreEqualFlightSearchCriteria(Avail.FlightSearchCriteria actual, Avail.FlightSearchCriteria expected)
        {
            if (!AreEqualFlightTravelPreference(actual.FlightTravelPreference, expected.FlightTravelPreference) || !AreEqualPaxTypeQuantity(actual.PassengerInfoSummary[0], expected.PassengerInfoSummary[0]) ||
                !AreAdditionalInfoDictionaryEqual(actual.AdditionalInfo, expected.AdditionalInfo) || !AreEqualSearchSegment(actual.SearchSegments[0], expected.SearchSegments[0]) ||
                actual.SortingOrder != expected.SortingOrder || !actual.SessionID.Equals(expected.SessionID))
                return false;

            return true;
        }

        private static bool AreEqualSearchSegment(Avail.SearchSegment actual, Avail.SearchSegment expected)
        {
            if (actual.ArrivalAlternateAirportInformation.IncludeNearByAirports != expected.ArrivalAlternateAirportInformation.IncludeNearByAirports || actual.Cabin != expected.Cabin ||
                actual.ConnectionPreferences[0].PreferenceType != expected.ConnectionPreferences[0].PreferenceType || actual.TravelDate.DateTime != expected.TravelDate.DateTime)
                return false;

            return true;
        }

        private static bool AreEqualPaxTypeQuantity(Entities.PaxTypeQuantity actual, Entities.PaxTypeQuantity expected)
        {
            if (actual.PassengerType != expected.PassengerType)
                return false;

            return true;
        }

        private static bool AreEqualFlightTravelPreference(Avail.FlightTravelPreference actual, Avail.FlightTravelPreference expected)
        {
            if (actual.AllowMixedAirlines != expected.AllowMixedAirlines || actual.AirlinesPreference.PermittedAirlines[0] != expected.AirlinesPreference.PermittedAirlines[0] ||
                actual.AirlinesPreference.PermittedAirlines[1] != expected.AirlinesPreference.PermittedAirlines[1])
                return false;

            return true;
        }

        private static bool AreEqualFlightFare(Entities.FlightFare actual, Entities.FlightFare expected)
        {
            if (!AreEqualMoney(actual.Savings, expected.Savings) || actual.FareType != expected.FareType || actual.FareAttributes[0] != expected.FareAttributes[0]
                || !AreEqualPassengerFare(actual.PassengerFares[0], expected.PassengerFares[0]))
                return false;

            return true;
        }

        private static bool AreEqualFlightRecommendation(Entities.FlightRecommendation actual, Entities.FlightRecommendation expected)
        {
            if (!AreAdditionalInfoDictionaryEqual(actual.AdditionalInfo, expected.AdditionalInfo) || !AreEqualFlightFare(actual.Fare, expected.Fare) || actual.ItineraryType != expected.ItineraryType ||
                !AreEqualFlightLeg(actual.Legs[0], expected.Legs[0]) || !AreEqualProviderSpace(actual.ProviderSpace, expected.ProviderSpace) || !AreEqualAirContract(actual.Contract, expected.Contract))
                return false;

            return true;
        }

        private static bool AreEqualBaggageDetails(Entities.Baggage actual, Entities.Baggage expected)
        {
            if (actual.Description != expected.Description || actual.WeightInPounds != expected.WeightInPounds || actual.Quantity != expected.Quantity)
                return false;

            return true;
        }

        private static bool AreEqualAirContract(Entities.AirContract actual, Entities.AirContract expected)
        {
            if (actual.AgencyId != expected.AgencyId || actual.Airlines[0] != expected.Airlines[0] || actual.Airlines[1] != expected.Airlines[1] ||
                actual.TicketingPeriod.Start != expected.TicketingPeriod.Start || actual.TicketingPeriod.End != expected.TicketingPeriod.End)
                return false;

            return true;
        }

        private static bool AreEqualProviderSpace(Entities.ProviderSpace actual, Entities.ProviderSpace expected)
        {
            if (actual.ID != expected.ID || actual.Name != expected.Name || !AreEqualRequester(actual.Requester, expected.Requester) || actual.Family != expected.Family ||
                actual.EmulatedBy != expected.EmulatedBy || actual.OwnerId != expected.OwnerId || actual.Type != expected.Type || !AreAdditionalInfoDictionaryEqual(actual.AdditionalInfo, expected.AdditionalInfo) ||
                actual.Configurations.ToListBuffered()[0].ConfigType != expected.Configurations.ToListBuffered()[0].ConfigType ||
                actual.Configurations.ToListBuffered()[0].ServiceUri.AbsoluteUri != expected.Configurations.ToListBuffered()[0].ServiceUri.AbsoluteUri
                || !AreEqualAirContract(actual.Contracts[0], expected.Contracts[0]))
                return false;

            return true;
        }

        private static bool AreEqualRequester(Entities.Requester actual, Entities.Requester expected)
        {
            if (actual.ID != expected.ID || actual.Type != expected.Type || actual.POS.ID != expected.POS.ID)
                return false;

            return true;
        }

        private static bool AreEqualFlightLeg(Entities.FlightLeg actual, Entities.FlightLeg expected)
        {
            if (actual.LegType != expected.LegType || !AreEqualFlightSegment(actual.Segments[0], expected.Segments[0]))
                return false;

            return true;
        }

        private static bool AreEqualFlightSegment(Entities.FlightSegment actual, Entities.FlightSegment expected)
        {
            if (actual.DepartureAirport.Code != expected.DepartureAirport.Code || actual.ArrivalAirport.Code != expected.ArrivalAirport.Code || actual.DepartureDateTime != expected.DepartureDateTime || actual.ArrivalDateTime != expected.ArrivalDateTime
                || actual.MarketingAirline.Code != expected.MarketingAirline.Code || actual.FlightNumber != expected.FlightNumber || actual.SegmentIndex != expected.SegmentIndex || !AreEqualBaggageDetails(actual.BaggageDetails[0], expected.BaggageDetails[0])
                || !AreEqualBaggageDetails(actual.BaggageDetails[1], expected.BaggageDetails[1]))
                return false;

            return true;
        }

        private static bool AreEqualPassengerFare(Entities.PassengerFare actual, Entities.PassengerFare expected)
        {
            if (!AreEqualMoney(actual.BaseAmount, expected.BaseAmount) || actual.PassengerType != expected.PassengerType || actual.Commissions[0].FareComponentType != expected.Commissions[0].FareComponentType)
                return false;

            return true;
        }

        private static bool AreEqualFareRule(FareRules.FareRule actual, FareRules.FareRule expected)
        {
            if (actual.FareBasisCode != expected.FareBasisCode || actual.FareRuleDetails[0].Title != expected.FareRuleDetails[0].Title)
                return false;

            return true;
        }

        private static bool AreAdditionalInfoDictionaryEqual(AdditionalInfoDictionary actual, AdditionalInfoDictionary expected)
        {
            if (actual["Flight"] != expected["Flight"] || actual["Airport"] != expected["Airport"])
                return false;

            return true;
        }

        private static bool AreEqualCabinFacility(SeatMap.CabinFacility actual, SeatMap.CabinFacility expected)
        {
            if (actual.CabinFacilityLocation != expected.CabinFacilityLocation || actual.CabinFacilityType != expected.CabinFacilityType)
                return false;

            return true;
        }

        private static bool AreEqualMoney(Entities.Money actual, Entities.Money expected)
        {
            if (actual.NativeAmount != expected.NativeAmount || actual.NativeCurrency != expected.NativeCurrency || actual.BaseAmount != expected.BaseAmount || actual.BaseCurrency != expected.BaseCurrency
                || actual.DisplayAmount != expected.DisplayAmount || actual.DisplayCurrency != expected.DisplayCurrency)
                return false;

            return true;
        }

        #endregion
    }
}
