﻿using System;
using System.Collections.Generic;
using System.Linq;
using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace ComponentTest
{
    [TestClass]
    public class AirContractsSearchTests : TestBase
    {
        private const long MockedAccountId = 123;
        private const long MockedProviderId = 123;

        public override AmbientContextBase GetContext()
        {
            System.Runtime.Remoting.Messaging.CallContext.LogicalSetData("scenario",
                                                                         MockKeys.TestScenario.ValidSearchContracts);
            return new CallContext("en-us", Guid.NewGuid().ToString(),1.ToString(), MockedAccountId.ToString(),"USD", "", false);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidTravelPeriod()
        {
            var contractManager = GetContractManager();

            var searchCriteria = GetValidSearchCriteria();
            searchCriteria.SearchSegments[0].TravelDate = GetInValidTravelDate();

            var contracts = contractManager.LoadContractsForProvider(MockedProviderId, searchCriteria);

            Assert.IsTrue(contracts == null || contracts.Count == 0);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestValidRequest()
        {
            var contractManager = GetContractManager();

            var searchCriteria = GetValidSearchCriteria();

            var contracts = contractManager.LoadContractsForProvider(MockedProviderId, searchCriteria);

            Assert.IsTrue(contracts.Count == 1);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidAgency()
        {
            try
            {
                typeof (CallContext).GetProperty("AccountId").SetValue(Utility.GetCurrentContext(), -1);
                var contractManager = GetContractManager();

                var searchCriteria = GetValidSearchCriteria();

                var contracts = contractManager.LoadContractsForProvider(123, searchCriteria);

                Assert.IsTrue(contracts == null || contracts.Count == 0);
            }
            finally
            {
                typeof(CallContext).GetProperty("AccountId").SetValue(Utility.GetCurrentContext(), MockedAccountId);
            }
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInValidValidity()
        {
            System.Runtime.Remoting.Messaging.CallContext.LogicalSetData("scenario",
                                                                         MockKeys.TestScenario.InvalidSearchValidPeriod);
            var searchCriteria = GetValidSearchCriteria();

            var contracts = GetContractManager().LoadContractsForProvider(MockedProviderId, searchCriteria);

            Assert.IsTrue(contracts == null || contracts.Count == 0);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestPaperContracts()
        {
            System.Runtime.Remoting.Messaging.CallContext.LogicalSetData("scenario", MockKeys.TestScenario.PaperContracts);

            var contractManager = GetContractManager();

            var searchCriteria = GetValidSearchCriteria();

            var contracts = contractManager.LoadContractsForProvider(MockedProviderId, searchCriteria);

            Assert.IsTrue(contracts.Count == 1);
            Assert.IsTrue(contracts.First().ContractType == MockKeys.PaperContract);
        }

        #region private Fields/methods

        private IContractManager GetContractManager()
        {
            return new MockContractManager();
        }

        private FlightSearchCriteria GetValidSearchCriteria()
        {
            return new FlightSearchCriteria() {SearchSegments = new List<SearchSegment>() {new SearchSegment(){TravelDate = GetValidTravelDate()}}};
        }

        private TravelDate GetValidTravelDate()
        {
            return new TravelDate() {DateTime = DateTime.Now.AddHours(1)};
        }

        private TravelDate GetInValidTravelDate()
        {
            return new TravelDate() {DateTime = DateTime.Now.AddHours(-3)};
        }
        #endregion

    }
}
