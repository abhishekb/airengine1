﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Common.TestHelpers.Mock;
using Tavisca.TravelNxt.Flight.Entities;
using KeyStore = Tavisca.TravelNxt.Flight.Settings.KeyStore;
using SettingManager = Tavisca.TravelNxt.Flight.Settings.SettingManager;

namespace ComponentTest
{
    [TestClass]
    public class RecommendationKeyTests : TestBase
    {
        private MockAppSettingProvider _mProvider;

        public override void TestInitializeAfterContextInject()
        {
            _mProvider = new MockAppSettingProvider();

            Assert.IsNotNull(SettingManager.AirConfigurationConnectionString);

            MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                new ShallowWrappedSettingProvider().WithSetInnerProvider(_mProvider));
        }

        [TestMethod]
        public void TestRecommendationKeyWithoutClassOfService()
        {
            ConfigurationManager.AppSettings[KeyStore.AppSettings.UseClassOfServiceInItineraryIdentification] =
                false.ToString();

            var recommendation = GetRecommendationWithSingleSegment();

            Assert.IsFalse(
                recommendation.Legs.All(
                    leg =>
                        leg.Segments.All(
                            segment => recommendation.Key.Contains(segment.PaxTypeFareBasisCodes.First().ClassOfService))));
        }

        [TestMethod]
        public void TestRecommendationKeyWithClassOfService()
        {
            ConfigurationManager.AppSettings[KeyStore.AppSettings.UseClassOfServiceInItineraryIdentification] =
                true.ToString();

            var recommendation = GetRecommendationWithSingleSegment();

            Assert.IsTrue(
                recommendation.Legs.All(
                    leg =>
                        leg.Segments.All(
                            segment => recommendation.Key.Contains(segment.PaxTypeFareBasisCodes.First().ClassOfService))));
        }

        #region private methods

        private static FlightRecommendation GetRecommendationWithSingleSegment()
        {
            var recommendation = new FlightRecommendation() { Legs = new FlightLegCollection() };

            var segment = new FlightSegment("LAS", "LAX", DateTime.Now.Date, DateTime.Now.AddDays(2).Date,
                123.ToString(), "MA", 1)
            {
                PaxTypeFareBasisCodes =
                    new List<PaxTypeFareBasis>() { new PaxTypeFareBasis() { ClassOfService = "COS" } }
            };

            var leg = new FlightLeg { Segments = new FlightSegmentCollection { segment } };
            recommendation.Legs.Add(leg);

            return recommendation;
        }

        #endregion
    }
}
