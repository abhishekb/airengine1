﻿using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Common.Validation;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;

namespace ComponentTest
{
    [TestClass]
    public class ValidationTestCases : TestBase
    {
        #region Fields

        private static readonly IContentManagerFactory ContentManagerFactory =
            RuntimeContext.Resolver.Resolve<IContentManagerFactory>();

        #endregion

        #region Test Setup

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", 
                Tavisca.TravelNxt.Common.Settings.StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), 
                "INR", "password", true);
        }

        #endregion

        #region Tests

        [TestMethod, TestCategory("Validation Tests")]
        public void TravelTimeTooNearTest()
        {
            var date = GetTravelDate(2, "JFK");

            var result = Validate(date);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void TravelTimeTooFarTest()
        {
            var date = GetTravelDate(331 * 24, "JFK");

            var result = Validate(date);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void TravelTimeValidTest()
        {
            var date = GetTravelDate(25, "JFK");

            var result = Validate(date);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void ConflictingAirlineCodePositiveEmpty()
        {
            var airlinePreference = new AirlinesPreference();

            var result = Validate(airlinePreference);

            Assert.IsTrue(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void ConflictingAirlineCodePositiveNull()
        {
            var airlinePreference = new FlightTravelPreference();

            var result = Validate(airlinePreference);

            Assert.IsTrue(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodePositiveWithValues()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() {"9W"};

            airlinePreference.PreferredAirlines = new List<string>() {"AF"};

            airlinePreference.ProhibitedAirlines = new List<string>() {"DL"};

            var result = Validate(airlinePreference);

            Assert.IsTrue(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithEmptyValuesPermitted()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "DL" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithEmptyValuesPreferred()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "AF" };

            airlinePreference.PreferredAirlines = new List<string>() { "" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "DL" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithEmptyValuesProhibited()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "DL" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithInvalidValuesPermitted()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "Blah" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "DL" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithInvalidValuesPreferred()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "AF" };

            airlinePreference.PreferredAirlines = new List<string>() { "Blah" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "DL" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeNegativeWithInvalidValuesProhibited()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "DL" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "Blah" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeConflictingValuesWithPreferred()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "DL" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "AF" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void AirlinesPreferencesAirlineCodeConflictingValuesWithPermitted()
        {
            var airlinePreference = new AirlinesPreference();

            airlinePreference.PermittedAirlines = new List<string>() { "DL" };

            airlinePreference.PreferredAirlines = new List<string>() { "AF" };

            airlinePreference.ProhibitedAirlines = new List<string>() { "AF" };

            var result = Validate(airlinePreference);

            Assert.IsFalse(result.IsValid);
        }

        [TestMethod, TestCategory("Validation Tests")]
        public void ArrivalDepartureAirportCodeSame()
        {
            var request = RequestFactory.GetOneAdultServiceSearchRequest();
            request.SearchCriterion.SearchSegments[0].ArrivalAirportCode = "LAS";
            request.SearchCriterion.SearchSegments[0].DepartureAirportCode = "las";
            var availResult = new AirEngine().Search(request);

            Assert.IsNotNull(availResult);
            Assert.AreEqual(availResult.ServiceStatus.Messages[0],
                "In 'SearchSegment' 'ArrivalLocation' must not be same as 'DepartureLocation'.");
        }

        [TestMethod]
        public void TestBothAirportAndCityInRequest()
        {
            var request = RequestFactory.GetOneAdultServiceSearchRequest();
            request.SearchCriterion.SearchSegments[0].ArrivalCityCode =
                request.SearchCriterion.SearchSegments[0].ArrivalAirportCode;
            request.SearchCriterion.SearchSegments[0].DepartureCityCode =
                request.SearchCriterion.SearchSegments[0].DepartureAirportCode;

            var availResult = new AirEngine().Search(request);

            Assert.IsNotNull(availResult);
            const string error1 =
                "In \"SearchSegment\" either \"DepartureAirportCode\" or \"DepartureCityCode\" can be specified but not both.";
            const string error2 =
                "In \"SearchSegment\" either \"ArrivalAirportCode\" or \"ArrivalCityCode\" can be specified but not both.";
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(error1));
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(error2));
        }

        [TestMethod]
        public void TestBothAirportAndCityInfoMissing()
        {
            var request = RequestFactory.GetOneAdultServiceSearchRequest();
            request.SearchCriterion.SearchSegments[0].ArrivalCityCode =
                request.SearchCriterion.SearchSegments[0].ArrivalAirportCode = string.Empty;
            request.SearchCriterion.SearchSegments[0].DepartureCityCode =
                request.SearchCriterion.SearchSegments[0].DepartureAirportCode = string.Empty;

            var availResult = new AirEngine().Search(request);

            Assert.IsNotNull(availResult);
            const string error1 =
                "In \"SearchSegment\" either one of \"ArrivalAirportCode\" / \"ArrivalCityCode\" needs to be specified.";
            const string error2 =
                "In \"SearchSegment\" either one of \"DepartureAirportCode\" / \"DepartureCityCode\" needs to be specified."; 
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(error1));
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(error2));
        }

        [TestMethod]
        public void TestInvalidCityCodeInRequest()
        {
            const string invalidArrivalCity = "Kratos's_arrival";
            const string invalidDepartureCity = "Kratos's_departure";

            var request = RequestFactory.GetOneAdultServiceSearchRequest();
            request.SearchCriterion.SearchSegments[0].ArrivalAirportCode = string.Empty;
            request.SearchCriterion.SearchSegments[0].ArrivalCityCode = invalidArrivalCity;

            request.SearchCriterion.SearchSegments[0].DepartureAirportCode = string.Empty;
            request.SearchCriterion.SearchSegments[0].DepartureCityCode = invalidDepartureCity;


            var availResult = new AirEngine().Search(request);

            Assert.IsNotNull(availResult);
            var arrivalError =
                string.Format(
                    "In \"SearchSegment\" \"ArrivalCityCode\" value of '{0}' is either not supported or is invalid.",
                    invalidArrivalCity);
            var departError =
                string.Format(
                    "In \"SearchSegment\" \"DepartureCityCode\" value of '{0}' is either not supported or is invalid.",
                    invalidDepartureCity);
            
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(arrivalError));
            Assert.IsTrue(availResult.ServiceStatus.Messages.Contains(departError));
        }

        #endregion

        #region Helper Methods

        private Tavisca.TravelNxt.Flight.Avail.DataContract.TravelDate GetTravelDate(int searchDateHourOffset, string airportCode)
        {
            var timeConverter = RuntimeContext.Resolver.Resolve<ITimeConverter>();

            var airport = ContentManagerFactory.GetContentManager().GetAirportByCode(airportCode);

            if (airport == null)
                throw new InternalTestFailureException("Failed to load content for airport code provided in the test. The code was: " + airportCode);

            var timeZoneInfo = airport.ToTimeZoneInfo();

            var currentTime = timeConverter.GetLocalTimeAtLocation(timeZoneInfo, DateTime.UtcNow);

            var travelDate = new Tavisca.TravelNxt.Flight.Avail.DataContract.TravelDate()
                                           {
                                               DateTime = currentTime.AddHours(searchDateHourOffset),
                                               DepartureLocation = airport.Code
                                           };

            return travelDate;
        }

        private ValidationResult Validate<T>(T travelDate) where T: class
        {
            return GetValidationFactory().Validate(travelDate);
        }

        private IValidationFactory GetValidationFactory()
        {
            return RuntimeContext.Resolver.Resolve<IValidationFactory>();
        }

        #endregion
    }
}
