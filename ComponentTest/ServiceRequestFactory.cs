﻿using System;
using System.Collections.Generic;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace ComponentTest
{
    public static partial class RequestFactory
    {
        public static FlightSearchRQ GetOneAdultServiceSearchRequest()
        {
            var request = GetBaseRequest();

            SetOneAdultInPassenger(request.SearchCriterion);

            return request;
        }

        #region private methods

        private static void SetOneAdultInPassenger(FlightSearchCriteria flightSearchCriteria)
        {
            flightSearchCriteria.PassengerInfoSummary.Add(new PaxTypeQuantity()
                                                              {
                                                                  PassengerType = PassengerType.Adult,
                                                                  Quantity = 1,
                                                                  Ages = new List<int>() {25}
                                                              });
        }

        private static FlightSearchRQ GetBaseRequest()
        {
            var flightSearchCriteria = new FlightSearchCriteria() {};

            //flightSearchCriteria.AdditionalInfo.Add("mock", "true");

            flightSearchCriteria.SortingOrder = SortingOrder.ByPriceLowToHigh;
            flightSearchCriteria.SearchSegments = new List<SearchSegment>();
            flightSearchCriteria.SearchSegments.Add(new SearchSegment
            {
                ArrivalAirportCode = "LAX",
                ArrivalAlternateAirportInformation =
                    new AlternateAirportInformation()
                    {
                        IncludeNearByAirports = true,
                        RadiusKm = 10
                    },
                DepartureAirportCode = "LAS",
                TravelDate =
                    new TravelDate
                    {
                        DateTime =
                            DateTime.Now
                            .AddMonths(3).Date

                    }
            });

            flightSearchCriteria.PassengerInfoSummary = new List<PaxTypeQuantity>();

            var request = new FlightSearchRQ() {SearchCriterion = flightSearchCriteria};
            request.Requester = new Requester() {CustomerType = CustomerType.Agent};
            return request;
        }

        #endregion
    }
}
