﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareComponent;
using Tavisca.TravelNxt.Common.Extensions;

namespace ComponentTest
{
    [TestClass]
    public class FareManagerTests
    {
        private static readonly CallContext WorldspanCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR", true);

        private const string Currency = "USD";

        [TestMethod]
        public void TestPercentMarkupOnBaseForPerPerson()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var flightFare = GetFlightFare();

                var farecomponent = GetFareComponentRule();
                farecomponent.FareComponentValue.FareApplicableOn = FareApplicableOnOptions.BaseFare;
                farecomponent.FareComponentValue.CalculationType = CalculationTypeOptions.PerPerson;

                var money = MarkupCalculator.GetPercentCalculatedAmount(flightFare, farecomponent);

                Assert.IsTrue(money.NativeAmount.Equals(new decimal(20)));
            }
        }

        [TestMethod]
        public void TestPercentMarkupOnBaseForItinerary()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var flightFare = GetFlightFare();

                var farecomponent = GetFareComponentRule();
                farecomponent.FareComponentValue.FareApplicableOn = FareApplicableOnOptions.BaseFare;
                farecomponent.FareComponentValue.CalculationType = CalculationTypeOptions.PerJourney;

                var money = MarkupCalculator.GetPercentCalculatedAmount(flightFare, farecomponent);

                Assert.IsTrue(money.NativeAmount.Equals(new decimal(10)));
            }
        }

        [TestMethod]
        public void TestPercentMarkupOnTotalForPerPerson()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var flightFare = GetFlightFare();

                var farecomponent = GetFareComponentRule();
                farecomponent.FareComponentValue.FareApplicableOn = FareApplicableOnOptions.TotalFare;
                farecomponent.FareComponentValue.CalculationType = CalculationTypeOptions.PerPerson;
                farecomponent.FareComponentValue.MaxAmt = 12;

                var money = MarkupCalculator.GetPercentCalculatedAmount(flightFare, farecomponent);

                Assert.IsTrue(money.NativeAmount.Equals(new decimal(24)));
            }
        }

        [TestMethod]
        public void TestPercentMarkupOnTotalForItinerary()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var flightFare = GetFlightFare();

                var farecomponent = GetFareComponentRule();
                farecomponent.FareComponentValue.FareApplicableOn = FareApplicableOnOptions.TotalFare;
                farecomponent.FareComponentValue.CalculationType = CalculationTypeOptions.PerJourney;
                farecomponent.FareComponentValue.MaxAmt = 12;

                var money = MarkupCalculator.GetPercentCalculatedAmount(flightFare, farecomponent);

                Assert.IsTrue(money.NativeAmount.Equals(new decimal(12)));
            }
        }

        [TestMethod]
        public void TestPercentMarkupOnBaseForPerPersonAndMultiplePassengers()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var flightFare = GetFlightFare();
                flightFare.PassengerFares.Add(new PassengerFare()
                {
                    Quantity = 3,
                    BaseAmount = new Money(200, Currency),
                    PassengerType = PassengerType.Adult
                });

                var farecomponent = GetFareComponentRule();
                farecomponent.FareComponentValue.MaxAmt = 30;
                farecomponent.FareComponentValue.FareApplicableOn = FareApplicableOnOptions.BaseFare;
                farecomponent.FareComponentValue.CalculationType = CalculationTypeOptions.PerPerson;

                var money = MarkupCalculator.GetPercentCalculatedAmount(flightFare, farecomponent);

                Assert.IsTrue(money.NativeAmount.Equals(new decimal(80)));
            }
        }

        private static FlightFare GetFlightFare()
        {
            return new FlightFare()
            {
                PassengerFares =
                    new List<PassengerFare>()
                    {
                        new PassengerFare()
                        {
                            Quantity = 2,
                            PassengerType = PassengerType.Adult,
                            BaseAmount = new Money(100, "USD"),
                            Fees =
                                new List<FareComponent>()
                                {
                                    new FareComponent()
                                    {
                                        FareComponentType = FareComponentType.Fees,
                                        Value = new Money(20, Currency)
                                    }
                                }
                        }
                    }
            };
        }

        private static FareComponentRule GetFareComponentRule()
        {
            return new FareComponentRule()
                {
                    FareComponentType = FareComponentTypeOptions.Markup,
                    FareComponentValue =
                        new FareComponentValue()
                        {
                            FareApplicableOn = FareApplicableOnOptions.TotalFare,
                            CalculationType = CalculationTypeOptions.PerPerson,
                            FareComponentValueType = FareComponentValueType.Percent,
                            MaxAmt = 10,
                            MinAmt = 0,
                            Currency = Currency,
                            Amount = 10
                        }
                };
        }
    }
}
