﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace ComponentTest
{
    public static partial class RequestFactory
    {
        private static FlightAvailRq GetBaseSearchRequest()
        {
            var flightSearchCriteria = new FlightSearchCriteria() {AdditionalInfo = new AdditionalInfoDictionary()};

            flightSearchCriteria.SortingOrder = SortingOrder.ByPriceLowToHigh;
            flightSearchCriteria.SearchSegments = new List<SearchSegment>();
            flightSearchCriteria.SearchSegments.Add(new SearchSegment
                                                        {
                                                            ArrivalAirportCode = "LAX",
                                                            ArrivalAlternateAirportInformation =
                                                                new AlternateAirportInformation()
                                                                    {
                                                                        IncludeNearByAirports = true,
                                                                        RadiusKm = 10
                                                                    },
                                                            DepartureAirportCode = "LAS",
                                                            TravelDate =
                                                                new TravelDate
                                                                    {
                                                                        DateTime =
                                                                            DateTime.Now
                                                                            .AddMonths(3).Date,
                                                                        TimeWindow = 2
                                                                    }
                                                        });

            flightSearchCriteria.FlightTravelPreference = new FlightTravelPreference()
                                                              {
                                                                  AirlinesPreference =
                                                                      new AirlinesPreference()
                                                                          {
                                                                              PermittedAirlines = new List<string>(),
                                                                              PreferredAirlines = new List<string>(),
                                                                              ProhibitedAirlines = new List<string>()
                                                                          }
                                                              };

            var providerSpaceManager = RuntimeContext.Resolver.Resolve<IProviderSpaceManager>();

            var requester = new Requester(int.Parse(TestCaseSettingManager.AccountId), int.Parse(TestCaseSettingManager.WorldspanPos), CustomerType.Agent);

            var providerSpaces = providerSpaceManager.LoadProviderSpaceForAvail(requester, flightSearchCriteria);

            return new FlightAvailRq(providerSpaces[0], flightSearchCriteria);
        }

        private static FlightAvailRq GetBasicRequest()
        {
            var req = GetBaseSearchRequest();

            return req;
        }

        public static FlightAvailRq GetOneAdultProviderSearchRequest()
        {
            var request = GetBasicRequest();

            SetOneAdultInPassenger(request);

            return request;
        }

        private static void SetOneAdultInPassenger(FlightAvailRq req)
        {
            req.SearchCriteria.PassengerInfoSummary = new List<PaxTypeQuantity>();
            req.SearchCriteria.PassengerInfoSummary.Add(new PaxTypeQuantity { Ages = new List<int>() { 20 } });
            req.SearchCriteria.PassengerInfoSummary[0].Quantity = 1;
            req.SearchCriteria.PassengerInfoSummary[0].PassengerType = PassengerType.Adult;
        }

        private static void SetTwoAdultInPassenger(FlightAvailRq req)
        {
            req.SearchCriteria.PassengerInfoSummary = new List<PaxTypeQuantity>();
            req.SearchCriteria.PassengerInfoSummary.Add(new PaxTypeQuantity { Ages = new List<int>() { 20, 25 } });
            req.SearchCriteria.PassengerInfoSummary[0].Quantity = 2;
            req.SearchCriteria.PassengerInfoSummary[0].PassengerType = PassengerType.Adult;
        }

        public static FlightAvailRq GetTwoAdultProviderSearchRequest()
        {
            var request = GetBasicRequest();

            SetTwoAdultInPassenger(request);

            return request;
        }

        public static int WaitTimeOut
        {
            get { return _waitTimeOut; }
            set { _waitTimeOut = value; }
        }

        //private static string _sessionId;
        private static int _waitTimeOut = 1000;

        public static string SessionId
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }
    }
}
