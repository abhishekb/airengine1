﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Entities;

namespace ComponentTest
{
    [TestClass]
    public class FlightFareDetailTests
    {
        [TestMethod]
        public void TestOriginDestinationExtractionForOneWay()
        {
            var fareDetails = FlightFareDetailsExtracter.GetFlightFareDetails(GetOnewayRecommendation(), false);

            Assert.IsTrue(fareDetails.OriginAirportCode.Equals("LAS"));
            Assert.IsTrue(fareDetails.DestinationAirportCode.Equals("LAX"));

            Assert.IsTrue(fareDetails.OriginCountry.Equals("US"));
            Assert.IsTrue(fareDetails.DestinationCountry.Equals("NONUS"));
        }

        [TestMethod]
        public void TestOriginDestinationExtractionForRoundTrip()
        {
            var fareDetails = FlightFareDetailsExtracter.GetFlightFareDetails(GetRoundtripRecommendation(), false);

            Assert.IsTrue(fareDetails.OriginAirportCode.Equals("LAS"));
            Assert.IsTrue(fareDetails.DestinationAirportCode.Equals("MCO"));

            Assert.IsTrue(fareDetails.OriginCountry.Equals("US"));
            Assert.IsTrue(fareDetails.DestinationCountry.Equals("LON"));
        }

        [TestMethod]
        public void TestOriginDestinationExtractionForMulticity()
        {
            var fareDetails = FlightFareDetailsExtracter.GetFlightFareDetails(GetMulticityRecommendation(), false);

            Assert.IsTrue(fareDetails.OriginAirportCode.Equals("LAS"));
            Assert.IsTrue(fareDetails.DestinationAirportCode.Equals("LAX"));

            Assert.IsTrue(fareDetails.OriginCountry.Equals("US"));
            Assert.IsTrue(fareDetails.DestinationCountry.Equals("US"));
        }

        #region private methods

        private static FlightRecommendation GetOnewayRecommendation()
        {
            var recommendation = new FlightRecommendation()
            {
                Legs = new FlightLegCollection(),
                ItineraryType = ItineraryTypeOptions.OneWay
            };

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("LAS", "LAS1", DateTime.Now, DateTime.Now.AddDays(2), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        },
                        new FlightSegment("LAS1", "LAX", DateTime.Now.AddDays(2), DateTime.Now.AddDays(3), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        },
                    }
            });

            recommendation.Legs[0].Segments[0].DepartureAirport.City = new City() { CountryCode = "US" };
            recommendation.Legs[0].Segments[0].ArrivalAirport.City = new City() { CountryCode = "US" };
            recommendation.Legs[0].Segments[1].DepartureAirport.City = new City() { CountryCode = "US" };
            recommendation.Legs[0].Segments[1].ArrivalAirport.City = new City() { CountryCode = "NONUS" };

            return recommendation;
        }

        private static FlightRecommendation GetRoundtripRecommendation()
        {
            var recommendation = new FlightRecommendation()
            {
                Legs = new FlightLegCollection(),
                ItineraryType = ItineraryTypeOptions.RoundTrip
            };

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("LAS", "MCO", DateTime.Now, DateTime.Now.AddDays(2), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        }
                    }
            });

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("MCO", "LAS", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        }
                    }
            });

            recommendation.Legs[0].Segments[0].DepartureAirport.City = new City() { CountryCode = "US" };
            recommendation.Legs[0].Segments[0].ArrivalAirport.City = new City() { CountryCode = "LON" };
            recommendation.Legs[1].Segments[0].DepartureAirport.City = new City() { CountryCode = "LON" };
            recommendation.Legs[1].Segments[0].ArrivalAirport.City = new City() { CountryCode = "US" };

            return recommendation;
        }

        private static FlightRecommendation GetMulticityRecommendation()
        {
            var recommendation = new FlightRecommendation()
            {
                Legs = new FlightLegCollection(),
                ItineraryType = ItineraryTypeOptions.MultiCity
            };

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("LAS", "MCO", DateTime.Now, DateTime.Now.AddDays(2), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        }
                    }
            });

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("MCO", "DEL", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        }
                    }
            });

            recommendation.Legs.Add(new FlightLeg()
            {
                Segments =
                    new FlightSegmentCollection()
                    {
                        new FlightSegment("DEL", "LAX", DateTime.Now.AddDays(3), DateTime.Now.AddDays(4), "fn", "ma", 1)
                        {
                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {GetPaxTypeFareBasis()}
                        }
                    }
            });

            recommendation.Legs[0].Segments[0].DepartureAirport.City = new City() { CountryCode = "US" };
            recommendation.Legs[0].Segments[0].ArrivalAirport.City = new City() { CountryCode = "LON" };
            recommendation.Legs[1].Segments[0].DepartureAirport.City = new City() { CountryCode = "LON" };
            recommendation.Legs[1].Segments[0].ArrivalAirport.City = new City() { CountryCode = "IN" };
            recommendation.Legs[2].Segments[0].DepartureAirport.City = new City() { CountryCode = "IN" };
            recommendation.Legs[2].Segments[0].ArrivalAirport.City = new City() { CountryCode = "US" };

            return recommendation;
        }

        private static PaxTypeFareBasis GetPaxTypeFareBasis()
        {
            return new PaxTypeFareBasis() { ClassOfService = "COS" };
        }

        #endregion
    }
}