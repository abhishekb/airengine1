﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Common.TestHelpers.Mock;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;
using Tavisca.TravelNxt.Flight.Entities;
using KeyStore = Tavisca.TravelNxt.Flight.Settings.KeyStore;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using SettingManager = Tavisca.TravelNxt.Flight.Settings.SettingManager;

namespace ComponentTest
{
    [TestClass]
    public class InterAirlineAgreementTests : TestBase
    {
        #region Test Setup

        private MockAppSettingProvider _mProvider;

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), "INR", "password", true);
        }

        public override void TestInitializeAfterContextInject()
        {
            InitializeTestOverrideConfigurations();

            var provider = RuntimeContext.Resolver.Resolve<IResolveCacheProvider>()
                .GetCacheProvider(CacheCategory.AirCore);

            provider.Remove(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.TicketingAgreementMap);
            provider.Remove(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.PosFaresourceCarriers);

            _mProvider = new MockAppSettingProvider();

            Assert.IsNotNull(SettingManager.AirConfigurationConnectionString);

            MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                new ShallowWrappedSettingProvider().WithSetInnerProvider(_mProvider));
        }

        public override void TestCleanupBeforeContextCleanup()
        {
            var builder = new StringBuilder();

            try
            {
                MockAirlineTicketingInfoProvider.ClearAllOverrides();
            }
            catch(Exception ex)
            {
                builder.Append(ex.ToString());
            }

            try
            {
                if (MockSettingsLoader.Instance != null)
                    MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                                                                              new FieldWrappedSettingsProvider().
                                                                                  WithSetInnerProvider(
                                                                                      new AppSettingProvider()));

                MockAppSettingProvider.ClearAdditionalSettings();
            }
            catch(Exception ex)
            {
                builder.Append(Environment.NewLine).Append(Environment.NewLine).Append(ex.ToString());
            }

            if (builder.Length > 10)
                throw new InternalTestFailureException(builder.ToString());
        }

        private void InitializeTestOverrideConfigurations()
        {
            var enabledPos = GetTestConfiguredPosEnabled();
            
            var disabledPos = GetTestConfiguredPosDisabled();
            
            var faresource = GetTestConfiguredProviderSpace();

            var nonMatchingAirlines = GetTestNonMatchingMarketingAirlines();

            foreach (var marketingAirline in nonMatchingAirlines)
            {
                MockAirlineTicketingInfoProvider.AddOverridePosTicketingAirlineInfo(new PosTicketingAirlineInfo()
                {
                    PosId = enabledPos,
                    AirlineCode = marketingAirline,
                    FareSourceId = faresource
                });

                var airline = marketingAirline;

                var everyAirlineExceptCurrent =
                    nonMatchingAirlines.Where(x => x.Equals(airline, StringComparison.Ordinal));

                MockAirlineTicketingInfoProvider.AddOverrideAirTicketingAgreements(new PosInterAirlineAgreement()
                {
                    AirlineCode = marketingAirline,
                    FareSourceId = faresource,
                    PosId = enabledPos,
                    AddDate = DateTime.UtcNow.AddDays(-12),
                    AgreementType = "e", //should be case independent, this will test that, don't change.
                    Agreements = string.Join("|", everyAirlineExceptCurrent)
                });
            }

            foreach (var marketingAirline in GetTestAirlinesPOSEnabledButEmptyAgreements())
            {
                MockAirlineTicketingInfoProvider.AddOverridePosTicketingAirlineInfo(new PosTicketingAirlineInfo()
                {
                    PosId = enabledPos,
                    AirlineCode = marketingAirline,
                    FareSourceId = faresource
                });

                MockAirlineTicketingInfoProvider.AddOverrideAirTicketingAgreements(new PosInterAirlineAgreement()
                {
                    AirlineCode = marketingAirline,
                    FareSourceId = faresource,
                    PosId = enabledPos,
                    AddDate = DateTime.UtcNow.AddDays(-12),
                    AgreementType = "E",
                    Agreements = string.Empty
                });
            }

            foreach (var marketingAirline in GetTestAirlinesPOSEnabledButNoAgreements())
            {
                MockAirlineTicketingInfoProvider.AddOverridePosTicketingAirlineInfo(new PosTicketingAirlineInfo()
                {
                    PosId = enabledPos,
                    AirlineCode = marketingAirline,
                    FareSourceId = faresource
                });
            }
            
            MockAirlineTicketingInfoProvider.AddOverridePosTicketingAirlineInfo(new PosTicketingAirlineInfo()
            {
                PosId = disabledPos,
                AirlineCode = "kjsndjksndfkjns",
                FareSourceId = faresource
            });
        }

        //[ClassInitialize]
        //public static void ClassInit(TestContext context)
        //{
            
        //}

        //[ClassCleanup]
        //public void ClassCleanup()
        //{
            
        //}

        #endregion

        #region Tests

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithAllTicketable()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosEnabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(marketingAirlines, GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsTrue(isTicketable, "All ticketless airlines, ticketable should be true in this case.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithMixedTicketlessScenario()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosEnabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(marketingAirlines.Concat(GetTestNonMatchingMarketingAirlines()).ToListBuffered(), 
                GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsFalse(isTicketable, "Ticketless with Eticket airlines mixed recommendation scenario, ticketable should be false in this case.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithOnlyETicketingNotConfiguredTicketingAgreementOrPOS()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestNotConfiguredAccount(), GetTestNotConfiguredPos(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(GetTestNonMatchingMarketingAirlines(),
                GetTestNotConfiguredProviderSpace(), requester),
                requester);

            Assert.IsTrue(isTicketable, "Only E-Ticket carriers without specific configurations for account must be assumed to be ticketable.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithOnlyETicketingConfiguredPOSWithDisable()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosDisabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(GetTestNonMatchingMarketingAirlines(),
                GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsFalse(isTicketable, "Only E-Ticket carriers with carrier override on pos-faresource level must be respected.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithOnlyETicketingConfiguredPOSEnabledTicketingDisabled()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosEnabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(GetTestAirlinesPOSEnabledButEmptyAgreements(),
                GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsFalse(isTicketable, "Only E-Ticket carriers with ticketing agreement override on pos-faresource level must be respected.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithOnlyETicketingConfiguredPOSEnabledTicketingEnabled()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosEnabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(GetTestNonMatchingMarketingAirlines(),
                GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsTrue(isTicketable, "Only E-Ticket carriers with proper ticketing agreements and pos enabled should have passed the ticketing check.");
        }

        [TestMethod, TestCategory("Inter Airline Agreements")]
        public void IsRecommendationTicketableWithOnlyETicketingConfiguredPOSEnabledAgreementNotFound()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            var provider = GetInterlineAgreementProvider();

            var marketingAirlines = GetTestMatchingMarketingAirlines();

            MockAppSettingProvider.AddReplaceKeyedSetting(key, string.Join(",", marketingAirlines));

            var requester = new Requester(GetTestAccount(), GetTestConfiguredPosEnabled(), CustomerType.Agent);

            var isTicketable = provider.IsRecommendationTicketable(
                GetTestRecommendation(GetTestAirlinesPOSEnabledButNoAgreements(),
                GetTestConfiguredProviderSpace(), requester),
                requester);

            Assert.IsFalse(isTicketable, "Only E-Ticket carriers with absent ticketing agreements and pos enabled should have failed the ticketing check.");
        }

        #endregion

        #region Helpers

        private IAirlineInterlineAgreementProvider GetInterlineAgreementProvider()
        {
            return RuntimeContext.Resolver.Resolve<IAirlineInterlineAgreementProvider>();
        }


        private FlightRecommendation GetTestRecommendation(IList<string> marketingAirlines, int providerSpaceId, 
            Requester requester)
        {
            var recommendation = new FlightRecommendation()
                {
                    Legs = new FlightLegCollection(),
                    ProviderSpace = new ProviderSpace(providerSpaceId, "blah", requester, "Arzoo", null, null, 
                        ProviderSpaceType.All, null, new AdditionalInfoDictionary())
                };

            for (var i = 0; i < marketingAirlines.Count; i += 2)
            {
                var first = marketingAirlines[i];
                var second = i < marketingAirlines.Count - 1 ? marketingAirlines[i + 1] : first;

                recommendation.Legs.Add(new FlightLeg()
                    {
                        Segments = new FlightSegmentCollection()
                                {
                                    new FlightSegment("DEL", "BOM", GetDepartureTime(), GetArrivalTime(), "GA123", first, 0)
                                        {
                                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>(){new PaxTypeFareBasis(){ClassOfService = "S"}}
                                        },
                                    new FlightSegment("DEL", "BOM", GetArrivalTime().AddHours(5), GetArrivalTime().AddHours(7), "GA123", second, 1)
                                        {
                                            PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>(){new PaxTypeFareBasis(){ClassOfService = "S"}}
                                        }
                                }
                    });
            }

            return recommendation;
        }

        private static DateTime GetDepartureTime()
        {
            return DateTime.Now.AddDays(15);
        }

        private static DateTime GetArrivalTime()
        {
            return GetDepartureTime().AddHours(2);
        }

        private static int GetTestNotConfiguredProviderSpace()
        {
            return 92823918;
        }

        private static int GetTestConfiguredProviderSpace()
        {
            return 675213721;
        }

        private static IList<string> GetTestMatchingMarketingAirlines()
        {
            return new List<string>() { "2L", "2M" };
        }

        private static IList<string> GetTestAirlinesPOSEnabledButEmptyAgreements()
        {
            return new List<string>() { "4O" };
        }

        private static IList<string> GetTestAirlinesPOSEnabledButNoAgreements()
        {
            return new List<string>() { "5T" };
        }

        private static IList<string> GetTestNonMatchingMarketingAirlines()
        {
            return new List<string>() { "2P", "2Q" };
        }

        private static long GetTestAccount()
        {
            return StaticSettings.GetRootAccountId();
        }

        private static long GetTestNotConfiguredAccount()
        {
            return 1856;
        }
        private static int GetTestNotConfiguredPos()
        {
            return 1;
        } 

        private static int GetTestConfiguredPosEnabled()
        {
            return 1736437;
        }

        private static int GetTestConfiguredPosDisabled()
        {
            return 19182372;
        } 

        #endregion
    }
}
