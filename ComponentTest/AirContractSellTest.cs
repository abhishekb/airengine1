﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Common.Extensions;

namespace ComponentTest
{
    [TestClass]
    public class AirContractSaleTest : TestBase
    {
        //private const long MockedAccountId = 123;
        //private const long ProviderId = 123;

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), 1.ToString(), GetAppSetting("AccountId"), "USD", "", false);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestValidContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var status = contract.CanSellRecommendation(GetValidRecommendationForContract());

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidCabinTypeContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var recommendation = GetValidRecommendationForContract();
            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidCabinTypeSegment()};

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsFalse(status);

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetValidSegment()};

            status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidClassOfServiceContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var recommendation = GetValidRecommendationForContract();
            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidClassOfServiceSegment()};

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsFalse(status);

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetValidSegment()};

            status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidFareBasisCodeContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var recommendation = GetValidRecommendationForContract();
            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidFareBasisSegment()};

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsFalse(status);

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetValidSegment()};

            status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidMarketingAirlineCodeContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var recommendation = GetValidRecommendationForContract();
            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidMarketingAirlineSegment()};

            var status = contract.CanSellRecommendation(recommendation);
            
            Assert.IsFalse(status);

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetValidSegment()};

            status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestInvalidAirportCodeContractForSale()
        {
            var contract = ContractStore.GetValidSaleContract();

            var recommendation = GetValidRecommendationForContract();
            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidAirportSegment()};

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsFalse(status);

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetValidSegment()};

            status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestEmptyPeriodsContract()
        {
            var contract = ContractStore.GetValidSaleContract();

            contract.ValidPeriod = null;
            contract.TravelPeriod = null;
            contract.TicketingPeriod = null;


            var recommendation = GetValidRecommendationForContract();

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }

        [TestMethod, TestCategory("Air Contract")]
        public void TestAirlinePreferenceIncludeAll()
        {
            var contract = ContractStore.GetValidSaleContract();

            contract.AirlinePreferenceLevel = MockKeys.AirlinePreference.Include;
            contract.Airlines[0] = MockKeys.All;

            var recommendation = GetValidRecommendationForContract();

            recommendation.Legs[0].Segments = new FlightSegmentCollection() {GetInvalidMarketingAirlineSegment()};

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsTrue(status);
        }


        [TestMethod, TestCategory("Air Contract")]
        public void TestAirlinePreferenceExcludeAll()
        {
            var contract = ContractStore.GetValidSaleContract();

            contract.AirlinePreferenceLevel = MockKeys.AirlinePreference.Exclude;
            contract.Airlines[0] = MockKeys.All;

            var recommendation = GetValidRecommendationForContract();

            var status = contract.CanSellRecommendation(recommendation);

            Assert.IsFalse(status);
        }

        #region private methods

        private FlightRecommendation GetValidRecommendationForContract()
        {
            return new FlightRecommendation()
                       {
                           Legs =
                               new FlightLegCollection()
                                   {new FlightLeg() {Segments = new FlightSegmentCollection() {GetValidSegment()}}},
                            ProviderSpace = GetProviderSpace()
                       };
        }

        private ProviderSpace GetProviderSpace()
        {
            return new ProviderSpace(MockKeys.ValidValues.ValidProvider, "", new Requester(long.Parse(GetAppSetting("AccountId")), 1, CustomerType.Agent), "test", null, null,
                                     ProviderSpaceType.Domestic, new List<ProviderSpaceConfiguration>(),
                                     new List<KeyValuePair<string, string>>());
        }

        private string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static List<PaxTypeFareBasis> GetValidFareBasis()
        {
            return new List<PaxTypeFareBasis>()
                       {new PaxTypeFareBasis() {FareBasisCode = MockKeys.ValidValues.ValidFareBasis, CabinType = CabinType.Economy, ClassOfService = MockKeys.ValidValues.ValidClassOfService}};
        }

        private FlightSegment GetValidSegment()
        {
            return new FlightSegment(MockKeys.ValidValues.ValidAirport1, MockKeys.ValidValues.ValidAirport2,
                                     DateTime.UtcNow, DateTime.UtcNow.AddHours(1), "123",
                                     MockKeys.ValidValues.ValidAirline, 1)
                       {
                           PaxTypeFareBasisCodes = GetValidFareBasis()
                       };
        }

        private FlightSegment GetInvalidMarketingAirlineSegment()
        {
            return new FlightSegment(MockKeys.ValidValues.ValidAirport1, MockKeys.ValidValues.ValidAirport2,
                                     DateTime.UtcNow, DateTime.UtcNow.AddHours(1), "123",
                                     MockKeys.InvalidValues.InvalidAirline, 1)
                       {
                           PaxTypeFareBasisCodes =  GetValidFareBasis()
                       };
        }

        private FlightSegment GetInvalidAirportSegment()
        {
            return new FlightSegment(MockKeys.InvalidValues.InvalidAirport1, MockKeys.ValidValues.ValidAirport2,
                                     DateTime.UtcNow, DateTime.UtcNow.AddHours(1), "123",
                                     MockKeys.InvalidValues.InvalidAirline, 1)
                       {
                           PaxTypeFareBasisCodes = GetValidFareBasis()
                       };
        }

        private FlightSegment GetInvalidCabinTypeSegment()
        {
            var segment = GetValidSegment();

            var paxTypeFareBasis = segment.PaxTypeFareBasisCodes.First();
            if (paxTypeFareBasis != null)
                paxTypeFareBasis.CabinType = CabinType.First;

            return segment;
        }

        private FlightSegment GetInvalidClassOfServiceSegment()
        {
            var segment = GetValidSegment();

            var paxTypeFareBasis = segment.PaxTypeFareBasisCodes.First();
            if (paxTypeFareBasis != null)
                paxTypeFareBasis.ClassOfService = MockKeys.InvalidValues.InvalidClassOfService;

            return segment;
        }

        private FlightSegment GetInvalidFareBasisSegment()
        {
            var segment = GetValidSegment();
            segment.PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                {
                                                    new PaxTypeFareBasis()
                                                        {
                                                            FareBasisCode = MockKeys.InvalidValues.InvalidFareBasis,
                                                            CabinType = CabinType.Economy,
                                                            ClassOfService = MockKeys.ValidValues.ValidClassOfService
                                                        }
                                                };

            return segment;
        }

        #endregion
    }
}
