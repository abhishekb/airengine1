﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Parallel;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Common.TestHelpers.Mock;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using KeyStore = Tavisca.TravelNxt.Flight.Settings.KeyStore;
using SettingManager = Tavisca.TravelNxt.Flight.Settings.SettingManager;

namespace ComponentTest
{
    [TestClass]
    public class Settings : TestBase
    {
        #region Test Setup

        private MockAppSettingProvider _mProvider;

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), "INR", "password", true);
        }

        public override void TestInitializeAfterContextInject()
        {
            _mProvider = new MockAppSettingProvider();

            Assert.IsNotNull(SettingManager.AirConfigurationConnectionString);

            MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                new ShallowWrappedSettingProvider().WithSetInnerProvider(_mProvider));
        }

        public override void TestCleanupBeforeContextCleanup()
        {
            if (MockSettingsLoader.Instance != null)
                MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                    new FieldWrappedSettingsProvider().WithSetInnerProvider(new AppSettingProvider()));

            MockAppSettingProvider.ClearAdditionalSettings();
        }

        #endregion

        #region Tests

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerGeneralTest()
        {
            var helper = new ReflectionTestHelper();

            var managerType = typeof (SettingManager);

            helper.CallAllStaticMethods(managerType);
            helper.CallAllStaticProperties(managerType);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerGetClientEndpoints()
        {
            var setting = SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirConfigurationServiceAddr);

            Assert.IsNotNull(setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void KeyStoreScepterServiceConnectionSuffix()
        {
            var suffix = KeyStore.ClientEndpoints.AirFareSearchServiceUriSuffix;

            Assert.AreEqual("/proto", suffix); //if this is changed deliberately (along with config changes), change this test, this test ensures accidental incorrect check ins.
        }
        
        [TestMethod, TestCategory("Settings")]
        public void SettingManagerGetClientEndpointsNotExists()
        {
            var setting = SettingManager.GetClientEndpointAddress("blah");

            Assert.AreEqual(null, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerDefaultTimeWindowNull()
        {
            MockAppSettingProvider.AddReplaceKeyedSetting(KeyStore.AppSettings.DefaultTimeWindow, null);
            
            var setting = SettingManager.DefaultTimeWindow;

            Assert.AreEqual(2, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerDefaultTimeWindow0()
        {
            MockAppSettingProvider.AddReplaceKeyedSetting(KeyStore.AppSettings.DefaultTimeWindow, "0");

            var setting = SettingManager.DefaultTimeWindow;

            Assert.AreEqual(2, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerScepterCacheExpirationHours0()
        {
            const string key = KeyStore.AppSettings.ScepterCacheExpirationHours;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "0");

            var setting = SettingManager.ScepterCacheExpirationHours;

            Assert.AreEqual(1, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerProviderSpaceEmulationChainingLimit0()
        {
            const string key = KeyStore.AppSettings.ProviderSpaceEmulationChainingLimit;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "0");

            var setting = SettingManager.ProviderSpaceEmulationChainingLimit;

            Assert.AreEqual(5, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerProviderSpaceEmulationChainingLimitNull()
        {
            const string key = KeyStore.AppSettings.ProviderSpaceEmulationChainingLimit;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.ProviderSpaceEmulationChainingLimit;

            Assert.AreEqual(5, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerAvailSessionTimeOutNull()
        {
            const string key = KeyStore.AppSettings.AvailSessionTimeOut;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.AvailSessionTimeOut;

            Assert.AreEqual(60, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerAvailSessionTimeOut0()
        {
            const string key = KeyStore.AppSettings.AvailSessionTimeOut;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "ksndksnd");

            var setting = SettingManager.AvailSessionTimeOut;

            Assert.AreEqual(60, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerNearbyAirportRadius0()
        {
            const string key = KeyStore.AppSettings.NearbyAirportRadius;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "ksndksnd");

            var setting = SettingManager.NearbyAirportRadius;

            Assert.AreEqual(50, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerNearbyAirportRadiusNull()
        {
            const string key = KeyStore.AppSettings.NearbyAirportRadius;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.NearbyAirportRadius;

            Assert.AreEqual(50, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerMaxNearbyAirportRadiusNull()
        {
            const string key = KeyStore.AppSettings.MaxNearbyAirportRadius;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.MaxNearbyAirportRadius;

            Assert.AreEqual(200, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerMaxNearbyAirportRadius0()
        {
            const string key = KeyStore.AppSettings.MaxNearbyAirportRadius;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "klsndjksbdjf");

            var setting = SettingManager.MaxNearbyAirportRadius;

            Assert.AreEqual(200, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerSupplierThreadTimeout0()
        {
            const string key = KeyStore.AppSettings.SupplierThreadTimeoutInSeconds;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "dsfsdfsdf");

            var setting = SettingManager.SupplierThreadTimeoutInSeconds;

            Assert.AreEqual(60, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerSupplierThreadTimeoutNull()
        {
            const string key = KeyStore.AppSettings.SupplierThreadTimeoutInSeconds;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.SupplierThreadTimeoutInSeconds;

            Assert.AreEqual(60, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerDistanceUnitNull()
        {
            const string key = KeyStore.AppSettings.DistanceUnit;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.DistanceUnit;

            Assert.AreEqual(UnitOptions.Miles, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerDistanceUnitInvalid()
        {
            const string key = KeyStore.AppSettings.DistanceUnit;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "ksdksndnskdn");

            var setting = SettingManager.DistanceUnit;

            Assert.AreEqual(UnitOptions.Miles, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerAgencyHomeCountryCodeNull()
        {
            const string key = KeyStore.AppSettings.AgencyHomeCountryCode;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.AgencyHomeCountryCode;

            Assert.AreEqual("US", setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerSupplierSchedulerTypeNull()
        {
            const string key = KeyStore.AppSettings.SupplierSchedulerType;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.SupplierSchedulerType;

            Assert.AreEqual(SchedulerTypeOptions.IOScheduler, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerSupplierSchedulerTypeInvalid()
        {
            const string key = KeyStore.AppSettings.SupplierSchedulerType;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, "lksndfjksndjfn");

            var setting = SettingManager.SupplierSchedulerType;

            Assert.AreEqual(SchedulerTypeOptions.IOScheduler, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerTicketlessCarriersNull()
        {
            const string key = KeyStore.AppSettings.TicketlessCarriers;

            MockAppSettingProvider.AddReplaceKeyedSetting(key, null);

            var setting = SettingManager.TicketlessCarriers;

            Assert.AreEqual(string.Empty, setting);
        }

        [TestMethod, TestCategory("Settings")]
        public void SettingManagerGetClientEndpointsNegative()
        {
            var setting = SettingManager.GetClientEndpointAddress("jksdnfjsndf");

            Assert.IsNull(setting);
        }

        #endregion
    }
}
