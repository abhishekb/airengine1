﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace ComponentTest
{
    [TestClass]
    public class FilterStrategyTests : TestBase
    {
        #region Test Setup

        private static IFilterStrategy GetFilterStrategy()
        {
            return RuntimeContext.Resolver.Resolve<IFilterStrategy>();
        }

        private IList<FlightRecommendation> GetRecommendations(int count)
        {
            var recommendations = new List<FlightRecommendation>(count);

            for (int i = 0; i < count; i++)
            {
                recommendations.Add(new FlightRecommendation());
            }

            return recommendations;
        }

        #endregion

        #region Tests

        [TestMethod, TestCategory("Filter Strategy")]
        public void FilterMultipleNoPreferenceTest()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
                {
                    MaxPreferredResults = null
                };

            var recommendations = GetRecommendations(1000);

            recommendations = filterStrategy.Filter(recommendations, criterion);

            Assert.AreEqual(1000, recommendations.Count, "The filter strategy filtered out some results, this shouldn't have happened when we don't set the MaxPreferredResults property.");
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void FilterMultipleWithPreferenceTest()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10
            };

            var recommendations = GetRecommendations(1000);

            recommendations = filterStrategy.Filter(recommendations, criterion);

            Assert.AreEqual(10, recommendations.Count);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterWhenRecommendationNull()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10
            };

            var shouldFilter = filterStrategy.ShouldFilter(null, criterion);

            Assert.IsTrue(shouldFilter);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterWhenCriterionNull()
        {
            var filterStrategy = GetFilterStrategy();

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation(), null);

            Assert.IsTrue(shouldFilter);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterWhenTravelPrefIsNull()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation(), criterion);

            Assert.IsFalse(shouldFilter);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterMixedAirlinePositive()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                    {
                        AllowMixedAirlines = false
                    }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
                {
                    Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "AA", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
                }, criterion);

            Assert.IsTrue(shouldFilter, "Mixed airlines should have been indicated to be filtered, filtering should be on marketing airline.");
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterMixedAirlineNegative()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                {
                    AllowMixedAirlines = false
                }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            }, criterion);

            Assert.IsFalse(shouldFilter, "Mixed airlines should not have been indicated to be filtered, filtering should be on marketing airline.");
        }

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterCabinClassPositive()
        //{
        //    var filterStrategy = GetFilterStrategy();

        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            CabinClassesPreferred = new List<CabinType>()
        //                {
        //                    CabinType.Economy, CabinType.PremiumEconomy
        //                }
        //        }
        //    };

        //    var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
        //    {
        //        Legs = new FlightLegCollection(new List<FlightLeg>()
        //                {
        //                    new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.Economy},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.Economy}
        //                                }
        //                        },
        //                        new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.Economy},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.Economy}
        //                                }
        //                        }
        //                })
        //    }, criterion);

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterCabinClassNegative()
        //{
        //    var filterStrategy = GetFilterStrategy();

        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            CabinClassesPreferred = new List<CabinType>()
        //                {
        //                    CabinType.Economy, CabinType.PremiumEconomy
        //                }
        //        }
        //    };

        //    var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
        //    {
        //        Legs = new FlightLegCollection(new List<FlightLeg>()
        //                {
        //                    new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.Business},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.Business}
        //                                }
        //                        },
        //                        new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.Business},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.Business}
        //                                }
        //                        }
        //                })
        //    }, criterion);

        //    Assert.IsTrue(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterCabinClassMixedNegative()
        //{
        //    var filterStrategy = GetFilterStrategy();

        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            CabinClassesPreferred = new List<CabinType>()
        //                {
        //                    CabinType.Business
        //                }
        //        }
        //    };

        //    var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
        //    {
        //        Legs = new FlightLegCollection(new List<FlightLeg>()
        //                {
        //                    new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.Economy},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.PremiumEconomy}
        //                                }
        //                        },
        //                        new FlightLeg()
        //                        {
        //                            Segments = new FlightSegmentCollection()
        //                                {
        //                                    new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
        //                                        "9W-321", "9W", 0){CabinType = CabinType.First},
        //                                    new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
        //                                        "9W-322", "9W", 1){CabinType = CabinType.Business}
        //                                }
        //                        }
        //                })
        //    }, criterion);

        //    Assert.IsFalse(shouldFilter, "Filtering when cabin types apart from my preferred cabin types are present should not be done, there will be routes where 'First' might not be available and hence lower ones may be provided.");
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterExcludeAirlinesPositiveDefault()
        //{
        //    var criterion = new FlightSearchCriteria()
        //                        {
        //                            MaxPreferredResults = 10,
        //                            FlightTravelPreference = new FlightTravelPreference()
        //                                                         {
        //                                                             AllowMixedAirlines = true,
        //                                                             AirlinesPreference =
        //                                                                 new AirlinesPreference()
        //                                                                     {
        //                                                                         ProhibitedAirlines =
        //                                                                             new List<string>() { "9W" }
        //                                                                     }
        //                                                         }
        //                        };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "AA" });

        //    Assert.IsTrue(shouldFilter, "Exclude airlines by 'Default' (passed via data contract) should be a hard filter, the end customer (agency) might not want to sell these.");
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterExcludeAirlinesPositive()
        //{
        //    var criterion = new FlightSearchCriteria()
        //                        {
        //                            MaxPreferredResults = 10,
        //                            FlightTravelPreference = new FlightTravelPreference()
        //                                                         {
        //                                                             AllowMixedAirlines = true,
        //                                                             AirlinesPreference =
        //                                                                 new AirlinesPreference()
        //                                                                     {
        //                                                                         ProhibitedAirlines =
        //                                                                             new List<string>() {"9W"}
        //                                                                     }
        //                                                         }
        //                        };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "AA" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterExcludeAirlinesNegativeUnacceptable()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            ExcludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Unacceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "DL", "AA" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterExcludeAirlinesNegativeAcceptable()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            ExcludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Acceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "DL", "AA" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterExcludeAirlinesNegativeDefault()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            ExcludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Default
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "DL", "AA" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesPositiveDefault()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Default
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "DL" });

        //    Assert.IsFalse(shouldFilter, "Include airlines by default is a soft filter.");
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesPositiveAcceptable()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Acceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "DL" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesPositiveUnacceptable()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Unacceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesNegativeUnacceptableMixed()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Unacceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "DL" });

        //    Assert.IsTrue(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesNegativeAcceptableMixed()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Acceptable
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "DL" });

        //    Assert.IsFalse(shouldFilter);
        //}

        //[TestMethod, TestCategory("Filter Strategy")]
        //public void ShouldFilterIncludeAirlinesNegativeDefaultMixed()
        //{
        //    var criterion = new FlightSearchCriteria()
        //    {
        //        MaxPreferredResults = 10,
        //        FlightTravelPreference = new FlightTravelPreference()
        //        {
        //            AllowMixedAirlines = true,
        //            IncludeAirlines = new List<AirlinesPreference>()
        //                {
        //                    new AirlinesPreference()
        //                    {
        //                        AirlineCode = "9W", 
        //                        PreferenceType = PreferenceTypeOptions.Default
        //                    }
        //                }
        //        }
        //    };

        //    var shouldFilter = FilterViaAirlineCriterion(criterion, new List<string>() { "9W", "DL" });

        //    Assert.IsFalse(shouldFilter);
        //}

        private bool FilterViaAirlineCriterion(FlightSearchCriteria criterion, IList<string> airlineMix)
        {
            var filterStrategy = GetFilterStrategy();

            if (airlineMix == null || airlineMix.Count == 0)
                airlineMix = new List<string>() { "9W" };

            var airlines = new string[4];

            for (var i = 0; i < 4; i++)
            {
                var mixIndex = i;

                if (mixIndex >= airlineMix.Count)
                    mixIndex = (airlineMix.Count - 1) % mixIndex;

                airlines[i] = airlineMix[mixIndex];
            }

            return filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", airlines[0], 0){OperatingAirline = new Airline("AA")},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", airlines[1], 1)
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", airlines[2], 0){OperatingAirline = new Airline("AA")},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", airlines[3], 1)
                                        }
                                }
                        })
            }, criterion);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterJetAircraftPositive()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                {
                    AllowMixedAirlines = true,
                    JetFlightsOnly = true
                }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "AA", 1){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            }, criterion);

            Assert.IsFalse(shouldFilter);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterJetAircraftMixedNegative()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                {
                    AllowMixedAirlines = true,
                    JetFlightsOnly = true
                }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){AircraftType = "jet", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "jEt", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "AA", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            }, criterion);

            Assert.IsTrue(shouldFilter);
        }

        [TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterJetAircraftNegative()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                {
                    AllowMixedAirlines = true,
                    JetFlightsOnly = true
                }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "sdfsdf", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){AircraftType = "sdfsds", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "blah", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "AA", 1){AircraftType = "blu", PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            }, criterion);

            Assert.IsTrue(shouldFilter);
        }

        //[TestMethod, TestCategory("Filter Strategy")]
        public void ShouldFilterRefundablePositive()
        {
            var filterStrategy = GetFilterStrategy();

            var criterion = new FlightSearchCriteria()
            {
                MaxPreferredResults = 10,
                FlightTravelPreference = new FlightTravelPreference()
                {
                    AllowMixedAirlines = true,
                    Refundable = true
                }
            };

            var shouldFilter = filterStrategy.ShouldFilter(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "sdfsdf"},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){AircraftType = "sdfsds"}
                                        }
                                },
                                new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){AircraftType = "blah"},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "AA", 1){AircraftType = "blu"}
                                        }
                                }
                        })
            }, criterion);

            Assert.IsFalse(shouldFilter, "Refundable is a soft filter, should not affect output.");
        }

        #endregion

        private static readonly List<PaxTypeFareBasis> PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                                                      {
                                                                                          new PaxTypeFareBasis()
                                                                                              {ClassOfService = "S"}
                                                                                      };
    }
}
