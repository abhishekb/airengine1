using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Excel;
using Tavisca.TravelNxt.TestFramework.Validator;

namespace ComponentTest.FullSchedule
{
    #region SheetClasses

    public class AlternateAirportInformation
    {
        public string Sno { get; set; }
        public string AirportCodes { get; set; }
        public string IncludeNearByAirports { get; set; }
        public string RadiusKm { get; set; }
    }

    public class CabinType
    {
        public string Sno { get; set; }
        public string cabinType { get; set; }
    }

    public class ConnectionPreference
    {
        public string Sno { get; set; }
        public string AirportCode { get; set; }
        public IEnumerable<ConnectionPreferenceType> PreferenceTypes { get; set; }
    }

    public class Validator : IValidatorDataEntity
    {
        public string ValidatorId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FullyQualifiedName { get; set; }
    }

    public class ConnectionPreferenceType
    {
        public string Sno { get; set; }
        public string connectionPreferenceType { get; set; }
    }

    public class FlightSearchCriteria
    {
        public string Sno { get; set; }
        public IEnumerable<PaxTypeQuantity> PassengerInfoSummarys { get; set; }
        public IEnumerable<SearchSegment> SearchSegmentss { get; set; }
        public IEnumerable<SortingOrder> SortingOrders { get; set; }
        public IEnumerable<FlightTravelPreference> FlightTravelPreferences { get; set; }
        public string AdditionalInfo { get; set; }
        public string MaxPreferredResults { get; set; }
    }

    public class FlightSearchRQ
    {
        public string Sno { get; set; }
        public string DoDeferredSearch { get; set; }
        public string AllowSupplierStreaming { get; set; }
        public IEnumerable<FlightSearchCriteria> SearchCriterions { get; set; }
        public IEnumerable<Requester> Requesters { get; set; }
    }

    public class FlightTravelPreference
    {
        public string Sno { get; set; }
        public IEnumerable<CabinType> CabinClassesPreferreds { get; set; }
        public string JetFlightsOnly { get; set; }
        public string AllowMixedAirlines { get; set; }
        public string UnRestrictedFare { get; set; }
        public string Refundable { get; set; }
        public string ExcludeAirlines { get; set; }
        public string IncludeAirlines { get; set; }
    }

    public class PassengerType
    {
        public string Sno { get; set; }
        public string passengerType { get; set; }
    }

    public class PaxTypeQuantity
    {
        public string Sno { get; set; }
        public string Ages { get; set; }
        public IEnumerable<PassengerType> PassengerTypes { get; set; }
        public string Quantity { get; set; }
    }

    public class Requester
    {
        public string Sno { get; set; }
        public string AdditionalInfo { get; set; }
        public string RequesterOrigin { get; set; }
    }

    public class SearchSegment
    {
        public string Sno { get; set; }
        public string DepartureAirportCode { get; set; }
        public string ArrivalAirportCode { get; set; }
        public IEnumerable<TravelDate> TravelDates { get; set; }
        public string IncludeNonStopOnly { get; set; }
        public string IncludeDirectOnly { get; set; }
        public IEnumerable<CabinType> Cabins { get; set; }
        public IEnumerable<AlternateAirportInformation> ArrivalAlternateAirportInformations { get; set; }
        public IEnumerable<AlternateAirportInformation> DepartureAlternateAirportInformations { get; set; }
        public IEnumerable<ConnectionPreference> ConnectionPreferencess { get; set; }
        public string IncludeServiceClass { get; set; }
    }

    public class SortingOrder
    {
        public string Sno { get; set; }
        public string sortingOrder { get; set; }
    }

    public class TravelDate
    {
        public string Sno { get; set; }
        public string DateTime { get; set; }
        public string TimeWindow { get; set; }
        public string AnyTime { get; set; }
        public string MinDate { get; set; }
        public string MaxDate { get; set; }
    }
    #endregion
    public class FlightSearchRQData
    {
        #region Properties
        public IList<AlternateAirportInformation> AlternateAirportInformations { get; set; }
        public IList<CabinType> CabinTypes { get; set; }
        public IList<ConnectionPreference> ConnectionPreferences { get; set; }
        public IList<ConnectionPreferenceType> ConnectionPreferenceTypes { get; set; }
        public IList<FlightSearchCriteria> FlightSearchCriterias { get; set; }
        public IList<FlightSearchRQ> FlightSearchRQs { get; set; }
        public IList<FlightTravelPreference> FlightTravelPreferences { get; set; }
        public IList<PassengerType> PassengerTypes { get; set; }
        public IList<PaxTypeQuantity> PaxTypeQuantitys { get; set; }
        public IList<Requester> Requesters { get; set; }
        public IList<SearchSegment> SearchSegments { get; set; }
        public IList<SortingOrder> SortingOrders { get; set; }
        public IList<TravelDate> TravelDates { get; set; }
        public IList<Validator> Validators { get; set; }
        #endregion

        #region Constructors

        public FlightSearchRQData()
        {
            AlternateAirportInformations = new List<AlternateAirportInformation>();
            CabinTypes = new List<CabinType>();
            ConnectionPreferences = new List<ConnectionPreference>();
            ConnectionPreferenceTypes = new List<ConnectionPreferenceType>();
            FlightSearchCriterias = new List<FlightSearchCriteria>();
            FlightSearchRQs = new List<FlightSearchRQ>();
            FlightTravelPreferences = new List<FlightTravelPreference>();
            PassengerTypes = new List<PassengerType>();
            PaxTypeQuantitys = new List<PaxTypeQuantity>();
            Requesters = new List<Requester>();
            SearchSegments = new List<SearchSegment>();
            SortingOrders = new List<SortingOrder>();
            TravelDates = new List<TravelDate>();
            Validators = new List<Validator>();
        }

        #endregion

        #region Public Methods

        public static FlightSearchRQData Load(string excelFilePath)
        {
            var data = new FlightSearchRQData();

            if (string.IsNullOrEmpty(excelFilePath))
                throw new ArgumentNullException("excelFilePath");

            using (var ds = GetExcelSheetDataSet(excelFilePath))
            {
                DataTable table;

                table = ds.Tables["AlternateAirportInformation"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var alternateairportinformationobj = new AlternateAirportInformation();

                    alternateairportinformationobj.Sno = ParseColumnData(row["Sno"]);
                    alternateairportinformationobj.AirportCodes = ParseColumnData(row["AirportCodes"]);
                    alternateairportinformationobj.IncludeNearByAirports = ParseColumnData(row["IncludeNearByAirports"]);
                    alternateairportinformationobj.RadiusKm = ParseColumnData(row["RadiusKm"]);
                    data.AlternateAirportInformations.Add(alternateairportinformationobj);
                }
                table = ds.Tables["CabinType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var cabintypeobj = new CabinType();

                    cabintypeobj.Sno = ParseColumnData(row["Sno"]);
                    cabintypeobj.cabinType = ParseColumnData(row["CabinType"]);
                    data.CabinTypes.Add(cabintypeobj);
                }
                table = ds.Tables["ConnectionPreference"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var connectionpreferenceobj = new ConnectionPreference();

                    connectionpreferenceobj.Sno = ParseColumnData(row["Sno"]);
                    connectionpreferenceobj.AirportCode = ParseColumnData(row["AirportCode"]);
                    var cDataConnectionPreferenceType363622 = ParseColumnData(row["PreferenceType"]);

                    if (string.IsNullOrWhiteSpace(cDataConnectionPreferenceType363622))
                    {
                        connectionpreferenceobj.PreferenceTypes = Enumerable.Empty<ConnectionPreferenceType>();
                    }
                    else
                    {
                        var splitData = cDataConnectionPreferenceType363622.Split(',');
                        connectionpreferenceobj.PreferenceTypes = data.ConnectionPreferenceTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.ConnectionPreferences.Add(connectionpreferenceobj);
                }
                table = ds.Tables["ConnectionPreferenceType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var connectionpreferencetypeobj = new ConnectionPreferenceType();

                    connectionpreferencetypeobj.Sno = ParseColumnData(row["Sno"]);
                    connectionpreferencetypeobj.connectionPreferenceType = ParseColumnData(row["ConnectionPreferenceType"]);
                    data.ConnectionPreferenceTypes.Add(connectionpreferencetypeobj);
                }
                table = ds.Tables["FlightSearchCriteria"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightsearchcriteriaobj = new FlightSearchCriteria();

                    flightsearchcriteriaobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataPaxTypeQuantity755421 = ParseColumnData(row["PassengerInfoSummary"]);

                    if (string.IsNullOrWhiteSpace(cDataPaxTypeQuantity755421))
                    {
                        flightsearchcriteriaobj.PassengerInfoSummarys = Enumerable.Empty<PaxTypeQuantity>();
                    }
                    else
                    {
                        var splitData = cDataPaxTypeQuantity755421.Split(',');
                        flightsearchcriteriaobj.PassengerInfoSummarys = data.PaxTypeQuantitys.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataSearchSegment244620 = ParseColumnData(row["SearchSegments"]);

                    if (string.IsNullOrWhiteSpace(cDataSearchSegment244620))
                    {
                        flightsearchcriteriaobj.SearchSegmentss = Enumerable.Empty<SearchSegment>();
                    }
                    else
                    {
                        var splitData = cDataSearchSegment244620.Split(',');
                        flightsearchcriteriaobj.SearchSegmentss = data.SearchSegments.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataSortingOrder977091 = ParseColumnData(row["SortingOrder"]);

                    if (string.IsNullOrWhiteSpace(cDataSortingOrder977091))
                    {
                        flightsearchcriteriaobj.SortingOrders = Enumerable.Empty<SortingOrder>();
                    }
                    else
                    {
                        var splitData = cDataSortingOrder977091.Split(',');
                        flightsearchcriteriaobj.SortingOrders = data.SortingOrders.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataFlightTravelPreference642018 = ParseColumnData(row["FlightTravelPreference"]);

                    if (string.IsNullOrWhiteSpace(cDataFlightTravelPreference642018))
                    {
                        flightsearchcriteriaobj.FlightTravelPreferences = Enumerable.Empty<FlightTravelPreference>();
                    }
                    else
                    {
                        var splitData = cDataFlightTravelPreference642018.Split(',');
                        flightsearchcriteriaobj.FlightTravelPreferences = data.FlightTravelPreferences.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    flightsearchcriteriaobj.AdditionalInfo = ParseColumnData(row["AdditionalInfo"]);
                    flightsearchcriteriaobj.MaxPreferredResults = ParseColumnData(row["MaxPreferredResults"]);
                    data.FlightSearchCriterias.Add(flightsearchcriteriaobj);
                }
                table = ds.Tables["FlightSearchRQ"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightsearchrqobj = new FlightSearchRQ();

                    flightsearchrqobj.Sno = ParseColumnData(row["Sno"]);
                    flightsearchrqobj.DoDeferredSearch = ParseColumnData(row["DoDeferredSearch"]);
                    flightsearchrqobj.AllowSupplierStreaming = ParseColumnData(row["AllowSupplierStreaming"]);
                    var cDataFlightSearchCriteria751907 = ParseColumnData(row["SearchCriterion"]);

                    if (string.IsNullOrWhiteSpace(cDataFlightSearchCriteria751907))
                    {
                        flightsearchrqobj.SearchCriterions = Enumerable.Empty<FlightSearchCriteria>();
                    }
                    else
                    {
                        var splitData = cDataFlightSearchCriteria751907.Split(',');
                        flightsearchrqobj.SearchCriterions = data.FlightSearchCriterias.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataRequester70527 = ParseColumnData(row["Requester"]);

                    if (string.IsNullOrWhiteSpace(cDataRequester70527))
                    {
                        flightsearchrqobj.Requesters = Enumerable.Empty<Requester>();
                    }
                    else
                    {
                        var splitData = cDataRequester70527.Split(',');
                        flightsearchrqobj.Requesters = data.Requesters.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.FlightSearchRQs.Add(flightsearchrqobj);
                }
                table = ds.Tables["FlightTravelPreference"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flighttravelpreferenceobj = new FlightTravelPreference();

                    flighttravelpreferenceobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataCabinType573123 = ParseColumnData(row["CabinClassesPreferred"]);

                    if (string.IsNullOrWhiteSpace(cDataCabinType573123))
                    {
                        flighttravelpreferenceobj.CabinClassesPreferreds = Enumerable.Empty<CabinType>();
                    }
                    else
                    {
                        var splitData = cDataCabinType573123.Split(',');
                        flighttravelpreferenceobj.CabinClassesPreferreds = data.CabinTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    flighttravelpreferenceobj.JetFlightsOnly = ParseColumnData(row["JetFlightsOnly"]);
                    flighttravelpreferenceobj.AllowMixedAirlines = ParseColumnData(row["AllowMixedAirlines"]);
                    flighttravelpreferenceobj.UnRestrictedFare = ParseColumnData(row["UnRestrictedFare"]);
                    flighttravelpreferenceobj.Refundable = ParseColumnData(row["Refundable"]);
                    flighttravelpreferenceobj.ExcludeAirlines = ParseColumnData(row["ExcludeAirlines"]);
                    flighttravelpreferenceobj.IncludeAirlines = ParseColumnData(row["IncludeAirlines"]);
                    data.FlightTravelPreferences.Add(flighttravelpreferenceobj);
                }
                table = ds.Tables["PassengerType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var passengertypeobj = new PassengerType();

                    passengertypeobj.Sno = ParseColumnData(row["Sno"]);
                    passengertypeobj.passengerType = ParseColumnData(row["PassengerType"]);
                    data.PassengerTypes.Add(passengertypeobj);
                }
                table = ds.Tables["PaxTypeQuantity"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var paxtypequantityobj = new PaxTypeQuantity();

                    paxtypequantityobj.Sno = ParseColumnData(row["Sno"]);
                    paxtypequantityobj.Ages = ParseColumnData(row["Ages"]);
                    var cDataPassengerType830990 = ParseColumnData(row["PassengerType"]);

                    if (string.IsNullOrWhiteSpace(cDataPassengerType830990))
                    {
                        paxtypequantityobj.PassengerTypes = Enumerable.Empty<PassengerType>();
                    }
                    else
                    {
                        var splitData = cDataPassengerType830990.Split(',');
                        paxtypequantityobj.PassengerTypes = data.PassengerTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    paxtypequantityobj.Quantity = ParseColumnData(row["Quantity"]);
                    data.PaxTypeQuantitys.Add(paxtypequantityobj);
                }
                table = ds.Tables["Requester"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var requesterobj = new Requester();

                    requesterobj.Sno = ParseColumnData(row["Sno"]);
                    requesterobj.AdditionalInfo = ParseColumnData(row["AdditionalInfo"]);
                    requesterobj.RequesterOrigin = ParseColumnData(row["RequesterOrigin"]);
                    data.Requesters.Add(requesterobj);
                }
                table = ds.Tables["SearchSegment"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var searchsegmentobj = new SearchSegment();

                    searchsegmentobj.Sno = ParseColumnData(row["Sno"]);
                    searchsegmentobj.DepartureAirportCode = ParseColumnData(row["DepartureAirportCode"]);
                    searchsegmentobj.ArrivalAirportCode = ParseColumnData(row["ArrivalAirportCode"]);
                    var cDataTravelDate197484 = ParseColumnData(row["TravelDate"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDate197484))
                    {
                        searchsegmentobj.TravelDates = Enumerable.Empty<TravelDate>();
                    }
                    else
                    {
                        var splitData = cDataTravelDate197484.Split(',');
                        searchsegmentobj.TravelDates = data.TravelDates.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    searchsegmentobj.IncludeNonStopOnly = ParseColumnData(row["IncludeNonStopOnly"]);
                    searchsegmentobj.IncludeDirectOnly = ParseColumnData(row["IncludeDirectOnly"]);
                    var cDataCabinType96840 = ParseColumnData(row["Cabin"]);

                    if (string.IsNullOrWhiteSpace(cDataCabinType96840))
                    {
                        searchsegmentobj.Cabins = Enumerable.Empty<CabinType>();
                    }
                    else
                    {
                        var splitData = cDataCabinType96840.Split(',');
                        searchsegmentobj.Cabins = data.CabinTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataAlternateAirportInformation582496 = ParseColumnData(row["ArrivalAlternateAirportInformation"]);

                    if (string.IsNullOrWhiteSpace(cDataAlternateAirportInformation582496))
                    {
                        searchsegmentobj.ArrivalAlternateAirportInformations = Enumerable.Empty<AlternateAirportInformation>();
                    }
                    else
                    {
                        var splitData = cDataAlternateAirportInformation582496.Split(',');
                        searchsegmentobj.ArrivalAlternateAirportInformations = data.AlternateAirportInformations.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataAlternateAirportInformation317147 = ParseColumnData(row["DepartureAlternateAirportInformation"]);

                    if (string.IsNullOrWhiteSpace(cDataAlternateAirportInformation317147))
                    {
                        searchsegmentobj.DepartureAlternateAirportInformations = Enumerable.Empty<AlternateAirportInformation>();
                    }
                    else
                    {
                        var splitData = cDataAlternateAirportInformation317147.Split(',');
                        searchsegmentobj.DepartureAlternateAirportInformations = data.AlternateAirportInformations.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataConnectionPreference779783 = ParseColumnData(row["ConnectionPreferences"]);

                    if (string.IsNullOrWhiteSpace(cDataConnectionPreference779783))
                    {
                        searchsegmentobj.ConnectionPreferencess = Enumerable.Empty<ConnectionPreference>();
                    }
                    else
                    {
                        var splitData = cDataConnectionPreference779783.Split(',');
                        searchsegmentobj.ConnectionPreferencess = data.ConnectionPreferences.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    searchsegmentobj.IncludeServiceClass = ParseColumnData(row["IncludeServiceClass"]);
                    data.SearchSegments.Add(searchsegmentobj);
                }
                table = ds.Tables["SortingOrder"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var sortingorderobj = new SortingOrder();

                    sortingorderobj.Sno = ParseColumnData(row["Sno"]);
                    sortingorderobj.sortingOrder = ParseColumnData(row["SortingOrder"]);
                    data.SortingOrders.Add(sortingorderobj);
                }
                table = ds.Tables["TravelDate"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var traveldateobj = new TravelDate();

                    traveldateobj.Sno = ParseColumnData(row["Sno"]);
                    traveldateobj.DateTime = ParseColumnData(row["DateTime"]);
                    traveldateobj.TimeWindow = ParseColumnData(row["TimeWindow"]);
                    traveldateobj.AnyTime = ParseColumnData(row["AnyTime"]);
                    traveldateobj.MinDate = ParseColumnData(row["MinDate"]);
                    traveldateobj.MaxDate = ParseColumnData(row["MaxDate"]);
                    data.TravelDates.Add(traveldateobj);
                }

                table = ds.Tables["Validators"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var validatorobj = new Validator();

                    validatorobj.ValidatorId = ParseColumnData(row["ValidatorId"]);
                    validatorobj.Name = ParseColumnData(row["Name"]);
                    validatorobj.Description = ParseColumnData(row["Description"]);
                    validatorobj.FullyQualifiedName = ParseColumnData(row["FullyQualifiedName"]);
                    data.Validators.Add(validatorobj);
                }
            }
            return data;
        }

        #endregion


        #region Private Methods

        private static DataSet GetExcelSheetDataSet(string excelSheetPath)
        {
            var stream = File.Open(excelSheetPath, FileMode.Open, FileAccess.Read);

            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
            {
                excelReader.IsFirstRowAsColumnNames = true;

                var testSheet = excelReader.AsDataSet();

                return testSheet;
            }

        }

        private static string ParseColumnData(object data)
        {
            if (data == null)
                return null;

            return data.ToString();
        }

        #endregion
    }
}
