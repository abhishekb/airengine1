﻿using System.Collections.Generic;
using System.Linq;
using SchemaGenerator;

namespace ComponentTest.FullSchedule
{
    static class FlightSearchModelExcelBridge
    {

        public static Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ GetFlightSearchRQFromExcelDataModel(FlightSearchCriteria flightSearchCriteria)
        {
            var returnObject = new Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ()
                                   {
                                       AllowSupplierStreaming = false,
                                       DoDeferredSearch = false,
                                       Requester =
                                           new Tavisca.TravelNxt.Flight.Avail.DataContract.Requester(),
                                       SearchCriterion = GetSearchCriteria(flightSearchCriteria)
                                   };


            return returnObject;

        }

        private static bool GetParsedBool(string value)
        {
            return !string.IsNullOrEmpty(value) && value.ToLower().Equals("true");
        }

        private static Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchCriteria GetSearchCriteria(FlightSearchCriteria excelSearchCriteria)
        {
            return new Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchCriteria
                       {
                           FlightTravelPreference =
                               GetTravelPreference(excelSearchCriteria.FlightTravelPreferences.First()),
                           PassengerInfoSummary =
                               excelSearchCriteria.PassengerInfoSummarys.Select(GetPaxTypeQuantity).ToList(),
                           SearchSegments = excelSearchCriteria.SearchSegmentss.Select(GetSearchSegment).ToList()
                       };
        }

        private static Tavisca.TravelNxt.Flight.Avail.DataContract.FlightTravelPreference GetTravelPreference(FlightTravelPreference flightTravelPreference)
        {
            return new Tavisca.TravelNxt.Flight.Avail.DataContract.FlightTravelPreference()
            {
                AllowMixedAirlines =
                    GetParsedBool(flightTravelPreference.AllowMixedAirlines),
                
                JetFlightsOnly =
                    GetParsedBool(flightTravelPreference.JetFlightsOnly),
                Refundable = GetParsedBool(flightTravelPreference.Refundable),
                UnRestrictedFare =
                    GetParsedBool(flightTravelPreference.UnRestrictedFare)
            };
        }

        private static Tavisca.TravelNxt.Flight.Avail.DataContract.PaxTypeQuantity GetPaxTypeQuantity(PaxTypeQuantity paxTypeQuantity)
        {
            return new Tavisca.TravelNxt.Flight.Avail.DataContract.PaxTypeQuantity()
                       {
                           PassengerType =
                               ExcelAutoMapper.GetEnum<Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerType>(
                                   paxTypeQuantity.PassengerTypes.First().passengerType),
                           Quantity = int.Parse(paxTypeQuantity.Quantity),
                           Ages = paxTypeQuantity.Ages.Split(',').Select(int.Parse).ToList()
                       };
        }

        private static Tavisca.TravelNxt.Flight.Avail.DataContract.SearchSegment GetSearchSegment(SearchSegment searchSegment)
        {
            return new Tavisca.TravelNxt.Flight.Avail.DataContract.SearchSegment()
                       {
                           ArrivalAirportCode = searchSegment.ArrivalAirportCode,
                           DepartureAirportCode = searchSegment.DepartureAirportCode,
                           Cabin =
                               ExcelAutoMapper.GetEnum<Tavisca.TravelNxt.Flight.Avail.DataContract.CabinType>(
                                   searchSegment.Cabins.First().cabinType),
                           TravelDate = GetTravelDate(searchSegment.TravelDates.First()),
                           IncludeServiceClass = searchSegment.IncludeServiceClass.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList()
                       };
        }

        private static Tavisca.TravelNxt.Flight.Avail.DataContract.TravelDate GetTravelDate(TravelDate travelDate)
        {
            return new Tavisca.TravelNxt.Flight.Avail.DataContract.TravelDate()
                       {
                           DateTime = System.DateTime.Parse(travelDate.DateTime),
                           AnyTime = GetParsedBool(travelDate.AnyTime),
                       };
        }
    }
}
