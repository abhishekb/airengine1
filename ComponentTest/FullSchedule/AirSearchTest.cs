using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchemaGenerator;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;
using Tavisca.TravelNxt.Common.Extensions;

namespace ComponentTest.FullSchedule
{
    [TestClass]
    public class FullScheduleTest
    {
        public TestContext TestContext { get; set; }

        private static readonly Tavisca.TravelNxt.TestFramework.Validator.Validator Validator;

        private static readonly FlightSearchRQData SearchDataSet;

        static FullScheduleTest()
        {
            SearchDataSet = FlightSearchRQData.Load("FullSchedule/FlightSearchRQData.xlsx");

            Validator = new Tavisca.TravelNxt.TestFramework.Validator.Validator(SearchDataSet.Validators);
        }

        [TestMethod, TestCategory("High Level Cases"), DataSource("AirSearchData")]
        public void TestFullSchedule()
        {
            var dataRow = this.TestContext.DataRow;

            var isEnabled = dataRow["IsEnabled"].ToString() != "0";
            if (isEnabled == false)
                return;

            var searchCriterionId = dataRow["FlightSearchCriterion"].ToString();

            var airEngine = new AirEngine();
            var searchRequest =
                FlightSearchModelExcelBridge.GetFlightSearchRQFromExcelDataModel(
                    SearchDataSet.FlightSearchCriterias.First(x => x.Sno == searchCriterionId));

            FlightSearchRS response;
            
            using (new AmbientContextScope(GetCallContext(dataRow)))
            {
                response = airEngine.Search(searchRequest);
            }
            var validator = dataRow["Validators"].ToString();
            var acceptanceCriteria = this.TestContext.DataRow["AcceptanceCriteria"].ToString();
            var validatorList = new List<int>();
            if (!string.IsNullOrEmpty(validator))
                validatorList = validator.Split(',').ToList().Select(int.Parse).ToList();
            Validator.Validate(response, searchRequest, validatorList, acceptanceCriteria);
        }

        private static CallContext GetCallContext(DataRow dataRow)
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), dataRow["PosId"].ToString(),
                                   dataRow["Owner"].ToString(), "USD", string.Empty, true, "USD");
        }
    }
}