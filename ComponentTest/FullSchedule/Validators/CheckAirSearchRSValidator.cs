﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckAirSearchRsValidator : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            Assert.IsNotNull(response, "response is Null");

            Assert.IsNotNull(response.ServiceStatus, "service status is null");

            Assert.AreNotEqual(response.ServiceStatus.Status, CallStatus.Failure, "call status is failure");

            Assert.IsNotNull(response.SessionID, "Session Id is missing.");

            Assert.AreNotEqual(response.SessionID, Guid.Empty, "Session Id is empty.");

            Assert.IsFalse(response.ItineraryRecommendations == null && response.LegRecommendations == null, "Recommendations not found");

            if (response.ItineraryRecommendations != null && response.LegRecommendations != null)
                Assert.IsFalse(response.ItineraryRecommendations.Count == 0 && response.LegRecommendations.Count == 0, "Recommendations not found");

            if (response.ItineraryRecommendations == null && response.LegRecommendations != null)
                Assert.IsFalse(response.LegRecommendations.Count == 0, "LegRecommendations not found");

            if (response.ItineraryRecommendations != null && response.LegRecommendations == null)
                Assert.IsFalse(response.ItineraryRecommendations.Count == 0, "ItineraryRecommendations not available");
        }
    }
}