﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckItineraryForIncludedClassOfService : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            for(var i=0; i<request.SearchCriterion.SearchSegments.Count; i++)
            {
                var requestedClassOfService = request.SearchCriterion.SearchSegments[i].IncludeServiceClass;

                var legSegmentIds = response.LegAlternates[i].Legs.SelectMany(leg => leg.FlightSegmentRefs);
                var legSegments =
                    response.Segments.Where(
                        segment => legSegmentIds.Contains(segment.RefId)).ToList();
                
                var status =
                    legSegments.TrueForAll(legSegment => requestedClassOfService.Contains(legSegment.ClassOfService));

                Assert.IsTrue(status, "Invalid class of service found");
            }
        }
    }
}