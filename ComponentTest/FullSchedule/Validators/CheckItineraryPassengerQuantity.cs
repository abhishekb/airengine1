﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckItineraryPassengerQuantity : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            var allRecommendations = new List<FlightRecommendation>();
            if(response.LegRecommendations != null)
                allRecommendations.AddRange(response.LegRecommendations);

            if(response.ItineraryRecommendations != null)
                allRecommendations.AddRange(response.ItineraryRecommendations);
            
            if (allRecommendations.Count > 0)
                foreach (var recommendation in allRecommendations)
                {
                    var status = recommendation.Fare.PassengerFares.TrueForAll(
                        x =>
                        request.SearchCriterion.PassengerInfoSummary.Exists(
                            y => y.PassengerType == x.PassengerType && y.Quantity == x.Quantity
                            ));
                    Assert.IsTrue(status, "Passenger type mismatch found");
                }
        }
    }
}