﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckLegTypeForReturnTrips : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            Assert.IsTrue(response.LegAlternates != null && response.LegAlternates.Count == 2, "LegAlternates not available for either for onward or for return");

            response.LegAlternates[0].Legs.ForEach(
                flightLeg =>
                Assert.IsTrue(flightLeg.LegType == LegType.Onward, "Leg type incorrect for onward leg alternates"));

            response.LegAlternates[1].Legs.ForEach(
                flightLeg =>
                Assert.IsTrue(flightLeg.LegType == LegType.Return, "Leg type incorrect for return leg alternates"));
        }
    }
}
