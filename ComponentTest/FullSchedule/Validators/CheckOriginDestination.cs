﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;
using System.Linq;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckOriginDestination : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            var segments = response.Segments;
            for(var i=0; i<request.SearchCriterion.SearchSegments.Count; i++)
            {
                var requestedSegment = request.SearchCriterion.SearchSegments[i];
                var legs = response.LegAlternates.First(x => x.LegIndex == i).Legs;
                foreach (var leg in legs)
                {
                    var firstSegment = segments.First(x => x.RefId == leg.FlightSegmentRefs.First());
                    var lastSegment = segments.First(x => x.RefId == leg.FlightSegmentRefs.Last());

                    Assert.AreEqual(requestedSegment.DepartureAirportCode, firstSegment.DepartureAirportCode);
                    Assert.AreEqual(requestedSegment.ArrivalAirportCode, lastSegment.ArrivalAirportCode);
                }
            }
        }
    }
}