﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;
using System.Linq;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckItineraryForIncludeCityValidator : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            var segments = response.Segments;
            for (var i = 0; i < request.SearchCriterion.SearchSegments.Count; i++)
            {
                var includeLocations =
                    request.SearchCriterion.SearchSegments[i].ConnectionPreferences.Where(
                        x =>
                        x.PreferenceType == Tavisca.TravelNxt.Flight.Avail.DataContract.ConnectionPreferenceType.Via).
                        Select(x => x.AirportCode).
                        ToList();

                var legSegmentsIds = response.LegAlternates[i].Legs.SelectMany(x => x.FlightSegmentRefs).ToList();
                var legSegments = segments.Where(x => legSegmentsIds.Contains(x.RefId)).ToList();

                if (!includeLocations.Any() || legSegments.Count == 1)
                    continue;

                var legSegmentsApartFromLast = legSegments.Take(legSegments.Count - 1).ToList();

                foreach (var flightSegment in legSegmentsApartFromLast)
                    Assert.IsTrue(includeLocations.Contains(flightSegment.ArrivalAirportCode));
            }
        }
    }
}