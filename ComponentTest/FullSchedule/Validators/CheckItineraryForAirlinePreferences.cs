﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckItineraryForAirlinePreferences : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            var airlinesPreference = request.SearchCriterion.FlightTravelPreference.AirlinesPreference;

            if(airlinesPreference == null)
                return;

            bool status;
            if (airlinesPreference.PermittedAirlines != null && airlinesPreference.PermittedAirlines.Count > 0)
            {
                status =
                    response.Airlines.TrueForAll(airline => airlinesPreference.PermittedAirlines.Contains(airline.Code));
                Assert.IsTrue(status, "Airlines outside of permitted ones found in response");
            }

            if (airlinesPreference.ProhibitedAirlines != null && airlinesPreference.ProhibitedAirlines.Count > 0)
            {
                status =
                    response.Airlines.TrueForAll(airline => !airlinesPreference.PermittedAirlines.Contains(airline.Code));
                Assert.IsTrue(status, "Airlines from prohibited ones found in response");
            }
        }
    }
}