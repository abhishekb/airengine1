﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.TestFramework.Validator;
using FSRQ = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRQ;
using FSRS = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchRS;

namespace ComponentTest.FullSchedule.Validators
{
    public class CheckOnlyLegFareValidator : BaseValidator<FSRS, FSRQ>
    {
        public override void CheckAssert(FSRS response, FSRQ request)
        {
            Assert.IsTrue(response.ItineraryRecommendations == null || response.ItineraryRecommendations.Count == 0, "Itinerary Recommendations found but not expected");
            Assert.IsTrue(response.LegRecommendations != null && response.LegRecommendations.Count > 0, "Leg Recommendations not found");
        }
    }
}