﻿using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;
using CallStatus = Tavisca.TravelNxt.Flight.Entities.SeatMap.CallStatus;

namespace ComponentTest
{
    [TestClass]
    public class MockSeatMapTest : TestBase
    {
        internal static readonly CallContext CallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");

        public MockSeatMapTest()
        {
            TestUtility.SetCurrentCallType(MockKeys.CallTypes.SeatMap);
        }

        [TestMethod, TestCategory("MockSeatmap tests")]
        public void TestGetSeatmapForAmadeus()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Amadeus);
            var seatmapResponse = GetSeatmapResponse();
            AssertResponseCorrectness(seatmapResponse);
        }

        [TestMethod, TestCategory("MockSeatmap tests")]
        public void TestGetSeatmapForWorldspan()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Worldspan);
            var seatmapResponse = GetSeatmapResponse();
            AssertResponseCorrectness(seatmapResponse);
        }

        //[TestMethod, TestCategory("MockSeatmap tests")]
        public void TestGetSeatmapForTravelport()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.TravelPort);
            var seatmapResponse = GetSeatmapResponse();
            AssertResponseCorrectness(seatmapResponse);
        }

        [TestMethod, TestCategory("MockSeatmap tests")]
        public void TestGetSeatmapForSabre()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Sabre);
            var seatmapResponse = GetSeatmapResponse();
            AssertResponseCorrectness(seatmapResponse);
        }

        private static SeatMapRS GetSeatmapResponse()
        {
            var request =new SeatMapRQ();
            request.RecommendationRefID = 123;
            request.SegmentKey = "UA-NRT-838-1812140000-1812140000-Y";
            using (new AmbientContextScope(CallContext))
                return new MockSeatMapEngine().SeatMap(request);
        }

        private static void AssertResponseCorrectness(SeatMapRS seatMapRs)
        {
            Assert.IsNotNull(seatMapRs);
            Assert.IsNotNull(seatMapRs.ServiceStatus);
            Assert.AreNotEqual(seatMapRs.ServiceStatus.Status, CallStatus.Failure);
            Assert.IsNotNull(seatMapRs.SeatMap);
            Assert.IsNotNull(seatMapRs.SeatMap.Cabins);
            Assert.IsTrue(seatMapRs.SeatMap.Cabins.Count > 0);
        }
    }
}
