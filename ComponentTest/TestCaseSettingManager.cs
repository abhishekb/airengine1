﻿using System.Collections.Generic;
using System.Linq;

namespace ComponentTest
{
    internal class TestCaseSettingManager
    {
        private static List<string> _travelportSeatmapAirlines;
        public static List<string> TravelportSeatmapAirlines
        {
            get
            {
                return (_travelportSeatmapAirlines == null || !_travelportSeatmapAirlines.Any())
                           ? (_travelportSeatmapAirlines =
                              System.Configuration.ConfigurationManager.AppSettings["TravelportSeatmapAirlines"].Split(
                                  ',').ToList())
                           : _travelportSeatmapAirlines;
            }
        }

        private static string _amadeusPos;
        public static string AmadeusPos
        {
            get
            {
                return string.IsNullOrEmpty(_amadeusPos)
                           ? (_amadeusPos = System.Configuration.ConfigurationManager.AppSettings["AmadeusPos"])
                           : _amadeusPos;
            }
        }

        private static string _worldspanPos;
        public static string WorldspanPos
        {
            get
            {
                return string.IsNullOrEmpty(_worldspanPos)
                           ? (_worldspanPos = System.Configuration.ConfigurationManager.AppSettings["WorldspanPos"])
                           : _worldspanPos;
            }
        }

        private static string _sabrePos;
        public static string SabrePos
        {
            get
            {
                return string.IsNullOrEmpty(_sabrePos)
                           ? (_sabrePos = System.Configuration.ConfigurationManager.AppSettings["SabrePos"])
                           : _sabrePos;
            }
        }

        private static string _travelportPos;
        public static string TravelportPos
        {
            get
            {
                return string.IsNullOrEmpty(_travelportPos)
                           ? (_travelportPos = System.Configuration.ConfigurationManager.AppSettings["TravelportPos"])
                           : _travelportPos;
            }
        }

        private static string _accountId;
        public static string AccountId
        {
            get
            {
                return string.IsNullOrEmpty(_accountId)
                           ? (_accountId = System.Configuration.ConfigurationManager.AppSettings["AccountId"])
                           : _accountId;
            }
        }
    }
}
