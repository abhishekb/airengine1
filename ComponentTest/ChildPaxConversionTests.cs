﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.Singularity;
using Tavisca.Singularity.Interfaces;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using System.Linq;
using Tavisca.TravelNxt.Flight.Settings;

namespace ComponentTest
{
    [TestClass]
    public class ChildPaxConversionTests : TestBase
    {
        private static readonly ProviderSpace Provider;

        static ChildPaxConversionTests()
        {
            SettingManager.LogMinPriority.ToString();
            using (new AmbientContextScope(FareRulesTests.SabreCallContext))
                Provider = new ProviderSpace(101, "test", new Requester(long.Parse(TestCaseSettingManager.AccountId), 1, CustomerType.Agent), "test",
                                             null, null, ProviderSpaceType.All, null,
                                             new AdditionalInfoDictionary());
            Provider.AdditionalInfo[KeyStore.ProviderSpaceAttributes.MaxAllowedChildAge] = "11";
        }

        [TestMethod, TestCategory("Child pax conversion")]
        public void TestNoConversion()
        {
            using (new AmbientContextScope(FareRulesTests.SabreCallContext))
            {
                var searchCriteria = new FlightSearchCriteria()
                                         {
                                             PassengerInfoSummary =
                                                 new List<PaxTypeQuantity>()
                                                     {GetTwoAdultPaxtype(), GetTwoChildPaxtype()}
                                         };

                var transformManager = GetTransformManager();

                var transformedCriteria = transformManager.GetTransformedSearchCriteria(searchCriteria, Provider);

                CheckForEquality(searchCriteria.PassengerInfoSummary, transformedCriteria.PassengerInfoSummary,
                                 PassengerType.Adult, PassengerType.Child);
            }
        }

        [TestMethod, TestCategory("Child pax conversion")]
        public void TestHalfNeededTransition()
        {
            using (new AmbientContextScope(FareRulesTests.SabreCallContext))
            {
                var searchCriteria = new FlightSearchCriteria()
                                         {
                                             PassengerInfoSummary =
                                                 new List<PaxTypeQuantity>()
                                                     {GetTwoAdultPaxtype(), GetTwoChildPaxtype()}
                                         };
                var transformManager = GetTransformManager();
                var transformedCriteria = transformManager.GetTransformedSearchCriteria(searchCriteria, Provider);
                var child = transformedCriteria.PassengerInfoSummary.First(x => x.PassengerType == PassengerType.Child);
                var adult = transformedCriteria.PassengerInfoSummary.First(x => x.PassengerType == PassengerType.Adult);
                Assert.IsTrue(child.Quantity == 1);
                Assert.IsTrue(child.Ages.Count == 1);
                Assert.IsTrue(adult.Quantity == 3);
                Assert.IsTrue(adult.Ages.Count == 3);
            }
        }

        [TestMethod, TestCategory("Child pax conversion")]
        public void TestFullTransition()
        {
            using (new AmbientContextScope(FareRulesTests.SabreCallContext))
            {
                var searchCriteria = new FlightSearchCriteria()
                                         {
                                             PassengerInfoSummary =
                                                 new List<PaxTypeQuantity>()
                                                     {GetTwoAdultPaxtype(), GetTwoChildPaxtype(true)}
                                         };
                var transformManager = GetTransformManager();
                var transformedCriteria = transformManager.GetTransformedSearchCriteria(searchCriteria, Provider);
                var child =
                    transformedCriteria.PassengerInfoSummary.FirstOrDefault(x => x.PassengerType == PassengerType.Child);
                var adult = transformedCriteria.PassengerInfoSummary.First(x => x.PassengerType == PassengerType.Adult);
                Assert.IsTrue(child == null);
                Assert.IsTrue(adult.Quantity == 4);
                Assert.IsTrue(adult.Ages.Count == 4);
            }
        }

        #region private methods

        private static PaxTypeQuantity GetTwoAdultPaxtype()
        {
            return new PaxTypeQuantity()
                       {
                           PassengerType = PassengerType.Adult,
                           Quantity = 2,
                           Ages = new List<int>() {25, 35}
                       };
        }

        private static PaxTypeQuantity GetTwoChildPaxtype(bool allInvalidAges = false)
        {
            if (!allInvalidAges)
                return new PaxTypeQuantity()
                           {
                               PassengerType = PassengerType.Child,
                               Quantity = 2,
                               Ages = new List<int>() {10, 15}
                           };

            return new PaxTypeQuantity()
            {
                PassengerType = PassengerType.Child,
                Quantity = 2,
                Ages = new List<int>() { 16, 15 }
            };
        }

        private static ITransformPassengerInfo GetTransformManager()
        {
            return RuntimeContext.Resolver.Resolve<ITransformPassengerInfo>(KeyStore.SingularityNameKeys.ChildPaxTransform);
        }

        private static void CheckForEquality(IList<PaxTypeQuantity> original, IList<PaxTypeQuantity> transformed, params PassengerType[] passengerTypesToCheck)
        {
            foreach (var passengerType in passengerTypesToCheck)
            {
                var type1 = original.First(x => x.PassengerType == passengerType);
                var type2 = transformed.First(x => x.PassengerType == passengerType);

                Assert.IsTrue(type1.Quantity == type2.Quantity);
                Assert.IsTrue(type1.Ages.All(type2.Ages.Contains));
            }
        }

        #endregion
    }
}
