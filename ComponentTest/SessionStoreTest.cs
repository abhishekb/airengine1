﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace ComponentTest
{
    [TestClass]
    public class SessionStoreTest : TestBase
    {
        private static readonly CallContext CallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR", true);
        [TestMethod, TestCategory("Session Store Test")]
        public void TestSearchCriteriaInSessionStore()
        {
            using (new AmbientContextScope(CallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);
                System.Threading.Thread.Sleep(2000);
                var flightSearchCriteria = Utility.GetSessionProvider().Get<FlightSearchCriteria>("FlightSearchCriteria",
                                                                       CallContext.SessionId.ToString());
                Assert.IsNotNull(flightSearchCriteria);
                Assert.IsTrue(flightSearchCriteria.SearchSegments.Count > 0);
                Assert.AreEqual(flightSearchCriteria.SearchSegments[0].ArrivalAirportCode,request.SearchCriterion.SearchSegments[0].ArrivalAirportCode);
                Assert.AreEqual(flightSearchCriteria.SearchSegments[0].DepartureAirportCode, request.SearchCriterion.SearchSegments[0].DepartureAirportCode);
            }
        }
    }
}
