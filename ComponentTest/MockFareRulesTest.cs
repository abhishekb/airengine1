﻿using System;
using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Entities.FareRules;

namespace ComponentTest
{
    [TestClass]
    public class MockFareRulesTest
    {
        internal static readonly CallContext CallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");

        public MockFareRulesTest()
        {
            TestUtility.SetCurrentCallType(MockKeys.CallTypes.FareRule);
        }

        //[TestMethod, TestCategory("MockFareRules tests")]
        public void TestGetFareRulesForWorldspan()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Worldspan);
            var farerulesResponse = GetFareRulesResponse();
            AssertResponseCorrectness(farerulesResponse);
        }

        //[TestMethod, TestCategory("MockFareRules tests")]
        public void TestGetFareRulesForSabre()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Sabre);
            var farerulesResponse = GetFareRulesResponse();
            AssertResponseCorrectness(farerulesResponse);
        }

        //[TestMethod, TestCategory("MockFareRules tests")]
        public void TestGetFareRulesForAmadeusWs()
        {
            TestUtility.SetCurrentProviderName(MockKeys.ProviderNames.Amadeus);
            var farerulesResponse = GetFareRulesResponse();
            AssertResponseCorrectness(farerulesResponse);
        }

        private static FareRulesRS GetFareRulesResponse()
        {
            var request = new FareRulesRQ { AdditionalInfo = new AdditionalInfoDictionary() };
            using (new AmbientContextScope(CallContext))
                return new MockFareRulesEngine().GetFareRules(request);
        }

        private static void AssertResponseCorrectness(FareRulesRS fareRulesRs)
        {
            Assert.IsNotNull(fareRulesRs);
            Assert.IsNotNull(fareRulesRs.ServiceStatus);
            Assert.AreNotEqual(fareRulesRs.ServiceStatus.Status, CallStatus.Failure);
            Assert.IsNotNull(fareRulesRs.FareRules);
            Assert.IsTrue(fareRulesRs.FareRules.Count > 0);
        }
    }
}
