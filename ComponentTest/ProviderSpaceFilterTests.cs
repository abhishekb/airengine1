﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Common.TestHelpers.Mock;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using KeyStore = Tavisca.TravelNxt.Flight.Settings.KeyStore;

namespace ComponentTest
{
    [TestClass]
    public class ProviderSpaceFilterTests : TestBase
    {
        #region Test Setup

        private MockAppSettingProvider _mProvider;
        private const string DomesticCountryCode = "IN";
        private const string OtherCountryCode = "PK";

        public override void TestInitializeAfterContextInject()
        {
            SettingManager.GetKeyedSetting(""); //necessary to load the mock setting loader.
            _mProvider = new MockAppSettingProvider();

            MockAppSettingProvider.AddReplaceKeyedSetting(KeyStore.AppSettings.AgencyHomeCountryCode, DomesticCountryCode);

            MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                new ShallowWrappedSettingProvider().WithSetInnerProvider(_mProvider));
        }

        public override void TestCleanupBeforeContextCleanup()
        {
            if (MockSettingsLoader.Instance != null)
                MockSettingsLoader.Instance.TriggerProviderSettingRefresh(new SettingsRefreshedEventArgs(true),
                                                                          new FieldWrappedSettingsProvider().
                                                                              WithSetInnerProvider(
                                                                                  new AppSettingProvider()));

            MockAppSettingProvider.ClearAdditionalSettings();
        }

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1",
                StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture),
                "INR", "password", true);
        }

        private IProviderSpaceFilterProvider GetProviderSpaceFilterProvider()
        {
            return RuntimeContext.Resolver.Resolve<IProviderSpaceFilterProvider>();
        }

        private static IList<ProviderSpace> GetProviderSpaces(int domesticCount, int internationalCount, int allCount)
        {
            var count = domesticCount + internationalCount + allCount;

            var retVal = new List<ProviderSpace>(count);

            for (var i = 0; i < count; i++)
            {
                if ((i + 1) <= domesticCount)
                {
                    retVal.Add(
                        new ProviderSpace(i + 1, "test" + (i + 1), GetRequester(), "testFamily", null, null,
                                          ProviderSpaceType.Domestic, null, new AdditionalInfoDictionary())
                        );
                }
                else if ((i + 1 - domesticCount) <= internationalCount)
                {
                    retVal.Add(
                        new ProviderSpace(i + 1, "test" + (i + 1), GetRequester(), "testFamily", null, null,
                            ProviderSpaceType.International, null, new AdditionalInfoDictionary())
                        );
                }
                else
                {
                    retVal.Add(
                        new ProviderSpace(i + 1, "test" + (i + 1), GetRequester(), "testFamily", null, null,
                            ProviderSpaceType.All, null, new AdditionalInfoDictionary())
                        );
                }
            }

            return retVal;
        }

        private static void AddCountryPairRestriction(ProviderSpace providerSpace, string countryPairs)
        {
            providerSpace.AdditionalInfo.AddOrUpdate(KeyStore.ProviderSpaceAttributes.CountryPairRestriction, countryPairs);
        }

        private static Requester GetRequester()
        {
            return new Requester(StaticSettings.GetRootAccountId(), 1, CustomerType.Agent);
        }

        private FlightSearchCriteria GetCriterion(bool isDomestic, bool internationalByArrival,
            bool internationalByDeparture, bool internationalByAlternateArrival,
            bool internationalByAlternateDeparture, bool isMulticity = false)
        {
            var criterion = new FlightSearchCriteria()
                {
                    SearchSegments = new List<SearchSegment>()
                };

            if (isDomestic)
            {
                criterion.SearchSegments.Add(new SearchSegment()
                    {
                        ArrivalAirportCode = GetDomesticAirport1(),
                        DepartureAirportCode = GetDomesticAirport2()
                    });

                criterion.SearchSegments.Add(new SearchSegment()
                {
                    ArrivalAirportCode = isMulticity ? GetDomesticAirport3() : GetDomesticAirport2(),
                    DepartureAirportCode = GetDomesticAirport1()
                });

                return criterion;
            }

            criterion.SearchSegments.Add(new SearchSegment()
                {
                    ArrivalAirportCode = internationalByArrival ? GetInternationalAirport1() : GetDomesticAirport1(),
                    DepartureAirportCode = internationalByDeparture ? GetInternationalAirport2() : GetDomesticAirport2(),
                    ArrivalAlternateAirportInformation = new AlternateAirportInformation()
                        {
                            AirportCodes = new List<string>()
                                {
                                    (internationalByAlternateArrival ? GetInternationalAirport3() : GetDomesticAirport3())
                                }
                        },
                    DepartureAlternateAirportInformation = new AlternateAirportInformation()
                        {
                            AirportCodes = new List<string>()
                                    {
                                        (internationalByAlternateDeparture ? GetInternationalAirport4() : GetDomesticAirport4())
                                    }
                        }
                });

            return criterion;
        }

        private string GetDomesticAirport1()
        {
            return "BOM"; //bombay
        }

        private string GetDomesticAirport2()
        {
            return "DEL"; //delhi
        }

        private string GetDomesticAirport3()
        {
            return "BLR"; //bombay
        }

        private string GetDomesticAirport4()
        {
            return "CCU"; //calcutta
        }

        private const string InternationalByArrivalCountryCode = "GB";
        private string GetInternationalAirport1()
        {
            return "LHR"; //london heathrow
        }

        private string GetInternationalAirport2()
        {
            return "JFK"; //john f. kenndy, new york
        }

        private string GetInternationalAirport3()
        {
            return "EWR"; //newark, new york
        }

        private string GetInternationalAirport4()
        {
            return "EDI"; //scotland edinburgh airport
        }

        #endregion

        #region International Filteration Tests

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesNullProviderspaceInput()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var retVal = mgr.FilterProviderSpaces(null, new FlightSearchCriteria() { SearchSegments = new List<SearchSegment>() });

            Assert.IsNull(retVal, "expected null in case of null provider space input.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesEmptyProviderspaceInput()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var retVal = mgr.FilterProviderSpaces(new List<ProviderSpace>(), new FlightSearchCriteria() { SearchSegments = new List<SearchSegment>() });

            Assert.IsNotNull(retVal, "expected empty in case of empty provider space input.");
        }

        [TestMethod, TestCategory("Provider Space Filtering"), ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void FilterProviderSpacesNullCriteriaInput()
        {
            var mgr = GetProviderSpaceFilterProvider();

            mgr.FilterProviderSpaces(GetProviderSpaces(1, 0, 0), null);
        }

        [TestMethod, TestCategory("Provider Space Filtering"), ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void FilterProviderSpacesNullSearchSegmentsInput()
        {
            var mgr = GetProviderSpaceFilterProvider();

            mgr.FilterProviderSpaces(GetProviderSpaces(1, 0, 0), new FlightSearchCriteria());
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesDomesticSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1), GetCriterion(true, false, false, false, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.International), "All international specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByArrivalSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1),
                GetCriterion(false, true, false, false, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByDepartureSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1),
                GetCriterion(false, false, true, false, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByAlternateArrivalSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1),
                GetCriterion(false, false, false, true, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByAlternateDepartureSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1),
                GetCriterion(false, false, false, false, true));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByAllInternationalSearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 1, 1),
                GetCriterion(false, true, true, true, true));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(2, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByAllInternationalSearchWithoutCorrespondingProviderSpace()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 0, 1),
                GetCriterion(false, true, true, true, true));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(1, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All domestic specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesInternationalByAllInternationalSearchWithoutCorrespondingProviderSpaceNoAllType()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(1, 0, 0),
                GetCriterion(false, true, true, true, true));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(0, providerspaces.Count);
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesDomesticWithoutCorrespondingProviderSpace()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(0, 1, 1),
                GetCriterion(true, false, false, false, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(1, providerspaces.Count);
            Assert.IsTrue(providerspaces.All(x => x.Type != ProviderSpaceType.Domestic), "All international specific provider spaces should have been filtered out, there is an issue with the provider.");
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesDomesticWithoutCorrespondingProviderSpaceNoAllType()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = mgr.FilterProviderSpaces(GetProviderSpaces(0, 1, 0),
                GetCriterion(true, false, false, false, false));

            Assert.IsNotNull(providerspaces);
            Assert.AreEqual(0, providerspaces.Count);
        }

        #endregion

        #region Country Pair Restriction Tests

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesPositiveSingleCountryMatch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = GetProviderSpaces(0, 0, 2);

            AddCountryPairRestriction(providerspaces[0], DomesticCountryCode);

            var criterion = GetCriterion(true, false, false, false, false);

            var filtered = mgr.FilterProviderSpaces(providerspaces, criterion);

            Assert.IsNotNull(filtered, "The filter manager returned a null response");
            Assert.AreEqual(providerspaces.Count, filtered.Count);
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesPositiveSingleCountrySingleProviderSpaceMatch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = GetProviderSpaces(0, 0, 3);

            AddCountryPairRestriction(providerspaces[0], DomesticCountryCode);

            AddCountryPairRestriction(providerspaces[1], OtherCountryCode);

            AddCountryPairRestriction(providerspaces[2], DomesticCountryCode + "-" + OtherCountryCode);

            var criterion = GetCriterion(true, false, false, false, false);

            var filtered = mgr.FilterProviderSpaces(providerspaces, criterion);

            Assert.IsNotNull(filtered, "The filter manager returned a null response");
            Assert.AreEqual(1, filtered.Count);
            Assert.AreSame(providerspaces[0], filtered[0]);
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesPositiveDualCountryMatch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = GetProviderSpaces(0, 0, 8);

            AddCountryPairRestriction(providerspaces[0], DomesticCountryCode);

            AddCountryPairRestriction(providerspaces[1], OtherCountryCode);

            AddCountryPairRestriction(providerspaces[2], DomesticCountryCode + "-" + OtherCountryCode);
            //skipping 3
            AddCountryPairRestriction(providerspaces[4], DomesticCountryCode + "-" + InternationalByArrivalCountryCode + "," + OtherCountryCode + ",");

            AddCountryPairRestriction(providerspaces[5], DomesticCountryCode + "-*" + "," + OtherCountryCode);
            AddCountryPairRestriction(providerspaces[6], "*-*" + "," + OtherCountryCode);
            AddCountryPairRestriction(providerspaces[7], "*-" + InternationalByArrivalCountryCode + "," + OtherCountryCode + ",");

            var criterion = GetCriterion(false, true, false, false, false);

            var filtered = mgr.FilterProviderSpaces(providerspaces, criterion);

            Assert.IsNotNull(filtered, "The filter manager returned a null response");
            Assert.AreEqual(6, filtered.Count);
            Assert.AreSame(providerspaces[0], filtered[0]);
            Assert.AreSame(providerspaces[3], filtered[1]);
            Assert.AreSame(providerspaces[4], filtered[2]);
            Assert.AreSame(providerspaces[5], filtered[3]);
            Assert.AreSame(providerspaces[6], filtered[4]);
            Assert.AreSame(providerspaces[7], filtered[5]);
        }

        [TestMethod, TestCategory("Provider Space Filtering")]
        public void FilterProviderSpacesForMulticitySearch()
        {
            var mgr = GetProviderSpaceFilterProvider();

            var providerspaces = GetProviderSpaces(0, 0, 2);

            AddCountryPairRestriction(providerspaces[0], DomesticCountryCode + "-" + OtherCountryCode);

            AddCountryPairRestriction(providerspaces[1], DomesticCountryCode);

            var criterion = GetCriterion(true, false, false, false, false, true);

            var filtered = mgr.FilterProviderSpaces(providerspaces, criterion);

            Assert.IsNotNull(filtered, "The filter manager returned a null response");
            Assert.AreEqual(1, filtered.Count);
            Assert.AreSame(providerspaces[1], filtered[0]);
        }

        #endregion
    }
}
