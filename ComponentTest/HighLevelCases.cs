﻿using System;
using System.Collections.Generic;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using System.Threading.Tasks;
using System.Linq;
using AirlinesPreference = Tavisca.TravelNxt.Flight.Avail.DataContract.AirlinesPreference;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using FareComponent = Tavisca.TravelNxt.Flight.Entities.FareComponent;
using FlightTravelPreference = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightTravelPreference;

namespace ComponentTest
{
    [TestClass]
    public class HighLevelCases : TestBase
    {
        private static readonly CallContext WorldspanCallContext = new CallContext("en-us", Guid.NewGuid().ToString(),
            TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
            string.Empty, true, "INR", true);

        private static readonly CallContext NonMockAmadeusCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.AmadeusPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR", false);

        private static readonly CallContext TravelportCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.TravelportPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR", true);

        [TestMethod, TestCategory("High Level Cases")]
        public void TestConcurrentAccessToCachedFare()
        {
            var twoAdultResponses = new List<FlightAvailResult>();
            var singleAdultResponse = GetSingleAdultResponse();

            var tasks = new List<Task>();
            for (var i = 0; i < 20; i++)
                tasks.Add(new Task(() => twoAdultResponses.Add(GetTwoAdultResponse())));

            tasks.ForEach(task => task.Start());

            Task.WaitAll(tasks.ToArray());

            twoAdultResponses.ForEach(
                twoAdultResponse => ValidatePassengerBasedFare(singleAdultResponse, twoAdultResponse, 2));
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestCacheDataTransform()
        {
            var oneAdultResponse = GetSingleAdultResponse();

            var twoAdultResponse = GetTwoAdultResponse();

            ValidatePassengerBasedFare(oneAdultResponse, twoAdultResponse, 2);
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestCityBasedSearch()
        {
            using (new AmbientContextScope(NonMockAmadeusCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();

                var searchSegment = request.SearchCriterion.SearchSegments[0];
                searchSegment.TravelDate.AnyTime = true;
                searchSegment.Cabin = CabinType.Economy;
                searchSegment.ArrivalAirportCode = searchSegment.DepartureAirportCode = string.Empty;
                searchSegment.ArrivalAirportCode = "LAS";
                searchSegment.DepartureCityCode = "NYC";

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.AreNotEqual(availResult.ServiceStatus.Status, CallStatus.Failure);
                
                var recommendations = new List<FlightRecommendation>();
                if (availResult.LegRecommendations != null)
                    recommendations.AddRange(availResult.LegRecommendations);
                if (availResult.ItineraryRecommendations != null)
                    recommendations.AddRange(availResult.ItineraryRecommendations);

                Assert.IsTrue(recommendations.Count > 0);
            }
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestFlightAvail()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);
            }
        }

        //[TestMethod, TestCategory("High Level Cases")]
        public void TestFlightAvailWithRefundableFares()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                request.SearchCriterion.FlightTravelPreference = request.SearchCriterion.FlightTravelPreference ??
                                                                 new FlightTravelPreference() { Refundable = true };

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.AreNotEqual(availResult.ServiceStatus.Status, CallStatus.Failure);
                var recommendations = new List<FlightRecommendation>();
                if (availResult.LegRecommendations != null)
                    recommendations.AddRange(availResult.LegRecommendations);
                if (availResult.ItineraryRecommendations != null)
                    recommendations.AddRange(availResult.ItineraryRecommendations);

                recommendations.ForEach(
                    x => Assert.IsTrue(x.Fare.FareAttributes.Contains(FareAttribute.RefundableFares)));
            }
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestFlightAvailWithProhibitedAirlines()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                request.SearchCriterion.FlightTravelPreference = request.SearchCriterion.FlightTravelPreference ??
                                                                 new FlightTravelPreference()
                                                                     {
                                                                         AirlinesPreference =
                                                                             new AirlinesPreference()
                                                                                 {
                                                                                     ProhibitedAirlines =
                                                                                         new List<string>() { "DL" }
                                                                                 }
                                                                     };

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);

                Assert.IsTrue(availResult.Airlines.TrueForAll(x => !x.Code.Equals("DL")));
            }
        }


        //[TestMethod, TestCategory("High Level Cases")]
        public void TestFlightAvailWithPermittedBookingClass()
        {
            using (new AmbientContextScope(TravelportCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                var searchSegment = request.SearchCriterion.SearchSegments.First();
                searchSegment.DepartureAirportCode = "NRT";
                searchSegment.ArrivalAirportCode = "SFO";
                searchSegment.TravelDate.AnyTime = true;
                searchSegment.ArrivalAlternateAirportInformation.IncludeNearByAirports = false;
                request.SearchCriterion.SearchSegments.First().IncludeServiceClass = new List<string>() { "S" };

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);

                Assert.IsTrue(availResult.Segments.TrueForAll(segment => segment.ClassOfService.Equals("Y")));
            }
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestFlightAvailWithPermittedAirlines()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                request.SearchCriterion.FlightTravelPreference = request.SearchCriterion.FlightTravelPreference ??
                                                                 new FlightTravelPreference()
                                                                 {
                                                                     AirlinesPreference =
                                                                         new AirlinesPreference()
                                                                         {
                                                                             PermittedAirlines =
                                                                                 new List<string>() { "AA" }
                                                                         }
                                                                 };

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);

                Assert.IsTrue(availResult.Airlines.TrueForAll(x => x.Code.Equals("AA")));
            }
        }

        [TestMethod, TestCategory("High Level Cases")]
        public void TestAvailWithChildAgeCrossingThreshold()
        {
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                request.SearchCriterion.PassengerInfoSummary.Add(new PaxTypeQuantity()
                                                                     {
                                                                         Quantity = 1,
                                                                         Ages = new List<int>() { 16 },
                                                                         PassengerType = PassengerType.Child
                                                                     });

                var availResult = new AirEngine().Search(request);
                Assert.IsNotNull(availResult);
                Assert.IsNotNull(availResult.ServiceStatus);
                Assert.IsTrue(availResult.ServiceStatus.Status == CallStatus.Success);
                CheckForRecommendations(availResult);
                var recommendation = GetAnyRecommendation(availResult);

                Assert.IsTrue(recommendation.Fare.PassengerFares.Count == 1);
                Assert.IsTrue(recommendation.Fare.PassengerFares.First().PassengerType == PassengerType.Adult);
                Assert.IsTrue(recommendation.Fare.PassengerFares.First().Quantity == 2);
            }
        }

        #region private methods

        private static FlightRecommendation GetAnyRecommendation(FlightSearchRS searchRS)
        {
            var recommendations = new List<FlightRecommendation>();
            if (searchRS.ItineraryRecommendations != null)
                recommendations.AddRange(searchRS.ItineraryRecommendations);
            if (searchRS.LegRecommendations != null)
                recommendations.AddRange(searchRS.LegRecommendations);

            return recommendations.First();
        }

        private static void CheckForRecommendations(FlightSearchRS searchRS)
        {
            var recommendations = new List<FlightRecommendation>();
            if (searchRS.ItineraryRecommendations != null)
                recommendations.AddRange(searchRS.ItineraryRecommendations);
            if (searchRS.LegRecommendations != null)
                recommendations.AddRange(searchRS.LegRecommendations);

            Assert.IsTrue(recommendations.Count > 0);
        }

        private static FlightAvailResult GetTwoAdultResponse()
        {
            FlightAvailResult twoAdultResponse;
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetTwoAdultProviderSearchRequest();

                var flightProvider = new MockFlightProvider();

                twoAdultResponse = flightProvider.Avail(request);
            }

            return twoAdultResponse;
        }

        private static FlightAvailResult GetSingleAdultResponse()
        {
            FlightAvailResult singleAdultResponse;
            using (new AmbientContextScope(WorldspanCallContext))
            {
                var request = RequestFactory.GetOneAdultProviderSearchRequest();

                var flightProvider = new MockFlightProvider();

                singleAdultResponse = flightProvider.Avail(request);
            }

            return singleAdultResponse;
        }

        private static void ValidatePassengerBasedFare(FlightAvailResult singlePaxResponse, FlightAvailResult multiplePaxResponse, int multiplyFactor)
        {
            Assert.IsNotNull(singlePaxResponse);
            Assert.IsNotNull(multiplePaxResponse);
            Assert.IsNotNull(singlePaxResponse.FlightRecommendations);
            Assert.IsNotNull(multiplePaxResponse.FlightRecommendations);

            Assert.IsTrue(singlePaxResponse.FlightRecommendations.Count == multiplePaxResponse.FlightRecommendations.Count);

            for (var i = 0; i < singlePaxResponse.FlightRecommendations.Count; i++)
            {
                var singlePaxRecommendation = singlePaxResponse.FlightRecommendations[i];
                var multiplePaxRecommendation = multiplePaxResponse.FlightRecommendations[i];

                Assert.AreEqual(singlePaxRecommendation.Fare.BaseAmount.BaseAmount * multiplyFactor,
                                multiplePaxRecommendation.Fare.BaseAmount.BaseAmount);

                for (var j = 0; j < singlePaxRecommendation.Fare.PassengerFares.Count; j++)
                {
                    var oneAdultPassengerFare = singlePaxRecommendation.Fare.PassengerFares[j];
                    var twoAdultPassengerFare = multiplePaxRecommendation.Fare.PassengerFares[j];

                    Assert.AreEqual(oneAdultPassengerFare.BaseAmount.BaseAmount,
                                    twoAdultPassengerFare.BaseAmount.BaseAmount);

                    Assert.AreEqual(oneAdultPassengerFare.Quantity * multiplyFactor,
                                    twoAdultPassengerFare.Quantity);

                    CheckFareComponent(oneAdultPassengerFare.Discounts, twoAdultPassengerFare.Discounts);
                    CheckFareComponent(oneAdultPassengerFare.Fees, twoAdultPassengerFare.Fees);
                    CheckFareComponent(oneAdultPassengerFare.Taxes, twoAdultPassengerFare.Taxes);
                }
            }
        }

        private static void CheckFareComponent(IList<FareComponent> source, IList<FareComponent> target)
        {
            for (var i = 0; i < source.Count; i++)
            {
                var sourceComponent = source[i];
                var targetComponent = target[i];

                Assert.AreEqual(sourceComponent.Value.BaseAmount,
                                targetComponent.Value.BaseAmount);
            }
        }

        #endregion

    }
}
