﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Serialization.Configuration;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;

namespace ComponentTest
{
    [TestClass]
    public class ContentManagerTests : TestBase
    {
        #region Test Setup

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), "INR", "password", true);
        }

        private static IContentManager GetContentManager()
        {
            return RuntimeContext.Resolver.Resolve<IContentManagerFactory>().GetContentManager();
        }

        private static readonly List<PaxTypeFareBasis> PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                                                      {
                                                                                          new PaxTypeFareBasis()
                                                                                              {ClassOfService = "S"}
                                                                                      };

        #endregion

        #region Tests

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetAirportByCodeTestValid()
        {
            var mgr = GetContentManager();

            var airport = mgr.GetAirportByCode("DEL");

            AssertAirport(airport, "DEL", "IN");
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetAirportByCodeTestNegative()
        {
            var mgr = GetContentManager();

            var airport = mgr.GetAirportByCode("blah");

            Assert.IsNull(airport);
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetAirlineByCodeTestValid()
        {
            var mgr = GetContentManager();

            var airline = mgr.GetAirlineByCode("9W");

            AssertAirline(airline, "9W");
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetAirlineByCodeTestNegative()
        {
            var mgr = GetContentManager();

            var airline = mgr.GetAirlineByCode("blah");

            Assert.IsNull(airline);
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetNearbyAirportsTestNoAirportWithoutSelf()
        {
            var mgr = GetContentManager();

            var airports = mgr.GetNearbyAirports("DEL", false, 1);

            Assert.IsNotNull(airports);

            Assert.AreEqual(0, airports.Count());

            //twice to test caching logic
            airports = mgr.GetNearbyAirports("DEL", false, 1);

            Assert.IsNotNull(airports);

            Assert.AreEqual(0, airports.Count());
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetNearbyAirportsTestNoAirportWithSelf()
        {
            var mgr = GetContentManager();

            var airports = mgr.GetNearbyAirports("DEL", true, 1).ToList();

            Assert.IsNotNull(airports);

            Assert.AreEqual(1, airports.Count());

            Assert.AreEqual("DEL", airports.First().Code);

            //twice to test caching logic
            airports = mgr.GetNearbyAirports("DEL", true, 1).ToList();

            Assert.IsNotNull(airports);

            Assert.AreEqual(1, airports.Count());

            Assert.AreEqual("DEL", airports.First().Code);
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void GetNearbyAirportsTestKnownAirport()
        {
            var mgr = GetContentManager();

            var airports = mgr.GetNearbyAirports("BOM", false, 130).ToList();

            Assert.IsNotNull(airports);

            Assert.AreNotEqual(0, airports.Count());

            Assert.IsNotNull(airports.First(x => x.Code.Equals("PNQ", StringComparison.OrdinalIgnoreCase)), "Nearby airport test failed, PNQ airport was expected within 130km of BOM airport.");

            //twice to test caching logic
            airports = mgr.GetNearbyAirports("BOM", false, 130).ToList();

            Assert.IsNotNull(airports);

            Assert.AreNotEqual(0, airports.Count());

            Assert.IsNotNull(airports.First(x => x.Code.Equals("PNQ", StringComparison.OrdinalIgnoreCase)), "Nearby airport test failed, PNQ airport was expected within 130km of BOM airport.");
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void LoadFlightSegmentStaticContentTestAllValid()
        {
            var flightRecommendations = new List<FlightRecommendation>();

            flightRecommendations.Add(new FlightRecommendation()
                {
                    Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
                });

            var clonedRecommendation =
                new Tavisca.Frameworks.Serialization.SerializationFactory()
                .GetSerializationFacade(SerializerType.Binary)
                    .DeepClone(flightRecommendations);

            var mgr = GetContentManager();

            mgr.LoadFlightSegmentStaticContent(flightRecommendations);

            var testDataMap =
            new Dictionary<int, SegmentExpectedData>()
                {
                    {
                        0,
                        new SegmentExpectedData(new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airline("9W"),
                                                new Airline("AA"))
                    },
                    {1, new SegmentExpectedData(new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airline("9W"), null)}
                };

            AssertRecommendationListContent(flightRecommendations, testDataMap);

            mgr.LoadFlightSegmentStaticContent(clonedRecommendation); //for caching logic.

            AssertRecommendationListContent(clonedRecommendation, testDataMap);
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void LoadFlightSegmentStaticContentTestWithInValidAirports()
        {
            var flightRecommendations = new List<FlightRecommendation>();

            flightRecommendations.Add(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            });

            flightRecommendations.Add(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("blah", "blu", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("blu", "blah", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            });

            var clonedRecommendation =
                new Tavisca.Frameworks.Serialization.SerializationFactory()
                .GetSerializationFacade(SerializerType.Binary)
                    .DeepClone(flightRecommendations);

            var mgr = GetContentManager();

            mgr.LoadFlightSegmentStaticContent(flightRecommendations);

            var testDataMap =
            new Dictionary<int, SegmentExpectedData>()
                {
                    {
                        0,
                        new SegmentExpectedData(new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airline("9W"),
                                                new Airline("AA"))
                    },
                    {1, new SegmentExpectedData(new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airline("9W"), null)}
                };

            AssertRecommendationListContent(flightRecommendations, testDataMap);

            mgr.LoadFlightSegmentStaticContent(clonedRecommendation); //for caching logic.

            AssertRecommendationListContent(clonedRecommendation, testDataMap);
        }

        [TestMethod, TestCategory("Content Manager Tests")]
        public void LoadFlightSegmentStaticContentTestWithInValidAirlines()
        {
            var flightRecommendations = new List<FlightRecommendation>();

            flightRecommendations.Add(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("AA"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "9W", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            });

            flightRecommendations.Add(new FlightRecommendation()
            {
                Legs = new FlightLegCollection(new List<FlightLeg>()
                        {
                            new FlightLeg()
                                {
                                    Segments = new FlightSegmentCollection()
                                        {
                                            new FlightSegment("BOM", "DEL", DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(5).AddHours(2), 
                                                "9W-321", "9W", 0){OperatingAirline = new Airline("Blu"), PaxTypeFareBasisCodes = PaxTypeFareBasisCodes},
                                            new FlightSegment("DEL", "BOM", DateTime.UtcNow.AddDays(6), DateTime.UtcNow.AddDays(6).AddHours(2), 
                                                "9W-322", "Blah", 1){PaxTypeFareBasisCodes = PaxTypeFareBasisCodes}
                                        }
                                }
                        })
            });

            var clonedRecommendation =
                new Tavisca.Frameworks.Serialization.SerializationFactory()
                .GetSerializationFacade(SerializerType.Binary)
                    .DeepClone(flightRecommendations);

            var mgr = GetContentManager();

            mgr.LoadFlightSegmentStaticContent(flightRecommendations);

            var testDataMap =
            new Dictionary<int, SegmentExpectedData>()
                {
                    {
                        0,
                        new SegmentExpectedData(new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airline("9W"),
                                                new Airline("AA"))
                    },
                    {1, new SegmentExpectedData(new Airport("BOM"){City = new City(){CountryCode = "IN"}}, new Airport("DEL"){City = new City(){CountryCode = "IN"}}, new Airline("9W"), null)}
                };

            AssertRecommendationListContent(flightRecommendations, testDataMap);

            mgr.LoadFlightSegmentStaticContent(clonedRecommendation); //for caching logic.

            AssertRecommendationListContent(clonedRecommendation, testDataMap);
        }

        /// <summary>
        /// Expects one leg
        /// </summary>
        private void AssertRecommendationListContent(IEnumerable<FlightRecommendation> recommendations,
            IDictionary<int, SegmentExpectedData> segmentIndexDataMap)
        {
            foreach (var segment in recommendations.SelectMany(x => x.Legs).SelectMany(x => x.Segments))
            {
                var dataMap = segmentIndexDataMap[segment.SegmentIndex];

                AssertAirport(segment.ArrivalAirport, dataMap.ExpectedArrival.Code, dataMap.ExpectedArrival.City.CountryCode);
                AssertAirport(segment.DepartureAirport, dataMap.ExpectedDeparture.Code, dataMap.ExpectedDeparture.City.CountryCode);

                AssertAirline(segment.MarketingAirline, dataMap.ExpectedMarketingAirline.Code);
                AssertAirline(segment.OperatingAirline, dataMap.ExpectedOperatingAirline == null ? null : dataMap.ExpectedOperatingAirline.Code);
            }
        }

        private class SegmentExpectedData
        {
            public Airport ExpectedArrival { get; set; }
            public Airport ExpectedDeparture { get; set; }
            public Airline ExpectedMarketingAirline { get; set; }
            public Airline ExpectedOperatingAirline { get; set; }

            public SegmentExpectedData(Airport expectedArrival, Airport expectedDeparture,
                Airline expectedMarketingAirline, Airline expectedOperatingAirline)
            {
                ExpectedArrival = expectedArrival;
                ExpectedDeparture = expectedDeparture;
                ExpectedMarketingAirline = expectedMarketingAirline;
                ExpectedOperatingAirline = expectedOperatingAirline;
            }
        }

        private void AssertAirport(Airport airport, string expectedCode, string expectedCountry)
        {
            Assert.IsNotNull(airport);
            Assert.AreEqual(expectedCode, airport.Code);
            Assert.IsNotNull(airport.City);
            Assert.AreEqual(expectedCountry, airport.City.CountryCode);
            Assert.AreNotEqual(0d, airport.Latitude);
            Assert.AreNotEqual(0d, airport.Longitude);
            Assert.IsNotNull(airport.FullName);
            Assert.IsNotNull(airport.City.WindowsTimeZone, "Not defining the windows timezone for this airport will lead to errenous time calcualtions.");
        }

        private static void AssertAirline(Airline airline, string expectedCode)
        {
            if (airline == null && expectedCode == null)
                return;

            Assert.IsNotNull(airline);
            Assert.AreEqual(expectedCode, airline.Code);
            Assert.IsNotNull(airline.FullName);
            Assert.IsNotNull(airline.WebsiteURL);
        }

        #endregion
    }
}
