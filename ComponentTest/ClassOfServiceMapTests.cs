﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentTest.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Entities;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;
using KeyStore = Tavisca.TravelNxt.Flight.Settings.KeyStore;
using SettingManager = Tavisca.TravelNxt.Common.Settings.SettingManager;

namespace ComponentTest
{
    [TestClass]
    public class ClassOfServiceMapTests : TestBase
    {
        #region Test Setup

        public override Tavisca.Frameworks.Parallel.Ambience.AmbientContextBase GetContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), "INR", "password", true);
        }

        public override void TestInitializeAfterContextInject()
        {
            MockClassOfServiceMap.ClearAllOverrides();

            var cacheProvider =
                RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.AirCore);

            cacheProvider.Remove(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.ClassOfService);
            cacheProvider.Remove(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.AirlineZones);
        }

        public override void TestCleanupBeforeContextCleanup()
        {
            MockClassOfServiceMap.ClearAllOverrides();
        }

        #endregion

        #region Tests

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapExpectedCases()
        {
            const string testAirlineCode = "DL";

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            foreach (var classOfService in GetAllClassOfService())
            {
                var map = GetClassOfServiceMapper();

                var cabinType = map.GetClassOfService(segment, classOfService);

                Assert.AreNotEqual(CabinType.Unknown, cabinType);
            }
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapBadAirline()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            var type = segment.MarketingAirline.GetType();

            var property = type.GetProperty("Code");

            var setter = property.GetSetMethod(true);

            setter.Invoke(segment.MarketingAirline, new object[] {"DL123"});
            
            var cabinType = map.GetClassOfService(segment, "J");

            Assert.AreEqual(CabinType.Business, cabinType, "Class of service 'J' should have 'Business' as the default cabin type, if this fact has changed, change the test case.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapBadCountryCodes()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            segment.DepartureAirport.City.CountryCode = "23232";
            segment.ArrivalAirport.City.CountryCode = "232132";

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.Business, cabinType, "Class of service 'O' should have 'Business' for DL as the default cabin type, if this fact has changed, change the test case.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void CabinTypeParsingTests()
        {
            var cabinType = MockClassOfServiceMap.InvokeCabinTypeParseMethod("fIrst");

            Assert.AreEqual(CabinType.First, cabinType, "The string 'fIrst' was expected to be resolved into cabin type 'First'.");

            cabinType = MockClassOfServiceMap.InvokeCabinTypeParseMethod("ecoNomy");

            Assert.AreEqual(CabinType.Economy, cabinType, "The string 'ecoNomy' was expected to be resolved into cabin type 'Economy'.");

            cabinType = MockClassOfServiceMap.InvokeCabinTypeParseMethod("premiereconomY");

            Assert.AreEqual(CabinType.PremiumEconomy, cabinType, "The string 'premiereconomY' was expected to be resolved into cabin type 'PremiumEconomy'.");

            cabinType = MockClassOfServiceMap.InvokeCabinTypeParseMethod("busineSs");

            Assert.AreEqual(CabinType.Business, cabinType, "The string 'Business' was expected to be resolved into cabin type 'Business'.");
        }

        [TestMethod, TestCategory("Class of Service Mapping"), ExpectedException(typeof(InvalidDataException), AllowDerivedTypes = true)]
        public void CabinTypeParsingNullNegative()
        {
            MockClassOfServiceMap.InvokeCabinTypeParseMethod(null);
        }

        [TestMethod, TestCategory("Class of Service Mapping"), ExpectedException(typeof(InvalidDataException), AllowDerivedTypes = true)]
        public void CabinTypeParsingInvalidNegative()
        {
            MockClassOfServiceMap.InvokeCabinTypeParseMethod("blah");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnFirstSide()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
                {
                    AirlineCode = testAirlineCode,
                    CabinType = "first",
                    ClassOfService = "O",
                    ZoneName = "USZone"
                });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.First, cabinType, "Higher cabin type should have been given preference.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnSecondSide()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
                {
                    AirlineCode = testAirlineCode,
                    CountryCode = "GB",
                    ZoneName = "GBZone"
                });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.First, cabinType, "Higher cabin type should have been given preference.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnBothSides()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "GBZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.First, cabinType);
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnBothSidesConflicting()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "GBZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "business",
                ClassOfService = "O",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.First, cabinType, "Higher cabin type should have been given preference.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnBothSidesWithDomesticOnSecond()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "DOmestic"
            });

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "business",
                ClassOfService = "O",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "DOmestic"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.Business, cabinType, "In case of 'Domestic' zones, the other cabin type should be returned.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnBothSidesWithDomesticOnFirst()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "GBZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "domestic"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "business",
                ClassOfService = "O",
                ZoneName = "domestic"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "O",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.First, cabinType, "In case of 'Domestic' zones, the other cabin type should be returned.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnBothSideWithoutClassOfService()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "F",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "GBZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "F",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.Business, cabinType, "Class of service 'O' was expected to have default cabin type data set to 'Business' for DL, if this fact has changed, change the test case.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnFirstSideWithoutClassOfService()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "US",
                ZoneName = "USZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "F",
                ZoneName = "USZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.Business, cabinType, "Class of service 'O' was expected to have default cabin type data set to 'Business' for DL, if this fact has changed, change the test case.");
        }

        [TestMethod, TestCategory("Class of Service Mapping")]
        public void ClassOfServiceMapWithCountryMappingFoundOnSecondSideWithoutClassOfService()
        {
            const string testAirlineCode = "DL";

            var map = GetClassOfServiceMapper();

            var segment = GetTestFlightSegment("LAS", "LHR", testAirlineCode);

            MockClassOfServiceMap.AddOverrideAirlineCosZoneCountry(new AirlineCosZoneCountry()
            {
                AirlineCode = testAirlineCode,
                CountryCode = "GB",
                ZoneName = "GBZone"
            });

            MockClassOfServiceMap.AddOverrideAirlineClassesOfService(new AirlineClassesOfService()
            {
                AirlineCode = testAirlineCode,
                CabinType = "first",
                ClassOfService = "F",
                ZoneName = "GBZone"
            });

            var cabinType = map.GetClassOfService(segment, "O");

            Assert.AreEqual(CabinType.Business, cabinType, "Class of service 'O' was expected to have default cabin type data set to 'Business' for DL, if this fact has changed, change the test case.");
        }

        #endregion

        #region Helpers

        private static readonly string[] ClassOfServices = new[]
                {
                    "A","AN","B","BN","C","CI","CN","D","DN","E","F","FN","G","H","HN","I","J","K","KN","L",
                    "M","N","NC","NF","NS","O","P","Q","QN","R","S","T","U","V","W","X","Y","YN","Z"
                };

        private IEnumerable<string> GetAllClassOfService()
        {
            return ClassOfServices;
        }

        private FlightSegment GetTestFlightSegment(string originAirport, string destinationAirport, string airlineCode)
        {
            var segment = new FlightSegment(originAirport, destinationAirport, DateTime.Now.AddDays(15),
                DateTime.Now.AddDays(15).AddHours(2), "DL123", airlineCode, 0);
            segment.PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>(){new PaxTypeFareBasis(){CabinType = CabinType.Economy, ClassOfService = "S"}};

            RuntimeContext.Resolver.Resolve<IContentManagerFactory>().GetContentManager().LoadFlightSegmentStaticContent(new List<FlightRecommendation>()
               {
                   new FlightRecommendation()
                       {
                           Legs = new FlightLegCollection()
                               {
                                   new FlightLeg()
                                       {
                                           Segments = new FlightSegmentCollection()
                                               {
                                                   segment
                                               }
                                       }
                               }
                       }
               });

            return segment;
        }

        private static MockClassOfServiceMap GetClassOfServiceMapper()
        {
            return new MockClassOfServiceMap();
        }

        #endregion
    }
}
