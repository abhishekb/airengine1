﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;
using System.Linq;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareRules.DataContract;
using Tavisca.TravelNxt.Flight.FareRules.ServiceImpl;
using CabinType = Tavisca.TravelNxt.Flight.Entities.CabinType;
using CallStatus = Tavisca.TravelNxt.Flight.FareRules.DataContract.CallStatus;
using FareRulesRS = Tavisca.TravelNxt.Flight.FareRules.DataContract.FareRulesRS;
using FlightLeg = Tavisca.TravelNxt.Flight.Entities.FlightLeg;
using FlightSegment = Tavisca.TravelNxt.Flight.Entities.FlightSegment;
using PaxTypeFareBasis = Tavisca.TravelNxt.Flight.Entities.PaxTypeFareBasis;
using FlightRecommendation = Tavisca.TravelNxt.Flight.Entities.FlightRecommendation;
using Tavisca.TravelNxt.Common.Extensions;

namespace ComponentTest
{
    [TestClass]
    public class FareRulesTests :TestBase
    {
        [TestMethod, TestCategory("FareRules tests")]
        public void TestGetFareDetailsFromRecommendation()
        {
            var response = new Mock.MockFareRuleProvider().GetFareDetails(GetFlightRecommendation());

            Assert.IsNotNull(response);
            Assert.AreEqual(response.Count, 4);
            Assert.IsTrue(
                response.Where(x => new[] { "B", "C", "D" }.Contains(x.FareBasis)).All(y => y.FlightSegments.Count == 1));
            Assert.IsTrue(response.First(x => x.FareBasis.Equals("A")).FlightSegments.Count == 2);
        }

        //[TestMethod, TestCategory("FareRules tests")]
        //public void TestGetFareRules()
        //{
        //    var searchResponse = GetFlightSearchResponse(WorldspanCallContext, "LAS", "LAX");

        //    Thread.Sleep(3000);

        //    var fareRulesService = new AirFareRules();

        //    FareRulesRS fareRulesResponse = null;
        //    using (new AmbientContextScope(WorldspanCallContext))
        //    {
        //        fareRulesResponse = fareRulesService.GetFareRules(GetFareRulesRequest(searchResponse));
        //    }

        //    ValidateResponse(fareRulesResponse);
        //}

        //[TestMethod, TestCategory("FareRules tests")]
        //public void TestTravelportFareRules()
        //{
        //    var searchResponse = GetFlightSearchResponse(TravelportCallContext, "LHR", "BKK", false);

        //    Thread.Sleep(3000);

        //    var fareRulesService = new AirFareRules();

        //    FareRulesRS fareRulesResponse = null;
        //    using (new AmbientContextScope(TravelportCallContext))
        //    {
        //        fareRulesResponse = fareRulesService.GetFareRules(GetFareRulesRequest(searchResponse));
        //    }

        //    ValidateResponse(fareRulesResponse);
        //}

        internal static void ValidateResponse(FareRulesRS fareRulesResponse)
        {
            Assert.IsNotNull(fareRulesResponse);
            Assert.IsNotNull(fareRulesResponse.ServiceStatus);
            Assert.AreEqual(fareRulesResponse.ServiceStatus.Status, CallStatus.Success);
            Assert.IsTrue(fareRulesResponse.FareRules != null && fareRulesResponse.FareRules.Count > 0);
        }

        #region private methods

        private const string Empty = "empty";
        private static readonly DateTime Now = DateTime.Now;
        private static readonly List<string> FareBasis = new List<string>() { "A", "B", "C", "D" };


        private static FlightRecommendation GetFlightRecommendation()
        {
            var recommendation = new FlightRecommendation();
            recommendation.Legs = new FlightLegCollection
                                      {new FlightLeg() {LegGroupNumber = 1, Segments = new FlightSegmentCollection()}};
            recommendation.Legs[0].Segments.AddRange(GetSegments());
            return recommendation;
        }

        private static List<FlightSegment> GetSegments()
        {
            var segments = new List<FlightSegment>();

            segments.Add(new FlightSegment(Empty, Empty, Now.AddDays(1), Now, Empty, Empty, 0));
            segments.Add(new FlightSegment(Empty, Empty, Now.AddDays(2), Now, Empty, Empty, 0));

            segments[0].PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                    {
                                                        new PaxTypeFareBasis() {FareBasisCode = FareBasis[0], CabinType = CabinType.Economy, ClassOfService = "Economy"},
                                                        new PaxTypeFareBasis() {FareBasisCode = FareBasis[1], CabinType = CabinType.Economy, ClassOfService = "Economy"}
                                                    };

            segments[1].PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>()
                                                    {
                                                        new PaxTypeFareBasis() {FareBasisCode = FareBasis[2], CabinType = CabinType.Economy, ClassOfService = "Economy"},
                                                        new PaxTypeFareBasis() {FareBasisCode = FareBasis[3], CabinType = CabinType.Economy, ClassOfService = "Economy"},
                                                        new PaxTypeFareBasis() {FareBasisCode = FareBasis[0], CabinType = CabinType.Economy, ClassOfService = "Economy"}
                                                    };

            return segments;
        }

        internal static readonly CallContext SabreCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.SabrePos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");

        internal static readonly CallContext WorldspanCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");


        internal static readonly CallContext TravelportCallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.TravelportPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");

        public static FlightSearchRS GetFlightSearchResponse(CallContext callContext, string departure, string arrival, bool includeNearbyAirports = true)
        {
            using (new AmbientContextScope(callContext))
            {
                var request = RequestFactory.GetOneAdultServiceSearchRequest();
                var segment = request.SearchCriterion.SearchSegments.First();
                segment.ArrivalAlternateAirportInformation.IncludeNearByAirports = includeNearbyAirports;
                segment.ArrivalAirportCode = arrival;
                segment.DepartureAirportCode = departure;
                segment.TravelDate.DateTime = DateTime.Now.AddDays(30);

                var availResult = new AirEngine().Search(request);

                Assert.IsNotNull(availResult);
                Assert.IsTrue(availResult.LegRecommendations.Count > 0 || availResult.ItineraryRecommendations.Count > 0);

                return availResult;
            }
        }

        private static FareRulesRQ GetFareRulesRequest(FlightSearchRS searchResponse)
        {
            var recommendationRefId = GetRecommendationRefId(searchResponse);
            return new FareRulesRQ()
                       {
                           RecommendationRefID = recommendationRefId
                       };
        }

        public static int GetRecommendationRefId(FlightSearchRS searchResponse)
        {
            if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                return searchResponse.LegRecommendations.First().RefID;

            return searchResponse.ItineraryRecommendations.First().RefID;
        }

        #endregion
    }
}
