﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.TestHelpers;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.SeatMap.DataContract;
using Tavisca.TravelNxt.Flight.SeatMap.ServiceImpl;
using CallStatus = Tavisca.TravelNxt.Flight.SeatMap.DataContract.CallStatus;
using Tavisca.TravelNxt.Common.Extensions;

namespace ComponentTest
{
    [TestClass]
    public class SeatMapTests : TestBase
    {
        internal static readonly CallContext CallContext = new CallContext("en-us", Guid.NewGuid().ToString(), TestCaseSettingManager.WorldspanPos, TestCaseSettingManager.AccountId, "USD",
                                                                 string.Empty, true, "INR");

        //[TestMethod, TestCategory("Seatmap tests")]
        public void TestGetSeatmap()
        {
            var searchResponse = FareRulesTests.GetFlightSearchResponse(CallContext, "BKK", "LHR");
            Thread.Sleep(2000);
            var seatmapResponse = GetSeatmapResponse(searchResponse, CallContext);

            ValidateResponse(seatmapResponse);
        }

        //[TestMethod, TestCategory("Seatmap tests")]
        public void TestGetTravelportSeatmap()
        {
            var searchResponse = FareRulesTests.GetFlightSearchResponse(FareRulesTests.TravelportCallContext, "SFO", "HKG", false);

            Thread.Sleep(3000);

            var seatmapResponse = GetSeatmapResponse(searchResponse, FareRulesTests.TravelportCallContext, TestCaseSettingManager.TravelportSeatmapAirlines, 3);

            ValidateResponse(seatmapResponse);
        }

        internal static void ValidateResponse(SeatMapRS seatmapResponse)
        {
            Assert.IsNotNull(seatmapResponse);
            Assert.IsNotNull(seatmapResponse.ServiceStatus);
            Assert.AreEqual(seatmapResponse.ServiceStatus.Status, CallStatus.Success);
            Assert.IsNotNull(seatmapResponse.SeatMap);
            Assert.IsTrue(seatmapResponse.SeatMap.Cabins != null && seatmapResponse.SeatMap.Cabins.Count > 0);
        }

        #region private methods

        private static SeatMapRS GetSeatmapResponse(FlightSearchRS searchResponse, CallContext callContext, List<string> airlines = null , int flightNumberLength = 0)
        {
            var request = GetSeatmapRequest(searchResponse, airlines, flightNumberLength);
            using (new AmbientContextScope(callContext))
                return new AirSeatMap().GetSeatMap(request);
        }

        private static SeatMapRQ GetSeatmapRequest(FlightSearchRS searchResponse, List<string> airlines = null, int flightNumberLength = 0)
        {
            int segmentRef = -1;
            var recommendation = GetSingleRecommendation(searchResponse, ref segmentRef, airlines, flightNumberLength);
            var recommendationLegRef = recommendation.LegRefs.First().Split('/');
            var legAlternateIndex = int.Parse(recommendationLegRef[0]);
            var lefRefId = int.Parse(recommendationLegRef[1]);

            var leg = searchResponse.LegAlternates[legAlternateIndex].Legs.First(l => l.RefID == lefRefId);

            var segment =
                searchResponse.Segments.First(
                    x => (segmentRef == -1) ? x.RefId == leg.FlightSegmentRefs.First() : x.RefId == segmentRef);

            return new SeatMapRQ()
                       {
                           RecommendationRefId = recommendation.RefID,
                           SegmentKey = segment.Key
                       };
        }

        private static FlightRecommendation GetSingleRecommendation(FlightSearchRS searchResponse,ref int segmentRefId, List<string> airlines = null, int flightNumberLength = 0)
        {
            if (airlines == null)
            {
                if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                    return searchResponse.LegRecommendations.First();

                return searchResponse.ItineraryRecommendations.First();
            }

            var segment =
                searchResponse.Segments.First(
                    x =>
                    (airlines.Contains(x.MarketingAirlineCode)) &&
                    (flightNumberLength == 0 || x.FlightNumber.Length <= flightNumberLength)).
                    RefId;
            var leg =
                searchResponse.LegAlternates.SelectMany(x => x.Legs).First(x => x.FlightSegmentRefs.Contains(segment)).
                    RefID;
            var allRecommendations = new List<FlightRecommendation>();
            if(searchResponse.LegRecommendations != null)
                allRecommendations.AddRange(searchResponse.LegRecommendations);

            if (searchResponse.ItineraryRecommendations != null)
                allRecommendations.AddRange(searchResponse.ItineraryRecommendations);

            segmentRefId = segment;

            return allRecommendations.First(x => x.LegRefs.Any(y => int.Parse(y.Split('/')[1]) == leg));
        }

        #endregion
    }
}
