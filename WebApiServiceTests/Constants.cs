﻿namespace WebApiServiceTests
{
    internal static class Constants
    {
        public const string Scenario = "Scenario";

        public const string OldEnginesResponse = "OldEngines webapi response";
        public const string OskiResponse = "Oski webapi response";
    }
}
