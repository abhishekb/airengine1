﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApiServiceTests.Validators;
using WebApiServiceTests.WebApiService;
using System.Linq;

namespace WebApiServiceTests
{
    [TestClass]
    [DeploymentItem("RequestData.xlsx")]
    [DeploymentItem("SearchSegments.xlsx")]
    public class WebApiTests
    {
        private static readonly DataTable SearchSegmentData;

        private static readonly List<IValidator> Validators = new List<IValidator>();
        private static readonly IValidator BaseValidator;

        static WebApiTests()
        {
            var stream = File.Open("SearchSegments.xlsx", FileMode.Open, FileAccess.Read);

            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
            {
                excelReader.IsFirstRowAsColumnNames = true;

                SearchSegmentData = excelReader.AsDataSet().Tables[ExcelColumnNames.SearchSegments];
            }


            var validatorType = typeof(IValidator);
            //Get hold of all validators defined in current assembly
            var validatorTypes =
                Assembly.GetExecutingAssembly().GetTypes().Where(
                    x => !x.IsAbstract && validatorType.IsAssignableFrom(x)).ToList();
            validatorTypes.ForEach(type => Validators.Add((IValidator)Activator.CreateInstance(type)));
            BaseValidator = Validators.First(x => x.IsBaseValidator);
            Validators.Remove(BaseValidator);
        }

        public TestContext TestContext { get; set; }

        [TestMethod, TestCategory("Markups"),
        DataSource("System.Data.Odbc",
            "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)}; DBQ=|DataDirectory|\\RequestData.xlsx;readonly=true",
            "Scenarios$",
            DataAccessMethod.Sequential)]
        public void Test()
        {
            var scenario = TestContext.DataRow[ExcelColumnNames.Scenario].ToString();
            Console.WriteLine("Scenario : " + scenario);
            //Used to pick scenario name by validators
            Helper.SetScenario(scenario);

            var oldEnginesSearchRequest = SearchRequestBuilder.GetRequest(TestContext.DataRow, SearchSegmentData);
            var oskiSearchRequest = SearchRequestBuilder.GetRequest(TestContext.DataRow, SearchSegmentData, true);

            var oldEnginesResponse = GetClient().Search(oldEnginesSearchRequest);
            var oskiResponse = GetClient().Search(oskiSearchRequest);

            RunValidation(oldEnginesResponse, oskiResponse);
        }

        #region private methods

        private static void RunValidation(AirAvailRs oldEnginesResponse, AirAvailRs oskiResponse)
        {
            BaseValidator.Validate(oldEnginesResponse, oskiResponse);
            Validators.ForEach(validator => validator.Validate(oldEnginesResponse, oskiResponse));
        }

        private static AirServiceClient GetClient()
        {
            return new AirServiceClient();
        }

        #endregion
    }
}
