﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebApiServiceTests.WebApiService;

namespace WebApiServiceTests
{
    internal class SearchRequestBuilder
    {
        public static AirAvailRq GetRequest(DataRow dataRow, DataTable searchSegmentData, bool forOski = false)
        {
            var request = new AirAvailRq()
                              {
                                  Uid =
                                      dataRow[forOski ? ExcelColumnNames.OskiUid : ExcelColumnNames.OldEnginesUid].
                                      ToString(),
                                  Adults = int.Parse(dataRow[ExcelColumnNames.Adults].ToString()),
                                  Infants = int.Parse(dataRow[ExcelColumnNames.Infants].ToString()),
                                  Seniors = int.Parse(dataRow[ExcelColumnNames.Seniors].ToString()),
                                  Cabin =
                                      (CabinType)
                                      Enum.Parse(typeof (CabinType), dataRow[ExcelColumnNames.Cabin].ToString()),
                                  Culture = dataRow[ExcelColumnNames.Culture].ToString(),
                                  CurrencyCode = dataRow[ExcelColumnNames.CurrencyCode].ToString(),
                                  IpAddress = dataRow[ExcelColumnNames.IpAddress].ToString(),
                                  SearchWindow = int.Parse(dataRow[ExcelColumnNames.SearchWindow].ToString()),
                                  Children = int.Parse(dataRow[ExcelColumnNames.Children].ToString()),
                                  SearchSegments =
                                      GetSearchSegments(GetRequiredRows(searchSegmentData,
                                                                        dataRow[ExcelColumnNames.SearchSegments].
                                                                            ToString().Split(','))),
                                NumResults = 100
                              };

            request.TripType = GetTripType(request.SearchSegments.Length);

            return request;
        }

        private static TripType GetTripType(int searchSegmentCount)
        {
            if(searchSegmentCount == 1)
                return TripType.OneWay;
            if (searchSegmentCount == 2)
                return TripType.RoundTrip;
            return TripType.MultiStop;
        }

        private static IEnumerable<DataRow> GetRequiredRows(DataTable searchSegmentData ,ICollection<string> indices)
        {
            return searchSegmentData.Rows.Cast<DataRow>().Where(row => indices.Contains(row[ExcelColumnNames.Sno].ToString())).ToList();
        }

        private static SearchSegment[] GetSearchSegments(IEnumerable<DataRow> dataRows)
        {
            return dataRows.Select(dataRow => new SearchSegment()
                                                  {
                                                      OriginCode = dataRow[ExcelColumnNames.OriginCode].ToString(),
                                                      DestCode = dataRow[ExcelColumnNames.DestCode].ToString(),
                                                      DepartDate =
                                                          DateTime.Now.AddDays(
                                                              int.Parse(
                                                                  dataRow[ExcelColumnNames.DepartureDaysFromNow].
                                                                      ToString()))
                                                  }).ToArray();
        }
    }

    internal static class ExcelColumnNames
    {
        public const string SearchSegments = "SearchSegments";
        public const string Scenario = "Scenario";
        public const string OskiUid = "OskiUid";
        public const string OldEnginesUid = "OldEnginesUid";
        public const string Adults = "Adults";
        public const string Children = "Children";
        public const string Infants = "Infants";
        public const string Seniors = "Seniors";
        public const string Cabin = "Cabin";
        public const string SearchWindow = "SearchWindow";
        public const string IpAddress = "IpAddress";
        public const string Culture = "Culture";
        public const string CurrencyCode = "CurrencyCode";
        public const string DepartureDaysFromNow = "DepartureDaysFromNow";
        public const string OriginCode = "OriginCode";
        public const string DestCode = "DestCode";
        public const string Sno = "Sno";
    }
}
