﻿using System.Runtime.Remoting.Messaging;
namespace WebApiServiceTests
{
    internal static class Helper
    {
        public static string GetScenario()
        {
            return CallContext.GetData(Constants.Scenario).ToString();
        }

        public static void SetScenario(string scenario)
        {
            CallContext.SetData(Constants.Scenario, scenario);
        }
    }
}
