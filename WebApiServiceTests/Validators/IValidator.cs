﻿using WebApiServiceTests.WebApiService;
namespace WebApiServiceTests.Validators
{
    public interface IValidator
    {
        void Validate(AirAvailRs oldEnginesResponse, AirAvailRs oskiResponse);

        bool IsBaseValidator { get; }
    }
}
