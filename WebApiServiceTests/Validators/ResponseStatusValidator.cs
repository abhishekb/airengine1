﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace WebApiServiceTests.Validators
{
    class ResponseStatusValidator : IValidator
    {
        public void Validate(WebApiService.AirAvailRs oldEnginesResponse, WebApiService.AirAvailRs oskiResponse)
        {
            Assert.IsNotNull(oldEnginesResponse,
                             string.Format("{0} is null for scenario : {1}", Constants.OldEnginesResponse, Helper.GetScenario()));

            Assert.IsNotNull(oldEnginesResponse,
                             string.Format("{0} is null for scenario : {1}", Constants.OskiResponse, Helper.GetScenario()));

            Assert.IsTrue(oldEnginesResponse.Error == null, oldEnginesResponse.Error == null ? null :
                          string.Format("{0} failed for scenario : {1}, Error Message : {2}",
                                        Constants.OldEnginesResponse,
                                        Helper.GetScenario(), oldEnginesResponse.Error.ErrorMessage));

            Assert.IsTrue(oskiResponse.Error == null, oskiResponse.Error == null ? null :
                          string.Format("{0} failed for scenario : {1}, Error Message : {2}",
                                        Constants.OskiResponse,
                                        Helper.GetScenario(), oskiResponse.Error.ErrorMessage));

            Assert.IsTrue(oldEnginesResponse.AirOptions != null && oldEnginesResponse.AirOptions.Length > 0,
                             string.Format("{0} returned no options for scenario : {1}", Constants.OldEnginesResponse, Helper.GetScenario()));

            Assert.IsTrue(oldEnginesResponse.AirOptions != null && oldEnginesResponse.AirOptions.Length > 0,
                             string.Format("{0} returned no options for scenario : {1}", Constants.OskiResponse, Helper.GetScenario()));
        }


        public bool IsBaseValidator
        {
            get { return true; }
        }
    }
}
