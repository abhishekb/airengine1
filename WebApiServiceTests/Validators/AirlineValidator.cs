﻿using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace WebApiServiceTests.Validators
{
    public class AirlineValidator : IValidator
    {
        public void Validate(WebApiService.AirAvailRs oldEnginesResponse, WebApiService.AirAvailRs oskiResponse)
        {
            var oldEnginesAirlines = oldEnginesResponse.Segments.Select(x => x.AirlineCode).ToList();
            var oskiAirlines = oskiResponse.Segments.Select(x => x.AirlineCode).ToList();

            Assert.IsTrue(oldEnginesAirlines.Count == oskiAirlines.Count,
                          string.Format("{0} airline count : {1}-{2}...{3} airline count : {4}-{5}",
                                        Constants.OldEnginesResponse, oldEnginesAirlines.Count,
                                        GetAirlinesString(oldEnginesAirlines), Constants.OskiResponse,
                                        oskiAirlines.Count, GetAirlinesString(oskiAirlines)));
        }

        private static string GetAirlinesString(List<string> airlines)
        {
            var stringBuilder = new StringBuilder();
            airlines.ForEach(airline => stringBuilder.Append(airline + ","));
            return airlines.ToString();
        }


        public bool IsBaseValidator
        {
            get { return false; }
        }
    }
}
