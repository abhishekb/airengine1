﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WebApiServiceTests.Validators
{
    class PriceValidator : IValidator
    {
        public void Validate(WebApiService.AirAvailRs oldEnginesResponse, WebApiService.AirAvailRs oskiResponse)
        {
            var oskiMinFare = oskiResponse.AirOptions.Min(x => x.Fare.TotalFare);
            var oldEnginesMinFare = oldEnginesResponse.AirOptions.Min(x => x.Fare.TotalFare);

            Assert.AreEqual(oskiMinFare, oldEnginesMinFare,
                            string.Format("Oski Min fare : {0}...OldEngines min fare : {1}", oskiMinFare,
                                          oldEnginesMinFare));

            var oskiMaxFare = oskiResponse.AirOptions.Max(x => x.Fare.TotalFare);
            var oldEnginesMaxFare = oldEnginesResponse.AirOptions.Max(x => x.Fare.TotalFare);

            Assert.AreEqual(oskiMinFare, oldEnginesMinFare,
                            string.Format("Oski Max fare : {0}...OldEngines max fare : {1}", oskiMaxFare,
                                          oldEnginesMaxFare));
        }


        public bool IsBaseValidator
        {
            get { return false; }
        }
    }
}
