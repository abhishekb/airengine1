﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Tavisca.TravelNxt.Flight.Services
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "culture,displayCurrency,posId,accept,accountId,content-Type,SOAPAction,password,accept-Type,sessionId");
                HttpContext.Current.Response.End();
            }

        }
    }
}