﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Frameworks.Parallel;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.Frameworks.Parallel.Ambience.Wcf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Analytics;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Validation;
using Tavisca.TravelNxt.Engines.WebAPI.Core;
using Tavisca.TravelNxt.Engines.WebAPI.Core.OSKI;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions;
using Tavisca.TravelNxt.Engines.WebAPI.FaultContract;
using Tavisca.TravelNxt.Flight.Analytics;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Avail.DataContract.Converters;
using Tavisca.TravelNxt.Flight.Avail.ServiceContract;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.Settings;
using FlightSearchCriteria = Tavisca.TravelNxt.Flight.Avail.DataContract.FlightSearchCriteria;
using Requester = Tavisca.TravelNxt.Flight.Avail.DataContract.Requester;

namespace Tavisca.TravelNxt.Flight.Avail.ServiceImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    
    public class AirEngine : IAirEngine
    {
        static AirEngine()
        {
            Utility.SetApplicationName("Air Engine");
        }

        #region IAirEngine Methods
        [AmbientOperation(ApplicationName = "Air Search")]
        public FlightSearchRS Search(FlightSearchRQ request)
        {
            var stopwatch = new WrappedTimer();
            stopwatch.Start();
            var success = false;
            var resultCount = 0;

            var entry = Utility.GetEventEntryContextual();
            entry.Title = "{0} - Root Level Air Engine Log";
            entry.CallType = KeyStore.CallTypes.AirSearch;
            entry.RequestObject = request;
            entry.ReqResSerializerType = SerializerType.DataContractSerializer;

            FlightSearchRS response = null;
            try
            {
                Utility.LogDiagnosticEntry("Set Analytics Data For Request - Begin");
                SetAnalyticsDataForRequest(request, KeyStore.Analytics.Titles.FlightSearchRequest, GetFlightSearchRQConverter());
                Utility.LogDiagnosticEntry("Set Analytics Data For Request - End");

                Utility.LogDiagnosticEntry("Validate Request - Begin");
                var validationResult = GetValidationFactory().Validate(request);
                Utility.LogDiagnosticEntry("Validate Request - End");

                
                if (validationResult.IsValid)
                {
                    if (request.DoDeferredSearch)
                    {
                        Utility.LogDiagnosticEntry("DeferredSearch - Begin");
                        response = DeferredSearch(request, entry);
                        Utility.LogDiagnosticEntry("DeferredSearch - End");
                    }
                    else
                    {
                        Utility.LogDiagnosticEntry("SearchInternal - Begin");
                        response = SearchInternal(request, entry);
                        Utility.LogDiagnosticEntry("SearchInternal - End");
                    }
                }
                else
                {
                    entry.Title = string.Format(entry.Title, "Search");
                    entry.AddMessage("Validation Failed");
                    entry.StatusType = StatusOptions.Failure;
                    response = GetValidationFailedResponse(validationResult.Errors.Select(x => x.ErrorMessage).ToListBuffered());

                    validationResult.Errors.For(x =>
                                                    {
                                                        var key = "Validation." + x.PropertyName;
                                                        if (entry.AdditionalInfo.ContainsKey(key))
                                                            entry.AdditionalInfo[key] += "\n" + x.ErrorMessage;
                                                        else
                                                            entry.AdditionalInfo[key] = x.ErrorMessage;
                                                    });

                    entry.ResponseObject = response;
                    return response;
                }

                GetCountNSuccess(response, out success, out resultCount);

                Utility.LogDiagnosticEntry("Set Analytics Data For Search Response - Begin");
                SetAnalyticsDataForSearchResponse(response);
                Utility.LogDiagnosticEntry("Set Analytics Data For Search Response - End");
                return response;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(),
                                                   KeyStore.LogCategories.ExceptionCategories.Regular);

                response = GetGenericErrorResponse();
                entry.StatusType = StatusOptions.Failure;
                entry.ResponseObject = response;
                return response;
            }
            finally
            {
                stopwatch.Stop();
                entry.TimeTaken = stopwatch.ElapsedSeconds;


                Utility.LogDiagnosticEntry("Set Additional Data For Search Request & Response - Begin");
                AddAdditionalDataToLogEntry(entry, request, response);
                Utility.LogDiagnosticEntry("Set Additional Data For Search Request & Response - End");

                if (SettingManager.AirEngineSearchCallLoggingEnabled || entry.StatusType == StatusOptions.Failure)
                {
                    //SetRequestResponseObjectNull(entry);
                    Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
                }

                Utility.LogDiagnosticEntry("Set Analytics Data For Root - Begin");
                SetAnalyticsDataForRoot(TimeSpan.FromSeconds(stopwatch.ElapsedSeconds), KeyStore.Analytics.Titles.FlightSearchRoot, success,
                                        resultCount);
                Utility.LogDiagnosticEntry("Set Analytics Data For Root - End");
            }
        }

        private void AddAdditionalDataToLogEntry(IEventEntry entry, FlightSearchRQ request, FlightSearchRS response)
        {
            if(request == null)
                return;
            entry.AddAdditionalInfo(KeyStore.AdditionalData.SourceLocationCode, request.SearchCriterion.SearchSegments.First().DepartureAirportCode);
            entry.AddAdditionalInfo(KeyStore.AdditionalData.DestinationLocationCode, request.SearchCriterion.SearchSegments.Last().ArrivalAirportCode);
        }


        private void SetRequestResponseObjectNull(IEventEntry entry)
        {
            entry.RequestObject = null;
            entry.ResponseObject = null;
        }

        [AmbientOperation(ApplicationName = "Air Search")]
        public FlightSearchRS TryGetFromSession(FlightPollRQ request)
        {
            var stopwatch = new WrappedTimer();
            stopwatch.Start();
            var success = false;
            var resultCount = 0;

            var entry = Utility.GetEventEntryContextual();
            entry.Title = "{0} - Root Level Air Engine Log";
            entry.CallType = KeyStore.CallTypes.TryGetFromSession;
            entry.RequestObject = request;
            entry.ReqResSerializerType = SerializerType.DataContractSerializer;

            try
            {
                Utility.LogDiagnosticEntry("Set Analytics Data For Request - Begin");
                SetAnalyticsDataForRequest(request, KeyStore.Analytics.Titles.FlightGetFromSessionRoot, GetFlightPollRQConverter());
                Utility.LogDiagnosticEntry("Set Analytics Data For Request - End");

                FlightSearchRS response;
                if (request == null || !request.GetAsSupplierResponds)
                {
                    entry.Title = string.Format(entry.Title, "TryGetFromSessionComplete");
                    Utility.LogDiagnosticEntry("Try Get From Session Complete - Begin");
                    response = TryGetFromSessionComplete();
                    Utility.LogDiagnosticEntry("Try Get From Session Complete - End");
                }
                else
                {
                    entry.Title = string.Format(entry.Title, "TryGetFromSessionAsAvailable");
                    Utility.LogDiagnosticEntry("Try Get From Session As Available - Begin");
                    response = TryGetFromSessionAsAvailable(request.PreviousTimeStamp);
                    Utility.LogDiagnosticEntry("Try Get From Session As Available - End");
                }

                GetCountNSuccess(response, out success, out resultCount);

                if (response != null)
                    entry.ResponseObject = response;

                if (response != null && response.ServiceStatus != null && response.ServiceStatus.Status == CallStatus.Failure)
                    entry.StatusType = StatusOptions.Failure;

                Utility.LogDiagnosticEntry("Set Analytics Data For Search Response - Begin");
                SetAnalyticsDataForSearchResponse(response);
                Utility.LogDiagnosticEntry("Set Analytics Data For Search Response - End");

                return response;
            }
            catch (Exception ex)
            {
                entry.StatusType = StatusOptions.Failure;

                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                throw FaultException.CreateFault(MessageFault.CreateFault(new FaultCode("101"), new FaultReason(ex.ToString()))); //TODO: make proper
            }
            finally
            {
                stopwatch.Stop();
                entry.TimeTaken = stopwatch.ElapsedSeconds;
                Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);

                Utility.LogDiagnosticEntry("Set Analytics Data For Root - Begin");
                SetAnalyticsDataForRoot(TimeSpan.FromSeconds(stopwatch.ElapsedSeconds), "Flight_TryGetFromSession_Root", success, resultCount);
                Utility.LogDiagnosticEntry("Set Analytics Data For Root - End");
            }
        }

        public AirAvailRs Search(string uid, string cul, string cid, string curr, string ip, int numRes, int adt, int chld, int inf, int senr,
            string frm1, bool isfrm1city, string to1, bool isto1city, string frm2, bool isfrm2city, string to2, bool isto2city, string frm3,
            bool isfrm3city, string to3, bool isto3city, string frm4, bool isfrm4city, string to4, bool isto4city, string frm5, bool isfrm5city, string to5, bool isto5city,
            string date1, string date2, string date3, string date4, string date5, string trip, bool nonstop, string cabin, int window, bool providebaggageinfo)
        {
            AirAvailRq airAvailRequest = null;           
            AirAvailRs airAvailRs = null;
          
            var stopwatch = new WrappedTimer();
            stopwatch.Start();
            var entry = Utility.GetEventEntryContextual();
            entry.Title = "{0} - Root Level WebAPI Log";
            entry.CallType = KeyStore.CallTypes.WebAPI_AirSearch;


            try
            {
                ClientConfiguration configurationManager = new ClientConfiguration();
              
                configurationManager.ResolveCulture(cul, curr);

                Utility.LogDiagnosticEntry("WebAPI_LoadConfiguration - Begin");
                configurationManager.LoadConfiguration();
                Utility.LogDiagnosticEntry("WebAPI_LoadConfiguration - End");


                //var callContext = new Tavisca.TravelNxt.Common.Extensions.CallContext(configurationManager.Culture,
                //    configurationManager.CID, configurationManager.PosId, configurationManager.DK, configurationManager.CurrencyCode, "password", true, "", false);
                using (new AmbientContextScope(new Tavisca.TravelNxt.Common.Extensions.CallContext(configurationManager.Culture,configurationManager.CID,
                                                configurationManager.PosId, configurationManager.DK, configurationManager.CurrencyCode, "password", true, false)))
                {
                    Utility.LogDiagnosticEntry("WebAPI_BuildAvailReq - Begin");
                    airAvailRequest = AirRequestBuilder.BuildRequest(uid, cul, cid, curr, ip, numRes, adt, chld, inf,
                                                                     senr, frm1, to1,
                                                                     frm2, to2, frm3,
                                                                     to3, frm4, to4, frm5, to5, date1, date2, date3,
                                                                     date4,
                                                                     date5, trip, nonstop, cabin,
                                                                     window, providebaggageinfo, isfrm1city, isfrm2city,
                                                                     isfrm3city, isfrm4city, isfrm5city,
                                                                     isto1city, isto2city, isto3city, isto4city,
                                                                     isto5city);
                    Utility.LogDiagnosticEntry("WebAPI_BuildAvailReq - End");
                    entry.RequestObject = airAvailRequest;
                    entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                    Utility.LogDiagnosticEntry("WebAPI_BuildFlightReq - Begin");
                    FlightSearchRQ flightSearchRequest = AirTranslator.GetFlightSearchRequest(airAvailRequest);
                    Utility.LogDiagnosticEntry("WebAPI_BuildFlightReq - End");

                    Utility.LogDiagnosticEntry("WebAPI_Search - Begin");
                    FlightSearchRS flightSearchResponse = Search(flightSearchRequest);
                    Utility.LogDiagnosticEntry("WebAPI_Search - End");

                    Utility.LogDiagnosticEntry("WebAPI_FlightRS_ApplyTransform - Begin");
                    flightSearchResponse = Transformer.ApplyTransforms(flightSearchResponse);
                    Utility.LogDiagnosticEntry("WebAPI_FlightRS_ApplyTransform - End");

                    Utility.LogDiagnosticEntry("WebAPI_TranslateToAvailRS - Begin");
                    airAvailRs = AirTranslator.GetAirAvailResponse(flightSearchResponse, airAvailRequest);
                    Utility.LogDiagnosticEntry("WebAPI_TranslateToAvailRS - End");

                    Utility.LogDiagnosticEntry("WebAPI_AvailRS_ApplyTransform - Begin");
                    airAvailRs = Transformer.ApplyTransforms(airAvailRs);
                    Utility.LogDiagnosticEntry("WebAPI_AvailRS_ApplyTransform - End");
                    entry.ResponseObject = airAvailRs;
                }

                return airAvailRs;
            }
            catch (BaseApplicationException exception)
            {
                Utility.GetLogFactory().WriteAsync(exception.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
                entry.Title = string.Format(entry.Title, "WebAPISearch");
                entry.StatusType = StatusOptions.Failure;
                throw new FaultException<ApplicationFault>(new ApplicationFault
                {
                    ErrorCode = exception.ErrorCode,
                    ErrorMessage = exception.Message
                }, exception.GetType().Name);
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
                entry.Title = string.Format(entry.Title, "WebAPISearch");
                entry.StatusType = StatusOptions.Failure;                
                throw new FaultException<ApplicationFault>(ApplicationFault.UnknownError, ex.GetType().Name);
            }
            finally
            {
                stopwatch.Stop();
                entry.TimeTaken = stopwatch.ElapsedSeconds;

                if (SettingManager.AirEngineSearchCallLoggingEnabled || entry.StatusType == StatusOptions.Failure)
                {
                    //SetRequestResponseObjectNull(entry);
                    Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
                }
            }
        }        

        #endregion

        #region Protected Members

        protected void GetCountNSuccess(FlightSearchRS response, out bool success, out int resultCount)
        {
            success = false;
            resultCount = 0;

            if (response != null && response.ServiceStatus.Status != CallStatus.Failure)
            {
                success = true;

                resultCount = response.ItineraryRecommendations == null
                                  ? 0
                                  : response.ItineraryRecommendations.Count;

                resultCount += response.LegRecommendations == null
                                  ? 0
                                  : response.LegRecommendations.Count;

                if (response.ServiceStatus.Status == CallStatus.InProgress && resultCount == 0)
                    resultCount = -1;
            }
        }

        protected virtual FlightSearchRS TryGetFromSessionComplete()
        {
            var availEngine = RuntimeContext.Resolver.Resolve<IAvailEngine>();

            var result = availEngine.TryGetResult(null);

            if (result.Status == Entities.CallStatus.InProgress)
                return new FlightSearchRS()
                           {
                               InProgress = true,
                               ServiceStatus = new ServiceStatus() {Status = CallStatus.InProgress},
                               SessionID = Utility.GetCurrentContext().SessionId
                           };

            var retVal = FlightSearchRS.ToDataContract(result);

            return retVal;
        }

        protected virtual FlightSearchRS TryGetFromSessionAsAvailable(DateTime previousTimeStamp)
        {
            var availEngine = RuntimeContext.Resolver.Resolve<IAvailEngine>();

            var timestamp = Math.Abs((DateTime.UtcNow - previousTimeStamp).TotalMinutes) > 10 ? null : new DateTime?(previousTimeStamp);

            var result = availEngine.TryGetResult(timestamp);

            Utility.LogDiagnosticEntry("Translating into response contract");
            var retVal = FlightSearchRS.ToDataContract(result);
            Utility.LogDiagnosticEntry("finished translating into response contract");

            return retVal;
        }

        protected virtual FlightSearchRS SearchInternal(FlightSearchRQ request, IEventEntry entry)
        {
            entry.Title = string.Format(entry.Title, "Search");

            try
            {
                var availEngine = RuntimeContext.Resolver.Resolve<IAvailEngine>();

                Utility.LogDiagnosticEntry("Translate into request entity - Begin");
                var requester = Requester.ToEntity(request.Requester);
                var criterion = FlightSearchCriteria.ToEntity(request.SearchCriterion);
                Utility.LogDiagnosticEntry("Translate into request entity - End");

                var result = availEngine.Avail(requester, criterion, false);

                Utility.LogDiagnosticEntry("Translating into response contract");
                var retVal = FlightSearchRS.ToDataContract(result);
                Utility.LogDiagnosticEntry(" finished translating into response contract");

                if (retVal != null)
                    entry.ResponseObject = retVal;

                if (retVal != null && retVal.ServiceStatus != null && retVal.ServiceStatus.Status == CallStatus.Failure)
                    entry.StatusType = StatusOptions.Failure;
                
                return retVal;

            }
            catch
            {
                entry.StatusType = StatusOptions.Failure;

                throw;
            }
        }

        protected virtual FlightSearchRS DeferredSearch(FlightSearchRQ request, IEventEntry entry)
        {
            entry.Title = string.Format(entry.Title, "Deferred search");

            var taskFactory = TaskFactoryFactory.GetTaskFactory(SchedulerTypeOptions.Default);
            var availEngine = RuntimeContext.Resolver.Resolve<IAvailEngine>();

            availEngine.NotifySession(SessionNotificationType.Begun);

            taskFactory.StartAmbient(() =>
            {
                var logFactory = Utility.GetLogFactory();
                var taskStopwatch = new WrappedTimer();
                taskStopwatch.Start();
                var taskEventEntry = Utility.GetEventEntryContextual();
                taskEventEntry.Title = entry.Title;
                taskEventEntry.CallType = KeyStore.CallTypes.DeferredAirSearch;

                try
                {
                    var requester = Requester.ToEntity(request.Requester);
                    var criterion = FlightSearchCriteria.ToEntity(request.SearchCriterion);

                    var retVal = availEngine.Avail(requester, criterion, request.AllowSupplierStreaming);

                    if (retVal != null && retVal.Status == Entities.CallStatus.Failure)
                        taskEventEntry.StatusType = StatusOptions.Failure;
                }
                catch (Exception ex)
                {
                    taskEventEntry.StatusType = StatusOptions.Failure;

                    logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
                }
                finally
                {
                    taskStopwatch.Stop();
                    taskEventEntry.TimeTaken = taskStopwatch.ElapsedSeconds;
                    logFactory.WriteAsync(taskEventEntry, KeyStore.LogCategories.ServiceRootLevel);
                }

            }, true);

            var response = new FlightSearchRS()
                               {
                                   InProgress = true,
                                   ServiceStatus = new ServiceStatus() {Status = CallStatus.InProgress},
                                   SessionID = Utility.GetCurrentContext().SessionId
                               };

            entry.ResponseObject = response;

            return response;
        }

        protected IValidationFactory GetValidationFactory()
        {
            return RuntimeContext.Resolver.Resolve<IValidationFactory>();
        }

        private static void SetAnalyticsDataForRequest<T>(T searchRQ, string title, IInfluxDbTableConverter<T> converter)
            where T : class
        {
            var data = new GenericBlobContextAwareAnalyticData<T>(KeyStore.Analytics.FlightSearchApplicationId,
                                                                  title, KeyStore.Analytics.RequestTag, searchRQ, converter);

            AnalyticWriterFacade.WriteAnalyticData(data);
        }

        private static void SetAnalyticsDataForSearchResponse(FlightSearchRS response)
        {
            if (response == null)
                return;

            var data = new GenericBlobContextAwareAnalyticData<FlightSearchRS>(
                KeyStore.Analytics.FlightSearchApplicationId, KeyStore.Analytics.Titles.FlightSearchResponse,
                KeyStore.Analytics.ResponseTag, response, GetFlightSearchRSConverter()
                );

            AnalyticWriterFacade.WriteAnalyticData(data);
        }

        private static void SetAnalyticsDataForRoot(TimeSpan timeTaken, string title, bool success, int resultCount)
        {
            var context = Utility.GetCurrentContext();

            var accId = context.AccountId;
            var sessionId = context.SessionId.ToString();

            var data = new FlightTimeAwareAnalyticalData(accId, sessionId,
                                                             KeyStore.Analytics.FlightSearchApplicationId,
                                                             title, KeyStore.Analytics.ResponseTimeTag,
                                                             resultCount, success, string.Empty,
                                                             timeTaken);

            AnalyticWriterFacade.WriteAnalyticData(data);

            var maxSupplierTime = Utility.GetCurrentContext().GetMaxSupplierTime();

            if (maxSupplierTime.HasValue)
            {
                var overheadData = new FlightTimeAwareAnalyticalData(accId, sessionId,
                                                             KeyStore.Analytics.FlightSearchApplicationId,
                                                             KeyStore.Analytics.Titles.FlightSearchOverhead,
                                                             KeyStore.Analytics.ResponseTimeTag,
                                                             resultCount, success, string.Empty,
                                                             TimeSpan.FromMilliseconds(timeTaken.TotalMilliseconds - maxSupplierTime.Value));

                AnalyticWriterFacade.WriteAnalyticData(overheadData);
            }
        }

        private static FlightSearchRQConverter GetFlightSearchRQConverter()
        {
            return RuntimeContext.Resolver.Resolve<FlightSearchRQConverter>();
        }

        private static FlightSearchRSConverter GetFlightSearchRSConverter()
        {
            return RuntimeContext.Resolver.Resolve<FlightSearchRSConverter>();
        }

        private static FlightPollRQConverter GetFlightPollRQConverter()
        {
            return RuntimeContext.Resolver.Resolve<FlightPollRQConverter>();
        }

        #endregion

        #region private methods

        private static FlightSearchRS GetGenericErrorResponse()
        {
            return new FlightSearchRS()
                       {
                           ServiceStatus =
                               new ServiceStatus()
                                   {
                                       Messages =
                                           new List<string>(),
                                       Status = CallStatus.Failure,
                                       StatusCode = KeyStore.ErrorCodes.InternalError,
                                       StatusMessage =
                                           ErrorCodeManager.GetErrorMessage(KeyStore.ErrorCodes.InternalError,
                                                                            KeyStore.ErrorMessages.
                                                                                DefaultErrorMessage)
                                   },
                           SessionID = Utility.GetCurrentContext().SessionId
                       };
        }

        private static FlightSearchRS GetValidationFailedResponse(List<string> errorMessages)
        {
            return new FlightSearchRS()
                       {
                           ServiceStatus = new ServiceStatus()
                                               {
                                                   Messages = errorMessages,
                                                   StatusCode = KeyStore.ErrorCodes.ValidationFailure,
                                                   Status = CallStatus.Failure,
                                                   StatusMessage = ErrorCodeManager.GetErrorMessage(KeyStore.ErrorCodes.ValidationFailure)
                                               },
                           SessionID = Utility.GetCurrentContext().SessionId
                       };
        }

        #endregion

    }
}