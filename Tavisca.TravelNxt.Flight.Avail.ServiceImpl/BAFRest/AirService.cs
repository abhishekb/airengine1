﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;
using Tavisca.TravelNxt.Flight.Avail.ServiceContract;
using Tavisca.TravelNxt.Flight.Avail.ServiceContract.BAFRest;
using AirAvailRs = Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF.AirAvailRs;

namespace Tavisca.TravelNxt.Flight.Avail.ServiceImpl.BAFRest
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class AirService : IAirService
    {       
        public AirAvailRs Search(string uid, string cul, string cid, string curr, string ip, int numRes, int adt, int chld, int inf,
           int senr, string frm1, bool isfrm1city, string to1, bool isto1city, string frm2, bool isfrm2city, string to2,
           bool isto2city, string frm3, bool isfrm3city, string to3, bool isto3city, string frm4, bool isfrm4city, string to4,
           bool isto4city, string frm5, bool isfrm5city, string to5, bool isto5city, string date1, string date2, string date3,
           string date4, string date5, string trip, bool nonstop, string cabin, int window, bool providebaggageinfo)
        {
            IAirEngine airEngine = new AirEngine();

            var oskiAirAvailRs = airEngine.Search(uid, cul, cid, curr, ip, numRes, adt, chld, inf, senr,
                frm1, isfrm1city, to1, isto1city, frm2, isfrm2city, to2, isto2city, frm3,
                isfrm3city, to3, isto3city, frm4, isfrm4city, to4, isto4city, frm5, isfrm5city, to5, isto5city,
                date1, date2, date3, date4, date5, trip, nonstop, cabin, window, providebaggageinfo);

            AirAvailRs airAvailRs = AirAvailRsTranslator.TranslateToBaf(oskiAirAvailRs);

            return airAvailRs;
        }
    }
}
