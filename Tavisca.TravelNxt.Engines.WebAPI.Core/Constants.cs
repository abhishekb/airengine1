﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public static class Constants
    {
        public const string HeaderName = "CultureInfoHeader";

        public const string HeaderNameSpace = "Tavisca.Framework.CulturePlugin";

        public const string LOG_ONLY_POLICY = "Log Only Policy";

        public static string DefaultCultureName
        {
            get
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCultureName"]) ? "en-US" : ConfigurationManager.AppSettings["DefaultCultureName"];
            }
        }


        public static string DefaultCurrencyCode
        {
            get
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCurrencyCode"]) ? "USD" : ConfigurationManager.AppSettings["DefaultCurrencyCode"];
            }
        }

        public static string CultureProvider
        {
            get
            {
                return ConfigurationManager.AppSettings["CultureProvider"];
            }
        }
    }
}
