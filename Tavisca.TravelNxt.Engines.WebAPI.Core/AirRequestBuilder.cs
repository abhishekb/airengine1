﻿using System;
using System.Collections.Generic;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public sealed class AirRequestBuilder
    {
        public static AirAvailRq BuildRequest(string uid, string cul, string cid, string curr, string ip, int numRes, int adt,
             int chld, int inf, int senr, string frm1, string to1, string frm2, string to2, string frm3, string to3, string frm4,
             string to4, string frm5, string to5, string date1, string date2, string date3, string date4, string date5,
             string trip, bool isNonStop, string cabin, int window, bool provideBaggageInfo,
             bool isfrm1city, bool isfrm2city, bool isfrm3city, bool isfrm4city, bool isfrm5city,
             bool isto1city, bool isto2city, bool isto3city, bool isto4city, bool isto5city)
        {
            SetDefaultValues(ref cul, ref curr, ref numRes, ref adt, ref chld, ref inf, ref senr, ref cabin, ref window);
            Validate(uid, ip, adt, chld, inf, senr, trip, frm1, to1, date1, frm2, to2, date2, frm3, to3, date3, frm4, to4, date4, frm5, to5, date5, cabin, window);

            var request = new AirAvailRq
            {
                Cabin = (CabinType)Enum.Parse(typeof(CabinType), cabin),
                CacheId = Utility.GetCurrentContext().SessionId.ToString(),
                Culture = cul,
                CurrencyCode = curr,
                IpAddress = ip,
                MaxStops = isNonStop ? 0 : 3,
                NumResults = numRes,
                SearchWindow = window,
                Uid = uid,
                TripType = GetTripType(trip),
                ProvideBaggageInfo = provideBaggageInfo
            };

            AddPassengers(adt, chld, inf, senr, request);

            AddSegments(frm1, to1, date1, frm2, to2, date2, frm3, to3, date3, frm4, to4, date4, frm5, to5, date5, isfrm1city, isfrm2city,
                                                                     isfrm3city, isfrm4city, isfrm5city,
                                                                     isto1city, isto2city, isto3city, isto4city,
                                                                     isto5city, request);

            return request;
        }

        //public static string BuildRequestString(string uid, string cul, string cid, string curr, string ip, int numRes, int adt,
        //    int chld, int inf, int senr, string frm1, string to1, string frm2, string to2, string frm3, string to3, string frm4,
        //    string to4, string frm5, string to5, string date1, string date2, string date3, string date4, string date5,
        //    string trip, bool isNonStop, string cabin, int window)
        //{
        //    var stringBuilder = new StringBuilder()
        //    .AppendLine("uid=" + uid)
        //    .AppendLine("cid=" + cid)
        //    .AppendLine("curr=" + curr)
        //    .AppendLine("ip=" + ip)
        //    .AppendLine("numRes=" + numRes)
        //    .AppendLine("adt=" + adt)
        //    .AppendLine("chld=" + chld)
        //    .AppendLine("inf=" + inf)
        //    .AppendLine("senr=" + senr)
        //    .AppendLine("frm1=" + frm1)
        //    .AppendLine("to1=" + to1)
        //    .AppendLine("frm2=" + frm2)
        //    .AppendLine("to2=" + to2)
        //    .AppendLine("frm3=" + frm3)
        //    .AppendLine("to3=" + to3)
        //    .AppendLine("frm4=" + frm4)
        //    .AppendLine("to4=" + to4)
        //    .AppendLine("frm5=" + frm5)
        //    .AppendLine("to5=" + to5)
        //    .AppendLine("date1=" + date1)
        //    .AppendLine("date2=" + date2)
        //    .AppendLine("date3=" + date3)
        //    .AppendLine("date4=" + date4)
        //    .AppendLine("date5=" + date5)
        //    .AppendLine("trip=" + trip)
        //    .AppendLine("isNonStop=" + isNonStop)
        //    .AppendLine("cabin=" + cabin)
        //    .AppendLine("window=" + window);
        //    return stringBuilder.ToString();
        //}

        private static void AddSegments(string frm1, string to1, string date1, string frm2, string to2, string date2, string frm3, string to3, string date3, string frm4, string to4, string date4, string frm5, string to5, string date5, bool isfrm1city, bool isfrm2city, bool isfrm3city, bool isfrm4city, bool isfrm5city,
            bool isto1city, bool isto2city, bool isto3city, bool isto4city, bool isto5city, AirAvailRq request)
        {
            request.SearchSegments = new List<SearchSegment>();

            request.SearchSegments.Add(new SearchSegment()
            {
                OriginCode = frm1,
                DestCode = to1,
                IsOriginCodeCity=isfrm1city,
                IsDestCodeCity=isto1city,
                DepartDate = DateTime.Parse(date1)
            });
            if (!string.IsNullOrEmpty(frm2))
            {
                request.SearchSegments.Add(new SearchSegment()
                {
                    OriginCode = frm2,
                    DestCode = to2,
                    IsOriginCodeCity = isfrm2city,
                    IsDestCodeCity = isto2city,
                    DepartDate = DateTime.Parse(date2)
                });
            }

            if (!string.IsNullOrEmpty(frm3))
            {
                request.SearchSegments.Add(new SearchSegment()
                {
                    OriginCode = frm3,
                    DestCode = to3,
                    IsOriginCodeCity = isfrm3city,
                    IsDestCodeCity = isto3city,
                    DepartDate = DateTime.Parse(date3)
                });
            }

            if (!string.IsNullOrEmpty(frm4))
            {
                request.SearchSegments.Add(new SearchSegment()
                {
                    OriginCode = frm4,
                    DestCode = to4,
                    IsOriginCodeCity = isfrm4city,
                    IsDestCodeCity = isto4city,
                    DepartDate = DateTime.Parse(date4)
                });
            }

            if (!string.IsNullOrEmpty(frm5))
            {
                request.SearchSegments.Add(new SearchSegment()
                {
                    OriginCode = frm5,
                    DestCode = to5,
                    IsOriginCodeCity = isfrm5city,
                    IsDestCodeCity = isto5city,
                    DepartDate = DateTime.Parse(date5)
                });
            }
        }

        private static void AddPassengers(int adt, int chld, int inf, int senr, AirAvailRq request)
        {
            request.Adults = 0;
            request.Children = 0;
            request.Infants = 0;
            request.Seniors = 0;

            if (adt > 0)
                request.Adults = adt;

            if (chld > 0)
                request.Children = chld;

            if (inf > 0)
                request.Infants = inf;

            if (senr > 0)
                request.Seniors = senr;
        }

        private static TripType GetTripType(string trip)
        {
            switch (trip.ToUpper())
            {
                case "OW":
                    return TripType.OneWay;
                case "RT":
                    return TripType.RoundTrip;
                case "MC":
                    return TripType.MultiStop;
                default:
                    return TripType.RoundTrip;
            }
        }

        public static void SetDefaultValues(ref string cul, ref string curr, ref int numRes, ref int adt, ref int chld, ref int inf, ref int senr, ref string cabin, ref int window)
        {
            var context = Utility.GetCurrentContext();
            if (context != null && context.Culture!=null && !string.IsNullOrEmpty(context.Culture.Name))
            {
                if (string.IsNullOrEmpty(cul))
                    cul = context.Culture.Name;
                if (string.IsNullOrEmpty(curr))
                    curr = context.DisplayCurrency;
            }
            if (numRes <= 0)
                numRes = 500;
            if (string.IsNullOrEmpty(cabin))
                cabin = CabinType.Economy.ToString();
            if (window < 0)
                window = 0;
            if (adt < 0)
                adt = 0;
            if (chld < 0)
                chld = 0;
            if (senr < 0)
                senr = 0;
            if (inf < 0)
                inf = 0;
        }

        public static void SetDefaultValues(AirAvailRq airAvailRq)
        {
            var context = Utility.GetCurrentContext();
            if (context != null && context.Culture != null && !string.IsNullOrEmpty(context.Culture.Name))
            {
                if (string.IsNullOrEmpty(airAvailRq.Culture))
                    airAvailRq.Culture = context.Culture.Name;
                if (string.IsNullOrEmpty(airAvailRq.CurrencyCode))
                    airAvailRq.CurrencyCode = context.DisplayCurrency;
            }
            if (airAvailRq.NumResults <= 0)
                airAvailRq.NumResults = 500;
            if (string.IsNullOrEmpty(airAvailRq.Cabin.ToString()))
                airAvailRq.Cabin = CabinType.Economy;
            if (airAvailRq.SearchWindow < 0)
                airAvailRq.SearchWindow = 0;
            if (airAvailRq.Adults < 0)
                airAvailRq.Adults = 0;
            if (airAvailRq.Children < 0)
                airAvailRq.Children = 0;
            if (airAvailRq.Seniors < 0)
                airAvailRq.Seniors = 0;
            if (airAvailRq.Infants < 0)
                airAvailRq.Infants = 0;
        }

        private static void Validate(string uid, string ip, int adt, int chld, int inf, int senr, string trip, string frm1, string to1, string date1, string frm2, string to2, string date2, string frm3, string to3, string date3, string frm4, string to4, string date4, string frm5, string to5, string date5, string cabin, int window)
        {
            if (string.IsNullOrEmpty(uid))
                throw new InvalidArgumentException("uid", ArgumentStatus.Null);

            if (string.IsNullOrEmpty(ip))
                throw new InvalidArgumentException("ip", ArgumentStatus.Null);

            int totalPax = 0;

            totalPax += adt;

            totalPax += chld;

            totalPax += inf;

            totalPax += senr;

            if (totalPax > 7)
                throw new InvalidArgumentException("passengers", ArgumentStatus.OutOfRange,
                                                   " total passengers less than 7.");

            if (string.IsNullOrEmpty(trip))
                throw new InvalidArgumentException("trip", ArgumentStatus.Null);

            if (!string.Equals(trip, "OW", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(trip, "RT", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(trip, "MC", StringComparison.OrdinalIgnoreCase))
                throw new InvalidArgumentException("trip", ArgumentStatus.Invalid, "OW/RT/MC");


            if (trip == "OW")
            {
                if (string.IsNullOrEmpty(frm1) || string.IsNullOrEmpty(to1))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);

                ValidateDate(date1, "date1");
            }

            if (trip == "RT")
            {

                if ((string.IsNullOrEmpty(frm1) || string.IsNullOrEmpty(to1) || string.IsNullOrEmpty(frm2) ||
                     string.IsNullOrEmpty(to2)))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);
                ValidateDate(date1, "date1");
                ValidateDate(date2, "date2");
            }

            if (trip == "MC")
            {
                if ((string.IsNullOrEmpty(frm1) || string.IsNullOrEmpty(to1) || string.IsNullOrEmpty(frm2) ||
                     string.IsNullOrEmpty(to2)))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);
                ValidateDate(date1, "date1");
                ValidateDate(date2, "date2");

                if (string.IsNullOrEmpty(frm3))
                {
                    if (!string.IsNullOrEmpty(to3))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(date3, "date3");
                }
                if (string.IsNullOrEmpty(frm4))
                {
                    if (!string.IsNullOrEmpty(to4))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(date4, "date4");
                }

                if (string.IsNullOrEmpty(frm5))
                {
                    if (!string.IsNullOrEmpty(to5))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(date5, "date5");
                }
            }


            CabinType result;
            if (!Enum.TryParse(cabin, true, out result))
                throw new InvalidArgumentException("cabin", ArgumentStatus.Invalid, "Economy/First/Business.");


            if (window > 5)
                throw new InvalidArgumentException("window", ArgumentStatus.OutOfRange, "less than or equal to 5.");
        }


        public static void Validate(AirAvailRq request)
        {
            if (string.IsNullOrEmpty(request.Uid))
                throw new InvalidArgumentException("uid", ArgumentStatus.Null);

            if (string.IsNullOrEmpty(request.IpAddress))
                throw new InvalidArgumentException("ip", ArgumentStatus.Null);

            int totalPax = 0;

            totalPax += request.Adults;

            totalPax += request.Children;

            totalPax += request.Infants;

            totalPax += request.Seniors;

            if (totalPax > 7)
                throw new InvalidArgumentException("passengers", ArgumentStatus.OutOfRange,
                                                   " total passengers less than 7.");

            //if (string.IsNullOrEmpty(request.TripType.ToString()))
            //    throw new InvalidArgumentException("trip", ArgumentStatus.Null);

            //if (!string.Equals(request.TripType.ToString(), "OW", StringComparison.OrdinalIgnoreCase) &&
            //    !string.Equals(request.TripType.ToString(), "RT", StringComparison.OrdinalIgnoreCase) &&
            //    !string.Equals(request.TripType.ToString(), "MC", StringComparison.OrdinalIgnoreCase))
            //    throw new InvalidArgumentException("trip", ArgumentStatus.Invalid, "OW/RT/MC");

            if (request.SearchSegments == null || request.SearchSegments.Count < 1)
                throw new InvalidArgumentException("SearchSegments", ArgumentStatus.Null);

            if (request.TripType == TripType.OneWay)
            {              
                if(request.SearchSegments.Count != 1)
                    throw new InvalidArgumentException("SearchSegments", ArgumentStatus.OutOfRange);

                if (string.IsNullOrEmpty(request.SearchSegments[0].DestCode) || string.IsNullOrEmpty(request.SearchSegments[0].OriginCode))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);

                ValidateDate(request.SearchSegments[0].DepartDate, "date1");
            }

            if (request.TripType == TripType.RoundTrip)
            {
                if (request.SearchSegments.Count != 2)
                    throw new InvalidArgumentException("SearchSegments", ArgumentStatus.OutOfRange);

                if ((string.IsNullOrEmpty(request.SearchSegments[0].OriginCode) || string.IsNullOrEmpty(request.SearchSegments[0].DestCode) 
                    || string.IsNullOrEmpty(request.SearchSegments[1].OriginCode) || string.IsNullOrEmpty(request.SearchSegments[1].DestCode)))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);
                ValidateDate(request.SearchSegments[0].DepartDate, "date1");
                ValidateDate(request.SearchSegments[1].DepartDate, "date2");
            }

            if (request.TripType == TripType.MultiStop)
            {
                if (request.SearchSegments.Count < 2 && request.SearchSegments.Count > 5)
                    throw new InvalidArgumentException("SearchSegments", ArgumentStatus.OutOfRange);

                if ((string.IsNullOrEmpty(request.SearchSegments[0].OriginCode) || string.IsNullOrEmpty(request.SearchSegments[0].DestCode) 
                    || string.IsNullOrEmpty(request.SearchSegments[1].OriginCode) || string.IsNullOrEmpty(request.SearchSegments[1].DestCode)))
                    throw new InvalidArgumentException("from/to", ArgumentStatus.Null);

                ValidateDate(request.SearchSegments[0].DepartDate, "date1");
                ValidateDate(request.SearchSegments[1].DepartDate, "date2");

                if (string.IsNullOrEmpty(request.SearchSegments[2].OriginCode))
                {
                    if (!string.IsNullOrEmpty(request.SearchSegments[2].DestCode))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(request.SearchSegments[2].DepartDate, "date3");
                }
                if (string.IsNullOrEmpty(request.SearchSegments[3].OriginCode))
                {
                    if (!string.IsNullOrEmpty(request.SearchSegments[3].DestCode))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(request.SearchSegments[3].DepartDate, "date4");
                }

                if (string.IsNullOrEmpty(request.SearchSegments[4].OriginCode))
                {
                    if (!string.IsNullOrEmpty(request.SearchSegments[4].DestCode))
                        throw new InvalidArgumentException("from/to", ArgumentStatus.Invalid);
                }
                else
                {
                    ValidateDate(request.SearchSegments[4].DepartDate, "date5");
                }
            }         


            if (request.SearchWindow > 5)
                throw new InvalidArgumentException("window", ArgumentStatus.OutOfRange, "less than or equal to 5.");
        }

        private static void ValidateDate(string date, string name)
        {
            DateTime dateTime;

            if (string.IsNullOrEmpty(date))
                throw new InvalidArgumentException(name, ArgumentStatus.Null);
            if (!DateTime.TryParse(date, out dateTime))
                throw new InvalidArgumentException(name, ArgumentStatus.Invalid);
            if (dateTime < DateTime.Now)
                throw new InvalidArgumentException(name, ArgumentStatus.Invalid, "Date cannot be in the past.");
        }


        private static void ValidateDate(DateTime date, string name)
        {           
            if (date==null)
                throw new InvalidArgumentException(name, ArgumentStatus.Null);          
            if (date < DateTime.Now)
                throw new InvalidArgumentException(name, ArgumentStatus.Invalid, "Date cannot be in the past.");
        }
    }
}
