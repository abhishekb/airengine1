﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class FareComponentTranslator
    {
        public static List<AirFareComponent> ToWebApi(List<Flight.Avail.DataContract.FareComponent> oskiFareComponents)
        {
            List<AirFareComponent> webApiFareComponents = new List<AirFareComponent>();

            if (oskiFareComponents != null && oskiFareComponents.Count > 0)
            {
                foreach (var fareComponent in oskiFareComponents)
                {
                    webApiFareComponents.Add(ToWebApi(fareComponent));
                }
            }

            return webApiFareComponents;
        }

        public static AirFareComponent ToWebApi(Flight.Avail.DataContract.FareComponent oskiFareComponent)
        {
            AirFareComponent airFareComponent = new AirFareComponent();

            if (oskiFareComponent != null)
            {
                airFareComponent = new AirFareComponent
                {
                    Code = oskiFareComponent.Code,
                    AirFareComponentType = (AirFareComponentType) oskiFareComponent.FareComponentType,                    
                    Amount = oskiFareComponent.Value.DisplayAmount                                 
                };
            }

            return airFareComponent;
        }
    }
}
