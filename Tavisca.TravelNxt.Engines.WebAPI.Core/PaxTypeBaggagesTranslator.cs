﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public static class PaxTypeBaggagesTranslator
    {
        public static List<PaxTypeBaggage> ToDataContract(List<Flight.Avail.DataContract.PaxTypeBaggage> paxTypeBaggages)
        {
            List<PaxTypeBaggage> wrapperPaxTypeBaggages = new List<PaxTypeBaggage>();

            if (paxTypeBaggages != null && paxTypeBaggages.Count > 0)
            {
                foreach (var paxTypeBaggage in paxTypeBaggages)
                {
                    if (paxTypeBaggage != null)
                    {
                        wrapperPaxTypeBaggages.Add(ToDataContract(paxTypeBaggage));
                    }
                }
            }

            return wrapperPaxTypeBaggages;
        }

        private static PaxTypeBaggage ToDataContract(Flight.Avail.DataContract.PaxTypeBaggage paxTypeBaggage)
        {
            return new PaxTypeBaggage
            {
                BaggageCode = paxTypeBaggage.BaggageCode,
                PassengerType = paxTypeBaggage.PassengerType
            };
        }
    }
}
