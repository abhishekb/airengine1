﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public interface IResultTransform
    {
        FlightSearchRS GetTransformedResponse(FlightSearchRS originalResponse);
    }
}
