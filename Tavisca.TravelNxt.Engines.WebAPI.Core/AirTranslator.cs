﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;
using Tavisca.TravelNxt.Flight.Settings;
using CabinType = Tavisca.TravelNxt.Flight.Avail.DataContract.CabinType;
using PassengerType = Tavisca.TravelNxt.Flight.Avail.DataContract.PassengerType;
using SearchSegment = Tavisca.TravelNxt.Flight.Avail.DataContract.SearchSegment;
using TimeZoneInfo = System.TimeZoneInfo;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.OSKI
{
    public class AirTranslator
    {
        #region Translate Request to Oski Contract
        public static FlightSearchRQ GetFlightSearchRequest(DataContracts.AirAvailRq airAvailRequest)
        {
            if (airAvailRequest == null)
                return null;
            var flightSearchRequest = new FlightSearchRQ
            {
                SearchCriterion = GetflightSearchCriterion(airAvailRequest),
                AllowSupplierStreaming = false,
                DoDeferredSearch = false,
                Requester = new Requester() { CustomerType = CustomerType.Traveller },
            };
            return flightSearchRequest;
        }

        private static FlightSearchCriteria GetflightSearchCriterion(DataContracts.AirAvailRq airAvailRequest)
        {
            var flightSearchCriterion = new FlightSearchCriteria
            {
                SearchSegments = GetFlightSearchSegments(airAvailRequest),
                MaxPreferredResults = airAvailRequest.NumResults,
                SortingOrder = SortingOrder.ByPriceLowToHigh,
                PassengerInfoSummary = GetPassengerInfo(airAvailRequest),
                FlightTravelPreference = GetFlightTravelPreference(airAvailRequest),
            };
            return flightSearchCriterion;
        }

        private static List<PaxTypeQuantity> GetPassengerInfo(DataContracts.AirAvailRq airAvailRequest)
        {
            var passengerInfoSummary = new List<PaxTypeQuantity>();
            //All Ages are hard coded because Oski Fails if ages are not sent.
            if (airAvailRequest.Adults != 0)
            {
                passengerInfoSummary.Add(new PaxTypeQuantity
                {
                    PassengerType = PassengerType.Adult,
                    Quantity = airAvailRequest.Adults,
                    Ages = Enumerable.Repeat(30, airAvailRequest.Adults).ToList()
                });
            }
            if (airAvailRequest.Children != 0)
            {
                passengerInfoSummary.Add(new PaxTypeQuantity
                {
                    PassengerType = PassengerType.Child,
                    Quantity = airAvailRequest.Children,
                    Ages = Enumerable.Repeat(7, airAvailRequest.Children).ToList()
                });
            }
            if (airAvailRequest.Infants != 0)
            {
                passengerInfoSummary.Add(new PaxTypeQuantity
                {
                    PassengerType = PassengerType.Infant,
                    Quantity = airAvailRequest.Infants,
                    Ages = Enumerable.Repeat(1, airAvailRequest.Infants).ToList()
                });
            }
            if (airAvailRequest.Seniors != 0)
            {
                passengerInfoSummary.Add(new PaxTypeQuantity
                {
                    PassengerType = PassengerType.Senior,
                    Quantity = airAvailRequest.Seniors,
                    Ages = Enumerable.Repeat(70, airAvailRequest.Seniors).ToList()
                });
            }

            return passengerInfoSummary;
        }

        private static AirlinesPreference GetAirlinePreference()
        {
            var prohibitedAirlines = SettingManager.WebAPI_BlockedAirlines;
            var preferredAirlines = SettingManager.WebAPI_PreferredAirlines;
            var permittedAirlines = SettingManager.WebAPI_RequiredAirlines;
            if (prohibitedAirlines != null || preferredAirlines != null || permittedAirlines != null)
                return new AirlinesPreference()
                {
                    PermittedAirlines = permittedAirlines,
                    ProhibitedAirlines = prohibitedAirlines,
                    PreferredAirlines = preferredAirlines
                };
            return null;

        }

        private static FlightTravelPreference GetFlightTravelPreference(DataContracts.AirAvailRq airAvailRequest)
        {
            var flightTravelPreference = new FlightTravelPreference
            {
                IncludeNonStopOnly = airAvailRequest.MaxStops == 0,
                AllowMixedAirlines = true,
                AirlinesPreference = GetAirlinePreference()
            };
            return flightTravelPreference;
        }

        private static List<SearchSegment> GetFlightSearchSegments(DataContracts.AirAvailRq airAvailRequest)
        {
            var flightSearchSegments = new List<SearchSegment>();
            if (airAvailRequest.SearchSegments != null && airAvailRequest.SearchSegments.Count != 0)
            {
                foreach (var searchSegment in airAvailRequest.SearchSegments)
                {
                    var flightSearchSegment = new SearchSegment
                    {
                        Cabin = GetCabinType(airAvailRequest.Cabin),
                        TravelDate = new TravelDate
                        {
                            DateTime = searchSegment.DepartDate,
                            AnyTime = airAvailRequest.SearchWindow <= 0,
                            TimeWindow = airAvailRequest.SearchWindow,
                        },
                    };

                    SetArrivalDepartureCodes(flightSearchSegment, searchSegment);

                    flightSearchSegments.Add(flightSearchSegment);
                }
            }
            return flightSearchSegments;
        }

        private static void SetArrivalDepartureCodes(SearchSegment searchSegment, DataContracts.SearchSegment dcSearchSegment)
        {
            ResolveAndSetLocation(dcSearchSegment.OriginCode, searchSegment,
                LocationType.Departure);

            ResolveAndSetLocation(dcSearchSegment.DestCode, searchSegment,
                LocationType.Arrival);

            //ResolveAndSetLocation(dcSearchSegment.OriginCode, searchSegment,
            //    LocationType.Departure, dcSearchSegment.IsOriginCodeCity);

            //ResolveAndSetLocation(dcSearchSegment.DestCode, searchSegment,
            //    LocationType.Arrival, dcSearchSegment.IsDestCodeCity);
        }

        private static void ResolveAndSetLocation(string locationCode, SearchSegment searchSegment, LocationType locationType)
        {
            if (IsLocationCodeCity(locationCode))
            {
                if (locationCode == "BJS")
                {
                    SetAirportCode("PEK", locationType, searchSegment);
                }
                else
                {
                    SetCityCode(locationCode, locationType, searchSegment);
                }
            }
            else
            {
                SetAirportCode(locationCode, locationType, searchSegment);
            }

            //if (isLocationCodeCity)
            //    SetCityCode(locationCode, locationType, searchSegment);
            //else
            //    SetAirportCode(locationCode, locationType, searchSegment);
        }

        private static bool IsLocationCodeCity(string locationCode)
        {
            if (string.IsNullOrWhiteSpace(locationCode))
            {
                return false;
            }
            return !String.IsNullOrEmpty(AllAirports.FirstOrDefault(a => a.Equals(locationCode, StringComparison.InvariantCultureIgnoreCase)));
        }

        private static void SetAirportCode(string locationCode, LocationType locationType, SearchSegment searchSegment)
        {
            if (locationType == LocationType.Arrival)
                searchSegment.ArrivalAirportCode = locationCode;
            else
                searchSegment.DepartureAirportCode = locationCode;
        }

        private static void SetCityCode(string locationCode, LocationType locationType, SearchSegment searchSegment)
        {
            if (locationType == LocationType.Arrival)
                searchSegment.ArrivalCityCode = locationCode;
            else
                searchSegment.DepartureCityCode = locationCode;
        }

        private static CabinType GetCabinType(DataContracts.CabinType cabinType)
        {
            switch (cabinType)
            {
                case DataContracts.CabinType.Business:
                    return CabinType.Business;
                case DataContracts.CabinType.Economy:
                    return CabinType.Economy;
                case DataContracts.CabinType.First:
                    return CabinType.First;
                default:
                    return CabinType.Unknown;
            }
        }
        #endregion

        #region Translate Response to WebAPI contract
        public static DataContracts.AirAvailRs GetAirAvailResponse(FlightSearchRS flightSearchResponse, DataContracts.AirAvailRq airAvailRequest)
        {
            var airAvailRs = new DataContracts.AirAvailRs();
            if (flightSearchResponse != null && airAvailRequest != null)
            {
                airAvailRs.CacheId = flightSearchResponse.SessionID != Guid.Empty
                                         ? flightSearchResponse.SessionID.ToString()
                                         : airAvailRequest.CacheId;

                airAvailRs.CurrencyCode = airAvailRequest.CurrencyCode;               
                var context = Utility.GetCurrentContext();            
                airAvailRs.Culture = context.Culture.Name;
                airAvailRs.Error = flightSearchResponse.ServiceStatus == null ||
                                   flightSearchResponse.ServiceStatus.Status != CallStatus.Failure
                                       ? null
                                       : new Error
                                       {
                                           ErrorMessage = flightSearchResponse.ServiceStatus.StatusMessage,
                                           ErrorCode = flightSearchResponse.ServiceStatus.StatusCode
                                       };
                airAvailRs.Segments = GetAirSegments(flightSearchResponse, airAvailRequest);

                airAvailRs.AirOptions = GetAirOptions(flightSearchResponse);

                airAvailRs.NumResults = airAvailRs.AirOptions.Count;

                airAvailRs.Baggages = airAvailRequest.ProvideBaggageInfo ? BaggageTranslator.ToDataContract(flightSearchResponse.Baggages) : null;
            }
            return airAvailRs;
        }

        private static List<DataContracts.Flight> GetAirSegments(FlightSearchRS flightSearchResponse, DataContracts.AirAvailRq airAvailRequest)
        {
            if (flightSearchResponse.Segments != null)
                return
                    flightSearchResponse.Segments.Select(
                        flightSegment => GetAirSegment(flightSegment, flightSearchResponse.Airlines, airAvailRequest)).ToList();
            return new List<DataContracts.Flight>();
        }

        private static DataContracts.Flight GetAirSegment(FlightSegment flightSegment, IEnumerable<Airline> airlines, DataContracts.AirAvailRq airAvailRequest)
        {
            var airSegment = new DataContracts.Flight
            {
                FromAirport = flightSegment.DepartureAirportCode,
                ToAirport = flightSegment.ArrivalAirportCode,
                FlightNumber = flightSegment.FlightNumber,
                DurationInMin = flightSegment.DurationMinutes,
                Arrive = TimeZoneInfo.ConvertTimeToUtc(flightSegment.ArrivalDateTime),
                Depart = TimeZoneInfo.ConvertTimeToUtc(flightSegment.DepartureDateTime),
                NumOfStops =
                    flightSegment.InSegmentStops != null ? flightSegment.InSegmentStops.Count : 0,
                Aircraft = flightSegment.AircraftType,
                AirlineCode = flightSegment.MarketingAirlineCode,
                OperatingAirlineCode = flightSegment.OperatingAirlineCode,
                Airline =
                    airlines.Single(airline => airline.Code == flightSegment.MarketingAirlineCode).
                        FullName,
                Key = flightSegment.Key,
                PaxTypeBaggages = PaxTypeBaggagesTranslator.ToDataContract(flightSegment.PaxTypeBaggages)
            };
            return airSegment;
        }

        private static List<AirOption> GetAirOptions(FlightSearchRS flightSearchResponse)
        {
            var airOptions = new List<AirOption>();

            if (flightSearchResponse.ItineraryRecommendations != null && flightSearchResponse.ItineraryRecommendations.Any())
            {
                var itineraryOptions =
                    CreateAirOptionsFromItineraryRecommendations(flightSearchResponse.ItineraryRecommendations,
                                                                 flightSearchResponse);
                airOptions.AddRange(itineraryOptions);
            }
            if (airOptions.Any())
            {
                return airOptions;
            }
            if (flightSearchResponse.LegRecommendations != null && flightSearchResponse.LegRecommendations.Any())
            {
                var legOptions = CreateAirOptionsFromLegRecommendations(flightSearchResponse.LegRecommendations,
                                                                        flightSearchResponse);
                airOptions.AddRange(legOptions);
                if (airOptions.Any())
                {
                    return airOptions;
                }
                var ex = new InvalidOperationException("Only Leg Recommendations returned for Round Trip Search");
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
            }
            return airOptions;
        }

        private static IEnumerable<AirOption> CreateAirOptionsFromItineraryRecommendations(IEnumerable<FlightRecommendation> itineraryRecommendations, FlightSearchRS flightSearchResponse)
        {
            return
                itineraryRecommendations.Select(
                    itineraryRecommendation =>
                    CreateAirOptionFromRecommendation(itineraryRecommendation, flightSearchResponse));
        }

        private static IEnumerable<AirOption> CreateAirOptionsFromLegRecommendations(List<FlightRecommendation> legRecommendations, FlightSearchRS flightSearchResponse)
        {
            //Assumption is LegRecommendations are available only for round trip and one way searches(max 2 LegAlternative Groups) where 0 -> onward, 1 -> return
            var onwardLegRecommendation =
                legRecommendations.Where(leg => leg.LegRefs[0].Split('/')[0].Equals("0")).ToList();
            var returnLegRecommendation =
                legRecommendations.Where(leg => leg.LegRefs[0].Split('/')[0].Equals("1")).ToList();

            var airOptions = new List<AirOption>();
            if (returnLegRecommendation.Any()) //Discard round trip altogether.
                return airOptions;
            airOptions.AddRange(
                onwardLegRecommendation.Select(
                    onwardLeg => CreateAirOptionFromRecommendation(onwardLeg, flightSearchResponse)).Where(
                        airOption => airOption != null));
            return airOptions;
        }

        private static AirOption CreateAirOptionFromRecommendation(FlightRecommendation recommendation, FlightSearchRS flightSearchResponse)
        {
            var currentLegGroupId = 0;
            var optionSegments = new List<FlightRef>();
            var isEticket = true;
            foreach (var legRef in recommendation.LegRefs)
            {
                var legAlternative =
                    flightSearchResponse.LegAlternates.Single(leg => leg.LegIndex == currentLegGroupId).Legs.Single(
                        leg => leg.RefID == Convert.ToInt32(legRef.Split('/')[1]));

                var currentSegments =
                    flightSearchResponse.Segments.Where(
                        segment => legAlternative.FlightSegmentRefs.Contains(segment.RefId)).ToList();
                currentLegGroupId++; //Indexing in response begins from 1

                var segments = GetFlightRefs(currentSegments, currentLegGroupId, ref isEticket);
                optionSegments.AddRange(segments);
            }
            var airOption = new AirOption
            {
                Segments = optionSegments,
                Fare = new Fare
                {
                    BaseFare = GetBaseAmount(recommendation.Fare),
                    Commission = recommendation.Fare.TotalCommission.DisplayAmount,
                    Discount = recommendation.Fare.TotalDiscount.DisplayAmount,
                    FeesPlusTaxes =
                        recommendation.Fare.TotalTaxes.DisplayAmount +
                        recommendation.Fare.TotalFees.DisplayAmount,
                    TotalFare = recommendation.Fare.TotalFare.DisplayAmount,
                    PassengerFares = PassengerFareTranslator.ToWebApi(recommendation.Fare.PassengerFares),
                },
                PlatingCarrier=recommendation.PlatingCarrier,
                Ticket = isEticket ? TicketType.E : TicketType.P,
                IsSpecial = recommendation.Fare.FareType == FareType.Negotiated,
                IsRefundable =
                    recommendation.Fare.FareAttributes != null &&
                    recommendation.Fare.FareAttributes.Contains(FareAttribute.RefundableFares),
                Rph = recommendation.RefID
            };
            return airOption;
        }              

        private static decimal GetBaseAmount(FlightFare flightFare)
        {
            var baseFare = flightFare.BaseAmount.DisplayAmount;
            if (flightFare.TotalMarkup != null)
                baseFare += flightFare.TotalMarkup.DisplayAmount;

            return baseFare;
        }

        private static IEnumerable<FlightRef> GetFlightRefs(IEnumerable<FlightSegment> currentSegments, int currentLegGroup, ref bool isEticket)
        {
            var localIsEticket = isEticket;
            var resultSet = currentSegments.Select(segment =>
            {
                localIsEticket &= segment.CanETicket;
                return new FlightRef
                {
                    FlightKey = segment.Key,
                    LegNum = currentLegGroup,
                    Cabin = segment.CabinType.ToString(),
                    FareClass = segment.ClassOfService
                };
            });
            isEticket = localIsEticket;
            return resultSet;
        }

        #endregion

        public readonly static List<string> AllAirports;

        static AirTranslator()
        {
            AllAirports = new List<string>()
                              {
                                  "YYC",
                                  "MWL",
                                  "DRT",
                                  "BOO",
                                  "MMA",
                                  "DAY",
                                  "CAK",
                                  "WBG",
                                  "SAF",
                                  "OAK",
                                  "BDA",
                                  "HAJ",
                                  "CBU",
                                  "BTZ",
                                  "YQB",
                                  "TIW",
                                  "HKG",
                                  "VAF",
                                  "NLP",
                                  "YMQ",
                                  "MHZ",
                                  "ILE",
                                  "RHE",
                                  "PED",
                                  "BLE",
                                  "AGS",
                                  "JKG",
                                  "CWB",
                                  "FAO",
                                  "FNA",
                                  "MRS",
                                  "NAP",
                                  "MIA",
                                  "ADQ",
                                  "BUR",
                                  "TRN",
                                  "CJU",
                                  "SNA",
                                  "GLL",
                                  "FMM",
                                  "SZG",
                                  "SAN",
                                  "LME",
                                  "PIP",
                                  "DRW",
                                  "SWI",
                                  "AEX",
                                  "ESD",
                                  "HYD",
                                  "MWH",
                                  "SIY",
                                  "PHX",
                                  "FAI",
                                  "MEX",
                                  "DUS",
                                  "YCD",
                                  "GEG",
                                  "ATH",
                                  "BCU",
                                  "CVN",
                                  "CSL",
                                  "MGM",
                                  "YOO",
                                  "VCE",
                                  "UMB",
                                  "MEM",
                                  "GCN",
                                  "VLD",
                                  "BGW",
                                  "AKL",
                                  "HFD",
                                  "FMY",
                                  "SRF",
                                  "SES",
                                  "SAO",
                                  "ZNQ",
                                  "NQT",
                                  "SBA",
                                  "PVD",
                                  "BNK",
                                  "UZC",
                                  "TLV",
                                  "QFF",
                                  "BZI",
                                  "DOV",
                                  "AUS",
                                  "ORB",
                                  "CHI",
                                  "YYJ",
                                  "ISC",
                                  "BUD",
                                  "SJT",
                                  "YCH",
                                  "TLL",
                                  "SFO",
                                  "MON",
                                  "BRQ",
                                  "WDG",
                                  "YUM",
                                  "TYO",
                                  "LEB",
                                  "RNB",
                                  "HAR",
                                  "OZR",
                                  "HAV",
                                  "MST",
                                  "TLN",
                                  "LAS",
                                  "MRY",
                                  "BIM",
                                  "VPS",
                                  "PBG",
                                  "TUP",
                                  "AAR",
                                  "AAL",
                                  "NTL",
                                  "NTE",
                                  "ORF",
                                  "BLK",
                                  "BJS",
                                  "BRN",
                                  "MSP",
                                  "KTW",
                                  "RFG",
                                  "BEG",
                                  "SYR",
                                  "YXS",
                                  "YKM",
                                  "IEV",
                                  "HAM",
                                  "FLR",
                                  "MKT",
                                  "KLV",
                                  "AVP",
                                  "IPL",
                                  "QRB",
                                  "XKJ",
                                  "ANK",
                                  "RIO",
                                  "SRZ",
                                  "JAX",
                                  "TEX",
                                  "RNN",
                                  "DFW",
                                  "ASP",
                                  "MOB",
                                  "PCT",
                                  "RAP",
                                  "TET",
                                  "LEJ",
                                  "CSG",
                                  "MLW",
                                  "BRF",
                                  "AAH",
                                  "LVS",
                                  "ZRH",
                                  "VRN",
                                  "MDE",
                                  "PFN",
                                  "MAN",
                                  "WJF",
                                  "TXG",
                                  "SMV",
                                  "DUG",
                                  "CLE",
                                  "YPR",
                                  "FNI",
                                  "ATP",
                                  "YSB",
                                  "LTT",
                                  "FAT",
                                  "SNI",
                                  "ERF",
                                  "NGO",
                                  "PSL",
                                  "HAD",
                                  "CEG",
                                  "CCB",
                                  "MYV",
                                  "AVX",
                                  "SUM",
                                  "UBS",
                                  "CYS",
                                  "REK",
                                  "RAL",
                                  "OKA",
                                  "TOL",
                                  "SBT",
                                  "MAF",
                                  "AGH",
                                  "KCK",
                                  "BGO",
                                  "LSI",
                                  "EBJ",
                                  "GLA",
                                  "KUL",
                                  "ORL",
                                  "KUV",
                                  "SGI",
                                  "SKE",
                                  "PBI",
                                  "DLH",
                                  "ANC",
                                  "SPK",
                                  "DMM",
                                  "KYN",
                                  "MSQ",
                                  "LYS",
                                  "YOW",
                                  "ALB",
                                  "HEL",
                                  "MTE",
                                  "TLS",
                                  "LCH",
                                  "SAC",
                                  "YTO",
                                  "BET",
                                  "EAP",
                                  "TPH",
                                  "BTS",
                                  "SHA",
                                  "HPN",
                                  "JSO",
                                  "RVK",
                                  "DNN",
                                  "FYV",
                                  "DRB",
                                  "YHZ",
                                  "SXB",
                                  "HOU",
                                  "ADL",
                                  "MES",
                                  "LHW",
                                  "ABI",
                                  "COI",
                                  "ETZ",
                                  "AUW",
                                  "OSL",
                                  "BAU",
                                  "FWA",
                                  "MUC",
                                  "REZ",
                                  "AUH",
                                  "PTY",
                                  "ELP",
                                  "LGG",
                                  "TPE",
                                  "GSP",
                                  "HVN",
                                  "ODM",
                                  "NOU",
                                  "LLU",
                                  "MOT",
                                  "HCA",
                                  "CRP",
                                  "HTS",
                                  "APL",
                                  "CAE",
                                  "NYC",
                                  "LBA",
                                  "PAR",
                                  "EDI",
                                  "LBB",
                                  "FKB",
                                  "BLZ",
                                  "HUC",
                                  "NAS",
                                  "CDV",
                                  "SYD",
                                  "CLT",
                                  "OXF",
                                  "TSN",
                                  "BOD",
                                  "OPO",
                                  "RTM",
                                  "LNS",
                                  "KRS",
                                  "HAG",
                                  "AMS",
                                  "PIS",
                                  "SZZ",
                                  "MCE",
                                  "CPH",
                                  "YJA",
                                  "HSV",
                                  "SSX",
                                  "BAK",
                                  "BER",
                                  "BCN",
                                  "MSC",
                                  "BHX",
                                  "OLO",
                                  "YAO",
                                  "NGB",
                                  "ROM",
                                  "OSA",
                                  "MOW",
                                  "ABZ",
                                  "TUF",
                                  "YXE",
                                  "LTS",
                                  "MNL",
                                  "MCN",
                                  "DUR",
                                  "FRD",
                                  "ICT",
                                  "DND",
                                  "ENS",
                                  "EIN",
                                  "TOP",
                                  "TMP",
                                  "BZE",
                                  "BHZ",
                                  "OGD",
                                  "YVR",
                                  "STT",
                                  "PUB",
                                  "TRD",
                                  "MKC",
                                  "UWE",
                                  "LCJ",
                                  "GVX",
                                  "RST",
                                  "GAO",
                                  "YBL",
                                  "ADW",
                                  "YQD",
                                  "COS",
                                  "SDZ",
                                  "SIN",
                                  "NRK",
                                  "YAZ",
                                  "UCA",
                                  "ATL",
                                  "BKK",
                                  "CVO",
                                  "TPA",
                                  "THA",
                                  "RNS",
                                  "ANE",
                                  "ANB",
                                  "GTF",
                                  "CMB",
                                  "CHA",
                                  "FRA",
                                  "BUE",
                                  "VEJ",
                                  "BPT",
                                  "SDL",
                                  "ROC",
                                  "DEN",
                                  "DRO",
                                  "SPS",
                                  "UTC",
                                  "YYQ",
                                  "MME",
                                  "ZRX",
                                  "SFY",
                                  "SQW",
                                  "GOT",
                                  "STL",
                                  "SDQ",
                                  "LAR",
                                  "RDD",
                                  "PRY",
                                  "ODE",
                                  "YQM",
                                  "LIL",
                                  "CGN",
                                  "LED",
                                  "IZM",
                                  "SEL",
                                  "SMX",
                                  "IST",
                                  "OMA",
                                  "SIC",
                                  "FLL",
                                  "DGM",
                                  "CSM",
                                  "SUN",
                                  "ORN",
                                  "JHE",
                                  "SXL",
                                  "KSU",
                                  "RIC",
                                  "OKC",
                                  "LIS",
                                  "BUH",
                                  "EYW",
                                  "NBU",
                                  "MYR",
                                  "BUF",
                                  "BZO",
                                  "SDF",
                                  "MAD",
                                  "SGD",
                                  "PRG",
                                  "VIE",
                                  "DOM",
                                  "NUE",
                                  "CVG",
                                  "YEA",
                                  "WBU",
                                  "KIN",
                                  "MGE",
                                  "AMA",
                                  "TCI",
                                  "BRU",
                                  "NFL",
                                  "GUT",
                                  "HOB",
                                  "KTN",
                                  "PNS",
                                  "AIY",
                                  "BOS",
                                  "MIL",
                                  "YGP",
                                  "FIH",
                                  "LAX",
                                  "SCH",
                                  "TWB",
                                  "HIJ",
                                  "STX",
                                  "SIA",
                                  "YQG",
                                  "DOH",
                                  "WAS",
                                  "ACT",
                                  "AMM",
                                  "SZD",
                                  "JAN",
                                  "MLE",
                                  "CFB",
                                  "TUS",
                                  "MCM",
                                  "SAT",
                                  "GST",
                                  "CMH",
                                  "LUL",
                                  "STR",
                                  "LPL",
                                  "PHC",
                                  "JNB",
                                  "HGH",
                                  "FNL",
                                  "MKE",
                                  "ULY",
                                  "AVN",
                                  "KCL",
                                  "SCL",
                                  "YVA",
                                  "HRT",
                                  "HKA",
                                  "SWP",
                                  "EIS",
                                  "BFS",
                                  "PTB",
                                  "YWG",
                                  "STO",
                                  "CEQ",
                                  "EGE",
                                  "STC",
                                  "BXN",
                                  "SAV",
                                  "DRS",
                                  "VRO",
                                  "UTI",
                                  "KID",
                                  "THR",
                                  "JKT",
                                  "WDH",
                                  "YQR",
                                  "BWI",
                                  "SKS",
                                  "INV",
                                  "YXU",
                                  "SHV",
                                  "PIT",
                                  "TUL",
                                  "OBN",
                                  "LON",
                                  "MPL",
                                  "MEL",
                                  "MGR",
                                  "ADM",
                                  "ESK",
                                  "YZR",
                                  "BTR",
                                  "LEX",
                                  "NBO",
                                  "SCN",
                                  "APW",
                                  "XWF",
                                  "XAK",
                                  "DXB",
                                  "CRK",
                                  "PYK",
                                  "DTT",
                                  "YGK",
                                  "SVG",
                                  "OSR",
                                  "BRW",
                                  "CAS",
                                  "PHL",
                                  "CDI",
                                  "SJC",
                                  "MTY",
                                  "ADA",
                                  "SSM",
                                  "CAX",
                                  "MSY",
                                  "ABY",
                                  "ALM",
                                  "SJO",
                                  "PHF",
                                  "NCL",
                                  "MKO",
                                  "HNL",
                                  "MNZ",
                                  "GVA",
                                  "ZTX",
                                  "FAY",
                                  "SEA",
                                  "GUM",
                                  "EKT",
                                  "PBM",
                                  "UME",
                                  "MLL",

                              };
        }
    }
}
