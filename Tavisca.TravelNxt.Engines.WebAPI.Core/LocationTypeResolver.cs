﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Entity=Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class LocationTypeResolver
    {
        private const string Culture = "en-US";
        private const string DisplayCurrency = "USD";
        private const string Password = "password";
        private const string PosId = "1";
        private static readonly string AccountId = ConfigurationManager.AppSettings["RootAccountId"];

       
        private static readonly object InstanceLock = new object();
        private static volatile LocationTypeResolver _instance;
        public static LocationTypeResolver Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (InstanceLock)
                {
                    if (_instance != null)
                        return _instance;

                    return (_instance = new LocationTypeResolver());
                }
            }
        }

        public void ResolveAndSetLocation(string locationCode, SearchSegment searchSegment, LocationType locationType)
        {
            if (IsLocationCodeCity(locationCode))
                SetCityCode(locationCode, locationType, searchSegment);
            else
                SetAirportCode(locationCode, locationType, searchSegment);
        }
     

        #region helper methods

        private static void SetAirportCode(string locationCode, LocationType locationType, SearchSegment searchSegment)
        {
            if (locationType == LocationType.Arrival)
                searchSegment.ArrivalAirportCode = locationCode;
            else
                searchSegment.DepartureAirportCode = locationCode;
        }

        private static void SetCityCode(string locationCode, LocationType locationType, SearchSegment searchSegment)
        {
            if (locationType == LocationType.Arrival)
                searchSegment.ArrivalCityCode = locationCode;
            else
                searchSegment.DepartureCityCode = locationCode;
        }

        private bool IsLocationCodeCity(string locationCode)
        {
            if (string.IsNullOrWhiteSpace(locationCode))
                return false;

            ContentManager contentManager = new ContentManager();
            List<Entity.City> lstCities = contentManager.GetCitiesByCode(locationCode);
            if (lstCities != null && lstCities.Count > 0)
                return true;

            return false;
        }

        #endregion
    }

    public enum LocationType
    {
        Departure,
        Arrival
    }
}
