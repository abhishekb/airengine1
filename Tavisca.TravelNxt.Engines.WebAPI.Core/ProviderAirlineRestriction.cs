﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class ProviderAirlineRestriction : IResultTransform
    {
        private const string SettingKey = "FareSourceRestrictedAirlineCodes";
        private readonly ConcurrentDictionary<string, List<string>> _fareSourceSupportedAirlineCodes;

        public ProviderAirlineRestriction()
        {
            var setting = ConfigurationManager.AppSettings[SettingKey];

            if (string.IsNullOrWhiteSpace(setting))
                return;

            var splitValues = setting.Split('|');
            if (!splitValues.Any())
                return;

            _fareSourceSupportedAirlineCodes = GetParsedSettings(splitValues.Where(x => !string.IsNullOrWhiteSpace(x)));
        }

        public FlightSearchRS GetTransformedResponse(FlightSearchRS originalResponse)
        {
            if (_fareSourceSupportedAirlineCodes == null || _fareSourceSupportedAirlineCodes.Count == 0)
                return originalResponse;

            if (AreRecommendationsPresentInResponse(originalResponse) == false)
                return originalResponse;

            return GetFilteredResponse(originalResponse);
        }

        private FlightSearchRS GetFilteredResponse(FlightSearchRS response)
        {
            response.LegRecommendations = GetRecommendationsToKeep(response.LegRecommendations, response);

            response.ItineraryRecommendations = GetRecommendationsToKeep(response.ItineraryRecommendations, response);

            return response;
        }

        private List<FlightRecommendation> GetRecommendationsToKeep(List<FlightRecommendation> recommendations, FlightSearchRS response)
        {
            if (recommendations == null || recommendations.Count == 0)
                return recommendations;

            var recommendationsToKeep = new List<FlightRecommendation>();
            foreach (var recommendation in recommendations)
            {
                var providerAirlineInfo = GetProviderAirlineInfo(recommendation, response);
                //Null info implies check not being applicable...so don't filter
                if (providerAirlineInfo == null || DoesProviderMatchCriteria(providerAirlineInfo))
                    recommendationsToKeep.Add(recommendation);
            }

            return recommendationsToKeep;
        }

        private bool DoesProviderMatchCriteria(ProviderAirlineInfo providerAirlineInfo)
        {
            List<string> airlinesToCheck;
            if (!_fareSourceSupportedAirlineCodes.TryGetValue(providerAirlineInfo.ProviderId, out airlinesToCheck))
                return true;

            return providerAirlineInfo.MarketingAirlines.Exists(ma => !airlinesToCheck.Contains(ma));
        }

        private static ConcurrentDictionary<string, List<string>> GetParsedSettings(IEnumerable<string> settingCollection)
        {
            var fareSourceSupportedAirlineCodes = new ConcurrentDictionary<string, List<string>>();

            foreach (var setting in settingCollection)
            {
                var settingParts = setting.Split('-');
                if (settingParts.Count() != 2)
                    continue;

                fareSourceSupportedAirlineCodes.TryAdd(settingParts[0],
                    settingParts[1].Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList());
            }

            return fareSourceSupportedAirlineCodes;
        }

        private static bool AreRecommendationsPresentInResponse(FlightSearchRS response)
        {
            return (response.LegRecommendations != null && response.LegRecommendations.Count > 0) ||
                   (response.ItineraryRecommendations != null && response.ItineraryRecommendations.Count > 0);
        }

        private static ProviderAirlineInfo GetProviderAirlineInfo(FlightRecommendation recommendation,
            FlightSearchRS response)
        {
            if (recommendation.LegRefs == null || recommendation.LegRefs.Count == 0)
                return null;

            var retVal = new ProviderAirlineInfo();

            var legs = GetFlightLegs(recommendation, response);

            //Invalid for the context of this filter
            if (legs.Select(x => x.ProviderSpaceRefId).Distinct().Count() > 1)
                return null;

            retVal.ProviderId =
                response.ProviderSpaces[legs.First().ProviderSpaceRefId].ID.ToString(CultureInfo.InvariantCulture);

            var segmentIds = legs.SelectMany(leg => leg.FlightSegmentRefs).ToList();
            retVal.MarketingAirlines.AddRange(
                response.Segments.Where(segment => segmentIds.Contains(segment.RefId))
                    .Select(segment => segment.MarketingAirlineCode).Distinct());

            return retVal;
        }

        private static List<FlightLeg> GetFlightLegs(FlightRecommendation recommendation, FlightSearchRS response)
        {
            var legs = new List<FlightLeg>();
            foreach (var legRef in recommendation.LegRefs)
            {
                var splitValue = legRef.Split('/');
                legs.Add(
                    response.LegAlternates.First(alternate => alternate.LegIndex == int.Parse(splitValue[0]))
                        .Legs.First(leg => leg.RefID == int.Parse(splitValue[1])));
            }

            return legs;
        }

        private class ProviderAirlineInfo
        {
            public ProviderAirlineInfo()
            {
                MarketingAirlines = new List<string>();
            }

            public string ProviderId { get; set; }

            public List<string> MarketingAirlines { get; set; }
        }
    }
}
