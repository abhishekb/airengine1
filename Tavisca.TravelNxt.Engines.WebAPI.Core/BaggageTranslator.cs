﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public static class BaggageTranslator
    {

        public static List<Baggage> ToDataContract(List<Flight.Avail.DataContract.Baggage> baggages)
        {
            List<Baggage> wrapperBaggage = new List<Baggage>();

            if (baggages != null && baggages.Count>0)
            {
                foreach (var baggage in baggages)
                {
                    if (baggage != null)
                    {
                        wrapperBaggage.Add(ToDataContract(baggage));                     
                    }
                }
            }

            return wrapperBaggage;
        }


        private static Baggage ToDataContract(Flight.Avail.DataContract.Baggage baggage)
        {
            return new Baggage
            {
                BaggageCode = baggage.BaggageCode,
                Description = baggage.Description,
                WeightInPounds = baggage.WeightInPounds
            };
        }
    }
}
