﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Tavisca.Frameworks.Caching;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Engines.WebAPI.Core.CulturePlugin;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions;
using Tavisca.TravelNxt.Engines.WebAPI.FaultContract;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class ClientConfiguration
    {
        public string Uid { get; set; }
       
        public string PosId { get; set; }

        public string DK { get; set; }

        public bool IsEnabled { get; set; }

        public string Culture { get; set; }

        public string CID { get; set; }

        public string CurrencyCode { get; set; }

        public void LoadConfiguration()
        {
            string uid = "testHappy";
            string cacheId = string.Empty;
            try
            {
                if (WebOperationContext.Current != null && WebOperationContext.Current.IncomingRequest.UriTemplateMatch != null)
                {
                    NameValueCollection valueCollection =
                        WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters;
                    foreach (var ele in valueCollection.AllKeys)
                    {
                        if (String.Compare(ele, "uid", StringComparison.InvariantCultureIgnoreCase) == 0)
                        {
                            uid = valueCollection[ele];
                        }
                        else if (String.Compare(ele, "cid", StringComparison.InvariantCultureIgnoreCase) == 0)
                        {
                            cacheId = valueCollection[ele];
                            Guid sessionId = Guid.Empty;
                            Guid.TryParse(cacheId, out sessionId);
                            if (sessionId == Guid.Empty)
                                cacheId = Guid.NewGuid().ToString();
                        }
                    }
                }

                if (string.IsNullOrEmpty(cacheId))
                    cacheId = Guid.NewGuid().ToString();
                else
                {
                    Guid sessionId = Guid.Empty;
                    Guid.TryParse(cacheId, out sessionId);
                    if (sessionId == Guid.Empty)
                        cacheId = Guid.NewGuid().ToString();
                }
                CID = cacheId;

                if (!string.IsNullOrEmpty(uid))
                    SetPosDKByUID(uid);
                Uid = uid;

            }
            catch (FaultException<ApplicationFault>)
            {
                throw;
            }
            catch (Exception)
            {
                throw new FaultException<ApplicationFault>(new ApplicationFault
                {
                    ErrorCode = ErrorCodes.UNKNOWN_ERROR,
                    ErrorMessage =
                        "Unknown error occurred. Contact support."

                });
            }          
        }

        public void LoadConfiguration(string uid, string cacheId)
        {           
            try
            {

                if (string.IsNullOrEmpty(cacheId))
                    cacheId = Guid.NewGuid().ToString();
                else
                {
                    Guid sessionId = Guid.Empty;
                    Guid.TryParse(cacheId, out sessionId);
                    if (sessionId == Guid.Empty)
                        cacheId = Guid.NewGuid().ToString();
                }
                CID = cacheId;

                if (!string.IsNullOrEmpty(uid))
                    SetPosDKByUID(uid);
                Uid = uid;
            }
            catch (FaultException<ApplicationFault>)
            {
                throw;
            }
            catch (Exception)
            {
                throw new FaultException<ApplicationFault>(new ApplicationFault
                {
                    ErrorCode = ErrorCodes.UNKNOWN_ERROR,
                    ErrorMessage =
                        "Unknown error occurred. Contact support."

                });
            }
        }

        public void ResolveCulture(string cul,string curr)
        {
            CultureInfo cultureInfo = new CultureInfo(Constants.DefaultCultureName, Constants.DefaultCurrencyCode);
            bool foundInHeader=false;
            int headerIndex = OperationContext.Current.IncomingMessageHeaders.FindHeader(Constants.HeaderName, Constants.HeaderNameSpace);
            if (headerIndex >= 0)
            {
                cultureInfo = OperationContext.Current.IncomingMessageHeaders.GetHeader<CultureInfo>(headerIndex);
                if (cultureInfo != null)
                    foundInHeader = true;
            }

            if (foundInHeader == false)
            {
                              if (!string.IsNullOrEmpty(cul))
                    cultureInfo.CultureName = cul;
                if (!string.IsNullOrEmpty(curr))
                   cultureInfo.CurrencyCode = curr;
            }
            Culture = cultureInfo.CultureName;
            CurrencyCode = cultureInfo.CurrencyCode;
        }

        private static List<UIDMapping> LoadXML()
        {
            string xmlPath = string.Concat(AppDomain.CurrentDomain.BaseDirectory, "UIDMappingFile.xml");
            XDocument doc = XDocument.Load(xmlPath);
            List<UIDMapping> lstUIDMapping = new List<UIDMapping>();

            var result = (from e in doc.Elements("members").Elements("member")
                               select e);

            if (result != null)
            {
                foreach (var member in result)
                {
                    UIDMapping uidMap = new UIDMapping();
                    uidMap.UID = member.Attribute("uid").Value;
                    uidMap.PosID=member.Attribute("posid").Value;
                    uidMap.DK = member.Attribute("dk").Value;
                    lstUIDMapping.Add(uidMap);
                }
            }
            return lstUIDMapping;            
        }

        private static List<UIDMapping> _lstUidMapping = null;
        public static List<UIDMapping> UIDMappingList
        {
            get
            {
                if (_lstUidMapping != null)
                    return _lstUidMapping;
                _lstUidMapping = LoadXML();
                return _lstUidMapping;
            }
        }
    

        public void SetPosDKByUID(string uid)
        {
            List<UIDMapping> lstUIDMapping=null;
            var uidElement=UIDMappingList.Find(x => x.UID.ToLower().Equals(uid.ToLower()));

            if (uidElement != null && !string.IsNullOrWhiteSpace(uidElement.PosID) && !string.IsNullOrEmpty(uidElement.DK))
            {
                PosId = uidElement.PosID;
                DK = uidElement.DK;
            }
            else
            {
                throw new FaultException<ApplicationFault>(new ApplicationFault
                {
                    ErrorCode = ErrorCodes.ARGUMENT_INVALID,
                    ErrorMessage =
                        "The UID: " +
                        uid +
                        " is not valid."
                });
            }
        }     
    }

    public class UIDMapping
    {
        public string UID { get; set; }

        public string PosID { get; set; }

        public string DK { get; set; }
    }
}
