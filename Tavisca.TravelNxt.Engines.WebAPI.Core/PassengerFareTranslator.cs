﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class PassengerFareTranslator
    {
        public static List<AirPassengerFare> ToWebApi(List<Flight.Avail.DataContract.PassengerFare> oskiPassengerFares)
        {
            List<AirPassengerFare> webApiPassengerFares = new List<AirPassengerFare>();

            if (oskiPassengerFares != null && oskiPassengerFares.Count > 0)
            {
                foreach (var oskiPassengerFare in oskiPassengerFares)
                {
                    if (oskiPassengerFare != null)
                    {
                        webApiPassengerFares.Add(GetPassengerFare(oskiPassengerFare));
                    }
                }
            }

            return webApiPassengerFares;
        }

        private static AirPassengerFare GetPassengerFare(Flight.Avail.DataContract.PassengerFare oskiPassengerFare)
        {
            AirPassengerFare passengersFare = new AirPassengerFare();

            if (oskiPassengerFare != null)
            {
                passengersFare = new AirPassengerFare()
                {
                    Quantity = oskiPassengerFare.Quantity,
                    AirPassengerType = (AirPassengerType)(oskiPassengerFare.PassengerType),
                    Commissions = FareComponentTranslator.ToWebApi(oskiPassengerFare.Commissions),
                    Discounts = FareComponentTranslator.ToWebApi(oskiPassengerFare.Discounts),
                    Fees = FareComponentTranslator.ToWebApi(oskiPassengerFare.Fees),
                    Taxes = FareComponentTranslator.ToWebApi(oskiPassengerFare.Taxes),
                    TotalAmount = oskiPassengerFare.TotalAmount.DisplayAmount * oskiPassengerFare.Quantity,
                    BaseAmount = oskiPassengerFare.BaseAmount.DisplayAmount
                };
            }

            return passengersFare;
        }
    }
}
