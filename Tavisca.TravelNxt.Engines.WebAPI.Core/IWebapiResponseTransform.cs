﻿using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public interface IWebapiResponseTransform
    {
        AirAvailRs GetTransformedResponse(AirAvailRs originalResponse);
    }
}
