﻿using System.Collections.Generic;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
  public class Transformer
    {
        public static FlightSearchRS ApplyTransforms(FlightSearchRS response)
        {
            IEnumerable<IResultTransform> transforms = null;

            try
            {
                transforms = RuntimeContext.Resolver.ResolveAll<IResultTransform>();
            }
            catch
            { }

            if (transforms == null)
                return response;

            foreach (var resultTransform in transforms)
                response = resultTransform.GetTransformedResponse(response);

            return response;
        }

        public static AirAvailRs ApplyTransforms(AirAvailRs response)
        {
            IEnumerable<IWebapiResponseTransform> transforms = null;

            try
            {
                transforms = RuntimeContext.Resolver.ResolveAll<IWebapiResponseTransform>();
            }
            catch
            { }

            if (transforms == null)
                return response;

            foreach (var resultTransform in transforms)
                response = resultTransform.GetTransformedResponse(response);

            return response;
        }
    }
}
