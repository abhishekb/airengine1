﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class FlightRefTranslator
    {
        public static List<FlightRef> TranslateToBAF(List<DataContracts.FlightRef> oskiFlightRefs)
        {
            List<FlightRef> flightRefs = new List<FlightRef>();

            if (oskiFlightRefs != null)
            {
                foreach (var oskiFlightRef in oskiFlightRefs)
                {
                    flightRefs.Add(TranslateToBAF(oskiFlightRef));
                }                
            }

            return flightRefs;
        }

        public static FlightRef TranslateToBAF(DataContracts.FlightRef oskiFlightRef)
        {
            FlightRef flightRef = new FlightRef();

            if (oskiFlightRef != null)
            {
                flightRef = new FlightRef
                {
                    Cabin = oskiFlightRef.Cabin,
                    FareClass = oskiFlightRef.FareClass,
                    FlightKey = oskiFlightRef.FlightKey,
                    LegNum = oskiFlightRef.LegNum
                };
            }

            return flightRef;
        }    
    }
}
