﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class TicketTypeTranslator
    {
        public static TicketType TranslateToBAF(DataContracts.TicketType oskiTicketType)
        {            
            switch (oskiTicketType)
            {
                case DataContracts.TicketType.E:
                    return TicketType.E;

                case DataContracts.TicketType.P:
                    return TicketType.P;                    
            }

            return default(TicketType);
        }
    }
}
