﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class BaggagesTranslator
    {
        public static List<Baggage> TransalateToBAF(List<DataContracts.Baggage> oskiBaggages)
        {
            List<Baggage> baggages = new List<Baggage>();

            if (oskiBaggages != null)
            {
                foreach (var oskiBaggage in oskiBaggages)
                {
                    baggages.Add(TranslateToBAF(oskiBaggage));
                }
            }

            return baggages;
        }

        public static Baggage TranslateToBAF(DataContracts.Baggage oskiBaggage)
        {
            Baggage airOption = new Baggage();

            if (oskiBaggage != null)
            {
                airOption = new Baggage
                {
                    BaggageCode = oskiBaggage.BaggageCode,
                    Description = oskiBaggage.Description,
                    Quantity = oskiBaggage.Quantity,
                    WeightInPounds = oskiBaggage.WeightInPounds,                    
                };
            }

            return airOption;
        }
    }
}
