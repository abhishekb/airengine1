﻿using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class FareTranslator
    {
        public static Fare TranslateToBAF(DataContracts.Common.Fare oskiFare)
        {
            Fare fare = new Fare();

            if (oskiFare != null)
            {
                fare = new Fare
                {
                    BaseFare = oskiFare.BaseFare,
                    Commission = oskiFare.Commission,
                    Discount = oskiFare.Discount,
                    FeesPlusTaxes = oskiFare.FeesPlusTaxes,
                    TotalFare = oskiFare.TotalFare
                };
            }

            return fare;
        }
    }
}
