﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public class AirAvailRsTranslator
    {
        public static AirAvailRs TranslateToBaf(DataContracts.AirAvailRs oskiAirAvailRs)
        {
            AirAvailRs airAvailRs = new AirAvailRs();

            if (oskiAirAvailRs != null)
            {
                airAvailRs.AirOptions = AirOptionsTranslator.TranslateToBAF(oskiAirAvailRs.AirOptions);

                //airAvailRs.Baggages = BaggagesTranslator.TransalateToBAF(oskiAirAvailRs.Baggages);

                airAvailRs.Segments = SegmentsTranslator.TranslateToBAF(oskiAirAvailRs.Segments);

                airAvailRs.CacheId = oskiAirAvailRs.CacheId;

                airAvailRs.Culture = oskiAirAvailRs.Culture;

                airAvailRs.CurrencyCode = oskiAirAvailRs.CurrencyCode;

                //airAvailRs.Error = ErrorTranslator.TransalateToBAF(oskiAirAvailRs.Error);

                airAvailRs.NumResults = oskiAirAvailRs.NumResults;                
            }

            return airAvailRs;
        }
    }
}
