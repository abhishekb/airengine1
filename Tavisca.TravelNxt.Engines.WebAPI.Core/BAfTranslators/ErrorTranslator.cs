﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public class ErrorTranslator
    {
        public static Error TransalateToBAF(DataContracts.Common.Error oskiError)
        {
            Error error = new Error();

            if (oskiError != null)
            {
                error = new Error
                {
                    ErrorCode = oskiError.ErrorCode,
                    ErrorMessage = oskiError.ErrorMessage
                };
            }

            return error;
        }
    }
}
