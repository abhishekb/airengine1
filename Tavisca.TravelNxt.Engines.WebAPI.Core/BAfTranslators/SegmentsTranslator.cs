﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class SegmentsTranslator
    {
        public static List<DataContracts.BAF.Flight> TranslateToBAF(List<DataContracts.Flight> oskiFlights)
        {
            List <DataContracts.BAF.Flight> flights = new List<DataContracts.BAF.Flight>();

            if (oskiFlights != null)
            {
                foreach (var oskiFlight in oskiFlights)
                {
                    flights.Add(TranslateToBAF(oskiFlight));
                }
            }

            return flights;
        }

        public static DataContracts.BAF.Flight TranslateToBAF(DataContracts.Flight oskiFlight)
        {
            DataContracts.BAF.Flight flight = new DataContracts.BAF.Flight();

            if (oskiFlight != null)
            {
                flight = new DataContracts.BAF.Flight
                {
                    Aircraft = oskiFlight.Aircraft,
                    Airline = oskiFlight.Airline,
                    AirlineCode = oskiFlight.AirlineCode,
                    Arrive = oskiFlight.Arrive,
                    Depart = oskiFlight.Depart,
                    DurationInMin = oskiFlight.DurationInMin,
                    FlightNumber = oskiFlight.FlightNumber,
                    FromAirport = oskiFlight.FromAirport,
                    Key = oskiFlight.Key,
                    NumOfStops = oskiFlight.NumOfStops,
                    //OperatingAirlineCode = oskiFlight.OperatingAirlineCode,
                    ToAirport = oskiFlight.ToAirport,                    
                };
            }

            return flight;
        }
    }
}
