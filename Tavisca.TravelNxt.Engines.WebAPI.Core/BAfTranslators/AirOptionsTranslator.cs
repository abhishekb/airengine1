﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.BAfTranslators
{
    public static class AirOptionsTranslator
    {
        public static List<AirOption> TranslateToBAF(List<DataContracts.AirOption> oskiAirOptions)
        {
            List<AirOption> airOptions = new List<AirOption>();

            if (oskiAirOptions != null)
            {
                foreach (var oskiAirOption in oskiAirOptions)
                {
                    airOptions.Add(TranslateToBAF(oskiAirOption));
                }
            }

            return airOptions;
        }

        public static AirOption TranslateToBAF(DataContracts.AirOption oskiAirOption)
        {
            AirOption airOption = new AirOption();

            if (oskiAirOption != null)
            {
                airOption = new AirOption
                {
                    Segments = FlightRefTranslator.TranslateToBAF(oskiAirOption.Segments),
                    Fare = FareTranslator.TranslateToBAF(oskiAirOption.Fare),
                    IsRefundable = oskiAirOption.IsRefundable,
                    IsSpecial = oskiAirOption.IsSpecial,
                    //PlatingCarrier = oskiAirOption.PlatingCarrier
                    Rph = oskiAirOption.Rph,
                    Ticket = TicketTypeTranslator.TranslateToBAF(oskiAirOption.Ticket),
                };
            }

            return airOption;
        }
    }
}
