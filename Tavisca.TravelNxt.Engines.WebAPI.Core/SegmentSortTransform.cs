﻿using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core
{
    public class SegmentSortTransform : IWebapiResponseTransform
    {
        public AirAvailRs GetTransformedResponse(AirAvailRs response)
        {
            if (response == null || response.AirOptions == null ||
                response.AirOptions.Count == 0)
                return response;

            response.Segments.Sort(new FlightComparer());

            foreach (var airOption in response.AirOptions)
            {
                var optionSegments = airOption.Segments;

                var referredSegmentKeys = optionSegments.Select(x => x.FlightKey).ToList();
                var referredSegments =
                    response.Segments.Where(segment => referredSegmentKeys.Contains(segment.Key)).ToList();

                //This sort call is needed to stop relying on the filter behavior of "Where" extension
                //which currently will return the elements in sorted order due to the above sort done on all segments
                referredSegments.Sort(new FlightComparer());

                airOption.Segments = new List<FlightRef>(referredSegments.Count);

                //The referredSegments collection will already be in sorted order due to the initial sort on the entire segment collection
                foreach (var referredSegment in referredSegments)
                    airOption.Segments.Add(optionSegments.First(x => x.FlightKey.Equals(referredSegment.Key)));
            }

            return response;
        }
    }

    internal class FlightComparer : IComparer<DataContracts.Flight>
    {
        public int Compare(DataContracts.Flight x, DataContracts.Flight y)
        {
            return x.Depart.CompareTo(y.Depart);
        }
    }
}
