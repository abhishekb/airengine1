﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.Core.CulturePlugin
{
    [Serializable]
    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/Tavisca.Frameworks.CulturePlugin")]
    public class CultureInfo
    {
        public CultureInfo()
        {
        }

        public CultureInfo(string culture, string currency)
        {
            CultureName = culture;
            CurrencyCode = currency;
        }

        /// <summary>x  
        /// Name of the culture e.g en-US, de-DE
        /// </summary>
        [DataMember]
        public string CultureName
        {
            get;
            set;
        }

        /// <summary>
        /// Curreny code for the culture e.g USD, EUR
        /// </summary>
        [DataMember]
        public string CurrencyCode
        {
            get;
            set;
        }
    }
}
