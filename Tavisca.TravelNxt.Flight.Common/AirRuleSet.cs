﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Serialization;
using Tavisca.Frameworks.Serialization.Binary;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Settings;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Common
{
    [Serializable]
    [ProtoContract(ImplicitFields=ImplicitFields.AllFields)]
    public sealed class AirRuleSet
    {
        #region Instance Members

        public List<AirRule> DestinationCountryCodeRules { get; set; }
        public List<AirRule> DepartureCountryCodeRules { get; set; }
        public List<AirRule> OperatingAirlineRules { get; set; }
        public List<AirRule> MarketingAirlineRules { get; set; }
        public List<AirRule> ClassOfServiceRules { get; set; }
        public List<AirRule> CustomRules { get; set; }

        #endregion

        #region Constructors

        public AirRuleSet()
        {
            ClassOfServiceRules = new List<AirRule>();
            CustomRules = new List<AirRule>();
            DepartureCountryCodeRules = new List<AirRule>();
            DestinationCountryCodeRules = new List<AirRule>();
            MarketingAirlineRules = new List<AirRule>();
            OperatingAirlineRules = new List<AirRule>();
        }

        #endregion

        #region Factory Methods & Helpers

        public static AirRuleSet FromXML(string xml)
        {
            try
            {
                var rule = GetSerializationFacade().Deserialize<Rules>(Encoding.UTF8.GetBytes(xml));

                if (rule == null || rule.Rule == null || !rule.Rule.Any())
                    return null;

                var ruleSet = new AirRuleSet();

                var allRules = rule.Rule.Select(x => new
                    {
                        Rules = x.add.Select(y => new AirRule()
                            {
                                Alignment = y.NegativeSpecified? (y.Negative? AlignmentType.Negative : AlignmentType.Positive): AlignmentType.Positive,
                                RuleType = ToRuleType(x.Type),
                                Value = y.value,
                                OriginalType = x.Type
                            })
                    }).SelectMany(x => x.Rules).GroupBy(x => x.RuleType).ToListBuffered();

                var classOfService = allRules.FirstOrDefault(x => x.Key == AirRuleType.ClassOfService);

                if (classOfService != null)
                    ruleSet.ClassOfServiceRules = classOfService.ToListBuffered();

                var depCountryCode = allRules.FirstOrDefault(x => x.Key == AirRuleType.DepartureCountryCode);

                if (depCountryCode != null)
                    ruleSet.DepartureCountryCodeRules = depCountryCode.ToListBuffered();

                var desCountryCode = allRules.FirstOrDefault(x => x.Key == AirRuleType.DestinationCountryCode);

                if (desCountryCode != null)
                    ruleSet.DestinationCountryCodeRules = desCountryCode.ToListBuffered();

                var marAirline = allRules.FirstOrDefault(x => x.Key == AirRuleType.MarketingAirline);

                if (marAirline != null)
                    ruleSet.MarketingAirlineRules = marAirline.ToListBuffered();

                var operAirline = allRules.FirstOrDefault(x => x.Key == AirRuleType.OperatingAirline);

                if (operAirline != null)
                    ruleSet.OperatingAirlineRules = operAirline.ToListBuffered();

                var custom = allRules.FirstOrDefault(x => x.Key == AirRuleType.Custom);

                if (custom != null)
                    ruleSet.CustomRules = custom.ToListBuffered();

                return ruleSet;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
            }
            
            return null;
        }

        private static AirRuleType ToRuleType(string type)
        {
            AirRuleType ruleType;

            return Enum.TryParse(type, true, out ruleType) ? ruleType : AirRuleType.Custom;
        }

        private static ISerializationFacade GetSerializationFacade()
        {
            return new XmlSerializerFacade();
        }

        #endregion
    }
}
