﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Common
{
    [Serializable]
    [ProtoContract(ImplicitFields=ImplicitFields.AllFields)]
    public enum AirRuleType: int
    {
        Custom = 0,
        DestinationCountryCode = 1,
        DepartureCountryCode = 2,
        OperatingAirline = 3,
        MarketingAirline = 4,
        ClassOfService = 5
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum AlignmentType: int
    {
        Positive = 0,
        Negative = 1
    }
}
