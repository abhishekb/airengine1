﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Common
{
    public interface IKeyedCollection<T>: ICollection<T>
    {
        string Key { get; }
        void AddRange(IEnumerable<T> items);
    }
}
