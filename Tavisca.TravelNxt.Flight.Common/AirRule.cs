using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Common
{
    [Serializable]
    [ProtoContract(ImplicitFields=ImplicitFields.AllFields)]
    public sealed class AirRule
    {
        public AirRuleType RuleType { get; set; }
        public AlignmentType Alignment { get; set; }
        public string OriginalType { get; set; }
        public string Value { get; set; }
    }
}