﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.FareRules;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.FareRules.Core
{
    public class FareRulesProviderStrategy : IFareRulesProviderStrategy
    {
        #region IFareRulesProviderStrategy Members

        public FareRulesRS GetFareRules(FareRulesRQ request, FlightRecommendation recommendation)
        {
            FareRulesRS response;
            try
            {
                IFareRulesProvider provider;

                if (request.AdditionalInfo != null &&
                    request.AdditionalInfo.ContainsKey(KeyStore.MockKeys.Mock))
                {
                    provider = RuntimeContext.Resolver.Resolve<IFareRulesProvider>(KeyStore.SingularityNameKeys.FareRulesMockDefaultProvider);
                }
                else
                {
                    var configuration =
                        recommendation.ProviderSpace.Configurations.FirstOrDefault(
                            x => x.ConfigType == ConfigurationType.AirFareRules);

                    if (configuration == null)
                        throw new InvalidDataException("Fare source configuration not found for fare rules, fare source: " +
                            recommendation.ProviderSpace.ID.ToString(CultureInfo.InvariantCulture));

                    var providerLocator = string.IsNullOrWhiteSpace(configuration.ContractVersion)
                                              ? KeyStore.SingularityNameKeys.FareRulesDefaultProvider
                                              : configuration.ContractVersion;

                    provider = RuntimeContext.Resolver.Resolve<IFareRulesProvider>(providerLocator);
                }

                response = provider.GetFareRules(request, recommendation);
            }
            catch (HandledException)
            {
                throw;
            }
            catch(Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }

            return response;
        }

        #endregion
    }
}
