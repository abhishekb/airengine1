﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Session;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Entities.FareRules;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.FareRules.Core
{
    public class FareRulesEngine : IFareRulesEngine
    {
        #region IFareRulesEngine Members

        public FareRulesRS GetFareRules(FareRulesRQ request)
        {
            FareRulesRS result;
            try
            {
                var providerStrategy = RuntimeContext.Resolver.Resolve<IFareRulesProviderStrategy>();

                var recommendation = GetRecommendationFromSession(request);

                if(recommendation == null)
                    throw new InvalidDataException("request is invalid or session has expired, no corresponding data found.");

                result = providerStrategy.GetFareRules(request, recommendation);
            }
            catch(HandledException)
            {
                throw;
            }
            catch(Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }

            result.SessionId = GetSessionId();

            return result;
        }

        #endregion

        #region Protected Members

        protected virtual FlightRecommendation GetRecommendationFromSession(FareRulesRQ request)
        {
            Utility.LogDiagnosticEntry("Get from session store - start");
            var result = GetSessionStore()
                .Get<FlightAvailResult>(KeyStore.APISessionKeys.FlightSeachCategory, GetSessionId().ToString());
            Utility.LogDiagnosticEntry("Get from session store - end");
            if (result != null)
            {
                var recommendation = result.FlightRecommendations.FirstOrDefault(x => x.RefId == request.RecommendationRefID);

                return recommendation;
            }

            return null;
        }

        protected Guid GetSessionId()
        {
            var context = Utility.GetCurrentContext();

            return context.SessionId;
        }

        protected SessionStore GetSessionStore()
        {
            return Utility.GetSessionProvider();
        }

        #endregion
    }
}
