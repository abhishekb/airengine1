﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.FareRules.Core
{
    public class MockFareRulesProvider: FareRulesProvider
    {
        protected override string LogEntryTitle
        {
            get
            {
                return "Fare rules Call to mock supplier: {0}";
            }
        }

        protected override Data.Services.FareRules.FareRulesRS GetResponse(Data.Services.FareRules.AirFareRulesRQ request, Entities.ProviderSpace providerSpace)
        {
            throw new NotImplementedException();
        }
    }
}
