﻿using System;
namespace Tavisca.TravelNxt.Flight.FareRules.Core.Exceptions
{
    [Serializable]
    public class FareRuleSupplierException : Exception
    {
        public FareRuleSupplierException()
        { }

        public FareRuleSupplierException(string message) : base(message)
        { }

        public FareRuleSupplierException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
