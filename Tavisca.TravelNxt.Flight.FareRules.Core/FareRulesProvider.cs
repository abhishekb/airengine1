﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.FareRules;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.FareRules;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.FareRules.Core.Exceptions;
using Tavisca.TravelNxt.Flight.Settings;
using CabinType = Tavisca.TravelNxt.Flight.Entities.CabinType;
using FareRulesRQ = Tavisca.TravelNxt.Flight.Entities.FareRules.FareRulesRQ;
using FareRulesRS = Tavisca.TravelNxt.Flight.Entities.FareRules.FareRulesRS;
using FlightSegment = Tavisca.TravelNxt.Flight.Data.Services.FareRules.FlightSegment;
using Requester = Tavisca.TravelNxt.Flight.Data.Services.FareRules.Requester;

namespace Tavisca.TravelNxt.Flight.FareRules.Core
{
    public class FareRulesProvider : IFareRulesProvider
    {
        protected virtual string LogEntryTitle { get { return "Fare rules Call to supplier: {0}"; } }

        public FareRulesRS GetFareRules(FareRulesRQ request, FlightRecommendation recommendation)
        {
            var eventEntry = Utility.GetEventEntryContextual();

            var providerSpace = recommendation.ProviderSpace;

            eventEntry.Title = string.Format(LogEntryTitle, providerSpace.Family);

            eventEntry.CallType = KeyStore.CallTypes.SupplierAirFareRules;

            var logFactory = Utility.GetLogFactory();

            var stopwatch = new WrappedTimer();
            try
            {
                eventEntry.AddAdditionalInfo(KeyStore.LogCategories.AdditionalInfoKeys.ProviderName,
                    providerSpace.Family);

                eventEntry.ProviderId = providerSpace.ID;

                Utility.LogDiagnosticEntry("Supplier request translation - start");
                var dataContract = ToDataContract(request, recommendation);
                Utility.LogDiagnosticEntry("Supplier request translation - end");

                eventEntry.RequestObject = dataContract;
                eventEntry.ReqResSerializerType = SerializerType.DataContractSerializer;

                stopwatch.Start();

                var result = GetResponse(dataContract, providerSpace);
                
                if (result != null)
                {
                    eventEntry.ResponseObject = result;
                    if (result.Status != null && result.Status.ResponseStatus == StatusType.Failure)
                        eventEntry.StatusType = StatusOptions.Failure;
                }

                Utility.LogDiagnosticEntry("Supplier response translation - start");
                var entityResponse = ToEntity(result, recommendation);
                Utility.LogDiagnosticEntry("Supplier response translation - end");

                return entityResponse;
            }
            catch (HandledException)
            {
                eventEntry.StatusType = StatusOptions.Failure;
                throw;
            }
            catch (Exception ex)
            {
                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);
                eventEntry.StatusType = StatusOptions.Failure;
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
            finally
            {
                stopwatch.Stop();
                eventEntry.TimeTaken = stopwatch.ElapsedSeconds;

                logFactory.WriteAsync(eventEntry, KeyStore.LogCategories.Core);
            }
        }

        protected virtual Data.Services.FareRules.FareRulesRS GetResponse(AirFareRulesRQ request, ProviderSpace providerSpace)
        {
            Data.Services.FareRules.FareRulesRS response;

            var configuration =
                        providerSpace.Configurations.First(
                            x => x.ConfigType == ConfigurationType.AirFareRules);

            try
            {
                using (var client = new AirFareRulesServiceV1Client(KeyStore.ClientEndpoints.FareRulesService,
                                                                    configuration.ServiceUri.ToString().TrimEnd('/') + KeyStore.ClientEndpoints.AirFareRulesServiceUriSuffix))
                {
                    client.InnerChannel.OperationTimeout =
                        TimeSpan.FromSeconds(SettingManager.SupplierThreadTimeoutInSeconds);
                    Utility.LogDiagnosticEntry("Supplier call - start");
                    response = client.GetFareRules(request);
                    Utility.LogDiagnosticEntry("Supplier call - end");
                }
            }
            catch (FaultException faultException)
            {
                var supplierException = new FareRuleSupplierException(KeyStore.ErrorMessages.SupplierRaisedException,
                                                                      faultException);

                Utility.GetLogFactory().WriteAsync(
                    supplierException.ToContextualEntry(PriorityOptions.Critical, SeverityOptions.Critical),
                    KeyStore.LogCategories.ExceptionCategories.Critical);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, supplierException);
            }
            catch (Exception ex)
            {
                var contextualEntry = ex.ToContextualEntry(PriorityOptions.Critical, SeverityOptions.Critical);
                Utility.GetLogFactory().WriteAsync(contextualEntry, KeyStore.LogCategories.ExceptionCategories.Critical);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }

            if (response == null || response.Status == null)
                throw new FareRuleSupplierException(KeyStore.ErrorMessages.InvalidSupplierResponse);

            return response;
        }

        protected static List<FareDetail> GetFareDetails(FlightRecommendation recommendation)
        {
            var fareDetails = new List<FareDetail>();
            var segmentMappings = new Dictionary<Entities.FlightSegment, FlightSegment>();

            foreach (var leg in recommendation.Legs)
            {
                for (var i = 0; i < leg.Segments.Count; i++)
                {
                    segmentMappings.Add(leg.Segments[i], ToDataContract(leg.Segments[i], leg.LegGroupNumber, i + 1));
                }
            }

            foreach (var key in segmentMappings.Keys)
            {
                var recommendationSegment = key;

                recommendationSegment.PaxTypeFareBasisCodes.ForEach(
                    paxTypeFareBasisCode =>
                    {
                        var existingFareDetail = fareDetails.FirstOrDefault(
                            fareDetail => fareDetail.Cabin == CabinTypeMapping[paxTypeFareBasisCode.CabinType] &&
                                          fareDetail.FareBasis.Equals(paxTypeFareBasisCode.FareBasisCode) &&
                                          fareDetail.ClassOfService.Equals(paxTypeFareBasisCode.ClassOfService));

                        if (existingFareDetail == null)
                            fareDetails.Add(new FareDetail()
                            {
                                ClassOfService = paxTypeFareBasisCode.ClassOfService,
                                FareBasis = paxTypeFareBasisCode.FareBasisCode,
                                Cabin = ToDataContract(paxTypeFareBasisCode.CabinType),
                                FlightSegments = new List<FlightSegment>() { segmentMappings[recommendationSegment] },
                                SupplierFareRefKey = paxTypeFareBasisCode.SupplierFareRefKey
                            });
                        else
                        {
                            if (!existingFareDetail.FlightSegments.Contains(segmentMappings[recommendationSegment]))
                                existingFareDetail.FlightSegments.Add(segmentMappings[recommendationSegment]);
                        }
                    });
            }

            if (recommendation.Legs.Count == 2 && fareDetails.Any(fareDetail => fareDetail.FlightSegments.Select(seg => seg.LegNumber).Distinct().Count() > 1))
            {
                var logger = Utility.GetLogFactory();
                var eventEntry = Utility.GetEventEntryContextual();
                eventEntry.Title = "MultipleLegsInOneFareDetailFound";
                eventEntry.CallType = KeyStore.CallTypes.MultipleLegsInOneFareDetailFound;
                eventEntry.AddMessage("Same FareDetail is applicable to segments in different legs. Please check the Request for detailed informaton.");
                eventEntry.ReqResSerializerType = SerializerType.DataContractSerializer;
                logger.WriteAsync(eventEntry, KeyStore.LogCategories.Core);
            }

            return fareDetails;
        }

        #region Translations

        #region ToContract

        public static AirFareRulesRQ ToDataContract(FareRulesRQ request, FlightRecommendation recommendation)
        {
            return new AirFareRulesRQ()
            {
                FareSpace = ToDataContract(recommendation.ProviderSpace),
                Requester = ToDataContract(recommendation.ProviderSpace.Requester),
                Culture = Utility.GetCurrentContext().Culture.Name,
                SupplierIdentifier = recommendation.ProviderSpace.Family,
                TimeOutInSeconds = SettingManager.SupplierThreadTimeoutInSeconds,
                FareDetails = GetFareDetails(recommendation)
            };
        }

        public static Data.Services.FareRules.CabinType ToDataContract(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Economy:
                    return Data.Services.FareRules.CabinType.Economy;
                case CabinType.Business:
                    return Data.Services.FareRules.CabinType.Business;
                case CabinType.First:
                    return Data.Services.FareRules.CabinType.First;
                case CabinType.PremiumEconomy:
                    return Data.Services.FareRules.CabinType.PremiumEconomy;
                case CabinType.Unknown:
                    return Data.Services.FareRules.CabinType.Unknown;
                default:
                    throw new ArgumentException(string.Format("Cabintype not supported - {0}", cabinType.ToString()),
                                                "cabinType");
            }
        }

        #endregion

        #region ToEntity

        public static FareRulesRS ToEntity(Data.Services.FareRules.FareRulesRS response, FlightRecommendation recommendation)
        {
            return new FareRulesRS()
            {
                ServiceStatus = ToEntity(response.Status),
                FareRules = ToEntity(response.FlightFareRules)
            };
        }

        public static ServiceStatus ToEntity(Status status)
        {
            var serviceStatus = new ServiceStatus()
            {
                Status = ToEntity(status.ResponseStatus),
                Messages = new List<string>()
            };

            if (status.ErrorMessages != null)
                status.ErrorMessages.ForEach(
                    message =>
                    serviceStatus.Messages.Add(string.Format(KeyStore.MessageFormats.ErrorMessage, message.Text)));

            if (status.WarningMessages != null)
                status.WarningMessages.ForEach(
                    message =>
                    serviceStatus.Messages.Add(string.Format(KeyStore.MessageFormats.WarningMessage, message.Text)));

            if (serviceStatus.Status == CallStatus.Failure)
            {
                serviceStatus.StatusCode = KeyStore.ErrorCodes.SupplierFailure;
                serviceStatus.StatusMessage = ErrorCodeManager.GetErrorMessage(KeyStore.ErrorCodes.SupplierFailure,
                                                                               KeyStore.ErrorMessages.
                                                                                   DefaultErrorMessage);
            }

            return serviceStatus;
        }

        public static CallStatus ToEntity(StatusType statusType)
        {
            switch (statusType)
            {
                case StatusType.Success:
                    return CallStatus.Success;
                case StatusType.Failure:
                    return CallStatus.Failure;
                case StatusType.Warning:
                    return CallStatus.Warning;
                default:
                    throw new ArgumentException(string.Format("Status type not supported - {0}", statusType),
                                                "statusType");
            }
        }

        public static List<FareRule> ToEntity(ICollection<FlightFareRule> flightFareRules)
        {
            if (flightFareRules == null)
                return null;

            var entity = flightFareRules.Select(x => new FareRule()
            {
                FareBasisCode = x.FareBasisCode,
                FareRuleDetails = x.FareRules.Select(y => new FareRuleDetail()
                {
                    Text = y.Text,
                    Title = y.Title
                }).ToListBuffered(x.FareRules.Count)
            });

            return entity.ToListBuffered(flightFareRules.Count);
        }

        #endregion

        #endregion

        #region private fields/methods

        private static readonly Dictionary<CabinType, Data.Services.FareRules.CabinType> CabinTypeMapping =
            GetCabinTypeMapping();

        private static Dictionary<CabinType, Data.Services.FareRules.CabinType> GetCabinTypeMapping()
        {
            var cabinTypeMapping = new Dictionary<CabinType, Data.Services.FareRules.CabinType>
                                       {
                                           {CabinType.Economy, Data.Services.FareRules.CabinType.Economy},
                                           {CabinType.PremiumEconomy, Data.Services.FareRules.CabinType.PremiumEconomy},
                                           {CabinType.First, Data.Services.FareRules.CabinType.First},
                                           {CabinType.Business, Data.Services.FareRules.CabinType.Business},
                                           {CabinType.Unknown, Data.Services.FareRules.CabinType.Unknown}
                                       };

            return cabinTypeMapping;
        }

        private static FlightSegment ToDataContract(Entities.FlightSegment flightSegment, int legGroupNumber, int segmentIndex)
        {
            if (flightSegment == null)
                return null;

            return new FlightSegment()
            {
                OperatingAirlineCode =
                    flightSegment.OperatingAirline == null ? null : flightSegment.OperatingAirline.Code,
                ArrivalDateTime = flightSegment.ArrivalDateTime,
                DepartureDateTime = flightSegment.DepartureDateTime,
                ArrivalLocation = flightSegment.ArrivalAirport.Code,
                DepartureLocation = flightSegment.DepartureAirport.Code,
                FlightNumber = flightSegment.FlightNumber,
                AirlineCode = flightSegment.MarketingAirline.Code,
                LegNumber = legGroupNumber,
                SegmentIndex = segmentIndex
            };
        }

        private static FareSpace ToDataContract(ProviderSpace providerSpace)
        {
            return new FareSpace()
            {
                Code = providerSpace.ID.ToString(),
                AdditionalParams = GetValuePairs(providerSpace.AdditionalInfo)
                //Not passed in the qualifiers as they are not needed for fare rules
            };
        }

        private static Requester ToDataContract(Entities.Requester requester)
        {
            var currentContext = Utility.GetCurrentContext();

            return new Requester()
            {
                TrackingId = currentContext.SessionId.ToString(),
                AffiliateId = requester.ID.ToString(),
                UserSessionId = currentContext.SessionId.ToString()
            };
        }

        private static List<KeyValue> GetValuePairs(AdditionalInfoDictionary additionalInfoDictionary)
        {
            if (additionalInfoDictionary == null || additionalInfoDictionary.Count == 0)
                return new List<KeyValue>();

            return
                AdditionalInfoDictionary.ToDictionary(additionalInfoDictionary).Select(
                    x => new KeyValue() { Key = x.Key, Value = x.Value }).ToListBuffered(additionalInfoDictionary.Count);
        }

        #endregion
    }
}
