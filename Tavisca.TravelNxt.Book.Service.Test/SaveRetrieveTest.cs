﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.TravelNxt.Book.Service.Test.AirBook;

namespace Tavisca.TravelNxt.Book.Service.Test
{
    [TestClass]
    public class SaveRetrieveTest: BookTestBase
    {
        [TestMethod, TestCategory("Service Tests")]
        public void SaveTest()
        {
            Save();
        }

        [TestMethod, TestCategory("Service Tests")]
        public void RetrieveTest()
        {
            using (var client = new AirBookClient())
            {
                var locator = Save();
                
                var res = client.Retrieve(BookRequestFactory.GetRetrieveRequest(locator));

                Assert.IsNotNull(res);
                Assert.IsTrue(res.Status == B_CallStatus.Success);

                Assert.IsNotNull(res.Product);
            }
        }

        private Guid Save()
        {
            using (var client = new AirBookClient())
            {
                var request = BookRequestFactory.GetSaveRequest();
                System.Threading.Thread.Sleep(3000);
                var res = client.Save(request);

                Assert.IsNotNull(res);
                Assert.IsTrue(res.Status == B_CallStatus.Success);

                Assert.IsTrue(res.RecordLocator != Guid.Empty);

                return res.RecordLocator;
            }
        }
    }
}
