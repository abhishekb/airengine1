﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Book.Service.Test.AirBook;
using Tavisca.TravelNxt.Price.Service.Test;

namespace Tavisca.TravelNxt.Book.Service.Test
{
    public class BookRequestFactory
    {
        public static AirBook.B_FlightSaveRQ GetSaveRequest()
        {
            var priceTest = new PriceTest();

            priceTest.PriceRequestTest();

            return new B_FlightSaveRQ()
                {
                    Requester = new B_Requester(),
                    FlightRecommendationInfo = new B_FlightRecommendationInfo()
                        {
                            RecommendationRefID = PriceRequestFactory.PricedRecommendations.First()
                        }
                };
        }

        public static B_FlightRetrieveRQ GetRetrieveRequest(Guid locator)
        {
            return new B_FlightRetrieveRQ()
                {
                    RecordLocator = locator,
                    Requester = new B_Requester()
                };
        }
    }
}
