﻿using System;
using System.IO;
using System.Linq;
using Tavisca.Frameworks.Session;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.SeatMap.Core
{
    public class SeatMapEngine : ISeatMapEngine
    {
        #region ISeatMapEngine Members

        public virtual SeatMapRS SeatMap(SeatMapRQ request)
        {
            const string invalidDataMessage = "request is invalid or session has expired, no corresponding data found.";

            SeatMapRS result;

            try
            {
                if (request == null)
                    throw new ArgumentNullException("request");

                var recommendation = GetRecommendationFromSession(request.RecommendationRefID);

                if (recommendation == null)
                    throw new InvalidDataException(invalidDataMessage);

                var segment =
                    recommendation.Legs.SelectMany(x => x.Segments).FirstOrDefault(x => x.Key == request.SegmentKey);

                if (segment == null)
                    throw new InvalidDataException(invalidDataMessage);

                var seatmapStrategy = RuntimeContext.Resolver.Resolve<ISeatMapProviderStrategy>();

                result = seatmapStrategy.GetSeatMap(request, segment, recommendation);
            }
            catch(HandledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }

            result.SessionId = GetSessionId();

            return result;
        }

        #endregion

        #region Protected Members

        protected virtual FlightRecommendation GetRecommendationFromSession(int refNumber)
        {
            var result = GetSessionStore()
                .Get<FlightAvailResult>(KeyStore.APISessionKeys.FlightSeachCategory, GetSessionId().ToString());

            if (result != null)
            {
                var recommendation = result.FlightRecommendations.FirstOrDefault(x => x.RefId == refNumber);

                return recommendation;
            }
            return null;
        }

        protected Guid GetSessionId()
        {
            var context = Utility.GetCurrentContext();

            return context.SessionId;
        }

        protected SessionStore GetSessionStore()
        {
            return Utility.GetSessionProvider();
        }

        #endregion
    }
}
