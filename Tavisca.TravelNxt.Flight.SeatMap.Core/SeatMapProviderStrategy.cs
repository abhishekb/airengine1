﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.SeatMap.Core
{
    public class SeatMapProviderStrategy : ISeatMapProviderStrategy
    {
        public SeatMapRS GetSeatMap(SeatMapRQ request, FlightSegment flightSegment, FlightRecommendation recommendation)
        {
            ISeatMapProvider provider;

            if (request.AdditionalInfo != null &&
                request.AdditionalInfo.ContainsKey(KeyStore.MockKeys.Mock))
            {
                provider = RuntimeContext.Resolver.Resolve<ISeatMapProvider>(KeyStore.SingularityNameKeys.SeatMapMockDefaultProvider);
            }
            else
            {
                var configuration =
                    recommendation.ProviderSpace.Configurations.FirstOrDefault(
                        x => x.ConfigType == ConfigurationType.SeatMap);

                if (configuration == null)
                    throw new InvalidDataException("Fare source configuration not found for seatmap, fare source: " +
                                                   recommendation.ProviderSpace.ID.ToString(CultureInfo.InvariantCulture));


                var providerLocator = string.IsNullOrWhiteSpace(configuration.ContractVersion)
                                          ? KeyStore.SingularityNameKeys.SeatMapDefaultProvider
                                          : configuration.ContractVersion;

                provider = RuntimeContext.Resolver.Resolve<ISeatMapProvider>(providerLocator);
            }

            var finalResponse = provider.GetSeatMap(request, flightSegment, recommendation);

            return finalResponse;
        }
    }
}
