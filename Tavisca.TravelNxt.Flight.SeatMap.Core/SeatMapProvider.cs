﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.SeatMap;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.SeatMap.Core.Exceptions;
using Tavisca.TravelNxt.Flight.Settings;
using CabinFacility = Tavisca.TravelNxt.Flight.Entities.SeatMap.CabinFacility;
using CabinFacilityLocation = Tavisca.TravelNxt.Flight.Entities.SeatMap.CabinFacilityLocation;
using CabinFacilityType = Tavisca.TravelNxt.Flight.Data.Services.SeatMap.CabinFacilityType;
using CabinType = Tavisca.TravelNxt.Flight.Data.Services.SeatMap.CabinType;
using CallStatus = Tavisca.TravelNxt.Flight.Entities.SeatMap.CallStatus;
using FlightSegment = Tavisca.TravelNxt.Flight.Entities.FlightSegment;
using RowCharacteristic = Tavisca.TravelNxt.Flight.Entities.SeatMap.RowCharacteristic;
using Seat = Tavisca.TravelNxt.Flight.Entities.SeatMap.Seat;
using SeatCharacteristic = Tavisca.TravelNxt.Flight.Data.Services.SeatMap.SeatCharacteristic;
using SeatColumn = Tavisca.TravelNxt.Flight.Entities.SeatMap.SeatColumn;
using SeatColumnType = Tavisca.TravelNxt.Flight.Entities.SeatMap.SeatColumnType;
using SeatMapRQ = Tavisca.TravelNxt.Flight.Entities.SeatMap.SeatMapRQ;
using SeatMapRS = Tavisca.TravelNxt.Flight.Entities.SeatMap.SeatMapRS;
using SeatRow = Tavisca.TravelNxt.Flight.Entities.SeatMap.SeatRow;

namespace Tavisca.TravelNxt.Flight.SeatMap.Core
{
    public class SeatMapProvider : ISeatMapProvider
    {
        protected virtual string LogEntryTitle { get { return "Seatmap Call to supplier: {0}"; } }

        public SeatMapRS GetSeatMap(SeatMapRQ request, FlightSegment flightSegment, FlightRecommendation recommendation)
        {
            var eventEntry = Utility.GetEventEntryContextual();

            var providerSpace = recommendation.ProviderSpace;

            eventEntry.Title = string.Format(LogEntryTitle, providerSpace.Family);

            eventEntry.CallType = KeyStore.CallTypes.SupplierAirSeatmap;

            var logFactory = Utility.GetLogFactory();

            var stopwatch = new WrappedTimer();
            try
            {
                eventEntry.AddAdditionalInfo(KeyStore.LogCategories.AdditionalInfoKeys.ProviderName,
                    providerSpace.Family);

                eventEntry.ProviderId = providerSpace.ID;

                Utility.LogDiagnosticEntry("Supplier request translation - start");
                var dataContract = ToDataContract(request, flightSegment, recommendation, GetSupplierFareRefKey(recommendation));
                Utility.LogDiagnosticEntry("Supplier request translation - end");

                eventEntry.RequestObject = dataContract;
                eventEntry.ReqResSerializerType = SerializerType.DataContractSerializer;

                stopwatch.Start();

                var result = GetResponse(dataContract, providerSpace);

                if (result != null)
                {
                    eventEntry.ResponseObject = result;
                    if (result.Status != null && result.Status.ResponseStatus == StatusType.Failure)
                        eventEntry.StatusType = StatusOptions.Failure;
                }

                Utility.LogDiagnosticEntry("Supplier response translation - start");
                var entityResponse = ToEntity(result, providerSpace);
                Utility.LogDiagnosticEntry("Supplier response translation - end");

                return entityResponse;
            }
            catch(HandledException)
            {
                eventEntry.StatusType = StatusOptions.Failure;
                throw;
            }
            catch (Exception ex)
            {
                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                eventEntry.StatusType = StatusOptions.Failure;
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
            finally
            {
                stopwatch.Stop();
                eventEntry.TimeTaken = stopwatch.ElapsedSeconds;

                logFactory.WriteAsync(eventEntry, KeyStore.LogCategories.Core);
            }
        }

        protected virtual Data.Services.SeatMap.SeatMapRS GetResponse(AirSeatMapRQ request, ProviderSpace providerSpace)
        {
            Data.Services.SeatMap.SeatMapRS response;

            var seatMapConfiguration =
                        providerSpace.Configurations.First(
                            x => x.ConfigType == ConfigurationType.SeatMap);

            try
            {
                using (var client = new SeatMapServiceV1Client(KeyStore.ClientEndpoints.SeatMapService,
                                                               seatMapConfiguration.ServiceUri.ToString().TrimEnd('/') + KeyStore.ClientEndpoints.AirSeatmapServiceUriSuffix))
                {
                    client.InnerChannel.OperationTimeout =
                        TimeSpan.FromSeconds(SettingManager.SupplierThreadTimeoutInSeconds);
                    Utility.LogDiagnosticEntry("Supplier call - start");
                    response = client.GetSeatMap(request);
                    Utility.LogDiagnosticEntry("Supplier call - end");
                }
            }
            catch (FaultException faultException)
            {
                var supplierException = new SeatMapSupplierException(KeyStore.ErrorMessages.SupplierRaisedException,
                                                                     faultException);

                Utility.GetLogFactory().WriteAsync(supplierException.ToContextualEntry(PriorityOptions.High),
                                                   KeyStore.LogCategories.ExceptionCategories.Regular);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, supplierException);
            }
            catch(Exception ex)
            {
                var contextualEntry = ex.ToContextualEntry(PriorityOptions.High);
                Utility.GetLogFactory().WriteAsync(contextualEntry, KeyStore.LogCategories.ExceptionCategories.Regular);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }

            if(response == null || response.Status == null)
                throw new SeatMapSupplierException(KeyStore.ErrorMessages.InvalidSupplierResponse);

            return response;
        }

        #region Translations

        #region ToContract

        public static AirSeatMapRQ ToDataContract(SeatMapRQ request, FlightSegment flightSegment, FlightRecommendation recommendation, string supplierFareRefKey)
        {
            var paxTypeFareBasis = flightSegment.PaxTypeFareBasisCodes.First();
            
            return new AirSeatMapRQ()
                       {
                           FlightSegment = new Data.Services.SeatMap.FlightSegment()
                                               {
                                                   Flight = new Data.Services.SeatMap.Flight()
                                                                {
                                                                    AircraftType = flightSegment.AircraftType,
                                                                    AirlineCode = flightSegment.MarketingAirline.Code,
                                                                    ArrivalDateTime = flightSegment.ArrivalDateTime,
                                                                    DepartureDateTime = flightSegment.DepartureDateTime,
                                                                    FlightNumber = flightSegment.FlightNumber,
                                                                    ArrivalAirportCode =
                                                                        flightSegment.ArrivalAirport.Code,
                                                                    DepartureAirportCode =
                                                                        flightSegment.DepartureAirport.Code,
                                                                    SupplierRefKey = supplierFareRefKey
                                                                },
                                                   CabinType = CabinTypeMapping[paxTypeFareBasis.CabinType],
                                                   ClassOfService = paxTypeFareBasis.ClassOfService
                                               },
                           Requester = ToDataContract(recommendation.ProviderSpace.Requester),
                           SupplierIdentifier = recommendation.ProviderSpace.Family,
                           Culture = Utility.GetCurrentContext().Culture.Name,
                           FareSpace = ToDataContract(recommendation.ProviderSpace),
                           TimeOutInSeconds = SettingManager.SupplierThreadTimeoutInSeconds
                       };
        }

        #endregion

        #region ToEntity

        public static SeatMapRS ToEntity(Data.Services.SeatMap.SeatMapRS response, ProviderSpace providerSpace)
        {
            var entity = new SeatMapRS
                             {
                                 ServiceStatus = ToEntity(response.Status)
                             };

            if (response.FlightSeatMap == null)
                return entity;

            entity.SeatMap = new Entities.SeatMap.SeatMap()
                                 {
                                     Cabins = new List<Cabin>(),
                                     ClassOfService = response.FlightSeatMap.ClassOfService,
                                     CabinType = ToEntity(response.FlightSeatMap.CabinType)
                                 };

            if (response.FlightSeatMap.Decks != null)
                response.FlightSeatMap.Decks.ForEach(
                    deck => entity.SeatMap.Cabins.Add(ToEntity(deck.Cabin, deck.DeckLocation)));

            return entity;
        }

        public static ServiceStatus ToEntity(Status status)
        {
            var serviceStatus = new ServiceStatus()
                                    {
                                        Status = ToEntity(status.ResponseStatus),
                                        Messages = new List<string>()
                                    };

            if (status.ErrorMessages != null)
                status.ErrorMessages.ForEach(
                    message =>
                    serviceStatus.Messages.Add(string.Format(KeyStore.MessageFormats.ErrorMessage, message.Text)));

            if (status.WarningMessages != null)
                status.WarningMessages.ForEach(
                    message =>
                    serviceStatus.Messages.Add(string.Format(KeyStore.MessageFormats.WarningMessage, message.Text)));

            if(serviceStatus.Status == CallStatus.Failure)
            {
                serviceStatus.StatusCode = KeyStore.ErrorCodes.SupplierFailure;
                serviceStatus.StatusMessage = ErrorCodeManager.GetErrorMessage(KeyStore.ErrorCodes.SupplierFailure,
                                                                               KeyStore.ErrorMessages.
                                                                                   DefaultErrorMessage);
            }

            return serviceStatus;
        }

        public static CallStatus ToEntity(StatusType statusType)
        {
            switch (statusType)
            {
                case StatusType.Success:
                    return CallStatus.Success;
                case StatusType.Failure:
                    return CallStatus.Failure;
                case StatusType.Warning:
                    return CallStatus.Warning;
                default:
                    throw new ArgumentException(string.Format("Status type not supported - {0}", statusType),
                                                "statusType");
            }
        }

        public static Entities.SeatMap.CabinType ToEntity(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.First:
                    return Entities.SeatMap.CabinType.First;
                case CabinType.Economy:
                    return Entities.SeatMap.CabinType.Economy;
                case CabinType.PremiumEconomy:
                    return Entities.SeatMap.CabinType.PremiumEconomy;
                case CabinType.Business:
                    return Entities.SeatMap.CabinType.Business;
                case CabinType.Unknown:
                    return Entities.SeatMap.CabinType.Unknown;
                default:
                    throw new ArgumentException(string.Format("CabinType not supported - {0}", cabinType), "cabinType");
            }
        }

        public static Cabin ToEntity(FlightCabin cabin, DeckLocation deckLocation)
        {
            return new Cabin()
                       {
                           CabinFacilities =
                               cabin.CabinFacilities == null
                                   ? new List<CabinFacility>()
                                   : cabin.CabinFacilities.Select(ToEntity).ToListBuffered(cabin.CabinFacilities.Count),
                           CabinLocation = ToEntity(deckLocation),
                           Capacity = cabin.Capacity,
                           OverwingRowsRange = cabin.OverwingRowsRange,
                           Columns =
                               cabin.Columns == null
                                   ? new List<SeatColumn>()
                                   : cabin.Columns.Select(ToEntity).ToListBuffered(cabin.Columns.Count),
                           Rows =
                               cabin.Rows == null ? new List<SeatRow>() : cabin.Rows.Select(ToEntity).ToListBuffered(cabin.Rows.Count),
                           Seats =
                               cabin.Seats == null ? new List<Seat>() : cabin.Seats.Select(ToEntity).ToListBuffered(cabin.Seats.Count)
                       };
        }

        public static Seat ToEntity(Data.Services.SeatMap.Seat seat)
        {
            if (seat == null)
                throw new ArgumentNullException("seat", "Seat cannot be null");

            return new Seat()
                       {
                           ColumnCharacter = seat.ColumnCharacter,
                           RowNumber = seat.RowNumber,
                           SeatCharacteristics =
                               seat.SeatCharacteristics == null
                                   ? new List<Entities.SeatMap.SeatCharacteristic>()
                                   : seat.SeatCharacteristics.Select(ToEntity).ToListBuffered(seat.SeatCharacteristics.Count)
                       };
        }

        public static SeatRow ToEntity(Data.Services.SeatMap.SeatRow seatRow)
        {
            if (seatRow == null)
                throw new ArgumentNullException("seatRow", "Seat row cannot be null");

            return new SeatRow()
                       {
                           RowNumber = seatRow.RowNumber,
                           RowCharacteristics =
                               seatRow.RowCharacteristics == null
                                   ? new List<RowCharacteristic>()
                                   : seatRow.RowCharacteristics.Select(ToEntity).ToListBuffered(
                                       seatRow.RowCharacteristics.Count)
                       };
        }

        public static SeatColumn ToEntity(Data.Services.SeatMap.SeatColumn seatColumn)
        {
            if (seatColumn == null)
                throw new ArgumentNullException("seatColumn", "Seat column cannot be null");

            return new SeatColumn()
                       {
                           ColumnCharacter = seatColumn.ColumnCharacter,
                           ColumnType = ToEntity(seatColumn.ColumnType)
                       };
        }

        public static Entities.SeatMap.SeatCharacteristic ToEntity(SeatCharacteristic seatCharacteristic)
        {
            switch (seatCharacteristic)
            {
                case SeatCharacteristic.BehindGalley:
                    return Entities.SeatMap.SeatCharacteristic.BehindGalley;
                case SeatCharacteristic.Infant:
                    return Entities.SeatMap.SeatCharacteristic.Infant;
                case SeatCharacteristic.LegRestAvailable:
                    return Entities.SeatMap.SeatCharacteristic.LegRestAvailable;
                case SeatCharacteristic.LegSpace:
                    return Entities.SeatMap.SeatCharacteristic.LegSpace;
                case SeatCharacteristic.MedicallyOkayForTravel:
                    return Entities.SeatMap.SeatCharacteristic.MedicallyOkayForTravel;
                case SeatCharacteristic.NearGalley:
                    return Entities.SeatMap.SeatCharacteristic.NearGalley;
                case SeatCharacteristic.Aisle:
                    return Entities.SeatMap.SeatCharacteristic.Aisle;
                case SeatCharacteristic.NearLavatory:
                    return Entities.SeatMap.SeatCharacteristic.NearLavatory;
                case SeatCharacteristic.NoSeatBar:
                    return Entities.SeatMap.SeatCharacteristic.NoSeatBar;
                case SeatCharacteristic.ExitRowSeats:
                    return Entities.SeatMap.SeatCharacteristic.ExitRowSeats;
                case SeatCharacteristic.AirportBlock:
                    return Entities.SeatMap.SeatCharacteristic.AirportBlock;
                case SeatCharacteristic.NoSmoking:
                    return Entities.SeatMap.SeatCharacteristic.NoSmoking;
                case SeatCharacteristic.NotForInfant:
                    return Entities.SeatMap.SeatCharacteristic.NotForInfant;
                case SeatCharacteristic.NotForUnaccompaniedMinor:
                    return Entities.SeatMap.SeatCharacteristic.NotForUnaccompaniedMinor;
                case SeatCharacteristic.NotForChildren:
                    return Entities.SeatMap.SeatCharacteristic.NotForChildren;
                case SeatCharacteristic.OverWing:
                    return Entities.SeatMap.SeatCharacteristic.OverWing;
                case SeatCharacteristic.Paid:
                    return Entities.SeatMap.SeatCharacteristic.Paid;
                case SeatCharacteristic.MovieScreen:
                    return Entities.SeatMap.SeatCharacteristic.MovieScreen;
                case SeatCharacteristic.IndividualAirphone:
                    return Entities.SeatMap.SeatCharacteristic.IndividualAirphone;
                case SeatCharacteristic.AisleToLeft:
                    return Entities.SeatMap.SeatCharacteristic.AisleToLeft;
                case SeatCharacteristic.FrontOfLavatory:
                    return Entities.SeatMap.SeatCharacteristic.FrontOfLavatory;
                case SeatCharacteristic.ElectronicConnectionAvailable:
                    return Entities.SeatMap.SeatCharacteristic.ElectronicConnectionAvailable;
                case SeatCharacteristic.EconomyComfort:
                    return Entities.SeatMap.SeatCharacteristic.EconomyComfort;
                case SeatCharacteristic.Deportee:
                    return Entities.SeatMap.SeatCharacteristic.Deportee;
                case SeatCharacteristic.Crew:
                    return Entities.SeatMap.SeatCharacteristic.Crew;
                case SeatCharacteristic.MiddleSection:
                    return Entities.SeatMap.SeatCharacteristic.MiddleSection;
                case SeatCharacteristic.Middle:
                    return Entities.SeatMap.SeatCharacteristic.Middle;
                case SeatCharacteristic.BulkheadSeatWithMovieScreen:
                    return Entities.SeatMap.SeatCharacteristic.BulkheadSeatWithMovieScreen;
                case SeatCharacteristic.FrontOfCabinClass:
                    return Entities.SeatMap.SeatCharacteristic.FrontOfCabinClass;
                case SeatCharacteristic.Premium:
                    return Entities.SeatMap.SeatCharacteristic.Premium;
                case SeatCharacteristic.Bulkhead:
                    return Entities.SeatMap.SeatCharacteristic.Bulkhead;
                case SeatCharacteristic.Buffer:
                    return Entities.SeatMap.SeatCharacteristic.Buffer;
                case SeatCharacteristic.BlockedForPreferredPassenger:
                    return Entities.SeatMap.SeatCharacteristic.BlockedForPreferredPassenger;
                case SeatCharacteristic.Blocked:
                    return Entities.SeatMap.SeatCharacteristic.Blocked;
                case SeatCharacteristic.BehindLavatory:
                    return Entities.SeatMap.SeatCharacteristic.BehindLavatory;
                case SeatCharacteristic.BehindGallery:
                    return Entities.SeatMap.SeatCharacteristic.BehindGallery;
                case SeatCharacteristic.AisleToRight:
                    return Entities.SeatMap.SeatCharacteristic.AisleToRight;
                case SeatCharacteristic.FullyRecline:
                    return Entities.SeatMap.SeatCharacteristic.FullyRecline;
                case SeatCharacteristic.FrontOfGalley:
                    return Entities.SeatMap.SeatCharacteristic.FrontOfGalley;
                case SeatCharacteristic.BufferZone:
                    return Entities.SeatMap.SeatCharacteristic.BufferZone;
                case SeatCharacteristic.PetInCabinOrSeat:
                    return Entities.SeatMap.SeatCharacteristic.PetInCabinOrSeat;
                case SeatCharacteristic.NoSeatCloset:
                    return Entities.SeatMap.SeatCharacteristic.NoSeatCloset;
                case SeatCharacteristic.QuietZone:
                    return Entities.SeatMap.SeatCharacteristic.QuietZone;
                case SeatCharacteristic.Window:
                    return Entities.SeatMap.SeatCharacteristic.Window;
                case SeatCharacteristic.Available:
                    return Entities.SeatMap.SeatCharacteristic.Available;
                case SeatCharacteristic.WindowAndAisle:
                    return Entities.SeatMap.SeatCharacteristic.WindowAndAisle;
                case SeatCharacteristic.HandicappedOrIncappacited:
                    return Entities.SeatMap.SeatCharacteristic.HandicappedOrIncappacited;
                case SeatCharacteristic.NotAllowedForMedical:
                    return Entities.SeatMap.SeatCharacteristic.NotAllowedForMedical;
                case SeatCharacteristic.UpperDeck:
                    return Entities.SeatMap.SeatCharacteristic.UpperDeck;
                case SeatCharacteristic.Preferred:
                    return Entities.SeatMap.SeatCharacteristic.Preferred;
                case SeatCharacteristic.ForwardEndCabin:
                    return Entities.SeatMap.SeatCharacteristic.ForwardEndCabin;
                case SeatCharacteristic.Occupied:
                    return Entities.SeatMap.SeatCharacteristic.Occupied;
                case SeatCharacteristic.HandleCabinBaggage:
                    return Entities.SeatMap.SeatCharacteristic.HandleCabinBaggage;
                case SeatCharacteristic.NoMovieView:
                    return Entities.SeatMap.SeatCharacteristic.NoMovieView;
                case SeatCharacteristic.MovieView:
                    return Entities.SeatMap.SeatCharacteristic.MovieView;
                case SeatCharacteristic.IndividualEarphone:
                    return Entities.SeatMap.SeatCharacteristic.IndividualEarphone;
                case SeatCharacteristic.NoEarphone:
                    return Entities.SeatMap.SeatCharacteristic.NoEarphone;
                case SeatCharacteristic.AdultWithInfant:
                    return Entities.SeatMap.SeatCharacteristic.AdultWithInfant;
                case SeatCharacteristic.UnaccompaniedMinor:
                    return Entities.SeatMap.SeatCharacteristic.UnaccompaniedMinor;
                case SeatCharacteristic.Smoking:
                    return Entities.SeatMap.SeatCharacteristic.Smoking;
                case SeatCharacteristic.AngledRight:
                    return Entities.SeatMap.SeatCharacteristic.AngledRight;
                case SeatCharacteristic.BassinetFacility:
                    return Entities.SeatMap.SeatCharacteristic.BassinetFacility;
                case SeatCharacteristic.LeftOfAircraft:
                    return Entities.SeatMap.SeatCharacteristic.LeftOfAircraft;
                case SeatCharacteristic.RestrictedRecline:
                    return Entities.SeatMap.SeatCharacteristic.RestrictedRecline;
                case SeatCharacteristic.RightOfAircraft:
                    return Entities.SeatMap.SeatCharacteristic.RightOfAircraft;
                case SeatCharacteristic.RearFacing:
                    return Entities.SeatMap.SeatCharacteristic.RearFacing;
                case SeatCharacteristic.WindowSeatWithNoWindow:
                    return Entities.SeatMap.SeatCharacteristic.WindowSeatWithNoWindow;
                case SeatCharacteristic.AdjacentToTable:
                    return Entities.SeatMap.SeatCharacteristic.AdjacentToTable;
                case SeatCharacteristic.Restricted:
                    return Entities.SeatMap.SeatCharacteristic.Restricted;
                case SeatCharacteristic.AdjacentToCloset:
                    return Entities.SeatMap.SeatCharacteristic.AdjacentToCloset;
                case SeatCharacteristic.AdjacentToStairsToUpperdeck:
                    return Entities.SeatMap.SeatCharacteristic.AdjacentToStairsToUpperdeck;
                case SeatCharacteristic.AngledLeft:
                    return Entities.SeatMap.SeatCharacteristic.AngledLeft;
                case SeatCharacteristic.AdjacentToBar:
                    return Entities.SeatMap.SeatCharacteristic.AdjacentToBar;
                case SeatCharacteristic.None:
                    return Entities.SeatMap.SeatCharacteristic.None;
                case SeatCharacteristic.NoSeat:
                    return Entities.SeatMap.SeatCharacteristic.NoSeat;
                default:
                    throw new ArgumentException(string.Format("Seat characteristic not supported - {0}", seatCharacteristic), "seatCharacteristic");
            }
        }

        public static SeatColumnType ToEntity(Data.Services.SeatMap.SeatColumnType seatColumnType)
        {
            switch (seatColumnType)
            {
                case Data.Services.SeatMap.SeatColumnType.Middle:
                    return SeatColumnType.Middle;
                case Data.Services.SeatMap.SeatColumnType.Aisle:
                    return SeatColumnType.Aisle;
                case Data.Services.SeatMap.SeatColumnType.Window:
                    return SeatColumnType.Window;
                case Data.Services.SeatMap.SeatColumnType.NotApplicable:
                    return SeatColumnType.NotApplicable;
                default:
                    throw new ArgumentException(string.Format("SeatColumnType not supported - {0}", seatColumnType),
                                                "seatColumnType");
            }
        }

        public static CabinFacility ToEntity(Data.Services.SeatMap.CabinFacility cabinFacility)
        {
            return new CabinFacility()
                       {
                           CabinFacilityLocation = ToEntity(cabinFacility.CabinFacilityLocation),
                           CabinFacilityType = ToEntity(cabinFacility.CabinFacilityType)
                       };
        }

        public static CabinFacilityLocation ToEntity(Data.Services.SeatMap.CabinFacilityLocation cabinFacilityLocation)
        {
            switch (cabinFacilityLocation)
            {
                case Data.Services.SeatMap.CabinFacilityLocation.Right:
                    return CabinFacilityLocation.Right;
                case Data.Services.SeatMap.CabinFacilityLocation.RightCenter:
                    return CabinFacilityLocation.RightCenter;
                case Data.Services.SeatMap.CabinFacilityLocation.Left:
                    return CabinFacilityLocation.Left;
                case Data.Services.SeatMap.CabinFacilityLocation.LeftCenter:
                    return CabinFacilityLocation.LeftCenter;
                case Data.Services.SeatMap.CabinFacilityLocation.Center:
                    return CabinFacilityLocation.Center;
                case Data.Services.SeatMap.CabinFacilityLocation.Rear:
                    return CabinFacilityLocation.Rear;
                case Data.Services.SeatMap.CabinFacilityLocation.Front:
                    return CabinFacilityLocation.Front;
                case Data.Services.SeatMap.CabinFacilityLocation.None:
                    return CabinFacilityLocation.None;
                default :
                    throw new ArgumentException(
                        string.Format("CabinFacilityLocation not supported - {0}", cabinFacilityLocation),
                        "cabinFacilityLocation");
            }
        }

        public static CabinLocation ToEntity(DeckLocation deckLocation)
        {
            switch (deckLocation)
            {
                case DeckLocation.Upperdeck:
                    return CabinLocation.Upperdeck;
                case DeckLocation.Maindeck:
                    return CabinLocation.Maindeck;
                case DeckLocation.Lowerdeck:
                    return CabinLocation.Lowerdeck;
                case DeckLocation.Unknown:
                    return CabinLocation.Unknown;
                default:
                    throw new ArgumentException(string.Format("DeckLocation not supported - {0}", deckLocation),
                                                "deckLocation");
            }
        }

        public static Entities.SeatMap.CabinFacilityType ToEntity(CabinFacilityType cabinFacilityType)
        {
            switch (cabinFacilityType)
            {
                case CabinFacilityType.Bulkhead:
                    return Entities.SeatMap.CabinFacilityType.Bulkhead;
                case CabinFacilityType.Airphone:
                    return Entities.SeatMap.CabinFacilityType.Airphone;
                case CabinFacilityType.Bar:
                    return Entities.SeatMap.CabinFacilityType.Bar;
                case CabinFacilityType.Closet:
                    return Entities.SeatMap.CabinFacilityType.Closet;
                case CabinFacilityType.EmergencyExit:
                    return Entities.SeatMap.CabinFacilityType.EmergencyExit;
                case CabinFacilityType.Lavatory:
                    return Entities.SeatMap.CabinFacilityType.Lavatory;
                case CabinFacilityType.ExitDoor:
                    return Entities.SeatMap.CabinFacilityType.ExitDoor;
                case CabinFacilityType.Gallery:
                    return Entities.SeatMap.CabinFacilityType.Gallery;
                case CabinFacilityType.Galley:
                    return Entities.SeatMap.CabinFacilityType.Galley;
                case CabinFacilityType.MovieScreen:
                    return Entities.SeatMap.CabinFacilityType.MovieScreen;
                case CabinFacilityType.QuietArea:
                    return Entities.SeatMap.CabinFacilityType.QuietArea;
                case CabinFacilityType.SmokingArea:
                    return Entities.SeatMap.CabinFacilityType.SmokingArea;
                case CabinFacilityType.StairsToUpperDeck:
                    return Entities.SeatMap.CabinFacilityType.StairsToUpperDeck;
                case CabinFacilityType.StorageSpace:
                    return Entities.SeatMap.CabinFacilityType.StorageSpace;
                case CabinFacilityType.Table:
                    return Entities.SeatMap.CabinFacilityType.Table;
                case CabinFacilityType.LuggageStorage:
                    return Entities.SeatMap.CabinFacilityType.LuggageStorage;
                case CabinFacilityType.None:
                    return Entities.SeatMap.CabinFacilityType.None;
                default:
                    throw new ArgumentException(string.Format("CabinFacilityType not supported - {0}", cabinFacilityType), "cabinFacilityType");
            }
        }

        public static RowCharacteristic ToEntity(Data.Services.SeatMap.RowCharacteristic rowCharacteristic)
        {
            switch (rowCharacteristic)
            {
                case Data.Services.SeatMap.RowCharacteristic.RightOfAisle:
                    return RowCharacteristic.RightOfAisle;
                case Data.Services.SeatMap.RowCharacteristic.ExitRow:
                    return RowCharacteristic.ExitRow;
                case Data.Services.SeatMap.RowCharacteristic.FirstRow:
                    return RowCharacteristic.FirstRow;
                case Data.Services.SeatMap.RowCharacteristic.LowerdeckRow:
                    return RowCharacteristic.LowerdeckRow;
                case Data.Services.SeatMap.RowCharacteristic.MaindeckRow:
                    return RowCharacteristic.MaindeckRow;
                case Data.Services.SeatMap.RowCharacteristic.LeftOfAisle:
                    return RowCharacteristic.LeftOfAisle;
                case Data.Services.SeatMap.RowCharacteristic.MiddleRow:
                    return RowCharacteristic.MiddleRow;
                case Data.Services.SeatMap.RowCharacteristic.ExitRight:
                    return RowCharacteristic.ExitRight;
                case Data.Services.SeatMap.RowCharacteristic.NonSmokingRow:
                    return RowCharacteristic.NonSmokingRow;
                case Data.Services.SeatMap.RowCharacteristic.OverwingRow:
                    return RowCharacteristic.OverwingRow;
                case Data.Services.SeatMap.RowCharacteristic.RearRow:
                    return RowCharacteristic.RearRow;
                case Data.Services.SeatMap.RowCharacteristic.RowWithMovieScreen:
                    return RowCharacteristic.RowWithMovieScreen;
                case Data.Services.SeatMap.RowCharacteristic.SmokingRow:
                    return RowCharacteristic.SmokingRow;
                case Data.Services.SeatMap.RowCharacteristic.UpperDeckRow:
                    return RowCharacteristic.UpperDeckRow;
                case Data.Services.SeatMap.RowCharacteristic.BulkheadRow:
                    return RowCharacteristic.BulkheadRow;
                case Data.Services.SeatMap.RowCharacteristic.BufferRow:
                    return RowCharacteristic.BufferRow;
                case Data.Services.SeatMap.RowCharacteristic.NotOverwingRow:
                    return RowCharacteristic.NotOverwingRow;
                case Data.Services.SeatMap.RowCharacteristic.ExitLeft:
                    return RowCharacteristic.ExitLeft;
                case Data.Services.SeatMap.RowCharacteristic.None:
                    return RowCharacteristic.None;
                default:
                    throw new ArgumentException(
                        string.Format("RowCharacteristic not supported - {0}", rowCharacteristic), "rowCharacteristic");
            }
        }

        #endregion

        #endregion

        #region private fields/methods

        private static readonly Dictionary<Entities.CabinType, CabinType> CabinTypeMapping =
            GetCabinTypeMapping();

        private static string GetSupplierFareRefKey(FlightRecommendation recommendation)
        {
            string supplierFareRefKey;
            recommendation.AdditionalInfo.TryGetValue(KeyStore.Scepter.FareSearchSupplierFareRefKey,
                                                      out supplierFareRefKey);

            return supplierFareRefKey ?? string.Empty;
        }

        private static Dictionary<Entities.CabinType, CabinType> GetCabinTypeMapping()
        {
            var cabinTypeMapping = new Dictionary<Entities.CabinType, CabinType>
                                       {
                                           {Entities.CabinType.Economy, CabinType.Economy},
                                           {Entities.CabinType.PremiumEconomy, CabinType.PremiumEconomy},
                                           {Entities.CabinType.First, CabinType.First},
                                           {Entities.CabinType.Business, CabinType.Business},
                                           {Entities.CabinType.Unknown, CabinType.Unknown}
                                       };

            return cabinTypeMapping;
        }

        private static Data.Services.SeatMap.Requester ToDataContract(Entities.Requester requester)
        {
            var currentContext = Utility.GetCurrentContext();

            return new Data.Services.SeatMap.Requester()
            {
                TrackingId = currentContext.SessionId.ToString(),
                AffiliateId = requester.ID.ToString(),
                UserSessionId = currentContext.SessionId.ToString(),
            };
        }

        private static FareSpace ToDataContract(ProviderSpace providerSpace)
        {
            return new FareSpace()
            {
                Code = providerSpace.ID.ToString(),
                AdditionalParams = GetValuePairs(providerSpace.AdditionalInfo)
                //Not passed in the qualifiers as they are not needed for seat map
            };
        }

        private static List<KeyValue> GetValuePairs(AdditionalInfoDictionary additionalInfoDictionary)
        {
            if (additionalInfoDictionary == null || additionalInfoDictionary.Count == 0)
                return new List<KeyValue>();

            return
                AdditionalInfoDictionary.ToDictionary(additionalInfoDictionary).Select(
                    x => new KeyValue() { Key = x.Key, Value = x.Value }).ToListBuffered(additionalInfoDictionary.Count);
        }

        #endregion
    }
}
