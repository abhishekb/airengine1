﻿using System;

namespace Tavisca.TravelNxt.Flight.SeatMap.Core.Exceptions
{
    [Serializable]
    public class SeatMapSupplierException : Exception
    {
        public SeatMapSupplierException()
        { }

        public SeatMapSupplierException(string message) : base(message)
        { }

        public SeatMapSupplierException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
