﻿namespace Tavisca.TravelNxt.Flight.ErrorSpace
{
    public static class ErrorCodeManager
    {
        public static string GetErrorMessage(string errorCode, string defaultError = "")
        {
            return ErrorMessages.ResourceManager.GetString(errorCode) ?? defaultError;
        }
    }
}
