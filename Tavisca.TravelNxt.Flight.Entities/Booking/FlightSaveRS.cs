﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightSaveRS
    {
        public CallStatus Status { get; set; }
        public string Message { get; set; }
        public Guid RecordLocator { get; set; }
    }
}
