﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightProduct
    {
        public FlightRecommendation Recommendation { get; set; }
    }
}
