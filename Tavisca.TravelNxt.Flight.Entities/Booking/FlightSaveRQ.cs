﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightSaveRQ
    {
        public FlightRecommendationInfo FlightRecommendationInfo { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public Requester Requester { get; set; }
    }
}
