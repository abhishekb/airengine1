﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightRecommendationInfo
    {
        public int FlightRecommendationRefId { get; set; }
    }
}
