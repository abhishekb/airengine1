﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightRetrieveRS
    {
        public CallStatus Status { get; set; }

        public string Message { get; set; }

        public FlightProduct Product { get; set; }
    }
}
