﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Booking
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightRetrieveRQ
    {
        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public Requester Requester { get; set; }

        public Guid RecordLocator { get; set; }
    }
}
