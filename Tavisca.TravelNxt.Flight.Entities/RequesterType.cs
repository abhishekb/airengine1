﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum RequesterType
    {
        Agency,
        Affiliate,
        Client,
        Platform,
        Root,
        User,
        Company
    }
}
