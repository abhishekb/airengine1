﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum FareComponentType
    {
        Tax,
        Fees,
        Markup,
        Discount,
        Commision
    }
}