﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class PassengerFare
    {
        private IList<FareComponent> _markups = new List<FareComponent>();
        private IList<FareComponent> _fees = new List<FareComponent>();
        private IList<FareComponent> _taxes = new List<FareComponent>();
        private IList<FareComponent> _commissions = new List<FareComponent>();
        private IList<FareComponent> _discounts = new List<FareComponent>();

        public IList<FareComponent> Markups
        {
            get
            {
                return _markups;
            }
            set
            {
                _markups = value ?? new List<FareComponent>();
            }
        }

        public IList<FareComponent> Fees
        {
            get
            {
                return _fees;
            }
            set
            {
                _fees = value ?? new List<FareComponent>();
            }
        }

        public IList<FareComponent> Commissions
        {
            get
            {
                return _commissions;
            }
            set
            {
                _commissions = value ?? new List<FareComponent>();
            }
        }

        public IList<FareComponent> Taxes
        {
            get
            {
                return _taxes;
            }
            set
            {
                _taxes = value ?? new List<FareComponent>();
            }
        }

        public IList<FareComponent> Discounts
        {
            get
            {
                return _discounts;
            }
            set
            {
                _discounts = value ?? new List<FareComponent>();
            }
        }

        public Money TotalMarkup
        {
            get
            {
                return Markups.Count == 0 ? Money.Zero : Markups.Select(x => x.Value).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalFees
        {
            get
            {
                return Fees.Count == 0 ? Money.Zero : Fees.Select(x => x.Value).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalCommission
        {
            get
            {
                return Commissions.Count == 0 ? Money.Zero : Commissions.Select(x => x.Value).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalTaxes
        {
            get
            {
                return Taxes.Count == 0 ? Money.Zero : Taxes.Select(x => x.Value).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalDiscount
        {
            get
            {
                return Discounts.Count == 0 ? Money.Zero : Discounts.Select(x => x.Value).Aggregate((x, y) => x + y);
            }
        }

        public int Quantity { get; set; }

        public Money BaseAmount { get; set; }

        public Money TotalAmount { get { return BaseAmount + TotalFees + TotalMarkup + TotalTaxes - TotalDiscount; } }

        public PassengerType PassengerType { get; set; }

        public void Add(PassengerFare passengerFare)
        {
            this.BaseAmount += passengerFare.BaseAmount;
            passengerFare.Commissions.ForEach(commission => this.Commissions.Add(commission));
            passengerFare.Discounts.ForEach(discount => this.Discounts.Add(discount));
            passengerFare.Taxes.ForEach(tax => this.Taxes.Add(tax));
            passengerFare.Markups.ForEach(markup => this.Markups.Add(markup));
            passengerFare.Fees.ForEach(fee => this.Commissions.Add(fee));
        }
    }
}
