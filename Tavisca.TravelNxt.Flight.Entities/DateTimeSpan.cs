﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class DateTimeSpan
    {
        public DateTime? End { get; set; }
        
        public DateTime? Start { get; set; }
    }
}
