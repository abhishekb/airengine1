﻿//Commented the class as it is not being used anywhere

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ProtoBuf;

//namespace Tavisca.TravelNxt.Flight.Entities
//{
//    [Serializable] 
//    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
//    public class Fare
//    {
//        private IList<FareComponent> _markups = new List<FareComponent>();
//        private IList<FareComponent> _fees = new List<FareComponent>();
//        private IList<FareComponent> _taxes = new List<FareComponent>();
//        private IList<FareComponent> _commissions = new List<FareComponent>();
//        private IList<FareComponent> _discounts = new List<FareComponent>();


//        public IList<FareComponent> Markups
//        {
//            get
//            {
//                return _markups;
//            }
//            set {
//                _markups = value ?? new List<FareComponent>();
//            }
//        }

//        public IList<FareComponent> Fees
//        {
//            get
//            {
//                return _fees;
//            }
//            set {
//                _fees = value ?? new List<FareComponent>();
//            }
//        }

//        public IList<FareComponent> Commissions
//        {
//            get
//            {
//                return _commissions;
//            }
//            set {
//                _commissions = value ?? new List<FareComponent>();
//            }
//        }

//        public IList<FareComponent> Taxes
//        {
//            get
//            {
//                return _taxes;
//            }
//            set {
//                _taxes = value ?? new List<FareComponent>();
//            }
//        }

//        public IList<FareComponent> Discounts
//        {
//            get
//            {
//                return _discounts;
//            }
//            set {
//                _discounts = value ?? new List<FareComponent>();
//            }
//        }

//        public Money BaseAmount { get; set; }

//        public Money TotalAmount { get; set; }

//        public FareType FareType { get; set; }
//    }
//}
