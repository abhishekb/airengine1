﻿using ProtoBuf;
using System;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class TravelDate: IDeepCopyable<TravelDate>
    {
        public DateTime DateTime { get; set; }

        public int TimeWindow { get; set; }

        public bool AnyTime { get; set; }

        public int? MinDate { get; set; }
        
        public int? MaxDate { get; set; }

        public TravelDate DeepCopy()
        {
            return new TravelDate()
                {
                    AnyTime = AnyTime,
                    DateTime = DateTime,
                    TimeWindow = TimeWindow,
                    MaxDate = MaxDate,
                    MinDate = MinDate
                };
        }
    }
}