﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Common;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightSegmentCollection : IKeyedCollection<FlightSegment>
    {
        private const string SegmentSeparator = "&&";
        private readonly IList<FlightSegment> _internalList = new List<FlightSegment>();

        public FlightSegment this[int index]
        {
            get { return _internalList[index]; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value", "FlightSegmentCollection does not support null values.");

                var previous = this[index];

                if (previous != null)
                {
                    this.Remove(previous);
                }
                _internalList[index] = value;
                Key = Key + SegmentSeparator + value.Key;
            }
        }

        private string _key = string.Empty;

        public string Key
        {
            get { return _key; }
            private set
            {
                _key = value;
            }
        }

        public void Add(FlightSegment item)
        {
            
            _internalList.Add(item);
            // && is the separator for each segment key
            Key = Key + SegmentSeparator + item.Key;

        }

        public void AddRange(IEnumerable<FlightSegment> items)
        {
            foreach (var flightSegment in items)
                Add(flightSegment);
        }

        public void Clear()
        {
            _internalList.Clear();
            Key = string.Empty;
        }

        public bool Contains(FlightSegment item)
        {
            return _internalList.Contains(item);
        }

        /// <summary>
        /// CopyTo method not supported for FlightSegments collection! Please do not use it
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        [Obsolete]
        public void CopyTo(FlightSegment[] array, int arrayIndex)
        {
            throw new NotSupportedException("CopyTo method not supported for FlightSegments collection!");

        }

        public int Count
        {
            get { return _internalList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(FlightSegment item)
        {
            Key = Key.Replace(item.Key + "&&", string.Empty);
            return _internalList.Remove(item);
        }

        public IEnumerator<FlightSegment> GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }
    }
}