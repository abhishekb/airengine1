﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightFare
    {
        public Money BaseAmount
        {
            get
            {
                return PassengerFares.Select(x => x.BaseAmount * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalFare
        {
            get { return TotalMarkup + TotalFees + TotalTaxes + BaseAmount - TotalDiscount; }
        }

        private IList<PassengerFare> _passengerFares = new List<PassengerFare>();

        public IList<PassengerFare> PassengerFares
        {
            get { return _passengerFares; }
            set { _passengerFares = value ?? new List<PassengerFare>(); }
        }

        public IList<FareAttribute> FareAttributes { get; set; } 

        public FareType FareType { get; set; }

        public Money TotalMarkup
        {
            get
            {
                return PassengerFares.Select(x => x.TotalMarkup * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalFees
        {
            get
            {
                return PassengerFares.Select(x => x.TotalFees * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalCommission
        {
            get
            {
                return PassengerFares.Select(x => x.TotalCommission * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalTaxes
        {
            get
            {
                return PassengerFares.Select(x => x.TotalTaxes * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money TotalDiscount
        {
            get
            {
                return PassengerFares.Select(x => x.TotalDiscount * x.Quantity).Aggregate((x, y) => x + y);
            }
        }

        public Money Savings { get; set; }

        public void Add(FlightFare flightFare)
        {
            var newPassengerFares = new List<PassengerFare>();
            foreach (var passengerFare in flightFare.PassengerFares)
            {
                var existingPassengerFare =
                    _passengerFares.FirstOrDefault(x => x.PassengerType == passengerFare.PassengerType);

                if (existingPassengerFare != null)
                    existingPassengerFare.Add(passengerFare);
                else
                    newPassengerFares.Add(passengerFare);
            }
            newPassengerFares.ForEach(newPassengerFare => _passengerFares.Add(newPassengerFare));
        }
    }
}