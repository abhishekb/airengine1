using ProtoBuf;
using System;
using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Entities.FareRules
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ServiceStatus
    {
        public string StatusCode { get; set; }
        public CallStatus Status { get; set; }
        public IList<string> Messages { get; set; }
        public string StatusMessage { get; set; }
    }
}