﻿using ProtoBuf;
using System;
namespace Tavisca.TravelNxt.Flight.Entities.FareRules
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FareRuleDetail
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
