﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Entities.FareRules
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FareRule
    {
        public IList<FareRuleDetail> FareRuleDetails { get; set; }
        public string FareBasisCode { get; set; }
    }
}
