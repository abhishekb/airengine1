﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Entities.FareRules
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FareRulesRS
    {
        public Guid SessionId { get; set; }
        public ServiceStatus ServiceStatus { get; set; }
        public IList<FareRule> FareRules { get; set; }
    }
}
