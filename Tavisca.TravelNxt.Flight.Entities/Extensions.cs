﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Entities
{
    public static class Extensions
    {
        public static void Collate(this IList<FareComponent> collection, FareComponent fareComponent)
        {
            var existing = collection.FirstOrDefault(x => 
                                !string.IsNullOrWhiteSpace(x.Description) &&
                                x.Description.Equals(fareComponent.Description, StringComparison.InvariantCultureIgnoreCase) &&
                                x.FareComponentRuleId == fareComponent.FareComponentRuleId
                            );

            if (existing != null)
            {
                fareComponent.Value += existing.Value;
                collection.Remove(existing);
            }

            collection.Add(fareComponent);
        }

        public static void AddRange<T>(this IList<T> list, IEnumerable<T> others)
        {
            if (others == null)
                return;

            foreach (var other in others)
            {
                list.Add(other);
            }
        }

        public static void ForEach<T>(this IList<T> list, Action<T>  action)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            if (action == null)
                return;

            foreach (var item in list)
            {
                action(item);
            }
        }

        public static TravelNxt.Common.Extensions.TimeZoneInfo ToTimeZoneInfo(this Airport airport)
        {
            return new TravelNxt.Common.Extensions.TimeZoneInfo(airport.City.WindowsTimeZone);
        }
    }
}
