﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightSegment : IEquatable<FlightSegment>
    {
        /// <summary>
        /// This value is for inbound or outbound leg.
        /// </summary>
        public int SegmentIndex { get; private set; }

        public string FlightNumber { get; private set; }

        public string MarriageSegmentCode { get; set; } //married segments have special fares, this code is required in pricing. not needed in search response.

        public IList<PaxTypeFareBasis> PaxTypeFareBasisCodes { get; set; }

        public Airline MarketingAirline { get; private set; }

        public Airline OperatingAirline { get; set; }

        public string CodeShareText { get; set; }

        public string AircraftType { get; set; }

        // Travel dates and points
        public Airport DepartureAirport { get; private set; }

        public Airport ArrivalAirport { get; private set; }

        public string DepartureTerminal { get; set; }

        public string ArrivalTerminal { get; set; }

        public DateTime DepartureDateTime { get; private set; }

        public DateTime ArrivalDateTime { get; private set; }

        public IList<SegmentStop> InSegmentStops { get; set; }

        /// <summary>
        /// Whether the particular flight in the pricing recommendation can be eticketed
        /// </summary>
        public bool CanETicket { get; set; }

        public int DurationMinutes { get; set; }

        public int LayoverDurationMinutes { get; set; }

        public int DistanceTraveledKms { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public IList<Baggage> BaggageDetails { get; set; }

        private FlightSegment() { } //for serialization, don't remove

        public FlightSegment(string departureAirportCode, string arrivalAirportCode, DateTime departureDateTime, 
            DateTime arrivalDateTime, string flightNumber, string marketingAirlineCode, int segmentIndex)
        {
            DepartureAirport = new Airport(departureAirportCode);
            ArrivalAirport = new Airport(arrivalAirportCode);
            DepartureDateTime = departureDateTime;
            ArrivalDateTime = arrivalDateTime;
            MarketingAirline = new Airline(marketingAirlineCode);
            FlightNumber = flightNumber;
            SegmentIndex = segmentIndex;

            Key = marketingAirlineCode + "-" + departureAirportCode + "-" + flightNumber + "-" +
                  departureDateTime.ToString("ddMMyyHHmm") + "-" + arrivalDateTime.ToString("ddMMyyHHmm");
        }


        /// <summary>
        /// Key to uniquely identify one segment.
        /// </summary>
        private string _key;

        private string _keyWithClassOfService;
        public string Key
        {
            get
            {
                if (!Settings.SettingManager.UseClassOfServiceInItineraryIdentification)
                    return _key;

                return _keyWithClassOfService ??
                       (_keyWithClassOfService = (_key + "-" + PaxTypeFareBasisCodes.First().ClassOfService));
            }

            private set { _key = value; } 
        }

        public bool Equals(FlightSegment other)
        {
            return other != null && string.Equals(Key, other.Key);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return Equals(((FlightSegment)obj));
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }
    }
}
