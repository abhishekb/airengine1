﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum PassengerType
    {
        Infant = 0,
        Child = 1,
        Adult = 2,
        Senior = 3,
        Mapped = 4,
        MilitaryAdult = 5,
        MilitaryChild = 6,
        MilitaryInfant = 7,
        MilitarySenior = 8
    }
}
