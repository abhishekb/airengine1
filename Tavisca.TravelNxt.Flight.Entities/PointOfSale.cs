﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class PointOfSale
    {
        public int ID { get; private set; }

        public string Name { get; set; }

        private PointOfSale() { } //for serialization, don't remove

        public PointOfSale(int posId)
        {
            //TODO: get point of sale
            ID = posId;
        }
    }
}