﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public abstract class AirContract
    {
        /// <summary>
        /// Contract Id 
        /// </summary>
        public long ContractId { get; set; }

        /// <summary>
        /// User friendly Name of the contract
        /// </summary>
        public string ContractName { get; set; }

        /// <summary>
        /// Agency Id to which this contract belongs to. * for all agencies
        /// </summary>
        public long AgencyId { get; set; }

        /// <summary>
        /// Air faresource to use while booking this contract specific itinerary. Use null or empty for using the same fare source as search faresource.
        /// </summary>

        public int? BookAirFareSourceId { get; set; }

        public int? SearchAirFareSourceId { get; set; }

        /// <summary>
        /// List of service classes that this contract applies to. Use null or empty list for all class of services
        /// </summary>
        public IList<string> ApplicableClassOfService { get; set; }

        /// <summary>
        /// List of cabin types that this contract applies to. Use null or empty list for all cabin classes
        /// </summary>
        public IList<string> ApplicableCabinTypes { get; set; }

        /// <summary>
        /// Time period for which this contract is applicable. Use null for no restrictions.
        /// </summary>
        public DateTimeSpan ValidPeriod { get; set; }

        /// <summary>
        /// Travel period for which this contract is applicable. Use null for no restrictions.
        /// </summary>
        public DateTimeSpan TravelPeriod { get; set; }

        public DateTimeSpan TicketingPeriod { get; set; }

        /// <summary>
        /// Type of contract. Valid values are 'Commissionable, ATPCONet, PaperNet, ZapOff'
        /// </summary>
        public string ContractType { get; set; }

        /// <summary>
        /// Contract rate code to be used to search/identify fares. Use null/empty for post-search contracts that have no rate codes.
        /// </summary>
        public string ContractRateCode { get; set; }

        /// <summary>
        /// Type of contract rate code. Valid values are 'NotApplicable, AccountCode, SnapCode, CorporateCode'
        /// </summary>

        public string ContractRateCodeType { get; set; }

        /// <summary>
        /// Fare Type of the itineraries that this contract is applicable to. For pre-search contracts, this tags all itineraries recieved by this fare type.
        /// </summary>
        public string FareType { get; set; }

        /// <summary>
        /// Specifies the preference value for this contract. This may be used to apply correct preference to an itinerary which is valid for more than 1 contract.
        /// </summary>
        public int Preference { get; set; }

        public string AirlinePreferenceLevel { get; set; }

        public IList<string> AirportPairs { get; set; }

        public IList<string> Airlines { get; set; }

        public IList<string> FareBasisCodes { get; set; }

        public string InterceptLevel { get; set; }

        public abstract bool IsContractApplicable(FlightSearchCriteria searchCriteria);

        public abstract bool CanSellRecommendation(FlightRecommendation flightRecommendation);
    }
}
