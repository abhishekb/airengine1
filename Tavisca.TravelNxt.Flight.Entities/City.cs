﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class City
    {
        public string CountryCode { get; set; }

        public string StateCode { get; set; }

        public string Code { get; set; }
         
        public string Name { get; set; }

        public string WindowsTimeZone { get; set; }
    }
}
