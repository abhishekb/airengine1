﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ProviderSpaceConfiguration
    {
        public ConfigurationType ConfigType { get; set; }
        public string ContractVersion { get; set; }
        public Uri ServiceUri { get; set; }
    }
}
