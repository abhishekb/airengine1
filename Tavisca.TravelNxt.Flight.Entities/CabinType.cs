﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CabinType
    {
        //The order of the enum is important here, higher classes must be represented by lower numbers, unknown must remain 0.
        //to understand why, see the cabin type resolution in case of two zones having conflicting cabin types.
        Unknown = 0,
        First = 1,
        Business = 2,
        PremiumEconomy = 3,
        Economy = 4
    }
}
