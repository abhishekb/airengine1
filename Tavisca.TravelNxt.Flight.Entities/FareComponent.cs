﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FareComponent
    {
        public FareComponentType FareComponentType { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public long OwnerId { get; set; }

        public long FareComponentRuleId { get; set; }

        public Money Value { get; set; }
    }
}
