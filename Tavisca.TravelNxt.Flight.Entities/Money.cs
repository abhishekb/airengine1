﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Extensions.ExchangeRate;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    sealed public class Money
    {
        #region Properties

        public string DisplayCurrency { get; private set; }

        public Decimal DisplayAmount { get; private set; }

        public string BaseCurrency { get; private set; }

        public Decimal BaseAmount { get; private set; }

        public Decimal NativeAmount { get; private set; }

        public string NativeCurrency { get; private set; }

        public static ExchangeRateContainer ExchangeRateContainer
        {
            get
            {
                return Utility.GetCurrentContext().ExchangeRateContainer;
            }

        }

        private readonly bool _isZero;

        #endregion

        #region Constructors

        private Money() { }

        private Money(bool isZero)
        {
            if (isZero)
            {
                NativeAmount = 0;
                NativeCurrency = null;
                _isZero = true;
            }
        }

        public Money(decimal nativeAmount, string nativeCurrency)
        {
            _isZero = false;
            NativeAmount = nativeAmount;
            NativeCurrency = nativeCurrency;
            BaseAmount = ConvertToValue(SettingManager.AgencyBaseCurrency);
            BaseCurrency = SettingManager.AgencyBaseCurrency;
        }

        public Money(decimal nativeAmount, string nativeCurrency, decimal baseAmount, string baseCurrency,
                     decimal displayAmount, string displayCurrency)
        {
            _isZero = false;
            NativeAmount = nativeAmount;
            NativeCurrency = nativeCurrency;
            BaseAmount = baseAmount;
            BaseCurrency = baseCurrency;
            DisplayAmount = displayAmount;
            DisplayCurrency = displayCurrency;
        }

        #endregion

        #region Conversions

        public Money ConvertTo(string currency)
        {
            if (IsZero(this))
                return Money.Zero;

            if (string.IsNullOrWhiteSpace(currency))
                throw new ArgumentNullException("currency", "currency parameter cannot be empty.");

            return string.Equals(NativeCurrency, currency) ? this : new Money(ConvertToValue(currency), currency);
        }


        public decimal ConvertToValue(string currency)
        {
            if (IsZero(this))
                return 0;

            if (string.IsNullOrWhiteSpace(currency))
                throw new ArgumentNullException("currency", "currency parameter cannot be empty.");

            if (string.Equals(currency, NativeCurrency))
                return NativeAmount;

            return Convert(ExchangeRateContainer.GetExchangeRate(NativeCurrency, currency), NativeAmount);
        }

        private decimal Convert(double conversionRate, decimal amount)
        {
            return (decimal)conversionRate * amount;
        }

        public decimal ToNativeCurrencyValue()
        {
            return ConvertToValue(this.NativeCurrency);
        }

        #endregion

        #region Operators

        private const string MoneyUnsupportedOperationMessage =
            "operation when any one money object is null is not supported.";

        public static Money operator +(Money m1, Money m2)
        {
            if (m1 == null || IsZero(m1))
                return m2;
            if (m2 == null || IsZero(m2))
                return m1;

            return Add(m1, m2);
        }

        public static Money operator -(Money m1, Money m2)
        {
            if (m1 == null || IsZero(m1))
                throw new NotSupportedException(MoneyUnsupportedOperationMessage);
            else if (m2 == null || IsZero(m2))
            {
                return m1;
            }


            return Subtract(m1, m2);
        }

        public static Money operator /(Money m1, int number)
        {
            if (m1 == null)
                throw new NotSupportedException(MoneyUnsupportedOperationMessage);

            if (IsZero(m1))
            {
                return m1;
            }
            
            return new Money(m1.NativeAmount / number, m1.NativeCurrency, m1.BaseAmount / number, m1.BaseCurrency,
                             m1.DisplayAmount / number, m1.DisplayCurrency);
        }



        public static Money operator *(Money m1, int number)
        {
            if (m1 == null)
                throw new NotSupportedException(MoneyUnsupportedOperationMessage);

            if (IsZero(m1))
            {
                return m1;
            }
            
            return new Money(m1.NativeAmount * number, m1.NativeCurrency, m1.BaseAmount * number, m1.BaseCurrency,
                             m1.DisplayAmount * number, m1.DisplayCurrency);
        }

        #endregion

        #region Zero Operations

        private static readonly Money ZeroMoney = new Money(true);
        public static Money Zero
        {
            get
            {
                return ZeroMoney;
                //return new Money { NativeAmount = 0, NativeCurrency = null };
            }
        }

        public static bool IsZero(Money m)
        {
            return m != null && m._isZero;
            //return m != null && m.NativeAmount == 0 && m.NativeCurrency == null;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return this.NativeAmount.ToString(CultureInfo.InvariantCulture) + " " + this.NativeCurrency;
        }

        #endregion

        #region Money Operations

        private static Money Add(Money m1, Money m2)
        {
            if (string.IsNullOrEmpty(m1.DisplayCurrency) && !string.IsNullOrEmpty(m2.DisplayCurrency))
                m1.DisplayAmount = m1.ConvertToValue(m2.DisplayCurrency);

            if (string.IsNullOrEmpty(m2.DisplayCurrency) && !string.IsNullOrEmpty(m1.DisplayCurrency))
                m2.DisplayAmount = m2.ConvertToValue(m1.DisplayCurrency);

            // We retain the currency code for the first amount
            if (string.Compare(m1.NativeCurrency, m2.NativeCurrency, StringComparison.OrdinalIgnoreCase) == 0)
                return new Money(m1.NativeAmount + m2.NativeAmount, m1.NativeCurrency, m1.BaseAmount + m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount + m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);

            if (!string.IsNullOrEmpty(m1.NativeCurrency))
            {

                return new Money(m1.NativeAmount + m2.ConvertToValue(m1.NativeCurrency), m1.NativeCurrency, m1.BaseAmount + m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount + m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);
            }
            return new Money(m2.NativeAmount + m1.ConvertToValue(m2.NativeCurrency), m2.NativeCurrency, m1.BaseAmount + m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount + m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);
        }

        private static Money Subtract(Money m1, Money m2)
        {
            if (string.IsNullOrEmpty(m1.DisplayCurrency) && !string.IsNullOrEmpty(m2.DisplayCurrency))
                m1.DisplayAmount = m1.ConvertToValue(m2.DisplayCurrency);

            if (string.IsNullOrEmpty(m2.DisplayCurrency) && !string.IsNullOrEmpty(m1.DisplayCurrency))
                m2.DisplayAmount = m2.ConvertToValue(m1.DisplayCurrency);

            // We retain the currency code for the first amount
            if (string.Equals(m1.NativeCurrency, m2.NativeCurrency, StringComparison.OrdinalIgnoreCase))
                return new Money(m1.NativeAmount - m2.NativeAmount, m1.NativeCurrency, m1.BaseAmount - m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount - m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);

            if (!string.IsNullOrEmpty(m1.NativeCurrency))
            {
                return new Money(m1.NativeAmount - m2.ConvertToValue(m1.NativeCurrency), m1.NativeCurrency, m1.BaseAmount - m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount - m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);
            }

            return new Money(m2.NativeAmount - m1.ConvertToValue(m2.NativeCurrency), m2.NativeCurrency, m1.BaseAmount - m2.BaseAmount, m1.BaseCurrency, m1.DisplayAmount - m2.DisplayAmount, m1.DisplayCurrency ?? m2.DisplayCurrency);
        }

        #endregion
    }
}
