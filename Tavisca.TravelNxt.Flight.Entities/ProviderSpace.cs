﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ProviderSpace : IEquatable<ProviderSpace>
    {
        public int ID { get; private set; }

        public string Name { get; private set; }

        public string Family { get; private set; }

        public Requester Requester { get; private set; }

        public int? EmulatedBy { get; private set; }
        
        public long? OwnerId { get; private set; }
        
        public ProviderSpaceType Type { get; private set; }

        public IEnumerable<ProviderSpaceConfiguration> Configurations { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public IList<AirContract> Contracts { get; set; }

        private ProviderSpace() { } //for serialization, don't remove

        public ProviderSpace(int id, string name, Requester requester, string family, 
            int? emulatedBy, long? ownerId, ProviderSpaceType type,
            IEnumerable<ProviderSpaceConfiguration> providerSpaceConfigurations,
            AdditionalInfoDictionary attributes) : this(id, name, requester, family, emulatedBy, ownerId, type, 
            providerSpaceConfigurations)
        {
            AdditionalInfo = attributes;
        }

        public ProviderSpace(int id, string name, Requester requester, string family, 
            int? emulatedBy, long? ownerId, ProviderSpaceType type,
            IEnumerable<ProviderSpaceConfiguration> providerSpaceConfigurations,
            IEnumerable<KeyValuePair<string, string>> attributes) : 
            this(id, name, requester, family, emulatedBy, ownerId, type, providerSpaceConfigurations)
        {
            AdditionalInfo = new AdditionalInfoDictionary(attributes);
        }

        protected ProviderSpace(int id, string name, Requester requester, string family,
            int? emulatedBy, long? ownerId, ProviderSpaceType type,
            IEnumerable<ProviderSpaceConfiguration> providerSpaceConfigurations)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (requester == null)
                throw new ArgumentNullException("requester");

            if (family == null)
                throw new ArgumentNullException("family");

            ID = id;
            Name = name;
            Family = family;
            EmulatedBy = emulatedBy;
            OwnerId = ownerId;
            this.Type = type;
            Requester = requester;

            Configurations = providerSpaceConfigurations == null ? new List<ProviderSpaceConfiguration>() :
                providerSpaceConfigurations.ToListBuffered();

            AdditionalInfo = new AdditionalInfoDictionary();
        }

        #region Equality members

        public bool Equals(ProviderSpace other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return ID == other.ID;
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            return Equals((ProviderSpace)obj);
        }

        #endregion
    }
}
