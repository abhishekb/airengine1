﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Airline : IEquatable<Airline>
    {
        private Airline() { } //for serialization, dont remove

        public Airline(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException("code");

            Code = code;
        }

        public string Code { get; private set; }

        public string FullName { get; set; }

        public string WebsiteURL { get; set; }

        #region Equality members

        public bool Equals(Airline other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return string.Equals(Code, other.Code, StringComparison.Ordinal);
        }

        private int _hashCode;
        public override int GetHashCode()
        {
            if (_hashCode != 0)
                return _hashCode;

            return (_hashCode = (Code != null ? Code.GetHashCode() : 0));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            return Equals((Airline)obj);
        }

        #endregion
    }
}
