﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Airport : IEquatable<Airport>
    {
        public string Code { get; private set; }

        public string FullName { get; set; }

        public City City { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        private Airport() { } //for serialization, don't remove

        public Airport(string code)
        {
            Code = code;
        }

        public double DistanceFrom(Airport other, UnitOptions unit)
        {
            return Utility.CalculateDistance(this.Latitude, this.Longitude, other.Latitude, other.Longitude, unit);
        }

        #region Equality members

        public bool Equals(Airport other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return string.Equals(Code, other.Code, StringComparison.Ordinal);
        }

        private int _hashCode;
        public override int GetHashCode()
        {
            if (_hashCode != 0)
                return _hashCode;

            return (_hashCode = (Code != null ? Code.GetHashCode() : 0));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            return Equals((Airport)obj);
        }

        #endregion
    }
}
