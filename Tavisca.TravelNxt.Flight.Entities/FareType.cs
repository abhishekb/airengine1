﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum FareType
    {
        Unknown,
        Published,
        Negotiated,
        Corporate
    }
}