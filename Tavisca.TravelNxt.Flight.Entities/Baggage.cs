﻿using ProtoBuf;
using System;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Baggage
    {
        public string Description { get; set; }

        public int? Quantity { get; set; }

        public decimal? WeightInPounds { get; set; }

        public PassengerType PassengerType { get; set; }

        public string BaggageCode { get; set; }
    }
}
