﻿using System;
using ProtoBuf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Core.Contracts;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Requester
    {
        private Address _address;
        public long ID { get; private set; }

        public PointOfSale POS { get; private set; }

        public RequesterType Type { get; private set; }

        public CustomerType CustomerType { get; private set; }

        public Address Address
        {
            get { return _address ?? (_address = GetAddress()); }
            set { _address = value; }
        }

        private Requester() { } //for serialization, don't remove

        public string RateCode { get; private set; }

        public Requester(long requesterId, int posId, CustomerType customerType)
        {
            RateCode = Settings.SettingManager.GetContextSettingValue(KeyStore.BackOffice.Sections.AppSettings,
                                                                      KeyStore.BackOffice.Keys.RateCode, false,
                                                                      KeyStore.BackOffice.Keys.BackOfficeContext);
            ID = requesterId;
            POS = new PointOfSale(posId);
            CustomerType = customerType;
            Type = GetRequesterType();
        }

        public string GetRateCode()
        {
            return RateCode;
        }

        protected virtual Address GetAddress()
        {
            var provider = RuntimeContext.Resolver.Resolve<IRequesterInfoProvider>();

            return (Address)provider.GetRequesterAddress(this.ID);
        }

        private RequesterType GetRequesterType()
        {
            var node = HierarchyFacade.GetNodeDetails(ID);
            return (RequesterType) Enum.Parse(typeof (RequesterType), node.Type);
        }
    }
}
