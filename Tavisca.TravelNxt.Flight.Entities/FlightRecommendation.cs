﻿using System;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightRecommendation
    {
        public int RefId { get; set; }
        
        public FlightLegCollection Legs { get; set; }

        public FlightFare Fare { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public string Key
        {
            get
            {
                return Legs.Key;
            }
            
        }

        public ProviderSpace ProviderSpace { get; set; }

        public ItineraryTypeOptions ItineraryType { get; set; }

        public string PlatingCarrier { get; set; }

        public string RateCode { get; set; }

        public AirContract Contract { get; set; }

        public int PassengerQuantity 
        { 
            get { return this.Fare.PassengerFares.Sum(x => x.Quantity); } 
        }
    }
}