﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class PaxTypeQuantity: IDeepCopyable<PaxTypeQuantity>
    {
        public IList<int> Ages { get; set; }
        
        public PassengerType PassengerType { get; set; }

        public bool ContainsConvertedChild { get; set; }

        public int Quantity { get; set; }

        public PaxTypeQuantity DeepCopy()
        {
            return new PaxTypeQuantity()
                {
                    PassengerType = PassengerType,
                    Quantity = Quantity,
                    Ages = Ages == null ? null : Ages.Select(x => x).ToListBuffered()
                };
        }
    }
}
