﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class PaxTypeFareBasis
    {
        public string FareBasisCode { get; set; }

        public PassengerType PassengerType { get; set; }

        public string SupplierFareRefKey { get; set; }

        public string ClassOfService { get; set; }

        public CabinType CabinType { get; set; }
    };
}
