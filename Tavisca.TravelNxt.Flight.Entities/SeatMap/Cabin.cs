﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Cabin
    {
        public IList<CabinFacility> CabinFacilities { get; set; }
        public int Capacity { get; set; }
        public IList<SeatColumn> Columns {get; set; }
        public IList<int> OverwingRowsRange { get; set; }
        public IList<SeatRow> Rows { get; set; }
        public IList<Seat> Seats { get; set; }
        public CabinLocation CabinLocation { get; set; }
    }
}