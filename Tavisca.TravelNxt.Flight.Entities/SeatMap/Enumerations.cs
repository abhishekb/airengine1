﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CabinType
    {
        Unknown,
        First,
        Business,
        PremiumEconomy,
        Economy
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CabinLocation : int
    {
        Unknown,
        Maindeck,
        Lowerdeck,
        Upperdeck
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum SeatCharacteristic
    {
        None,
        AirportBlock,
        Aisle,
        AisleToLeft,
        AisleToRight,
        BehindGallery,
        BehindLavatory,
        Blocked,
        BlockedForPreferredPassenger,
        Buffer,
        BufferZone,
        Bulkhead,
        BulkheadSeatWithMovieScreen,
        Middle,
        MiddleSection,
        Crew,
        Deportee,
        EconomyComfort,
        ElectronicConnectionAvailable,
        ExitRowSeats,
        FrontOfCabinClass,
        FullyRecline,
        FrontOfGalley,
        FrontOfLavatory,
        IndividualAirphone,
        MovieScreen,
        Infant,
        LegRestAvailable,
        LegSpace,
        MedicallyOkayForTravel,
        NearGalley,
        NearLavatory,
        NoSeatBar,
        NoSeatCloset,
        NoSmoking,
        NotForInfant,
        NotForUnaccompaniedMinor,
        NotForChildren,
        OverWing,
        Paid,
        Premium,
        PetInCabinOrSeat,
        Preferred,
        RearFacing,
        RestrictedRecline,
        Restricted,
        RightOfAircraft,
        LeftOfAircraft,
        AdjacentToTable,
        AdjacentToBar,
        AdjacentToCloset,
        AdjacentToStairsToUpperdeck,
        AngledLeft,
        AngledRight,
        ForwardEndCabin,
        Available,
        QuietZone,
        NotAllowedForMedical,
        UpperDeck,
        Occupied,
        AdultWithInfant,
        UnaccompaniedMinor,
        BassinetFacility,
        HandicappedOrIncappacited,
        NoEarphone,
        IndividualEarphone,
        MovieView,
        NoMovieView,
        HandleCabinBaggage,
        Smoking,
        WindowAndAisle,
        Window,
        WindowSeatWithNoWindow,
        BehindGalley,
        NoSeat
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CallStatus
    {
        Success = 0,
        Failure = 1,
        Warning = 2,
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CabinFacilityLocation
    {
        None,
        Center,
        LeftCenter,
        Left,
        RightCenter,
        Right,
        Rear,
        Front
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum RowCharacteristic
    {
        None,
        BufferRow,
        ExitLeft,
        ExitRight,
        ExitRow,
        FirstRow,
        LowerdeckRow,
        MaindeckRow,
        MiddleRow,
        NonSmokingRow,
        NotOverwingRow,
        OverwingRow,
        RearRow,
        RowWithMovieScreen,
        SmokingRow,
        UpperDeckRow,
        BulkheadRow,
        LeftOfAisle,
        RightOfAisle
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum SeatColumnType
    {
        NotApplicable,
        Window,
        Aisle,
        Middle
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CabinFacilityType
    {
        None,
        Airphone,
        Bar,
        Closet,
        EmergencyExit,
        ExitDoor,
        Gallery,
        Galley,
        LuggageStorage,
        MovieScreen,
        QuietArea,
        SmokingArea,
        StairsToUpperDeck,
        StorageSpace,
        Table,
        Lavatory,
        Bulkhead
    }
}
