using ProtoBuf;
using System;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SeatColumn
    {
        public char ColumnCharacter { get; set; }
        public SeatColumnType ColumnType { get; set; }
    }
}