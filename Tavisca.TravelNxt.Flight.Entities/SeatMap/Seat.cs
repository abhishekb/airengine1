using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Seat
    {
        public char ColumnCharacter { get; set; }
        public int RowNumber { get; set; }
        public IList<SeatCharacteristic> SeatCharacteristics { get; set; }
    }
}