﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ServiceStatus
    {
        public string StatusCode { get; set; }
        public CallStatus Status { get; set; }
        public IList<string> Messages { get; set; }
        public string StatusMessage { get; set; }
    }
}
