using ProtoBuf;
using System;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class CabinFacility
    {
        public CabinFacilityLocation CabinFacilityLocation { get; set; }
        public CabinFacilityType CabinFacilityType { get; set; }
    }
}