﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SeatMap
    {
        public CabinType CabinType { get; set; }
        public string ClassOfService { get; set; }
        public IList<Cabin> Cabins { get; set; }
    }
}
