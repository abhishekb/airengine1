using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SeatRow
    {
        public IList<RowCharacteristic> RowCharacteristics { get; set; }
        public int RowNumber { get; set; }
    }
}