﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SeatMapRQ
    {
        public int RecommendationRefID { get; set; }
        public string SegmentKey { get; set; }
        public Dictionary<string, string> AdditionalInfo { get; set; }
    }
}
