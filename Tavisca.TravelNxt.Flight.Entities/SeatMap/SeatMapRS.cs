﻿using ProtoBuf;
using System;

namespace Tavisca.TravelNxt.Flight.Entities.SeatMap
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SeatMapRS
    {
        public Guid SessionId { get; set; }
        public SeatMap SeatMap { get; set; }
        public ServiceStatus ServiceStatus { get; set; }
    }
}
