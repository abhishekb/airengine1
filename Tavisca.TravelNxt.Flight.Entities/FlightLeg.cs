﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightLeg : IEquatable<FlightLeg>
    {
        /// <summary>
        /// This is Leg Index considering type of itinerary like RoundTrip then it can have value upto 2
        /// </summary>
        public int LegIndex { get; set; }
        
        /// <summary>
        /// This is used to persist the LegGroupNumber from the Scepter FareSearchRS 
        /// </summary>
        public int LegGroupNumber { get; set; }

        public LegType LegType { get; set; }

        public FlightSegmentCollection Segments { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public string Key
        {
            get
            {
                return Segments.Key;
            }
           
        }

        public bool Equals(FlightLeg other)
        {
            return other != null && string.Equals(Key, other.Key);
        }

        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return Equals(((FlightLeg)obj));
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        public FlightLeg()
        {
            AdditionalInfo = new AdditionalInfoDictionary();
            Segments = new FlightSegmentCollection();
        }
    }
}
