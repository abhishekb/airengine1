﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Common;

namespace Tavisca.TravelNxt.Flight.Entities.Pricing
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ProviderSpaceReplacementPriceConfiguration
    {
        public int Id { get; set; }
        public int PriceConfigurationId { get; set; }
        public int ProviderSpaceId { get; set; }
        public int ReplacementProviderSpaceId { get; set; }
        public int? RuleId { get; set; }

        public AirRuleSet AirRuleSet { get; set; }
    }
}
