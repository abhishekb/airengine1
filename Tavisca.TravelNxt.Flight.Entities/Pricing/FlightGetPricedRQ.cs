﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Pricing
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightGetPricedRQ
    {
        public string ExternalIdentifier { get; set; }
        public AdditionalInfoDictionary AdditionalInfo { get; set; }
        public Requester Requester { get; set; }
    }
}
