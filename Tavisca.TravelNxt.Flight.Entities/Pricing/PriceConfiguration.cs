﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;

namespace Tavisca.TravelNxt.Flight.Entities.Pricing
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class PriceConfiguration
    {
        public int Id { get; set; }
        public int? PosId { get; set; }
        public long? AccountId { get; set; }
        public bool IsFailoverEnabled { get; set; }

        public ICollection<ProviderSpacePriceConfiguration> FareSourcePriceConfigurations { get; set; }
        public ICollection<ProviderSpaceReplacementPriceConfiguration> FareSourceReplacementPriceConfigurations { get; set; }

        public PriceConfiguration()
        {
            FareSourcePriceConfigurations = new List<ProviderSpacePriceConfiguration>();
            FareSourceReplacementPriceConfigurations = new List<ProviderSpaceReplacementPriceConfiguration>();
        }
    }
}
