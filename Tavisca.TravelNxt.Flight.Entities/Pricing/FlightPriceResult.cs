﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Pricing
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightPriceResult
    {
        public FlightRecommendation Recommendation { get; set; }

        public string ExternalIdentifier { get; set; }

        public string StatusCode { get; set; }

        public CallStatus Status { get; set; }

        public string Message { get; set; }
    }
}
