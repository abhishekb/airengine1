﻿using System;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Common;

namespace Tavisca.TravelNxt.Flight.Entities.Pricing
{
    [Serializable] [ProtoBuf.ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ProviderSpacePriceConfiguration
    {
        public int Id { get; set; }
        public int PriceConfigurationId { get; set; }
        public int ProviderSpaceId { get; set; }
        public int PreferenceOrder { get; set; }
    }
}
