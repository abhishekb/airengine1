﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CallStatus
    {
        Success,
        Failure,
        Warning,
        InProgress
    }
}
