﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Flight.Common;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightLegCollection : IKeyedCollection<FlightLeg>
    {
        private readonly IList<FlightLeg> _internalList = new List<FlightLeg>();
        private const string LegSeparator = "||";
        public FlightLeg this[int index]
        {
            get { return _internalList[index]; }
            set { 
                if (value == null)
                    throw new ArgumentNullException("value", "FlightLegCollection does not support null values.");

                var previous = this[index];
            
                if (previous != null)
                {
                    this.Remove(previous);
                }
                _internalList[index] = value;
                Key = Key + LegSeparator + value.Key;
            }
        }

        private string _key = string.Empty;
        
        public string Key
        {
            get { return _key; }
            private set
            {
                _key = value;
            }
        }

        public void Add(FlightLeg item)
        {
            _internalList.Add(item);
            // || is the separator for each segment key
            Key = Key + LegSeparator + item.Key;

        }

        public void AddRange(IEnumerable<FlightLeg> items)
        {
            foreach (var flightLeg in items)
                Add(flightLeg);
        }

        public void Clear()
        {
            _internalList.Clear();
            Key = string.Empty;
        }

        public bool Contains(FlightLeg item)
        {
            return _internalList.Contains(item);
        }

        /// <summary>
        /// CopyTo method not supported for FlightLegs collection! Please do not use it
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        [Obsolete]
        public void CopyTo(FlightLeg[] array, int arrayIndex)
        {
            throw new NotSupportedException("CopyTo method not supported for FlightLegs collection!");
        }

        public int Count
        {
            get { return _internalList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(FlightLeg item)
        {
            // || is the separator for each segment key
            Key = Key.Replace(item.Key + "||", string.Empty);
            return _internalList.Remove(item);
        }


        public IEnumerator<FlightLeg> GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }

        public FlightLegCollection(IEnumerable<FlightLeg> flightLegs)
        {
            AddRange(flightLegs);
        }

        public FlightLegCollection()
        {
            
        }
    }
}