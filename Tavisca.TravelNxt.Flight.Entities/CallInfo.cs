﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class CompletionInfo
    {
        public string Code { get; set; }

        public CallStatus Status { get; set; }

        public string Message { get; set; }
    }
}
