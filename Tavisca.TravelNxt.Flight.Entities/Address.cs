﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class Address
    {
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public City City { get; set; }

        public string ZipCode { get; set; }

        public static explicit operator Address(TravelNxt.Common.Core.Contracts.Entities.Address address)
        {
            if (address == null)
                return null;

            return new Address()
                {
                    AddressLine1 = address.AddressLine1,
                    AddressLine2 = address.AddressLine2,
                    ZipCode = address.ZipCode,
                    City = new City()
                        {
                            Code = address.City.Code,
                            CountryCode = address.City.Country,
                            Name = address.City.Code,
                            StateCode = address.City.State,
                        }
                };
        }
    }
}
