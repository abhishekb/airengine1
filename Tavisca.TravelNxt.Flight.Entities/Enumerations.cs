﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum ItineraryTypeOptions
    {
        NotSet = 0,
        MultiCity = 1,
        OneWay = 2,
        RoundTrip = 3
    }

    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum ConfigurationType : int
    {
        NotSet = 0,
        Search = 1,
        Price = 2,
        Book = 3,
        Cancel = 4,
        SeatMap = 5,
        AirFareRules = 6
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum CustomerType
    {
        Agent,
        Admin,
        Traveller
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum ProviderSpaceType
    {
        All = 0,
        Domestic = 1,
        International = 2
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum FareAttribute
    {
        EconomyUnrestricted,
        NoAdvancePurchase,
        NoDayTimeRestriction,
        NoMaximumStayRestriction,
        NoMinimumStayRestriction,
        NoPenalty,
        WithPenalty,
        NoRestriction,
        NoMinMaxFare,
        NonRefundableFares,
        RefundableFares,
        IncludeBookingClasses,
        ExcludeBookingClasses,
        Unknown,
        CouponFare,
        FrozenFares
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum LegType
    {
        NotApplicable = 0,
        Onward = 1,
        Return = 2
    }
}
