﻿using System;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ConnectionPreference: IDeepCopyable<ConnectionPreference>
    {
        public string AirportCode { get; set; }

        public ConnectionPreferenceType PreferenceType { get; set; }
        
        public ConnectionPreference DeepCopy()
        {
            return new ConnectionPreference()
                {
                    AirportCode = AirportCode,
                    PreferenceType = PreferenceType
                };
        }
    }
}
