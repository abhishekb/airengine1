﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class AlternateAirportInformation : IDeepCopyable<AlternateAirportInformation>
    {
        public IList<string> AirportCodes { get; set; }
        public bool? IncludeNearByAirports { get; set; }
        public int? RadiusKm { get; set; }
        
        public AlternateAirportInformation DeepCopy()
        {
            return new AlternateAirportInformation()
                {
                    AirportCodes = AirportCodes == null ? null : new List<string>(AirportCodes),
                    IncludeNearByAirports = IncludeNearByAirports,
                    RadiusKm = RadiusKm
                };
        }

        public bool IsEmpty()
        {
            if ((AirportCodes == null || AirportCodes.Count == 0)
                && !IncludeNearByAirports.HasValue)
                return true;

            return false;
        }
    }
}
