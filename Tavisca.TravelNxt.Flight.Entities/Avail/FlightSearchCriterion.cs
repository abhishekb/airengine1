﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightSearchCriteria : IDeepCopyable<FlightSearchCriteria>
    {
        public Guid SessionID { get; set; }

        public IList<PaxTypeQuantity> PassengerInfoSummary { get; set; }

        public IList<SearchSegment> SearchSegments { get; set; }

        public SortingOrder SortingOrder { get; set; }

        public FlightTravelPreference FlightTravelPreference { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public int? MaxPreferredResults { get; set; }

        public FlightSearchCriteria DeepCopy()
        {
            return new FlightSearchCriteria()
                       {
                           AdditionalInfo = AdditionalInfo,
                           SortingOrder = SortingOrder,
                           FlightTravelPreference =
                               FlightTravelPreference == null
                                   ? GetEmptyFlightTravelPreferenceObject()
                                   : FlightTravelPreference.DeepCopy(),
                           PassengerInfoSummary =
                               PassengerInfoSummary == null
                                   ? null
                                   : PassengerInfoSummary.Select(x => x.DeepCopy()).ToListBuffered(),
                           SearchSegments =
                               SearchSegments == null ? null : SearchSegments.Select(x => x.DeepCopy()).ToListBuffered(),
                           SessionID = SessionID,
                           MaxPreferredResults = MaxPreferredResults
                       };
        }

        private static FlightTravelPreference GetEmptyFlightTravelPreferenceObject()
        {
            return new FlightTravelPreference()
                       {
                           AirlinesPreference =
                               new AirlinesPreference()
                                   {
                                       PermittedAirlines = new List<string>(),
                                       ProhibitedAirlines = new List<string>(),
                                       PreferredAirlines = new List<string>()
                                   }
                       };
        }
    }
}
