﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum PreferenceTypeOptions : int
    {
        Default = 0,
        Acceptable = 1,
        Unacceptable = 2
    }

    [Serializable] 
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum ConnectionPreferenceType
    {
        Via,
        Avoid
    }

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public enum SortingOrder
    {

        ByDepartureTime,

        ByArrivalTime,

        ByJourneyTime,

        ByPriceHighToLow,

        ByPriceLowToHigh,

        ByAirline,

        ByPreference
    }
}
