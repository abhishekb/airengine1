﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightTravelPreference : IDeepCopyable<FlightTravelPreference>
    {
        public bool IncludeNonStopOnly { get; set; }

        public bool IncludeDirectOnly { get; set; }

        public bool JetFlightsOnly { get; set; }

        public bool AllowMixedAirlines { get; set; }

        public bool UnRestrictedFare { get; set; }

        public bool NoPenalty { get; set; }

        public bool? Refundable { get; set; }

        public AirlinesPreference AirlinesPreference { get; set; }

        public FlightTravelPreference()
        {
            AirlinesPreference = new AirlinesPreference();
        }

        public FlightTravelPreference DeepCopy()
        {
            var copy = new FlightTravelPreference()
                {
                    AllowMixedAirlines = AllowMixedAirlines,
                    JetFlightsOnly = JetFlightsOnly,
                    Refundable = Refundable,
                    UnRestrictedFare = UnRestrictedFare,
                    IncludeDirectOnly = IncludeDirectOnly,
                    NoPenalty = NoPenalty,
                    IncludeNonStopOnly = IncludeNonStopOnly
                };

            if (AirlinesPreference == null)
            {
                copy.AirlinesPreference = new AirlinesPreference()
                                              {
                                                  PermittedAirlines = new List<string>(),
                                                  ProhibitedAirlines = new List<string>(),
                                                  PreferredAirlines = new List<string>()
                                              };
                return copy;
            }
            copy.AirlinesPreference = new AirlinesPreference
                                          {
                                              PermittedAirlines = AirlinesPreference.PermittedAirlines == null
                                                                      ? new List<string>()
                                                                      : AirlinesPreference.PermittedAirlines.Select(
                                                                          x => x).
                                                                            ToListBuffered(),
                                              ProhibitedAirlines = AirlinesPreference.ProhibitedAirlines == null
                                                                       ? new List<string>()
                                                                       : AirlinesPreference.ProhibitedAirlines.Select(
                                                                           x => x).
                                                                             ToListBuffered(),
                                              PreferredAirlines = AirlinesPreference.PreferredAirlines == null
                                                                      ? new List<string>()
                                                                      : AirlinesPreference.PreferredAirlines.Select(
                                                                          x => x).
                                                                            ToListBuffered()
                                          };

            return copy;
        }
    }
}