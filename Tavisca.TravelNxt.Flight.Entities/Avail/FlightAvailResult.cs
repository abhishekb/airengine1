﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightAvailResult
    {
        public IList<FlightRecommendation> FlightRecommendations { get; set; }

        public AdditionalInfoDictionary AdditionalInfo { get; set; }

        public string StatusCode { get; set; }

        public CallStatus Status { get; set; }

        public string StatusMessage { get; set; }

        public IList<string> Messages { get; set; }

        public DateTime TimeStamp { get; set; }

        public FlightAvailResult()
        {
            TimeStamp = DateTime.UtcNow;
        }
    }
}
