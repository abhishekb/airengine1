﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;
using System.Linq;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class AirlinesPreference: IDeepCopyable<AirlinesPreference>
    {
        public IList<string> PreferredAirlines { get; set; }

        public IList<string> ProhibitedAirlines { get; set; }

        public IList<string> PermittedAirlines { get; set; }

        public AirlinesPreference DeepCopy()
        {
            return new AirlinesPreference()
                       {
                           PermittedAirlines =
                               PermittedAirlines == null ? null : PermittedAirlines.Select(x => x).ToListBuffered(),
                           ProhibitedAirlines =
                               ProhibitedAirlines == null ? null : ProhibitedAirlines.Select(x => x).ToListBuffered(),
                           PreferredAirlines =
                               PreferredAirlines == null ? null : PreferredAirlines.Select(x => x).ToListBuffered()
                       };
        }
    }
}
