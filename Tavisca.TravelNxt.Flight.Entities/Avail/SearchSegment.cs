﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class SearchSegment: IDeepCopyable<SearchSegment>
    {
        public string DepartureAirportCode { get; set; }

        public string ArrivalAirportCode { get; set; }

        public string DepartureCityCode { get; set; }
        
        public string ArrivalCityCode { get; set; }

        public TravelDate TravelDate { get; set; }

        public CabinType Cabin { get; set; }

        public AlternateAirportInformation ArrivalAlternateAirportInformation { get; set; }
        public AlternateAirportInformation DepartureAlternateAirportInformation { get; set; }

        private IList<ConnectionPreference> _connectionPreferences = new List<ConnectionPreference>();
        public IList<ConnectionPreference> ConnectionPreferences
        {
            get { return _connectionPreferences; }
            set
            {
                _connectionPreferences = value ?? new List<ConnectionPreference>();
            }
        }

        private IList<string> _includeServiceClass = new List<string>();
        public IList<string> IncludeServiceClass
        {
            get { return _includeServiceClass; }
            set { _includeServiceClass = value ?? new List<string>(); }
        }

        public SearchSegment DeepCopy()
        {
            return new SearchSegment()
                {
                    TravelDate = TravelDate == null? null: TravelDate.DeepCopy(),
                    ArrivalAirportCode = ArrivalAirportCode,
                    Cabin = Cabin,
                    ConnectionPreferences = ConnectionPreferences == null ? null : ConnectionPreferences.Select(x => x.DeepCopy()).ToListBuffered(),
                    DepartureAirportCode = DepartureAirportCode,
                    ArrivalAlternateAirportInformation = ArrivalAlternateAirportInformation == null ? null : ArrivalAlternateAirportInformation.DeepCopy(),
                    DepartureAlternateAirportInformation = DepartureAlternateAirportInformation == null ? null : DepartureAlternateAirportInformation.DeepCopy(),
                    IncludeServiceClass = new List<string>(IncludeServiceClass),
                    ArrivalCityCode = ArrivalCityCode,
                    DepartureCityCode = DepartureCityCode
                };
        }

        public string GetDepartureLocation()
        {
            return !string.IsNullOrWhiteSpace(DepartureAirportCode) ? DepartureAirportCode : DepartureCityCode;
        }

        public string GetArrivalLocation()
        {
            return !string.IsNullOrWhiteSpace(ArrivalAirportCode) ? ArrivalAirportCode : ArrivalCityCode;
        }

        public bool IsArrivalSpecCity()
        {
            return !string.IsNullOrWhiteSpace(ArrivalCityCode);
        }

        public bool IsDepartureSpecCity()
        {
            return !string.IsNullOrWhiteSpace(DepartureCityCode);
        }
    }
}
