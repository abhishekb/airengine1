﻿using System;
using ProtoBuf;

namespace Tavisca.TravelNxt.Flight.Entities.Avail
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class FlightAvailRq
    {
        public ProviderSpace ProviderSpace { get; private set; }

        public FlightSearchCriteria SearchCriteria { get; private set; }

        public  FlightAvailRq()
        {}

        public FlightAvailRq(ProviderSpace providerSpace, FlightSearchCriteria searchCriteria)
        {
            ProviderSpace = providerSpace;
            SearchCriteria = searchCriteria;
        }
    }
}
