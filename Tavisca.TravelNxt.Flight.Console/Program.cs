﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.TaskScheduling;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.Frameworks.Serialization.Compression;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Settings.Injection;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Avail.DataContract;
using Tavisca.TravelNxt.Flight.Avail.ServiceImpl;
using CallContext = Tavisca.TravelNxt.Common.Extensions.CallContext;

namespace Tavisca.TravelNxt.Flight.Console
{
    class Program
    {
        private const int ParallelRequestCount = 20;
        static void Main(string[] args)
        {
            InjectConfigs();

            var requestText = GetJsonRequest();

            var reqData = Encoding.UTF8.GetBytes(requestText);

            var request = Deserialize<FlightSearchRQ>(reqData);

            var clonedRequests = new List<FlightSearchRQ>(ParallelRequestCount);

            for (var i = 0; i < ParallelRequestCount; i++)
            {
                clonedRequests.Add(Deserialize<FlightSearchRQ>(reqData));
            }

            request.SearchCriterion.MaxPreferredResults = null;
            try
            {
                DoSearch(request);

                var facade = Frameworks.Parallel.TaskParallelismFacadeFactory
                    .GetParallelismFacade<FlightSearchRQ, FlightSearchRS>(DoSearch, 60000);

                var result = facade.Execute(
                    Enumerable.Range(1, ParallelRequestCount).Select(x => clonedRequests[x - 1]).ToListBuffered(
                        ParallelRequestCount));

                foreach (var executionResult in result)
                {
                    if (executionResult.Error != null)
                        System.Console.WriteLine(executionResult.Error.ToString());
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                System.Console.ReadLine();
            }
            System.Threading.Thread.Sleep(2000);
        }

        private static void InjectConfigs()
        {
            var injector = RuntimeContext.Resolver.Resolve<IConfigurationInjector>();

            injector.InjectConfigurations();
        }

        private static FlightSearchRS DoSearch(FlightSearchRQ request)
        {
            using (var scope = new AmbientContextScope(GetContext()))
            {
                var serviceImpl = new AirEngine();

                var response = serviceImpl.Search(request);

                if (response == null)
                    throw new Exception("response not recieved");

                //System.Threading.Thread.Sleep(5000);

                //var data = serviceImpl.TryGetFromSession(new FlightPollRQ()
                //{
                //    GetAsSupplierResponds = false,
                //    PreviousTimeStamp = response.TimeStamp
                //});

                //if (data == null)
                //    throw new Exception("try get response not recieved");

                return response;
            }
        }

        //private static FlightSearchRQ DeepCopy(FlightSearchRQ request)
        //{
        //    //var serializer = new Frameworks.Serialization.SerializationFactory().GetSerializationFacade(SerializerType.NewtonsoftJsonNetSerializer, CompressionTypeOptions.None);

        //    //return serializer.DeepClone(request);

        //    return new FlightSearchRQ();
        //}

        private static AmbientContextBase GetContext()
        {
            return new CallContext(GetTestSetting("MockCulture"), GetSessionId(), GetTestSetting("MockPosId"),
                                   GetTestSetting("MockGetAccountId"), GetTestSetting("DisplayCurrency"),
                                   GetTestSetting("Password"), false, GetTestSetting("BaseCurrency"));
        }

        private const bool ReuseSession = false;
        private static string _sessionId;

        private static string GetSessionId()
        {
            //if (ReuseSession)
            {
                return _sessionId ?? (_sessionId = Guid.NewGuid().ToString());
            }

            //return Guid.NewGuid().ToString();
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private static string GetJsonRequest()
        {
            var text = File.ReadAllText(@"Requests\JsonRequest.txt");

            return text;
        }

        private static string Serialize<T>(T obj)
        {
            var ser = new DataContractJsonSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.Default.GetString(ms.ToArray());
            }
            return json;
        }

        private static T Deserialize<T>(byte[] data)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }
    }
}
