﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Frameworks.Parallel.Ambience.Wcf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.FareRules.Core.Exceptions;
using Tavisca.TravelNxt.Flight.FareRules.DataContract;
using Tavisca.TravelNxt.Flight.FareRules.ServiceContract;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.FareRules.ServiceImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AmbientOperation(ApplicationName = "Air Fare Rules")]
    public class AirFareRules : IAirFareRules
    {
        static AirFareRules()
        {
            Utility.SetApplicationName("Air Engine");
        }

        #region IAirFareRules Members

        public FareRulesRS GetFareRules(FareRulesRQ request)
        {
            if (request == null)
                throw new FaultException<ArgumentNullException>(new ArgumentNullException("request"));

            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            ILogger logFactory = null;
            IEventEntry entry = null;
            try
            {
                logFactory = Utility.GetLogFactory();

                entry = Utility.GetEventEntryContextual();

                entry.Title = "Fare rules - Root Level fare rule Log";

                entry.CallType = KeyStore.CallTypes.AirFareRules;

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var fareRuleEngine = RuntimeContext.Resolver.Resolve<IFareRulesEngine>();

                var entity = FareRulesRQ.ToEntity(request);

                var result = fareRuleEngine.GetFareRules(entity);

                var retVal = FareRulesRS.ToDataContract(result);

                //entry.TimeTaken = (DateTime.Now - startTime).TotalSeconds;

                if (retVal != null)
                {
                    entry.ResponseObject = retVal;
                    if (retVal.ServiceStatus != null && retVal.ServiceStatus.Status == CallStatus.Failure)
                        entry.StatusType = StatusOptions.Failure;
                }

                return retVal;
            }
            catch (HandledException handledException)
            {
                if (entry != null) 
                    entry.StatusType = StatusOptions.Failure;

                return handledException.InnerException.GetType() == typeof(FareRuleSupplierException)
                           ? GetFailureResponse(KeyStore.ErrorCodes.SupplierFailure)
                           : GetFailureResponse();
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                if (entry != null)
                    entry.StatusType = StatusOptions.Failure;

                return GetFailureResponse();
            }
            finally
            {
                if (logFactory != null && entry != null)
                {
                    stopwatch.Stop();
                    entry.TimeTaken = stopwatch.ElapsedSeconds;
                    logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
                }
            }
        }

        #endregion

        #region private methods

         private static FareRulesRS GetFailureResponse(string errorCode = KeyStore.ErrorCodes.InternalError)
         {
             return new FareRulesRS()
                        {
                            ServiceStatus = new ServiceStatus()
                                                {
                                                    Messages = new List<string>(),
                                                    Status = CallStatus.Failure,
                                                    StatusCode = KeyStore.ErrorCodes.InternalError,
                                                    StatusMessage =
                                                        ErrorCodeManager.GetErrorMessage(errorCode,
                                                                                         KeyStore.ErrorMessages.
                                                                                             DefaultErrorMessage)
                                                },
                            SessionId = Utility.GetCurrentContext().SessionId
                        };
         }

        #endregion
    }
}
