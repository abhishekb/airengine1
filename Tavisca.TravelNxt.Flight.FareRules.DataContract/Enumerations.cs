﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public enum CallStatus
    {
        [EnumMember]
        Success = 0,
        [EnumMember]
        Failure = 1,
        [EnumMember]
        Warning = 2,
        [EnumMember]
        InProgress = 3
    }
}
