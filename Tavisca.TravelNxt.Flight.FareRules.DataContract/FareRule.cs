﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public class FareRule
    {
        [DataMember]
        public string FareBasisCode { get; set; }
        [DataMember]
        public List<FareRuleDetail> FareRuleDetails { get; set; }

        #region Translations

        public static FareRule ToDataContract(Entities.FareRules.FareRule entity)
        {
            var contract = new FareRule()
                {
                    FareBasisCode = entity.FareBasisCode,
                    FareRuleDetails = entity.FareRuleDetails == null ? null : entity.FareRuleDetails.Select(FareRuleDetail.ToDataContract).ToListBuffered()
                };

            return contract;
        }

        public static Entities.FareRules.FareRule ToEntity(FareRule contract)
        {
            var entity = new Entities.FareRules.FareRule()
                {
                    FareBasisCode = contract.FareBasisCode,
                    FareRuleDetails = contract.FareRuleDetails == null ? null : contract.FareRuleDetails.Select(FareRuleDetail.ToEntity).ToListBuffered()
                };

            return entity;
        }

        #endregion
    }
}
