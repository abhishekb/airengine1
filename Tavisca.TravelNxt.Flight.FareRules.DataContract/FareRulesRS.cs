﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public class FareRulesRS
    {
        [DataMember]
        public Guid SessionId { get; set; }
        [DataMember]
        public ServiceStatus ServiceStatus { get; set; }
        [DataMember]
        public List<FareRule> FareRules { get; set; }

        #region Translations

        public static FareRulesRS ToDataContract(Entities.FareRules.FareRulesRS entity)
        {
            var contract = new FareRulesRS()
                {
                    FareRules = entity.FareRules == null ? null : entity.FareRules.Select(FareRule.ToDataContract).ToListBuffered(),
                    SessionId = entity.SessionId,
                    ServiceStatus = ServiceStatus.ToDataContract(entity.ServiceStatus)
                };

            return contract;
        }

        public static Entities.FareRules.FareRulesRS ToEntity(FareRulesRS contract)
        {
            var entity = new Entities.FareRules.FareRulesRS()
                {
                    FareRules = contract.FareRules == null ? null : contract.FareRules.Select(FareRule.ToEntity).ToListBuffered(),
                    ServiceStatus = ServiceStatus.ToEntity(contract.ServiceStatus),
                    SessionId = contract.SessionId
                };

            return entity;
        }
        
        #endregion
    }
}
