﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public class FareRulesRQ
    {
        [DataMember]
        public int RecommendationRefID { get; set; }
        //[DataMember]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        #region Translations

        public static FareRulesRQ ToDataContract(Entities.FareRules.FareRulesRQ entity)
        {
            var contract = new FareRulesRQ()
                {
                    AdditionalInfo = AdditionalInfoDictionary.ToDictionary(entity.AdditionalInfo),
                    RecommendationRefID = entity.RecommendationRefID
                };

            return contract;
        }

        public static Entities.FareRules.FareRulesRQ ToEntity(FareRulesRQ contract)
        {
            var entity = new Entities.FareRules.FareRulesRQ()
                {
                    AdditionalInfo = new AdditionalInfoDictionary(contract.AdditionalInfo),
                    RecommendationRefID = contract.RecommendationRefID
                };

            return entity;
        }

        #endregion
    }
}
