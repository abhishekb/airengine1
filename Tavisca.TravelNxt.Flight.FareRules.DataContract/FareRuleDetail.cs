﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/AirFareRule/V1")]
    public class FareRuleDetail
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Text { get; set; }

        #region Translations

        public static FareRuleDetail ToDataContract(Entities.FareRules.FareRuleDetail entity)
        {
            var contract = new FareRuleDetail()
            {
                Text = entity.Text,
                Title = entity.Title
            };

            return contract;
        }

        public static Entities.FareRules.FareRuleDetail ToEntity(FareRuleDetail contract)
        {
            var entity = new Entities.FareRules.FareRuleDetail()
            {
                Text = contract.Text,
                Title = contract.Title
            };

            return entity;
        }

        #endregion
    }
}
