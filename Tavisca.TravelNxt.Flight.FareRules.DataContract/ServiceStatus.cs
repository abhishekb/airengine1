using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Flight.FareRules.DataContract
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class ServiceStatus
    {
        [DataMember]
        public string StatusCode { get; set; }

        [DataMember]
        public CallStatus Status { get; set; }

        [DataMember]
        public List<string> Messages { get; set; }

        [DataMember]
        public string StatusMessage { get; set; }

        #region Translation

        public static ServiceStatus ToDataContract(Entities.FareRules.ServiceStatus entity)
        {
            var contract = new ServiceStatus()
            {
                Messages = new List<string>(entity.Messages),
                Status = ToDataContract(entity.Status),
                StatusCode = entity.StatusCode,
                StatusMessage = entity.StatusMessage
            };

            return contract;
        }

        public static Entities.FareRules.ServiceStatus ToEntity(ServiceStatus contract)
        {
            var entity = new Entities.FareRules.ServiceStatus()
            {
                Messages = new List<string>(contract.Messages),
                Status = ToEntity(contract.Status),
                StatusCode = contract.StatusCode,
                StatusMessage = contract.StatusMessage
            };

            return entity;
        }

        public static CallStatus ToDataContract(Entities.CallStatus entity)
        {
            switch (entity)
            {
                case Entities.CallStatus.InProgress:
                    return CallStatus.InProgress;
                case Entities.CallStatus.Failure:
                    return CallStatus.Failure;
                case Entities.CallStatus.Success:
                    return CallStatus.Success;
                case Entities.CallStatus.Warning:
                    return CallStatus.Warning;
            }
            throw new NotSupportedException("call status not supported - " + entity);
        }

        public static Entities.CallStatus ToEntity(CallStatus contract)
        {
            switch (contract)
            {
                case CallStatus.InProgress:
                    return Entities.CallStatus.InProgress;
                case CallStatus.Failure:
                    return Entities.CallStatus.Failure;
                case CallStatus.Success:
                    return Entities.CallStatus.Success;
                case CallStatus.Warning:
                    return Entities.CallStatus.Warning;
            }
            throw new NotSupportedException("call status not supported - " + contract);
        }
        #endregion
    }
}