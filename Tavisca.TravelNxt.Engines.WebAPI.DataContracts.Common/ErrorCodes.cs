﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions
{
    public class ErrorCodes
    {
        public static readonly string ARGUMENT_INVALID = "AE0001";
        public static readonly string ARGUMENT_NULL = "AE0002";
        public static readonly string ARGUMENT_OUT_OF_RANGE = "AE0003";
        public static readonly string ACCESS_ERROR = "ACE0001";
        public static readonly string ACCESS_BAD_USER = "ACE0002";
        public static readonly string ACCESS_IP_ERROR = "ACE0003";
        public static readonly string ACCESS_CALL_LIMIT_CROSSED_ERROR = "ACE0004";
        public static readonly string CONFIG_ERROR = "CCE0001";

        public static readonly string UNKNOWN_ERROR = "UE0001";

        public static readonly string OBJECT_INVALID = "OE0001";
        public static readonly string OBJECT_NULL = "OE0002";
        public static readonly string OBJECT_OUT_OF_RANGE = "OE0003";

    }
}
