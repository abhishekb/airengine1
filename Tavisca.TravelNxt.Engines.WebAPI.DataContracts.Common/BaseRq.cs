﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract]
    public class BaseRq
    {
        [DataMember]
        public string Uid { get; set; }

        [DataMember]
        public string CacheId { get; set; }

        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string IpAddress { get; set; }
    }
}
