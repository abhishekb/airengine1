﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class SearchBaseRs : BaseRs
    {
        [DataMember]
        public int NumResults { get; set; }
    }
}
