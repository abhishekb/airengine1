﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [Serializable]
    public class InvalidConfigurationException : BaseApplicationException
    {
        public InvalidConfigurationException(string message)
            : base(message)
        {
            ErrorCode = ErrorCodes.CONFIG_ERROR;
        }

        public InvalidConfigurationException(string message, Exception inner)
            : base(message, inner)
        {
            ErrorCode = ErrorCodes.CONFIG_ERROR;
        }
    }
}
