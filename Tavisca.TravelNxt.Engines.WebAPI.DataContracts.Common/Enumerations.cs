﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum AirPassengerType
    {
        [EnumMember] Infant,
        [EnumMember] Child,
        [EnumMember] Adult,
        [EnumMember] Senior,
        [EnumMember] Mapped,
        [EnumMember] MilitaryAdult,
        [EnumMember] MilitaryChild,
        [EnumMember] MilitaryInfant,
        [EnumMember] MilitarySenior
    }

    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum AirFareComponentType
    {
        [EnumMember] Tax,
        [EnumMember] Fees,
        [EnumMember] Discount,
        [EnumMember] Commision
    }
}