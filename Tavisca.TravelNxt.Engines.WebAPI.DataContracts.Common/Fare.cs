﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Fare
    {
        [DataMember]
        public decimal BaseFare { get; set; }

        [DataMember]
        public decimal TotalFare { get; set; }

        [DataMember]
        public decimal FeesPlusTaxes { get; set; }

        [DataMember]
        public decimal Discount { get; set; }

        [DataMember]
        public decimal Commission { get; set; }

        [DataMember]
        public List<AirPassengerFare> PassengerFares { get; set; }
    }
}
