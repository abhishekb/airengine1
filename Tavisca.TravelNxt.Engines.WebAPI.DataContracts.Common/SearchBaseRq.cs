﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    //  [Serializable][DataContract(Name = "SearchBaseRequest")]
    [DataContract]
    public class SearchBaseRq : BaseRq
    {
        [DataMember]
        public int NumResults { get; set; }
    }
}
