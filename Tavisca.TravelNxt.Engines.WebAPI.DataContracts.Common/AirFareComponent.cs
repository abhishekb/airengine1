﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirFareComponent
    {
        [DataMember]
        public AirFareComponentType AirFareComponentType { get; set; }

        [DataMember]
        public string Code { get; set; }     

        [DataMember]
        public decimal Amount { get; set; }
    }
}
