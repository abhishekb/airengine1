﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions
{
    [Serializable]
    public class BaseApplicationException : Exception
    {
        private const string ErrorCodeKey = "ErrorCode";

        public BaseApplicationException(string message) : base(message) { }

        public BaseApplicationException(string message, Exception inner) : base(message, inner) { }

        public string ErrorCode { get; set; }

        public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            info.AddValue(ErrorCodeKey, ErrorCode);
            base.GetObjectData(info, context);
        }
    }
}
