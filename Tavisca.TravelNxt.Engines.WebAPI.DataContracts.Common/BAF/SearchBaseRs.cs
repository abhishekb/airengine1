﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class SearchBaseRs : BaseRs
    {
        [DataMember]
        public int NumResults { get; set; }
    }
}
