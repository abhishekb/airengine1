﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;


namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class BaseRs
    {
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public string CacheId { get; set; }

        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public string Culture { get; set; }
    }
}
