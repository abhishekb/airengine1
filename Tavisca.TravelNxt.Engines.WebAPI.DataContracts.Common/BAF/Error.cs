﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Error
    {        
        [DataMember]
        public string ErrorCode { get; set; }
        
        public string ErrorMessage { get; set; }
    }
}
