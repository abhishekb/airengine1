﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Fare
    {
        [DataMember]
        public decimal BaseFare { get; set; }

        [DataMember]
        public decimal TotalFare { get; set; }

        [DataMember]
        public decimal FeesPlusTaxes { get; set; }

        [DataMember]
        public decimal Discount { get; set; }

        [DataMember]
        public decimal Commission { get; set; }
    }
}
