﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.Exceptions
{
    [Serializable]
    public class InvalidArgumentException : BaseApplicationException
    {
        public InvalidArgumentException()
            : base(GetMessage("", ArgumentStatus.Invalid))
        {
            ErrorCode = ErrorCodes.ARGUMENT_INVALID;
        }

        public InvalidArgumentException(string message, Exception inner)
            : base(message, inner)
        {
            ErrorCode = ErrorCodes.ARGUMENT_INVALID;
        }

        public InvalidArgumentException(string argumentName, ArgumentStatus status)
            : base(GetMessage(argumentName, status))
        {
            switch (status)
            {
                case ArgumentStatus.Null:
                    ErrorCode = ErrorCodes.ARGUMENT_NULL;
                    break;
                case ArgumentStatus.Empty:
                    ErrorCode = ErrorCodes.ARGUMENT_INVALID;
                    break;
                case ArgumentStatus.OutOfRange:
                    ErrorCode = ErrorCodes.ARGUMENT_OUT_OF_RANGE;
                    break;
                default:
                    ErrorCode = ErrorCodes.ARGUMENT_INVALID;
                    break;
            }
        }

        public InvalidArgumentException(string argumentName, ArgumentStatus status, string validValues)
            : base(GetMessage(argumentName, status, validValues))
        {
            switch (status)
            {
                case ArgumentStatus.Null:
                    ErrorCode = ErrorCodes.ARGUMENT_NULL;
                    break;
                case ArgumentStatus.Empty:
                    ErrorCode = ErrorCodes.ARGUMENT_INVALID;
                    break;
                case ArgumentStatus.OutOfRange:
                    ErrorCode = ErrorCodes.ARGUMENT_OUT_OF_RANGE;
                    break;
                default:
                    ErrorCode = ErrorCodes.ARGUMENT_INVALID;
                    break;
            }
        }

        public static string GetMessage(string argumentName, ArgumentStatus status)
        {
            return string.Format("Argument {1} error. {0} is {1}", argumentName, status);
        }

        public static string GetMessage(string argumentName, ArgumentStatus status, string validValues)
        {
            return string.Format("Argument {1} error. {0} is {1}. Valid values are {2}", argumentName, status, validValues);
        }
    }

    public enum ArgumentStatus
    {
        Empty,
        Null,
        Invalid,
        OutOfRange
    }
}
