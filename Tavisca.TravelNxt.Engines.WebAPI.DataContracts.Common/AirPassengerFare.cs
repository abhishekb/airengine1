﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirPassengerFare
    {       
        [DataMember]
        public List<AirFareComponent> Taxes { get; set; }

        [DataMember]
        public List<AirFareComponent> Commissions { get; set; }

        [DataMember]
        public List<AirFareComponent> Fees { get; set; }

        [DataMember]
        public List<AirFareComponent> Discounts { get; set; }        

        //[DataMember]
        //public decimal TotalFees { get; set; }

        //[DataMember]
        //public decimal TotalCommission { get; set; }

        //[DataMember]
        //public decimal TotalTaxes { get; set; }

        //[DataMember]
        //public decimal TotalDiscount { get; set; }

        [DataMember]
        public decimal BaseAmount { get; set; }

        [DataMember]
        public decimal TotalAmount { get; set; }

        [DataMember]
        public AirPassengerType AirPassengerType { get; set; }

        [DataMember]
        public int Quantity { get; set; }
    }
}
