﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class BaseRs
    {
        [DataMember]
        public Error Error { get; set; }

        [DataMember]
        public string CacheId { get; set; }

        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public string Culture { get; set; }
    }
}
