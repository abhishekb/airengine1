﻿namespace Tavisca.TravelNxt.Flight.Settings
{
    public static class KeyStore
    {
        public const string DefaultCurrency = "USD";

        public static class AttributeKeys
        {
            public const string CouponFare = "Coupon fare";
        }

        public static class CallTypes
        {
            public const string AirSearch = "AirSearch";
            public const string DeferredAirSearch = "DeferredAirSearch";
            public const string AirBook = "AirBook";
            public const string AirBookingRetrieve = "AirBookingRetrieve";
            public const string AirFareRules = "AirFareRules";
            public const string AirPrice = "AirPrice";
            public const string AirSeatmap = "AirSeatmap";
            public const string SupplierAirSeatmap = "SupplierAirSeatmap";
            public const string AirPriceRetrieve = "AirPriceRetrieve";
            public const string SupplierAirPrice = "SupplierAirPrice";
            public const string SupplierAirFareRules = "SupplierAirFareRules";
            public const string SupplierAirSearch = "SupplierAirSearch";
            public const string CachedSupplierAirSearch = "CachedSupplierAirSearch";
            public const string LongTermDiagnostics = "LongTermDiagnostics";
            public const string TryGetFromSession = "TryGetFromSession";
            public const string MultipleLegsInOneFareDetailFound = "MultipleLegsInOneFareDetailFound";
            public const string FilterProviderSpaces = "FilterProviderSpaces";
            public const string WebAPI_AirSearch = "WebAPI_AirSearch";
            public const string WebAPI_AirMetaSearch = "WebAPI_AirMetaSearch";
        }

        public static class AdditionalData
        {
            public const string SourceLocationCode = "SourceLocationCode";
            public const string DestinationLocationCode = "DestinationLocationCode";
            public const string TravelDateTime = "TravelDateTime";
            public const string SourceCountryCode = "SourceCountryCode";
            public const string DestinationCountryCode= "DestinationCountryCode";
        }

        public static class AppSettings
        {
            public const string UseClassOfServiceInItineraryIdentification =
                "UseClassOfServiceInItineraryIdentification";
            public const string CommonServiceRoot = "CommonServiceRoot";
            public const string CommonContentServiceRoot = "CommonContentServiceRoot";
            public const string TicketlessCarriers = "TicketlessCarriers";
            public const string DefaultTimeWindow = "DefaultTimeWindow";
            public const string ProviderSpaceEmulationChainingLimit = "ProviderSpaceEmulationChainingLimit";
            public const string AvailSessionTimeOut = "AvailSessionTimeOut";
            public const string NearbyAirportRadius = "NearbyAirportRadius";
            public const string MaxNearbyAirportRadius = "MaxNearbyAirportRadius";
            public const string SupplierThreadTimeoutInSeconds = "SupplierThreadTimeoutInSeconds";
            public const string DistanceUnit = "DistanceUnit";
            public const string AgencyHomeCountryCode = "AgencyHomeCountryCode";
            public const string SupplierSchedulerType = "SupplierSchedulerType";
            public const string ScepterCacheExpirationHours = "ScepterCacheExpirationHours";
            public const string AirTimeAheadInHours = "AirTimeAheadInHours";
            public const string MaxValidAgeForChild = "MaxValidAgeForChild";
            public const string DefaultAdultAge = "DefaultAdultAge";
            public const string MinTravelDateOffsetHours = "MinTravelDateOffsetHours";
            public const string BackOfficeSettingsExpirationInSeconds = "BackOfficeSettingsExpirationInSeconds";
            public const string NonCouponFareCustomerTypes = "NonCouponFareCustomerTypes";
            public const string CacheRangeAndTime = "CacheRangeAndTime";
            public const string SceptrLoggingEnabled = "SceptrLoggingEnabled";
            public const string AirEngineSearchCallLoggingEnabled = "AirEngineSearchCallLoggingEnabled";
            public const string WebAPICallLoggingEnabled = "WebAPICallLoggingEnabled";
            public const string WebAPI_PreferredAirlines = "WebAPI_PreferredAirlines";
            public const string WebAPI_BlockedAirlines = "WebAPI_BlockedAirlines";
            public const string WebAPI_RequiredAirlines = "WebAPI_RequiredAirlines";
        }

        public static class ProviderSpaceAttributes
        {
            public const string TimeWindowKey = "TimeWindow";
            public const string ShowDiscount = "ShowDiscount";
            public const string PrivateCode = "PrivateCode";
            public const string CountryPairRestriction = "CountryPairRestriction";
            public const string IsPrivateFareOnly = "IsPrivateFareOnly";
            public const string MaxAllowedChildAge = "MaxAllowedChildAge";
        }

        public static class Scepter
        {
            public const string SupplierRefKey = "SupplierRefKey";
            public const string FareSearchSupplierFareRefKey = "FareSearchSupplierFareRefKey";
        }

        public static class LogCategories
        {
            public static class AdditionalInfoKeys
            {
                public const string ProviderName =
                    Common.Settings.KeyStore.LogCategories.AdditionalInfoKeys.ProviderName;
            }

            public const string Default = "Default";
            public const string ServiceRootLevel = "ServiceRootLevel";
            public const string FareManagement = "FareManagement";
            public const string Core = "Core";
            public const string LongTermDiagnostics = "LongTermDiagnostics";

            public static class ExceptionCategories
            {
                public const string Critical = "ExceptionCritical";
                public const string Regular = "ExceptionRegular";
                public const string Expected = "ExceptionExpected";
            }
        }

        public static class CacheKeys
        {
            public const string TicketingAgreements = "TicketingAgreements";
            public const string PosTicketingAirlinesInfo = "PosTicketingAirlinesInfo";
            public const string ClassOfService = "ClassOfService";
            public const string AirlineZones = "AirlineZones";
            public const string AirlineOperatingAirports = "AirlineOperatingAirports";
            public const string NearbyAirports = "NearbyAirports";
            public const string PosFareSourceStrategySetting = "PosFareSourceStrategySetting";
            public const string ProviderSpaces = "ProviderSpaces-{0}-{1}";
            public const string ProviderSpacePlain = "ProviderSpacePlain-{0}-{1}";
            public const string AirlinesCacheKey = "AirlinesCache-{0}";
            public const string AirportCacheKey = "AirportCache-{0}";
            public const string CityCacheKey = "CityCache-{0}";
            public const string PriceConfigurationsCacheKey = "PriceConfigurations";
            public const string PosFaresourceCarriers = "POSFARESOURCECARRIERS";
            public const string TicketingAgreementMap = "TicketingAgreementMap";
            public const string SupplierCurrencyContext = "SupplierCurrencyContext";
            public const string UIDMappingXmlCacheKey = "UIDMappingXmlCache";
        }

        public static class CacheBucket
        {
            public const string AirSettings = "AirSettings";
            public const string AirportContent = "AirportContent";
            public const string CityContent = "CityContent";
            public const string AirMarkup = "AirMarkup";
            public const string ScepterCacheBucket = "ScepterCacheBucket";
            public const string SupplierConfiguration = "SupplierConfiguration";
            public const string UIDMappingXMLConfiguration = "UIDMappingXMLConfiguration";
        }

        public static class Repositories
        {
            public const string AirConfiguration = "airConfiguration";
            public const string AirConfigurationReadOnly = "airConfigurationReadOnly";
            public const string Markup = "markup";
            public const string MarkupReadOnly = "markupReadOnly";
            public const string AirBook = "airBook";
            public const string AirBookReadOnly = "airBookReadOnly";

            public static class DatabaseConnections
            {
                public const string AirBookDB = "AirBookDB";
                public const string AirConfigurationDB = "AirConfigurationDB";
            }
        }

        public static class SingularityNameKeys
        {
            public const string SearchDefaultProvider = "searchDefaultProvider";
            public const string SearchMockDefaultProvider = "searchMockDefaultProvider";

            public const string SeatMapDefaultProvider = "seatMapDefaultProvider";
            public const string SeatMapMockDefaultProvider = "seatMapMockDefaultProvider";

            public const string PriceDefaultProvider = "priceDefaultProvider";
            public const string PriceMockDefaultProvider = "priceMockDefaultProvider";

            public const string FareRulesDefaultProvider = "fareRulesDefaultProvider";
            public const string FareRulesMockDefaultProvider = "fareRulesMockDefaultProvider";

            public const string AirFareManager = "AirFareManager";

            public const string ChildPaxTransform = "ChildPaxTransform";

            public const string DefaultKeyProvider = "Default";
            public const string CouponFareKeyProvider = "CouponFare";
        }

        public static class EnumValues
        {
            public const string Any = "Any";
            public const string All = "All";
        }

        public static class MockKeys
        {
            public const string Mock = "mock";
            public const string MockCategory = "Scepter";
            public const string MockUrlBase = "MockUrlBase";

            public static class Scenario
            {
                public const string OneWay = "OneWay";
                public const string Roundtrip = "RoundTrip";
                public const string MultiCity = "MultiCity";
            }
        }

        public static class ClientEndpoints
        {
            public const string FareSearchService = "BasicHttpBinding_IFareSearchService";
            public const string FareQuoteService = "BasicHttpBinding_IFareQuoteService";
            public const string FareRulesService = "Binding_IFareRulesService";
            public const string SeatMapService = "Binding_ISeatMapService";
            public const string DataServicesIAirConfigurationService = "ProtoBuf_IAirConfigurationService";
            public const string DataServicesIAirContentDataService = "BasicHttpBinding_IAirContentDataService";
            public const string AirFareSearchServiceProtobuf = "ProtobufBinding_IAirFareSearchV1";
            public const string AirFareSearchServiceBasicHttp = "BasicHttpBinding_IAirFareSearchV1";
            public static readonly string AirFareSearchServiceUriSuffix = "/proto"; //static readonly, so that only this dll can be pushed to change the behaviour.
            public const string DataServicesISupplierConfigurationService = "ProtoBuf_ISupplierConfigurationService";
            public static readonly string AirFareRulesServiceUriSuffix = "/proto";
            public static readonly string AirSeatmapServiceUriSuffix = "/proto";

            public static class AddressKeys
            {
                public const string DataServicesIAirConfigurationServiceAddr = "DataServices_IAirConfigurationService";
                public const string DataServicesIAirContentDataServiceAddr = "DataServices_IAirContentDataService";
                public const string DataServicesISupplierConfigurationServiceAddr = "DataServices_ISupplierConfigurationService";
            }
        }

        public static class MarkupKeys
        {
            public const string CustomerType = "CustomerType";
        }

        public static class APISessionKeys
        {
            public const string FlightSeachCategory = "FlightSearchResult";
            public const string FlightPriceCategory = "FlightPriceResult";
            public const string FlightRecommendationCategory = "FlightRecommendation";
            public const string FlightSeachCriteria = "FlightSearchCriteria";
        }

        public static class ContextKeys
        {
            public const string RateCode = "RateCode";
        }

        public static class Analytics
        {
            public const string FlightSearchApplicationId = "fltsrch";
            public const string ResponseTimeTag = "rspTime";
            public const string RequestTag = "rq";
            public const string ResponseTag = "rs";
            public const string SearchTag = "search";

            public static class Titles
            {
                public const string FlightSearchOverhead = "Service_Root_Overhead";
                public const string FlightSearchRequest = "Flight_Search";
                public const string FlightSearchResponse = "Flight_Search_RS";
                public const string FlightSearchRoot = "Flight_Search_Root";
                public const string FlightGetFromSessionRoot = "Flight_TryGetFromSession";
                public const string SupplierRoot = "Supplier_Root";
                public const string SupplierCacheHit = "Supplier_Cache_Transform";
            }
        }

        public static class ErrorCategory
        {
            public const string SupplierFailure = "SupplierFailure";
        }

        public static class ErrorCodes
        {
            public const string SupplierFailure = "1";
            public const string InternalError = "2";
            public const string ValidationFailure = "3";
            public const string NoResults = "4";
            public const string InternalErrorAndSupplierFailure = "5";
        }

        public static class ErrorMessages
        {
            public const string DefaultErrorMessage = "Internal Error";
            public const string ExceptionHasBeenLogged = "This exception has been logged";
            public const string InvalidSupplierResponse = "Invalid response from supplier";
            public const string SupplierRaisedException = "Supplier threw an exception. See inner logs for details";
            public const string FareManagerInitializationPending = "Initialization pending for faremanager. Cannot be used uninitialized";
        }

        public static class BooleanValues
        {
            public const string True = "true";
            public const string False = "false";
        }

        public static class AllowedFareTypes
        {
            public const string IsPrivateFareOnly = "IsPrivateFareOnly";
            public const string IsPublishedFareOnly = "IsPublishedFareOnly";
            public const string IsCorporateFareOnly = "IsCorporateFareOnly";
        }

        public static class FareType
        {
            public const string Published = "Published";
            public const string Negotiated = "Negotiated";
            public const string Corporate = "Corporate";
        }

        public static class InterceptLevel
        {
            public const string Post = "Post";
            public const string Both = "Both";
            public const string Pre = "Pre";
        }

        public static class AirlinePreference
        {
            public const string Include = "Include";
            public const string Exclude = "Exclude";
            public const string Only = "Only";
        }

        public static class MessageFormats
        {
            public const string ErrorMessage = "Error : {0}";
            public const string WarningMessage = "Warning : {0}";
            public const string InfoMessage = "Info : {0}";
            public const string SupplierMessage = "SupplierMessage : {0}";
        }

        public static class InformationMessages
        {
            public const string ChildConversionTookPlace = "One or more of the recommendations contains one/more children treated as adults";
        }
    }
}
