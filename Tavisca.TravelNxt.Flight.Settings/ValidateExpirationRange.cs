﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Flight.Settings
{
    public class ValidateExpirationRange
    {
        public static LongRange ValidateAndGetRange(string rangeString)
        {
            var splitRange = rangeString.Split('-');

            long firstVal;
            if (!long.TryParse(splitRange[0].Trim(), out firstVal))
                throw new InvalidDataException(string.Format("Inavlid value '{0}'",splitRange[0]));

            if (firstVal < 0)
                throw new InvalidDataException(string.Format("Inavlid value '{0}'", splitRange[0]));

            long secondVal;
            if (!long.TryParse(splitRange[1].Trim(), out secondVal))
                throw new InvalidDataException(string.Format("Inavlid value '{0}'", splitRange[1]));

            if (secondVal < firstVal)
                throw new InvalidDataException(string.Format(
                    "The ranges defined in the configuration with key '{0}' is invalid, the second part of the range cannot be less than the first part",
                    KeyStore.AppSettings.CacheRangeAndTime));

            if (secondVal < 1)
                throw new InvalidDataException(string.Format("Inavlid value '{0}' to end the range", splitRange[1]));
            return new LongRange(firstVal, secondVal);
        }
    }
}
