﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.Frameworks.Parallel;
using Tavisca.TravelNxt.Common.Settings.BackOffice;

namespace Tavisca.TravelNxt.Flight.Settings
{
    public class SettingManager : Common.Settings.SettingManager
    {
        #region Setting Loading

        static SettingManager()
        {
            ClearSettingAction = ClearSettings;
        }

        private static void ClearSettings()
        {
            _cacheRangeValues = new ConcurrentDictionary<LongRange, int>();
        }

        #endregion

        #region Setting Fields
        private static ConcurrentDictionary<LongRange, int> _cacheRangeValues = new ConcurrentDictionary<LongRange, int>();
        private static readonly IDictionary<string, string> ContextValues = new ConcurrentDictionary<string, string>();

        private static readonly string ApplicationName =
            ((NameValueCollection)
             ConfigurationManager.GetSection(Common.Settings.KeyStore.Configuration.StaticSettings))
                [Common.Settings.KeyStore.Configuration.AppContextName];

        #endregion

        #region Setting Properties


        public static string NonCouponFareCustomerTypes
        {
            get
            {
                return GetKeyedSetting(KeyStore.AppSettings.NonCouponFareCustomerTypes);
            }
        }

        public static string AirConfigurationConnectionString
        {
            get
            {
                return GetKeyedSetting(KeyStore.Repositories.DatabaseConnections.AirConfigurationDB);
            }
        }

        public static string AirBookConnectionString
        {
            get
            {
                return GetKeyedSetting(KeyStore.Repositories.DatabaseConnections.AirBookDB);
            }
        }

        public static string TicketlessCarriers
        {
            get
            {
                return GetKeyedSetting(KeyStore.AppSettings.TicketlessCarriers) ?? string.Empty;
            }
        }

        public static List<string> WebAPI_PreferredAirlines
        {
            get
            {
                string preferredAirlines= GetKeyedSetting(KeyStore.AppSettings.WebAPI_PreferredAirlines) ?? string.Empty;
                if (!string.IsNullOrEmpty(preferredAirlines))
                {
                   return preferredAirlines.Split(',').ToListBuffered();
                }
                return null;
            }
        }

        public static List<string> WebAPI_BlockedAirlines
        {
            get
            {
                string blockedAirlines=GetKeyedSetting(KeyStore.AppSettings.WebAPI_BlockedAirlines) ?? string.Empty;
                if (!string.IsNullOrEmpty(blockedAirlines))
                {
                   return blockedAirlines.Split(',').ToListBuffered();
                }
                return null;
            }
        }

        public static List<string> WebAPI_RequiredAirlines
        {
            get
            {
                string requiredAirlines= GetKeyedSetting(KeyStore.AppSettings.WebAPI_RequiredAirlines) ?? string.Empty;
                if (!string.IsNullOrEmpty(requiredAirlines))
                {
                   return requiredAirlines.Split(',').ToListBuffered();
                }
                return null;
            }
        }

        public static int MinTravelDateOffsetHours
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.MinTravelDateOffsetHours) ?? "24";

                var val = setting.ToInt();

                if (val <= 0)
                {
                    val = 24;
                }

                return val;
            }
        }

        public static int DefaultTimeWindow
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.DefaultTimeWindow) ?? "2";

                var val = setting.ToInt();

                if (val == 0)
                    val = 2;

                return val;
            }
        }

        public static int AirTimeAheadInHours
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.AirTimeAheadInHours) ?? "24";

                var val = setting.ToInt();

                if (val == 0)
                    val = 24;

                return val;
            }
        }

        public static int ScepterCacheExpirationHours
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.ScepterCacheExpirationHours) ?? "1";

                var val = setting.ToInt();

                if (val == 0)
                    val = 1;

                return val;
            }
        }

        public static int ProviderSpaceEmulationChainingLimit
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.ProviderSpaceEmulationChainingLimit) ?? "5";

                var val = setting.ToInt();

                if (val == 0)
                    val = 5;

                return val;
            }
        }

        public static int AvailSessionTimeOut
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.AvailSessionTimeOut) ?? "60";

                var val = setting.ToInt();

                if (val == 0)
                    val = 60;

                return val;
            }
        }

        public static int NearbyAirportRadius
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.NearbyAirportRadius) ?? "50";

                var val = setting.ToInt();

                if (val == 0)
                    val = 50;

                return val;
            }
        }

        public static int MaxNearbyAirportRadius
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.MaxNearbyAirportRadius) ?? "200";

                var val = setting.ToInt();

                if (val == 0)
                    val = 200;

                return val;
            }
        }

        public static int SupplierThreadTimeoutInSeconds
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.SupplierThreadTimeoutInSeconds) ?? "60";

                var val = setting.ToInt();

                if (val == 0)
                    val = 60;

                return val;
            }
        }

        public static bool SceptrLoggingEnabled
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.SceptrLoggingEnabled) ?? "false";

                bool settingValue;
                bool.TryParse(setting, out settingValue);

                return settingValue;
            }
        }

        public static bool AirEngineSearchCallLoggingEnabled
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.AirEngineSearchCallLoggingEnabled) ?? "false";

                bool settingValue;
                bool.TryParse(setting, out settingValue);

                return settingValue;
            }
        }

        public static bool WebAPICallLoggingEnabled
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.WebAPICallLoggingEnabled) ?? "false";

                bool settingValue;
                bool.TryParse(setting, out settingValue);

                return settingValue;
            }
        }

        public static bool UseClassOfServiceInItineraryIdentification
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.UseClassOfServiceInItineraryIdentification) ?? "true";

                bool settingValue;
                if (!bool.TryParse(setting, out settingValue))
                    settingValue = true;

                return settingValue;
            }
        }

        public static UnitOptions DistanceUnit
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.DistanceUnit) ?? Enum.GetName(typeof(UnitOptions), UnitOptions.Miles);

                UnitOptions val;
                if (!Enum.TryParse(setting, true, out val))
                    val = UnitOptions.Miles;

                return val;
            }
        }

        public static string AgencyHomeCountryCode
        {
            get
            {
                return GetKeyedSetting(KeyStore.AppSettings.AgencyHomeCountryCode) ?? "US";
            }
        }

        public static string AgencyBaseCurrency
        {
            get { return Utility.GetCurrentContext().BaseCurrency; }
        }

        public static SchedulerTypeOptions SupplierSchedulerType
        {
            get
            {
                var setting = GetKeyedSetting(KeyStore.AppSettings.SupplierSchedulerType)
                        ?? Enum.GetName(typeof(SchedulerTypeOptions), SchedulerTypeOptions.IOScheduler);

                SchedulerTypeOptions val;
                if (!Enum.TryParse(setting, true, out val))
                    val = SchedulerTypeOptions.IOScheduler;

                return val;
            }
        }

        public new static string GetClientEndpointAddress(string key)
        {
            return Common.Settings.SettingManager.GetClientEndpointAddress(key);
        }

        public static int GetExpirationTime(int daysToDepartureOfFlight)
        {
            if (_cacheRangeValues.Count < 1)
                PopulateCacheRangeValues();
            foreach (var cacheRange in _cacheRangeValues.Keys)
            {
                if (cacheRange.IsInRange(daysToDepartureOfFlight))
                    return _cacheRangeValues[cacheRange];
            }
            return 0;//don't want to cache the data which changes frequently if setting is missing
        }

        #endregion

        #region methods

        public static string GetContextSettingValue(string group, string key, bool fetchFromRoot = false, string applicationName = "")
        {
            var identifierKey = GetKey(group, key, applicationName);

            string settingValue;
            if (ContextValues.TryGetValue(identifierKey, out settingValue))
                return settingValue;

            var contextProvider = GetContextProvider(applicationName);

            settingValue = contextProvider.GetSetting(group, key, fetchFromRoot);
            ContextValues[identifierKey] = settingValue;

            return settingValue;
        }

        #endregion

        #region private methods

        private static string GetKey(string sectionName, string keyName, string applicationName)
        {
            applicationName = string.IsNullOrEmpty(applicationName) ? ApplicationName : applicationName;
            var contextId = Utility.GetCurrentContext().AccountId;

            return string.Format("{0}-{1}-{2}-{3}", contextId, applicationName, sectionName, keyName);
        }

        private static IContextProvider GetContextProvider(string applicationName = "")
        {
            return new ContextProvider() { ContextName = applicationName };
        }

        private static void PopulateCacheRangeValues()
        {
            var setting = GetKeyedSetting(KeyStore.AppSettings.CacheRangeAndTime);

            if (string.IsNullOrWhiteSpace(setting))
                throw new ConfigurationErrorsException(string.Format("Setting {0} is missing from AppSettings",
                                                  KeyStore.AppSettings.CacheRangeAndTime));
            var ranges = setting.Split(',');

            foreach (var range in ranges)
            {
                var trimmedRange = range.Trim();

                if (string.IsNullOrEmpty(trimmedRange))
                    continue;

                var splitRangeAndTime = trimmedRange.Split(':');

                var rangeOfDays = ValidateExpirationRange.ValidateAndGetRange(splitRangeAndTime[0]);
                int expirationTime;
                if (!int.TryParse(splitRangeAndTime[1].Trim(), out expirationTime))
                    throw new InvalidDataException(string.Format("{0} is invalid value for number of days",
                                                  splitRangeAndTime[1]));
                _cacheRangeValues.TryAdd(rangeOfDays, expirationTime);
            }
        }

        #endregion
    }
}
