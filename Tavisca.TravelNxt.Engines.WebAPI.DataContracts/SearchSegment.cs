﻿using System;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract]
    public class SearchSegment
    {
        [DataMember]
        public string OriginCode { get; set; }

        [DataMember]
        public bool IsOriginCodeCity { get; set; }

        [DataMember]
        public string DestCode { get; set; }

        [DataMember]
        public bool IsDestCodeCity { get; set; }

        [DataMember]
        public DateTime DepartDate { get; set; }
    }
}
