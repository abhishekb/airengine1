﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirOption
    {
        [DataMember]
        public Fare Fare { get; set; }

        [DataMember]
        public List<FlightRef> Segments { get; set; }

        [DataMember]
        public TicketType Ticket { get; set; }

        [DataMember]
        public bool IsSpecial { get; set; }

        [DataMember]
        public bool IsRefundable { get; set; }

        [DataMember]
        public int Rph { get; set; }

        //[DataMember]
        //public string PlatingCarrier { get; set; }
    }
}
