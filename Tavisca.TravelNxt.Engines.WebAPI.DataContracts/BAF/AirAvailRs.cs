﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common.BAF;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirAvailRs : SearchBaseRs
    {
        [DataMember]
        public List<AirOption> AirOptions { get; set; }

        [DataMember]
        public List<Flight> Segments { get; set; }

        //[DataMember]
        //public List<Baggage> Baggages { get; set; }
    }
}
