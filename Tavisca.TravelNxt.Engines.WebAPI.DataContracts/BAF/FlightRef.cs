﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightRef
    {
        [DataMember]
        public string FlightKey { get; set; }

        [DataMember]
        public string FareClass { get; set; }

        [DataMember]
        public int LegNum { get; set; }

        [DataMember]
        public string Cabin { get; set; }
    }
}
