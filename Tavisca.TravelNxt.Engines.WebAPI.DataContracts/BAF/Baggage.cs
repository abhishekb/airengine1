﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts.BAF
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Baggage
    {
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? Quantity { get; set; }

        [DataMember]
        public decimal? WeightInPounds { get; set; }

        [DataMember]
        public string BaggageCode { get; set; }
    }
}
