﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Tavisca.TravelNxt.Flight.Avail.DataContract;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    public class PaxTypeBaggage
    {
        [DataMember]
        public string BaggageCode { get; set; }

        [DataMember]
        public PassengerType PassengerType { get; set; }
    }
}
