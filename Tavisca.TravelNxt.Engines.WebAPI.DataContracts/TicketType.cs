﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    //P : Paper
    //E : Eticket
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public enum TicketType
    {
        [EnumMember]
        E,
        [EnumMember]
        P
    }
}
