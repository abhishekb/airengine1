﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class FlightRef
    {
        [DataMember]
        public string FlightKey { get; set; }

        [DataMember]
        public string FareClass { get; set; }

        [DataMember]
        public int LegNum { get; set; }

        [DataMember]
        public string Cabin { get; set; }
    }
}
