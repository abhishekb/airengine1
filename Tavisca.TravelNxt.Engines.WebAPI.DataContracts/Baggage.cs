﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    public class Baggage
    {
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? Quantity { get; set; }

        [DataMember]
        public decimal? WeightInPounds { get; set; }

        [DataMember]
        public string BaggageCode { get; set; }

        //public static Baggage ToDataContract(Entities.Baggage baggage)
        //{
        //    if (baggage == null)
        //        return null;

        //    return new Baggage()
        //    {
        //        Description = baggage.Description,
        //        Quantity = baggage.Quantity,
        //        WeightInPounds = baggage.WeightInPounds,
        //        BaggageCode = baggage.BaggageCode
        //    };
        //}
    }
}
