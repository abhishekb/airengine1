﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract]
    public class AirAvailRq : SearchBaseRq
    {
        [DataMember]
        public List<SearchSegment> SearchSegments { get; set; }

        [DataMember]
        public int Children { get; set; }

        [DataMember]
        public int Adults { get; set; }

        [DataMember]
        public int Infants { get; set; }

        [DataMember]
        public int Seniors { get; set; }

        [DataMember]
        public TripType TripType { get; set; }

        [DataMember]
        public int MaxStops { get; set; }

        [DataMember]
        public CabinType Cabin { get; set; }
        
        [DataMember]
        public int SearchWindow { get; set; }

        [DataMember]
        public bool ProvideBaggageInfo { get; set; }
    }
}
