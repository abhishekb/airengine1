﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirAvailRs : SearchBaseRs    
    {
        [DataMember]
        public List<AirOption> AirOptions { get; set; }

        [DataMember]
        public List<Flight> Segments { get; set; }

        [DataMember]
        public List<Baggage> Baggages { get; set; }
    }
}
