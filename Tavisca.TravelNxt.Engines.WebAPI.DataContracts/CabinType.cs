﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract]
    public enum CabinType
    {
        [EnumMember] Economy,
        [EnumMember] Business,
        [EnumMember] First,
    }
}
