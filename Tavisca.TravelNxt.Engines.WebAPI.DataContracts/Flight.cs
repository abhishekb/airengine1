﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class Flight
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string AirlineCode { get; set; }

        [DataMember]
        public string Airline { get; set; }

        [DataMember]
        public string FlightNumber { get; set; }

        [DataMember]
        public string FromAirport { get; set; }

        [DataMember]
        public string ToAirport { get; set; }

        [DataMember]
        public int DurationInMin { get; set; }

        [DataMember]
        public DateTime Depart { get; set; }

        [DataMember]
        public DateTime Arrive { get; set; }

        [DataMember]
        public string Aircraft { get; set; }

        [DataMember]
        public int NumOfStops { get; set; }

        [DataMember]
        public string OperatingAirlineCode { get; set; }

        [DataMember]
        public List<PaxTypeBaggage> PaxTypeBaggages { get; set; }
    }
}
