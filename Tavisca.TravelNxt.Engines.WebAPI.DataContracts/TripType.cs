﻿using System.Runtime.Serialization;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract]
    public enum TripType
    {
        [EnumMember] RoundTrip,
        [EnumMember] OneWay,
        [EnumMember] MultiStop
    }
}
