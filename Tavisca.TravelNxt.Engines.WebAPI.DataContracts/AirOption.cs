﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Engines.WebAPI.DataContracts.Common;

namespace Tavisca.TravelNxt.Engines.WebAPI.DataContracts
{
    [DataContract(Namespace = "http://tavisca.com/Air/V1")]
    public class AirOption
    {
        [DataMember]
        public Fare Fare { get; set; }

        [DataMember]
        public List<FlightRef> Segments { get; set; }

        [DataMember]
        public TicketType Ticket { get; set; }

        [DataMember]
        public bool IsSpecial { get; set; }

        [DataMember]
        public bool IsRefundable { get; set; }

        [DataMember]
        public int Rph { get; set; }

        [DataMember]
        public string PlatingCarrier { get; set; }
    }
}
