﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkupTests.ExcelClasses
{
    public partial class Markup
    {
        private string _ownerId;
        private string _childNodeId;

        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }

        public string ChildNodeId
        {
            get { return _childNodeId; }
            set { _childNodeId = DataHelper.ReplaceOwnerValue(value); }
        }
    }

    public partial class MarkupRule
    {
        private string _ownerId;
        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }
    }

    public partial class SaveBulkMarkups
    {
        private string _ownerId;
        private string _childNodeId;

        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }

        public string ChildNodeId
        {
            get { return _childNodeId; }
            set { _childNodeId = DataHelper.ReplaceOwnerValue(value); }
        }
    }

    public partial class MarkupGroupingData
    {
        private string _ownerId;
        private string _childNodeId;

        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }

        public string ChildNodeId
        {
            get { return _childNodeId; }
            set { _childNodeId = DataHelper.ReplaceOwnerValue(value); }
        }
    }
}
