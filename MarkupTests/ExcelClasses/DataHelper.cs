using System.Globalization;
using Tavisca.TravelNxt.Common.TestHelpers;

namespace MarkupTests.ExcelClasses
{
    internal static class DataHelper
    {
        public static string ReplaceOwnerValue(string val)
        {
            switch (val.ToUpper())
            {
                case "##PLATFORMID##":
                    return AccountHelper.GetTestPlatformAccountId().ToString(CultureInfo.InvariantCulture);
                case "##CLIENTID##":
                    return AccountHelper.GetTestClientAccountId().ToString(CultureInfo.InvariantCulture);
                case "##AGENCYID##":
                    return AccountHelper.GetTestAgencyAccountId().ToString(CultureInfo.InvariantCulture);
            }

            return val;
        }
    }
}