﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Excel;

namespace MarkupTests.ExcelClasses
{
    #region SheetClasses

    public partial class Markup
    {
        public string Sno { get; set; }
        public IEnumerable<MarkupRule> MarkupRules { get; set; }
        public IEnumerable<MarkupValue> MarkupValues { get; set; }
        public string ConstraintId { get; set; }
        public string Description { get; set; }
        public string FriendlyName { get; set; }
        public string Enabled { get; set; }
        public IEnumerable<MarkupType> MarkupTypes { get; set; }
        public string IsRefundable { get; set; }
        public string Preference { get; set; }
        public string POS { get; set; }
        public string FareSource { get; set; }
        public IEnumerable<ProductType> ProductTypes { get; set; }
        public string IsOverridable { get; set; }
        public string IsInsert { get; set; }
        public string ExpectedStatus { get; set; }
        public string IsExclusive { get; set; }
        public string UpdatedPreference { get; set; }
        public string AddedAtTop { get; set; }
        public string MarkupGroup { get; set; }
        public string DynamicValue { get; set; }
    }

    public partial class MarkupRule
    {
        public string Sno { get; set; }
        public string RateCode { get; set; }
        public string CustomerType { get; set; }
        public string IsPublic { get; set; }
        public IEnumerable<AirMarkupRule> AirMarkupRules { get; set; }
        public IEnumerable<CarMarkupRule> CarMarkupRules { get; set; }
        public IEnumerable<HotelMarkupRule> HotelMarkupRules { get; set; }
        public IEnumerable<ActivityMarkupRule> ActivityMarkupRules { get; set; }
    }

    public class AirMarkupRule
    {
        public string Sno { get; set; }
        public string OriginCountries { get; set; }
        public string OriginAirports { get; set; }
        public string DestinationCountries { get; set; }
        public string DestinationAirports { get; set; }
        public string MarketingCarriers { get; set; }
        public string ClassesOfService { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public string FareBasisCodes { get; set; }
        public IEnumerable<TravelDaySpan> AdvancePurchaseDaySpans { get; set; }
        public IEnumerable<TravelDateSpan> TravelDateSpans { get; set; }
        public IEnumerable<TravelDateSpan> BookingDateSpans { get; set; }
        public IEnumerable<TravelDaySpan> TravelDurations { get; set; }
        public IEnumerable<ItineraryType> ItineraryTypes { get; set; }
        public IEnumerable<CabinType> CabinTypes { get; set; }
        public IEnumerable<SupplierFareRange> SupplierFareRanges { get; set; }
    }

    public class CarMarkupRule
    {
        public string Sno { get; set; }
        public string Country { get; set; }
        public IEnumerable<CarCompany> CarCompanys { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public IEnumerable<TravelDaySpan> AdvancePurchaseDaySpans { get; set; }
        public IEnumerable<TravelDateSpan> TravelDateSpans { get; set; }
        public IEnumerable<TravelDateSpan> BookingDateSpans { get; set; }
        public IEnumerable<TravelDaySpan> TravelDaySpans { get; set; }
        public IEnumerable<CarCategory> CarCategorys { get; set; }
        public IEnumerable<Location> Locations { get; set; }
        public IEnumerable<SupplierFareRange> SupplierFareRanges { get; set; }
        public IEnumerable<DayOfWeek> DayOfWeeks { get; set; }
    }

    public class HotelMarkupRule
    {
        public string Sno { get; set; }
        public string HotelChainId { get; set; }
        public string HotelId { get; set; }
        public IEnumerable<HotelIdType> HotelIdTypes { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public IEnumerable<TravelDaySpan> AdvancePurchaseDaySpans { get; set; }
        public IEnumerable<TravelDateSpan> TravelDateSpans { get; set; }
        public IEnumerable<TravelDateSpan> BookingDateSpans { get; set; }
        public IEnumerable<TravelDaySpan> TravelDurations { get; set; }
        public IEnumerable<Location> Locations { get; set; }
        public string HotelRoomType { get; set; }
        public IEnumerable<SupplierFareRange> SupplierFareRanges { get; set; }
    }

    public class ActivityMarkupRule
    {
        public string Sno { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public IEnumerable<TravelDaySpan> AdvancePurchaseDaySpans { get; set; }
        public IEnumerable<TravelDateSpan> TravelDateSpans { get; set; }
        public IEnumerable<TravelDateSpan> BookingDateSpans { get; set; }
        public IEnumerable<TravelDaySpan> TravelDurations { get; set; }
        public IEnumerable<Location> Locations { get; set; }
    }

    public class MarkupValue
    {
        public string Sno { get; set; }
        public string Currency { get; set; }
        public IEnumerable<ValueType> ValueTypes { get; set; }
        public IEnumerable<FareCalculatedOn> FareCalculatedOns { get; set; }
        public string FriendlyType { get; set; }
        public IEnumerable<CalculationType> CalculationTypes { get; set; }
        public string Amount { get; set; }
        public string MinimumValue { get; set; }
        public string MaximumValue { get; set; }
    }

    public class TravelDaySpan
    {
        public string Sno { get; set; }
        public string MinimumDays { get; set; }
        public string MaximumDays { get; set; }
    }

    public class TravelDateSpan
    {
        public string Sno { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class ItineraryType
    {
        public string Sno { get; set; }
        public string itineraryType { get; set; }
    }

    public class CabinType
    {
        public string Sno { get; set; }
        public string cabinType { get; set; }
    }

    public class FareType
    {
        public string Sno { get; set; }
        public string fareType { get; set; }
    }

    public class CarCompany
    {
        public string Sno { get; set; }
        public string carCompany { get; set; }
    }

    public class HotelChain
    {
        public string Sno { get; set; }
        public string hotelChain { get; set; }
    }

    public class HotelIdType
    {
        public string Sno { get; set; }
        public string hotelIdType { get; set; }
    }

    public class FareCalculatedOn
    {
        public string Sno { get; set; }
        public string fareCalculatedOn { get; set; }
    }

    public class CalculationType
    {
        public string Sno { get; set; }
        public string calculationType { get; set; }
    }

    public class MarkUpValueType
    {
        public string Sno { get; set; }
        public string markUpValueType { get; set; }
    }

    public class ProductType
    {
        public string Sno { get; set; }
        public string productType { get; set; }
    }

    public class MarkupType
    {
        public string Sno { get; set; }
        public string markupType { get; set; }
    }

    public class ValueType
    {
        public string Sno { get; set; }
        public string valueType { get; set; }
    }

    public class Location
    {
        public string SNO { get; set; }
        public string AirPortCode { get; set; }
        public string CityCode { get; set; }
        public string CountryCode { get; set; }
        public IEnumerable<GeoCode> GeoCodes { get; set; }
        public string CheckExactMatch { get; set; }
    }

    public class GeoCode
    {
        public string SNO { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string RadiusUnit { get; set; }
        public string RadiusValue { get; set; }
    }

    public class CarCategory
    {
        public string Sno { get; set; }
        public string carCategory { get; set; }
    }

    public class SupplierFareRange
    {
        public string SNo { get; set; }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public string Currency { get; set; }
        public string OperatingFareType { get; set; }
    }

    public class DayOfWeek
    {
        public string Sno { get; set; }
        public string dayOfWeek { get; set; }
    }

    public class MarkupStatus
    {
        public string Sno { get; set; }
        public IEnumerable<MarkupStatusType> MarkupStatusTypes { get; set; }
    }

    public class MarkupStatusType
    {
        public string Sno { get; set; }
        public string markupStatusType { get; set; }
    }

    public partial class SaveBulkMarkups
    {
        public string Sno { get; set; }
        public IEnumerable<MarkupRule> MarkupRules { get; set; }
        public IEnumerable<MarkupValue> MarkupValues { get; set; }
        public string ConstraintId { get; set; }
        public string Description { get; set; }
        public string FriendlyName { get; set; }
        public string Enabled { get; set; }
        public IEnumerable<MarkupType> MarkupTypes { get; set; }
        public string IsRefundable { get; set; }
        public string Preference { get; set; }
        public string POS { get; set; }
        public string FareSource { get; set; }
        public IEnumerable<ProductType> ProductTypes { get; set; }
        public string IsOverridable { get; set; }
        public string IsInsert { get; set; }
        public string ExpectedStatus { get; set; }
        public string IsExclusive { get; set; }
        public IEnumerable<ActionOptions> ActionOptionss { get; set; }
        public string MarkupGroup { get; set; }
    }

    public class ActionOptions
    {
        public string Sno { get; set; }
        public string actionOptions { get; set; }
    }

    public partial class MarkupGroupingData
    {
        public string Sno { get; set; }
        public IEnumerable<MarkupRule> MarkupRules { get; set; }
        public IEnumerable<MarkupValue> MarkupValues { get; set; }
        public string ConstraintId { get; set; }
        public string Description { get; set; }
        public string FriendlyName { get; set; }
        public string Enabled { get; set; }
        public IEnumerable<MarkupType> MarkupTypes { get; set; }
        public string IsRefundable { get; set; }
        public string Preference { get; set; }
        public string POS { get; set; }
        public string FareSource { get; set; }
        public IEnumerable<ProductType> ProductTypes { get; set; }
        public string IsOverridable { get; set; }
        public string IsInsert { get; set; }
        public string ExpectedStatus { get; set; }
        public string IsExclusive { get; set; }
        public string UpdatedPreference { get; set; }
        public string AddedAtTop { get; set; }
        public string MarkupGroup { get; set; }
    }
    #endregion
    public class FareComponents
    {
        #region Properties
        public IList<Markup> Markups { get; set; }
        public IList<MarkupRule> MarkupRules { get; set; }
        public IList<AirMarkupRule> AirMarkupRules { get; set; }
        public IList<CarMarkupRule> CarMarkupRules { get; set; }
        public IList<HotelMarkupRule> HotelMarkupRules { get; set; }
        public IList<ActivityMarkupRule> ActivityMarkupRules { get; set; }
        public IList<MarkupValue> MarkupValues { get; set; }
        public IList<TravelDaySpan> TravelDaySpans { get; set; }
        public IList<TravelDateSpan> TravelDateSpans { get; set; }
        public IList<ItineraryType> ItineraryTypes { get; set; }
        public IList<CabinType> CabinTypes { get; set; }
        public IList<FareType> FareTypes { get; set; }
        public IList<CarCompany> CarCompanys { get; set; }
        public IList<HotelChain> HotelChains { get; set; }
        public IList<HotelIdType> HotelIdTypes { get; set; }
        public IList<FareCalculatedOn> FareCalculatedOns { get; set; }
        public IList<CalculationType> CalculationTypes { get; set; }
        public IList<MarkUpValueType> MarkUpValueTypes { get; set; }
        public IList<ProductType> ProductTypes { get; set; }
        public IList<MarkupType> MarkupTypes { get; set; }
        public IList<ValueType> ValueTypes { get; set; }
        public IList<Location> Locations { get; set; }
        public IList<GeoCode> GeoCodes { get; set; }
        public IList<CarCategory> CarCategorys { get; set; }
        public IList<SupplierFareRange> SupplierFareRanges { get; set; }
        public IList<DayOfWeek> DayOfWeeks { get; set; }
        public IList<MarkupStatus> MarkupStatuss { get; set; }
        public IList<MarkupStatusType> MarkupStatusTypes { get; set; }
        public IList<SaveBulkMarkups> SaveBulkMarkupss { get; set; }
        public IList<ActionOptions> ActionOptionss { get; set; }
        public IList<MarkupGroupingData> MarkupGroupingDatas { get; set; }
        #endregion

        #region Constructors

        public FareComponents()
        {
            Markups = new List<Markup>();
            MarkupRules = new List<MarkupRule>();
            AirMarkupRules = new List<AirMarkupRule>();
            CarMarkupRules = new List<CarMarkupRule>();
            HotelMarkupRules = new List<HotelMarkupRule>();
            ActivityMarkupRules = new List<ActivityMarkupRule>();
            MarkupValues = new List<MarkupValue>();
            TravelDaySpans = new List<TravelDaySpan>();
            TravelDateSpans = new List<TravelDateSpan>();
            ItineraryTypes = new List<ItineraryType>();
            CabinTypes = new List<CabinType>();
            FareTypes = new List<FareType>();
            CarCompanys = new List<CarCompany>();
            HotelChains = new List<HotelChain>();
            HotelIdTypes = new List<HotelIdType>();
            FareCalculatedOns = new List<FareCalculatedOn>();
            CalculationTypes = new List<CalculationType>();
            MarkUpValueTypes = new List<MarkUpValueType>();
            ProductTypes = new List<ProductType>();
            MarkupTypes = new List<MarkupType>();
            ValueTypes = new List<ValueType>();
            Locations = new List<Location>();
            GeoCodes = new List<GeoCode>();
            CarCategorys = new List<CarCategory>();
            SupplierFareRanges = new List<SupplierFareRange>();
            DayOfWeeks = new List<DayOfWeek>();
            MarkupStatuss = new List<MarkupStatus>();
            MarkupStatusTypes = new List<MarkupStatusType>();
            SaveBulkMarkupss = new List<SaveBulkMarkups>();
            ActionOptionss = new List<ActionOptions>();
            MarkupGroupingDatas = new List<MarkupGroupingData>();
        }

        #endregion

        #region Public Methods

        public static FareComponents Load(string excelFilePath)
        {
            var data = new FareComponents();

            if (string.IsNullOrEmpty(excelFilePath))
                throw new ArgumentNullException("excelFilePath");

            using (var ds = GetExcelSheetDataSet(excelFilePath))
            {
                DataTable table;

                table = ds.Tables["Markup"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupobj = new Markup();

                    markupobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataMarkupRule517321 = ParseColumnData(row["MarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupRule517321))
                    {
                        markupobj.MarkupRules = Enumerable.Empty<MarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataMarkupRule517321.Split(',');
                        markupobj.MarkupRules = data.MarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataMarkupValue676788 = ParseColumnData(row["MarkupValue"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupValue676788))
                    {
                        markupobj.MarkupValues = Enumerable.Empty<MarkupValue>();
                    }
                    else
                    {
                        var splitData = cDataMarkupValue676788.Split(',');
                        markupobj.MarkupValues = data.MarkupValues.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupobj.ConstraintId = ParseColumnData(row["ConstraintId"]);
                    markupobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    markupobj.ChildNodeId = ParseColumnData(row["ChildNodeId"]);
                    markupobj.Description = ParseColumnData(row["Description"]);
                    markupobj.FriendlyName = ParseColumnData(row["FriendlyName"]);
                    markupobj.Enabled = ParseColumnData(row["Enabled"]);
                    var cDataMarkupType961852 = ParseColumnData(row["MarkupType"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupType961852))
                    {
                        markupobj.MarkupTypes = Enumerable.Empty<MarkupType>();
                    }
                    else
                    {
                        var splitData = cDataMarkupType961852.Split(',');
                        markupobj.MarkupTypes = data.MarkupTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupobj.IsRefundable = ParseColumnData(row["IsRefundable"]);
                    markupobj.Preference = ParseColumnData(row["Preference"]);
                    markupobj.POS = ParseColumnData(row["POS"]);
                    markupobj.FareSource = ParseColumnData(row["FareSource"]);
                    var cDataProductType345607 = ParseColumnData(row["ProductType"]);

                    if (string.IsNullOrWhiteSpace(cDataProductType345607))
                    {
                        markupobj.ProductTypes = Enumerable.Empty<ProductType>();
                    }
                    else
                    {
                        var splitData = cDataProductType345607.Split(',');
                        markupobj.ProductTypes = data.ProductTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupobj.IsOverridable = ParseColumnData(row["IsOverridable"]);
                    markupobj.IsInsert = ParseColumnData(row["IsInsert"]);
                    markupobj.ExpectedStatus = ParseColumnData(row["ExpectedStatus"]);
                    markupobj.IsExclusive = ParseColumnData(row["IsExclusive"]);
                    markupobj.UpdatedPreference = ParseColumnData(row["UpdatedPreference"]);
                    markupobj.AddedAtTop = ParseColumnData(row["AddedAtTop"]);
                    markupobj.MarkupGroup = ParseColumnData(row["MarkupGroup"]);
                    markupobj.DynamicValue = ParseColumnData(row["DynamicValue"]);
                    data.Markups.Add(markupobj);
                }
                table = ds.Tables["MarkupRule"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupruleobj = new MarkupRule();

                    markupruleobj.Sno = ParseColumnData(row["Sno"]);
                    markupruleobj.RateCode = ParseColumnData(row["RateCode"]);
                    markupruleobj.CustomerType = ParseColumnData(row["CustomerType"]);
                    markupruleobj.IsPublic = ParseColumnData(row["IsPublic"]);
                    markupruleobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    var cDataAirMarkupRule189447 = ParseColumnData(row["AirMarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataAirMarkupRule189447))
                    {
                        markupruleobj.AirMarkupRules = Enumerable.Empty<AirMarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataAirMarkupRule189447.Split(',');
                        markupruleobj.AirMarkupRules = data.AirMarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataCarMarkupRule277489 = ParseColumnData(row["CarMarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataCarMarkupRule277489))
                    {
                        markupruleobj.CarMarkupRules = Enumerable.Empty<CarMarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataCarMarkupRule277489.Split(',');
                        markupruleobj.CarMarkupRules = data.CarMarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataHotelMarkupRule471560 = ParseColumnData(row["HotelMarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataHotelMarkupRule471560))
                    {
                        markupruleobj.HotelMarkupRules = Enumerable.Empty<HotelMarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataHotelMarkupRule471560.Split(',');
                        markupruleobj.HotelMarkupRules = data.HotelMarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataActivityMarkupRule459258 = ParseColumnData(row["ActivityMarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataActivityMarkupRule459258))
                    {
                        markupruleobj.ActivityMarkupRules = Enumerable.Empty<ActivityMarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataActivityMarkupRule459258.Split(',');
                        markupruleobj.ActivityMarkupRules = data.ActivityMarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.MarkupRules.Add(markupruleobj);
                }
                table = ds.Tables["AirMarkupRule"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var airmarkupruleobj = new AirMarkupRule();

                    airmarkupruleobj.Sno = ParseColumnData(row["Sno"]);
                    airmarkupruleobj.OriginCountries = ParseColumnData(row["OriginCountries"]);
                    airmarkupruleobj.OriginAirports = ParseColumnData(row["OriginAirports"]);
                    airmarkupruleobj.DestinationCountries = ParseColumnData(row["DestinationCountries"]);
                    airmarkupruleobj.DestinationAirports = ParseColumnData(row["DestinationAirports"]);
                    airmarkupruleobj.MarketingCarriers = ParseColumnData(row["MarketingCarriers"]);
                    airmarkupruleobj.ClassesOfService = ParseColumnData(row["ClassesOfService"]);
                    var cDataFareType931005 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType931005))
                    {
                        airmarkupruleobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType931005.Split(',');
                        airmarkupruleobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    airmarkupruleobj.FareBasisCodes = ParseColumnData(row["FareBasisCodes"]);
                    var cDataTravelDaySpan959363 = ParseColumnData(row["AdvancePurchaseDaySpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan959363))
                    {
                        airmarkupruleobj.AdvancePurchaseDaySpans = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan959363.Split(',');
                        airmarkupruleobj.AdvancePurchaseDaySpans = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan477578 = ParseColumnData(row["TravelDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan477578))
                    {
                        airmarkupruleobj.TravelDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan477578.Split(',');
                        airmarkupruleobj.TravelDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan415891 = ParseColumnData(row["BookingDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan415891))
                    {
                        airmarkupruleobj.BookingDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan415891.Split(',');
                        airmarkupruleobj.BookingDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan27840 = ParseColumnData(row["TravelDuration"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan27840))
                    {
                        airmarkupruleobj.TravelDurations = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan27840.Split(',');
                        airmarkupruleobj.TravelDurations = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataItineraryType54129 = ParseColumnData(row["ItineraryType"]);

                    if (string.IsNullOrWhiteSpace(cDataItineraryType54129))
                    {
                        airmarkupruleobj.ItineraryTypes = Enumerable.Empty<ItineraryType>();
                    }
                    else
                    {
                        var splitData = cDataItineraryType54129.Split(',');
                        airmarkupruleobj.ItineraryTypes = data.ItineraryTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataCabinType326223 = ParseColumnData(row["CabinType"]);

                    if (string.IsNullOrWhiteSpace(cDataCabinType326223))
                    {
                        airmarkupruleobj.CabinTypes = Enumerable.Empty<CabinType>();
                    }
                    else
                    {
                        var splitData = cDataCabinType326223.Split(',');
                        airmarkupruleobj.CabinTypes = data.CabinTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataSupplierFareRange611547 = ParseColumnData(row["SupplierFareRange"]);

                    if (string.IsNullOrWhiteSpace(cDataSupplierFareRange611547))
                    {
                        airmarkupruleobj.SupplierFareRanges = Enumerable.Empty<SupplierFareRange>();
                    }
                    else
                    {
                        var splitData = cDataSupplierFareRange611547.Split(',');
                        airmarkupruleobj.SupplierFareRanges = data.SupplierFareRanges.Where(x => splitData.Any(y => y.Equals(x.SNo)));
                    }
                    data.AirMarkupRules.Add(airmarkupruleobj);
                }
                table = ds.Tables["CarMarkupRule"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var carmarkupruleobj = new CarMarkupRule();

                    carmarkupruleobj.Sno = ParseColumnData(row["Sno"]);
                    carmarkupruleobj.Country = ParseColumnData(row["Country"]);
                    var cDataCarCompany569309 = ParseColumnData(row["CarCompany"]);

                    if (string.IsNullOrWhiteSpace(cDataCarCompany569309))
                    {
                        carmarkupruleobj.CarCompanys = Enumerable.Empty<CarCompany>();
                    }
                    else
                    {
                        var splitData = cDataCarCompany569309.Split(',');
                        carmarkupruleobj.CarCompanys = data.CarCompanys.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataFareType721079 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType721079))
                    {
                        carmarkupruleobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType721079.Split(',');
                        carmarkupruleobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan844490 = ParseColumnData(row["AdvancePurchaseDaySpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan844490))
                    {
                        carmarkupruleobj.AdvancePurchaseDaySpans = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan844490.Split(',');
                        carmarkupruleobj.AdvancePurchaseDaySpans = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan767087 = ParseColumnData(row["TravelDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan767087))
                    {
                        carmarkupruleobj.TravelDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan767087.Split(',');
                        carmarkupruleobj.TravelDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan783795 = ParseColumnData(row["BookingDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan783795))
                    {
                        carmarkupruleobj.BookingDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan783795.Split(',');
                        carmarkupruleobj.BookingDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan248572 = ParseColumnData(row["TravelDaySpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan248572))
                    {
                        carmarkupruleobj.TravelDaySpans = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan248572.Split(',');
                        carmarkupruleobj.TravelDaySpans = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataCarCategory755827 = ParseColumnData(row["CarCategory"]);

                    if (string.IsNullOrWhiteSpace(cDataCarCategory755827))
                    {
                        carmarkupruleobj.CarCategorys = Enumerable.Empty<CarCategory>();
                    }
                    else
                    {
                        var splitData = cDataCarCategory755827.Split(',');
                        carmarkupruleobj.CarCategorys = data.CarCategorys.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataLocation130859 = ParseColumnData(row["Location"]);

                    if (string.IsNullOrWhiteSpace(cDataLocation130859))
                    {
                        carmarkupruleobj.Locations = Enumerable.Empty<Location>();
                    }
                    else
                    {
                        var splitData = cDataLocation130859.Split(',');
                        carmarkupruleobj.Locations = data.Locations.Where(x => splitData.Any(y => y.Equals(x.SNO)));
                    }
                    var cDataSupplierFareRange817351 = ParseColumnData(row["SupplierFareRange"]);

                    if (string.IsNullOrWhiteSpace(cDataSupplierFareRange817351))
                    {
                        carmarkupruleobj.SupplierFareRanges = Enumerable.Empty<SupplierFareRange>();
                    }
                    else
                    {
                        var splitData = cDataSupplierFareRange817351.Split(',');
                        carmarkupruleobj.SupplierFareRanges = data.SupplierFareRanges.Where(x => splitData.Any(y => y.Equals(x.SNo)));
                    }
                    var cDataDayOfWeek828740 = ParseColumnData(row["DayOfWeek"]);

                    if (string.IsNullOrWhiteSpace(cDataDayOfWeek828740))
                    {
                        carmarkupruleobj.DayOfWeeks = Enumerable.Empty<DayOfWeek>();
                    }
                    else
                    {
                        var splitData = cDataDayOfWeek828740.Split(',');
                        carmarkupruleobj.DayOfWeeks = data.DayOfWeeks.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.CarMarkupRules.Add(carmarkupruleobj);
                }
                table = ds.Tables["HotelMarkupRule"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var hotelmarkupruleobj = new HotelMarkupRule();

                    hotelmarkupruleobj.Sno = ParseColumnData(row["Sno"]);
                    hotelmarkupruleobj.HotelChainId = ParseColumnData(row["HotelChainId"]);
                    hotelmarkupruleobj.HotelId = ParseColumnData(row["HotelId"]);
                    var cDataHotelIdType786917 = ParseColumnData(row["HotelIdType"]);

                    if (string.IsNullOrWhiteSpace(cDataHotelIdType786917))
                    {
                        hotelmarkupruleobj.HotelIdTypes = Enumerable.Empty<HotelIdType>();
                    }
                    else
                    {
                        var splitData = cDataHotelIdType786917.Split(',');
                        hotelmarkupruleobj.HotelIdTypes = data.HotelIdTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataFareType79179 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType79179))
                    {
                        hotelmarkupruleobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType79179.Split(',');
                        hotelmarkupruleobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan699662 = ParseColumnData(row["AdvancePurchaseDaySpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan699662))
                    {
                        hotelmarkupruleobj.AdvancePurchaseDaySpans = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan699662.Split(',');
                        hotelmarkupruleobj.AdvancePurchaseDaySpans = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan888137 = ParseColumnData(row["TravelDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan888137))
                    {
                        hotelmarkupruleobj.TravelDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan888137.Split(',');
                        hotelmarkupruleobj.TravelDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan171722 = ParseColumnData(row["BookingDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan171722))
                    {
                        hotelmarkupruleobj.BookingDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan171722.Split(',');
                        hotelmarkupruleobj.BookingDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan381735 = ParseColumnData(row["TravelDuration"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan381735))
                    {
                        hotelmarkupruleobj.TravelDurations = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan381735.Split(',');
                        hotelmarkupruleobj.TravelDurations = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataLocation385952 = ParseColumnData(row["Location"]);

                    if (string.IsNullOrWhiteSpace(cDataLocation385952))
                    {
                        hotelmarkupruleobj.Locations = Enumerable.Empty<Location>();
                    }
                    else
                    {
                        var splitData = cDataLocation385952.Split(',');
                        hotelmarkupruleobj.Locations = data.Locations.Where(x => splitData.Any(y => y.Equals(x.SNO)));
                    }
                    hotelmarkupruleobj.HotelRoomType = ParseColumnData(row["HotelRoomType"]);
                    var cDataSupplierFareRange497064 = ParseColumnData(row["SupplierFareRange"]);

                    if (string.IsNullOrWhiteSpace(cDataSupplierFareRange497064))
                    {
                        hotelmarkupruleobj.SupplierFareRanges = Enumerable.Empty<SupplierFareRange>();
                    }
                    else
                    {
                        var splitData = cDataSupplierFareRange497064.Split(',');
                        hotelmarkupruleobj.SupplierFareRanges = data.SupplierFareRanges.Where(x => splitData.Any(y => y.Equals(x.SNo)));
                    }
                    data.HotelMarkupRules.Add(hotelmarkupruleobj);
                }
                table = ds.Tables["ActivityMarkupRule"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var activitymarkupruleobj = new ActivityMarkupRule();

                    activitymarkupruleobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataFareType381760 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType381760))
                    {
                        activitymarkupruleobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType381760.Split(',');
                        activitymarkupruleobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan392148 = ParseColumnData(row["AdvancePurchaseDaySpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan392148))
                    {
                        activitymarkupruleobj.AdvancePurchaseDaySpans = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan392148.Split(',');
                        activitymarkupruleobj.AdvancePurchaseDaySpans = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan102195 = ParseColumnData(row["TravelDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan102195))
                    {
                        activitymarkupruleobj.TravelDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan102195.Split(',');
                        activitymarkupruleobj.TravelDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDateSpan310959 = ParseColumnData(row["BookingDateSpan"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDateSpan310959))
                    {
                        activitymarkupruleobj.BookingDateSpans = Enumerable.Empty<TravelDateSpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDateSpan310959.Split(',');
                        activitymarkupruleobj.BookingDateSpans = data.TravelDateSpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataTravelDaySpan219578 = ParseColumnData(row["TravelDuration"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDaySpan219578))
                    {
                        activitymarkupruleobj.TravelDurations = Enumerable.Empty<TravelDaySpan>();
                    }
                    else
                    {
                        var splitData = cDataTravelDaySpan219578.Split(',');
                        activitymarkupruleobj.TravelDurations = data.TravelDaySpans.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataLocation418093 = ParseColumnData(row["Location"]);

                    if (string.IsNullOrWhiteSpace(cDataLocation418093))
                    {
                        activitymarkupruleobj.Locations = Enumerable.Empty<Location>();
                    }
                    else
                    {
                        var splitData = cDataLocation418093.Split(',');
                        activitymarkupruleobj.Locations = data.Locations.Where(x => splitData.Any(y => y.Equals(x.SNO)));
                    }
                    data.ActivityMarkupRules.Add(activitymarkupruleobj);
                }
                table = ds.Tables["MarkupValue"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupvalueobj = new MarkupValue();

                    markupvalueobj.Sno = ParseColumnData(row["Sno"]);
                    markupvalueobj.Currency = ParseColumnData(row["Currency"]);
                    var cDataValueType77304 = ParseColumnData(row["ValueType"]);

                    if (string.IsNullOrWhiteSpace(cDataValueType77304))
                    {
                        markupvalueobj.ValueTypes = Enumerable.Empty<ValueType>();
                    }
                    else
                    {
                        var splitData = cDataValueType77304.Split(',');
                        markupvalueobj.ValueTypes = data.ValueTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataFareCalculatedOn609374 = ParseColumnData(row["FareCalculatedOn"]);

                    if (string.IsNullOrWhiteSpace(cDataFareCalculatedOn609374))
                    {
                        markupvalueobj.FareCalculatedOns = Enumerable.Empty<FareCalculatedOn>();
                    }
                    else
                    {
                        var splitData = cDataFareCalculatedOn609374.Split(',');
                        markupvalueobj.FareCalculatedOns = data.FareCalculatedOns.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupvalueobj.FriendlyType = ParseColumnData(row["FriendlyType"]);
                    var cDataCalculationType535680 = ParseColumnData(row["CalculationType"]);

                    if (string.IsNullOrWhiteSpace(cDataCalculationType535680))
                    {
                        markupvalueobj.CalculationTypes = Enumerable.Empty<CalculationType>();
                    }
                    else
                    {
                        var splitData = cDataCalculationType535680.Split(',');
                        markupvalueobj.CalculationTypes = data.CalculationTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupvalueobj.Amount = ParseColumnData(row["Amount"]);
                    markupvalueobj.MinimumValue = ParseColumnData(row["MinimumValue"]);
                    markupvalueobj.MaximumValue = ParseColumnData(row["MaximumValue"]);
                    data.MarkupValues.Add(markupvalueobj);
                }
                table = ds.Tables["TravelDaySpan"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var traveldayspanobj = new TravelDaySpan();

                    traveldayspanobj.Sno = ParseColumnData(row["Sno"]);
                    traveldayspanobj.MinimumDays = ParseColumnData(row["MinimumDays"]);
                    traveldayspanobj.MaximumDays = ParseColumnData(row["MaximumDays"]);
                    data.TravelDaySpans.Add(traveldayspanobj);
                }
                table = ds.Tables["TravelDateSpan"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var traveldatespanobj = new TravelDateSpan();

                    traveldatespanobj.Sno = ParseColumnData(row["Sno"]);
                    traveldatespanobj.FromDate = ParseColumnData(row["FromDate"]);
                    traveldatespanobj.ToDate = ParseColumnData(row["ToDate"]);
                    data.TravelDateSpans.Add(traveldatespanobj);
                }
                table = ds.Tables["ItineraryType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var itinerarytypeobj = new ItineraryType();

                    itinerarytypeobj.Sno = ParseColumnData(row["Sno"]);
                    itinerarytypeobj.itineraryType = ParseColumnData(row["ItineraryType"]);
                    data.ItineraryTypes.Add(itinerarytypeobj);
                }
                table = ds.Tables["CabinType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var cabintypeobj = new CabinType();

                    cabintypeobj.Sno = ParseColumnData(row["Sno"]);
                    cabintypeobj.cabinType = ParseColumnData(row["CabinType"]);
                    data.CabinTypes.Add(cabintypeobj);
                }
                table = ds.Tables["FareType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var faretypeobj = new FareType();

                    faretypeobj.Sno = ParseColumnData(row["Sno"]);
                    faretypeobj.fareType = ParseColumnData(row["FareType"]);
                    data.FareTypes.Add(faretypeobj);
                }
                table = ds.Tables["CarCompany"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var carcompanyobj = new CarCompany();

                    carcompanyobj.Sno = ParseColumnData(row["Sno"]);
                    carcompanyobj.carCompany = ParseColumnData(row["CarCompany"]);
                    data.CarCompanys.Add(carcompanyobj);
                }
                table = ds.Tables["HotelChain"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var hotelchainobj = new HotelChain();

                    hotelchainobj.Sno = ParseColumnData(row["Sno"]);
                    hotelchainobj.hotelChain = ParseColumnData(row["HotelChain"]);
                    data.HotelChains.Add(hotelchainobj);
                }
                table = ds.Tables["HotelIdType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var hotelidtypeobj = new HotelIdType();

                    hotelidtypeobj.Sno = ParseColumnData(row["Sno"]);
                    hotelidtypeobj.hotelIdType = ParseColumnData(row["HotelIdType"]);
                    data.HotelIdTypes.Add(hotelidtypeobj);
                }
                table = ds.Tables["FareCalculatedOn"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var farecalculatedonobj = new FareCalculatedOn();

                    farecalculatedonobj.Sno = ParseColumnData(row["Sno"]);
                    farecalculatedonobj.fareCalculatedOn = ParseColumnData(row["FareCalculatedOn"]);
                    data.FareCalculatedOns.Add(farecalculatedonobj);
                }
                table = ds.Tables["CalculationType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var calculationtypeobj = new CalculationType();

                    calculationtypeobj.Sno = ParseColumnData(row["Sno"]);
                    calculationtypeobj.calculationType = ParseColumnData(row["CalculationType"]);
                    data.CalculationTypes.Add(calculationtypeobj);
                }
                table = ds.Tables["MarkUpValueType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupvaluetypeobj = new MarkUpValueType();

                    markupvaluetypeobj.Sno = ParseColumnData(row["Sno"]);
                    markupvaluetypeobj.markUpValueType = ParseColumnData(row["MarkUpValueType"]);
                    data.MarkUpValueTypes.Add(markupvaluetypeobj);
                }
                table = ds.Tables["ProductType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var producttypeobj = new ProductType();

                    producttypeobj.Sno = ParseColumnData(row["Sno"]);
                    producttypeobj.productType = ParseColumnData(row["ProductType"]);
                    data.ProductTypes.Add(producttypeobj);
                }
                table = ds.Tables["MarkupType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markuptypeobj = new MarkupType();

                    markuptypeobj.Sno = ParseColumnData(row["Sno"]);
                    markuptypeobj.markupType = ParseColumnData(row["MarkupType"]);
                    data.MarkupTypes.Add(markuptypeobj);
                }
                table = ds.Tables["ValueType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var valuetypeobj = new ValueType();

                    valuetypeobj.Sno = ParseColumnData(row["Sno"]);
                    valuetypeobj.valueType = ParseColumnData(row["ValueType"]);
                    data.ValueTypes.Add(valuetypeobj);
                }
                table = ds.Tables["Location"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var locationobj = new Location();

                    locationobj.SNO = ParseColumnData(row["SNO"]);
                    locationobj.AirPortCode = ParseColumnData(row["AirPortCode"]);
                    locationobj.CityCode = ParseColumnData(row["CityCode"]);
                    locationobj.CountryCode = ParseColumnData(row["CountryCode"]);
                    var cDataGeoCode201117 = ParseColumnData(row["GeoCode"]);

                    if (string.IsNullOrWhiteSpace(cDataGeoCode201117))
                    {
                        locationobj.GeoCodes = Enumerable.Empty<GeoCode>();
                    }
                    else
                    {
                        var splitData = cDataGeoCode201117.Split(',');
                        locationobj.GeoCodes = data.GeoCodes.Where(x => splitData.Any(y => y.Equals(x.SNO)));
                    }
                    locationobj.CheckExactMatch = ParseColumnData(row["CheckExactMatch"]);
                    data.Locations.Add(locationobj);
                }
                table = ds.Tables["GeoCode"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var geocodeobj = new GeoCode();

                    geocodeobj.SNO = ParseColumnData(row["SNO"]);
                    geocodeobj.Longitude = ParseColumnData(row["Longitude"]);
                    geocodeobj.Latitude = ParseColumnData(row["Latitude"]);
                    geocodeobj.RadiusUnit = ParseColumnData(row["RadiusUnit"]);
                    geocodeobj.RadiusValue = ParseColumnData(row["RadiusValue"]);
                    data.GeoCodes.Add(geocodeobj);
                }
                table = ds.Tables["CarCategory"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var carcategoryobj = new CarCategory();

                    carcategoryobj.Sno = ParseColumnData(row["Sno"]);
                    carcategoryobj.carCategory = ParseColumnData(row["CarCategory"]);
                    data.CarCategorys.Add(carcategoryobj);
                }
                table = ds.Tables["SupplierFareRange"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var supplierfarerangeobj = new SupplierFareRange();

                    supplierfarerangeobj.SNo = ParseColumnData(row["SNo"]);
                    supplierfarerangeobj.MinValue = ParseColumnData(row["MinValue"]);
                    supplierfarerangeobj.MaxValue = ParseColumnData(row["MaxValue"]);
                    supplierfarerangeobj.Currency = ParseColumnData(row["Currency"]);
                    supplierfarerangeobj.OperatingFareType = ParseColumnData(row["OperatingFareType"]);
                    data.SupplierFareRanges.Add(supplierfarerangeobj);
                }
                table = ds.Tables["DayOfWeek"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var dayofweekobj = new DayOfWeek();

                    dayofweekobj.Sno = ParseColumnData(row["Sno"]);
                    dayofweekobj.dayOfWeek = ParseColumnData(row["DayOfWeek"]);
                    data.DayOfWeeks.Add(dayofweekobj);
                }
                table = ds.Tables["MarkupStatus"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupstatusobj = new MarkupStatus();

                    markupstatusobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataMarkupStatusType29472 = ParseColumnData(row["MarkupStatusType"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupStatusType29472))
                    {
                        markupstatusobj.MarkupStatusTypes = Enumerable.Empty<MarkupStatusType>();
                    }
                    else
                    {
                        var splitData = cDataMarkupStatusType29472.Split(',');
                        markupstatusobj.MarkupStatusTypes = data.MarkupStatusTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.MarkupStatuss.Add(markupstatusobj);
                }
                table = ds.Tables["MarkupStatusType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupstatustypeobj = new MarkupStatusType();

                    markupstatustypeobj.Sno = ParseColumnData(row["Sno"]);
                    markupstatustypeobj.markupStatusType = ParseColumnData(row["MarkupStatusType"]);
                    data.MarkupStatusTypes.Add(markupstatustypeobj);
                }
                table = ds.Tables["SaveBulkMarkups"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var savebulkmarkupsobj = new SaveBulkMarkups();

                    savebulkmarkupsobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataMarkupRule471058 = ParseColumnData(row["MarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupRule471058))
                    {
                        savebulkmarkupsobj.MarkupRules = Enumerable.Empty<MarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataMarkupRule471058.Split(',');
                        savebulkmarkupsobj.MarkupRules = data.MarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataMarkupValue289293 = ParseColumnData(row["MarkupValue"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupValue289293))
                    {
                        savebulkmarkupsobj.MarkupValues = Enumerable.Empty<MarkupValue>();
                    }
                    else
                    {
                        var splitData = cDataMarkupValue289293.Split(',');
                        savebulkmarkupsobj.MarkupValues = data.MarkupValues.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    savebulkmarkupsobj.ConstraintId = ParseColumnData(row["ConstraintId"]);
                    savebulkmarkupsobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    savebulkmarkupsobj.ChildNodeId = ParseColumnData(row["ChildNodeId"]);
                    savebulkmarkupsobj.Description = ParseColumnData(row["Description"]);
                    savebulkmarkupsobj.FriendlyName = ParseColumnData(row["FriendlyName"]);
                    savebulkmarkupsobj.Enabled = ParseColumnData(row["Enabled"]);
                    var cDataMarkupType779532 = ParseColumnData(row["MarkupType"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupType779532))
                    {
                        savebulkmarkupsobj.MarkupTypes = Enumerable.Empty<MarkupType>();
                    }
                    else
                    {
                        var splitData = cDataMarkupType779532.Split(',');
                        savebulkmarkupsobj.MarkupTypes = data.MarkupTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    savebulkmarkupsobj.IsRefundable = ParseColumnData(row["IsRefundable"]);
                    savebulkmarkupsobj.Preference = ParseColumnData(row["Preference"]);
                    savebulkmarkupsobj.POS = ParseColumnData(row["POS"]);
                    savebulkmarkupsobj.FareSource = ParseColumnData(row["FareSource"]);
                    var cDataProductType427868 = ParseColumnData(row["ProductType"]);

                    if (string.IsNullOrWhiteSpace(cDataProductType427868))
                    {
                        savebulkmarkupsobj.ProductTypes = Enumerable.Empty<ProductType>();
                    }
                    else
                    {
                        var splitData = cDataProductType427868.Split(',');
                        savebulkmarkupsobj.ProductTypes = data.ProductTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    savebulkmarkupsobj.IsOverridable = ParseColumnData(row["IsOverridable"]);
                    savebulkmarkupsobj.IsInsert = ParseColumnData(row["IsInsert"]);
                    savebulkmarkupsobj.ExpectedStatus = ParseColumnData(row["ExpectedStatus"]);
                    savebulkmarkupsobj.IsExclusive = ParseColumnData(row["IsExclusive"]);
                    var cDataActionOptions946475 = ParseColumnData(row["ActionOptions"]);

                    if (string.IsNullOrWhiteSpace(cDataActionOptions946475))
                    {
                        savebulkmarkupsobj.ActionOptionss = Enumerable.Empty<ActionOptions>();
                    }
                    else
                    {
                        var splitData = cDataActionOptions946475.Split(',');
                        savebulkmarkupsobj.ActionOptionss = data.ActionOptionss.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    savebulkmarkupsobj.MarkupGroup = ParseColumnData(row["MarkupGroup"]);
                    data.SaveBulkMarkupss.Add(savebulkmarkupsobj);
                }
                table = ds.Tables["ActionOptions"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var actionoptionsobj = new ActionOptions();

                    actionoptionsobj.Sno = ParseColumnData(row["Sno"]);
                    actionoptionsobj.actionOptions = ParseColumnData(row["ActionOptions"]);
                    data.ActionOptionss.Add(actionoptionsobj);
                }
                table = ds.Tables["MarkupGroupingData"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var markupgroupingdataobj = new MarkupGroupingData();

                    markupgroupingdataobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataMarkupRule222544 = ParseColumnData(row["MarkupRule"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupRule222544))
                    {
                        markupgroupingdataobj.MarkupRules = Enumerable.Empty<MarkupRule>();
                    }
                    else
                    {
                        var splitData = cDataMarkupRule222544.Split(',');
                        markupgroupingdataobj.MarkupRules = data.MarkupRules.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataMarkupValue672567 = ParseColumnData(row["MarkupValue"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupValue672567))
                    {
                        markupgroupingdataobj.MarkupValues = Enumerable.Empty<MarkupValue>();
                    }
                    else
                    {
                        var splitData = cDataMarkupValue672567.Split(',');
                        markupgroupingdataobj.MarkupValues = data.MarkupValues.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupgroupingdataobj.ConstraintId = ParseColumnData(row["ConstraintId"]);
                    markupgroupingdataobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    markupgroupingdataobj.ChildNodeId = ParseColumnData(row["ChildNodeId"]);
                    markupgroupingdataobj.Description = ParseColumnData(row["Description"]);
                    markupgroupingdataobj.FriendlyName = ParseColumnData(row["FriendlyName"]);
                    markupgroupingdataobj.Enabled = ParseColumnData(row["Enabled"]);
                    var cDataMarkupType804042 = ParseColumnData(row["MarkupType"]);

                    if (string.IsNullOrWhiteSpace(cDataMarkupType804042))
                    {
                        markupgroupingdataobj.MarkupTypes = Enumerable.Empty<MarkupType>();
                    }
                    else
                    {
                        var splitData = cDataMarkupType804042.Split(',');
                        markupgroupingdataobj.MarkupTypes = data.MarkupTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupgroupingdataobj.IsRefundable = ParseColumnData(row["IsRefundable"]);
                    markupgroupingdataobj.Preference = ParseColumnData(row["Preference"]);
                    markupgroupingdataobj.POS = ParseColumnData(row["POS"]);
                    markupgroupingdataobj.FareSource = ParseColumnData(row["FareSource"]);
                    var cDataProductType515377 = ParseColumnData(row["ProductType"]);

                    if (string.IsNullOrWhiteSpace(cDataProductType515377))
                    {
                        markupgroupingdataobj.ProductTypes = Enumerable.Empty<ProductType>();
                    }
                    else
                    {
                        var splitData = cDataProductType515377.Split(',');
                        markupgroupingdataobj.ProductTypes = data.ProductTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    markupgroupingdataobj.IsOverridable = ParseColumnData(row["IsOverridable"]);
                    markupgroupingdataobj.IsInsert = ParseColumnData(row["IsInsert"]);
                    markupgroupingdataobj.ExpectedStatus = ParseColumnData(row["ExpectedStatus"]);
                    markupgroupingdataobj.IsExclusive = ParseColumnData(row["IsExclusive"]);
                    markupgroupingdataobj.UpdatedPreference = ParseColumnData(row["UpdatedPreference"]);
                    markupgroupingdataobj.AddedAtTop = ParseColumnData(row["AddedAtTop"]);
                    markupgroupingdataobj.MarkupGroup = ParseColumnData(row["MarkupGroup"]);
                    data.MarkupGroupingDatas.Add(markupgroupingdataobj);
                }
            }
            return data;
        }

        #endregion


        #region Private Methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private static DataSet GetExcelSheetDataSet(string excelSheetPath)
        {
            using (var stream = File.OpenRead(excelSheetPath))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    excelReader.IsFirstRowAsColumnNames = true;

                    var testSheet = excelReader.AsDataSet();

                    return testSheet;
                }
            }
        }

        private static string ParseColumnData(object data)
        {
            if (data == null)
                return null;

            return data.ToString();
        }

        #endregion
    }
}
