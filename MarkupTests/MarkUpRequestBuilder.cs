﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MarkupTests.ExcelClasses;
using MarkupTests.MarkupService;
using Tavisca.TravelNxt.Common.Extensions;
using ActivityMarkupRule = MarkupTests.MarkupService.ActivityMarkupRule;
using AirMarkupRule = MarkupTests.MarkupService.AirMarkupRule;
using CalculationType = MarkupTests.MarkupService.CalculationType;
using CarMarkupRule = MarkupTests.MarkupService.CarMarkupRule;
using FareCalculatedOn = MarkupTests.MarkupService.FareCalculatedOn;
using HotelMarkupRule = MarkupTests.MarkupService.HotelMarkupRule;
using MarkUpValueType = MarkupTests.MarkupService.MarkUpValueType;
using Markup = MarkupTests.MarkupService.Markup;
using MarkupStatusType = MarkupTests.MarkupService.MarkupStatusType;
using MarkupType = MarkupTests.MarkupService.MarkupType;
using MarkupValue = MarkupTests.MarkupService.MarkupValue;
using ProductType = MarkupTests.MarkupService.ProductType;

namespace MarkupTests
{
    public static class MarkupRequestBuilder
    {
        static List<string> _markupGroupList = new List<string>();

        public static LookupMarkupRQ CreateLookupMarkupRequest(ExcelClasses.Markup markup, MarkupStatusType markupStatusType)
        {
            var accountInfo = GetAccountInfo(markup.OwnerId, AccountType.Agency);
            MarkupType markuptype;
            Enum.TryParse(markup.MarkupTypes.First().markupType, out markuptype);
            ProductType productType;
            Enum.TryParse(markup.ProductTypes.First().productType, out productType);

            _markupGroupList.Add(markup.MarkupGroup);
            _markupGroupList = _markupGroupList.Distinct().ToList();

            var lookupMarkupRq = new LookupMarkupRQ()
            {
                AccountInfo = accountInfo,
                MarkupType = markuptype,
                PagingInfo = new PagingInfo()
                {
                    PageNumber = 1,
                    PageSize = 120,
                    TotalRecords = 300
                },
                ProductType = productType,
                MarkupStatusType = markupStatusType,
                MarkupGroupList = _markupGroupList
            };
            if (!string.IsNullOrEmpty(markup.FareSource))
                lookupMarkupRq.FaresourceId = Convert.ToInt32(markup.FareSource);
            if (!string.IsNullOrEmpty(markup.POS))
                lookupMarkupRq.POS = Convert.ToInt32(markup.POS);
            return lookupMarkupRq;
        }


        public static CreateMarkupRQ CreateMarkupRQ(ExcelClasses.Markup markup, bool addAtTop)
        {
            var accountInfo = GetAccountInfo(markup.OwnerId, AccountType.Agency);
            var markuptype = MarkupType.Markup;
            var markupTypes = markup.MarkupTypes.FirstOrDefault();
            if (markupTypes != null)
                Enum.TryParse(markupTypes.markupType, out markuptype);
            var productType = ProductType.Trip;
            var productTypes = markup.ProductTypes.FirstOrDefault();
            if (productTypes != null)
                Enum.TryParse(productTypes.productType, out productType);
            var request = new CreateMarkupRQ
            {
                AccountInfo = accountInfo,
                Markup = new Markup
                {
                    ChildNodeId = markup.ChildNodeId,
                    Enabled = CommonHelper.GetBooleanValue(markup.Enabled),
                    FriendlyName = markup.FriendlyName,
                    MarkupType = markuptype,
                    OwnerId = accountInfo.Id,
                    Preference = Convert.ToInt32(markup.Preference),
                    Product = productType,

                    IsOverridable = CommonHelper.GetBooleanValue(markup.IsOverridable),
                    Value = GetMarkupValue(markup.MarkupValues.FirstOrDefault(), markup.FriendlyName),
                    Rule = GetMarkupRule(productType, markup.MarkupRules.FirstOrDefault()),
                    Distribution = GetDistribution(markup.FareSource, markup.POS),
                    MarkupGroup = markup.MarkupGroup,
                    DynamicValue = GetDynamicValue(markup.DynamicValue),
                    IsExclusive = CommonHelper.GetBooleanValue(markup.IsExclusive)
                },
                AddAtTop = addAtTop
            };
            return request;
        }

        internal static DynamicValue GetDynamicValue(string dynamicValueXmlString)
        {
            if (string.IsNullOrEmpty(dynamicValueXmlString))
                return null;

            var dynamicValueXml = XElement.Parse(dynamicValueXmlString);

            return new DynamicValue()
            {
                Amount = Convert.ToDecimal(dynamicValueXml.GetElementValue("Amount", null, false)),
                CalculationLevel =
                    dynamicValueXml.GetElementValue("CalculationLevel", string.Empty, false).ToEnum
                    <CalculationLevel>(),
                MarkUpValueType = 
                    dynamicValueXml.GetElementValue("MarkUpValueType", string.Empty, false).ToEnum
                    <MarkUpValueType>(),
                MinimumValue =
                    Convert.ToDecimal(dynamicValueXml.GetElementValue("MinimumValue", null, false)),
                Currency = dynamicValueXml.GetElementValue("Currency", string.Empty, false)
            };
        }

        internal static Distribution GetDistribution(string fareSource, string posId)
        {
            return new Distribution
            {
                FaresourceId = CommonHelper.GetIntegerValue(fareSource),
                POS = CommonHelper.GetIntegerValue(posId)
            };
        }


        internal static AccountInfo GetAccountInfo(string ownerId, AccountType accountType)
        {
            return new AccountInfo() { Id = ownerId, Type = accountType };
        }

        internal static MarkupStatusType GetMarkupStatusType(IList<ExcelClasses.MarkupStatus> markupStatusTypes)
        {
            var markupStatus = MarkupStatusType.All;
            var markupStatusType = markupStatusTypes.FirstOrDefault();
            if (markupStatusType != null)
            {
                var firstOrDefault = markupStatusType.MarkupStatusTypes.FirstOrDefault();
                if (firstOrDefault != null)
                    Enum.TryParse(firstOrDefault.markupStatusType, out markupStatus);
            }
            return markupStatus;
        }

        internal static MarkupValue GetMarkupValue(ExcelClasses.MarkupValue markupValue, string description)
        {
            var fareCalculatedOns = FareCalculatedOn.BaseFare;
            var fareCalculatedOn = markupValue.FareCalculatedOns.FirstOrDefault();
            if (fareCalculatedOn != null)
                Enum.TryParse(fareCalculatedOn.fareCalculatedOn, out fareCalculatedOns);
            var calculationType = CalculationType.PerDay;
            var calculationTypes = markupValue.CalculationTypes.FirstOrDefault();
            if (calculationTypes != null)
                Enum.TryParse(calculationTypes.calculationType, out calculationType);
            var markUpValueType = MarkUpValueType.Absolute;
            var valueTypes = markupValue.ValueTypes.FirstOrDefault();
            if (valueTypes != null)
                Enum.TryParse(valueTypes.valueType, out markUpValueType);
            decimal? minValue = CommonHelper.GetDecimalValue(markupValue.MinimumValue);
            decimal? maxValue = CommonHelper.GetDecimalValue(markupValue.MaximumValue);
            if (minValue <= 0)
                minValue = null;
            if (maxValue <= 0)
                maxValue = null;
            return new MarkupValue()
            {
                Amount = string.IsNullOrEmpty(markupValue.Amount) ? 0 : Convert.ToDecimal(markupValue.Amount),
                ApplyOnFare = fareCalculatedOns,
                Currency = markupValue.Currency,
                FriendlyType = description, //markupValue.FriendlyType,
                MaximumValue = maxValue,
                MinimumValue = minValue,
                CalculationType = calculationType,
                ValueType = markUpValueType
            };
        }

        internal static Rule GetMarkupRule(ProductType productType, MarkupRule markupRule)
        {
            Rule rule = null;
            switch (productType)
            {
                case ProductType.Air:
                    rule = GetAirMarkupRule(markupRule.AirMarkupRules.FirstOrDefault());
                    break;
                case ProductType.Car:
                    rule = GetCarMarkupRule(markupRule.CarMarkupRules.FirstOrDefault());
                    break;
                case ProductType.Hotel:
                    rule = GetHotelMarkupRule(markupRule.HotelMarkupRules.FirstOrDefault());
                    break;
                case ProductType.Activity:
                    rule = GetActivityMarkupRule(markupRule.ActivityMarkupRules.FirstOrDefault());
                    break;
            }
            if (rule != null)
            {
                ApplyMarkupRule(markupRule, rule);
                return rule;
            }
            return null;
        }

        internal static AirMarkupRule GetAirMarkupRule(ExcelClasses.AirMarkupRule airMarkupRule)
        {
            var fareType = MarkupService.FareType.Any;
            var airFareTypes = airMarkupRule.FareTypes.FirstOrDefault();
            if (airFareTypes != null)
                Enum.TryParse(airFareTypes.fareType, out  fareType);
            var itineraryType = MarkupService.ItineraryType.Any;
            var airItineraryTypes = airMarkupRule.ItineraryTypes.FirstOrDefault();
            if (airItineraryTypes != null)
                Enum.TryParse(airItineraryTypes.itineraryType, out itineraryType);
            var cabinType = MarkupService.CabinType.Any;
            var airCabinTypes = airMarkupRule.CabinTypes.FirstOrDefault();
            if (airCabinTypes != null)
                Enum.TryParse(airCabinTypes.cabinType, out cabinType);
            return new AirMarkupRule()
            {
                Itinerary = new AirItinerary()
                {
                    OriginAirports = airMarkupRule.OriginAirports,
                    OriginCountries = airMarkupRule.OriginCountries,
                    ClassesOfService = airMarkupRule.ClassesOfService,
                    DestinationAirports = airMarkupRule.DestinationAirports,
                    DestinationCountries = airMarkupRule.DestinationCountries,
                    FareBasisCodes = airMarkupRule.FareBasisCodes,
                    MarketingCarriers = airMarkupRule.MarketingCarriers,
                    FareType = fareType
                },
                TravelType = new TravelType()
                {
                    ItineraryType = itineraryType,
                    CabinType = cabinType,
                },
                RateCode = airMarkupRule.FareBasisCodes,
                TravelDates = new TravelDates()
                {
                    AdvancePurchase = GetTravelDaySpan(airMarkupRule.AdvancePurchaseDaySpans.FirstOrDefault()),
                    TravelDuration = GetTravelDaySpan(airMarkupRule.TravelDurations.FirstOrDefault()),
                    BookingDateSpan = GetBookingDateSpan(airMarkupRule.BookingDateSpans.FirstOrDefault()),
                    TravelDateSpan = GetTravelDateSpan(airMarkupRule.TravelDateSpans.FirstOrDefault())
                },
                AirFareRange = GetAirFareRange(airMarkupRule.SupplierFareRanges)
            };
        }

        internal static AirFareRange GetAirFareRange(IEnumerable<ExcelClasses.SupplierFareRange> supplierFareRanges)
        {
            var fareRanges = supplierFareRanges as SupplierFareRange[] ?? supplierFareRanges.ToArray();
            if (supplierFareRanges == null || !fareRanges.Any())
                return null;
            var supplierFareRange = fareRanges.SingleOrDefault();
            if (supplierFareRange != null)
                return new AirFareRange()
                {
                    MinValue = CommonHelper.GetMinMaxValue(supplierFareRange.MinValue),
                    MaxValue = CommonHelper.GetMinMaxValue(supplierFareRange.MaxValue),
                    Currency = supplierFareRange.Currency,
                    FareRangeFareType = (FareRangeFareType)Enum.Parse(typeof(FareRangeFareType), supplierFareRange.OperatingFareType)
                };
            return null;
        }

        internal static CarFareRange GetCarFareRange(IEnumerable<ExcelClasses.SupplierFareRange> supplierFareRanges)
        {
            var fareRanges = supplierFareRanges as SupplierFareRange[] ?? supplierFareRanges.ToArray();
            if (supplierFareRanges == null || !fareRanges.Any())
                return null;
            var supplierFareRange = fareRanges.SingleOrDefault();
            if (supplierFareRange != null)
                return new CarFareRange()
                {
                    MinValue = CommonHelper.GetMinMaxValue(supplierFareRange.MinValue),
                    MaxValue = CommonHelper.GetMinMaxValue(supplierFareRange.MaxValue),
                    Currency = supplierFareRange.Currency,
                    FareRangeFareType = (FareRangeFareType)Enum.Parse(typeof(FareRangeFareType), supplierFareRange.OperatingFareType)
                };
            return null;
        }

        internal static void ApplyMarkupRule(MarkupRule markupRule, Rule rule)
        {
            if (rule != null)
            {
                rule.CustomerType = markupRule.CustomerType;
                rule.IsPublic = CommonHelper.GetBooleanValue(markupRule.IsPublic);
                rule.OwnerId = markupRule.OwnerId;
                rule.RateCode = markupRule.RateCode;
            }
        }

        internal static MarkupService.TravelDaySpan GetTravelDaySpan(ExcelClasses.TravelDaySpan travelDaySpan)
        {
            if (travelDaySpan == null)
                return null;
            return new MarkupService.TravelDaySpan()
            {
                MaximumDays = CommonHelper.GetIntegerValue(travelDaySpan.MaximumDays),
                MinimumDays = CommonHelper.GetIntegerValue(travelDaySpan.MinimumDays)
            };
        }

        internal static MarkupService.TravelDateSpan GetTravelDateSpan(ExcelClasses.TravelDateSpan travelDateSpan)
        {
            if (travelDateSpan == null)
                return null;
            return new MarkupService.TravelDateSpan()
            {
                From = DateTime.Now.AddDays(Convert.ToInt32(travelDateSpan.FromDate)),
                To = DateTime.Now.AddDays(Convert.ToInt32(travelDateSpan.ToDate))
            };
        }

        internal static CarMarkupRule GetCarMarkupRule(ExcelClasses.CarMarkupRule carMarkupRule)
        {
            var fareType = MarkupService.FareType.Any;
            var carCategory = MarkupService.CarCategory.NotSpecified;
            var dayOfWeek = MarkupService.DayOfWeek.None;

            var faretypes = carMarkupRule.FareTypes.FirstOrDefault();
            if (faretypes != null)
                Enum.TryParse(faretypes.fareType, out fareType);
            var carCompany = carMarkupRule.CarCompanys.FirstOrDefault();

            var carCategories = carMarkupRule.CarCategorys.FirstOrDefault();
            if (carCategories != null)
                Enum.TryParse(carCategories.carCategory, out carCategory);

            var location = carMarkupRule.Locations.FirstOrDefault();

            var dayOfWeeks = carMarkupRule.DayOfWeeks.FirstOrDefault();
            if (dayOfWeeks != null)
                Enum.TryParse(dayOfWeeks.dayOfWeek, out dayOfWeek);

            return new CarMarkupRule()
            {
                Itinerary = new CarItinerary()
                {
                    CarCompany = carCompany != null ? carCompany.carCompany : null,
                    Country = carMarkupRule.Country,
                    FareType = fareType,
                    CarCategory = carCategory,
                    Location = location == null ? null : GetLocation(location)
                },
                TravelDates = new TravelDates()
                {
                    AdvancePurchase = GetTravelDaySpan(carMarkupRule.AdvancePurchaseDaySpans.FirstOrDefault()),
                    TravelDuration = GetTravelDaySpan(carMarkupRule.TravelDaySpans.FirstOrDefault()),
                    BookingDateSpan = GetBookingDateSpan(carMarkupRule.BookingDateSpans.FirstOrDefault()),
                    TravelDateSpan = GetTravelDateSpan(carMarkupRule.TravelDateSpans.FirstOrDefault())
                },
                CarFareRange = GetCarFareRange(carMarkupRule.SupplierFareRanges),
                DayOfWeek = dayOfWeek
            };
        }

        private static MarkupService.TravelDateSpan GetBookingDateSpan(ExcelClasses.TravelDateSpan travelDateSpan)
        {
            if (travelDateSpan == null)
                return null;
            return new MarkupService.TravelDateSpan()
            {
                From = DateTime.Now.AddDays(-Convert.ToInt32(travelDateSpan.FromDate)),
                To = DateTime.Now.AddDays(Convert.ToInt32(travelDateSpan.ToDate))
            };
        }

        internal static HotelMarkupRule GetHotelMarkupRule(ExcelClasses.HotelMarkupRule hotelMarkupRule)
        {
            var fareType = MarkupService.FareType.Any;
            var first = hotelMarkupRule.FareTypes.FirstOrDefault();
            if (first != null)
                Enum.TryParse(first.fareType, out fareType);
            MarkupService.HotelIdType hotelIdType = MarkupService.HotelIdType.Supplier;
            var hotelIdTypes = hotelMarkupRule.HotelIdTypes.FirstOrDefault();
            if (!string.IsNullOrEmpty(hotelMarkupRule.HotelId) && hotelIdTypes != null)
                Enum.TryParse(hotelIdTypes.hotelIdType, out hotelIdType);

            var location = hotelMarkupRule.Locations.FirstOrDefault();

            return new HotelMarkupRule()
            {
                Itinerary = new HotelItinerary()
                {
                    FareType = fareType,
                    HotelChain = hotelMarkupRule.HotelChainId,
                    Location = location == null ? null : GetLocation(location),
                    HotelId = hotelMarkupRule.HotelId,
                    HotelIdType = hotelIdType,
                    HotelRoomType = hotelMarkupRule.HotelRoomType
                },
                TravelDates = new TravelDates()
                {
                    AdvancePurchase = GetTravelDaySpan(hotelMarkupRule.AdvancePurchaseDaySpans.FirstOrDefault()),
                    TravelDuration = GetTravelDaySpan(hotelMarkupRule.TravelDurations.FirstOrDefault()),
                    BookingDateSpan = GetBookingDateSpan(hotelMarkupRule.BookingDateSpans.FirstOrDefault()),
                    TravelDateSpan = GetTravelDateSpan(hotelMarkupRule.TravelDateSpans.FirstOrDefault())
                },
                HotelFareRange = GetHotelFareRange(hotelMarkupRule.SupplierFareRanges)
            };
        }

        private static HotelFareRange GetHotelFareRange(IEnumerable<SupplierFareRange> supplierFareRanges)
        {
            var fareRanges = supplierFareRanges as SupplierFareRange[] ?? supplierFareRanges.ToArray();
            if (supplierFareRanges == null || !fareRanges.Any())
                return null;
            var supplierFareRange = fareRanges.SingleOrDefault();
            if (supplierFareRange != null)
                return new HotelFareRange()
                {
                    MinValue = CommonHelper.GetMinMaxValue(supplierFareRange.MinValue),
                    MaxValue = CommonHelper.GetMinMaxValue(supplierFareRange.MaxValue),
                    Currency = supplierFareRange.Currency,
                    FareRangeFareType = (FareRangeFareType)Enum.Parse(typeof(FareRangeFareType), supplierFareRange.OperatingFareType)
                };
            return null;
        }

        internal static MarkupService.ActivityMarkupRule GetActivityMarkupRule(ExcelClasses.ActivityMarkupRule activityMarkupRule)
        {
            var fareType = MarkupService.FareType.Any;
            var fareTypes = activityMarkupRule.FareTypes.FirstOrDefault();
            if (fareTypes != null)
                Enum.TryParse(fareTypes.fareType, out fareType);

            var location = activityMarkupRule.Locations.FirstOrDefault();

            return new ActivityMarkupRule()
            {
                Itinerary = new ActivityItinerary()
                {
                    FareType = fareType,
                    Location = location == null ? null : GetLocation(location)
                },
                TravelDates = new TravelDates()
                {
                    AdvancePurchase = GetTravelDaySpan(activityMarkupRule.AdvancePurchaseDaySpans.FirstOrDefault()),
                    TravelDuration = GetTravelDaySpan(activityMarkupRule.TravelDurations.FirstOrDefault()),
                    BookingDateSpan = GetBookingDateSpan(activityMarkupRule.BookingDateSpans.FirstOrDefault()),
                    TravelDateSpan = GetTravelDateSpan(activityMarkupRule.TravelDateSpans.FirstOrDefault())
                },

            };
        }

        private static MarkupService.Location GetLocation(ExcelClasses.Location location)
        {
            if (location == null)
                return null;

            ExcelClasses.GeoCode geoCode = null;
            if (location != null)
                geoCode = location.GeoCodes.FirstOrDefault();
            var distanceUnit = DistanceUnit.KM;
            if (geoCode != null)
                Enum.TryParse(geoCode.RadiusUnit, out distanceUnit);

            return new MarkupService.Location()
            {
                AirportCode = location.AirPortCode,
                CityCode = location.CityCode,
                CountryCode = location.CountryCode,
                CheckExactMatch = location.CheckExactMatch == "1",
                GeoCode = geoCode == null ? null : new MarkupService.GeoCode()
                {
                    Latitude = Convert.ToSingle(geoCode.Latitude),
                    Longitude = Convert.ToSingle(geoCode.Longitude),
                    Radius = new Distance { Unit = distanceUnit, Value = CommonHelper.GetIntegerValue(geoCode.RadiusValue) }
                }
            };
        }
    }
}
