﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirMarkupTests;
using Tavisca.TravelNxt.Flight.Avail.Core;
using Tavisca.TravelNxt.Flight.Entities;
using AirContract = AirMarkupTests.AirContract;
using CabinType = Tavisca.TravelNxt.Flight.Entities.CabinType;
using FareType = Tavisca.TravelNxt.Flight.Entities.FareType;
using FlightFare = Tavisca.TravelNxt.Flight.Entities.FlightFare;
using FlightFareDetails = Tavisca.TravelNxt.Flight.FareComponent.FlightFareDetails;
using ItineraryTypeOptions = Tavisca.TravelNxt.Flight.Entities.ItineraryTypeOptions;
using PassengerFare = Tavisca.TravelNxt.Flight.Entities.PassengerFare;
using PassengerType = Tavisca.TravelNxt.Flight.Entities.PassengerType;
using ProviderSpaceConfiguration = Tavisca.TravelNxt.Flight.Entities.ProviderSpaceConfiguration;
using ProviderSpaceType = Tavisca.TravelNxt.Flight.Entities.ProviderSpaceType;
using Requester = AirMarkupTests.Requester;

namespace MarkupTests.AirTests
{
    internal class RequestBuilder
    {
        private static readonly FlightFareDetailsData Data = AirMarkupTestsCollection.ExcelData;
        public static FlightFareDetails GetFlightFareDetails(int sno)
        {
            return ParseFlightFareDetails(Data.FlightFareDetailss.First(x => x.Sno == sno.ToString()));
        }

        private static FlightFareDetails ParseFlightFareDetails(AirMarkupTests.FlightFareDetails flightFareDetails)
        {
            var details = new FlightFareDetails()
                       {
                           ArrivalTime = DateTime.Now.AddDays(int.Parse(flightFareDetails.ArrivalTime)),
                           DepartureTime = DateTime.Now.AddDays(int.Parse(flightFareDetails.DepartureTime)),
                           SegmentCount = int.Parse(flightFareDetails.SegmentCount),
                           LegCount = int.Parse(flightFareDetails.LegCount),
                           Provider =
                               ParseProviderSpace(Data.ProviderSpaces.First(x => x.Sno == flightFareDetails.Provider)),
                           DestinationCountry = flightFareDetails.DestinationCountry,
                           DestinationAirportCode = flightFareDetails.DestinationAirportCode,
                           OriginCountry = flightFareDetails.OriginCountry,
                           OriginAirportCode = flightFareDetails.OriginAirportCode,
                           JumpNegoRate = flightFareDetails.JumpNegoRate.Equals("0"),
                           MarketingCarriers = flightFareDetails.MarketingCarriers.Split(',').ToList(),
                           ItineraryType =
                               (ItineraryTypeOptions)
                               Enum.Parse(typeof (ItineraryTypeOptions),
                                          Data.ItineraryTypeOptionss.First(x => x.Sno == flightFareDetails.ItineraryType)
                                              .itineraryTypeOptions),
                           ClassOfServices = flightFareDetails.ClassOfServices.Split(',').ToList(),
                           FareBasisCodes = flightFareDetails.FareBasisCodes.Split(','),
                           CabinTypes = ParseCabinTypes(flightFareDetails.CabinTypes.Split(',').ToList()),
                           Fare = ParseFlightFare(Data.FlightFares.First(x => x.Sno == flightFareDetails.Fare))
                       };

            details.Fare.Savings = ParseMoney(Data.Moneys.FirstOrDefault(x => x.Sno == flightFareDetails.NetRateSavings));

            return details;
        }

        private static FlightFare ParseFlightFare(AirMarkupTests.FlightFare flightFare)
        {
            var ret = new FlightFare() { FareType = (FareType)Enum.Parse(typeof(FareType), flightFare.FareTypes.First().fareType) };

            ret.PassengerFares.AddRange(
                flightFare.PassengerFares.Split(',').Select(x => ParsePassengerFare(Data.PassengerFares.First(y => y.Sno == x)))
                );

            return ret;
        }

        private static PassengerFare ParsePassengerFare(AirMarkupTests.PassengerFare passengerFare)
        {
            return new PassengerFare()
                       {
                           Quantity = int.Parse(passengerFare.Quantity),
                           BaseAmount = ParseMoney(Data.Moneys.First(x => x.Sno == passengerFare.BaseAmount)),
                           PassengerType =
                               (PassengerType)
                               Enum.Parse(typeof (PassengerType), passengerFare.PassengerTypes.First().passengerType)
                       };
        }

        private static List<CabinType> ParseCabinTypes(List<string> cabinTypes)
        {
            cabinTypes = cabinTypes.Where(x => !string.IsNullOrEmpty(x)).ToList();
            return cabinTypes.
                Select(x =>
                       (CabinType)
                       Enum.Parse(typeof (CabinType), Data.CabinTypes.First(y => y.Sno == x).cabinType)).ToList();
        }

        private static Tavisca.TravelNxt.Flight.Entities.Money ParseMoney(AirMarkupTests.Money money)
        {
            return money == null
                       ? null
                       : new Tavisca.TravelNxt.Flight.Entities.Money(decimal.Parse(money.NativeAmount),
                                                                     money.NativeCurrency);
        }

        private static Tavisca.TravelNxt.Flight.Entities.ProviderSpace ParseProviderSpace(AirMarkupTests.ProviderSpace providerSpace)
        {
            return new Tavisca.TravelNxt.Flight.Entities.ProviderSpace(
                int.Parse(providerSpace.ID),
                providerSpace.Name,
                ParseRequester(providerSpace.Requesters.First()),
                providerSpace.Family,
                null, long.Parse(providerSpace.OwnerId),
                ProviderSpaceType.All,
                new List<ProviderSpaceConfiguration>(), new List<KeyValuePair<string, string>>()
                )
                       {
                           Contracts =
                               new List<Tavisca.TravelNxt.Flight.Entities.AirContract>()
                                   {
                                       ParseAirContract(
                                           Data.AirContracts.FirstOrDefault(x => x.Sno == providerSpace.Contract))
                                   }
                       };
        }

        private static Tavisca.TravelNxt.Flight.Entities.AirContract ParseAirContract(AirContract airContract)
        {
            if(airContract == null)
                return null;
            return new DbAirContract
                       {
                           ContractRateCode = airContract.ContractRateCode
                       };
        }

        private static Tavisca.TravelNxt.Flight.Entities.Requester ParseRequester(Requester requester)
        {
            return new Tavisca.TravelNxt.Flight.Entities.Requester(long.Parse(requester.ID), int.Parse(requester.POS),
                (CustomerType) Enum.Parse(typeof (CustomerType), requester.CustomerType));
        }
    }
}
