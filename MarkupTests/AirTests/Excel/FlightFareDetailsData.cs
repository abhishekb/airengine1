using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Excel;

namespace AirMarkupTests
{
    #region SheetClasses

    public class AdditionalInfoDictionary
    {
        public string Sno { get; set; }
        public string Item { get; set; }
    }

    public class Address
    {
        public string Sno { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public IEnumerable<City> Citys { get; set; }
        public string ZipCode { get; set; }
    }

    public class AirContract
    {
        public string Sno { get; set; }
        public string ContractId { get; set; }
        public string ContractName { get; set; }
        public string AgencyId { get; set; }
        public string BookAirFareSourceId { get; set; }
        public string ApplicableClassOfService { get; set; }
        public string ApplicableCabinTypes { get; set; }
        public string ValidPeriod { get; set; }
        public string TravelPeriod { get; set; }
        public string ContractType { get; set; }
        public string ContractRateCode { get; set; }
        public string ContractRateCodeType { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public string Preference { get; set; }
        public string AirportPairs { get; set; }
        public string Airlines { get; set; }
        public string FareBasisCodes { get; set; }
    }

    public class CabinType
    {
        public string Sno { get; set; }
        public string cabinType { get; set; }
    }

    public class City
    {
        public string Sno { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string WindowsTimeZone { get; set; }
    }

    public class ConfigurationType
    {
        public string Sno { get; set; }
        public string configurationType { get; set; }
    }

    public class DateTimeSpan
    {
        public string Sno { get; set; }
        public string End { get; set; }
        public string Start { get; set; }
    }

    public partial class FareComponent
    {
        public string Sno { get; set; }
        public IEnumerable<FareComponentType> FareComponentTypes { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string FareComponentRuleId { get; set; }
        public string Value { get; set; }
    }

    public class FareComponentType
    {
        public string Sno { get; set; }
        public string fareComponentType { get; set; }
    }

    public class FareRestrictionType
    {
        public string Sno { get; set; }
        public string fareRestrictionType { get; set; }
    }

    public class FareType
    {
        public string Sno { get; set; }
        public string fareType { get; set; }
    }

    public class FlightFare
    {
        public string Sno { get; set; }
        public string PassengerFares { get; set; }
        public string FareRestrictionTypes { get; set; }
        public IEnumerable<FareType> FareTypes { get; set; }
        public string FareAttributes { get; set; }
    }

    public class FlightFareDetails
    {
        public string Sno { get; set; }
        public string Fare { get; set; }
        public string Provider { get; set; }
        public string ItineraryType { get; set; }
        public string CabinTypes { get; set; }
        public string OriginCountry { get; set; }
        public string OriginAirportCode { get; set; }
        public string DestinationCountry { get; set; }
        public string DestinationAirportCode { get; set; }
        public string MarketingCarriers { get; set; }
        public string ClassOfServices { get; set; }
        public string FareBasisCodes { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public string LegCount { get; set; }
        public string SegmentCount { get; set; }
        public string CustomerType { get; set; }
        public string NetRateSavings { get; set; }
        public string JumpNegoRate { get; set; }
    }

    public class ItineraryTypeOptions
    {
        public string Sno { get; set; }
        public string itineraryTypeOptions { get; set; }
    }

    public class Money
    {
        public string Sno { get; set; }
        public string DisplayCurrency { get; set; }
        public string DisplayAmount { get; set; }
        public string BaseCurrency { get; set; }
        public string BaseAmount { get; set; }
        public string NativeAmount { get; set; }
        public string NativeCurrency { get; set; }
    }

    public class PassengerFare
    {
        public string Sno { get; set; }
        public string Markups { get; set; }
        public string Fees { get; set; }
        public string Commissions { get; set; }
        public string Taxes { get; set; }
        public string Discounts { get; set; }
        public string Quantity { get; set; }
        public string BaseAmount { get; set; }
        public IEnumerable<PassengerType> PassengerTypes { get; set; }
    }

    public class PassengerType
    {
        public string Sno { get; set; }
        public string passengerType { get; set; }
    }

    public class PointOfSale
    {
        public string Sno { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public partial class ProviderSpace
    {
        public string Sno { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public IEnumerable<Requester> Requesters { get; set; }
        public string EmulatedBy { get; set; }
        public string Type { get; set; }
        public string Configurations { get; set; }
        public string AdditionalInfo { get; set; }
        public string Contract { get; set; }
    }

    public class ProviderSpaceConfiguration
    {
        public string Sno { get; set; }
        public string ConfigType { get; set; }
        public string ContractVersion { get; set; }
        public string ServiceUri { get; set; }
    }

    public class ProviderSpaceType
    {
        public string Sno { get; set; }
        public string providerSpaceType { get; set; }
    }

    public class Requester
    {
        public string Sno { get; set; }
        public string ID { get; set; }
        public string POS { get; set; }
        public string Type { get; set; }
        public string CustomerType { get; set; }
        public IEnumerable<Address> Addresss { get; set; }
    }

    public class RequesterType
    {
        public string Sno { get; set; }
        public string requesterType { get; set; }
    }
    #endregion
    public class FlightFareDetailsData
    {
        #region Properties
        public IList<AdditionalInfoDictionary> AdditionalInfoDictionarys { get; set; }
        public IList<Address> Addresss { get; set; }
        public IList<AirContract> AirContracts { get; set; }
        public IList<CabinType> CabinTypes { get; set; }
        public IList<City> Citys { get; set; }
        public IList<ConfigurationType> ConfigurationTypes { get; set; }
        public IList<DateTimeSpan> DateTimeSpans { get; set; }
        public IList<FareComponent> FareComponents { get; set; }
        public IList<FareComponentType> FareComponentTypes { get; set; }
        public IList<FareRestrictionType> FareRestrictionTypes { get; set; }
        public IList<FareType> FareTypes { get; set; }
        public IList<FlightFare> FlightFares { get; set; }
        public IList<FlightFareDetails> FlightFareDetailss { get; set; }
        public IList<ItineraryTypeOptions> ItineraryTypeOptionss { get; set; }
        public IList<Money> Moneys { get; set; }
        public IList<PassengerFare> PassengerFares { get; set; }
        public IList<PassengerType> PassengerTypes { get; set; }
        public IList<PointOfSale> PointOfSales { get; set; }
        public IList<ProviderSpace> ProviderSpaces { get; set; }
        public IList<ProviderSpaceConfiguration> ProviderSpaceConfigurations { get; set; }
        public IList<ProviderSpaceType> ProviderSpaceTypes { get; set; }
        public IList<Requester> Requesters { get; set; }
        public IList<RequesterType> RequesterTypes { get; set; }
        #endregion

        #region Constructors

        public FlightFareDetailsData()
        {
            AdditionalInfoDictionarys = new List<AdditionalInfoDictionary>();
            Addresss = new List<Address>();
            AirContracts = new List<AirContract>();
            CabinTypes = new List<CabinType>();
            Citys = new List<City>();
            ConfigurationTypes = new List<ConfigurationType>();
            DateTimeSpans = new List<DateTimeSpan>();
            FareComponents = new List<FareComponent>();
            FareComponentTypes = new List<FareComponentType>();
            FareRestrictionTypes = new List<FareRestrictionType>();
            FareTypes = new List<FareType>();
            FlightFares = new List<FlightFare>();
            FlightFareDetailss = new List<FlightFareDetails>();
            ItineraryTypeOptionss = new List<ItineraryTypeOptions>();
            Moneys = new List<Money>();
            PassengerFares = new List<PassengerFare>();
            PassengerTypes = new List<PassengerType>();
            PointOfSales = new List<PointOfSale>();
            ProviderSpaces = new List<ProviderSpace>();
            ProviderSpaceConfigurations = new List<ProviderSpaceConfiguration>();
            ProviderSpaceTypes = new List<ProviderSpaceType>();
            Requesters = new List<Requester>();
            RequesterTypes = new List<RequesterType>();
        }

        #endregion

        #region Public Methods

        public static FlightFareDetailsData Load(string excelFilePath)
        {
            var data = new FlightFareDetailsData();

            if (string.IsNullOrEmpty(excelFilePath))
                throw new ArgumentNullException("excelFilePath");

            using (var ds = GetExcelSheetDataSet(excelFilePath))
            {
                DataTable table;

                table = ds.Tables["AdditionalInfoDictionary"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var additionalinfodictionaryobj = new AdditionalInfoDictionary();

                    additionalinfodictionaryobj.Sno = ParseColumnData(row["Sno"]);
                    additionalinfodictionaryobj.Item = ParseColumnData(row["Item"]);
                    data.AdditionalInfoDictionarys.Add(additionalinfodictionaryobj);
                }
                table = ds.Tables["Address"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var addressobj = new Address();

                    addressobj.Sno = ParseColumnData(row["Sno"]);
                    addressobj.AddressLine1 = ParseColumnData(row["AddressLine1"]);
                    addressobj.AddressLine2 = ParseColumnData(row["AddressLine2"]);
                    var cDataCity263684 = ParseColumnData(row["City"]);

                    if (string.IsNullOrWhiteSpace(cDataCity263684))
                    {
                        addressobj.Citys = Enumerable.Empty<City>();
                    }
                    else
                    {
                        var splitData = cDataCity263684.Split(',');
                        addressobj.Citys = data.Citys.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    addressobj.ZipCode = ParseColumnData(row["ZipCode"]);
                    data.Addresss.Add(addressobj);
                }
                table = ds.Tables["AirContract"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var aircontractobj = new AirContract();

                    aircontractobj.Sno = ParseColumnData(row["Sno"]);
                    aircontractobj.ContractId = ParseColumnData(row["ContractId"]);
                    aircontractobj.ContractName = ParseColumnData(row["ContractName"]);
                    aircontractobj.AgencyId = ParseColumnData(row["AgencyId"]);
                    aircontractobj.BookAirFareSourceId = ParseColumnData(row["BookAirFareSourceId"]);
                    aircontractobj.ApplicableClassOfService = ParseColumnData(row["ApplicableClassOfService"]);
                    aircontractobj.ApplicableCabinTypes = ParseColumnData(row["ApplicableCabinTypes"]);
                    aircontractobj.ValidPeriod = ParseColumnData(row["ValidPeriod"]);
                    aircontractobj.TravelPeriod = ParseColumnData(row["TravelPeriod"]);
                    aircontractobj.ContractType = ParseColumnData(row["ContractType"]);
                    aircontractobj.ContractRateCode = ParseColumnData(row["ContractRateCode"]);
                    aircontractobj.ContractRateCodeType = ParseColumnData(row["ContractRateCodeType"]);
                    var cDataFareType370989 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType370989))
                    {
                        aircontractobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType370989.Split(',');
                        aircontractobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    aircontractobj.Preference = ParseColumnData(row["Preference"]);
                    aircontractobj.AirportPairs = ParseColumnData(row["AirportPairs"]);
                    aircontractobj.Airlines = ParseColumnData(row["Airlines"]);
                    aircontractobj.FareBasisCodes = ParseColumnData(row["FareBasisCodes"]);
                    data.AirContracts.Add(aircontractobj);
                }
                table = ds.Tables["CabinType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var cabintypeobj = new CabinType();

                    cabintypeobj.Sno = ParseColumnData(row["Sno"]);
                    cabintypeobj.cabinType = ParseColumnData(row["CabinType"]);
                    data.CabinTypes.Add(cabintypeobj);
                }
                table = ds.Tables["City"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var cityobj = new City();

                    cityobj.Sno = ParseColumnData(row["Sno"]);
                    cityobj.CountryCode = ParseColumnData(row["CountryCode"]);
                    cityobj.StateCode = ParseColumnData(row["StateCode"]);
                    cityobj.Code = ParseColumnData(row["Code"]);
                    cityobj.Name = ParseColumnData(row["Name"]);
                    cityobj.WindowsTimeZone = ParseColumnData(row["WindowsTimeZone"]);
                    data.Citys.Add(cityobj);
                }
                table = ds.Tables["ConfigurationType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var configurationtypeobj = new ConfigurationType();

                    configurationtypeobj.Sno = ParseColumnData(row["Sno"]);
                    configurationtypeobj.configurationType = ParseColumnData(row["ConfigurationType"]);
                    data.ConfigurationTypes.Add(configurationtypeobj);
                }
                table = ds.Tables["DateTimeSpan"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var datetimespanobj = new DateTimeSpan();

                    datetimespanobj.Sno = ParseColumnData(row["Sno"]);
                    datetimespanobj.End = ParseColumnData(row["End"]);
                    datetimespanobj.Start = ParseColumnData(row["Start"]);
                    data.DateTimeSpans.Add(datetimespanobj);
                }
                table = ds.Tables["FareComponent"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var farecomponentobj = new FareComponent();

                    farecomponentobj.Sno = ParseColumnData(row["Sno"]);
                    var cDataFareComponentType893943 = ParseColumnData(row["FareComponentType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareComponentType893943))
                    {
                        farecomponentobj.FareComponentTypes = Enumerable.Empty<FareComponentType>();
                    }
                    else
                    {
                        var splitData = cDataFareComponentType893943.Split(',');
                        farecomponentobj.FareComponentTypes = data.FareComponentTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    farecomponentobj.Code = ParseColumnData(row["Code"]);
                    farecomponentobj.Description = ParseColumnData(row["Description"]);
                    farecomponentobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    farecomponentobj.FareComponentRuleId = ParseColumnData(row["FareComponentRuleId"]);
                    farecomponentobj.Value = ParseColumnData(row["Value"]);
                    data.FareComponents.Add(farecomponentobj);
                }
                table = ds.Tables["FareComponentType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var farecomponenttypeobj = new FareComponentType();

                    farecomponenttypeobj.Sno = ParseColumnData(row["Sno"]);
                    farecomponenttypeobj.fareComponentType = ParseColumnData(row["FareComponentType"]);
                    data.FareComponentTypes.Add(farecomponenttypeobj);
                }
                table = ds.Tables["FareRestrictionType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var farerestrictiontypeobj = new FareRestrictionType();

                    farerestrictiontypeobj.Sno = ParseColumnData(row["Sno"]);
                    farerestrictiontypeobj.fareRestrictionType = ParseColumnData(row["FareRestrictionType"]);
                    data.FareRestrictionTypes.Add(farerestrictiontypeobj);
                }
                table = ds.Tables["FareType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var faretypeobj = new FareType();

                    faretypeobj.Sno = ParseColumnData(row["Sno"]);
                    faretypeobj.fareType = ParseColumnData(row["FareType"]);
                    data.FareTypes.Add(faretypeobj);
                }
                table = ds.Tables["FlightFare"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightfareobj = new FlightFare();

                    flightfareobj.Sno = ParseColumnData(row["Sno"]);
                    flightfareobj.PassengerFares = ParseColumnData(row["PassengerFares"]);
                    flightfareobj.FareRestrictionTypes = ParseColumnData(row["FareRestrictionTypes"]);
                    var cDataFareType296236 = ParseColumnData(row["FareType"]);

                    if (string.IsNullOrWhiteSpace(cDataFareType296236))
                    {
                        flightfareobj.FareTypes = Enumerable.Empty<FareType>();
                    }
                    else
                    {
                        var splitData = cDataFareType296236.Split(',');
                        flightfareobj.FareTypes = data.FareTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    flightfareobj.FareAttributes = ParseColumnData(row["FareAttributes"]);
                    data.FlightFares.Add(flightfareobj);
                }
                table = ds.Tables["FlightFareDetails"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightfaredetailsobj = new FlightFareDetails();

                    flightfaredetailsobj.Sno = ParseColumnData(row["Sno"]);
                    flightfaredetailsobj.Fare = ParseColumnData(row["Fare"]);
                    flightfaredetailsobj.Provider = ParseColumnData(row["Provider"]);
                    flightfaredetailsobj.ItineraryType = ParseColumnData(row["ItineraryType"]);
                    flightfaredetailsobj.CabinTypes = ParseColumnData(row["CabinTypes"]);
                    flightfaredetailsobj.OriginCountry = ParseColumnData(row["OriginCountry"]);
                    flightfaredetailsobj.OriginAirportCode = ParseColumnData(row["OriginAirportCode"]);
                    flightfaredetailsobj.DestinationCountry = ParseColumnData(row["DestinationCountry"]);
                    flightfaredetailsobj.DestinationAirportCode = ParseColumnData(row["DestinationAirportCode"]);
                    flightfaredetailsobj.MarketingCarriers = ParseColumnData(row["MarketingCarriers"]);
                    flightfaredetailsobj.ClassOfServices = ParseColumnData(row["ClassOfServices"]);
                    flightfaredetailsobj.FareBasisCodes = ParseColumnData(row["FareBasisCodes"]);
                    flightfaredetailsobj.DepartureTime = ParseColumnData(row["DepartureTime"]);
                    flightfaredetailsobj.ArrivalTime = ParseColumnData(row["ArrivalTime"]);
                    flightfaredetailsobj.LegCount = ParseColumnData(row["LegCount"]);
                    flightfaredetailsobj.SegmentCount = ParseColumnData(row["SegmentCount"]);
                    flightfaredetailsobj.CustomerType = ParseColumnData(row["CustomerType"]);
                    flightfaredetailsobj.JumpNegoRate = ParseColumnData(row["JumpNegoRate"]);
                    flightfaredetailsobj.NetRateSavings = ParseColumnData(row["NetRateSavings"]);
                    data.FlightFareDetailss.Add(flightfaredetailsobj);
                }
                table = ds.Tables["ItineraryTypeOptions"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var itinerarytypeoptionsobj = new ItineraryTypeOptions();

                    itinerarytypeoptionsobj.Sno = ParseColumnData(row["Sno"]);
                    itinerarytypeoptionsobj.itineraryTypeOptions = ParseColumnData(row["ItineraryTypeOptions"]);
                    data.ItineraryTypeOptionss.Add(itinerarytypeoptionsobj);
                }
                table = ds.Tables["Money"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var moneyobj = new Money();

                    moneyobj.Sno = ParseColumnData(row["Sno"]);
                    moneyobj.DisplayCurrency = ParseColumnData(row["DisplayCurrency"]);
                    moneyobj.DisplayAmount = ParseColumnData(row["DisplayAmount"]);
                    moneyobj.BaseCurrency = ParseColumnData(row["BaseCurrency"]);
                    moneyobj.BaseAmount = ParseColumnData(row["BaseAmount"]);
                    moneyobj.NativeAmount = ParseColumnData(row["NativeAmount"]);
                    moneyobj.NativeCurrency = ParseColumnData(row["NativeCurrency"]);
                    data.Moneys.Add(moneyobj);
                }
                table = ds.Tables["PassengerFare"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var passengerfareobj = new PassengerFare();

                    passengerfareobj.Sno = ParseColumnData(row["Sno"]);
                    passengerfareobj.Markups = ParseColumnData(row["Markups"]);
                    passengerfareobj.Fees = ParseColumnData(row["Fees"]);
                    passengerfareobj.Commissions = ParseColumnData(row["Commissions"]);
                    passengerfareobj.Taxes = ParseColumnData(row["Taxes"]);
                    passengerfareobj.Discounts = ParseColumnData(row["Discounts"]);
                    passengerfareobj.Quantity = ParseColumnData(row["Quantity"]);
                    passengerfareobj.BaseAmount = ParseColumnData(row["BaseAmount"]);
                    var cDataPassengerType335768 = ParseColumnData(row["PassengerType"]);

                    if (string.IsNullOrWhiteSpace(cDataPassengerType335768))
                    {
                        passengerfareobj.PassengerTypes = Enumerable.Empty<PassengerType>();
                    }
                    else
                    {
                        var splitData = cDataPassengerType335768.Split(',');
                        passengerfareobj.PassengerTypes = data.PassengerTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.PassengerFares.Add(passengerfareobj);
                }
                table = ds.Tables["PassengerType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var passengertypeobj = new PassengerType();

                    passengertypeobj.Sno = ParseColumnData(row["Sno"]);
                    passengertypeobj.passengerType = ParseColumnData(row["PassengerType"]);
                    data.PassengerTypes.Add(passengertypeobj);
                }
                table = ds.Tables["PointOfSale"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var pointofsaleobj = new PointOfSale();

                    pointofsaleobj.Sno = ParseColumnData(row["Sno"]);
                    pointofsaleobj.ID = ParseColumnData(row["ID"]);
                    pointofsaleobj.Name = ParseColumnData(row["Name"]);
                    data.PointOfSales.Add(pointofsaleobj);
                }
                table = ds.Tables["ProviderSpace"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var providerspaceobj = new ProviderSpace();

                    providerspaceobj.Sno = ParseColumnData(row["Sno"]);
                    providerspaceobj.ID = ParseColumnData(row["ID"]);
                    providerspaceobj.Name = ParseColumnData(row["Name"]);
                    providerspaceobj.Family = ParseColumnData(row["Family"]);
                    var cDataRequester362613 = ParseColumnData(row["Requester"]);

                    if (string.IsNullOrWhiteSpace(cDataRequester362613))
                    {
                        providerspaceobj.Requesters = Enumerable.Empty<Requester>();
                    }
                    else
                    {
                        var splitData = cDataRequester362613.Split(',');
                        providerspaceobj.Requesters = data.Requesters.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    providerspaceobj.EmulatedBy = ParseColumnData(row["EmulatedBy"]);
                    providerspaceobj.OwnerId = ParseColumnData(row["OwnerId"]);
                    providerspaceobj.Type = ParseColumnData(row["Type"]);
                    providerspaceobj.Configurations = ParseColumnData(row["Configurations"]);
                    providerspaceobj.AdditionalInfo = ParseColumnData(row["AdditionalInfo"]);
                    providerspaceobj.Contract = ParseColumnData(row["Contract"]);
                    data.ProviderSpaces.Add(providerspaceobj);
                }
                table = ds.Tables["ProviderSpaceConfiguration"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var providerspaceconfigurationobj = new ProviderSpaceConfiguration();

                    providerspaceconfigurationobj.Sno = ParseColumnData(row["Sno"]);
                    providerspaceconfigurationobj.ConfigType = ParseColumnData(row["ConfigType"]);
                    providerspaceconfigurationobj.ContractVersion = ParseColumnData(row["ContractVersion"]);
                    providerspaceconfigurationobj.ServiceUri = ParseColumnData(row["ServiceUri"]);
                    data.ProviderSpaceConfigurations.Add(providerspaceconfigurationobj);
                }
                table = ds.Tables["ProviderSpaceType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var providerspacetypeobj = new ProviderSpaceType();

                    providerspacetypeobj.Sno = ParseColumnData(row["Sno"]);
                    providerspacetypeobj.providerSpaceType = ParseColumnData(row["ProviderSpaceType"]);
                    data.ProviderSpaceTypes.Add(providerspacetypeobj);
                }
                table = ds.Tables["Requester"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var requesterobj = new Requester();

                    requesterobj.Sno = ParseColumnData(row["Sno"]);
                    requesterobj.ID = ParseColumnData(row["ID"]);
                    requesterobj.POS = ParseColumnData(row["POS"]);
                    requesterobj.Type = ParseColumnData(row["Type"]);
                    requesterobj.CustomerType = ParseColumnData(row["CustomerType"]);
                    var cDataAddress242634 = ParseColumnData(row["Address"]);

                    if (string.IsNullOrWhiteSpace(cDataAddress242634))
                    {
                        requesterobj.Addresss = Enumerable.Empty<Address>();
                    }
                    else
                    {
                        var splitData = cDataAddress242634.Split(',');
                        requesterobj.Addresss = data.Addresss.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.Requesters.Add(requesterobj);
                }
                table = ds.Tables["RequesterType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var requestertypeobj = new RequesterType();

                    requestertypeobj.Sno = ParseColumnData(row["Sno"]);
                    requestertypeobj.requesterType = ParseColumnData(row["RequesterType"]);
                    data.RequesterTypes.Add(requestertypeobj);
                }
            }
            return data;
        }

        #endregion


        #region Private Methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private static DataSet GetExcelSheetDataSet(string excelSheetPath)
        {
            using (var stream = File.Open(excelSheetPath, FileMode.Open, FileAccess.Read))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    excelReader.IsFirstRowAsColumnNames = true;

                    var testSheet = excelReader.AsDataSet();

                    return testSheet;
                }
            }
        }

        private static string ParseColumnData(object data)
        {
            if (data == null)
                return null;

            return data.ToString();
        }

        #endregion
    }
}
