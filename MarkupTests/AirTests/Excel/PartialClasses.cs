﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkupTests.ExcelClasses;

namespace AirMarkupTests
{
    public partial class FareComponent
    {
        private string _ownerId;
        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }
    }

    public partial class ProviderSpace
    {
        private string _ownerId;
        public string OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = DataHelper.ReplaceOwnerValue(value); }
        }
    }
}
