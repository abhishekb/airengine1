﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Reflection;
using AirMarkupTests;
using MarkupTests.ExcelClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareComponent;
using ProviderSpace = Tavisca.TravelNxt.Flight.Entities.ProviderSpace;
using Tavisca.TravelNxt.Common.Extensions;

namespace MarkupTests.AirTests
{
    [TestClass]
    [DeploymentItem("FlightFareDetailsData.xlsx")]
    //[DeploymentItem("TestData.xlsx")]
    public class AirMarkupTestsCollection
    {
        public TestContext TestContext { get; set; }

        internal static readonly FlightFareDetailsData ExcelData =
            FlightFareDetailsData.Load("AirTests\\Excel\\FlightFareDetailsData.xlsx");

        [TestMethod, TestCategory("Markups"),
        DataSource("System.Data.Odbc", "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)}; DBQ=|DataDirectory|\\AirTests\\Excel\\TestMapping.xlsx",
            "Scenarios$", DataAccessMethod.Sequential)]
        public void TestAppliedMarkups()
        {
            var dataRow = TestContext.DataRow;

            if (!dataRow["IsEnabled"].ToString().Equals("1"))
                return;

            var fareManager = new AirFareManager();

            using (new AmbientContextScope(GetCallContext(dataRow)))
            {
                var flightFareDetails = RequestBuilder.GetFlightFareDetails(int.Parse(dataRow["Sno"].ToString()));
                fareManager.Initialize(new Collection<ProviderSpace>() {flightFareDetails.Provider});
                var posInfo = new PosInfo()
                                  {
                                      PosId = flightFareDetails.Provider.Requester.POS.ID,
                                      RequesterDK = flightFareDetails.Provider.OwnerId.ToString()
                                  };

                fareManager.ApplyFareComponents(posInfo, flightFareDetails);

                ValidateResponse(flightFareDetails.Fare, dataRow);
            }
        }

        private static void ValidateResponse(Tavisca.TravelNxt.Flight.Entities.FlightFare flightFare, DataRow dataRow)
        {
            var totalCommision = dataRow["TotalCommission"].ToString();
            var totalMarkup = dataRow["TotalMarkup"].ToString();
            var totalDiscount = dataRow["TotalDiscount"].ToString();
            var totalFees = dataRow["TotalFees"].ToString();

            Assert.AreEqual(flightFare.TotalCommission.NativeAmount, decimal.Parse(totalCommision));
            Assert.AreEqual(flightFare.TotalMarkup.NativeAmount, decimal.Parse(totalMarkup));
            Assert.AreEqual(flightFare.TotalDiscount.NativeAmount, decimal.Parse(totalDiscount));
            Assert.AreEqual(flightFare.TotalFees.NativeAmount, decimal.Parse(totalFees));
        }

        private static CallContext GetCallContext(DataRow dataRow)
        {
            var callContext = new CallContext("en-us", Guid.NewGuid().ToString(), dataRow["PosID"].ToString(),
                                             DataHelper.ReplaceOwnerValue(dataRow["DK"].ToString()), "USD", "", false);

            var field = typeof(CallContext).GetField("_baseCurrency", BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(callContext, "USD");

            return callContext;
        }

        #region Test Initialize/Cleanup

        [ClassInitialize]
        public static void TestInitialize(TestContext testContext)
        {
            MarkupCrud.TestInitialize();
        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            MarkupCrud.TestCleanup();
        }

        #endregion
    }
}
