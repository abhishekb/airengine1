﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarkupTests
{
    public static class CommonHelper
    {
        internal static bool GetBooleanValue(string value)
        {
            if (value.Trim() == "1")
                return true;
            bool isValue;
            Boolean.TryParse(value, out isValue);
            return isValue;
        }

        internal static Int32 GetIntegerValue(string value)
        {
            Int32 integerValue;
            Int32.TryParse(value, out integerValue);
            return integerValue;
        }

        internal static decimal GetDecimalValue(string value)
        {
            decimal decimalValue;
            decimal.TryParse(value, out decimalValue);
            return decimalValue;
        }

        internal static decimal? GetMinMaxValue(string value)
        {
            decimal decimalValue;
            if (!string.IsNullOrEmpty(value))
            {
                decimal.TryParse(value, out decimalValue);
                return decimalValue;
            }
            return null;
        }

        internal static void GetErrorMessage(string description, string scenarioDescription, bool isNegativeTestCase = false)
        {
            Console.WriteLine(description + " " + scenarioDescription + " scenario");
            if (!isNegativeTestCase)
                Assert.Fail(description + " " + scenarioDescription + " scenario");
        }
    }
}
