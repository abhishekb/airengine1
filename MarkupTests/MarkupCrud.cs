﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using MarkupTests.ExcelClasses;
using MarkupTests.MarkupService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchemaGenerator;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.Frameworks.Repositories;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Common.FareComponent.Data.Models;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareComponent;
using Markup = MarkupTests.MarkupService.Markup;
using MarkupValue = MarkupTests.MarkupService.MarkupValue;
using Rule = MarkupTests.MarkupService.Rule;

namespace MarkupTests
{
    //[TestClass]
    //[DeploymentItem("FareComponents.xlsx")]
    public class MarkupCrud
    {
        #region test initialize / cleanup

        private static List<Markup> lookupResponses;

        //[ClassInitialize]
        public static void TestInitialize()
        {
            lookupResponses = new List<Markup>();
            using (var client = new MarkUpServiceClient())
            {
                var fareComponents = FareComponents.Load("FareComponents.xlsx");
                if (fareComponents == null || fareComponents.Markups == null || fareComponents.Markups.Count <= 0)
                    return;
                var markupStatus = MarkupRequestBuilder.GetMarkupStatusType(fareComponents.MarkupStatuss);
                foreach (var markup in fareComponents.Markups)
                {
                    if (!CommonHelper.GetBooleanValue(markup.IsInsert) ||
                        !CommonHelper.GetBooleanValue(markup.ExpectedStatus)) continue;
                    var addAtTop = CommonHelper.GetBooleanValue(markup.AddedAtTop);
                    var request = MarkupRequestBuilder.CreateMarkupRQ(markup, addAtTop);

                    var response = client.CreateMarkup(request);

                    ValidateBaseResponse(response, markup.ExpectedStatus);

                    var lookupMarkupRq = MarkupRequestBuilder.CreateLookupMarkupRequest(markup, markupStatus);
                    lookupMarkupRq.MarkupGroupList.RemoveAll(x => !x.Equals(markup.MarkupGroup));

                    var lookupResponse = client.LookupMarkup(lookupMarkupRq);
                    lookupResponses.AddRange(lookupResponse.Markups);
                    if (addAtTop)
                        ValidateAddedOnTopMarkup(lookupResponse, markup);
                    else
                        ValidateAddedToBottomMarkup(lookupResponse, markup);
                }
            }
        }

        //[ClassCleanup]
        public static void TestCleanup()
        {
            var factory = RuntimeContext.Resolver.Resolve<IRepositoryFactory>();
            var repository = factory.GetRepository(KeyStore.Repositories.MarkupReadWrite);
            
            lookupResponses.ForEach(lookupResponse =>
                                        {
                                            var flag = false;
                                            if (lookupResponse.Rule != null)
                                            {
                                                repository.Delete
                                                    <Tavisca.TravelNxt.Common.FareComponent.Data.Models.MarkupRule>(
                                                        x => x.Id == lookupResponse.Rule.Id);
                                                flag = true;
                                            }

                                            if (lookupResponse.Value != null)
                                            {
                                                repository.Delete
                                                    <Tavisca.TravelNxt.Common.FareComponent.Data.Models.MarkupValue>(
                                                        x => x.Id == lookupResponse.Rule.Id);
                                                flag = true;
                                            }

                                            if (lookupResponse.Constraints != null)
                                            {
                                                repository.Delete
                                                    <MarkupConstraint>(
                                                        x => x.Id == lookupResponse.Constraints.Id);
                                                flag = true;
                                            }
                                            if (flag == false)
                                                repository.Delete
                                                    <Tavisca.TravelNxt.Common.FareComponent.Data.Models.Markup>(
                                                        x => x.Id == lookupResponse.Id);
                                        });

            repository.SaveChanges();

        }


        #endregion

        private List<PassengerFare> GetPassengerFares()
        {
            var fares = new List<PassengerFare>();

            var fare = new PassengerFare()
                           {
                               BaseAmount = new Money(100, "USD"),
                               PassengerType = PassengerType.Adult,
                               Quantity = 1
                           };

            fares.Add(fare);

            return fares;
        }

        #region private methods

        private static void ValidateBaseResponse(BaseResponse response, string expectedStatus = "1")
        {
            Assert.IsNotNull(response);
            if (expectedStatus.Equals("1"))
                Assert.IsTrue(response.Status.Successful);
            else
                Assert.IsFalse(response.Status.Successful);
        }

        private static void ValidateAddedOnTopMarkup(LookupMarkupRS lookupResponse, ExcelClasses.Markup markup)
        {
            var markupAdded = lookupResponse.Markups.First(m => m.FriendlyName.Contains(markup.FriendlyName));
            //Assert.IsTrue(markupAdded.Preference == 1);
        }

        private static void ValidateBaseNegativeResponse(MarkupService.BaseResponse baseResponse)
        {
            Assert.IsNotNull(baseResponse);
            Assert.IsFalse(baseResponse.Status.Successful);
        }

        private static void ValidateGetMarkupNegativeResponse(MarkupService.GetMarkupRS getMarkupResponse, ExcelClasses.Markup markup)
        {
            Assert.IsNull(getMarkupResponse.Markup);
            Assert.IsTrue(getMarkupResponse.Status.Message.Contains("Markup does not exist"));
        }

        private static void ValidateAddedToBottomMarkup(LookupMarkupRS lookupResponse, ExcelClasses.Markup markup)
        {
            int lastEnabledMarkupPreference = 0;

            var enabledMarkups = lookupResponse.Markups.Where(x => x.Enabled).ToList();

            if (enabledMarkups.Count > 0)
            {
                lastEnabledMarkupPreference = enabledMarkups.Max(x => x.Preference);
            }

            var markupAdded = lookupResponse.Markups.Find(m => m.FriendlyName.Contains(markup.FriendlyName));
            Assert.IsTrue(markupAdded != null);
            //if (CommonHelper.GetBooleanValue(markup.Enabled))
            //    Assert.IsTrue(markupAdded.Preference == lastEnabledMarkupPreference);
            //else
            //{
            //    Assert.IsTrue((lastEnabledMarkupPreference + 1) == markupAdded.Preference);
            //}

        }

        private static MarkupService.Markup ValidateLookupResponse(LookupMarkupRS response, ExcelClasses.Markup markup)
        {
            ValidateBaseResponse(response);
            Assert.IsTrue(response.Markups != null);
            var retrievedMarkup = response.Markups.Find(x => x.FriendlyName == markup.FriendlyName);
            Assert.IsTrue(retrievedMarkup != null);
            Assert.IsTrue(retrievedMarkup.ChildNodeId == markup.ChildNodeId);
            Assert.IsTrue(retrievedMarkup.OwnerId == markup.OwnerId);
            return retrievedMarkup;
        }

        private static void ValidateCreateMarkupRS(CreateMarkupRS createMarkupRS, string expectedStatus = "1")
        {
            ValidateBaseResponse(createMarkupRS, expectedStatus);
        }
        #endregion
    }
}
