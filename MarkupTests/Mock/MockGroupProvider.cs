﻿using System;
using System.Globalization;
using Tavisca.TravelNxt.Common.TestHelpers;

namespace MarkupTests.Mock
{
    public class MockGroupProvider : Tavisca.TravelNxt.Common.FareComponent.IGroupProvider
    {
        public string GetGroupAssignedToNodeByParent(long node)
        {
            if (node == AccountHelper.GetTestClientAccountId())
            {
                return "GroupA";
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetSelfAssignedGroup(long node)
        {
            return "Self";
        }
    }
}
