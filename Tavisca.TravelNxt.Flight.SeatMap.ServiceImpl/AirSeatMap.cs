﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Frameworks.Parallel.Ambience.Wcf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.SeatMap.Core.Exceptions;
using Tavisca.TravelNxt.Flight.SeatMap.DataContract;
using Tavisca.TravelNxt.Flight.SeatMap.ServiceContract;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.SeatMap.ServiceImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AmbientOperation(ApplicationName = "Air Seat Map")]
    public class AirSeatMap : IAirSeatMap
    {
        static AirSeatMap()
        {
            Utility.SetApplicationName("Air Engine");
        }

        #region IAirSeatMap Members

        public SeatMapRS GetSeatMap(SeatMapRQ request)
        {
            if (request == null)
                throw new FaultException<ArgumentNullException>(new ArgumentNullException("request"));

            var stopwatch = new WrappedTimer();
            stopwatch.Start();

            ILogger logFactory = null;
            IEventEntry entry = null;
            try
            {
                logFactory = Utility.GetLogFactory();
                
                entry = Utility.GetEventEntryContextual();

                entry.Title = "SeatMap - Root Level Seat Map Log";

                entry.CallType = KeyStore.CallTypes.AirSeatmap;

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var seatMapEngine = RuntimeContext.Resolver.Resolve<ISeatMapEngine>();

                var entity = SeatMapRQ.ToEntity(request);

                var result = seatMapEngine.SeatMap(entity);

                var retVal = SeatMapRS.ToDataContract(result);

                //entry.TimeTaken = (DateTime.Now - startTime).TotalSeconds;

                if (retVal != null)
                {
                    entry.ResponseObject = retVal;
                    if (retVal.ServiceStatus != null && retVal.ServiceStatus.Status == CallStatus.Failure)
                        entry.StatusType = StatusOptions.Failure;
                }

                return retVal; 
            }
            catch(HandledException handledException)
            {
                if (entry != null) 
                    entry.StatusType = StatusOptions.Failure;

                return handledException.InnerException.GetType() == typeof (SeatMapSupplierException)
                           ? GetFailureResponse(KeyStore.ErrorCodes.SupplierFailure)
                           : GetFailureResponse();
            }
            catch (Exception ex)
            {
                if (entry != null)
                    entry.StatusType = StatusOptions.Failure;

                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                return GetFailureResponse();
            }
            finally
            {
                if (logFactory != null && entry != null)
                {
                    stopwatch.Stop();
                    entry.TimeTaken = stopwatch.ElapsedSeconds;
                    logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
                }
            }
        }

        #endregion

        #region private methods

        private static SeatMapRS GetFailureResponse(string errorCode = KeyStore.ErrorCodes.InternalError)
        {
            return new SeatMapRS()
                       {
                           ServiceStatus = new ServiceStatus()
                                               {
                                                   Messages = new List<string>(),
                                                   Status = CallStatus.Failure,
                                                   StatusCode = errorCode,
                                                   StatusMessage =
                                                       ErrorCodeManager.GetErrorMessage(errorCode,
                                                                                        KeyStore.ErrorMessages.
                                                                                            DefaultErrorMessage)
                                               },
                           SessionId = Utility.GetCurrentContext().SessionId
                       };
        }

        #endregion
    }
}
