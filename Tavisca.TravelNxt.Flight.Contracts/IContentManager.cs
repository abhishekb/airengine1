using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IContentManager
    {
        void LoadFlightSegmentStaticContent(IList<FlightRecommendation> flightRecommendations);
        Airport GetAirportByCode(string airportCode);
        List<City> GetCitiesByCode(string cityCode);
        Airline GetAirlineByCode(string airlineCode);
        IEnumerable<Airport> GetNearbyAirports(string airportCode, bool includeSelf = true, int? radiusKm = null);
        List<Airport> GetAirportsForCity(string cityCode);
    }
}