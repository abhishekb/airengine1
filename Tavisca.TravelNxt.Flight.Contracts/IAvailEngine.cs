﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IAvailEngine
    {
        void NotifySession(SessionNotificationType sessionNotificationType);
        FlightAvailResult Avail(Requester requester, FlightSearchCriteria searchCriteria, bool registerSupplierStreaming);
        FlightAvailResult TryGetResult(DateTime? previousRequestTimeStamp);
    }

    public enum SessionNotificationType
    {
        Begun = 0,
        Result = 1
    }
}
