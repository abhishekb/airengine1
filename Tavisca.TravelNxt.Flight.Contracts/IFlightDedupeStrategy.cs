﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
   public interface IFlightDedupeStrategy
    {
        List<FlightRecommendation> Dedupe(List<FlightRecommendation> uniqueRecommendations,
            FlightRecommendation newRecommendation);

        IFlightKeyProvider FLightKeyProvider { get; set; }

        IDictionary<string, int> ProviderFamilyPreferenceMapping { get; set; }
    }
}
