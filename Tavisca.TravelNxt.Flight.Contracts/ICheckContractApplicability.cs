﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface ICheckContractApplicability
    {
        bool IsContractApplicable(FlightRecommendation flightRecommendation);

        bool CanSellRecommendation(FlightRecommendation flightRecommendation);
    }
}
