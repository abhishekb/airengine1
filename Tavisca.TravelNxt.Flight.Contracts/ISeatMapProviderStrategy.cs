using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.SeatMap;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface ISeatMapProviderStrategy
    {
        SeatMapRS GetSeatMap(SeatMapRQ request, FlightSegment flightSegment, FlightRecommendation recommendation);
    }
}