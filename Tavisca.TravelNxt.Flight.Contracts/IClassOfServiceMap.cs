﻿using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IClassOfServiceMap
    {
        CabinType GetClassOfService(FlightSegment segment, string classOfService);
    }
}