using Tavisca.TravelNxt.Flight.Entities.FareRules;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFareRulesEngine
    {
        FareRulesRS GetFareRules(FareRulesRQ request);
    }
}