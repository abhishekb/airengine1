﻿using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IPaperContract
    {
        AirContract GetContract();

        AirContract GetOverriddenContract(AirContract airContract);
    }
}
