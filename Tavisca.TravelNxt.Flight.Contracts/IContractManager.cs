﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Contracts
{
   public interface IContractManager
   {
       ICollection<AirContract> LoadContractsForProvider(long providerSpaceId, FlightSearchCriteria searchCriteria);
   }
}
