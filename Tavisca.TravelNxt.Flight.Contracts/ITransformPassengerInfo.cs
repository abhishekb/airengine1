﻿using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface ITransformPassengerInfo
    {
        FlightSearchCriteria GetTransformedSearchCriteria(FlightSearchCriteria searchCriteria, ProviderSpace providerSpace);
    }
}
