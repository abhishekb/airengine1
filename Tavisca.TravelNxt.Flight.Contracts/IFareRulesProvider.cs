﻿using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.FareRules;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFareRulesProvider
    {
        FareRulesRS GetFareRules(FareRulesRQ request, FlightRecommendation recommendation);
    }
}
