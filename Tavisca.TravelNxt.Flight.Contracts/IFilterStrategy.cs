﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFilterStrategy
    {
        bool ShouldFilter(FlightRecommendation flightRecommendation, FlightSearchCriteria flightSearchCriteria);

        IList<FlightRecommendation> Filter(IList<FlightRecommendation> flightRecommendations, FlightSearchCriteria searchCriteria);
    }
}