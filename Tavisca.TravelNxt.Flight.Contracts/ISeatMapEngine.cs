using Tavisca.TravelNxt.Flight.Entities.SeatMap;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface ISeatMapEngine
    {
        SeatMapRS SeatMap(SeatMapRQ request);
    }
}