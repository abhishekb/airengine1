using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IAirlineInterlineAgreementProvider
    {
        bool IsRecommendationTicketable(FlightRecommendation recommendation, Requester requester);
    }
}