﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Pricing;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IPricingProviderStrategy
    {
         FlightRecommendation Price(FlightPriceRQ request, FlightRecommendation recommendation);
    }
}
