﻿using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFlightKeyProvider
    {
        string GetKey(FlightRecommendation flightRecommendation);
    }
}
