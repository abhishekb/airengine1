﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities.Booking;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IBookingEngine
    {
        FlightSaveRS Save(FlightSaveRQ request);
        FlightRetrieveRS Retrieve(FlightRetrieveRQ request);
    }
}
