using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IAirlineTicketingInfoProvider
    {
        IList<PosTicketingAirlineInfo> GetPosAirlineTicketingInfo();
        IList<PosInterAirlineAgreement> GetAirTicketingAgreements();
    }
}