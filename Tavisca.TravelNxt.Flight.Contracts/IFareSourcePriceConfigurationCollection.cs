﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Flight.Entities.Pricing;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFareSourcePriceConfigurationCollection : ICollection<PriceConfiguration>
    {
        void LoadAll();
        PriceConfiguration GetConfiguration(int posId, long accountId);
        PriceConfiguration GetDefault();
    }
}