﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IProviderSpaceFilterProvider
    {
        IList<ProviderSpace> FilterProviderSpaces(IList<ProviderSpace> providerSpaces,
                                                  FlightSearchCriteria searchCriteria);
    }
}
