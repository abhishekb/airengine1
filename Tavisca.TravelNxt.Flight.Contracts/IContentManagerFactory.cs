namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IContentManagerFactory
    {
        IContentManager GetContentManager();
    }
}