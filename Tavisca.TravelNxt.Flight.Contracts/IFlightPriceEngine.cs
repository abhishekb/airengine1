﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities.Pricing;

namespace Tavisca.TravelNxt.Flight.Contracts
{
    public interface IFlightPriceEngine
    {
        FlightPriceResult Pricing(FlightPriceRQ request);
        FlightPriceResult GetPrePricedResult(FlightGetPricedRQ request);
    }
}
