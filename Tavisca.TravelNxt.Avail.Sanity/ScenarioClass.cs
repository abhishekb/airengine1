﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Excel;

namespace Tavisca.TravelNxt.Avail.Sanity
{
    #region SheetClasses

    public class AlternateAirportInformation
    {
        public string Sno { get; set; }
        public string AirportCodes { get; set; }
        public string IncludeNearByAirports { get; set; }
        public string RadiusKm { get; set; }
    }

    public class CabinType
    {
        public string Sno { get; set; }
        public string cabinType { get; set; }
    }

    public class ConnectionPreference
    {
        public string Sno { get; set; }
        public string AirportCode { get; set; }
        public IEnumerable<ConnectionPreferenceType> PreferenceTypes { get; set; }
    }

    public class ConnectionPreferenceType
    {
        public string Sno { get; set; }
        public string connectionPreferenceType { get; set; }
    }

    public class FlightSearchCriteria
    {
        public string Sno { get; set; }
        public string AdditionalInfo { get; set; }
        public IEnumerable<FlightTravelPreference> FlightTravelPreferences { get; set; }
        public string MaxPreferredResults { get; set; }
        public IEnumerable<PaxTypeQuantity> PassengerInfoSummarys { get; set; }
        public IEnumerable<SearchSegment> SearchSegmentss { get; set; }
        public IEnumerable<SortingOrder> SortingOrders { get; set; }
    }

    public class FlightSearchRQ
    {
        public string Sno { get; set; }
        public string AllowSupplierStreaming { get; set; }
        public string DoDeferredSearch { get; set; }
        public IEnumerable<Requester> Requesters { get; set; }
        public IEnumerable<FlightSearchCriteria> SearchCriterions { get; set; }
    }

    public class FlightTravelPreference
    {
        public string Sno { get; set; }
        public string AllowMixedAirlines { get; set; }
        public IEnumerable<CabinType> CabinClassesPreferreds { get; set; }
        public string ExcludeAirlines { get; set; }
        public string IncludeAirlines { get; set; }
        public string JetFlightsOnly { get; set; }
        public string Refundable { get; set; }
        public string UnRestrictedFare { get; set; }
    }

    public class PassengerType
    {
        public string Sno { get; set; }
        public string passengerType { get; set; }
    }

    public class PaxTypeQuantity
    {
        public string Sno { get; set; }
        public string Ages { get; set; }
        public IEnumerable<PassengerType> PassengerTypes { get; set; }
        public string Quantity { get; set; }
    }

    public class Requester
    {
        public string Sno { get; set; }
        public string AdditionalInfo { get; set; }
        public string RequesterOrigin { get; set; }
    }

    public class SearchSegment
    {
        public string Sno { get; set; }
        public string ArrivalAirportCode { get; set; }
        public string ArrivalCityCode { get; set; }
        public string DepartureCityCode { get; set; }
        public IEnumerable<AlternateAirportInformation> ArrivalAlternateAirportInformations { get; set; }
        public IEnumerable<CabinType> Cabins { get; set; }
        public IEnumerable<ConnectionPreference> ConnectionPreferencess { get; set; }
        public string DepartureAirportCode { get; set; }
        public IEnumerable<AlternateAirportInformation> DepartureAlternateAirportInformations { get; set; }
        public string IncludeDirectOnly { get; set; }
        public string IncludeNonStopOnly { get; set; }
        public string IncludeServiceClass { get; set; }
        public IEnumerable<TravelDate> TravelDates { get; set; }
    }

    public class SortingOrder
    {
        public string Sno { get; set; }
        public string sortingOrder { get; set; }
    }

    public class TravelDate
    {
        public string Sno { get; set; }
        public string AnyTime { get; set; }
        public string DateTime { get; set; }
        public string MaxDate { get; set; }
        public string MinDate { get; set; }
        public string TimeWindow { get; set; }
    }

    public class HeaderConfiguration
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    #endregion
    public class FlightSearchRQTestData
    {
        #region Properties
        public IList<AlternateAirportInformation> AlternateAirportInformations { get; set; }
        public IList<CabinType> CabinTypes { get; set; }
        public IList<ConnectionPreference> ConnectionPreferences { get; set; }
        public IList<ConnectionPreferenceType> ConnectionPreferenceTypes { get; set; }
        public IList<FlightSearchCriteria> FlightSearchCriterias { get; set; }
        public IList<FlightSearchRQ> FlightSearchRQs { get; set; }
        public IList<FlightTravelPreference> FlightTravelPreferences { get; set; }
        public IList<PassengerType> PassengerTypes { get; set; }
        public IList<PaxTypeQuantity> PaxTypeQuantitys { get; set; }
        public IList<Requester> Requesters { get; set; }
        public IList<SearchSegment> SearchSegments { get; set; }
        public IList<SortingOrder> SortingOrders { get; set; }
        public IList<TravelDate> TravelDates { get; set; }
        public IList<HeaderConfiguration> Headers { get; set; }
        #endregion

        #region Constructors

        public FlightSearchRQTestData()
        {
            AlternateAirportInformations = new List<AlternateAirportInformation>();
            CabinTypes = new List<CabinType>();
            ConnectionPreferences = new List<ConnectionPreference>();
            ConnectionPreferenceTypes = new List<ConnectionPreferenceType>();
            FlightSearchCriterias = new List<FlightSearchCriteria>();
            FlightSearchRQs = new List<FlightSearchRQ>();
            FlightTravelPreferences = new List<FlightTravelPreference>();
            PassengerTypes = new List<PassengerType>();
            PaxTypeQuantitys = new List<PaxTypeQuantity>();
            Requesters = new List<Requester>();
            SearchSegments = new List<SearchSegment>();
            SortingOrders = new List<SortingOrder>();
            TravelDates = new List<TravelDate>();
            Headers = new List<HeaderConfiguration>();
        }

        #endregion

        #region Public Methods

        public static FlightSearchRQTestData Load(string excelFilePath)
        {
            var data = new FlightSearchRQTestData();

            if (string.IsNullOrEmpty(excelFilePath))
                throw new ArgumentNullException("excelFilePath");

            using (var ds = GetExcelSheetDataSet(excelFilePath))
            {
                DataTable table = ds.Tables["AlternateAirportInformation"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var alternateairportinformationobj = new AlternateAirportInformation
                                                             {
                                                                 Sno = ParseColumnData(row["Sno"]),
                                                                 AirportCodes = ParseColumnData(row["AirportCodes"]),
                                                                 IncludeNearByAirports =
                                                                     ParseColumnData(row["IncludeNearByAirports"]),
                                                                 RadiusKm = ParseColumnData(row["RadiusKm"])
                                                             };

                    data.AlternateAirportInformations.Add(alternateairportinformationobj);
                }
                table = ds.Tables["CabinType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var cabintypeobj = new CabinType
                                           {
                                               Sno = ParseColumnData(row["Sno"]),
                                               cabinType = ParseColumnData(row["CabinType"])
                                           };

                    data.CabinTypes.Add(cabintypeobj);
                }
                table = ds.Tables["ConnectionPreference"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var connectionpreferenceobj = new ConnectionPreference
                                                      {
                                                          Sno = ParseColumnData(row["Sno"]),
                                                          AirportCode = ParseColumnData(row["AirportCode"])
                                                      };

                    var cDataConnectionPreferenceType456156 = ParseColumnData(row["PreferenceType"]);

                    if (string.IsNullOrWhiteSpace(cDataConnectionPreferenceType456156))
                    {
                        connectionpreferenceobj.PreferenceTypes = Enumerable.Empty<ConnectionPreferenceType>();
                    }
                    else
                    {
                        var splitData = cDataConnectionPreferenceType456156.Split(',');
                        connectionpreferenceobj.PreferenceTypes = data.ConnectionPreferenceTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.ConnectionPreferences.Add(connectionpreferenceobj);
                }
                table = ds.Tables["ConnectionPreferenceType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var connectionpreferencetypeobj = new ConnectionPreferenceType
                                                          {
                                                              Sno = ParseColumnData(row["Sno"]),
                                                              connectionPreferenceType =
                                                                  ParseColumnData(row["ConnectionPreferenceType"])
                                                          };

                    data.ConnectionPreferenceTypes.Add(connectionpreferencetypeobj);
                }
                table = ds.Tables["FlightSearchCriteria"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightsearchcriteriaobj = new FlightSearchCriteria
                                                      {
                                                          Sno = ParseColumnData(row["Sno"]),
                                                          AdditionalInfo = ParseColumnData(row["AdditionalInfo"])
                                                      };

                    var cDataFlightTravelPreference336466 = ParseColumnData(row["FlightTravelPreference"]);

                    if (string.IsNullOrWhiteSpace(cDataFlightTravelPreference336466))
                    {
                        flightsearchcriteriaobj.FlightTravelPreferences = Enumerable.Empty<FlightTravelPreference>();
                    }
                    else
                    {
                        var splitData = cDataFlightTravelPreference336466.Split(',');
                        flightsearchcriteriaobj.FlightTravelPreferences = data.FlightTravelPreferences.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    flightsearchcriteriaobj.MaxPreferredResults = ParseColumnData(row["MaxPreferredResults"]);
                    var cDataPaxTypeQuantity175511 = ParseColumnData(row["PassengerInfoSummary"]);

                    if (string.IsNullOrWhiteSpace(cDataPaxTypeQuantity175511))
                    {
                        flightsearchcriteriaobj.PassengerInfoSummarys = Enumerable.Empty<PaxTypeQuantity>();
                    }
                    else
                    {
                        var splitData = cDataPaxTypeQuantity175511.Split(',');
                        flightsearchcriteriaobj.PassengerInfoSummarys = data.PaxTypeQuantitys.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataSearchSegment296066 = ParseColumnData(row["SearchSegments"]);

                    if (string.IsNullOrWhiteSpace(cDataSearchSegment296066))
                    {
                        flightsearchcriteriaobj.SearchSegmentss = Enumerable.Empty<SearchSegment>();
                    }
                    else
                    {
                        var splitData = cDataSearchSegment296066.Split(',');
                        flightsearchcriteriaobj.SearchSegmentss = data.SearchSegments.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataSortingOrder42504 = ParseColumnData(row["SortingOrder"]);

                    if (string.IsNullOrWhiteSpace(cDataSortingOrder42504))
                    {
                        flightsearchcriteriaobj.SortingOrders = Enumerable.Empty<SortingOrder>();
                    }
                    else
                    {
                        var splitData = cDataSortingOrder42504.Split(',');
                        flightsearchcriteriaobj.SortingOrders = data.SortingOrders.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.FlightSearchCriterias.Add(flightsearchcriteriaobj);
                }
                table = ds.Tables["FlightSearchRQ"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flightsearchrqobj = new FlightSearchRQ
                                                {
                                                    Sno = ParseColumnData(row["Sno"]),
                                                    AllowSupplierStreaming = ParseColumnData(row["AllowSupplierStreaming"]),
                                                    DoDeferredSearch = ParseColumnData(row["DoDeferredSearch"])
                                                };

                    var cDataRequester950596 = ParseColumnData(row["Requester"]);

                    if (string.IsNullOrWhiteSpace(cDataRequester950596))
                    {
                        flightsearchrqobj.Requesters = Enumerable.Empty<Requester>();
                    }
                    else
                    {
                        var splitData = cDataRequester950596.Split(',');
                        flightsearchrqobj.Requesters = data.Requesters.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataFlightSearchCriteria563947 = ParseColumnData(row["SearchCriterion"]);

                    if (string.IsNullOrWhiteSpace(cDataFlightSearchCriteria563947))
                    {
                        flightsearchrqobj.SearchCriterions = Enumerable.Empty<FlightSearchCriteria>();
                    }
                    else
                    {
                        var splitData = cDataFlightSearchCriteria563947.Split(',');
                        flightsearchrqobj.SearchCriterions = data.FlightSearchCriterias.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.FlightSearchRQs.Add(flightsearchrqobj);
                }
                table = ds.Tables["FlightTravelPreference"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var flighttravelpreferenceobj = new FlightTravelPreference
                                                        {
                                                            Sno = ParseColumnData(row["Sno"]),
                                                            AllowMixedAirlines = ParseColumnData(row["AllowMixedAirlines"])
                                                        };

                    var cDataCabinType546635 = ParseColumnData(row["CabinClassesPreferred"]);

                    if (string.IsNullOrWhiteSpace(cDataCabinType546635))
                    {
                        flighttravelpreferenceobj.CabinClassesPreferreds = Enumerable.Empty<CabinType>();
                    }
                    else
                    {
                        var splitData = cDataCabinType546635.Split(',');
                        flighttravelpreferenceobj.CabinClassesPreferreds = data.CabinTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    flighttravelpreferenceobj.ExcludeAirlines = ParseColumnData(row["ExcludeAirlines"]);
                    flighttravelpreferenceobj.IncludeAirlines = ParseColumnData(row["IncludeAirlines"]);
                    flighttravelpreferenceobj.JetFlightsOnly = ParseColumnData(row["JetFlightsOnly"]);
                    flighttravelpreferenceobj.Refundable = ParseColumnData(row["Refundable"]);
                    flighttravelpreferenceobj.UnRestrictedFare = ParseColumnData(row["UnRestrictedFare"]);
                    data.FlightTravelPreferences.Add(flighttravelpreferenceobj);
                }
                table = ds.Tables["PassengerType"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var passengertypeobj = new PassengerType
                                               {
                                                   Sno = ParseColumnData(row["Sno"]),
                                                   passengerType = ParseColumnData(row["PassengerType"])
                                               };

                    data.PassengerTypes.Add(passengertypeobj);
                }
                table = ds.Tables["PaxTypeQuantity"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var paxtypequantityobj = new PaxTypeQuantity
                                                 {
                                                     Sno = ParseColumnData(row["Sno"]),
                                                     Ages = ParseColumnData(row["Ages"])
                                                 };

                    var cDataPassengerType574249 = ParseColumnData(row["PassengerType"]);

                    if (string.IsNullOrWhiteSpace(cDataPassengerType574249))
                    {
                        paxtypequantityobj.PassengerTypes = Enumerable.Empty<PassengerType>();
                    }
                    else
                    {
                        var splitData = cDataPassengerType574249.Split(',');
                        paxtypequantityobj.PassengerTypes = data.PassengerTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    paxtypequantityobj.Quantity = ParseColumnData(row["Quantity"]);
                    data.PaxTypeQuantitys.Add(paxtypequantityobj);
                }
                table = ds.Tables["Requester"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var requesterobj = new Requester
                                           {
                                               Sno = ParseColumnData(row["Sno"]),
                                               AdditionalInfo = ParseColumnData(row["AdditionalInfo"]),
                                               RequesterOrigin = ParseColumnData(row["RequesterOrigin"])
                                           };

                    data.Requesters.Add(requesterobj);
                }
                table = ds.Tables["SearchSegment"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var searchsegmentobj = new SearchSegment
                                               {
                                                   Sno = ParseColumnData(row["Sno"]),
                                                   ArrivalAirportCode = ParseColumnData(row["ArrivalAirportCode"]),
                                                   ArrivalCityCode = ParseColumnData(row["ArrivalCityCode"])
                                               };

                    var cDataAlternateAirportInformation565692 = ParseColumnData(row["ArrivalAlternateAirportInformation"]);

                    if (string.IsNullOrWhiteSpace(cDataAlternateAirportInformation565692))
                    {
                        searchsegmentobj.ArrivalAlternateAirportInformations = Enumerable.Empty<AlternateAirportInformation>();
                    }
                    else
                    {
                        var splitData = cDataAlternateAirportInformation565692.Split(',');
                        searchsegmentobj.ArrivalAlternateAirportInformations = data.AlternateAirportInformations.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataCabinType17314 = ParseColumnData(row["Cabin"]);

                    if (string.IsNullOrWhiteSpace(cDataCabinType17314))
                    {
                        searchsegmentobj.Cabins = Enumerable.Empty<CabinType>();
                    }
                    else
                    {
                        var splitData = cDataCabinType17314.Split(',');
                        searchsegmentobj.Cabins = data.CabinTypes.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    var cDataConnectionPreference589609 = ParseColumnData(row["ConnectionPreferences"]);

                    if (string.IsNullOrWhiteSpace(cDataConnectionPreference589609))
                    {
                        searchsegmentobj.ConnectionPreferencess = Enumerable.Empty<ConnectionPreference>();
                    }
                    else
                    {
                        var splitData = cDataConnectionPreference589609.Split(',');
                        searchsegmentobj.ConnectionPreferencess = data.ConnectionPreferences.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    searchsegmentobj.DepartureAirportCode = ParseColumnData(row["DepartureAirportCode"]);
                    searchsegmentobj.DepartureCityCode = ParseColumnData(row["DepartureCityCode"]);
                    var cDataAlternateAirportInformation599904 = ParseColumnData(row["DepartureAlternateAirportInformation"]);

                    if (string.IsNullOrWhiteSpace(cDataAlternateAirportInformation599904))
                    {
                        searchsegmentobj.DepartureAlternateAirportInformations = Enumerable.Empty<AlternateAirportInformation>();
                    }
                    else
                    {
                        var splitData = cDataAlternateAirportInformation599904.Split(',');
                        searchsegmentobj.DepartureAlternateAirportInformations = data.AlternateAirportInformations.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    searchsegmentobj.IncludeDirectOnly = ParseColumnData(row["IncludeDirectOnly"]);
                    searchsegmentobj.IncludeNonStopOnly = ParseColumnData(row["IncludeNonStopOnly"]);
                    searchsegmentobj.IncludeServiceClass = ParseColumnData(row["IncludeServiceClass"]);
                    var cDataTravelDate108826 = ParseColumnData(row["TravelDate"]);

                    if (string.IsNullOrWhiteSpace(cDataTravelDate108826))
                    {
                        searchsegmentobj.TravelDates = Enumerable.Empty<TravelDate>();
                    }
                    else
                    {
                        var splitData = cDataTravelDate108826.Split(',');
                        searchsegmentobj.TravelDates = data.TravelDates.Where(x => splitData.Any(y => y.Equals(x.Sno)));
                    }
                    data.SearchSegments.Add(searchsegmentobj);
                }
                table = ds.Tables["SortingOrder"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var sortingorderobj = new SortingOrder
                                              {
                                                  Sno = ParseColumnData(row["Sno"]),
                                                  sortingOrder = ParseColumnData(row["SortingOrder"])
                                              };

                    data.SortingOrders.Add(sortingorderobj);
                }
                table = ds.Tables["TravelDate"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var traveldateobj = new TravelDate
                                            {
                                                Sno = ParseColumnData(row["Sno"]),
                                                AnyTime = ParseColumnData(row["AnyTime"]),
                                                DateTime = ParseColumnData(row["DateTime"]),
                                                MaxDate = ParseColumnData(row["MaxDate"]),
                                                MinDate = ParseColumnData(row["MinDate"]),
                                                TimeWindow = ParseColumnData(row["TimeWindow"])
                                            };

                    data.TravelDates.Add(traveldateobj);
                }

                table = ds.Tables["HeaderConfiguration"];

                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    var requesterobj = new HeaderConfiguration
                                           {Key = ParseColumnData(row["Key"]), Value = ParseColumnData(row["Value"])};

                    data.Headers.Add(requesterobj);
                }
            }
            return data;
        }

        #endregion


        #region Private Methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private static DataSet GetExcelSheetDataSet(string excelSheetPath)
        {
            using (var stream = File.Open(excelSheetPath, FileMode.Open, FileAccess.Read))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    excelReader.IsFirstRowAsColumnNames = true;

                    var testSheet = excelReader.AsDataSet();

                    return testSheet;
                }
            }
        }

        private static string ParseColumnData(object data)
        {
            if (data == null)
                return null;

            return data.ToString();
        }

        #endregion
    }
}
