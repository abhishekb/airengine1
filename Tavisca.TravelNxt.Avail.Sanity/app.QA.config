﻿<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <section name="microsoft.visualstudio.testtools" type="Microsoft.VisualStudio.TestTools.UnitTesting.TestConfigurationSection, Microsoft.VisualStudio.QualityTools.UnitTestFramework, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"/>
    <section name="tavisca.singularity.configuration" type="Tavisca.Singularity.Configuration.SingularityConfigurationSection, Tavisca.Singularity.Core"/>
  </configSections>
  <tavisca.singularity.configuration initializer="default" host="wcf.windows" interceptionProvider="InterfaceInterceptor"/>
  <appSettings>
    <add key="serverIpFile" value="" />
    <add key="baseAirEngineAddress" value="http://oskinxtqa.tavisca.com/AirEngine/AirEngine.svc" />
    <add key="baseAirFareRulesAddress" value="http://oskinxtqa.tavisca.com/AirEngine/AirFareRules.svc" />
    <add key="baseAirSeatMapAddress" value="http://oskinxtqa.tavisca.com/AirEngine/AirSeatMap.svc" />
    <add key="TimeOutLimitInMilliSeconds" value="60000"/>
    <add key="MockCulture" value="en-US" />
    <add key="MockGetAccountId" value="123456" />
    <add key="MockPosId" value="1" />
    <add key="DisplayCurrency" value="INR"/>
    <!--<add key="BaseCurrency" value="USD" />-->
    <add key="Password" value="password" />
    <add key="EnableFareRulesTest" value="true"/>
    <add key="EnableSeatMapTest" value="true"/>
    <add key="FareRulesTryCount" value="3"/>
    <add key="SeatMapTryCount" value="5"/>
  </appSettings>
  <microsoft.visualstudio.testtools>
    <dataSources>
      <add name="AirSearchRequestData" connectionString="LoadScenarioMapping" dataTableName="Scenario$" dataAccessMethod="Sequential"/>
    </dataSources>
  </microsoft.visualstudio.testtools>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="BasicHttpBinding_IAirEngine" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647"/>
        <binding name="BasicHttpBinding_IFareRules" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="BasicHttpBinding_ISeatMap" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
      </basicHttpBinding>

      <webHttpBinding>
        <binding name="webHttpBinding_IAirEngine" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647"/>
        <binding name="WebHttpBinding_IAirFareRules" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
        <binding name="WebHttpBinding_IAirSeatMap" closeTimeout="00:10:00" openTimeout="00:10:00" sendTimeout="00:10:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" />
      </webHttpBinding>

      <protoBufBinding>
        <binding name="ProtoBufBinding_IAirEngine" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647"  />
        <binding name="ProtoBufBinding_IFareRules" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647"  />
        <binding name="ProtoBufBinding_ISeatMap" closeTimeout="00:59:00" openTimeout="00:59:00" sendTimeout="00:59:00" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647"  />
      </protoBufBinding>

    </bindings>
    <client>
      <endpoint binding="webHttpBinding" bindingConfiguration="webHttpBinding_IAirEngine" contract="AirEngineClient.IAirEngine" name="webHttpBinding_IAirEngine" behaviorConfiguration="flightClient" />
      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IAirEngine" contract="AirEngineClient.IAirEngine" name="BasicHttpBinding_IAirEngine" behaviorConfiguration="flightClient" />
      <endpoint binding="protoBufBinding"  contract="AirEngineClient.IAirEngine" name="ProtoBufBinding_IAirEngine" behaviorConfiguration="protoBinding" bindingConfiguration="ProtoBufBinding_IAirEngine" />

      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IFareRules" contract="AirFareRulesClient.IAirFareRules" name="BasicHttpBinding_IFareRules" behaviorConfiguration="flightClient" />
      <endpoint binding="protoBufBinding" bindingConfiguration="ProtoBufBinding_IFareRules" contract="AirFareRulesClient.IAirFareRules" name="ProtoBufBinding_IFareRules" behaviorConfiguration="protoBinding" />
      <endpoint binding="webHttpBinding" bindingConfiguration="WebHttpBinding_IAirFareRules" contract="AirFareRulesClient.IAirFareRules" name="webHttpBinding_IAirFareRules" behaviorConfiguration="flightClient" />

      <endpoint binding="webHttpBinding" bindingConfiguration="WebHttpBinding_IAirSeatMap" contract="AirSeatMapClient.IAirSeatMap" name="webHttpBinding_IAirSeatMap" behaviorConfiguration="flightClient" />
      <endpoint binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_ISeatMap" contract="AirSeatMapClient.IAirSeatMap" name="BasicHttpBinding_ISeatMap" behaviorConfiguration="flightClient" />
      <endpoint binding="protoBufBinding" bindingConfiguration="ProtoBufBinding_ISeatMap" contract="AirSeatMapClient.IAirSeatMap" name="ProtoBufBinding_ISeatMap" behaviorConfiguration="protoBinding" />

    </client>
    <behaviors>
      <endpointBehaviors>
        <behavior name="flightClient">
          <flightEndpointBehaviourExtension />
        </behavior>
        <behavior name="protoBinding">
          <protoBufFormatterBehaviour />
          <flightEndpointBehaviourExtension />
        </behavior>
      </endpointBehaviors>
    </behaviors>
    <extensions>
      <bindingExtensions>
        <add name="protoBufBinding"
           type="ProtoBuf.Wcf.Channels.Bindings.ProtoBufBindingCollectionElement, ProtoBuf.Wcf.Channels, Version=1.0.0.0, Culture=neutral" />
      </bindingExtensions>
      <behaviorExtensions>
        <add name="protoBufFormatterBehaviour" type="ProtoBuf.Wcf.Channels.Bindings.ProtoBufBindingEndpointBehaviour, ProtoBuf.Wcf.Channels" />
        <add name="flightBehaviourExtension" type="Tavisca.TravelNxt.Common.Service.Inspectors.Header.ServiceBehaviour, Tavisca.TravelNxt.Common.Service.Inspectors" />
        <add name="flightEndpointBehaviourExtension" type="Tavisca.TravelNxt.Common.Service.Inspectors.Header.EndpointBehaviour, Tavisca.TravelNxt.Common.Service.Inspectors" />
      </behaviorExtensions>
    </extensions>
  </system.serviceModel>
  <connectionStrings>
    <add name="LoadScenarioMapping" connectionString="Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=TestData\ScenarioMapping.xls;DefaultDir=|DataDirectory|" providerName="System.Data.Odbc"/>
  </connectionStrings>
  <system.net>
    <connectionManagement>
      <add address="*" maxconnection="999"/>
    </connectionManagement>
  </system.net>
</configuration>