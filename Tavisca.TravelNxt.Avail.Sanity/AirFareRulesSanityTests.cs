﻿using System.IO;
using Tavisca.TravelNxt.Avail.Sanity.AirEngineClient;
using System;
using System.Configuration;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Avail.Sanity.AirFareRulesClient;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Sanity
{

    public class AirFareRulesSanityTests
    {
        public AirFareRulesSanityTests()
        {
            SetFareAirRulesUri();
        }
        private static void SetFareAirRulesUri()
        {
            const string serverIpFileKey = "serverIpFile";
            var ipFile = ConfigurationManager.AppSettings[serverIpFileKey];

            if (string.IsNullOrEmpty(ipFile))
                return;

            if (!File.Exists(ipFile))
                throw new InternalTestFailureException(string.Format("The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' is not valid, either empty it or the file should exist.", serverIpFileKey, ipFile));

            var text = File.ReadAllText(ipFile);

            var invalidFileTextExceptionMessage =
                string.Format(
                    "The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' contains an empty or invalid text.",
                    serverIpFileKey, ipFile);

            if (string.IsNullOrEmpty(text))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var parameters = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (parameters.Length == 0)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var serverIpLine = parameters[0].Split(':');

            if (serverIpLine.Length < 2)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var ip = serverIpLine[1].Trim();

            if (string.IsNullOrWhiteSpace(ip))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var address = GetBaseAirFareRulesAddress();

            SetBaseAirFareRulesAddress(address.Replace("##domain##", ip));
        }

        private static void SetBaseAirFareRulesAddress(string address)
        {
            _airFareRulesBaseAddress = address;
        }

        private static string _airFareRulesBaseAddress;
        private static string GetBaseAirFareRulesAddress()
        {
            return _airFareRulesBaseAddress ?? (_airFareRulesBaseAddress = ConfigurationManager.AppSettings["baseAirFareRulesAddress"]);
        }

/*
        private static void SetBaseAirFareRulesAddress(string value)
        {
            _airFareRulesBaseAddress = value;
        }
*/

        public bool TestAirFareRulesWithProtoAndHttpBinding(FlightSearchRS response, string binding, out string errorMessage)
        {
            FareRulesRS fareRuleResponse = null;
            for (int tryCount = Int16.Parse(ConfigurationManager.AppSettings["FareRulesTryCount"]); tryCount > 0; tryCount--)
            {
                FareRulesRQ fareRuleRequest = GetFareRulesRequest(response);
                fareRuleResponse = CallAirFareRulesService(fareRuleRequest, response.SessionID.ToString(), binding);
                if (fareRuleResponse.FareRules != null && fareRuleResponse.FareRules.Length > 0)
                    break;
            }
            return ValidateFareRulesResponse(fareRuleResponse, out errorMessage);
        }

        public bool TestAirFareRulesWithWebHttpBinding(FlightSearchRS response, string contentType, out string errorMessage)
        {
            SetFareAirRulesUri();
            FareRulesRS fareRuleResponse = null;
            for (int tryCount = Int16.Parse(ConfigurationManager.AppSettings["FareRulesTryCount"]); tryCount > 0; tryCount--)
            {
                var fareRuleRequest = GetFareRulesRequest(response);
                var endPoint = GetBaseAirFareRulesAddress() + "/rest";
                fareRuleResponse = CallAirFareRulesWebHttpBinding(fareRuleRequest, response.SessionID.ToString(), endPoint, contentType);
                if (fareRuleResponse.FareRules != null && fareRuleResponse.FareRules.Length > 0)
                    break;
            }
            return ValidateFareRulesResponse(fareRuleResponse, out errorMessage);
        }

        private FareRulesRQ GetFareRulesRequest(FlightSearchRS response)
        {
            FareRulesRQ fareRuleRequest;

            if (string.IsNullOrEmpty(response.SessionID.ToString()))
                throw new InternalTestFailureException("Null or Empty SessionId found in one of the AirSearch Response made....");

            if ((response.LegRecommendations != null && response.LegRecommendations.Count > 0) || (response.ItineraryRecommendations != null && response.ItineraryRecommendations.Count > 0))
            {
                fareRuleRequest = new FareRulesRQ {RecommendationRefID = GetRecommendationRefId(response)};
                Console.WriteLine("RecommendationRefID for FareRules Request: " + fareRuleRequest.RecommendationRefID);

            }
            else
                throw new InternalTestFailureException("Could not start AirFareRules Test because 0 ItineraryRecommendations or LegRecommendations found in the AirSearch request made...");
            return fareRuleRequest;
        }

        private static int GetRecommendationRefId(FlightSearchRS searchResponse)
        {
            if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                return searchResponse.LegRecommendations[new Random().Next(0, searchResponse.LegRecommendations.Count)].RefID;

            return searchResponse.ItineraryRecommendations[new Random().Next(0, searchResponse.ItineraryRecommendations.Count)].RefID;
        }

        private static FareRulesRS CallAirFareRulesWebHttpBinding(FareRulesRQ request, string sessionId, string endPoint, string contentType = "text/json")
        {
            FareRulesRS response;
            using (var client = AirSearchSanityTests.GetWebClient(contentType, sessionId))
            {
                var serializedRequest = AirSearchSanityTests.GetSerializedRequest(request, contentType);
                var bytesResponse = client.UploadData(endPoint + "/getfarerules",
                    Encoding.UTF8.GetBytes(serializedRequest));
                response = GetFareRulesResponseFromBytes(bytesResponse, contentType);
            }
            return response;
        }


/*
        private static void PrintFareRulesResponse(FareRulesRS response)
        {
            Console.WriteLine("\n\n***** Fare Rules Response *****");
            Console.WriteLine("Fare Tules Count: " + ((response.FareRules != null) ? response.FareRules.Length.ToString() : "null"));
            Console.WriteLine("***** Fare Rules Response *****\n\n");
        }
*/

        private static FareRulesRS GetFareRulesResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? AirSearchSanityTests.Deserialize<FareRulesRS>(byteResponse) : AirSearchSanityTests.XmlDeserialize<FareRulesRS>(byteResponse);
            return response;
        }

        private static FareRulesRS CallAirFareRulesService(FareRulesRQ searchRequest, string sessionId, string addressSuffix)
        {
            SetFareAirRulesUri();
            string endPointName = addressSuffix.ToLower().Equals("") ? "BasicHttpBinding_IFareRules" : "ProtoBufBinding_IFareRules";

            FareRulesRS response;
            var address = GetBaseAirFareRulesAddress() + addressSuffix;
            Console.WriteLine(" Calling Service End Point {0} for Scenario", address);

            using (var client = new AirFareRulesClient.AirFareRulesClient(endPointName, address))
            {
                using (new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                                                               sessionId, GetTestSetting("MockPosId"), GetTestSetting("MockGetAccountId"),
                                                               GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                {
                    response = client.GetFareRules(searchRequest);
                }
            }
            return response;
        }

        private bool ValidateFareRulesResponse(FareRulesRS fareRulesResponse, out string errorMessage)
        {
            var errorMeassages = new StringBuilder();
            bool isSuccesfull = true;

            if (fareRulesResponse == null)
            {
                errorMeassages.AppendLine("Fare rule response is null...");
                isSuccesfull = false;
            }
            else if (fareRulesResponse.FareRules == null)
            {
                errorMeassages.AppendLine("Fare rules are null...");
                isSuccesfull = false;
            }
            else if (fareRulesResponse.ServiceStatus == null)
            {
                errorMeassages.AppendLine("Service status is null...");
                isSuccesfull = false;
            }

            if (fareRulesResponse != null && (fareRulesResponse.ServiceStatus != null && (string.Equals(AirFareRulesClient.CallStatus.Failure, fareRulesResponse.ServiceStatus.Status))))
            {
                if (!string.IsNullOrEmpty(fareRulesResponse.ServiceStatus.StatusMessage))
                    errorMeassages.AppendLine(fareRulesResponse.ServiceStatus.StatusMessage);
                if (fareRulesResponse.ServiceStatus.Messages.Length > 0)
                {
                    foreach (string message in fareRulesResponse.ServiceStatus.Messages)
                        errorMeassages.AppendLine(message);
                }
            }
            //Assert.AreEqual(Tavisca.TravelNxt.Avail.Sanity.AirFareRulesClient.CallStatus.Success, fareRulesResponse.ServiceStatus.Status,
            //    errorMeassages.ToString());

            if (fareRulesResponse != null && (fareRulesResponse.FareRules != null && fareRulesResponse.FareRules.Length == 0))
                isSuccesfull = false;


            errorMessage = errorMeassages.ToString();
            return isSuccesfull;
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

    }
}
