﻿//using System.Data;
//using System.Diagnostics;
//using System.Threading;
//using SchemaGenerator;
//using Tavisca.TravelNxt.Avail.Sanity.AirEngineClient;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Json;
//using System.ServiceModel;
//using System.Text;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//using Tavisca.Frameworks.Parallel.Ambience;

//using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
//using Tavisca.TravelNxt.Common.Extensions;

//namespace Tavisca.TravelNxt.Avail.Sanity
//{
//    [TestClass]
//    public class AirMetaSearchSanityTests
//    {
//        private static IList<FlightSearchRQ> _scenarioDataFromExcel;
//        public TestContext TestContext
//        {
//            get;
//            set;
//        }

//        [ClassInitialize]
//        public static void InitilizeScenarioData(TestContext context)
//        {
//            var loadExcelData = FlightSearchRQTestData.Load(@"TestData\FlightSearchRQTestData.xlsx");
//            _scenarioDataFromExcel = loadExcelData.FlightSearchRQs;
//            SetAirEngineUri();
//        }
        
//        [TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
//        public void TestAirSearchWithBasicHttpBinding()
//        {
//            var getRequest = CreateRequest(TestContext, _scenarioDataFromExcel);

//            if (getRequest.Any())
//            {
//                CallAirSearchService(getRequest.FirstOrDefault().Key, getRequest.FirstOrDefault().Value, "BasicHttpBinding_IAirEngine", "");

//            }
//        }

//        private static void SetAirEngineUri()
//        {
//            const string serverIpFileKey = "serverIpFile";
//            var ipFile = ConfigurationManager.AppSettings[serverIpFileKey];

//            if (string.IsNullOrEmpty(ipFile))
//                return;

//            if (!File.Exists(ipFile))
//                throw new InternalTestFailureException(string.Format("The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' is not valid, either empty it or the file should exist.", serverIpFileKey, ipFile));

//            var text = File.ReadAllText(ipFile);

//            var invalidFileTextExceptionMessage =
//                string.Format(
//                    "The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' contains an empty or invalid text.",
//                    serverIpFileKey, ipFile);

//            if (string.IsNullOrEmpty(text))
//                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

//            var parameters = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

//            if (parameters.Length == 0)
//                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

//            var serverIpLine = parameters[0].Split(':');

//            if (serverIpLine.Length < 2)
//                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

//            var ip = serverIpLine[1].Trim();

//            if (string.IsNullOrWhiteSpace(ip))
//                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

//            var address = GetBaseAirEngineAddress();

//            SetBaseAirEngineAddress(address.Replace("##domain##", ip));
//        }

//        private static string _airEngineBaseAddress;
//        private static string GetBaseAirEngineAddress()
//        {
//            //return "http://localhost:63174/AirEngine.svc";
//            return _airEngineBaseAddress ?? (_airEngineBaseAddress = ConfigurationManager.AppSettings["baseAirEngineAddress"]);
//        }


//        private static void SetBaseAirEngineAddress(string value)
//        {
//            _airEngineBaseAddress = value;
//        }


//        public Dictionary<string, AirAvailRq> CreateRequest(TestContext context, IList<FlightSearchRQ> scenarioDataFromExcel, bool isdeferredSearch = false, bool isSupplierStreaming = false)
//        {
//            var response = new Dictionary<string, AirAvailRq>();
//            DataRow dataRow = context.DataRow;

//            var isScenarioEnable = bool.Parse(dataRow["IsEnable"].ToString());

//            var scenarioName = dataRow["ScenarioName"].ToString();
//            if (!isScenarioEnable)
//            {
//                Console.WriteLine(" **** Scenario is Disabled ***** {0}", scenarioName);
//                return response;
//            }

//            var sno = dataRow["Sno"].ToString();
//            var scenarioData = scenarioDataFromExcel.FirstOrDefault(x => x.Sno == sno);

//            Console.WriteLine(" **** Scenario Started ***** {0}", scenarioName);

//            if (scenarioData == null)
//            {
//                throw new InternalTestFailureException(string.Format("Data not found for scenarion {0}", scenarioName));
//            }

//            var excelAutoMapper = new ExcelAutoMapper();

//            AirEngineClient.FlightSearchRQ clientRequest = excelAutoMapper.CopyObject(scenarioData, new AirEngineClient.FlightSearchRQ());

//            for (int searcSegmentNumber = 0;
//                searcSegmentNumber < clientRequest.SearchCriterion.SearchSegments.Count;
//                searcSegmentNumber++)
//            {
//                clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber].TravelDate.DateTime = searcSegmentNumber == 0 ? DateTime.Now.AddDays(120)
//                        : clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber - 1].TravelDate.DateTime.AddDays(150);
//            }

//            AirAvailRq airAvailRq = CreateAirAvailRequest(clientRequest);

//            response.Add(scenarioName, airAvailRq);

//            return response;
//        }


//        private static AirAvailRq CreateAirAvailRequest(AirEngineClient.FlightSearchRQ flightSearchRequest)
//        {
//            AirAvailRq airAvailRq = new AirAvailRq
//            {
//                Uid = "4",
//                IpAddress = "127.0.0.1"
//            };                

//            foreach (var passenger in flightSearchRequest.SearchCriterion.PassengerInfoSummary)
//            {
//                if (passenger.Quantity > 0)
//                {
//                    if (passenger.PassengerType == AirEngineClient.PassengerType.Adult)
//                        airAvailRq.Adults = passenger.Quantity;
                   
//                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Child)
                        
//                        airAvailRq.Children = passenger.Quantity;

//                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Infant)                        
//                        airAvailRq.Infants = passenger.Quantity;

//                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Senior)                        
//                        airAvailRq.Seniors = passenger.Quantity;
//                }
//            }
            
//            airAvailRq.SearchSegments = new List<SearchSegment1>();

//            foreach (var segment in flightSearchRequest.SearchCriterion.SearchSegments)
//            {
//                SearchSegment1 segment1 = new SearchSegment1();                

//                if (!string.IsNullOrEmpty(segment.DepartureAirportCode))                    
//                    segment1.OriginCode = segment.DepartureAirportCode;
                    
//                else
//                    segment1.OriginCode = segment.DepartureCityCode;               

//                if (!string.IsNullOrEmpty(segment.ArrivalAirportCode))
//                    segment1.DestCode = segment.ArrivalAirportCode;
               
//                else
//                    segment1.DestCode = segment.ArrivalCityCode;
            
//                segment1.DepartDate = segment.TravelDate.DateTime;

//                airAvailRq.SearchSegments.Add(segment1);
//            }
            
//            if (flightSearchRequest.SearchCriterion.SearchSegments.Count == 1)                
//                airAvailRq.TripType = TripType.OneWay;
//            else if (flightSearchRequest.SearchCriterion.SearchSegments.Count == 2)
//                airAvailRq.TripType = TripType.RoundTrip;
//            else
//                airAvailRq.TripType = TripType.MultiStop;

//            return airAvailRq;
//        }
  
//        private static void CallAirSearchService(string scenarioName, AirAvailRq searchRequest, string endPointName,string addressSuffix)
//        {
//            AirAvailRs response = null;
         
//            var address = GetBaseAirEngineAddress() + addressSuffix;

//            using (var client = new AirEngineClient.AirEngineClient(endPointName, address))
//            {
//                Console.WriteLine(" Calling Service End Point {0} for Scenario {1}", client.Endpoint.Address,scenarioName);                

//                using (new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
//                   Guid.NewGuid().ToString(), GetTestSetting("MockPosId"), GetTestSetting("MockGetAccountId"),
//                   GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
//                {
//                    //using (new OperationContextScope(client.InnerChannel))
//                    //{
//                        //MessageHeader<string> header = new MessageHeader<string>("secret message");
//                        //OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("uid", "4"));
//                        //OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("ip", "127.0.0.1"));

//                        response = client.Avail(searchRequest);
//                    //}                    
//                }
//            }
//            string resultMessage = "Success";

//            bool isSuccessfull = ValidateAirMetaSearchResponse(response, out resultMessage);

//            Assert.IsTrue(isSuccessfull, resultMessage);

//        }

       
//        private static string GetTestSetting(string name)
//        {
//            return ConfigurationManager.AppSettings[name];
//        }       
        

//        private static bool ValidateAirMetaSearchResponse(AirAvailRs res, out string result)
//        {
//            var message = new StringBuilder();
//            bool isSuccesfull;

//            if (res == null)
//            {
//                message.AppendLine("Response is NULL");
//                isSuccesfull = false;
//            }

//            else if (res.Error != null)
//            {
//                message.AppendLine(string.Format("Failure Error code : {0} Message : {1}", res.Error.ErrorCode, res.Error.ErrorMessage));
//                isSuccesfull = false;
//            }
//            else if (res.AirOptions == null || res.Segments == null)
//            {
//                message.AppendLine("AirOptions and Segments are NULL");
//                isSuccesfull = false;
//            }
//            else if ((res.AirOptions != null && res.AirOptions.Count == 0) || (res.Segments != null && res.Segments.Count == 0))
//            {
//                message.AppendLine("No Results Found");
//                isSuccesfull = false;
//            }
//            else
//                isSuccesfull = true;

//            result = message.ToString();

//            return isSuccesfull;
//        }
//    }
//}
