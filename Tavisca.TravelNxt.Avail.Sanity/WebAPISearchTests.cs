﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchemaGenerator;
using Tavisca.TravelNxt.Avail.Sanity.AirEngineClient;
//using System.Net.Http.Formatting;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Runtime.Serialization;
using Tavisca.TravelNxt.Engines.CAPI.Services.Tests.Helpers;

namespace Tavisca.TravelNxt.Avail.Sanity
{
     [TestClass]
    public class WebAPISearchTests
    {
        private static IList<FlightSearchRQ> _scenarioDataFromExcel;

        public TestContext TestContext
        {
            get;
            set;
        }

        [ClassInitialize]
        public static void InitilizeScenarioData(TestContext context)
        {
            var loadExcelData = FlightSearchRQTestData.Load(@"TestData\FlightSearchRQTestData.xlsx");
            _scenarioDataFromExcel = loadExcelData.FlightSearchRQs;
            SetAirEngineUri();
           
        }

        //[TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
        //public void TestWebApiSearch()
        //{
        //   var getRequest = CreateRequestString(TestContext, _scenarioDataFromExcel);
        //    var endPoint = GetBaseAirEngineAddress() + "/webapi";

        //    if (getRequest.Any())
        //    {
        //        CallWebHttpBinding(getRequest, endPoint);
        //    }
        //}

        private static void CallWebHttpBinding(Dictionary<string, AirEngineClient.FlightSearchRQ> request, string endPoint)
        {
            string uri = CreateWebApiSearchRequestString(request, endPoint);
            AirAvailRs webAPIRS=null;
            string resultMessage = string.Empty;
            try
            {
                HttpClient _httpClient = _httpClient = new HttpClient(uri, ContentType.Json, null);

                webAPIRS = _httpClient.Get<AirAvailRs>();

                //HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(uri);
                //var response = webReq.GetResponse();

                //if (response != null)
                //{
                //    Stream stream = response.GetResponseStream();
                //    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(AirAvailRs));
                //    if (stream != null)
                //    {
                //        webAPIRS = (AirAvailRs)ser.ReadObject(stream);
                //        stream.Close();
                //    }
                //}
                //response.Close();

                bool isSuccessfull = ValidateWebAPISearchResponse(webAPIRS, out resultMessage);
                Assert.IsTrue(isSuccessfull, resultMessage);
            }
            finally
            {
                //if (webAPIRS != null)
                //    Console.WriteLine(" **** Session Id : {0} ****", webAPIRS.CacheId.ToString());
                //Console.WriteLine(" Result {0}", resultMessage);
                //Console.WriteLine(" **** Scenario Completed ***** {0}", request.FirstOrDefault().Key);
            }
        }

        private static void SetAirEngineUri()
        {
            const string serverIpFileKey = "serverIpFile";
            var ipFile = ConfigurationManager.AppSettings[serverIpFileKey];

            if (string.IsNullOrEmpty(ipFile))
                return;

            if (!File.Exists(ipFile))
                throw new InternalTestFailureException(string.Format("The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' is not valid, either empty it or the file should exist.", serverIpFileKey, ipFile));

            var text = File.ReadAllText(ipFile);

            var invalidFileTextExceptionMessage =
                string.Format(
                    "The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' contains an empty or invalid text.",
                    serverIpFileKey, ipFile);

            if (string.IsNullOrEmpty(text))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var parameters = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (parameters.Length == 0)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var serverIpLine = parameters[0].Split(':');

            if (serverIpLine.Length < 2)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var ip = serverIpLine[1].Trim();

            if (string.IsNullOrWhiteSpace(ip))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var address = GetBaseAirEngineAddress();

            SetBaseAirEngineAddress(address.Replace("##domain##", ip));
        }

        private static void SetBaseAirEngineAddress(string value)
        {
            _airEngineBaseAddress = value;
        }
        private static string _airEngineBaseAddress;
        private static string GetBaseAirEngineAddress()
        {
           // return "http://localhost:63174/AirEngine.svc";
           return _airEngineBaseAddress ?? (_airEngineBaseAddress = ConfigurationManager.AppSettings["baseAirEngineAddress"]);
        }

        private Dictionary<string, AirEngineClient.FlightSearchRQ> CreateRequestString(TestContext context, IList<FlightSearchRQ> scenarioDataFromExcel)
        {
            var response = new Dictionary<string, AirEngineClient.FlightSearchRQ>();
            DataRow dataRow = context.DataRow;

            var isScenarioEnable = bool.Parse(dataRow["IsEnable"].ToString());

            var scenarioName = dataRow["ScenarioName"].ToString();
            if (!isScenarioEnable)
            {
                Console.WriteLine(" **** Scenario is Disabled ***** {0}", scenarioName);
                return response;
            }

            var sno = dataRow["Sno"].ToString();
            var scenarioData = scenarioDataFromExcel.FirstOrDefault(x => x.Sno == sno);

            Console.WriteLine(" **** Scenario Started ***** {0}", scenarioName);

            if (scenarioData == null)
            {
                throw new InternalTestFailureException(string.Format("Data not found for scenarion {0}", scenarioName));
            }

            var excelAutoMapper = new ExcelAutoMapper();
            var clientRequest = excelAutoMapper.CopyObject(scenarioData, new AirEngineClient.FlightSearchRQ());
            
            for (int searcSegmentNumber = 0; searcSegmentNumber < clientRequest.SearchCriterion.SearchSegments.Count; searcSegmentNumber++)
            {
                clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber].TravelDate.DateTime = searcSegmentNumber == 0 ? DateTime.Now.AddDays(new Random(Guid.NewGuid().GetHashCode()).Next(4, 70))
                    : clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber - 1].TravelDate.DateTime.AddDays(new Random(Guid.NewGuid().GetHashCode()).Next(6, 20));
            }
            response.Add(scenarioName, clientRequest);
            return response;
        }      

        private static string CreateWebApiSearchRequestString(Dictionary<string, AirEngineClient.FlightSearchRQ> request, string endPoint)
        {
            var flightSearchRequest = request.FirstOrDefault().Value;
            string uriTemplate = string.Format("{0}/avail?uid=4&ip=127.0.0.1", endPoint);

            foreach (var passenger in flightSearchRequest.SearchCriterion.PassengerInfoSummary)
            {
                if (passenger.Quantity > 0)
                {
                    if (passenger.PassengerType == AirEngineClient.PassengerType.Adult)
                        uriTemplate = string.Format("{0}&adt={1}", uriTemplate, passenger.Quantity);

                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Child)
                        uriTemplate = string.Format("{0}&chld={1}", uriTemplate, passenger.Quantity);

                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Infant)
                        uriTemplate = string.Format("{0}&inf={1}", uriTemplate, passenger.Quantity);

                    else if (passenger.PassengerType == AirEngineClient.PassengerType.Senior)
                        uriTemplate = string.Format("{0}&senr={1}", uriTemplate, passenger.Quantity);
                }
            }
            int i = 1;
            foreach (var segment in flightSearchRequest.SearchCriterion.SearchSegments)
            {               
                string paramName = string.Format("frm{0}", i);
                if (!string.IsNullOrEmpty(segment.DepartureAirportCode))
                    uriTemplate = string.Format("{0}&{1}={2}", uriTemplate, paramName, segment.DepartureAirportCode);
                else
                    uriTemplate = string.Format("{0}&{1}={2}", uriTemplate, paramName, segment.DepartureCityCode);

                string paramName2 = string.Format("to{0}", i);

                if (!string.IsNullOrEmpty(segment.ArrivalAirportCode))
                    uriTemplate = string.Format("{0}&{1}={2}", uriTemplate, paramName2, segment.ArrivalAirportCode);
                else
                    uriTemplate = string.Format("{0}&{1}={2}", uriTemplate, paramName2, segment.ArrivalCityCode);

                string paramName3 = string.Format("date{0}", i);
                uriTemplate = string.Format("{0}&{1}={2}", uriTemplate, paramName3, segment.TravelDate.DateTime);

                i++;

            }

            string tripType = "";

            if (flightSearchRequest.SearchCriterion.SearchSegments.Count == 1)
                tripType = "OW";
            else if (flightSearchRequest.SearchCriterion.SearchSegments.Count == 2)
                tripType = "RT";
            else
                tripType = "MC";

            uriTemplate = string.Format("{0}&trip={1}", uriTemplate, tripType);
            return uriTemplate;
        }

        private static bool ValidateWebAPISearchResponse(AirAvailRs res, out string result)
        {
            var message = new StringBuilder();
            bool isSuccesfull;

            if (res == null)
            {
                message.AppendLine("Response is NULL");
                isSuccesfull = false;
            }

            else if (res.Error != null)
            {
                message.AppendLine(string.Format("Failure Error code : {0} Message : {1}", res.Error.ErrorCode, res.Error.ErrorMessage));
                isSuccesfull = false;
            }
            else if (res.AirOptions == null || res.Segments == null)
            {
                message.AppendLine("AirOptions and Segments are NULL");
                isSuccesfull = false;
            }
            else if ((res.AirOptions != null && res.AirOptions.Count == 0) || (res.Segments != null && res.Segments.Count == 0))
            {
                message.AppendLine("No Results Found");
                isSuccesfull = false;
            }
            else
                isSuccesfull = true;

            result = message.ToString();

            return isSuccesfull;
        }
    }
}
