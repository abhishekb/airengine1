﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Tavisca.TravelNxt.Engines.CAPI.Services.Tests.Helpers
{
    public static class SerialisationProvider
    {
        private static readonly Dictionary<Type, DataContractSerializer> SerializerCache =
            new Dictionary<Type, DataContractSerializer>();

        private static readonly Dictionary<Type, DataContractJsonSerializer> JsonSerializerCache =
            new Dictionary<Type, DataContractJsonSerializer>();

        private static readonly ReaderWriterLockSlim Locker = new ReaderWriterLockSlim();

        private static DataContractSerializer GetSerializer(Type type)
        {
            DataContractSerializer match;
            Locker.EnterUpgradeableReadLock();
            try
            {
                if (!SerializerCache.TryGetValue(type, out match))
                {
                    Locker.EnterWriteLock();
                    try
                    {
                        if (!SerializerCache.TryGetValue(type, out match))
                        {
                            match = new DataContractSerializer(type);
                            SerializerCache[type] = match;
                        }
                    }
                    finally
                    {
                        Locker.ExitWriteLock();
                    }
                }
            }
            finally
            {
                Locker.ExitUpgradeableReadLock();
            }
            return match;
        }

        public static DataContractJsonSerializer GetJsonSerializer(Type type)
        {
            DataContractJsonSerializer match;
            Locker.EnterUpgradeableReadLock();
            try
            {
                if (!JsonSerializerCache.TryGetValue(type, out match))
                {
                    Locker.EnterWriteLock();
                    try
                    {
                        if (!JsonSerializerCache.TryGetValue(type, out match))
                        {
                            match = new DataContractJsonSerializer(type, null, int.MaxValue, true, null, false);
                            JsonSerializerCache[type] = match;

                            //match = new DataContractJsonSerializer(type, new DataContractJsonSerializerSettings
                            //{
                            //    DateTimeFormat = new DateTimeFormat("yyyy-MM-dd'T'HH:mm:ssZ"),
                            //    //EmitTypeInformation = EmitTypeInformation.Never,
                            //    DataContractSurrogate = null,
                            //    KnownTypes = new List<Type> {typeof (int)}.ToArray(),
                            //    RootName = null,
                            //    IgnoreExtensionDataObject = true,
                            //    UseSimpleDictionaryFormat = true,
                            //    MaxItemsInObjectGraph = int.MaxValue,
                            //    SerializeReadOnlyTypes = false,
                            //});

                            JsonSerializerCache[type] = match;
                        }
                    }
                    finally
                    {
                        Locker.ExitWriteLock();
                    }
                }
            }
            finally
            {
                Locker.ExitUpgradeableReadLock();
            }
            return match;
        }

        public static string SerializeDataContract(object obj)
        {
            if (obj == null) return string.Empty;
            DataContractSerializer serializer = GetSerializer(obj.GetType());
            using (StringWriter writer = new StringWriter())
            {
                using (XmlTextWriter xw = new XmlTextWriter(writer))
                {
                    serializer.WriteObject(xw, obj);
                    return writer.ToString();
                }
            }
        }

        public static object DeserializeDataContract(string str, Type type)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            DataContractSerializer serializer = GetSerializer(type);
            using (TextReader txtReader = new StringReader(str))
            {
                using (XmlReader xmlReader = new XmlTextReader(txtReader))
                {
                    return serializer.ReadObject(xmlReader);
                }
            }
        }

        public static string JsonSerializeDataContract(object obj)
        {
            if (obj == null) return string.Empty;
            DataContractJsonSerializer serializer = GetJsonSerializer(obj.GetType());

            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, obj);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public static T JsonDeserializeDataContract<T>(string jsonResp)
        {
            if (string.IsNullOrEmpty(jsonResp)) return default(T);
            DataContractJsonSerializer serializer = GetJsonSerializer(typeof(T));
            Stream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonResp)) as Stream;
            return (T)serializer.ReadObject(memoryStream);
        }

        public static string Serialize(object req)
        {
            XmlSerializer ser = new XmlSerializer(req.GetType());
            StringWriter writer = new StringWriter();

            ser.Serialize(writer, req);
            return writer.ToString();
        }

        public static object Deserialize(string response, Type type)
        {
            XmlSerializer ser = new XmlSerializer(type);
            return ser.Deserialize(new StringReader(response));
        }
    }
}
