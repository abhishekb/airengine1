﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Engines.CAPI.Services.Tests.Helpers
{
    public class WcfFormatter<TU> : IFormatter
        where TU : XmlObjectSerializer
    {
        private static readonly Dictionary<Type, TU> Cache = new Dictionary<Type, TU>();
        private static readonly ReaderWriterLockSlim Locker = new ReaderWriterLockSlim();

        public static readonly byte[] Empty = new byte[0];

        public void Serialize(Stream stream, object o)
        {
            TU serializer = GetSerializer(o.GetType());
            serializer.WriteObject(stream, o);
        }

        public object Deserialize(Type returnType, Stream stream)
        {
            TU serializer = GetSerializer(returnType);
            return serializer.ReadObject(stream);
        }

        public void Serialize<T>(Stream stream, T o)
        {
            TU serializer = GetSerializer(typeof(T));
            serializer.WriteObject(stream, o);
        }

        private static TU GetSerializer(Type type)
        {
            TU serializer;
            Locker.EnterUpgradeableReadLock();
            try
            {
                if (!Cache.TryGetValue(type, out serializer))
                {
                    Locker.EnterWriteLock();
                    try
                    {
                        if (!Cache.TryGetValue(type, out serializer))
                        {
                            serializer = Activator.CreateInstance(typeof(TU), type) as TU;
                            Cache[type] = serializer;
                        }
                    }
                    finally
                    {
                        Locker.ExitWriteLock();
                    }
                }
            }
            finally
            {
                Locker.ExitUpgradeableReadLock();
            }
            return serializer;
        }
    }
}
