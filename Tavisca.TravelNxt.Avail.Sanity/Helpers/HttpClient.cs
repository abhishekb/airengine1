﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tavisca.TravelNxt.Engines.CAPI.Services.Tests.Helpers
{
    public class HttpClient
    {
        private readonly Dictionary<string, string> _headers;
        private readonly bool _useNewtonSoft;
        public ContentType MimeType;

        public HttpClient(string url, ContentType contentType, Dictionary<string, string> headers = null,
            bool useNewtonSoft = true)
        {
            Url = url;
            MimeType = contentType;
            _headers = headers;
            _useNewtonSoft = useNewtonSoft;
            switch (contentType)
            {
                case ContentType.Json:
                    Formatter = new WcfFormatter<DataContractJsonSerializer>();
                    break;
                default:
                    Formatter = new WcfFormatter<DataContractSerializer>();
                    break;
            }
        }

        public string Url { get; private set; }
        public IFormatter Formatter { get; private set; }

        public TRs Get<TRs>()
            where TRs : class
        {
            HttpWebRequest webRequest = GetHttpWebRequest("GET");

            return GetResponse<TRs>(webRequest);
        }

        public TRs Post<TRq, TRs>(TRq request)
            where TRq : class
            where TRs : class
        {
            HttpWebRequest webRequest = GetHttpWebRequest("POST");

            return GetResponse<TRq, TRs>(request, webRequest);
        }

        public TRs UPDATE<TRq, TRs>(TRq request)
            where TRq : class
            where TRs : class
        {
            HttpWebRequest webRequest = GetHttpWebRequest("PUT");

            return GetResponse<TRq, TRs>(request, webRequest);
        }

        public TRs Update<TRq, TRs>(TRq request)
            where TRq : class
            where TRs : class
        {
            HttpWebRequest webRequest = GetHttpWebRequest("POST");

            return GetResponse<TRq, TRs>(request, webRequest);
        }

        public TRs Delete<TRq, TRs>(TRq request)
            where TRq : class
            where TRs : class
        {
            HttpWebRequest webRequest = GetHttpWebRequest("DELETE");

            return GetResponse<TRq, TRs>(request, webRequest);
        }

        private HttpWebRequest GetHttpWebRequest(string httpVerb)
        {
            HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(Url);

            webRequest.Method = httpVerb;         

            int timeOut = GetTimeOut();

            webRequest.Timeout = timeOut;

            webRequest.ContentType = MimeType == ContentType.Json
                ? "application/json"
                : "application/xml";

            webRequest.MediaType = MimeType == ContentType.Json
                ? "application/json"
                : "application/xml";

            AddHeaders(webRequest);

            return webRequest;
        }

        private int GetTimeOut()
        {
            int timeOut = 90000;
            string timeOutValue = ConfigurationManager.AppSettings["TimeOutLimitInMilliSeconds"];
            if (!string.IsNullOrEmpty(timeOutValue))
            {
                int.TryParse(timeOutValue, out timeOut);
            }
            return timeOut;
        }

        private void AddHeaders(HttpWebRequest webRequest)
        {
            if (_headers != null && _headers.Count > 0)
            {
                foreach (var keyValuePair in _headers)
                {
                    webRequest.Headers[keyValuePair.Key] = keyValuePair.Value;
                }
            }
        }

        private TRs GetResponse<TRq, TRs>(TRq postingRequest, HttpWebRequest webRequest)
            where TRq : class
            where TRs : class
        {
            webRequest.Headers.Add("Accept-Encoding", "gzip,deflate");

            webRequest = SerialisePostRequest<TRq, TRs>(postingRequest, webRequest);

            return GetResponse<TRs>(webRequest);
        }

        private static TRs GetResponseUsingNewtonSoft<TRq, TRs>(TRq postingRequest, HttpWebRequest webRequest)
            where TRq : class
            where TRs : class
        {
            using (HttpWebResponse httpResponse = (HttpWebResponse) webRequest.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return JsonConvert.DeserializeObject(streamReader.ReadToEnd(), typeof (TRs)) as TRs;
                }
            }
        }

        private HttpWebRequest SerialisePostRequest<TRq, TRs>(TRq postingRequest, HttpWebRequest webRequest)
            where TRq : class
            where TRs : class
        {
            using (Stream inputStream = webRequest.GetRequestStream())
            {
                Formatter.Serialize(inputStream, postingRequest);
            }
            return webRequest;
        }

        private TRs GetResponse<TRs>(HttpWebRequest webRequest)
            where TRs : class
        {
            using (WebResponse response = webRequest.GetResponse())
            {
                using (Stream outputStream = GetStreamForResponse(response as HttpWebResponse))
                {
                    return Formatter.Deserialize(typeof (TRs), outputStream) as TRs;
                }
            }
        }


        private static Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                default:
                    stream = webResponse.GetResponseStream();
                    //stream.ReadTimeout = readTimeOut;
                    break;
            }
            return stream;
        }
    }

    public enum ContentType
    {
        Json,
        Xml
    }
}
