﻿using System;
using System.IO;

namespace Tavisca.TravelNxt.Engines.CAPI.Services.Tests.Helpers
{
    public interface IFormatter
    {
        void Serialize<T>(Stream stream, T o);

        void Serialize(Stream stream, object o);

        object Deserialize(Type returnType, Stream stream);
    }
}