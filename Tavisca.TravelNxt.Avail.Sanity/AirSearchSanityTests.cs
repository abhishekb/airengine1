﻿using System.Data;
using System.Diagnostics;
using System.Threading;
using SchemaGenerator;
using Tavisca.TravelNxt.Avail.Sanity.AirEngineClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Tavisca.Frameworks.Parallel.Ambience;

using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.Avail.Sanity
{
    [TestClass]
    public class AirSearchSanityTests
    {
        private static IList<FlightSearchRQ> _scenarioDataFromExcel;

        public TestContext TestContext
        {
            get;
            set;
        }

        [ClassInitialize]
        public static void InitilizeScenarioData(TestContext context)
        {
            var loadExcelData = FlightSearchRQTestData.Load(@"TestData\FlightSearchRQTestData.xlsx");
            _scenarioDataFromExcel = loadExcelData.FlightSearchRQs;
            SetAirEngineUri();
        }

        private static void SetAirEngineUri()
        {
            const string serverIpFileKey = "serverIpFile";
            var ipFile = ConfigurationManager.AppSettings[serverIpFileKey];

            if (string.IsNullOrEmpty(ipFile))
                return;

            if (!File.Exists(ipFile))
                throw new InternalTestFailureException(string.Format("The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' is not valid, either empty it or the file should exist.", serverIpFileKey, ipFile));

            var text = File.ReadAllText(ipFile);

            var invalidFileTextExceptionMessage =
                string.Format(
                    "The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' contains an empty or invalid text.",
                    serverIpFileKey, ipFile);

            if (string.IsNullOrEmpty(text))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var parameters = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (parameters.Length == 0)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var serverIpLine = parameters[0].Split(':');

            if (serverIpLine.Length < 2)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var ip = serverIpLine[1].Trim();

            if (string.IsNullOrWhiteSpace(ip))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var address = GetBaseAirEngineAddress();

            SetBaseAirEngineAddress(address.Replace("##domain##", ip));
        }

        private static string _airEngineBaseAddress;
        private static string GetBaseAirEngineAddress()
        {
            return _airEngineBaseAddress ?? (_airEngineBaseAddress = ConfigurationManager.AppSettings["baseAirEngineAddress"]);
        }


        private static void SetBaseAirEngineAddress(string value)
        {
            _airEngineBaseAddress = value;
        }

        [TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
        public void TestAirSearchWithBasicHttpBinding()
        {
            var getRequest = CreateRequest(TestContext, _scenarioDataFromExcel);

            if (getRequest.Any())
            {
                CallAirSearchService(getRequest.FirstOrDefault().Key, getRequest.FirstOrDefault().Value, "BasicHttpBinding_IAirEngine", "");

            }
        }

        [TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
        public void TestAirSearchProtoBufBinding()
       {
            var getRequest = CreateRequest(TestContext, _scenarioDataFromExcel);

            if (getRequest.Any())
            {
                CallAirSearchService(getRequest.FirstOrDefault().Key, getRequest.FirstOrDefault().Value, "ProtoBufBinding_IAirEngine", "/proto");
            }
        }

        [TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
        public void TestAirSearchwithWebHttpBindingAndJson()
        {
            var getRequest = CreateRequest(TestContext, _scenarioDataFromExcel);
            var endPoint = GetBaseAirEngineAddress() + "/rest";

            if (getRequest.Any())
            {
                CallWebHttpBinding(getRequest, endPoint);
            }
        }

        [TestMethod, DataSource("AirSearchRequestData"), TestCategory("Sanity Tests")]
        public void TestAirSearchWebHttpBindingAndXml()
        {
            var getRequest = CreateRequest(TestContext, _scenarioDataFromExcel);
            var endPoint = GetBaseAirEngineAddress() + "/rest";
            if (getRequest.Any())
            {
                CallWebHttpBinding(getRequest, endPoint, "text/xml");
            }
        }

        public Dictionary<string, AirEngineClient.FlightSearchRQ> CreateRequest(TestContext context, IList<FlightSearchRQ> scenarioDataFromExcel, bool isdeferredSearch = false, bool isSupplierStreaming = false)
        {
            var response = new Dictionary<string, AirEngineClient.FlightSearchRQ>();
            DataRow dataRow = context.DataRow;

            var isScenarioEnable = bool.Parse(dataRow["IsEnable"].ToString());

            var scenarioName = dataRow["ScenarioName"].ToString();
            if (!isScenarioEnable)
            {
                Console.WriteLine(" **** Scenario is Disabled ***** {0}", scenarioName);
                return response;
            }

            var sno = dataRow["Sno"].ToString();
            var scenarioData = scenarioDataFromExcel.FirstOrDefault(x => x.Sno == sno);

            Console.WriteLine(" **** Scenario Started ***** {0}", scenarioName);

            if (scenarioData == null)
            {
                throw new InternalTestFailureException(string.Format("Data not found for scenarion {0}", scenarioName));
            }

            var excelAutoMapper = new ExcelAutoMapper();
            var clientRequest = excelAutoMapper.CopyObject(scenarioData, new AirEngineClient.FlightSearchRQ());
            if (isdeferredSearch)
                clientRequest.DoDeferredSearch = true;
            if (isSupplierStreaming)
                clientRequest.AllowSupplierStreaming = true;

            for (int searcSegmentNumber = 0; searcSegmentNumber < clientRequest.SearchCriterion.SearchSegments.Count; searcSegmentNumber++)
            {
                clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber].TravelDate.DateTime = searcSegmentNumber == 0 ? DateTime.Now.AddDays(120)
                    : clientRequest.SearchCriterion.SearchSegments[searcSegmentNumber - 1].TravelDate.DateTime.AddDays(150);
            }

            response.Add(scenarioName, clientRequest);
            return response;
        }

        /*
                private static DateTime GetRandomDateTime(DateTime? min = null, DateTime? max = null)
                {
                    Random Rnd = new Random();
                    min = min ?? new DateTime(1753, 01, 01);
                    max = max ?? new DateTime(9999, 12, 31);

                    var range = max.Value - min.Value;
                    var randomUpperBound = (Int32)range.TotalSeconds;
                    if (randomUpperBound <= 0)
                        randomUpperBound = Rnd.Next(1, Int32.MaxValue);

                    var randTimeSpan = TimeSpan.FromSeconds((Int64)(range.TotalSeconds - Rnd.Next(0, randomUpperBound)));
                    return min.Value.Add(randTimeSpan);
                }
        */

        private static void CallWebHttpBinding(Dictionary<string, AirEngineClient.FlightSearchRQ> request, string endPoint, string contentType = "text/json")
        {
            var fareRules = new AirFareRulesSanityTests();
            AirSeatMapSanityTests seatMaps = new AirSeatMapSanityTests();
            FlightSearchRS response = null;
            string resultMessage = "Success";
            try
            {
                using (var client = GetWebClient(contentType, Guid.NewGuid().ToString()))
                {
                    Console.WriteLine(" Calling Service End Point {0} for Scenario {1}", endPoint,
                        request.FirstOrDefault().Key);

                    var serializedRequest = GetSerializedRequest(request.FirstOrDefault().Value, contentType);
                    var bytesResponse = client.UploadData(endPoint + "/search",
                        Encoding.UTF8.GetBytes(serializedRequest));
                    response = GetResponseFromBytes(bytesResponse, contentType);
                }

                if (request.FirstOrDefault().Value.DoDeferredSearch)
                {
                    DoDeferredSearch(ref response, null, false, contentType, endPoint);
                }
                if (request.FirstOrDefault().Value.AllowSupplierStreaming)
                    DoDeferredSearch(ref response, null, true, contentType, endPoint);

                bool isSuccessfull = ValidateAirSearchResponse(response, out resultMessage);
                Assert.IsTrue(isSuccessfull, resultMessage);

                if (Boolean.Parse(ConfigurationManager.AppSettings["EnableFareRulesTest"]))
                {
                    string fareRulesErrorMessage;
                    Assert.IsTrue(fareRules.TestAirFareRulesWithWebHttpBinding(response, contentType, out fareRulesErrorMessage), fareRulesErrorMessage);
                }
                if (Boolean.Parse(ConfigurationManager.AppSettings["EnableSeatMapTest"]))
                {
                    string seatMapErrorMessage;
                    Assert.IsTrue(seatMaps.TestAirSeatMapWithWebHttpBinding(response, contentType, out seatMapErrorMessage), seatMapErrorMessage);
                }
            }
            finally
            {
                if (response != null)
                    Console.WriteLine(" **** Session Id : {0} ****", response.SessionID.ToString());
                Console.WriteLine(" Result {0}", resultMessage);
                Console.WriteLine(" **** Scenario Completed ***** {0}", request.FirstOrDefault().Key);
            }
        }

        public static void PrintResponseHighlights(FlightSearchRS response)
        {
            Console.WriteLine("\n\n***** Response Highlights *****\n");
            Console.WriteLine("Airlines Count: " + ((response.Airlines != null) ? response.Airlines.Count : 0));
            Console.WriteLine("Airport Count: " + ((response.Airports != null) ? response.Airports.Count : 0));
            Console.WriteLine("ItineraryRecommendations Count: " + ((response.ItineraryRecommendations != null) ? response.ItineraryRecommendations.Count : 0));
            Console.WriteLine("LegRecommendations Count: " + ((response.LegRecommendations != null) ? response.LegRecommendations.Count : 0));
            Console.WriteLine("LegAlternates Count: " + ((response.LegAlternates != null) ? response.LegAlternates.Count : 0));
            Console.WriteLine("LegAlternates Count: " + ((response.Segments != null) ? response.Segments.Count : 0));
            Console.WriteLine("Service Status: " + ((string.IsNullOrEmpty(response.ServiceStatus.Status.ToString())) ? response.ServiceStatus.Status.ToString() : "Empty Response status"));
            Console.WriteLine("Service Status Message: " + (response.ServiceStatus.StatusMessage ?? "Success"));
            Console.WriteLine("\n***** Response Highlights *****\n\n");
        }

        public static WebClient GetWebClient(string contentType, string guid)
        {
            var client = new WebClient();

            client.Headers.Add("culture", GetTestSetting("MockCulture"));
            client.Headers.Add("posId", GetTestSetting("MockPosId"));
            client.Headers.Add("accountId", GetTestSetting("MockGetAccountId"));
            client.Headers.Add("displayCurrency", GetTestSetting("DisplayCurrency"));
            client.Headers.Add("password", GetTestSetting("Password"));
            client.Headers.Add("sessionId", guid);
            client.Headers.Add("Content-Type", contentType);
            client.Headers.Add("Accept-Type", contentType);

            return client;
        }

        private static string XmlSerialize<T>(T obj)
        {
            var ser = new DataContractSerializer(obj.GetType());
            string xml;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                xml = Encoding.UTF8.GetString(ms.ToArray());
            }
            return xml;
        }

        public static T XmlDeserialize<T>(byte[] data)
        {
            var ser = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }

        public static T Deserialize<T>(byte[] data)
        {
            var ser = new DataContractJsonSerializer(typeof(T));
            using (var ms = new MemoryStream(data))
            {
                var obj = ser.ReadObject(ms);

                return (T)obj;
            }
        }

        private static string Serialize<T>(T obj)
        {
            var ser = new DataContractJsonSerializer(obj.GetType());
            string json;
            using (var ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);

                json = Encoding.UTF8.GetString(ms.ToArray());
            }
            return json;
        }


        private static void CallAirSearchService(string scenarioName, AirEngineClient.FlightSearchRQ searchRequest, string endPointName, string addressSuffix)
        {
            FlightSearchRS response = null;
            var fareRules = new AirFareRulesSanityTests();
            var seatMaps = new AirSeatMapSanityTests();
            var address = GetBaseAirEngineAddress() + addressSuffix;
            try
            {
                using (var client = new AirEngineClient.AirEngineClient(endPointName, address))
                {                    
                    Console.WriteLine(" Calling Service End Point {0} for Scenario {1}", client.Endpoint.Address, scenarioName);

                    using (new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                                                                   Guid.NewGuid().ToString(), GetTestSetting("MockPosId"), GetTestSetting("MockGetAccountId"),
                                                                   GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                    {                        
                        response = client.Search(searchRequest);
                        if (searchRequest.DoDeferredSearch)
                            DoDeferredSearch(ref response, client);

                    }
                }
                string resultMessage = "Success";
                bool isSeatMapSucessfull = true;
                bool isFareRulesSucessfull = true;
                string fareRulesErrorMessage = "Success";
                string seatMapErrorMessage = "Success";
                bool isSuccessfull = ValidateAirSearchResponse(response, out resultMessage);

                Assert.IsTrue(isSuccessfull, resultMessage);

                if (Boolean.Parse(ConfigurationManager.AppSettings["EnableFareRulesTest"]))
                {
                    isFareRulesSucessfull = fareRules.TestAirFareRulesWithProtoAndHttpBinding(response, addressSuffix, out fareRulesErrorMessage);
                }
                if (Boolean.Parse(ConfigurationManager.AppSettings["EnableSeatMapTest"]))
                {
                    isSeatMapSucessfull = seatMaps.TestAirSeatMapWithBasicHttpAndProtoBuffBinding(response, addressSuffix, out seatMapErrorMessage);
                }

                Assert.IsTrue(isFareRulesSucessfull, fareRulesErrorMessage);

                Assert.IsTrue(isSeatMapSucessfull, seatMapErrorMessage);
            }
            finally
            {
                if (response != null)
                    Console.WriteLine(" **** Session Id : {0} ****", response.SessionID);

                Console.WriteLine(" **** Scenario Completed ***** {0}", scenarioName);
            }

        }

        public static FlightSearchRS GetAirSearchResponse(string scenarioName, AirEngineClient.FlightSearchRQ searchRequest, string sessionId, string endPointName, string addressSuffix)
        {
            var address = GetBaseAirEngineAddress() + addressSuffix;

            using (var client = new AirEngineClient.AirEngineClient(endPointName, address))
            {
                Console.WriteLine(" Calling Service End Point {0} for Scenario {1}", client.Endpoint.Address, scenarioName);

                FlightSearchRS response;
                using (new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                                                               sessionId, GetTestSetting("MockPosId"), GetTestSetting("MockGetAccountId"),
                                                               GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                {
                    response = client.Search(searchRequest);

                    if (searchRequest.DoDeferredSearch)
                        DoDeferredSearch(ref response, client);
                    if (searchRequest.AllowSupplierStreaming)
                        DoDeferredSearch(ref response, client, true);
                }
                return response;
            }
        }

        /*
                private static string GetHeadersValue(string key)
                {
                    var value = _headers.First(x => x.Key == key).Value;
                    return value.ToString(CultureInfo.InvariantCulture);
                }
        */

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private static void DoDeferredSearch(ref FlightSearchRS response,
            AirEngineClient.AirEngineClient client, bool streamSuppliers = false, string contentType = null, string endPoint = null)
        {
            var timeOutLimit = long.Parse(GetTestSetting("TimeOutLimitInMilliSeconds"));

            var timeOutTimer = new Stopwatch();
            Guid guid = response.SessionID;
            timeOutTimer.Start();

            while (response != null && response.ServiceStatus.Status == CallStatus.InProgress &&
                   timeOutTimer.ElapsedMilliseconds <= timeOutLimit)
            {
                if (response.ServiceStatus.Status == CallStatus.InProgress)
                {
                    Thread.Sleep(1000);
                }

                var doDeferredRequest = new FlightPollRQ
                                            {
                                                PreviousTimeStamp = DateTime.SpecifyKind(new DateTime(2000, 1, 1), DateTimeKind.Utc),
                                                GetAsSupplierResponds = streamSuppliers

                                            };

                if (client != null)
                {
                    response = client.TryGetFromSession(doDeferredRequest);
                }
                else
                {
                    using (var currentClient = GetWebClient(contentType, guid.ToString()))
                    {
                        var serializedRequest = GetDeferredSerializedRequest(contentType, doDeferredRequest);
                        var bytesResponse = currentClient.UploadData(endPoint + "/tryGetFromSession",
                                                                 Encoding.UTF8.GetBytes(serializedRequest));
                        response = GetResponseFromBytes(bytesResponse, contentType);
                    }
                }
            }
            timeOutTimer.Stop();
        }

        public static string GetSerializedRequest<T>(T flightSearchRequest, string contentType)
        {
            string response = contentType == "text/json" ? Serialize(flightSearchRequest) : XmlSerialize(flightSearchRequest);
            return response;
        }

        private static string GetDeferredSerializedRequest(string contentType, FlightPollRQ doDeferredSearchRequest)
        {
            string response = contentType == "text/json" ? Serialize(doDeferredSearchRequest) : XmlSerialize(doDeferredSearchRequest);
            return response;
        }

        private static FlightSearchRS GetResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? Deserialize<FlightSearchRS>(byteResponse) : XmlDeserialize<FlightSearchRS>(byteResponse);

            return response;
        }


        /*
                private static AirSeatMapClient.SeatMapRS GetSeatMapResponseFromBytes(byte[] byteResponse, string contentType)
                {
                    var response = contentType == "text/json" ? Deserialize<AirSeatMapClient.SeatMapRS>(byteResponse) : XmlDeserialize<AirSeatMapClient.SeatMapRS>(byteResponse);

                    return response;
                }
        */


        private static bool ValidateAirSearchResponse(FlightSearchRS res, out string result)
        {
            var message = new StringBuilder();
            bool isSuccesfull;

            if (res == null)
            {
                message.AppendLine("Response is NULL");
                isSuccesfull = false;
            }

            else if (CallStatus.Failure == res.ServiceStatus.Status)
            {
                if (res.ServiceStatus.Messages != null && res.ServiceStatus.Messages.Count > 0)
                    message.AppendLine(res.ServiceStatus.Messages[0]);
                else if (res.ServiceStatus.StatusMessage.Length > 0)
                    message.AppendLine(res.ServiceStatus.StatusMessage + " Failure Status Code: " + res.ServiceStatus.StatusCode);

                isSuccesfull = false;
            }
            else if (res.LegRecommendations == null && res.ItineraryRecommendations == null)
            {
                message.AppendLine("LegAlternates and ItineraryRecommendations are NULL");
                isSuccesfull = false;
            }
            //else if ((res.LegRecommendations != null && res.LegRecommendations.Count == 0) && res.ItineraryRecommendations.Count == 0)
            //{
            //    message.AppendLine("No Results Found");
            //    isSuccesfull = false;
            //}
            else
                isSuccesfull = true;

            result = message.ToString();

            return isSuccesfull;
        }
    }
}
