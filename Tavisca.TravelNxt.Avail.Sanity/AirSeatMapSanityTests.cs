﻿using System.IO;
using Tavisca.TravelNxt.Avail.Sanity.AirEngineClient;
using Tavisca.TravelNxt.Avail.Sanity.AirSeatMapClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;
using Tavisca.TravelNxt.Common.Extensions;
using System.Threading;

namespace Tavisca.TravelNxt.Avail.Sanity
{

    public class AirSeatMapSanityTests
    {
        private static string _airSeatMapBaseAddress;
        private static readonly List<int> RefIds = new List<int>();
        private static readonly List<FlightRecommendation> PreloadedRecommendations = new List<FlightRecommendation>();
        private static readonly List<string> SegamentKeyList = new List<string>()
                                                          {
                                                             "AA-LAS-1226", "AA-LAS-2493","AA-LAS-1126","AA-LAS-169","AA-LAS-215","AA-LAS-2384","AA-LAS-169","AA-LAS-1126","AA-LAS-2493","VX-LAS-481","VX-LAS-469","DL-LAS-5685","AA-LAX-1226","B6-LAS-879",
                                                             "B6-LAS-379","B6-LAS-279","VX-LAS-797","DL-LAS-5684","VX-LAX-480","VX-LAX-474","VX-LAS-469","VX-LAS-1495","AA-LAS-215","AA-ORD-392","AA-ORD-398","AA-ORD-394","AA-ORD-374","AA-ORD-380","AA-ORD-2224",
                                                             "AA-ORD-610","AA-ORD-2347","AA-ORD-2021","VX-LAS-260","AA-LAS-2395","AA-LAS-1547","AA-LAS-160","AA-ORD-388","AA-LGA-337","AA-ORD-2305"
                                                          };

        public AirSeatMapSanityTests()
        {
            SetSeatMapUri();
        }

        private static void SetSeatMapUri()
        {
            const string serverIpFileKey = "serverIpFile";
            var ipFile = ConfigurationManager.AppSettings[serverIpFileKey];

            if (string.IsNullOrEmpty(ipFile))
                return;

            if (!File.Exists(ipFile))
                throw new InternalTestFailureException(string.Format("The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' is not valid, either empty it or the file should exist.", serverIpFileKey, ipFile));

            var text = File.ReadAllText(ipFile);

            var invalidFileTextExceptionMessage =
                string.Format(
                    "The air engine ip file provided in the configuration file with key: '{0}' and value: '{1}' contains an empty or invalid text.",
                    serverIpFileKey, ipFile);

            if (string.IsNullOrEmpty(text))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var parameters = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (parameters.Length == 0)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var serverIpLine = parameters[0].Split(':');

            if (serverIpLine.Length < 2)
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var ip = serverIpLine[1].Trim();

            if (string.IsNullOrWhiteSpace(ip))
                throw new InternalTestFailureException(invalidFileTextExceptionMessage);

            var address = GetBaseAirSeatMapAddress();

            SetBaseAirSeatMapAddress(address.Replace("##domain##", ip));
        }

        private static void SetBaseAirSeatMapAddress(string address)
        {
            _airSeatMapBaseAddress = address;
        }

        private static string GetBaseAirSeatMapAddress()
        {
            return _airSeatMapBaseAddress ?? (_airSeatMapBaseAddress = ConfigurationManager.AppSettings["baseAirSeatMapAddress"]);
        }

        /// <summary>
        /// Load the reccomedations other than recommendations which contains US and UA as marketing airlines. Reason behind filetring these air lines is they dont have seatmaps avaiblable with them
        /// </summary>
        /// <param name="searchResponse"></param>
        public static void LoadRecommendations(FlightSearchRS searchResponse)
        {
            PreloadedRecommendations.Clear();
            var allRecommendations = new List<FlightRecommendation>();
            if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                allRecommendations.AddRange(searchResponse.LegRecommendations);
            if (searchResponse.ItineraryRecommendations != null && searchResponse.ItineraryRecommendations.Count > 0)
                allRecommendations.AddRange(searchResponse.ItineraryRecommendations);

            foreach (var flightRecommendation in allRecommendations)
            {
                var recommendation = flightRecommendation;
                var recommendationLegRef = recommendation.LegRefs[new Random().Next(recommendation.LegRefs.Count)].Split('/');
                var legAlternateIndex = int.Parse(recommendationLegRef[0]);
                var lefRefId = int.Parse(recommendationLegRef[1]);

                var leg = searchResponse.LegAlternates[legAlternateIndex].Legs.First(l => l.RefID == lefRefId);

                var segment =
                    searchResponse.Segments.First(x => x.RefId == leg.FlightSegmentRefs.First());
                if (!(segment.MarketingAirlineCode.Contains("UA") || segment.MarketingAirlineCode.Contains("US")))
                    PreloadedRecommendations.Add(flightRecommendation);

            }
        }

        /// <summary>
        /// Loading most probable airlines which have SeatMaps available with them all the time.
        /// </summary>
        /// <param name="searchResponse"></param>

        public static void LoadRefIdsForAirlines(FlightSearchRS searchResponse)
        {

            RefIds.Clear();
            foreach (var airline in SegamentKeyList)
            {
                foreach (var segment1 in searchResponse.Segments)
                {
                    if (airline.Equals(segment1.MarketingAirlineCode + "-" + segment1.DepartureAirportCode + "-" + segment1.FlightNumber))
                    {
                        RefIds.Add(segment1.RefId);
                        break;
                    }
                }
            }
        }

        public bool TestAirSeatMapWithBasicHttpAndProtoBuffBinding(FlightSearchRS response, string binding, out string errorMessage)
        {
            SeatMapRS seatMapResponse = null;
            LoadRecommendations(response);
            LoadRefIdsForAirlines(response);
            for (int tryCount = Int16.Parse(ConfigurationManager.AppSettings["SeatMapTryCount"]);tryCount > 0;tryCount--)
            {
                seatMapResponse = CallAirSeatMapService(GetSeatMapRequest(response), response.SessionID.ToString(), binding);
                if (seatMapResponse.SeatMap != null && seatMapResponse.SeatMap.Cabins.Length > 0)
                    break;
            }

            return ValidateSeatMapResponse(seatMapResponse, out errorMessage);
        }


        public bool TestAirSeatMapWithWebHttpBinding(FlightSearchRS response, string contentType, out string errorMessage)
        {
            SeatMapRS seatMapResponse = null;
            LoadRecommendations(response);
            LoadRefIdsForAirlines(response);
            for (int tryCount = Int16.Parse(ConfigurationManager.AppSettings["SeatMapTryCount"]); tryCount > 0; tryCount--)
            {
                var endPoint = GetBaseAirSeatMapAddress() + "/rest";
                seatMapResponse = CallSeatMapWebHttpBinding(GetSeatMapRequest(response), response.SessionID.ToString(), endPoint, contentType);
                if (seatMapResponse.SeatMap != null && seatMapResponse.SeatMap.Cabins.Length > 0)
                    break;
            }
            return ValidateSeatMapResponse(seatMapResponse, out errorMessage);
        }

        private static SeatMapRS CallSeatMapWebHttpBinding(SeatMapRQ request, string sessionId, string endPoint, string contentType = "text/json")
        {
            SeatMapRS response;
            using (var client = AirSearchSanityTests.GetWebClient(contentType, sessionId))
            {
                var serializedRequest = AirSearchSanityTests.GetSerializedRequest(request, contentType);
                var bytesResponse = client.UploadData(endPoint + "/getseatmap",
                    Encoding.UTF8.GetBytes(serializedRequest));
                response = GetSeatMapResponseFromBytes(bytesResponse, contentType);
            }
            return response;
        }

        private static SeatMapRS GetSeatMapResponseFromBytes(byte[] byteResponse, string contentType)
        {
            var response = contentType == "text/json" ? AirSearchSanityTests.Deserialize<SeatMapRS>(byteResponse) : AirSearchSanityTests.XmlDeserialize<SeatMapRS>(byteResponse);

            return response;
        }

        private SeatMapRS CallAirSeatMapService(SeatMapRQ seatMapRequest, string sessionId, string addressSuffix)
        {
            string endPointName = addressSuffix.ToLower().Equals("") ? "BasicHttpBinding_ISeatMap" : "ProtoBufBinding_ISeatMap";
            SeatMapRS response;
            var address = GetBaseAirSeatMapAddress() + addressSuffix;
            Console.WriteLine(" Calling Service End Point {0} for Scenario", address);
            using (var client = new AirSeatMapClient.AirSeatMapClient(endPointName, address))
            {
                using (new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                                                               sessionId, GetTestSetting("MockPosId"), GetTestSetting("MockGetAccountId"),
                                                               GetTestSetting("DisplayCurrency"), GetTestSetting("Password"), false, string.Empty)))
                {
                    response = client.GetSeatMap(seatMapRequest);
                }
            }
            return response;

        }

        private string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private SeatMapRQ GetSeatMapRequest(FlightSearchRS response, int refId = 0)
        {
            SeatMapRQ seatMapRequest = null;
            if (string.IsNullOrEmpty(response.SessionID.ToString()))
                throw new InternalTestFailureException("Null or Empty SessionId found in one of the AirSearch Response made in ClassInitialize method....");

            if ((response.LegRecommendations != null && response.LegRecommendations.Count > 0) || (response.ItineraryRecommendations != null && response.ItineraryRecommendations.Count > 0))
            {
               
                if (RefIds.Count == 0)
                {
                    seatMapRequest = CreateSeatmapRequest(response);
                }
                else
                {
                    seatMapRequest =  CreateSeatmapRequest(response, RefIds.First());
                    RefIds.Remove(RefIds.First());
                }
                Console.WriteLine("\n\n***** Seat Map Request *****");
                Console.WriteLine("RecommendationRefId: " + seatMapRequest.RecommendationRefId);
                Console.WriteLine("SegmentKey: " + seatMapRequest.SegmentKey);
            }

            else
                throw new InternalTestFailureException("Could not start AirSeatMap Test because 0 ItineraryRecommendations or LegRecommendations found in AirSearch request made....");
            return seatMapRequest;
        }

        private static SeatMapRQ CreateSeatmapRequest(FlightSearchRS searchResponse, int refId = 0, int flightNumberLength = 0)
        {
            int segmentRef = -1;
            var recommendation = GetSingleRecommendation(searchResponse, ref segmentRef, refId, flightNumberLength);
            var recommendationLegRef = recommendation.LegRefs[new Random().Next(recommendation.LegRefs.Count)].Split('/');
            var legAlternateIndex = int.Parse(recommendationLegRef[0]);
            var lefRefId = int.Parse(recommendationLegRef[1]);

            var leg = searchResponse.LegAlternates[legAlternateIndex].Legs.First(l => l.RefID == lefRefId);

            var segment =
                searchResponse.Segments.First(
                    x => (segmentRef == -1) ? x.RefId == leg.FlightSegmentRefs.First() : x.RefId == segmentRef);

            return new SeatMapRQ
                       {
                           RecommendationRefId = recommendation.RefID,
                           SegmentKey = segment.Key
                       };
        }


        private static FlightRecommendation GetSingleRecommendation(FlightSearchRS searchResponse, ref int segmentRefId, int refId = 0, int flightNumberLength = 0)
        {
            if (refId == 0)
            {
                if (PreloadedRecommendations.Count > 0)
                    return PreloadedRecommendations[StaticRandom.Instance.Next(0, PreloadedRecommendations.Count)];

                if (searchResponse.LegRecommendations != null && searchResponse.LegRecommendations.Count > 0)
                    return searchResponse.LegRecommendations[StaticRandom.Instance.Next(0, searchResponse.LegRecommendations.Count)];

                return searchResponse.ItineraryRecommendations[StaticRandom.Instance.Next(0, searchResponse.ItineraryRecommendations.Count)];
            }
            var leg =
                searchResponse.LegAlternates.SelectMany(x => x.Legs).First(x => x.FlightSegmentRefs.Contains(refId)).
                    RefID;
            var allRecommendations = new List<FlightRecommendation>();
            if (searchResponse.LegRecommendations != null)
                allRecommendations.AddRange(searchResponse.LegRecommendations);

            if (searchResponse.ItineraryRecommendations != null)
                allRecommendations.AddRange(searchResponse.ItineraryRecommendations);

            segmentRefId = refId;

            return allRecommendations.First(x => x.LegRefs.Any(y => int.Parse(y.Split('/')[1]) == leg));
        }


        private static bool ValidateSeatMapResponse(SeatMapRS seatmapResponse, out string errorMessage)
        {
            var errorMessages = new StringBuilder();
            bool isSuccessFull = true;

            if (seatmapResponse == null)
            {
                isSuccessFull = false;
                errorMessages.AppendLine("SeatMap response is null...");
            }
            if (seatmapResponse != null && seatmapResponse.SeatMap == null)
            {
                isSuccessFull = false;
                errorMessages.AppendLine("SeatMaps are null...");
            }

            if (seatmapResponse != null && seatmapResponse.ServiceStatus == null)
            {
                isSuccessFull = false;
                errorMessages.AppendLine("Service status is null...");
            }
            if (seatmapResponse != null && (seatmapResponse.ServiceStatus != null && (string.Equals(AirSeatMapClient.CallStatus.Failure, seatmapResponse.ServiceStatus.Status))))
            {
                if (!string.IsNullOrEmpty(seatmapResponse.ServiceStatus.StatusMessage))
                    errorMessages.AppendLine(seatmapResponse.ServiceStatus.StatusMessage);
                if (seatmapResponse.ServiceStatus.Messages.Length > 0)
                {
                    foreach (string message in seatmapResponse.ServiceStatus.Messages)
                        errorMessages.AppendLine(message);
                }
            }
            //Assert.AreEqual(Tavisca.TravelNxt.Avail.Sanity.AirSeatMapClient.CallStatus.Success, seatmapResponse.ServiceStatus.Status,
            //   errorMessages.ToString());

            if (seatmapResponse != null && (seatmapResponse.SeatMap != null && seatmapResponse.SeatMap.Cabins.Length == 0))
                isSuccessFull = false;

            errorMessage = errorMessages.ToString();
            return isSuccessFull;
        }


    }

    public static class StaticRandom
    {
        private static int _seed;

        private static readonly ThreadLocal<Random> ThreadLocal = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref _seed)));

        static StaticRandom()
        {
            _seed = Environment.TickCount;
        }

        public static Random Instance { get { return ThreadLocal.Value; } }
    }
}
