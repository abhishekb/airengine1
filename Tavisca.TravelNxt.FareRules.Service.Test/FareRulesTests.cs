﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.FareRules.Service.Test.AirEngine;
using Tavisca.TravelNxt.FareRules.Service.Test.AirFareRules;
using CallStatus = Tavisca.TravelNxt.FareRules.Service.Test.AirEngine.CallStatus;
using Tavisca.TravelNxt.Common.Extensions;

namespace Tavisca.TravelNxt.FareRules.Service.Test
{
    [TestClass]
    public class FareRulesTests
    {
        [TestMethod]
        [TestCategory("Service Test FareRules")]
        public void TestTrvelportFareRules()
        {
            FlightSearchRQ fareSearchRq = RequestProvider.GetFareSearchRequest(Constants.Travelport);
            using (var scope = new AmbientContextScope(new CallContext(GetTestSetting("MockCulture"),
                            Guid.NewGuid().ToString(), "170",
                            GetTestSetting("MockGetAccountId"), GetTestSetting("DisplayCurrency"),
                            GetTestSetting("Password"), false, string.Empty)))
            using (var client = new AirEngineClient("AirBasicHttpEndpoint"))
            {
                var fareSearchResponse = client.Search(fareSearchRq);
                Assert.IsNotNull(fareSearchResponse.ServiceStatus);
                Assert.AreEqual(fareSearchResponse.ServiceStatus.Status, CallStatus.Success);
                var fareRulesResponse = new AirFareRulesClient().GetFareRules(GetFareRulesRequest(fareSearchResponse, Constants.Travelport));
                ValidateResponse(fareRulesResponse);
            }
        }

        private static string GetTestSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        private static FareRulesRQ GetFareRulesRequest(FlightSearchRS fareSearchResponse, string provider)
        {
            return new FareRulesRQ
            {
                RecommendationRefID = fareSearchResponse.LegRecommendations.First().RefID
            };
        }

        private static void ValidateResponse(FareRulesRS fareRulesResponse)
        {
            Assert.IsNotNull(fareRulesResponse);
            Assert.IsNotNull(fareRulesResponse.FareRules);
            Assert.IsNotNull(fareRulesResponse.ServiceStatus);
            Assert.AreEqual(fareRulesResponse.ServiceStatus.Status, AirFareRules.CallStatus.Success);
        }
    }
}
