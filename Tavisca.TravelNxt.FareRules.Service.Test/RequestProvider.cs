﻿using System;
using System.Collections.Generic;
using Tavisca.TravelNxt.FareRules.Service.Test.AirEngine;

namespace Tavisca.TravelNxt.FareRules.Service.Test
{
    public class RequestProvider
    {
        public static FlightSearchRQ GetFareSearchRequest(string provider)
        {
            var flightSearchCriteria = new FlightSearchCriteria() { };

            //flightSearchCriteria.AdditionalInfo.Add("mock", "true");

            flightSearchCriteria.SortingOrder = SortingOrder.ByPriceLowToHigh;
            flightSearchCriteria.SearchSegments = new List<SearchSegment>();
            flightSearchCriteria.SearchSegments.Add(new SearchSegment
            {
                ArrivalAirportCode = "LAX",
                ArrivalAlternateAirportInformation =
                    new AlternateAirportInformation()
                    {
                        IncludeNearByAirports = false,
                        RadiusKm = 10
                    },
                DepartureAirportCode = "LAS",
                TravelDate =
                    new TravelDate
                    {
                        DateTime =
                            DateTime.Now
                            .AddMonths(3).AddMinutes(20).Date,
                            AnyTime = true

                    }
            });
            flightSearchCriteria.PassengerInfoSummary = new List<PaxTypeQuantity>();

            var request = new FlightSearchRQ() { SearchCriterion = flightSearchCriteria };
            request.Requester = new Requester();
            flightSearchCriteria.PassengerInfoSummary.Add(new PaxTypeQuantity()
            {
                PassengerType = PassengerType.Adult,
                Quantity = 1,
                Ages = new List<int>() { 25 }
            });
            return request;
        }
    }
}
