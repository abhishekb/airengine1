﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Parallel.Ambience.Wcf;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Validation;
using Tavisca.TravelNxt.Flight.Book.DataContract;
using Tavisca.TravelNxt.Flight.Book.ServiceContract;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Book.ServiceImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [AmbientOperation(ApplicationName = "Air Book")]
    public class AirBook : IAirBook
    {
        static AirBook()
        {
            Utility.SetApplicationName("Air Engine");
        }

        #region IAirBook Members

        public FlightSaveRS Save(FlightSaveRQ request)
        {
            var logFactory = Utility.GetLogFactory();
            IEventEntry entry = null;
            try
            {
                var stopwatch = new WrappedTimer();
                stopwatch.Start();

                entry = Utility.GetEventEntryContextual();

                entry.Title = "{0} - Root Level Air Save Log";

                entry.CallType = KeyStore.CallTypes.AirBook;

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var validationResult = GetValidationFactory().Validate(request);

                if (validationResult.IsValid)
                {
                    var entityRQ = FlightSaveRQ.ToEntity(request);

                    var entityRS = GetBookingEngine().Save(entityRQ);

                    var retVal = FlightSaveRS.ToDataContract(entityRS);

                    stopwatch.Stop();
                    entry.TimeTaken = stopwatch.ElapsedSeconds;

                    if (retVal != null)
                        entry.ResponseObject = retVal;

                    return retVal;
                }
                else
                {
                    throw validationResult.ToException();
                }
            }
            catch (Exception ex)
            {
                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                throw;
            }
            finally
            {
                if (entry != null) logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
            }
        }

        public FlightRetrieveRS Retrieve(FlightRetrieveRQ request)
        {
            var logFactory = Utility.GetLogFactory();
            IEventEntry entry = null;
            try
            {
                var stopwatch = new WrappedTimer();
                stopwatch.Start();

                entry = Utility.GetEventEntryContextual();

                entry.CallType = KeyStore.CallTypes.AirBookingRetrieve;

                entry.Title = "{0} - Root Level Air Retrieve Log";

                entry.RequestObject = request;
                entry.ReqResSerializerType = SerializerType.DataContractSerializer;

                var validationResult = GetValidationFactory().Validate(request);

                if (validationResult.IsValid)
                {
                    var entityRQ = FlightRetrieveRQ.ToEntity(request);

                    var entityRS = GetBookingEngine().Retrieve(entityRQ);

                    var retVal = FlightRetrieveRS.ToDataContract(entityRS);

                    stopwatch.Stop();
                    entry.TimeTaken = stopwatch.ElapsedSeconds;

                    if (retVal != null)
                        entry.ResponseObject = retVal;

                    return retVal;
                }
                else
                {
                    throw validationResult.ToException();
                }
            }
            catch (Exception ex)
            {
                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Regular);

                throw;
            }
            finally
            {
                if (entry != null) logFactory.WriteAsync(entry, KeyStore.LogCategories.ServiceRootLevel);
            }
        }

        #endregion

        #region Protected Members

        protected IValidationFactory GetValidationFactory()
        {
            return RuntimeContext.Resolver.Resolve<IValidationFactory>();
        }

        protected IBookingEngine GetBookingEngine()
        {
            return RuntimeContext.Resolver.Resolve<IBookingEngine>();
        }

        #endregion
    }
}
