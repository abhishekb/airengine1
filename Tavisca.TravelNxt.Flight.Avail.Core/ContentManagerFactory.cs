using System.Collections.Concurrent;
using System.Globalization;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ContentManagerFactory : IContentManagerFactory
    {
        private readonly ConcurrentDictionary<string, IContentManager> _contentManagers = new ConcurrentDictionary<string, IContentManager>();

        public IContentManager GetContentManager()
        {
            return _contentManagers.GetOrAdd(GetCultureKey(), 
                s => RuntimeContext.Resolver.Resolve<IContentManager>());
        }

        protected virtual string GetCultureKey()
        {
            var context = Utility.GetCurrentContext();

            if (context == null || context.Culture == null)
                return CultureInfo.InvariantCulture.Name;

            return context.Culture.Name;
        }
    }
}