﻿using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;
using Fare = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Fare;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    internal static class LegGroupTranslator
    {
        internal static List<FlightLeg> ToLegDetails(this List<LegGroup> legGroups, List<Data.Services.AirFareSearchV1.Flight> uniqueFlighs, Fare fare, IList<PaxTypeQuantity> paxTypeQuantities, ProviderSpace providerSpace, List<Data.Services.AirFareSearchV1.Baggage> baggages,
            ItineraryTypeOptions itineraryTypeOptions)
        {
            var flightLegs = new List<FlightLeg>();

            if (legGroups == null || legGroups.Count == 0)
                return flightLegs;

            //One Fare context is equal to one leg.
            for (var legIndex = 0; legIndex < fare.FareContext.Count; legIndex++)
            {
                var fareContext = fare.FareContext[legIndex];
                var legGroup = legGroups.Find(legG => legG.LegGroupNumber == fareContext.LegGroupNumber);
                if (legGroup == null)
                    continue;
                var legDetail = legGroup.LegRecommendations.Find(leg => leg.LegNumber == fareContext.LegNumber);
                if (legDetail == null) continue;
                var flightLeg = new FlightLeg()
                {
                    LegIndex = legIndex,
                    LegGroupNumber = fareContext.LegGroupNumber
                };
                flightLeg.AdditionalInfo.AddOrUpdate(KeyStore.Scepter.SupplierRefKey, legGroup.SupplierRefKey);
                flightLeg.Segments.AddRange(
                    legDetail.ToFlightSegments(uniqueFlighs, fare, paxTypeQuantities,
                                               providerSpace, fareContext.MarriageSegmentDetails, baggages));
                GetLegFareRules(flightLeg.Segments, fare).ForEach(
                    fareRule => flightLeg.AdditionalInfo.AddOrUpdate(fareRule.Key, fareRule.Value));

                flightLegs.Add(flightLeg);

                //Don't set the legtype in case of multicity
                if (itineraryTypeOptions == ItineraryTypeOptions.MultiCity)
                    continue;

                flightLeg.LegType = fareContext.LegGroupNumber == 1 ? LegType.Onward : LegType.Return;
            }
            return flightLegs;
        }

        private static IList<KeyValuePair<string, string>> GetLegFareRules(FlightSegmentCollection flightSegmentCollection, Fare fare)
        {
            var legFareRules = new List<KeyValuePair<string, string>>();

            if (flightSegmentCollection != null && flightSegmentCollection.Count > 0)
                legFareRules.AddRange(from flightSegment in flightSegmentCollection
                                      let segmentFareRules = GetFareRulesForFlightSegment(flightSegment, fare)
                                      where segmentFareRules != null && segmentFareRules.Count > 0
                                      select
                                          new KeyValuePair<string, string>(
                                          "FareRules_" + flightSegment.GetFlightCode(),
                                          string.Join("|", segmentFareRules)));
            return legFareRules;
        }

        private static List<string> GetFareRulesForFlightSegment(FlightSegment flightSegment, Fare fare)
        {
            var segmentFareRules = new List<string>();
            if (fare.PassengerFareBreakups != null)
            {
                foreach (var passengerFareBreakup in fare.PassengerFareBreakups)
                {
                    // check for matching flight segment
                    if (passengerFareBreakup.FareDetails.Exists(f => f.FlightCode == flightSegment.GetFlightCode()))
                    {
                        if (passengerFareBreakup.FareRules != null)
                        {
                            foreach (var fareRule in passengerFareBreakup.FareRules)
                            {
                                // add fare rule only if not empty and not duplicate
                                if (!string.IsNullOrEmpty(fareRule) && !segmentFareRules.Contains(fareRule))
                                    segmentFareRules.Add(fareRule);
                            }
                        }
                    }
                }
            }
            return segmentFareRules;
        }

        public static string GetFlightCode(this FlightSegment flightSegment)
        {
            return string.Format("{0}-{1}-{2}-{3}", flightSegment.MarketingAirline.Code, flightSegment.FlightNumber, flightSegment.DepartureDateTime.ToString("ddMMyyHHmm"), flightSegment.ArrivalDateTime.ToString("ddMMyyHHmm"));
        }
    }
}
