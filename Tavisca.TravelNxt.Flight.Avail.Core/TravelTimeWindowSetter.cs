﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class TravelTimeWindowSetter : ITravelTimeWindowSetter
    {
        public void SetTravelTimeWindows(FlightSearchCriteria searchCriteria, ProviderSpace providerSpace)
        {
            if (searchCriteria == null)
                throw new ArgumentNullException("searchCriteria");

            if (searchCriteria.SearchSegments == null)
                throw new ArgumentException("searchCriteria provided cannot have SearchSegments as null.", "searchCriteria");

            foreach (var searchSegment in searchCriteria.SearchSegments)
            {
                if (searchSegment.TravelDate == null)
                    searchSegment.TravelDate = new TravelDate();

                SetTravelTime(searchSegment.TravelDate, providerSpace);
            }
        }

        protected virtual void SetTravelTime(TravelDate travelDate, ProviderSpace providerSpace)
        {
            if (travelDate.AnyTime)
            {
                return;
            }

            if (travelDate.TimeWindow != 0) //UI takes priority
                return;

            string timeWindowText;
            if (providerSpace.AdditionalInfo.TryGetValue(
                KeyStore.ProviderSpaceAttributes.TimeWindowKey, out timeWindowText)) //provider space attributes takes second priority.
            {
                int fsTimeWindow;
                if (int.TryParse(timeWindowText, out fsTimeWindow))
                {
                    travelDate.TimeWindow = fsTimeWindow;
                    return;
                }
            }

            travelDate.TimeWindow = SettingManager.DefaultTimeWindow; //deployment settings takes last priority, if none of the above are found.
        }
    }
}
