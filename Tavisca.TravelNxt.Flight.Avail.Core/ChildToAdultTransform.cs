﻿using System.Collections.Generic;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using System.Linq;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ChildToAdultTransform : ITransformPassengerInfo
    {
        public FlightSearchCriteria GetTransformedSearchCriteria(FlightSearchCriteria searchCriteria, ProviderSpace providerSpace)
        {
            LinkedList<int> thresholdCrossedChildAges;

            Utility.LogDiagnosticEntry("Masquerading child as adult not required");

            if (IsProcessingNeeded(searchCriteria) == false)
                return searchCriteria;

            Utility.LogDiagnosticEntry("Proceeding to masquerade child as adult");

            ProcessThresholdCrossedChildAges(searchCriteria, GetMaxChildAge(providerSpace), out thresholdCrossedChildAges);

            if (thresholdCrossedChildAges.Count == 0)
                return searchCriteria;

            AddAdultPassengersForConvertedChildren(searchCriteria, thresholdCrossedChildAges);

            return searchCriteria;
        }

        #region private methods

        private static int GetMaxChildAge(ProviderSpace providerSpace)
        {
            var maxAllowedChildAge = string.Empty;
            if (providerSpace.AdditionalInfo != null)
                providerSpace.AdditionalInfo.TryGetValue(KeyStore.ProviderSpaceAttributes.MaxAllowedChildAge,
                                                         out maxAllowedChildAge);
            int retVal;
            return (!string.IsNullOrWhiteSpace(maxAllowedChildAge) && int.TryParse(maxAllowedChildAge, out retVal))
                       ? retVal
                       : 11;
        }

        private static bool IsProcessingNeeded(FlightSearchCriteria flightSearchCriteria)
        {
            return flightSearchCriteria != null && flightSearchCriteria.PassengerInfoSummary != null &&
                   flightSearchCriteria.PassengerInfoSummary.Count != 0 &&
                   flightSearchCriteria.PassengerInfoSummary.Any(x => x.PassengerType == PassengerType.Child);
        }

        private static void AddAdultPassengersForConvertedChildren(FlightSearchCriteria searchCriteria, ICollection<int> thresholdCrossedChildAges)
        {
            var adultPaxType = GetExistingPaxtypeOrCreateIfAbsent(searchCriteria.PassengerInfoSummary, PassengerType.Adult);
            adultPaxType.Quantity += thresholdCrossedChildAges.Count;
            adultPaxType.ContainsConvertedChild = true;

            foreach (var childAge in thresholdCrossedChildAges)
                adultPaxType.Ages.Add(childAge);
        }

        private static void ProcessThresholdCrossedChildAges(FlightSearchCriteria flightSearchCriteria, int maxAllowedChildAge, out LinkedList<int> thresholdCrossedChildAges)
        {
            thresholdCrossedChildAges = new LinkedList<int>();
            var childTypesToProcess =
                flightSearchCriteria.PassengerInfoSummary.Where(
                    x => x.PassengerType == PassengerType.Child && x.Ages.Any(age => age > maxAllowedChildAge));

            var paxTypesToRemove = new LinkedList<PaxTypeQuantity>();
            foreach (var childTypeQuantity in childTypesToProcess)
            {
                var currentInvalidAges = new LinkedList<int>();
                foreach (var age in childTypeQuantity.Ages.Where(x => x > maxAllowedChildAge))
                {
                    currentInvalidAges.AddLast(age);
                    thresholdCrossedChildAges.AddLast(age);
                }

                if (currentInvalidAges.Count == childTypeQuantity.Ages.Count)
                {
                    paxTypesToRemove.AddLast(childTypeQuantity);
                    continue;
                }

                childTypeQuantity.Quantity -= currentInvalidAges.Count;
                foreach (var invalidAge in currentInvalidAges)
                    childTypeQuantity.Ages.Remove(invalidAge);
            }

            //remove paxtypes for which all have been transferred to adult
            foreach (var paxTypeToRemove in paxTypesToRemove)
                flightSearchCriteria.PassengerInfoSummary.Remove(paxTypeToRemove);
        }

        private static PaxTypeQuantity GetExistingPaxtypeOrCreateIfAbsent(ICollection<PaxTypeQuantity> paxTypeQuantities, PassengerType passengerType)
        {
            var requestedPaxType =
                paxTypeQuantities.FirstOrDefault(x => x.PassengerType == passengerType);

            if (requestedPaxType == null)
            {
                requestedPaxType = new PaxTypeQuantity() { PassengerType = passengerType, Ages = new List<int>(), Quantity = 0 };

                paxTypeQuantities.Add(requestedPaxType);
            }

            return requestedPaxType;
        }

        #endregion
    }
}
