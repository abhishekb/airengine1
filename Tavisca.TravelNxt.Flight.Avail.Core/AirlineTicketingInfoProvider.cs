﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class AirlineTicketingInfoProvider : IAirlineTicketingInfoProvider
    {
        public IList<PosTicketingAirlineInfo> GetPosAirlineTicketingInfo()
        {
            Utility.LogDiagnosticEntry("calling AirConfigurationService.GetPosTicketingAirlineInfo");
            PosTicketingAirlineInfo[] posTicketingAirlines;
            using (var client = new AirConfigurationServiceClient(KeyStore.ClientEndpoints.DataServicesIAirConfigurationService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirConfigurationServiceAddr)))
            {
                posTicketingAirlines = client.GetPosTicketingAirlineInfo();
            }
            Utility.LogDiagnosticEntry("call finished for AirConfigurationService.GetPosTicketingAirlineInfo");

            return posTicketingAirlines ?? new PosTicketingAirlineInfo[0];
        }

        public IList<PosInterAirlineAgreement> GetAirTicketingAgreements()
        {
            Utility.LogDiagnosticEntry("calling AirConfigurationService.GetAirTicketingAgreements");
            PosInterAirlineAgreement[] ticketingAgreement;
            using (var client = new AirConfigurationServiceClient(KeyStore.ClientEndpoints.DataServicesIAirConfigurationService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirConfigurationServiceAddr)))
            {
                ticketingAgreement = client.GetAirTicketingAgreements();
            }
            Utility.LogDiagnosticEntry("call finished for AirConfigurationService.GetAirTicketingAgreements");

            return ticketingAgreement ?? new PosInterAirlineAgreement[0];
        }
    }
}
