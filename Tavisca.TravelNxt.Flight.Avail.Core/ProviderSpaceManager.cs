﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Frameworks.Repositories;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ProviderSpaceManager : IProviderSpaceManager
    {
        #region Static Members

        protected const string AllSymbol = "*";

        protected static readonly object AirlineOperatingAirportLocker = new object();

        private ICacheProvider _cacheProvider;
        protected ICacheProvider CacheProvider
        {
            get
            {
                return _cacheProvider ??
                       (_cacheProvider =
                        RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.AirCore));
            }
        }

        #endregion

        #region Properties and Members

        private List<PosFareSourceStrategySetting> _posFareSourceStrategySettings;
        private ConcurrentDictionary<string, IList<string>> _airlineOperatingAirports;

        private readonly IContentManagerFactory _contentManagerFactory = RuntimeContext.Resolver.Resolve<IContentManagerFactory>();
        protected virtual IContentManager ContentManager
        {
            get { return _contentManagerFactory.GetContentManager(); }
        }


        protected virtual ITransformPassengerInfo GetSearchCriteriaTransform(string name)
        {
            return RuntimeContext.Resolver.Resolve<ITransformPassengerInfo>(name);
        }


        #endregion

        #region IProviderSpaceManager Members

        public virtual IList<ProviderSpace> LoadProviderSpaceForAvail(Requester requester,
                                                                    FlightSearchCriteria searchCriteria)
        {
            var requesterProviderSpaces = LoadProviderSpacesByRequester(requester, searchCriteria);

            var providerSpaceFilterProvider = RuntimeContext.Resolver.Resolve<IProviderSpaceFilterProvider>();

            return providerSpaceFilterProvider.FilterProviderSpaces(requesterProviderSpaces, searchCriteria);
        }

        public virtual IList<ProviderSpace> GetAllProviderSpaceForRequest(Requester requester)
        {
            var key = GetProviderSpacePlainKey(requester);

            List<ProviderSpace> providerSpaces;

            providerSpaces = CacheProvider.Get<List<ProviderSpace>>(KeyStore.CacheBucket.AirSettings, key);

            if (providerSpaces != null)
                return providerSpaces;

            //Get uncached data

            ICollection<SupplierFareSource> faresources;

            Utility.LogDiagnosticEntry("calling AirConfigurationService.GetAirSupplierFareSource");
            using (var client = new AirConfigurationServiceClient(KeyStore.ClientEndpoints.DataServicesIAirConfigurationService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirConfigurationServiceAddr)))
            {
                faresources = client.GetAirSupplierFareSource();
            }
            Utility.LogDiagnosticEntry("call finished for AirConfigurationService.GetAirSupplierFareSource");

            if (faresources == null || faresources.Count == 0)
            {
                var dex = new DataException("Faresources returned by the data service was null, there are probably no faresources configured for this accountid: " + Utility.GetCurrentContext().AccountId);

                var entry = dex.ToContextualEntry();

                entry.SeverityType = SeverityOptions.Critical;
                entry.PriorityType = PriorityOptions.Critical;

                Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.ExceptionCategories.Critical);

                throw new HandledException(dex);
            }

            providerSpaces = faresources.Select(x => new ProviderSpace(x.FareSourceId, x.SupplierName, requester,
                    x.SupplierFamilyName, x.EmulatedBy, x.OwnerId, ToProviderSpaceType(x.Type),
                    x.FareSourceConfigurations == null ? null : x.FareSourceConfigurations.Select(y => new ProviderSpaceConfiguration()
                    {
                        ConfigType = (Entities.ConfigurationType)((int)y.ConfigType),
                        ContractVersion = y.ContractVersion,
                        ServiceUri = y.ServiceUri
                    }), x.Attributes == null ? null : x.Attributes.Select(y => new KeyValuePair<string, string>(y.Name, y.Value))))
                .ToListBuffered(faresources.Count);

            CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, key, providerSpaces, new ExpirationSettings()
            {
                ExpirationType = ExpirationType.SlidingExpiration,
                SlidingExpirationInterval = new TimeSpan(
                        TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
            });

            return providerSpaces;
        }

        public virtual IDictionary<ProviderSpace, FlightSearchCriteria> ProviderSearchCriteriaStrategy(IList<ProviderSpace> providers, FlightSearchCriteria searchCriteria)
        {
            var retVal = new Dictionary<ProviderSpace, FlightSearchCriteria>(providers.Count);

            var timeWindowSetter = RuntimeContext.Resolver.Resolve<ITravelTimeWindowSetter>();

            foreach (var providerSpace in providers)
            {
                var airSearchCriterion = searchCriteria.DeepCopy();

                airSearchCriterion =
                    GetSearchCriteriaTransform(KeyStore.SingularityNameKeys.ChildPaxTransform).
                        GetTransformedSearchCriteria(airSearchCriterion, providerSpace);

                //airSearchCriterion.PassengerInfoSummary=UpdatePassengerTypeForPrivateFare(airSearchCriterion.PassengerInfoSummary, providerSpace);

                timeWindowSetter.SetTravelTimeWindows(airSearchCriterion, providerSpace);

                var providerSpacePreferences = GetPrefferedAirlines(providerSpace).ToListBuffered();

                //Missing configuration implies no exclusions
                if (providerSpacePreferences.Count == 0)
                {
                    retVal.Add(providerSpace, airSearchCriterion);
                    continue;
                }

                var requesterCode = providerSpace.Requester.GetRateCode();

                if (CheckAirDefaultSearchForRequesterCode(string.IsNullOrEmpty(requesterCode) ? AllSymbol : requesterCode,
                    providerSpacePreferences))
                {
                    retVal.Add(providerSpace, airSearchCriterion);
                }
                else
                {
                    var excludeFareSource = true;
                    for (var configCount = 0; configCount < providerSpacePreferences.Count; configCount++)
                    {
                        var config = providerSpacePreferences[configCount];
                        if (CheckRateCode(providerSpace.Requester.GetRateCode(), string.IsNullOrWhiteSpace(config.RateCode) ? string.Empty : config.RateCode.Trim()))
                        {
                            if (ParseMarketType(config.MarketType) == AirMarketType.None)
                            {
                                if (config.AirLineCode.Equals(AllSymbol))
                                {
                                    excludeFareSource = true;
                                    break;
                                }

                                airSearchCriterion.FlightTravelPreference.AirlinesPreference.ProhibitedAirlines.Add(
                                    config.AirLineCode);
                            }
                            else if (!config.AirLineCode.Equals(AllSymbol) &&
                                     ((config.CheckMarket &&
                                       DoesAirlineFlyForAirports(config.AirLineCode, airSearchCriterion))
                                      || (!config.CheckMarket)))
                            {
                                if (IsSearchSegmentMatchesMarketType(airSearchCriterion.SearchSegments,
                                                                     ParseMarketType(config.MarketType), providerSpace.Requester))
                                {
                                    airSearchCriterion.FlightTravelPreference.AirlinesPreference.PermittedAirlines.Add(
                                    config.AirLineCode);

                                    excludeFareSource = false;
                                }
                            }
                        }
                    }

                    if (excludeFareSource)
                    {
                        if (providerSpacePreferences.Any(t => t.AirLineCode.Equals(AllSymbol,
                            StringComparison.OrdinalIgnoreCase)))
                            excludeFareSource = false;
                    }

                    if (!excludeFareSource)
                        retVal.Add(providerSpace, airSearchCriterion);
                }
            }

            PreferExcludedAirlines(retVal);

            return retVal;
        }

        private IList<PaxTypeQuantity> UpdatePassengerTypeForPrivateFare(IList<PaxTypeQuantity> passengerInfoSummary, ProviderSpace providerSpace)
        {
            string isPrivateFareOnly;
            providerSpace.AdditionalInfo.TryGetValue(KeyStore.ProviderSpaceAttributes.IsPrivateFareOnly, out isPrivateFareOnly);
            IList<PaxTypeQuantity> updatedPassengerInfoSummary = passengerInfoSummary ?? passengerInfoSummary.Select(x => x.DeepCopy()).ToList();

            bool isPrivateFare;
            bool.TryParse(isPrivateFareOnly, out isPrivateFare);
            
            if (isPrivateFare && updatedPassengerInfoSummary!=null)
            {
                foreach(var passenger in updatedPassengerInfoSummary)
                {
                    string paxCode = null;
                    providerSpace.AdditionalInfo.TryGetValue(passenger.PassengerType.ToString() + KeyStore.ProviderSpaceAttributes.PrivateCode, out paxCode);
                    if (!string.IsNullOrEmpty(paxCode))
                        passenger.PassengerType = PassengerType.Mapped;
                }
            }
            return updatedPassengerInfoSummary;
        }

        public virtual ProviderSpace Get(Requester requester, int id)
        {
            var providerSpaces = GetAllProviderSpaceForRequest(requester);

            return providerSpaces == null ? null : providerSpaces.FirstOrDefault(x => x.ID == id);
        }

        #endregion

        #region Protected Members

        protected virtual IList<ProviderSpace> LoadProviderSpacesByRequester(Requester requester, FlightSearchCriteria searchCriteria)
        {
            var key = GetProviderSpaceKey(requester);

            IList<ProviderSpace> providerSpaces = null;
            providerSpaces = CacheProvider.Get<List<ProviderSpace>>(KeyStore.CacheBucket.AirSettings, key);
            if (providerSpaces != null)
                return providerSpaces;

            providerSpaces = GetAllProviderSpaceForRequest(requester);

            providerSpaces = LoadContracts(requester, providerSpaces, searchCriteria);

            CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, key, providerSpaces,
                                 new ExpirationSettings()
                                 {
                                     ExpirationType = ExpirationType.SlidingExpiration,
                                     SlidingExpirationInterval =
                                         new TimeSpan(
                                         TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                 });

            return providerSpaces;
        }

        protected string GetProviderSpaceKey(Requester requester)
        {
            return string.Format(KeyStore.CacheKeys.ProviderSpaces,
                                    requester.POS.ID.ToString(CultureInfo.InvariantCulture),
                                    requester.ID.ToString(CultureInfo.InvariantCulture));
        }

        protected string GetProviderSpacePlainKey(Requester requester)
        {
            return string.Format(KeyStore.CacheKeys.ProviderSpacePlain,
                                    requester.POS.ID.ToString(CultureInfo.InvariantCulture),
                                    requester.ID.ToString(CultureInfo.InvariantCulture));
        }

        protected bool DoesAirlineFlyForAirports(string airlineCode, FlightSearchCriteria searchCriteria)
        {
            var operatingAirports = GetAllAirlineOperatingAirports();

            IList<string> airports;
            if (!operatingAirports.TryGetValue(airlineCode, out airports))
                return false;

            foreach (var searchSegment in searchCriteria.SearchSegments)
            {
                var arrivalAirports = new List<string>();
                var departureAirports = new List<string>();

                PopulateArrivalDepartureAirports(searchSegment, arrivalAirports, departureAirports);

                var arrivalFound = arrivalAirports.Any(
                    x =>
                    airports.Any(y => x.Equals(y, StringComparison.OrdinalIgnoreCase)));

                var departureFound = departureAirports.Any(
                    x =>
                    airports.Any(y => x.Equals(y, StringComparison.OrdinalIgnoreCase)));

                if (!(arrivalFound && departureFound))
                    return false;
            }

            return true;
        }

        private void PopulateArrivalDepartureAirports(SearchSegment searchSegment, List<string> arrivalAirports, List<string> departureAirports)
        {
            var isArrivalCity = searchSegment.IsArrivalSpecCity();
            if (isArrivalCity)
                arrivalAirports.AddRange(
                    ContentManager.GetAirportsForCity(searchSegment.GetArrivalLocation()).Select(x => x.Code));
            else
                arrivalAirports.Add(searchSegment.GetArrivalLocation());

            var altArrivalCodes = GetAltAirportCodes(searchSegment.ArrivalAlternateAirportInformation,
                isArrivalCity ? null : searchSegment.GetArrivalLocation());
            if (altArrivalCodes != null)
                arrivalAirports.AddRange(altArrivalCodes);

            var isDepartureCity = searchSegment.IsDepartureSpecCity();
            if (isDepartureCity)
                departureAirports.AddRange(
                    ContentManager.GetAirportsForCity(searchSegment.GetDepartureLocation()).Select(x => x.Code));
            else
                departureAirports.Add(searchSegment.GetDepartureLocation());

            var altDepartureCodes = GetAltAirportCodes(searchSegment.DepartureAlternateAirportInformation,
                isDepartureCity ? null : searchSegment.GetDepartureLocation());
            if (altDepartureCodes != null)
                departureAirports.AddRange(altDepartureCodes);
        }

        private static void PreferExcludedAirlines(IDictionary<ProviderSpace, FlightSearchCriteria> providerCriteriaMapping)
        {
            foreach (var criteria in providerCriteriaMapping.Values)
            {
                if (criteria.FlightTravelPreference == null ||
                    criteria.FlightTravelPreference.AirlinesPreference == null) continue;

                var airlinePreference = criteria.FlightTravelPreference.AirlinesPreference;
                airlinePreference.ProhibitedAirlines.ForEach(
                    prohibitedAirline =>
                    {
                        if (airlinePreference.PermittedAirlines != null)
                            airlinePreference.PermittedAirlines.Remove(prohibitedAirline);
                        if (airlinePreference.PreferredAirlines != null)
                            airlinePreference.PreferredAirlines.Remove(prohibitedAirline);
                    });
            }
        }

        private IEnumerable<string> GetAltAirportCodes(AlternateAirportInformation alternateAirportInformation, string referenceAirport = null)
        {
            if (alternateAirportInformation != null && !alternateAirportInformation.IsEmpty())
            {
                if (alternateAirportInformation.AirportCodes != null &&
                    alternateAirportInformation.AirportCodes.Count > 0)
                    return alternateAirportInformation.AirportCodes;

                if (!string.IsNullOrWhiteSpace(referenceAirport))
                    return
                        ContentManager.GetNearbyAirports(referenceAirport, false, alternateAirportInformation.RadiusKm)
                            .Select(x => x.Code).ToListBuffered();
            }

            return new string[0];
        }

        protected bool IsSearchSegmentMatchesMarketType(ICollection<SearchSegment> searchSegments, AirMarketType airMarketType, Requester requester)
        {
            if (airMarketType == AirMarketType.All)
                return true;

            if (airMarketType == AirMarketType.International || airMarketType == AirMarketType.Domestic)
            {
                if (requester.Address == null)
                    return true; //TODO: Which address should be pulled? affiliates or agency's?

                var isInternationalSearch = InternationalSearchCheckProvider.IsSearchInternational(searchSegments,
                    ContentManager);

                if (isInternationalSearch && airMarketType == AirMarketType.International)
                    return true;

                if (!isInternationalSearch && airMarketType == AirMarketType.Domestic)
                    return true;

                return false;
            }
            return true;
        }

        protected virtual bool CheckRateCode(string requesterCode, string configRateCode)
        {
            var requesterRateCode = string.IsNullOrEmpty(requesterCode) ? AllSymbol : requesterCode;
            if (configRateCode.Equals(AllSymbol) || requesterRateCode.Equals(configRateCode, StringComparison.OrdinalIgnoreCase))
                return true;

            return false;
        }

        protected virtual bool CheckAirDefaultSearchForRequesterCode(string rateCode, List<PosFareSourceStrategySetting> airFareSourceConfigs)
        {
            var isRateCodeMatched = false;

            foreach (var posFareSourceStrategySetting in airFareSourceConfigs)
            {
                if (ParseMarketType(posFareSourceStrategySetting.MarketType) == AirMarketType.None)
                    return false;

                if ((posFareSourceStrategySetting.RateCode.Equals(rateCode, StringComparison.OrdinalIgnoreCase)
                    || posFareSourceStrategySetting.RateCode.Trim().Equals(AllSymbol)) &&
                    posFareSourceStrategySetting.AirLineCode.Trim().Equals(AllSymbol))
                    isRateCodeMatched = true;
            }

            return isRateCodeMatched;
        }

        protected enum AirMarketType
        {
            All,
            Airline,
            Domestic,
            International,
            None
        }

        protected virtual AirMarketType ParseMarketType(string marketType)
        {
            switch (marketType.ToUpper().Trim())
            {
                case "AIRLINE":
                    return AirMarketType.Airline;
                case "ALL":
                    return AirMarketType.All;
                case "DOMESTIC":
                    return AirMarketType.Domestic;
                case "INTERNATIONAL":
                    return AirMarketType.International;
                case "NONE":
                    return AirMarketType.None;
                default:
                    return AirMarketType.All;
            }
        }

        protected virtual IEnumerable<PosFareSourceStrategySetting> GetPrefferedAirlines(ProviderSpace providerSpace)
        {
            var allList = GetAllPrefferedAirlines();

            return allList.Where(x => x.PosID == providerSpace.Requester.POS.ID &&
                               x.FareSourceID == providerSpace.ID);
        }

        protected virtual IEnumerable<PosFareSourceStrategySetting> GetAllPrefferedAirlines()
        {
            if (_posFareSourceStrategySettings != null)
                return _posFareSourceStrategySettings;

            _posFareSourceStrategySettings =
                CacheProvider.Get<List<PosFareSourceStrategySetting>>(KeyStore.CacheBucket.AirSettings,
                                                                       KeyStore.CacheKeys.
                                                                           PosFareSourceStrategySetting);

            if (_posFareSourceStrategySettings != null)
                return _posFareSourceStrategySettings;

            var factory = RuntimeContext.Resolver.Resolve<IRepositoryFactory>();

            using (var repository = factory.GetRepository(KeyStore.Repositories.AirConfigurationReadOnly))
            {
                _posFareSourceStrategySettings = repository.All<PosFareSourceStrategySetting>().ToListBuffered()
                    .Select(x => new PosFareSourceStrategySetting()
                    {
                        AirLineCode = x.AirLineCode == null ? null : x.AirLineCode.Trim(),
                        CheckMarket = x.CheckMarket,
                        ID = x.ID,
                        FareSourceID = x.FareSourceID,
                        MarketType = x.MarketType == null ? null : x.MarketType.Trim(),
                        PosID = x.PosID,
                        RateCode = x.RateCode == null ? null : x.RateCode.Trim()
                    }).ToListBuffered();
            }


            CacheProvider.Insert(KeyStore.CacheBucket.AirSettings,
                                 KeyStore.CacheKeys.PosFareSourceStrategySetting, _posFareSourceStrategySettings,
                                 new ExpirationSettings()
                                 {
                                     ExpirationType = ExpirationType.SlidingExpiration,
                                     SlidingExpirationInterval =
                                         new TimeSpan(
                                         TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                 });

            return _posFareSourceStrategySettings;
        }

        protected virtual IDictionary<string, IList<string>> GetAllAirlineOperatingAirports()
        {
            if (_airlineOperatingAirports != null)
                return _airlineOperatingAirports;

            _airlineOperatingAirports =
                CacheProvider.Get<ConcurrentDictionary<string, IList<string>>>(KeyStore.CacheBucket.AirSettings,
                                                                      KeyStore.CacheKeys.AirlineOperatingAirports);

            if (_airlineOperatingAirports != null)
                return _airlineOperatingAirports;

            lock (AirlineOperatingAirportLocker)
            {
                if (_airlineOperatingAirports != null)
                    return _airlineOperatingAirports;

                var factory = RuntimeContext.Resolver.Resolve<IRepositoryFactory>();

                using (var repository = factory.GetRepository(KeyStore.Repositories.AirConfigurationReadOnly))
                {

                    var operatingAirports = repository.All<AirlineOperatingAirport>();

                    var airlineOperatingAirports = new ConcurrentDictionary<string, IList<string>>();

                    foreach (var airlineOperatingAirport in operatingAirports)
                    {
                        airlineOperatingAirports.TryAdd(airlineOperatingAirport.AirlineCode,
                                                         new List<string>() { airlineOperatingAirport.AirportCode });
                    }
                    _airlineOperatingAirports = airlineOperatingAirports;
                }

                CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.AirlineOperatingAirports,
                                     _airlineOperatingAirports, new ExpirationSettings()
                                                                    {
                                                                        ExpirationType =
                                                                            ExpirationType.SlidingExpiration,
                                                                        SlidingExpirationInterval =
                                                                            new TimeSpan(
                                                                            TravelNxt.Common.Settings.SettingManager.
                                                                                CacheExpirationHours, 0, 0)
                                                                    });
            }

            return _airlineOperatingAirports;
        }

        protected virtual IList<ProviderSpace> LoadContracts(Requester requester, IList<ProviderSpace> providerSpaces, FlightSearchCriteria searchCriteria)
        {
            var contractManager = RuntimeContext.Resolver.Resolve<IContractManager>();

            providerSpaces.ForEach(
                providerSpace =>
                providerSpace.Contracts = contractManager.LoadContractsForProvider(providerSpace.ID, searchCriteria).ToListBuffered());

            return providerSpaces;
        }

        protected ProviderSpaceType ToProviderSpaceType(SupplierFareSourceType supplierFareSourceType)
        {
            switch (supplierFareSourceType)
            {
                case SupplierFareSourceType.All:
                    return ProviderSpaceType.All;
                case SupplierFareSourceType.International:
                    return ProviderSpaceType.International;
                case SupplierFareSourceType.Domestic:
                    return ProviderSpaceType.Domestic;
                default:
                    return ProviderSpaceType.All;
            }
        }

        #endregion
    }
}