﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;
using Fare = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Fare;
using FareComponent = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareComponent;
using FareComponentType = Tavisca.TravelNxt.Flight.Entities.FareComponentType;
using PassengerType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.PassengerType;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    internal static class PassengerFareBreakupTranslator
    {

        internal static Dictionary<string, Entities.PassengerType> PaxTypeMapping = new Dictionary<string, Entities.PassengerType> { { "ITX", Entities.PassengerType.Adult }, { "INN", Entities.PassengerType.Child }, { "ITF", Entities.PassengerType.Infant } };
        internal static List<PassengerFare> GetMappedPassengerFares(this PassengerFareBreakup passengerFareBreakup, Fare fare, IList<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace, bool isMilitaryFare)
        {
            var matchedPassengerTypeQuantities = passengerFareBreakup.GetMatchingPassengerTypesQuantitiesForPassengerFare(passengerTypeQuantities, providerSpace, isMilitaryFare);

            var mappedPassengerFares = new List<PassengerFare>(matchedPassengerTypeQuantities == null ? 3 : matchedPassengerTypeQuantities.Count);

            if (matchedPassengerTypeQuantities != null && matchedPassengerTypeQuantities.Count > 0 
                && matchedPassengerTypeQuantities.Sum(passengerTypeQuantity => passengerTypeQuantity.Quantity) == passengerFareBreakup.PassengerTypeQuantity.Quantity)
            {
                var totalPassengerCount = passengerTypeQuantities.Sum(passengerTypeQuantity => passengerTypeQuantity.Quantity);
                var taxes = CreateAdditionalTax(fare.Taxes, totalPassengerCount, FareComponentType.Tax);
                taxes.AddRange(CreateAdditionalTax(fare.Fees, totalPassengerCount, FareComponentType.Tax));
                var perPassengerSupplierDiscounts = CreatePerPassengerSupplierDiscounts(fare.Discounts, totalPassengerCount, providerSpace);

                mappedPassengerFares.AddRange(matchedPassengerTypeQuantities.Select(matchedPassengerTypeQuantity => new PassengerFare
                {
                    PassengerType = GetPassengerType(matchedPassengerTypeQuantity.PassengerType,passengerFareBreakup.PassengerTypeQuantity.MappedPaxTypeCode),
                    Quantity = matchedPassengerTypeQuantity.Quantity,
                    Discounts = perPassengerSupplierDiscounts,
                    BaseAmount = new Entities.Money(passengerFareBreakup.BaseAmount.Amount, passengerFareBreakup.BaseAmount.CurrencyCode),
                    Taxes = passengerFareBreakup.Taxes.ToTaxes(taxes)
                }));
            }
            return mappedPassengerFares;
        }

        private static Entities.PassengerType GetPassengerType(Entities.PassengerType passengerType, string mappedPaxTypeCode)
        {
            if (passengerType == Entities.PassengerType.Mapped && !string.IsNullOrEmpty(mappedPaxTypeCode) && PaxTypeMapping.ContainsKey(mappedPaxTypeCode))
                return PaxTypeMapping[mappedPaxTypeCode];
            return passengerType;
        }

        private static List<Entities.FareComponent> CreateAdditionalTax(IEnumerable<Data.Services.AirFareSearchV1.FareComponent> fareComponents, int totalPassengerCount, FareComponentType fareComponentType)
        {
            if (fareComponents == null)
                return new List<Entities.FareComponent>();

            return fareComponents.Select(fareComponent => new Entities.FareComponent()
                                                              {
                                                                  Code = fareComponent.Code,
                                                                  Description = fareComponent.Description,
                                                                  FareComponentType = fareComponentType,
                                                                  Value =
                                                                      fareComponent.Amount > 0
                                                                          ? new Entities.Money(
                                                                                fareComponent.Amount/totalPassengerCount,
                                                                                fareComponent.CurrencyCode)
                                                                          : Entities.Money.Zero
                                                              }).ToListBuffered();
        }

        private static List<Entities.FareComponent> CreatePerPassengerSupplierDiscounts(List<Data.Services.AirFareSearchV1.FareComponent> discounts, int totalPassengerCount, ProviderSpace providerSpace)
        {
            var showDiscounts = false;
            string showDiscountAttribute;
            if (providerSpace.AdditionalInfo.TryGetValue(KeyStore.ProviderSpaceAttributes.ShowDiscount, out showDiscountAttribute))
                Boolean.TryParse(showDiscountAttribute, out showDiscounts);
            
            var perPassengerDiscounts = new List<Entities.FareComponent>();
            if (discounts != null && discounts.Count != 0 && showDiscounts)
            {
                var amount = discounts.Sum(component => component.Amount / totalPassengerCount);
                if (amount != 0)
                {
                    perPassengerDiscounts = new List<Entities.FareComponent>
                                                {
                                                    new Entities.FareComponent()
                                                        {
                                                            FareComponentType = FareComponentType.Discount,
                                                            Value =
                                                                new Entities.Money(amount,
                                                                                   discounts.First().CurrencyCode),
                                                            OwnerId =
                                                                providerSpace.OwnerId.HasValue
                                                                    ? providerSpace.OwnerId.Value
                                                                    : HierarchyFacade.GetClientId(
                                                                        Utility.GetCurrentContext().AccountId)
                                                        }
                                                };
                }

            }
            return perPassengerDiscounts;
        }

        internal static List<PaxTypeQuantity> GetMatchingPassengerTypesQuantitiesForPassengerFare(this PassengerFareBreakup passengerFareBreakup, IList<PaxTypeQuantity> paxTypeQuantities, ProviderSpace providerSpace, bool isMilitaryFare)
        {
            var matchedPaxTypeQuantities = new List<PaxTypeQuantity>();

            foreach (var paxTypeQuantity in paxTypeQuantities)
            {
                if (passengerFareBreakup.PassengerTypeQuantity.PassengerType == PassengerType.Mapped &&
                    !string.IsNullOrEmpty(passengerFareBreakup.PassengerTypeQuantity.MappedPaxTypeCode) &&
                    passengerFareBreakup.PassengerTypeQuantity.MappedPaxTypeCode.Equals(GetMappedPassengerTypeCode(paxTypeQuantity, providerSpace, isMilitaryFare), StringComparison.OrdinalIgnoreCase))
                {
                    matchedPaxTypeQuantities.Add(paxTypeQuantity);
                }
                else if (passengerFareBreakup.PassengerTypeQuantity.PassengerType != PassengerType.Mapped && paxTypeQuantity.PassengerType == passengerFareBreakup.PassengerTypeQuantity.PassengerType.Cast())
                {
                    matchedPaxTypeQuantities.Add(paxTypeQuantity);
                }
            }
            return matchedPaxTypeQuantities;
        }

        public static string GetMappedPassengerTypeCode(PaxTypeQuantity passengerTypeQuantity,
                                                        ProviderSpace providerSpace, bool isMilitaryFare)
        {
            string mappedCode;

            providerSpace.AdditionalInfo.TryGetValue(
                passengerTypeQuantity.PassengerType + KeyStore.ProviderSpaceAttributes.PrivateCode, out mappedCode);

            return mappedCode;
        }
    }
}
