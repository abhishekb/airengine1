﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.Frameworks.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.Singularity;
using System.Threading;
using Tavisca.TravelNxt.Flight.Settings;
using Tavisca.TravelNxt.Common.Caching;
using System.Collections.Concurrent;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class AirlineInterlineAgreementProvider : IAirlineInterlineAgreementProvider
    {
        private ICacheProvider _cacheProvider;
        protected ICacheProvider CacheProvider
        {
            get
            {
                return _cacheProvider ??
                       (_cacheProvider =
                        RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.AirCore));
            }
        }

        public bool IsRecommendationTicketable(FlightRecommendation recommendation, Requester requester)
        {
            var carriers = new HashSet<string>();
            foreach (var segment in recommendation.Legs.SelectMany(x => x.Segments))
            {
                carriers.Add(segment.MarketingAirline.Code);
            }

            var carrierType = CheckTickelLessCarriers(carriers);

            if (carrierType == TicketLessCarrier.OnlyTicketLess)
                return true;

            if (carrierType == TicketLessCarrier.TicketLessWithEticketCarrier)
                return false;

            var carriersList = carriers.ToListBuffered(carriers.Count);

            if (!CheckAirlinesAllowedOnPOS(carriersList, requester, recommendation))
                return false;

            return CheckTicketingAgreement(carriersList, recommendation, requester);
        }

        #region Check Interline TicketingAgreement

        protected virtual bool CheckTicketingAgreement(List<string> carriers, FlightRecommendation recommendation, Requester requester)
        {
            var posId = requester.POS.ID;
            var providerId = recommendation.ProviderSpace.ID;

            IDictionary<int, IDictionary<string, IList<InterlineAgreement>>> posTicketAgreements;
            IDictionary<string, IList<InterlineAgreement>> fareSourceTicketAgreements;

            if (TicketingAgreementMap.TryGetValue(posId, out posTicketAgreements) &&
                posTicketAgreements.TryGetValue(providerId, out fareSourceTicketAgreements))
            {
                foreach (var airline in carriers)
                {
                    IList<InterlineAgreement> interlineAgreement;

                    if (!fareSourceTicketAgreements.TryGetValue(airline, out interlineAgreement))
                        return false;

                    var electronicAgreement = GetInterAirlineElectronicAgreement(interlineAgreement);

                    if (electronicAgreement == null)
                        return false;

                    if (electronicAgreement.Airlines.Contains(airline))
                        continue;

                    return false;
                }
            }

            return true;
        }

        protected InterlineAgreement GetInterAirlineElectronicAgreement(IList<InterlineAgreement> inteAirlineAgreements)
        {
            return inteAirlineAgreements
                .FirstOrDefault(temp => temp.AgreementType.Equals("E", StringComparison.Ordinal));
        }

        protected virtual IDictionary<int, IDictionary<int, IDictionary<string, IList<InterlineAgreement>>>> TicketingAgreementMap
        {
            get
            {
                IDictionary<int, IDictionary<int, IDictionary<string, IList<InterlineAgreement>>>> value;
                value = CacheProvider.Get<IDictionary<int, IDictionary<int, IDictionary<string, IList<InterlineAgreement>>>>>
                        (KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.TicketingAgreementMap);

                if (value != null)
                    return value;

                var posTicketingAgreements = LoadInterLineAgreement();
                CacheProvider.Insert(KeyStore.CacheBucket.AirSettings,
                                         KeyStore.CacheKeys.TicketingAgreementMap, posTicketingAgreements,
                                         new ExpirationSettings()
                                             {
                                                 ExpirationType = ExpirationType.SlidingExpiration,
                                                 SlidingExpirationInterval =
                                                     new TimeSpan(
                                             TravelNxt.Common.Settings.SettingManager.
                                                 CacheExpirationHours, 0, 0)
                                             });

                return posTicketingAgreements;
            }
        }

        protected virtual IDictionary<int, IDictionary<int, IDictionary<string, IList<InterlineAgreement>>>> LoadInterLineAgreement()
        {
            var ticketingAgreement = GetAirlineTicketingInfoProvider().GetAirTicketingAgreements();

            var interAirlineAgreements = new ConcurrentDictionary<int, IDictionary<int, IDictionary<string, IList<InterlineAgreement>>>>();

            foreach (var agreement in ticketingAgreement)
            {
                var posId = agreement.PosId;

                interAirlineAgreements.TryAdd(posId,
                                            new ConcurrentDictionary<int, IDictionary<string, IList<InterlineAgreement>>>());

                var fareSourceDictionary =
                    (ConcurrentDictionary<int, IDictionary<string, IList<InterlineAgreement>>>)interAirlineAgreements[posId];

                var fareSourceId = agreement.FareSourceId;

                fareSourceDictionary.TryAdd(fareSourceId, new ConcurrentDictionary<string, IList<InterlineAgreement>>());

                var codes = (ConcurrentDictionary<string, IList<InterlineAgreement>>)fareSourceDictionary[fareSourceId];

                var agreements = GetSplitAgreements(agreement.Agreements);

                var interAirlineAgr = new InterlineAgreement
                    {
                        Airlines = agreements,
                        AgreementType = agreement.AgreementType.ToUpper()
                    };

                var airlineCode = agreement.AirlineCode;

                IList<InterlineAgreement> agreementsList;

                if (codes.TryGetValue(airlineCode, out agreementsList))
                {
                    agreementsList.Add(interAirlineAgr);
                }
                else
                {
                    codes.TryAdd(airlineCode, new List<InterlineAgreement> { interAirlineAgr });
                }
            }

            return interAirlineAgreements;
        }

        protected IList<string> GetSplitAgreements(string agreements)
        {
            if (string.IsNullOrEmpty(agreements))
                return new List<string>();

            var airlines = agreements.Split('|');

            if (airlines.Length <= 0)
            {
                return new List<string>();
            }
            return new List<string>(airlines);
        }

        #endregion

        #region Check Airlines Allowed On POS

        protected virtual bool CheckAirlinesAllowedOnPOS(IList<string> carriers, Requester requester,
            FlightRecommendation recommendation)
        {
            IDictionary<int, IList<string>> posFaresourceCarrier;
            IList<string> faresourceCarriers;
            if (POSFaresourceCarriers.TryGetValue(requester.POS.ID, out posFaresourceCarrier) &&
                posFaresourceCarrier.TryGetValue(recommendation.ProviderSpace.ID, out faresourceCarriers))
            {
                for (var i = 0; i < carriers.Count; i++)
                {
                    if (faresourceCarriers.Contains(carriers[i]))
                        continue;

                    return false;
                }
            }
            return true;
        }

        protected virtual IDictionary<int, IDictionary<int, IList<string>>> POSFaresourceCarriers
        {
            get
            {
                IDictionary<int, IDictionary<int, IList<string>>> value;

                value =
                    CacheProvider.Get<ConcurrentDictionary<int, IDictionary<int, IList<string>>>>(
                        KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.PosFaresourceCarriers);

                if (value != null)
                {
                    return value;
                }

                var posTicketingAirlineInfo = LoadPOSTicketingAirlineInfo();
                CacheProvider.Insert(KeyStore.CacheBucket.AirSettings,
                                         KeyStore.CacheKeys.PosFaresourceCarriers, posTicketingAirlineInfo,
                                         new ExpirationSettings()
                                             {
                                                 ExpirationType = ExpirationType.SlidingExpiration,
                                                 SlidingExpirationInterval =
                                                     new TimeSpan(
                                             TravelNxt.Common.Settings.SettingManager.CacheExpirationHours,
                                             0, 0)
                                             });

                return posTicketingAirlineInfo;
            }
        }

        protected virtual IDictionary<int, IDictionary<int, IList<string>>> LoadPOSTicketingAirlineInfo()
        {
            var posFareSourceAirline = new ConcurrentDictionary<int, IDictionary<int, IList<string>>>();

            var posTicketingAirlines = GetAirlineTicketingInfoProvider().GetPosAirlineTicketingInfo();

            foreach (var posAirline in posTicketingAirlines)
            {
                IDictionary<int, IList<string>> fareSourceAirline;
                IList<string> airlines;
                if (!posFareSourceAirline.TryGetValue(posAirline.PosId, out fareSourceAirline))
                {
                    posFareSourceAirline[posAirline.PosId] =
                        (fareSourceAirline = new ConcurrentDictionary<int, IList<string>>());
                }

                if (!fareSourceAirline.TryGetValue(posAirline.FareSourceId, out airlines))
                {
                    fareSourceAirline[posAirline.FareSourceId] = (airlines = new List<string>());
                }

                airlines.Add(posAirline.AirlineCode);
            }

            return posFareSourceAirline;
        }

        #endregion

        protected TicketLessCarrier CheckTickelLessCarriers(IEnumerable<string> carriers)
        {
            var allTrue = false;
            var ticketLessSetting = SettingManager.TicketlessCarriers.Split(',');

            foreach (var carrier in carriers)
            {
                if (ticketLessSetting.Any(x => x.Equals(carrier, StringComparison.OrdinalIgnoreCase)))
                {
                    allTrue = true;
                }
                else if (allTrue)
                {
                    //Mixed airline case with TicketLess Carriers not allowed
                    return TicketLessCarrier.TicketLessWithEticketCarrier;
                }
            }

            if (!allTrue)
                return TicketLessCarrier.OnlyEticketingCarrier;

            return TicketLessCarrier.OnlyTicketLess;
        }

        protected virtual IAirlineTicketingInfoProvider GetAirlineTicketingInfoProvider()
        {
            return RuntimeContext.Resolver.Resolve<IAirlineTicketingInfoProvider>();
        }

        protected enum TicketLessCarrier
        {
            OnlyTicketLess,
            TicketLessWithEticketCarrier,
            OnlyEticketingCarrier
        }

        [Serializable]
        [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
        public class InterlineAgreement
        {
            public string AgreementType { get; set; }

            public IList<string> Airlines { get; set; }
        }
    }
}
