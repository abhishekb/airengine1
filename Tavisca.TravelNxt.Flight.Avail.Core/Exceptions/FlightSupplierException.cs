﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Exceptions
{
    [Serializable]
    public class FlightSupplierException : Exception
    {
        public FlightSupplierException()
        { }

        public FlightSupplierException(string message)
            : base(message)
        { }

        public FlightSupplierException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
