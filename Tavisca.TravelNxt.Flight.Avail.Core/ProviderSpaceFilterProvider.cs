﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ProviderSpaceFilterProvider : IProviderSpaceFilterProvider
    {
        #region IProviderSpaceFilterProvider Members

        public IList<ProviderSpace> FilterProviderSpaces(IList<ProviderSpace> providerSpaces, FlightSearchCriteria searchCriteria)
        {
            if (providerSpaces == null || providerSpaces.Count == 0)
                return providerSpaces;

            if (searchCriteria == null)
                throw new ArgumentNullException("searchCriteria");

            var contentManager = RuntimeContext.Resolver.Resolve<IContentManagerFactory>().GetContentManager();

            if (searchCriteria.SearchSegments == null || searchCriteria.SearchSegments.Count == 0)
                throw new ArgumentException("SearchSegments cannot be null or empty in search criteria.", "searchCriteria");

            var filteredProviderspaces = FilterOnCountryPairRestrictions(providerSpaces, searchCriteria, contentManager);

            var maxCount = providerSpaces.Count;

            var isInternationalSearch = IsInternationalSearch(searchCriteria, contentManager);

            if (!isInternationalSearch)
                return filteredProviderspaces.Where(x => x.Type != ProviderSpaceType.International)
                .ToListBuffered(maxCount);

            return filteredProviderspaces.Where(x => x.Type != ProviderSpaceType.Domestic)
                .ToListBuffered(maxCount);
        }

        #endregion

        #region Filtering on Country Pair Restrictions

        protected virtual IEnumerable<ProviderSpace> FilterOnCountryPairRestrictions(
            IEnumerable<ProviderSpace> providerSpaces, FlightSearchCriteria searchCriteria,
            IContentManager contentManager)
        {
            var countryPair = GetCriterionCountryPair(searchCriteria, contentManager);

            return providerSpaces.Where(providerSpace => IsApplicableForCountryPair(providerSpace, countryPair));
        }

        protected virtual bool IsApplicableForCountryPair(ProviderSpace providerSpace, CountryPair searchedCountryPair)
        {
            var setting = GetProviderspaceCountryPairRestrictionSetting(providerSpace);

            var countryPairs = ParseProviderSpaceSetting(setting);

            if (countryPairs == null || countryPairs.Count == 0)
            {
                return true;
            }

            return countryPairs.Any(searchedCountryPair.Equals);
        }

        protected virtual ICollection<CountryPair> ParseProviderSpaceSetting(string setting)
        {
            if (string.IsNullOrWhiteSpace(setting))
                return new List<CountryPair>();

            var splitSetting = setting.Split(',');

            var retVal = new List<CountryPair>(splitSetting.Length);

            foreach (var pair in splitSetting)
            {
                if (string.IsNullOrWhiteSpace(pair))
                    continue;

                var splitPair = pair.Split('-');

                var origin = splitPair[0];

                var destination = splitPair.Length > 1 ? splitPair[1] : null;

                retVal.Add(new CountryPair(origin, destination));
            }
            retVal.TrimExcess();

            return retVal;
        }

        protected virtual string GetProviderspaceCountryPairRestrictionSetting(ProviderSpace providerSpace)
        {
            string setting;
            providerSpace.AdditionalInfo.TryGetValue(KeyStore.ProviderSpaceAttributes.CountryPairRestriction,
                                                     out setting);

            return setting;
        }

        protected CountryPair GetCriterionCountryPair(FlightSearchCriteria searchCriteria, IContentManager contentManager)
        {
            var searchSegments = searchCriteria.SearchSegments;

            var departure = searchSegments[0].GetDepartureLocation();
            var isDepartureCity = searchSegments[0].IsDepartureSpecCity();

            string arrival; //connection preference if any are ignored.
            
            bool isArrivalCity;

            var isMultiCity = false;

            if (searchSegments.Count == 2 && //round trip
                departure.Equals(searchSegments[1].GetArrivalLocation(), StringComparison.Ordinal))
            {
                arrival = searchSegments[0].GetArrivalLocation();
                isArrivalCity = searchSegments[0].IsArrivalSpecCity();
            }
            else if (searchSegments.Count == 1) //one-way
            {
                arrival = searchSegments[0].GetArrivalLocation();
                isArrivalCity = searchSegments[0].IsArrivalSpecCity();
            }
            else //multi-city
            {
                arrival = searchSegments[0].GetArrivalLocation();
                isArrivalCity = searchSegments[0].IsArrivalSpecCity();
                isMultiCity = true;
            }

            var departureCountry = isDepartureCity
                ? contentManager.GetCitiesByCode(departure).First().CountryCode
                : contentManager.GetAirportByCode(departure).City.CountryCode;

            var arrivalCountry = isArrivalCity
                ? contentManager.GetCitiesByCode(arrival).First().CountryCode
                : contentManager.GetAirportByCode(arrival).City.CountryCode;

            return new CountryPair(departureCountry, arrivalCountry, isMultiCity);
        }

        protected sealed class CountryPair
        {
            public bool IsMultiCity { get; private set; }
            public string Origin { get; private set; }
            public string Destination { get; private set; }

            public CountryPair(string origin, string destination, bool isMultiCity) 
                : this(origin, destination)
            {
                IsMultiCity = isMultiCity;
            }

            public CountryPair(string origin, string destination)
            {
                if (!string.IsNullOrWhiteSpace(origin) && !origin.Equals("*", StringComparison.InvariantCulture))
                    Origin = origin.Trim().ToLowerInvariant();

                if (!string.IsNullOrWhiteSpace(destination) && !destination.Equals("*", StringComparison.InvariantCulture))
                    Destination = destination.Trim().ToLowerInvariant();
            }

            private bool Equals(CountryPair other)
            {
                var currentOrigin = Origin;
                var currentDestination = Destination;

                var otherOrigin = other.Origin;
                var otherDestination = other.Destination;

                if (string.IsNullOrWhiteSpace(currentOrigin))
                    currentOrigin = otherOrigin;

                if (string.IsNullOrWhiteSpace(currentDestination))
                    currentDestination = otherDestination;

                if (string.IsNullOrWhiteSpace(otherOrigin))
                    otherOrigin = currentOrigin;

                if (string.IsNullOrWhiteSpace(otherDestination))
                    otherDestination = currentDestination;

                return string.Equals(currentOrigin, otherOrigin) && string.Equals(currentDestination, otherDestination);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return ((Origin != null ? Origin.GetHashCode() : 0) * 397) ^ (Destination != null ? Destination.GetHashCode() : 0);
                }
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                return obj is CountryPair && Equals((CountryPair) obj);
            }
        }

        #endregion

        #region International Search

        protected virtual bool IsInternationalSearch(FlightSearchCriteria searchCriteria, IContentManager contentManager)
        {
            return InternationalSearchCheckProvider.IsSearchInternational(searchCriteria.SearchSegments, contentManager);
        }

        #endregion
    }
}
