﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class FlightRecommendationComparer : IFlightRecommendationComparer
    {
        protected static SortingOrder DefaultOrder = Entities.Avail.SortingOrder.ByPriceLowToHigh;

        public SortingOrder? SortingOrder { get; set; }

        public bool SortByDesc { get; set; }

        public int Compare(FlightRecommendation x, FlightRecommendation y)
        {
            var order = SortingOrder ?? DefaultOrder;
            var multiplier = SortByDesc ? -1 : 1;

            switch (order)
            {
                case Entities.Avail.SortingOrder.ByPriceHighToLow:
                    return (multiplier) * (-1) * x.Fare.TotalFare.BaseAmount.CompareTo(y.Fare.TotalFare.BaseAmount);
                //case Entities.Avail.SortingOrder.ByPreference: //ByPreference will be handled via custom code.
                case Entities.Avail.SortingOrder.ByAirline:
                    
                    var val = (multiplier)* StringComparer.Ordinal.Compare(
                            x.Legs.SelectMany(z => z.Segments.Select(s => s.MarketingAirline.Code)).First(), 
                            y.Legs.SelectMany(z => z.Segments.Select(s => s.MarketingAirline.Code)).First()
                        );

                    if (val > 0)
                        return 1;
                    if (val < 0)
                        return -1;

                    return 0;

                case Entities.Avail.SortingOrder.ByJourneyTime:
                    return
                        (multiplier) * x.Legs.SelectMany(z => z.Segments.Select(s => s.DurationMinutes))
                         .Sum()
                         .CompareTo(y.Legs.SelectMany(z => z.Segments.Select(s => s.DurationMinutes)).Sum());
                case Entities.Avail.SortingOrder.ByDepartureTime:
                    return
                        (multiplier) * x.Legs.First()
                         .Segments.First()
                         .DepartureDateTime.CompareTo(y.Legs.First().Segments.First().DepartureDateTime);
                case Entities.Avail.SortingOrder.ByArrivalTime:
                    return
                        (multiplier) * x.Legs.First()
                         .Segments.First()
                         .ArrivalDateTime.CompareTo(y.Legs.First().Segments.First().ArrivalDateTime);
                case Entities.Avail.SortingOrder.ByPriceLowToHigh:
                default:
                    return (multiplier) * x.Fare.TotalFare.BaseAmount.CompareTo(y.Fare.TotalFare.BaseAmount);
            }
        }
    }
}
