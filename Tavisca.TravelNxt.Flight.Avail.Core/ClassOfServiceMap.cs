﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Repositories;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ClassOfServiceMap : IClassOfServiceMap
    {
        #region Static Members

        private ICacheProvider _cacheProvider;
        protected ICacheProvider CacheProvider
        {
            get
            {
                return _cacheProvider ??
                       (_cacheProvider =
                        RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.AirCore));
            }
        }

        #endregion

        #region Maps

        private IDictionary<string, IDictionary<string, IDictionary<string, CabinType>>> _classOfServiceMap;
        protected IDictionary<string, IDictionary<string, IDictionary<string, CabinType>>> ClassesOfServiceMap
        {
            get { return _classOfServiceMap ?? (_classOfServiceMap = GetClassOfServiceMap()); }
        }

        private IDictionary<string, IDictionary<string, string>> _airlineZoneMap;
        protected IDictionary<string, IDictionary<string, string>> AirlineZoneMap
        {
            get { return _airlineZoneMap ?? (_airlineZoneMap = GetAirlinesZoneMap()); }
        }

        #endregion

        #region IClassOfServiceMap Members

        public virtual CabinType GetClassOfService(FlightSegment segment, string classOfService)
        {
            var zoneMap = AirlineZoneMap;
            var airlineCode = segment.MarketingAirline.Code;


            var countryCode = new[] { segment.DepartureAirport.City.CountryCode, segment.ArrivalAirport.City.CountryCode };

            IDictionary<string, string> airlineCountryMap;
            if (!zoneMap.TryGetValue(airlineCode, out airlineCountryMap))
            {
                airlineCode = "*"; // To load default settings for all airlines
                countryCode[0] = "*";
                countryCode[1] = "*";

                zoneMap.TryGetValue(airlineCode, out airlineCountryMap);
            }
            else
            {
                if (!airlineCountryMap.ContainsKey(countryCode[0]))
                {
                    countryCode[0] = "*"; // To load default settings for that specific airline for all countries
                    if (!airlineCountryMap.ContainsKey(countryCode[0]))
                    {
                        airlineCode = "*";
                        countryCode[0] = "*"; // To load default settings for that specific airline for all countries
                    }
                }
                if (!airlineCountryMap.ContainsKey(countryCode[1]))
                {
                    countryCode[1] = "*"; // To load default settings for that specific airline for all countries
                    if (!airlineCountryMap.ContainsKey(countryCode[1]))
                    {
                        airlineCode = "*";
                        countryCode[1] = "*"; // To load default settings for that specific airline for all countries
                    }
                }
            }

            string zone1;
            if (airlineCountryMap != null && airlineCountryMap.TryGetValue(countryCode[0], out zone1))
            {
                if (countryCode[0].Equals(countryCode[1], StringComparison.OrdinalIgnoreCase))
                {
                    var cabinClass = GetZoneCabinClass(zone1, classOfService, airlineCode);

                    return cabinClass == CabinType.Unknown ? CabinType.Economy : cabinClass;
                }

                var zone1CabinClass = GetZoneCabinClass(zone1, classOfService, airlineCode);

                string zone2;
                if (!airlineCountryMap.TryGetValue(countryCode[1], out zone2))
                    zone2 = string.Empty;

                if (zone2.Equals("Domestic", StringComparison.OrdinalIgnoreCase) && zone1CabinClass != CabinType.Unknown)
                    return zone1CabinClass;

                var zone2CabinClass = GetZoneCabinClass(zone2, classOfService, airlineCode);

                if (zone1.Equals("Domestic", StringComparison.OrdinalIgnoreCase) && zone2CabinClass != CabinType.Unknown)
                    return zone2CabinClass;

                if (zone1CabinClass != CabinType.Unknown && zone2CabinClass != CabinType.Unknown)
                {
                    if (zone1CabinClass == zone2CabinClass)
                        return zone1CabinClass;

                    return (int)zone1CabinClass > (int)zone2CabinClass ? zone2CabinClass : zone1CabinClass; //show the higher cabin type as per Anshul Chrungu
                }
            }

            return CabinType.Economy;
        }

        #endregion


        #region Protected Members

        protected virtual CabinType GetZoneCabinClass(string zone, string classOfService, string airLineCode)
        {
            var classOfServiceMap = ClassesOfServiceMap;

            IDictionary<string, IDictionary<string, CabinType>> airlinecodeZoneMap;

            if (classOfServiceMap.TryGetValue(airLineCode, out airlinecodeZoneMap))
            {
                IDictionary<string, CabinType> cabinTypeClassOfServiceMap;

                if (airlinecodeZoneMap.TryGetValue(zone, out cabinTypeClassOfServiceMap))
                {
                    CabinType cabinType;
                    //Exception case found
                    if (cabinTypeClassOfServiceMap.TryGetValue(classOfService, out cabinType))
                        return cabinType;
                }

                return GetDefaultCabinType(airLineCode, zone, classOfService);
            }

            return GetDefaultCabinType("*", zone, classOfService);
        }

        protected virtual CabinType GetDefaultCabinType(string airlineCode, string zone, string classOfService)
        {
            var airlineCodes = new List<string>()
                {
                    airlineCode
                };

            if (!airlineCode.Equals("*", StringComparison.Ordinal))
                airlineCodes.Add("*");
            
            var classOfServiceMap = ClassesOfServiceMap;

            foreach (var code in airlineCodes)
            {
                var commonServiceMap = classOfServiceMap[code];
                IDictionary<string, CabinType> zoneSpecificMap;
                CabinType targetCabinType;
                //Return default cabin class for that zone
                if (commonServiceMap.TryGetValue(zone, out zoneSpecificMap) && zoneSpecificMap.TryGetValue(classOfService, out targetCabinType))
                    return targetCabinType;
                //Return default cabin class 

                zoneSpecificMap = commonServiceMap["*"];

                if (zoneSpecificMap.TryGetValue(classOfService, out targetCabinType))
                    return targetCabinType;
            }

            return CabinType.Unknown;
        }

        protected virtual IDictionary<string, IDictionary<string, IDictionary<string, CabinType>>> GetClassOfServiceMap()
        {
                var classesOfService = CacheProvider.Get<IDictionary<string, IDictionary<string, IDictionary<string, CabinType>>>>
                    (KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.ClassOfService);

                if (classesOfService != null)
                    return classesOfService;

            var classOfServices = GetAirlineClassesOfServicesFromSlowStore();

            var map = ParseClassOfServiceMap(classOfServices);

                CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.ClassOfService, map,
                                     new ExpirationSettings()
                                         {
                                             ExpirationType = ExpirationType.SlidingExpiration,
                                             SlidingExpirationInterval =
                                                 new TimeSpan(
                                                 TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                         });
        
            return map;
        }

        private static IDictionary<string, IDictionary<string, IDictionary<string, CabinType>>> ParseClassOfServiceMap(IEnumerable<AirlineClassesOfService> airlineClassesOfServices)
        {
            var airlineServiceClasses =
                new ConcurrentDictionary<string, IDictionary<string, IDictionary<string, CabinType>>>();

            if (airlineClassesOfServices != null)
            {
                foreach (var airlineClassesOfService in airlineClassesOfServices)
                {
                    var airlineCode = airlineClassesOfService.AirlineCode;
                    var zoneName = airlineClassesOfService.ZoneName;
                    var classofService = airlineClassesOfService.ClassOfService;
                    var cabinType = airlineClassesOfService.CabinType;

                    if (airlineCode == null || zoneName == null || classofService == null || cabinType == null)
                        continue;

                    airlineCode = airlineCode.Trim();
                    zoneName = zoneName.Trim();
                    classofService = classofService.Trim();
                    cabinType = cabinType.Trim();

                    IDictionary<string, IDictionary<string, CabinType>> zoneClassMap;
                    if (!airlineServiceClasses.TryGetValue(airlineCode, out zoneClassMap))
                    {
                        zoneClassMap = new ConcurrentDictionary<string, IDictionary<string, CabinType>>();

                        airlineServiceClasses.TryAdd(airlineCode, zoneClassMap);
                    }

                    IDictionary<string, CabinType> classMap;
                    if (!zoneClassMap.TryGetValue(zoneName, out classMap))
                    {
                        classMap = new ConcurrentDictionary<string, CabinType>();

                        ((ConcurrentDictionary<string, IDictionary<string, CabinType>>)zoneClassMap).TryAdd(zoneName, classMap);
                    }

                    ((ConcurrentDictionary<string, CabinType>) classMap).AddOrUpdate(classofService,
                                                                                     s => ParseCabinClass(cabinType),
                                                                                     (s, type) => type);
                }
            }

            return airlineServiceClasses;
        }

        protected virtual IDictionary<string, IDictionary<string, string>> GetAirlinesZoneMap()
        {
                var airlineMap = CacheProvider.Get<IDictionary<string, IDictionary<string, string>>>(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.AirlineZones);

                if (airlineMap != null)
                    return airlineMap;
            
            var airlineZones = GetAirlineCosZoneCountriesFromSlowStore();

            var map = ParseAirlinesZoneMap(airlineZones);

                CacheProvider.Insert(KeyStore.CacheBucket.AirSettings, KeyStore.CacheKeys.AirlineZones, map,
                                new ExpirationSettings()
                                {
                                    ExpirationType = ExpirationType.SlidingExpiration,
                                    SlidingExpirationInterval =
                                        new TimeSpan(
                                            TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                });
            
            return map;
        }

        protected virtual IList<AirlineClassesOfService> GetAirlineClassesOfServicesFromSlowStore()
        {
            IList<AirlineClassesOfService> classOfServices;

            var repositoryFactory =
                RuntimeContext.Resolver.Resolve<IRepositoryFactory>();

            Utility.LogDiagnosticEntry("Loading airline class of service mapping from slow store.");
            using (var repository = repositoryFactory.GetRepository(KeyStore.Repositories.AirConfigurationReadOnly))
            {
                classOfServices = repository.All<AirlineClassesOfService>().ToListBuffered();
            }
            Utility.LogDiagnosticEntry("Finished loading airline class of service mapping from slow store.");

            return classOfServices;
        }

        protected virtual IList<AirlineCosZoneCountry> GetAirlineCosZoneCountriesFromSlowStore()
        {
            IList<AirlineCosZoneCountry> airlineZones;

            var repositoryFactory =
                RuntimeContext.Resolver.Resolve<IRepositoryFactory>();

            Utility.LogDiagnosticEntry("Loading airline zone country mapping from slow store.");
            using (var repository = repositoryFactory.GetRepository(KeyStore.Repositories.AirConfigurationReadOnly))
            {
                airlineZones = repository.All<AirlineCosZoneCountry>().ToListBuffered();
            }
            Utility.LogDiagnosticEntry("Finished loading airline zone country mapping from slow store.");

            return airlineZones;
        }

        private static IDictionary<string, IDictionary<string, string>> ParseAirlinesZoneMap(IEnumerable<AirlineCosZoneCountry> airlineCosZoneCountries)
        {
            var airlineCountryCodeMap = new ConcurrentDictionary<string, IDictionary<string, string>>();

            if (airlineCosZoneCountries != null)
            {
                foreach (var airlineCosZoneCountry in airlineCosZoneCountries)
                {
                    var airlineCode = airlineCosZoneCountry.AirlineCode;
                    var zoneName = airlineCosZoneCountry.ZoneName;
                    var countryCode = airlineCosZoneCountry.CountryCode;

                    if (airlineCode == null || zoneName == null || countryCode == null)
                        continue;

                    airlineCode = airlineCode.Trim();
                    zoneName = zoneName.Trim();
                    countryCode = countryCode.Trim();
                    
                    IDictionary<string, string> countryZoneMap;
                    if (!airlineCountryCodeMap.TryGetValue(airlineCode, out countryZoneMap))
                    {
                        countryZoneMap = new ConcurrentDictionary<string, string>();

                        airlineCountryCodeMap.TryAdd(airlineCode, countryZoneMap);
                    }

                    ((ConcurrentDictionary<string, string>) countryZoneMap).TryAdd(countryCode, zoneName);
                }
            }

            return airlineCountryCodeMap;
        }

        protected static CabinType ParseCabinClass(string cabinType)
        {
            if (string.IsNullOrEmpty(cabinType))
                throw new InvalidDataException("Cabin type cannot be empty. Please configure CabinClass properly in DB.");

            switch (cabinType.ToLower().Trim())
            {
                case "first":
                    return CabinType.First;
                case "economy":
                    return CabinType.Economy;
                case "premiereconomy":
                    return CabinType.PremiumEconomy;
                case "business":
                    return CabinType.Business;
                default:
                    throw new InvalidDataException("Please configure CabinClass properly in DB. CabinType: '" + cabinType + "' not found as valid cabinType");
            }
        }

        #endregion
    }
}
