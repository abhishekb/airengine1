﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;
using Baggage = Tavisca.TravelNxt.Flight.Entities.Baggage;
using Fare = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Fare;
using PassengerType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.PassengerType;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    internal static class FlightSegmentTranslator
    {
        internal static List<FlightSegment> ToFlightSegments(this LegRecommendation leg, List<Data.Services.AirFareSearchV1.Flight> uniqueFlights,
                                                             Fare fare,
                                                             IList<PaxTypeQuantity> passengerTypeQuantities,
                                                             ProviderSpace providerSpace, ICollection<MarriageSegmentDetail> marriageSegmentDetails,
                                                             List<Data.Services.AirFareSearchV1.Baggage> baggages)
        {
            var flightSegments = new List<FlightSegment>();
            if (leg.FlightCodes == null)
                return flightSegments;

            var segmentIndex = 0;
            foreach (var flightCode in leg.FlightCodes)
            {
                var uniqueFlight = uniqueFlights.GetFlightByFlightCode(flightCode);
                if (uniqueFlight == null) continue;

                flightSegments.Add(CreateFlightSegment(fare, passengerTypeQuantities, providerSpace, uniqueFlight,
                                                       segmentIndex, marriageSegmentDetails, baggages));
                segmentIndex++;
            }

            return flightSegments;
        }

        private static FlightSegment CreateFlightSegment(Fare fare, IList<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace,
            Data.Services.AirFareSearchV1.Flight uniqueflight, int segmentIndex, IEnumerable<MarriageSegmentDetail> marriageSegmentDetails, List<Data.Services.AirFareSearchV1.Baggage> baggages)
        {
            var marriedSegmentDetails = marriageSegmentDetails == null ? null :
                marriageSegmentDetails.FirstOrDefault(x => x.FlightCode != null && x.FlightCode.Equals(uniqueflight.FlightCode, StringComparison.Ordinal));

            var additionalInfoDictionary = new AdditionalInfoDictionary();
            additionalInfoDictionary.AddOrUpdate("FlightSupplierRefKey", uniqueflight.SupplierRefKey);
            
            return new FlightSegment(uniqueflight.DepartureAirport, uniqueflight.ArrivalAirport, uniqueflight.DepartureDateTime, uniqueflight.ArrivalDateTime, uniqueflight.FlightNumber, uniqueflight.AirlineCode, segmentIndex)
            {
                AircraftType = uniqueflight.AircraftType,
                DurationMinutes = uniqueflight.DurationMinutes,
                CanETicket = uniqueflight.CanETicket,
                InSegmentStops = uniqueflight.NumberOfStops == 0 ? null : GetFlightSegmentStops(uniqueflight.StopDetails),
                OperatingAirline = string.IsNullOrEmpty(uniqueflight.OperatingAirlineCode) ? null : new Airline(uniqueflight.OperatingAirlineCode),
                PaxTypeFareBasisCodes = GetPaxTypeFareBasis(fare.PassengerFareBreakups, uniqueflight.FlightCode, passengerTypeQuantities, providerSpace),
                CodeShareText = uniqueflight.CodeShareText,
                ArrivalTerminal = uniqueflight.ArrivalTerminal,
                DepartureTerminal = uniqueflight.DepartureTerminal,
                DistanceTraveledKms = (int)Utility.ConvertDistance(uniqueflight.DistanceTravelledInMiles,UnitOptions.Miles, UnitOptions.Kilometers),
                MarriageSegmentCode = marriedSegmentDetails == null ? null : marriedSegmentDetails.MarriageGroupCode,
                BaggageDetails = GetBaggageDetails(baggages, fare.PassengerFareBreakups, uniqueflight.FlightCode, passengerTypeQuantities, providerSpace),
                AdditionalInfo = additionalInfoDictionary,
            };
        }

        private static List<PaxTypeFareBasis> GetPaxTypeFareBasis(IEnumerable<PassengerFareBreakup> passengerFareBreakups, string flightCode, IList<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace)
        {
            var segmentQualifiers = new List<PaxTypeFareBasis>();
            var isMilitaryFare =
                passengerTypeQuantities.Any(
                    p =>
                    p.PassengerType == Entities.PassengerType.MilitaryAdult ||
                    p.PassengerType == Entities.PassengerType.MilitaryChild ||
                    p.PassengerType == Entities.PassengerType.MilitarySenior ||
                    p.PassengerType == Entities.PassengerType.MilitaryInfant);
            //Segment qualifiers is resolved from priced flight which is parsed from passengers fare
            foreach (var passengerFareBreakup in passengerFareBreakups)
            {
                if (passengerFareBreakup.FareDetails == null) continue;
                foreach (var fareDetail in passengerFareBreakup.FareDetails)
                {
                    if (fareDetail.FlightCode != flightCode) continue;
                    var matchedPassengerTypeQuantities = passengerFareBreakup.GetMatchingPassengerTypesQuantitiesForPassengerFare(passengerTypeQuantities, providerSpace, isMilitaryFare);

                    if (matchedPassengerTypeQuantities == null || matchedPassengerTypeQuantities.Count <= 0 || matchedPassengerTypeQuantities.Sum(passengerTypeQuantity => passengerTypeQuantity.Quantity) != passengerFareBreakup.PassengerTypeQuantity.Quantity) continue;
                    segmentQualifiers.AddRange(matchedPassengerTypeQuantities.Select(matchedPassengerTypeQuantity => CreatePaxTypeFareBasis(matchedPassengerTypeQuantity, fareDetail)));
                }
            }

            return segmentQualifiers;
        }

        private static PaxTypeFareBasis CreatePaxTypeFareBasis(PaxTypeQuantity matchedPassengerTypeQuantity, FareDetails fareDetail)
        {
            return new PaxTypeFareBasis
            {
                PassengerType = matchedPassengerTypeQuantity.PassengerType,
                FareBasisCode = fareDetail.FareBasisCode,
                SupplierFareRefKey = fareDetail.SupplierFareRefKey,
                ClassOfService = fareDetail.ClassOfService,
                CabinType = fareDetail.Cabin.Cast()
            };
        }

        internal static Data.Services.AirFareSearchV1.Flight GetFlightByFlightCode(this IEnumerable<Data.Services.AirFareSearchV1.Flight> flights, string flightCode)
        {
            return flights.First(flight => flight != null && flight.FlightCode == flightCode);
        }

        private static List<SegmentStop> GetFlightSegmentStops(List<StopDetail> stopDetails)
        {
            if (stopDetails == null || stopDetails.Count == 0) return null;

            var flightSegmentStops = new List<SegmentStop>();
            foreach (var stopDetail in stopDetails)
            {
                if (stopDetail == null) continue;
                var flightSegmentStop = new SegmentStop
                {
                    Airport = new Airport(stopDetail.AirportCode),
                    ArrivalTerminal = stopDetail.ArrivalTerminal,
                    DepartureTerminal = stopDetail.DepartureTerminal,
                    Duration = stopDetail.DurationMinutes,
                    ArrivalDateTime = stopDetail.ArrivalDateTime,
                    DepartureDateTime = stopDetail.DepartureDateTime
                };
                flightSegmentStops.Add(flightSegmentStop);
            }
            return flightSegmentStops;
        }

        private static IList<Baggage> GetBaggageDetails(List<Data.Services.AirFareSearchV1.Baggage> baggages, IEnumerable<PassengerFareBreakup> passengerFareBreakups, string flightCode,
            IList<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace)
        {
            if (baggages == null || baggages.Count == 0) return null;

            var segmentBaggageDetails = new List<Baggage>();

            foreach (var passengerFareBreakup in passengerFareBreakups)
            {
                if (passengerFareBreakup.FareDetails == null || passengerFareBreakup.FareDetails.Count == 0)
                    continue;

                var fareDetail = passengerFareBreakup.FareDetails.FirstOrDefault(x => x.FlightCode == flightCode);

                if (fareDetail == null)
                    continue;

                var baggageCode = fareDetail.BaggageCode;

                if (string.IsNullOrEmpty(baggageCode)) continue;

                var baggageDetail = baggages.Find(baggage => baggage.BaggageCode == baggageCode);

                if (baggageDetail != null)
                {

                    segmentBaggageDetails.Add(new Baggage
                                                  {
                                                      PassengerType =
                                                          GetPassengerType(passengerFareBreakup.PassengerTypeQuantity,
                                                                           passengerTypeQuantities, providerSpace),
                                                      Description = baggageDetail.Description,
                                                      Quantity = baggageDetail.Quantity,
                                                      WeightInPounds = baggageDetail.WeightInPounds,
                                                      BaggageCode = baggageDetail.BaggageCode
                                                  });
                }
            }

            return segmentBaggageDetails.Count == 0 ? null : segmentBaggageDetails;
        }

        private static Entities.PassengerType GetPassengerType(PassengerTypeQuantity passengerTypeQuantity, IEnumerable<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace)
        {
            if (passengerTypeQuantity.PassengerType == PassengerType.Mapped)
            {
                if (!string.IsNullOrEmpty(passengerTypeQuantity.MappedPaxTypeCode))
                {
                    var paxTypeQuantity = passengerTypeQuantities.FirstOrDefault(
                        x =>
                        passengerTypeQuantity.MappedPaxTypeCode.Equals(GetMappedPassengerTypeCode(x.PassengerType,
                                                                            providerSpace)));
                    if (paxTypeQuantity != null)
                    {
                        return paxTypeQuantity.PassengerType;
                    }
                    throw new InvalidDataException("PassengerType not found for MappedPaxTypeCode : " +
                                                   passengerTypeQuantity.MappedPaxTypeCode);
                }

                throw new InvalidOperationException("MappedPaxTypeCode not set for MappedPassengerType");
            }

            return passengerTypeQuantity.PassengerType.Cast();
        }

        private static string GetMappedPassengerTypeCode(Entities.PassengerType passengerType,
                                                        ProviderSpace providerSpace)
        {
            string mappedCode;

            providerSpace.AdditionalInfo.TryGetValue(
                passengerType + KeyStore.ProviderSpaceAttributes.PrivateCode, out mappedCode);

            return mappedCode ?? string.Empty;
        }
    }
}
