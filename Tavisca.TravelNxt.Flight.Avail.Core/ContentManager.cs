﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ContentManager : IContentManager
    {
        private ICacheProvider _cacheProvider;
        protected ICacheProvider CacheProvider
        {
            get { return _cacheProvider ?? (_cacheProvider = CacheProviderResolver.GetCacheProvider(CacheCategory.Content)); }
        }

        private static IResolveCacheProvider _cacheProviderResolver;
        private static IResolveCacheProvider CacheProviderResolver
        {
            get { return _cacheProviderResolver ?? (_cacheProviderResolver = RuntimeContext.Resolver.Resolve<IResolveCacheProvider>()); }
        }

        public virtual void LoadFlightSegmentStaticContent(IList<FlightRecommendation> flightRecommendations)
        {
            var recommendationsToRemove = new List<FlightRecommendation>();
            if (flightRecommendations != null)
            {
                var timeConverter = RuntimeContext.Resolver.Resolve<ITimeConverter>(); //scoped out as the object does instance level caching.

                foreach (var flightRecommendation in flightRecommendations)
                {
                    for (var legIndex = 0; legIndex < flightRecommendation.Legs.Count; legIndex++)
                    {
                        var currentLeg = flightRecommendation.Legs[legIndex];

                        for (var segmentIndex = 0; segmentIndex < currentLeg.Segments.Count; segmentIndex++)
                        {
                            var currentSegment = currentLeg.Segments[segmentIndex];

                            //loading static content [BEGIN]
                            if (!LoadAirport(currentSegment.DepartureAirport) || !LoadAirport(currentSegment.ArrivalAirport)
                                || (currentSegment.InSegmentStops != null && currentSegment.InSegmentStops.Any(x => !LoadAirport(x.Airport))))
                                recommendationsToRemove.Add(flightRecommendation);
                            else
                            {
                                if (!LoadAirline(currentSegment.MarketingAirline) || !LoadAirline(currentSegment.OperatingAirline)) //loading static content [END]
                                    recommendationsToRemove.Add(flightRecommendation);
                                else
                                {
                                    //loading dynamic content [BEGIN]
                                    currentSegment.DurationMinutes = Convert.ToInt32(
                                            timeConverter.GetAbsoluteTimeDifference(
                                                        currentSegment.ArrivalAirport.ToTimeZoneInfo(),
                                                        currentSegment.ArrivalDateTime,
                                                        currentSegment.DepartureAirport.ToTimeZoneInfo(),
                                                        currentSegment.DepartureDateTime
                                                        ).TotalMinutes
                                            );
                                }
                            }
                            //loading dynamic content [END]
                        }
                    }
                }

                foreach (var flightRecommendation in recommendationsToRemove)
                {
                    flightRecommendations.Remove(flightRecommendation);
                }
            }
        }

        public virtual Airport GetAirportByCode(string airportCode)
        {
            Airport airport;

            return AirportCache.TryGetValue(airportCode, out airport) ? airport : null;
        }

        public List<City> GetCitiesByCode(string cityCode)
        {
            List<City> cities;

            return CityCache.TryGetValue(cityCode, out cities) ? cities : null;
        }

        public virtual Airline GetAirlineByCode(string airlineCode)
        {
            Airline airline;

            return AirlineCache.TryGetValue(airlineCode, out airline) ? airline : null;
        }

        public virtual IEnumerable<Airport> GetNearbyAirports(string airportCode, bool includeSelf = true, int? radiusKm = null)
        {
            var targetRadius = radiusKm.HasValue ? radiusKm.Value :
                ConvertToKm(SettingManager.NearbyAirportRadius, SettingManager.DistanceUnit);

            IList<Tuple<Airport, double>> cachedNearbyAirports;
            if (NearbyAirports != null && NearbyAirports.TryGetValue(airportCode, out cachedNearbyAirports))
            {
                if (!includeSelf)
                {
                    cachedNearbyAirports = NearbyAirports[airportCode].Where(x =>
                        !x.Item1.Code.Equals(airportCode, StringComparison.OrdinalIgnoreCase))
                        .ToListBuffered(cachedNearbyAirports.Count);
                }

                return cachedNearbyAirports.Where(x => x.Item2 <= targetRadius).Select(x => x.Item1);
            }

            var allAirports = AirportCache.Values.ToListBuffered(AirportCache.Count);

            var target = GetAirportByCode(airportCode);

            const UnitOptions unit = UnitOptions.Kilometers;
            var radius = ConvertToKm(SettingManager.MaxNearbyAirportRadius, SettingManager.DistanceUnit);

            var nearbyAirports =
                allAirports.Where(
                    airport =>
                    airport.City.WindowsTimeZone.Equals(target.City.WindowsTimeZone,
                                                     StringComparison.Ordinal) &&
                    airport.City.CountryCode.Equals(target.City.CountryCode, StringComparison.OrdinalIgnoreCase)
                    )
                    .Select(x => new Tuple<Airport, double>(x, target.DistanceFrom(x, unit)))
                    .Where(x => x.Item2 <= radius)
                    .ToListBuffered();

            #region Write in Cache if there is a miss

            if (_nearbyAirports == null)
                _nearbyAirports = new ConcurrentDictionary<string, IList<Tuple<Airport, double>>>();

            _nearbyAirports.TryAdd(target.Code, nearbyAirports);

            CacheProvider.Insert(KeyStore.CacheBucket.AirportContent, KeyStore.CacheKeys.NearbyAirports,
                                 _nearbyAirports,
                                 new ExpirationSettings()
                                 {
                                     ExpirationType = ExpirationType.SlidingExpiration,
                                     SlidingExpirationInterval =
                                         new TimeSpan(
                                         TravelNxt.Common.Settings.SettingManager.CacheExpirationHours, 0, 0)
                                 });

            #endregion

            if (!includeSelf)
            {
                nearbyAirports = nearbyAirports.Where(x =>
                    !x.Item1.Code.Equals(airportCode, StringComparison.OrdinalIgnoreCase))
                    .ToListBuffered();
            }

            return nearbyAirports.Where(x => x.Item2 <= targetRadius).Select(x => x.Item1);
        }

        public List<Airport> GetAirportsForCity(string cityCode)
        {
            var cityAirports = AirportCache.Values.Where(
                airport =>
                    airport.City != null &&
                    airport.City.Code.Equals(cityCode, StringComparison.InvariantCultureIgnoreCase));

            return cityAirports.ToList();
        }

        private double ConvertToKm(double value, UnitOptions unit)
        {
            switch (unit)
            {
                case UnitOptions.Kilometers:
                    return value;
                case UnitOptions.Miles:
                    return value * 1.609344;
                case UnitOptions.NauticalMiles:
                    return value * 1.85200;
            }

            throw new NotSupportedException("The unit option is not supported - " +
                Enum.GetName(typeof(UnitOptions), unit));
        }

        #region Load Airline Content


        protected virtual bool LoadAirline(Airline airline)
        {
            if (airline == null)
                return true; //airlines can be null in some cases such as operating airline.

            var cached = GetAirlineByCode(airline.Code);

            if (cached == null)
            {
                var entry = Utility.GetEventEntryContextual();

                entry.Title = "Airline not found";
                entry.CallType = KeyStore.CallTypes.LongTermDiagnostics;

                entry.AddAdditionalInfo("MissingAirlineCode", airline.Code);

                entry.AddMessage(string.Format("Airline not found for code: '{0}'", airline.Code));

                entry.StatusType = StatusOptions.Failure;
                entry.SeverityType = SeverityOptions.Warning;

                Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.LongTermDiagnostics);

                return false;
            }

            airline.FullName = cached.FullName;
            airline.WebsiteURL = cached.WebsiteURL;

            return true;
        }

        private ConcurrentDictionary<string, Airline> _airlineCache;
        private readonly object _airlineCacheLocker = new object();
        protected virtual ConcurrentDictionary<string, Airline> AirlineCache
        {
            get
            {
                if (_airlineCache != null)
                    return _airlineCache;

                var key = string.Format(KeyStore.CacheKeys.AirlinesCacheKey, GetCurrentCulture());

                #region Read from cache

                _airlineCache =
                        CacheProvider.Get<ConcurrentDictionary<string, Airline>>(KeyStore.CacheBucket.AirportContent, key);

                if (_airlineCache != null)
                    return _airlineCache;

                #endregion

                #region Write To Cache if there is a miss

                lock (_airlineCacheLocker)
                {
                    if (_airlineCache != null)
                        return _airlineCache;

                    var airlineCache = LoadAirlineContent();

                    _airlineCache = airlineCache;

                    CacheProvider.Insert(KeyStore.CacheBucket.AirportContent, key, airlineCache,
                                         new ExpirationSettings()
                                             {
                                                 ExpirationType = ExpirationType.SlidingExpiration,
                                                 SlidingExpirationInterval =
                                                     new TimeSpan(
                                                     TravelNxt.Common.Settings.SettingManager.
                                                         CacheExpirationHours, 0, 0)
                                             });
                }

                #endregion

                return _airlineCache;
            }
        }

        protected static string GetCurrentCulture()
        {
            return Utility.GetCurrentContext().Culture.ToString();
        }

        protected virtual ConcurrentDictionary<string, Airline> LoadAirlineContent()
        {
            ICollection<Data.Services.AirContentDataService.Airline> airlines;

            Utility.LogDiagnosticEntry("calling AirContentDataService.GetAllAirlines");
            using (var client = new Data.Services.AirContentDataService.AirContentDataServiceClient(KeyStore.ClientEndpoints.DataServicesIAirContentDataService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirContentDataServiceAddr)))
            {
                airlines = client.GetAllAirlines();
            }
            Utility.LogDiagnosticEntry("call finished for AirContentDataService.GetAllAirlines");

            var airLineData = new Dictionary<string, Airline>();
            foreach (var airline in airlines)
            {
                if (!airLineData.ContainsKey(airline.AirlineCode))
                {
                    airLineData.Add(airline.AirlineCode, new Airline(airline.AirlineCode) { FullName = airline.FullName, WebsiteURL = airline.Website });
                }
            }

            return new ConcurrentDictionary<string, Airline>(airLineData);
        }

        #endregion

        #region Content Loading

        private volatile ConcurrentDictionary<string, List<City>> _cityCache;
        private readonly object _cityCacheLocker = new object();
        protected virtual ConcurrentDictionary<string, List<City>> CityCache
        {
            get
            {
                if (_cityCache != null)
                    return _cityCache;

                var key = string.Format(KeyStore.CacheKeys.CityCacheKey, GetCurrentCulture());

                #region Read from Cache

                _cityCache =
                        CacheProvider.Get<ConcurrentDictionary<string, List<City>>>(KeyStore.CacheBucket.CityContent, key);

                if (_cityCache != null)
                    return _cityCache;

                #endregion

                #region Write in Cache if there is a miss

                lock (_cityCacheLocker)
                {
                    if (_cityCache != null)
                        return _cityCache;

                    var cityCache = LoadAllCities();

                    _cityCache = cityCache;

                    CacheProvider.Insert(KeyStore.CacheBucket.CityContent, key, cityCache,
                                         new ExpirationSettings()
                                         {
                                             ExpirationType = ExpirationType.SlidingExpiration,
                                             SlidingExpirationInterval =
                                                 new TimeSpan(
                                                 TravelNxt.Common.Settings.SettingManager.
                                                     CacheExpirationHours,
                                                 0, 0)
                                         });

                }

                #endregion

                return _cityCache;
            }
        }

        private volatile ConcurrentDictionary<string, Airport> _airportCache;
        private readonly object _airportCacheLocker = new object();
        protected virtual ConcurrentDictionary<string, Airport> AirportCache
        {
            get
            {
                if (_airportCache != null)
                    return _airportCache;

                var key = string.Format(KeyStore.CacheKeys.AirportCacheKey, GetCurrentCulture());

                #region Read from Cache

                _airportCache =
                        CacheProvider.Get<ConcurrentDictionary<string, Airport>>(KeyStore.CacheBucket.AirportContent, key);

                if (_airportCache != null)
                    return _airportCache;

                #endregion

                #region Write in Cache if there is a miss

                lock (_airportCacheLocker)
                {
                    if (_airportCache != null)
                        return _airportCache;

                    var airportCache = LoadAllAirports();

                    _airportCache = airportCache;

                    CacheProvider.Insert(KeyStore.CacheBucket.AirportContent, key, airportCache,
                                         new ExpirationSettings()
                                             {
                                                 ExpirationType = ExpirationType.SlidingExpiration,
                                                 SlidingExpirationInterval =
                                                     new TimeSpan(
                                                     TravelNxt.Common.Settings.SettingManager.
                                                         CacheExpirationHours,
                                                     0, 0)
                                             });

                }

                #endregion

                return _airportCache;
            }
        }

        protected virtual ConcurrentDictionary<string, Airport> LoadAllAirports()
        {
            ICollection<Data.Services.AirContentDataService.Airport> airports;

            Utility.LogDiagnosticEntry("calling AirContentDataService.GetAllAirports");
            using (var client = new Data.Services.AirContentDataService.AirContentDataServiceClient(KeyStore.ClientEndpoints.DataServicesIAirContentDataService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirContentDataServiceAddr)))
            {
                airports = client.GetAllAirports();
            }
            Utility.LogDiagnosticEntry("call finished for AirContentDataService.GetAllAirports");

            var allAirports = from airport in airports
                              select
                                  new Airport(airport.AirportCode)
                                  {
                                      City =
                                          new City
                                          {
                                              Code = airport.CityCode,
                                              CountryCode = airport.CountryCode,
                                              StateCode = !string.IsNullOrEmpty(airport.StateCode) ? airport.StateCode : airport.StateName,
                                              Name = airport.CityName,
                                              WindowsTimeZone = airport.WindowsTimeZone
                                          },
                                      FullName = airport.AirportName,
                                      Latitude = airport.Latitude,
                                      Longitude = airport.Longitude
                                  };

            return new ConcurrentDictionary<string, Airport>(allAirports.ToDictionary(x => x.Code),
                StringComparer.OrdinalIgnoreCase);

        }

        protected virtual ConcurrentDictionary<string, List<City>> LoadAllCities()
        {
            ICollection<Data.Services.AirContentDataService.City> cities;

            Utility.LogDiagnosticEntry("calling AirContentDataService.GetAllCities");
            using (var client = new Data.Services.AirContentDataService.AirContentDataServiceClient(KeyStore.ClientEndpoints.DataServicesIAirContentDataService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirContentDataServiceAddr)))
            {
                cities = client.GetAllCities();
            }
            Utility.LogDiagnosticEntry("call finished for AirContentDataService.GetAllCities");

            var allCities = from city in cities
                where !string.IsNullOrWhiteSpace(city.IataCityCode)
                select
                    new City
                    {
                        Code = city.IataCityCode,
                        CountryCode = city.CountryCode,
                        Name = city.CityName,
                        StateCode = city.StateCode
                    };

            return
                new ConcurrentDictionary<string, List<City>>(allCities.GroupBy(x => x.Code)
                    .ToDictionary(x => x.Key, y => y.ToList()));
        }

        protected virtual bool LoadAirport(Airport airport)
        {
            if (airport == null)
                throw new ArgumentNullException("airport");

            var cached = GetAirportByCode(airport.Code);

            if (cached == null)
            {
                var entry = Utility.GetEventEntryContextual();

                entry.Title = "Airport not found";
                entry.CallType = KeyStore.CallTypes.LongTermDiagnostics;

                entry.AddAdditionalInfo("MissingAirportCode", airport.Code);

                entry.AddMessage(string.Format("Airport not found for code: '{0}'", airport.Code));

                entry.StatusType = StatusOptions.Failure;
                entry.SeverityType = SeverityOptions.Warning;

                Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.LongTermDiagnostics);

                return false;
            }

            if (cached.City != null)
            {
                airport.City = new City { Code = cached.City.Code, CountryCode = cached.City.CountryCode, StateCode = cached.City.StateCode, WindowsTimeZone = cached.City.WindowsTimeZone, Name = cached.City.Name };
            }
            airport.FullName = cached.FullName;

            airport.Latitude = cached.Latitude;
            airport.Longitude = cached.Longitude;

            return true;
        }

        #endregion

        #region Nearby Airports

        private ConcurrentDictionary<string, IList<Tuple<Airport, double>>> _nearbyAirports;

        protected virtual ConcurrentDictionary<string, IList<Tuple<Airport, double>>> NearbyAirports
        {
            get
            {
                if (_nearbyAirports != null)
                    return _nearbyAirports;

                #region Read from Cache

                _nearbyAirports =
                        CacheProvider.Get<ConcurrentDictionary<string, IList<Tuple<Airport, double>>>>(
                            KeyStore.CacheBucket.AirportContent, KeyStore.CacheKeys.NearbyAirports);
                #endregion

                return _nearbyAirports;
            }
        }

        #endregion
    }
}
