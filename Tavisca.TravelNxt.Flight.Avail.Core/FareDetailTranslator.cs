﻿using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;
using Fare = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Fare;
using FareType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareType;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public static class FareDetailTranslator
    {
        internal static FlightFare GetFlightFare(this Fare fare, IList<PaxTypeQuantity> passengerTypeQuantities, ProviderSpace providerSpace)
        {
            var passengerFares = new List<PassengerFare>(fare.PassengerFareBreakups.Count);

            var isMilitaryFare =
                passengerTypeQuantities.Any(
                    p =>
                    p.PassengerType == PassengerType.MilitaryAdult || p.PassengerType == PassengerType.MilitaryChild ||
                    p.PassengerType == PassengerType.MilitaryInfant || p.PassengerType == PassengerType.MilitarySenior);

            foreach (var passengerFareBreakup in fare.PassengerFareBreakups)
            {
                var mappedPassengerFares = passengerFareBreakup.GetMappedPassengerFares(fare, passengerTypeQuantities, providerSpace, isMilitaryFare);
                foreach (var mappedPassengerFare in mappedPassengerFares)
                {
                    var passengerFare = passengerFares.FirstOrDefault(flightFarepassengerFare => flightFarepassengerFare.PassengerType == mappedPassengerFare.PassengerType);
                    if (passengerFare != null)
                    {
                        passengerFare.BaseAmount += mappedPassengerFare.BaseAmount;
                        passengerFare.Taxes.AddRange(mappedPassengerFare.Taxes);
                    }
                    else
                        passengerFares.Add(mappedPassengerFare);
                }
            }

            passengerFares.TrimExcess();

            //Tax amounts grouped according to code
            passengerFares.ForEach(
                passengerFare => passengerFare.Taxes = GroupFareComponentAccordingToCode(passengerFare.Taxes));

            var flightFare = new FlightFare() {PassengerFares = passengerFares};

            if (fare.FareAttributes != null && fare.FareAttributes.Any())
            {
                flightFare.FareAttributes = new List<FareAttribute>();
                flightFare.FareAttributes.AddRange(
                    fare.FareAttributes.Select(farePreference => farePreference.Cast()));
            }

            if (fare.FareType == FareType.Private)
                flightFare.FareType = Entities.FareType.Negotiated;
            else
                flightFare.FareType = flightFare.PassengerFares.Any(x => x.Discounts.Count > 0)
                                          ? Entities.FareType.Negotiated
                                          : Entities.FareType.Published;

            return flightFare;
        }

        private static IList<Entities.FareComponent> GroupFareComponentAccordingToCode(IEnumerable<Entities.FareComponent> fareComponents)
        {
            var fareComponentCodeGroup = fareComponents.GroupBy(fareComponent => fareComponent.Code);

            var groupedFareComponents = new List<Entities.FareComponent>();

            foreach (var group in fareComponentCodeGroup)
            {
                var groupElements = group.ToListBuffered();
                var groupElementsSum = groupElements.Sum(groupElement => groupElement.Value.NativeAmount);
                var groupCurrency = (groupElementsSum > 0)
                                        ? groupElements.First(groupElement => groupElement.Value.BaseAmount > 0).Value.
                                              NativeCurrency
                                        : null;
                groupedFareComponents.Add(new Entities.FareComponent()
                                              {
                                                  Code = groupElements[0].Code,
                                                  FareComponentType = groupElements[0].FareComponentType,
                                                  Value =
                                                      groupElementsSum > 0
                                                          ? new Money(
                                                                groupElementsSum, groupCurrency)
                                                          : Money.Zero,
                                                  Description = groupElements[0].Description,
                                                  OwnerId = groupElements[0].OwnerId,
                                                  FareComponentRuleId = groupElements[0].FareComponentRuleId
                                              });
            }

            return groupedFareComponents;
        }

        internal static List<Entities.FareComponent> ToTaxes(this List<Data.Services.AirFareSearchV1.FareComponent> nTaxes, List<Entities.FareComponent> additionalTaxes)
        {
            var taxes = new List<Entities.FareComponent>();
            taxes.AddRange(additionalTaxes);
            if (nTaxes == null)
                return taxes;
            taxes.AddRange((from fareComponent in nTaxes
                            where fareComponent != null
                            select new Entities.FareComponent()
                                       {
                                           Value =
                                               fareComponent.Amount > 0
                                                   ? new Money(fareComponent.Amount, fareComponent.CurrencyCode)
                                                   : Money.Zero,
                                           Code = fareComponent.Code,
                                           Description = fareComponent.Description,
                                           FareComponentType = FareComponentType.Tax
                                       }));
            return taxes;
        }
    }
}
