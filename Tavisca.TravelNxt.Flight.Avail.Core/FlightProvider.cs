﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Tavisca.Frameworks.Caching;
using Tavisca.Frameworks.Logging;
using Tavisca.Frameworks.Logging.Infrastructure;
using Tavisca.Frameworks.Parallel;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Caching;
using Tavisca.TravelNxt.Common.Core.Contracts;
using Tavisca.TravelNxt.Common.Core.Contracts.Entities;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Analytics;
using Tavisca.TravelNxt.Flight.Avail.Core.CacheDataTransforms;
using Tavisca.TravelNxt.Flight.Avail.Core.Exceptions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;
using AirlinesPreference = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.AirlinesPreference;
using Baggage = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Baggage;
using CabinType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.CabinType;
using ConnectionPreference = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.ConnectionPreference;
using Fare = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Fare;
using FareComponentType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareComponentType;
using FareType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.FareType;
using PassengerType = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.PassengerType;
using Requester = Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1.Requester;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class FlightProvider : IFlightProvider
    {
        protected virtual string LogEntryTitle { get { return "Search Call to supplier: {0}"; } }

        private ICacheProvider _cacheProvider;

        protected virtual ICacheProvider CacheProvider
        {
            get
            {
                return _cacheProvider ??
                       (_cacheProvider =
                        RuntimeContext.Resolver.Resolve<IResolveCacheProvider>().GetCacheProvider(CacheCategory.AirSupplierData));
            }
        }

        public FlightAvailResult Avail(FlightAvailRq request)
        {
            var eventEntry = Utility.GetEventEntryContextual();

            eventEntry.Title = string.Format(LogEntryTitle, request.ProviderSpace.Family);

            eventEntry.CallType = KeyStore.CallTypes.SupplierAirSearch;

            var logFactory = Utility.GetLogFactory();

            var stopwatch = new WrappedTimer();
            bool sceptrLoggingEnabled = SettingManager.SceptrLoggingEnabled;
            try
            {
                eventEntry.AddAdditionalInfo(KeyStore.LogCategories.AdditionalInfoKeys.ProviderName,
                    request.ProviderSpace.Family);

                eventEntry.ProviderId = request.ProviderSpace.ID;

                Utility.LogDiagnosticEntry("beginning translation into scepter contract: " + request.ProviderSpace.ID);
                var dataContract = ToDataContract(request);
                Utility.LogDiagnosticEntry("finished translation into scepter contract: " + request.ProviderSpace.ID);

                eventEntry.RequestObject = sceptrLoggingEnabled ? dataContract : null;
                eventEntry.ReqResSerializerType = SerializerType.DataContractSerializer;

                stopwatch.Start();

                var result = GetResponse(dataContract, request.ProviderSpace, eventEntry);

                if (result != null)
                {
                    if (result.Status == null || result.Status.ResponseStatus == StatusType.Failure)
                        eventEntry.StatusType = StatusOptions.Failure;
                    else
                        eventEntry.StatusType = StatusOptions.Success;

                    eventEntry.ResponseObject = sceptrLoggingEnabled ? result : null;
                }

                Utility.LogDiagnosticEntry("beginning translation from scepter contract: " + request.ProviderSpace.ID);
                var entity = ToEntity(result, request);
                Utility.LogDiagnosticEntry("finished translation from scepter contract: " + request.ProviderSpace.ID);

                return entity;

            }
            catch (FlightSupplierException)
            {
                eventEntry.StatusType = StatusOptions.Failure;
                throw;
            }
            catch (HandledException)
            {
                eventEntry.StatusType = StatusOptions.Failure;
                throw;
            }
            catch (Exception ex)
            {
                eventEntry.StatusType = StatusOptions.Failure;
                logFactory.WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);

                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
            finally
            {
                stopwatch.Stop();
                eventEntry.TimeTaken = stopwatch.ElapsedSeconds;

                if (sceptrLoggingEnabled || eventEntry.StatusType == StatusOptions.Failure)
                    logFactory.WriteAsync(eventEntry, KeyStore.LogCategories.Core);
            }
        }

        private FareSearchRS GetResponse(AirFareSearchRQ airFareSearchRequest, ProviderSpace providerSpace, IEventEntry eventEntry)
        {
            Utility.LogDiagnosticEntry("supplier looking for cached results, provider id: " + providerSpace.ID);

            string cacheKey = null;

            var cacheExpirationTime = GetCacheExpirationTime(airFareSearchRequest);

            if (cacheExpirationTime > 0)
            {
                var cachedScepterResponse = GetCachedScepterResponse(airFareSearchRequest, out cacheKey);
                if (cachedScepterResponse != null && IsResponseValidForPassengerTypes(airFareSearchRequest.Passengers.Select(x => x.PassengerType), cachedScepterResponse))
                {
                    eventEntry.CallType = KeyStore.CallTypes.CachedSupplierAirSearch;
                    eventEntry.AdditionalInfo.Add("CachedSceptrResponseSessionId", cachedScepterResponse.TrackingId);
                    return GetModifiedResponseAsPerRequest(airFareSearchRequest, cachedScepterResponse, providerSpace);
                }
            }

            var context = Utility.GetCurrentContext();
            FareSearchRS providerResponse = null;
            var supplierGetTimer = Stopwatch.StartNew();
            try
            {
                providerResponse = GetProviderResponse(airFareSearchRequest, providerSpace);
            }
            finally
            {
                supplierGetTimer.Stop();
                int resultCount;
                bool success;
                string message;

                GetResultCountNSuccess(providerResponse, out success, out resultCount, out message);

                var data = new FlightTimeNSupplierAwareAnalyticalData(context.AccountId, context.SessionId.ToString(),
                                                                 KeyStore.Analytics.FlightSearchApplicationId,
                                                                 KeyStore.Analytics.Titles.SupplierRoot,
                                                                 KeyStore.Analytics.ResponseTimeTag, resultCount, success,
                                                                 message, airFareSearchRequest.SupplierIdentifier,
                                                                 providerSpace.ID.ToString(CultureInfo.InvariantCulture), supplierGetTimer.Elapsed);

                AnalyticWriterFacade.WriteAnalyticData(data);
            }

            if (cacheExpirationTime > 0 && IsResponseValidToBeCached(providerResponse))
                TaskFactoryFactory.GetTaskFactory(SchedulerTypeOptions.LimitedConcurrencyScheduler).StartAmbient(
                    () => AddScepterResponseToCache(cacheKey, providerResponse, cacheExpirationTime));

            context.TrySetMaxSupplierTime(supplierGetTimer.Elapsed.TotalMilliseconds);

            return providerResponse;
        }

        private FareSearchRS GetCachedScepterResponse(AirFareSearchRQ airFareSearchRequest, out string cacheKey)
        {
            var stopwatch = new WrappedTimer();
            cacheKey = GetCacheKeyForSearch(airFareSearchRequest);

            stopwatch.Start();
            var cachedScepterResponse = CacheProvider.Get<FareSearchRS>(KeyStore.CacheBucket.ScepterCacheBucket, cacheKey);
            stopwatch.Stop();

            CheckCacheResponseTime(cacheKey, stopwatch.ElapsedSeconds);

            return cachedScepterResponse;
        }

        private void CheckCacheResponseTime(string key, double elapsedTime)
        {
            if (elapsedTime > 1)
            {
                var logEntry = Utility.GetEventEntryContextual();
                logEntry.SeverityType = elapsedTime > 2 ? SeverityOptions.Critical : SeverityOptions.Warning;
                logEntry.Title = "ScepterCacheOperation";
                logEntry.AddMessage(string.Format("Cache operation Taking longer Time then usual for key {0} and Time Taken : {1}", key, elapsedTime));
                Utility.GetLogFactory().WriteAsync(logEntry, TravelNxt.Common.Settings.KeyStore.LogCategories.Default);
            }
        }

        private static bool IsResponseValidForPassengerTypes(IEnumerable<PassengerType> passengerTypes, FareSearchRS cachedResponse)
        {
            List<PassengerType> existingPassengerTypes;
            if (cachedResponse.LegFares != null && cachedResponse.LegFares.Count > 0)
                existingPassengerTypes =
                    cachedResponse.LegFares.First().PassengerFareBreakups.Select(
                        x => x.PassengerTypeQuantity.PassengerType).ToListBuffered();
            else
                existingPassengerTypes = cachedResponse.ItineraryFares.First().PassengerFareBreakups.Select(
                        x => x.PassengerTypeQuantity.PassengerType).ToListBuffered();

            return passengerTypes.All(passengerType => existingPassengerTypes.Contains(passengerType));
        }

        protected virtual FareSearchRS GetProviderResponse(AirFareSearchRQ airFareSearchRequest, ProviderSpace providerSpace)
        {
            try
            {
                var searchConfiguration =
                    providerSpace.Configurations.First(
                        x => x.ConfigType == ConfigurationType.Search);

                FareSearchRS response;
                using (var client = new AirFareSearchV1Client(KeyStore.ClientEndpoints.AirFareSearchServiceProtobuf,
                                                              searchConfiguration.ServiceUri.ToString().TrimEnd('/') +
                                                              KeyStore.ClientEndpoints.AirFareSearchServiceUriSuffix))
                {
                    client.InnerChannel.OperationTimeout =
                    TimeSpan.FromSeconds(SettingManager.SupplierThreadTimeoutInSeconds);

                    Utility.LogDiagnosticEntry("sending request to supplier, provider id: " + providerSpace.ID);
                    try
                    {
                        response = client.GetFares(airFareSearchRequest);
                    }
                    catch (Exception ex)
                    {
                        var supplierException =
                            new FlightSupplierException(
                                "Flight supplier { Name : " + providerSpace.Family + " } { Id : " + providerSpace.ID + " }" +
                                " threw an exception, see inner exception for details.", ex);

                        Utility.GetLogFactory().WriteAsync(supplierException.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);

                        throw new FlightSupplierException(supplierException.Message, new HandledException(supplierException));
                    }

                    Utility.LogDiagnosticEntry("recieved response from supplier, provider id: " + providerSpace.ID);
                }

                return response;
            }
            catch (FlightSupplierException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
        }

        #region Translation

        #region ToEntity

        public static FlightAvailResult ToEntity(FareSearchRS fareSearchResponse, FlightAvailRq flightAvailRq)
        {
            return new FlightAvailResult()
            {
                Status = ToEntity(fareSearchResponse.Status.ResponseStatus),
                AdditionalInfo = null, //TODO: should something be done here?
                FlightRecommendations = ToEntityRecommendation(fareSearchResponse, flightAvailRq)
            };
        }

        public static List<FlightRecommendation> ToEntityRecommendation(FareSearchRS fareSearchResponse, FlightAvailRq flightAvailRq)
        {
            if (fareSearchResponse == null || fareSearchResponse.Status == null || fareSearchResponse.Status.ResponseStatus == StatusType.Failure || fareSearchResponse.LegGroups == null)
                return new List<FlightRecommendation>();

            var count = 1;
            if (fareSearchResponse.ItineraryFares != null)
                count += fareSearchResponse.ItineraryFares.Count;
            if (fareSearchResponse.LegFares != null)
                count += fareSearchResponse.LegFares.Count;


            var flightRecommendations = new List<FlightRecommendation>(count);

            var itineraryOptions = GetItineraryType(flightAvailRq.SearchCriteria.SearchSegments);

            //Add itinerary fare
            if (fareSearchResponse.ItineraryFares != null && fareSearchResponse.ItineraryFares.Count != 0)
            {
                flightRecommendations.AddRange(fareSearchResponse.ItineraryFares.Select(fare =>
                                                                                CreateFlightRecommendation(
                                                                                    fareSearchResponse,
                                                                                    itineraryOptions,
                                                                                    flightAvailRq.ProviderSpace,
                                                                                    flightAvailRq.SearchCriteria.
                                                                                        PassengerInfoSummary,
                                                                                    fare, true)));
            }

            //Add leg fare 
            if (fareSearchResponse.LegFares != null && fareSearchResponse.LegFares.Count != 0)
            {
                flightRecommendations.AddRange(fareSearchResponse.LegFares.Select(
                    fare =>
                    CreateFlightRecommendation(fareSearchResponse,
                                               ItineraryTypeOptions.OneWay,
                                               flightAvailRq.ProviderSpace,
                                               flightAvailRq.SearchCriteria.
                                                   PassengerInfoSummary, fare,
                                               false)));
            }

            flightRecommendations.TrimExcess();

            //Add index numbers
            var index = 0;
            foreach (var flightRecommendation in flightRecommendations)
            {
                flightRecommendation.RefId = index;
                index++;
            }

            return flightRecommendations;
        }

        private static ItineraryTypeOptions GetItineraryType(IList<SearchSegment> searchSegments)
        {
            if (searchSegments.Count == 1)
                return ItineraryTypeOptions.OneWay;

            if (searchSegments.Count == 2 && (searchSegments[0].GetDepartureLocation() == searchSegments[1].GetArrivalLocation()))
                return ItineraryTypeOptions.RoundTrip;

            return ItineraryTypeOptions.MultiCity;
        }

        private static FlightRecommendation CreateFlightRecommendation(FareSearchRS fareSearchRs, ItineraryTypeOptions itineraryTypeOptions, ProviderSpace providerSpace, IList<PaxTypeQuantity> paxTypeQuantities, Fare fare, bool isItineraryFare)
        {
            if (fareSearchRs.Flights == null)
                fareSearchRs.Flights = new List<Data.Services.AirFareSearchV1.Flight>();

            return new FlightRecommendation
            {
                Fare = fare.GetFlightFare(paxTypeQuantities, providerSpace),
                Legs = new FlightLegCollection(fareSearchRs.LegGroups.ToLegDetails(
                        fareSearchRs.Flights, fare, paxTypeQuantities, providerSpace, fareSearchRs.Baggages, itineraryTypeOptions)),
                ProviderSpace = providerSpace,
                ItineraryType = itineraryTypeOptions,
                RateCode = fare.AccountCode,
                PlatingCarrier = fare.PlatingCarrier,
                AdditionalInfo = new AdditionalInfoDictionary(GetSupplierAdditionalInformation(fare.SupplierRefKey,
                                                                                GetFareType(fare, isItineraryFare)))
            };
        }

        private static IEnumerable<KeyValuePair<string, string>> GetSupplierAdditionalInformation(string supplierRefKey, string legType)
        {
            var additionalInformationCollection = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrEmpty(supplierRefKey))
                additionalInformationCollection.Add(
                    new KeyValuePair<string, string>(KeyStore.Scepter.FareSearchSupplierFareRefKey, supplierRefKey));

            if (!string.IsNullOrEmpty(legType))
                additionalInformationCollection.Add(new KeyValuePair<string, string>("LegType", legType));

            return additionalInformationCollection;
        }

        private static string GetFareType(Fare fare, bool isItineraryFare)
        {
            if (isItineraryFare) return string.Empty;
            return fare.FareContext.Exists(x => x.LegGroupNumber == 1) ? "Onward" : "Return";
        }

        public static Entities.Money ToEntity(Data.Services.FareSearch.Money money)
        {
            var entity = new Entities.Money(money.Amount, money.CurrencyCode);

            return entity;
        }

        public static Entities.FareComponent ToEntity(Data.Services.AirFareSearchV1.FareComponent fareComponent)
        {
            return new Entities.FareComponent()
            {
                Code = fareComponent.Code,
                Description = fareComponent.Description,
                Value = new Entities.Money(fareComponent.Amount, fareComponent.CurrencyCode),
                FareComponentType = ToEntity(fareComponent.Type)
            };
        }

        public static Entities.CabinType ToEntity(CabinType cabinType)
        {
            switch (cabinType)
            {
                case CabinType.Business:
                    return Entities.CabinType.Business;
                case CabinType.Economy:
                    return Entities.CabinType.Economy;
                case CabinType.First:
                    return Entities.CabinType.First;
                case CabinType.PremiumEconomy:
                    return Entities.CabinType.PremiumEconomy;
                case CabinType.Unknown:
                    return Entities.CabinType.Unknown;
                default:
                    return Entities.CabinType.Unknown;
            }
        }

        public static Entities.PassengerType ToEntity(PassengerType passengerType)
        {
            switch (passengerType)
            {
                case PassengerType.Adult:
                    return Entities.PassengerType.Adult;
                case PassengerType.Child:
                    return Entities.PassengerType.Child;
                case PassengerType.InfantInLap:
                    return Entities.PassengerType.Infant;
                case PassengerType.InfantWithSeat:
                    return Entities.PassengerType.Child;
                case PassengerType.Senior:
                    return Entities.PassengerType.Senior;
                case PassengerType.Youth:
                    return Entities.PassengerType.Adult;
                case PassengerType.Government:
                    return Entities.PassengerType.Adult;
                default:
                    return Entities.PassengerType.Adult;
            }
        }

        public static Entities.FareComponentType ToEntity(FareComponentType fareComponentType)
        {
            switch (fareComponentType)
            {
                case FareComponentType.Discount:
                    return Entities.FareComponentType.Discount;
                case FareComponentType.CommissionAmount:
                    return Entities.FareComponentType.Commision;
                case FareComponentType.Fee:
                    return Entities.FareComponentType.Fees;
                case FareComponentType.Tax:
                    return Entities.FareComponentType.Tax;
                case FareComponentType.Penalty:
                case FareComponentType.EquivalentAmount:
                default:
                    return default(Entities.FareComponentType);
            }
        }

        public static Entities.FareType ToEntity(FareType fareType)
        {
            switch (fareType)
            {
                case FareType.Published:
                    return Entities.FareType.Published;
                case FareType.Private:
                    return Entities.FareType.Negotiated; //TODO: verify.
                case FareType.NotSpecified:
                    return Entities.FareType.Unknown;
                case FareType.Corporate:
                    return Entities.FareType.Corporate; //TODO: verify.
                case FareType.Agency:
                    return Entities.FareType.Negotiated; //TODO: verify.
                default:
                    return Entities.FareType.Unknown;
            }
        }

        public static CallStatus ToEntity(StatusType statusType)
        {
            switch (statusType)
            {
                case StatusType.Failure:
                    return CallStatus.Failure;
                case StatusType.Success:
                    return CallStatus.Success;
                case StatusType.Warning:
                    return CallStatus.Warning;
            }

            return default(CallStatus);
        }

        #endregion

        #region ToContract

        public static AirFareSearchRQ ToDataContract(FlightAvailRq flightAvailRq)
        {
            //flightAvailRq.SearchCriteria.
            //TODO: include service class handling
            var contract = new AirFareSearchRQ()
            {
                SupplierIdentifier = flightAvailRq.ProviderSpace.Family,
                Currency = GetCurrencyCode(flightAvailRq.ProviderSpace.Family),
                Requester = ToDataContract(flightAvailRq.ProviderSpace.Requester),
                AirlinesPreference = ToDataContract(flightAvailRq.SearchCriteria.FlightTravelPreference),
                Culture = Utility.GetCurrentContext().Culture.Name,
                FareSpace = ToDataContract(flightAvailRq.ProviderSpace),
                Passengers = flightAvailRq.SearchCriteria.PassengerInfoSummary.Select(x => ToDataContract(x, flightAvailRq.ProviderSpace.AdditionalInfo)).ToListBuffered(),
                FarePreference = ToDataContractFarePreference(flightAvailRq.SearchCriteria.FlightTravelPreference),
                FlightTypeOption = ToDataContract(flightAvailRq.SearchCriteria.FlightTravelPreference, flightAvailRq.SearchCriteria.SearchSegments),
                JourneyLegs = flightAvailRq.SearchCriteria.SearchSegments.Select(ToDataContract).ToListBuffered(),
                TripType = GetTripType(flightAvailRq.SearchCriteria.SearchSegments),
                TimeoutInSeconds = SettingManager.SupplierThreadTimeoutInSeconds
            };

            return contract;
        }

        private static string GetCurrencyCode(string supplierFamily)
        {
            var supplierSettingProvider = RuntimeContext.Resolver.Resolve<ISupplierConfigurationProvider>();
            var currentContext = Utility.GetCurrentContext();
            var currencyPreference = new CurrencyPreference(currentContext.DisplayCurrency, currentContext.BaseCurrency,
                                                            KeyStore.DefaultCurrency);

            return supplierSettingProvider.GetApplicableCurrency(supplierFamily, currentContext.PosId,
                                                                 currencyPreference);
        }

        private static TripType GetTripType(IList<SearchSegment> searchSegments)
        {
            if (searchSegments.Count == 1)
                return TripType.OneWay;

            if (searchSegments.Count == 2 && (searchSegments[0].GetDepartureLocation() == searchSegments[1].GetArrivalLocation()))
                return TripType.ReturnTrip;

            return TripType.MultiCity;
        }

        public static Requester ToDataContract(Entities.Requester requester)
        {
            //TODO: fill security info
            var currentContext = Utility.GetCurrentContext();
            var contract = new Requester()
            {
                AffiliateId = requester.ID.ToString(CultureInfo.InvariantCulture),
                TrackingId = currentContext.SessionId.ToString()
            };

            return contract;
        }

        public static AirlinesPreference ToDataContract(FlightTravelPreference flightTravelPreference)
        {
            if (flightTravelPreference == null || flightTravelPreference.AirlinesPreference == null)
                return null;

            var airlinePreference = flightTravelPreference.AirlinesPreference;

            var contract = new AirlinesPreference()
            {
                PermittedAirlines =
                    airlinePreference.PermittedAirlines == null
                        ? null
                        : airlinePreference.PermittedAirlines.ToListBuffered(),
                ProhibitedAirlines =
                    airlinePreference.ProhibitedAirlines == null
                        ? null
                        : airlinePreference.ProhibitedAirlines.ToListBuffered(),
                PreferredAirlines =
                    airlinePreference.PreferredAirlines == null
                        ? null
                        : airlinePreference.PreferredAirlines.ToListBuffered()
            };

            return contract;
        }

        public static FlightTypeOption ToDataContract(
            FlightTravelPreference flightTravelPreference,
           IEnumerable<SearchSegment> searchSegments)
        {
            if (flightTravelPreference == null)
                return null;

            var flightTypeOption = new FlightTypeOption();

            if (!flightTravelPreference.AllowMixedAirlines)
                flightTypeOption.OnlineConnectionsOnly = true;
            flightTypeOption.JetPlanesOnly = flightTravelPreference.JetFlightsOnly;
            flightTypeOption.DirectFlightOnly = flightTravelPreference.IncludeDirectOnly;
            flightTypeOption.NonStopFlightOnly = flightTravelPreference.IncludeNonStopOnly;

            return flightTypeOption;
        }

        public static FareSpace ToDataContract(ProviderSpace providerSpace)
        {
            var fareTypes = GetApplicableFareTypes(providerSpace);
            List<string> accountCodes;
            List<string> corporateCodes;
            PopulateAccountAndCorporateCodes(providerSpace.Contracts, out accountCodes, out corporateCodes);

            return new FareSpace()
            {
                Code = providerSpace.ID.ToString(CultureInfo.InvariantCulture),
                AdditionalParams = GetAdditionalParams(providerSpace.AdditionalInfo),
                Qualifiers = new FareSpaceQualifiers()
                {
                    FareTypes = fareTypes,
                    AccountCodes = accountCodes,
                    CorporateCodes = corporateCodes
                }
            };
        }

        private static List<KeyValue> GetAdditionalParams(AdditionalInfoDictionary additionalInfoDictionary)
        {
            if (additionalInfoDictionary == null)
                return null;

            var valuesDictionary = AdditionalInfoDictionary.ToDictionary(additionalInfoDictionary);

            return valuesDictionary.Keys.Select(key => new KeyValue() { Key = key, Value = valuesDictionary[key] }).ToListBuffered();
        }

        private static void PopulateAccountAndCorporateCodes(ICollection<AirContract> airContracts, out List<string> accountCodes, out List<string> corporateCodes)
        {
            accountCodes = new List<string>();
            corporateCodes = new List<string>();

            if (airContracts == null || airContracts.Count == 0)
                return;

            foreach (var contract in airContracts)
            {
                var fareType = contract.FareType;

                if (!string.IsNullOrEmpty(contract.ContractRateCode))
                {
                    if (fareType.Equals(KeyStore.FareType.Negotiated)) accountCodes.Add(contract.ContractRateCode);
                    if (fareType.Equals(KeyStore.FareType.Corporate)) corporateCodes.Add(contract.ContractRateCode);
                }
            }
        }

        private static List<FareType> GetApplicableFareTypes(ProviderSpace providerSpace)
        {
            var applicableFareTypes = new List<FareType>();

            string isPublishedFareOnly, isPrivateFareOnly, isCorporateFareOnly;
            providerSpace.AdditionalInfo.TryGetValue(KeyStore.AllowedFareTypes.IsPublishedFareOnly, out isPublishedFareOnly);
            providerSpace.AdditionalInfo.TryGetValue(KeyStore.AllowedFareTypes.IsPrivateFareOnly, out isPrivateFareOnly);
            providerSpace.AdditionalInfo.TryGetValue(KeyStore.AllowedFareTypes.IsCorporateFareOnly, out isCorporateFareOnly);

            isPublishedFareOnly = isPublishedFareOnly ?? string.Empty;
            isPrivateFareOnly = isPrivateFareOnly ?? string.Empty;
            isCorporateFareOnly = isCorporateFareOnly ?? string.Empty;

            if (isPublishedFareOnly.Equals(KeyStore.BooleanValues.True, StringComparison.OrdinalIgnoreCase))
                applicableFareTypes.Add(FareType.Published);

            if (isPrivateFareOnly.Equals(KeyStore.BooleanValues.True, StringComparison.OrdinalIgnoreCase))
                applicableFareTypes.Add(FareType.Private);

            if (isCorporateFareOnly.Equals(KeyStore.BooleanValues.True, StringComparison.OrdinalIgnoreCase))
                applicableFareTypes.Add(FareType.Corporate);

            //Fail safe....in case nothing has been specified default to published.
            if (applicableFareTypes.Count == 0)
                applicableFareTypes.Add(FareType.Published);

            return applicableFareTypes;
        }

        public static PassengerTypeQuantity ToDataContract(PaxTypeQuantity paxTypeQuantity, AdditionalInfoDictionary providerAdditionalInfoDictionary)
        {
            var passengerTypeQuantity = new PassengerTypeQuantity()
            {
                PassengerType = ToDataContract(paxTypeQuantity.PassengerType),
                Quantity = paxTypeQuantity.Quantity
            };

            if (passengerTypeQuantity.PassengerType == PassengerType.Mapped)
            {
                var mappedCode = string.Empty;
                if (providerAdditionalInfoDictionary != null)
                    providerAdditionalInfoDictionary.TryGetValue(
                        paxTypeQuantity.PassengerType + KeyStore.ProviderSpaceAttributes.PrivateCode, out mappedCode);
                if (string.IsNullOrEmpty(mappedCode))
                    throw new InvalidDataException("Mapped code not found for passengertype : " +
                                                   paxTypeQuantity.PassengerType);

                passengerTypeQuantity.MappedPaxTypeCode = mappedCode;
                return passengerTypeQuantity;
            }

            string isPrivateFareOnly;
            providerAdditionalInfoDictionary.TryGetValue(KeyStore.ProviderSpaceAttributes.IsPrivateFareOnly, out isPrivateFareOnly);
            bool isPrivateFare;
            bool.TryParse(isPrivateFareOnly, out isPrivateFare);
            string paxCode = null;
            providerAdditionalInfoDictionary.TryGetValue(paxTypeQuantity.PassengerType.ToString() + KeyStore.ProviderSpaceAttributes.PrivateCode, out paxCode);
            if (!string.IsNullOrEmpty(paxCode))
            {
                passengerTypeQuantity.PassengerType = PassengerType.Mapped;
                passengerTypeQuantity.MappedPaxTypeCode = paxCode;
            }

            return passengerTypeQuantity;
        }

        public static JourneyLegCriteria ToDataContract(SearchSegment searchSegment)
        {
            string[] arrivalAlternateAirports;
            var arrivalTargetAirportRadius = GetAlternateAirportInfo(searchSegment.ArrivalAlternateAirportInformation, out arrivalAlternateAirports);

            string[] departureAlternateAirports;
            var departureTargetAirportRadius = GetAlternateAirportInfo(searchSegment.ArrivalAlternateAirportInformation, out departureAlternateAirports);

            var criteria = new JourneyLegCriteria()
            {
                ArrivalLocationSpec = new LocationSpec()
                {
                    Code = searchSegment.GetArrivalLocation(),
                    AlternateAirports = new List<string>(arrivalAlternateAirports),
                    LocationType = searchSegment.IsArrivalSpecCity() ? LocationType.City : LocationType.Airport,
                    RadiusKm = arrivalTargetAirportRadius
                },
                DepartureLocationSpec = new LocationSpec()
                {
                    Code = searchSegment.GetDepartureLocation(),
                    AlternateAirports = new List<string>(departureAlternateAirports),
                    LocationType = searchSegment.IsDepartureSpecCity() ? LocationType.City : LocationType.Airport,
                    RadiusKm = departureTargetAirportRadius
                },
                CabinType = ToDataContract(searchSegment.Cabin),
                TravelDateSpec = GetTravelDateForSegment(searchSegment),
                ConnectionPreference = new ConnectionPreference()
                {
                    IncludeConnections = searchSegment.ConnectionPreferences
                        .Where(x => x.PreferenceType == ConnectionPreferenceType.Via)
                        .Select(x => x.AirportCode).ToListBuffered(),

                    ExcludeConnections = searchSegment.ConnectionPreferences
                        .Where(x => x.PreferenceType == ConnectionPreferenceType.Avoid)
                        .Select(x => x.AirportCode).ToListBuffered()
                },
                BookingClassPreference = new BookingClassPreference()
            };

            criteria.BookingClassPreference.PermittedBookingClasses = searchSegment.IncludeServiceClass == null
                                                                          ? null
                                                                          : searchSegment.IncludeServiceClass.ToListBuffered();

            return criteria;
        }

        private static TravelDateSpec GetTravelDateForSegment(SearchSegment segment)
        {
            var travelDate = segment.TravelDate;

            if (travelDate.MinDate.HasValue || travelDate.MaxDate.HasValue)
            {
                var flexiDate = new FlexTravelDateSpec()
                {
                    TravelDate = travelDate.DateTime,
                    TravelDateType = TravelDateType.Depature,
                    MinDay = travelDate.MinDate.HasValue ? travelDate.MinDate.Value : 0,
                    MaxDay = travelDate.MaxDate.HasValue ? travelDate.MaxDate.Value : 0
                };
                return flexiDate;
            }

            return new FixedTravelDateSpec()
            {
                AnyTime = travelDate.AnyTime,
                TimeWindow = travelDate.TimeWindow,
                TravelDate = travelDate.DateTime,
                TravelDateType = TravelDateType.Depature,
            };
        }

        private static int GetAlternateAirportInfo(AlternateAirportInformation altAirportInfo, out string[] alternateAirports)
        {
            var targetAirportRadius = 0;

            alternateAirports = null;

            if (altAirportInfo != null && !altAirportInfo.IsEmpty())
            {
                if (altAirportInfo.IncludeNearByAirports.HasValue && altAirportInfo.IncludeNearByAirports.Value)
                {
                    if (altAirportInfo.RadiusKm.HasValue)
                    {
                        targetAirportRadius = altAirportInfo.RadiusKm.Value;
                    }
                    else
                    {
                        targetAirportRadius = SettingManager.NearbyAirportRadius;
                    }
                }
                else if (altAirportInfo.AirportCodes != null && altAirportInfo.AirportCodes.Count > 0)
                {
                    alternateAirports = altAirportInfo.AirportCodes.ToArray();
                }
            }
            alternateAirports = alternateAirports ?? new string[0];
            return targetAirportRadius;
        }

        public static FarePreference ToDataContractFarePreference(FlightTravelPreference flightTravelPreference)
        {
            if (flightTravelPreference == null)
                return null;

            var farePreference = new FarePreference();

            if (flightTravelPreference.Refundable.HasValue && flightTravelPreference.Refundable == true)
                farePreference.RefundableFares = true;

            farePreference.NoRestriction = flightTravelPreference.UnRestrictedFare;
            farePreference.NoPenalty = flightTravelPreference.NoPenalty;
            return farePreference;
        }

        public static PassengerType ToDataContract(Entities.PassengerType passengerType)
        {
            switch (passengerType)
            {
                case Entities.PassengerType.Adult:
                    return PassengerType.Adult;
                case Entities.PassengerType.Child:
                    return PassengerType.Child;
                case Entities.PassengerType.Infant:
                    return PassengerType.InfantInLap;
                case Entities.PassengerType.Senior:
                    return PassengerType.Senior;
                case Entities.PassengerType.MilitaryAdult:
                case Entities.PassengerType.MilitaryChild:
                case Entities.PassengerType.MilitaryInfant:
                case Entities.PassengerType.MilitarySenior:
                case Entities.PassengerType.Mapped:
                    return PassengerType.Mapped;
                default:
                    return PassengerType.Adult;
            }
        }

        public static CabinType ToDataContract(Entities.CabinType cabinType)
        {
            switch (cabinType)
            {
                case Entities.CabinType.Business:
                    return CabinType.Business;
                case Entities.CabinType.Economy:
                    return CabinType.Economy;
                case Entities.CabinType.First:
                    return CabinType.First;
                case Entities.CabinType.PremiumEconomy:
                    return CabinType.PremiumEconomy;
                case Entities.CabinType.Unknown:
                default:
                    return CabinType.Unknown;
            }
        }

        #endregion

        #endregion

        #region Cache Data Retrieval/manipulation

        private void AddScepterResponseToCache(string cacheKey, FareSearchRS response, int expirationTime)
        {
            var stopwatch = new WrappedTimer();
            try
            {
                stopwatch.Start();
                CacheProvider.Insert(KeyStore.CacheBucket.ScepterCacheBucket, cacheKey, response,
                                     new ExpirationSettings
                                     {
                                         ExpirationType = ExpirationType.AbsoluteExpiration,
                                         AbsoluteExpiration = DateTime.Now.AddMinutes(expirationTime)
                                     });
                stopwatch.Stop();
                CheckCacheResponseTime(cacheKey, stopwatch.ElapsedSeconds);
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(PriorityOptions.High), KeyStore.LogCategories.ExceptionCategories.Critical);
            }
        }

        private static int GetCacheExpirationTime(FareSearchRQ airFareSearchRequest)
        {
            var firstJourney = airFareSearchRequest.JourneyLegs.FirstOrDefault();

            if (firstJourney == null)
                throw new InvalidDataException("The journey legs were empty, this was not expected.");

            var departureDate = firstJourney.TravelDateSpec.TravelDate;

            var daysToDepart = (departureDate - DateTime.UtcNow).Days;

            var expirationTime = SettingManager.GetExpirationTime(daysToDepart);

            return expirationTime;
        }

        private static bool IsResponseValidToBeCached(FareSearchRS response)
        {
            if (response != null && response.Status != null && response.Status.ResponseStatus != StatusType.Failure)
                if ((response.LegFares != null && response.LegFares.Count > 0) || (response.ItineraryFares != null && response.ItineraryFares.Count > 0))
                    return true;

            return false;
        }

        private static FareSearchRS GetModifiedResponseAsPerRequest(AirFareSearchRQ fareSearchRequest,
            FareSearchRS cachedResponse, ProviderSpace providerSpace)
        {
            Utility.LogDiagnosticEntry("cached results found, modifying responses, provider id: " + providerSpace.ID);

            var stopwatch = Stopwatch.StartNew();
            var responseToReturn = new FareSearchRS()
            {
                ExtensionData = cachedResponse.ExtensionData,
                Flights = cachedResponse.Flights,
                LegGroups = cachedResponse.LegGroups,
                Status = cachedResponse.Status,
                LegFares =
                    cachedResponse.LegFares == null
                        ? new List<Fare>()
                        : new List<Fare>(cachedResponse.LegFares),
                ItineraryFares =
                    cachedResponse.ItineraryFares == null
                        ? new List<Fare>()
                        : new List<Fare>(cachedResponse.ItineraryFares),
                Baggages =
                    cachedResponse.Baggages == null
                        ? new List<Baggage>()
                        : new List<Baggage>(cachedResponse.Baggages),
                TrackingId = Utility.GetCurrentContext().SessionId.ToString()
            };

            //Shallow copy of fares need to be created to avoid affecting the passengerType information
            //int the cached response

            var requiredPaxQuantity = fareSearchRequest.Passengers.Sum(x => x.Quantity);
            var fares = cachedResponse.LegFares != null && cachedResponse.LegFares.Count > 0
                            ? cachedResponse.LegFares
                            : cachedResponse.ItineraryFares;

            if (fares == null)
                throw new InvalidOperationException("Either of cachedResponse.LegFares or cachedResponse.ItineraryFares cannot be null or empty");

            var cachedPaxQuantity = fares.First().PassengerFareBreakups.Sum(x => x.PassengerTypeQuantity.Quantity);

            if (cachedResponse.LegFares != null)
                for (var i = 0; i < cachedResponse.LegFares.Count; i++)
                {
                    responseToReturn.LegFares[i] = GetFareShallowCopy(responseToReturn.LegFares[i]);
                    responseToReturn.LegFares[i].PassengerFareBreakups =
                        cachedResponse.LegFares[i].PassengerFareBreakups.GetTransformedFareBreakups(
                            fareSearchRequest.Passengers);
                    FareTransforms.TransformFareLevelComponents(cachedResponse.LegFares[i], responseToReturn.LegFares[i],
                                                                cachedPaxQuantity, requiredPaxQuantity);
                }

            if (cachedResponse.ItineraryFares != null)
                for (var i = 0; i < cachedResponse.ItineraryFares.Count; i++)
                {
                    responseToReturn.ItineraryFares[i] = GetFareShallowCopy(cachedResponse.ItineraryFares[i]);
                    responseToReturn.ItineraryFares[i].PassengerFareBreakups =
                        cachedResponse.ItineraryFares[i].PassengerFareBreakups.GetTransformedFareBreakups(
                            fareSearchRequest.Passengers);
                    FareTransforms.TransformFareLevelComponents(cachedResponse.ItineraryFares[i], responseToReturn.ItineraryFares[i],
                                                                cachedPaxQuantity, requiredPaxQuantity);
                }

            stopwatch.Stop();

            var context = Utility.GetCurrentContext();

            int resultCount;
            bool success;
            string message;

            GetResultCountNSuccess(responseToReturn, out success, out resultCount, out message);

            var data = new FlightTimeNSupplierAwareAnalyticalData(context.AccountId, context.SessionId.ToString(),
                                                             KeyStore.Analytics.FlightSearchApplicationId,
                                                             KeyStore.Analytics.Titles.SupplierCacheHit,
                                                             KeyStore.Analytics.ResponseTimeTag,
                                                             resultCount, success, message,
                                                             fareSearchRequest.SupplierIdentifier,
                                                             providerSpace.ID.ToString(CultureInfo.InvariantCulture), stopwatch.Elapsed);

            AnalyticWriterFacade.WriteAnalyticData(data);

            return responseToReturn;
        }

        private static void GetResultCountNSuccess(FareSearchRS searchRS, out bool success, out int resultCount, out string message)
        {
            resultCount = 0;
            success = false;
            message = string.Empty;

            if (searchRS == null)
                return;

            if (searchRS.ItineraryFares != null)
                resultCount = searchRS.ItineraryFares.Count;

            if (searchRS.LegFares != null)
                resultCount += searchRS.LegFares.Count;

            if (searchRS.Status.ResponseStatus != StatusType.Failure)
                success = true;
        }

        private static Fare GetFareShallowCopy(Fare fareToCopy)
        {
            return new Fare()
            {
                AccountCode = fareToCopy.AccountCode,
                BaseAmount = fareToCopy.BaseAmount,
                Discounts = fareToCopy.Discounts,
                ExtensionData = fareToCopy.ExtensionData,
                FareContext = fareToCopy.FareContext,
                FareType = fareToCopy.FareType,
                Fees = fareToCopy.Fees,
                LastTicketableDate = fareToCopy.LastTicketableDate,
                PassengerFareBreakups = fareToCopy.PassengerFareBreakups,
                Penalties = fareToCopy.Penalties,
                PlatingCarrier = fareToCopy.PlatingCarrier,
                SupplierRefKey = fareToCopy.SupplierRefKey,
                Taxes = fareToCopy.Taxes,
                TotalAmount = fareToCopy.TotalAmount,
                TotalDiscounts = fareToCopy.TotalDiscounts,
                TotalEquivalentAmount = fareToCopy.TotalEquivalentAmount,
                TotalFees = fareToCopy.TotalFees,
                TotalTaxes = fareToCopy.TotalTaxes,
                FareAttributes = fareToCopy.FareAttributes
            };
        }

        private static string GetCacheKeyForSearch(AirFareSearchRQ airFareSearchRQ)
        {
            return GetCacheKey(airFareSearchRQ).GetHashCode().ToString(CultureInfo.InvariantCulture);
        }

        #region CacheKeyGeneration

        private static string GetCacheKey(AirFareSearchRQ airFareSearchRQ)
        {
            var stringBuilder = new StringBuilder();

            SerializeAirlinePreference(stringBuilder, airFareSearchRQ.AirlinesPreference);

            stringBuilder.Append(airFareSearchRQ.Culture).Append(airFareSearchRQ.Currency).Append(
                airFareSearchRQ.SupplierIdentifier).Append(airFareSearchRQ.TripType);

            var supplierCacheKey = GetSupplierCacheGroup(airFareSearchRQ);
            if (string.IsNullOrEmpty(supplierCacheKey))
                stringBuilder.Append(airFareSearchRQ.FareSpace.Code).Append(airFareSearchRQ.FareSpace.TargetPcc);
            else
                stringBuilder.Append(supplierCacheKey);

            SerializeFarePreference(stringBuilder, airFareSearchRQ.FarePreference);

            SerializeFlightTypeOption(stringBuilder, airFareSearchRQ.FlightTypeOption);

            airFareSearchRQ.JourneyLegs.ForEach(journeyLeg => SerializeLegInformation(stringBuilder, journeyLeg));

            return stringBuilder.ToString();
        }

        private static string GetSupplierCacheGroup(AirFareSearchRQ airFareSearchRQ)
        {
            string supplierResponseCacheGroupName = null;

            if (airFareSearchRQ.FareSpace != null && airFareSearchRQ.FareSpace.AdditionalParams != null && airFareSearchRQ.FareSpace.AdditionalParams.Count > 0)
            {
                var cacheGroupName = airFareSearchRQ.FareSpace.AdditionalParams.Find(x => x.Key.Equals("CacheGroupName"));
                if(cacheGroupName == null)
                    return null;
                if (string.IsNullOrEmpty(cacheGroupName.Value) == false)
                    supplierResponseCacheGroupName = cacheGroupName.Value.Trim();
            }
            return supplierResponseCacheGroupName;
        }

        private static void SerializeLegInformation(StringBuilder stringBuilder, JourneyLegCriteria journeyLeg)
        {
            SerializeLocationSpec(stringBuilder, journeyLeg.ArrivalLocationSpec);
            SerializeLocationSpec(stringBuilder, journeyLeg.DepartureLocationSpec);
            SerializeConnectionPreference(stringBuilder, journeyLeg.ConnectionPreference);
            stringBuilder.Append(journeyLeg.CabinType).Append(
                journeyLeg.TravelDateSpec.TravelDate).Append(
                    journeyLeg.TravelDateSpec.TravelDateType);
            SerializeBookingClassPreference(stringBuilder, journeyLeg.BookingClassPreference);
        }

        private static void SerializeLocationSpec(StringBuilder stringBuilder, LocationSpec locationSpec)
        {
            locationSpec.AlternateAirports.ForEach(x => stringBuilder.Append(x));
            stringBuilder.Append(locationSpec.Code).Append(locationSpec.LocationType).Append(locationSpec.RadiusKm);
        }

        private static void SerializeConnectionPreference(StringBuilder stringBuilder, ConnectionPreference connectionPreference)
        {
            connectionPreference.ExcludeConnections.ForEach(x => stringBuilder.Append(x));
            connectionPreference.IncludeConnections.ForEach(x => stringBuilder.Append(x));
        }

        private static void SerializeBookingClassPreference(StringBuilder stringBuilder, BookingClassPreference bookingClassPreference)
        {
            if (bookingClassPreference == null)
                return;

            if (bookingClassPreference.PermittedBookingClasses != null)
            {
                stringBuilder.Append("Perm");
                bookingClassPreference.PermittedBookingClasses.ForEach(x => stringBuilder.Append(x));
            }
            if (bookingClassPreference.ProhibitedBookingClasses != null)
            {
                stringBuilder.Append("Proh");
                bookingClassPreference.ProhibitedBookingClasses.ForEach(x => stringBuilder.Append(x));
            }
            if (bookingClassPreference.PreferredBookingClasses != null)
            {
                stringBuilder.Append("Pref");
                bookingClassPreference.PreferredBookingClasses.ForEach(x => stringBuilder.Append(x));
            }
        }

        private static void SerializeAirlinePreference(StringBuilder stringBuilder, AirlinesPreference airlinesPreference)
        {
            if (airlinesPreference == null)
                return;

            if (airlinesPreference.PermittedAirlines != null)
            {
                stringBuilder.Append("Perm");
                airlinesPreference.PermittedAirlines.ForEach(x => stringBuilder.Append(x));
            }
            if (airlinesPreference.ProhibitedAirlines != null)
            {
                stringBuilder.Append("Proh");
                airlinesPreference.ProhibitedAirlines.ForEach(x => stringBuilder.Append(x));
            }
            if (airlinesPreference.PreferredAirlines != null)
            {
                stringBuilder.Append("Pref");
                airlinesPreference.PreferredAirlines.ForEach(x => stringBuilder.Append(x));
            }
        }

        private static void SerializeFlightTypeOption(StringBuilder stringBuilder, FlightTypeOption flightTypeOption)
        {
            if (flightTypeOption == null)
                return;

            stringBuilder.Append(flightTypeOption.DirectFlightOnly);
            stringBuilder.Append(flightTypeOption.ETicketOnly);
            stringBuilder.Append(flightTypeOption.JetPlanesOnly);
            stringBuilder.Append(flightTypeOption.NonStopFlightOnly);
            stringBuilder.Append(flightTypeOption.OnlineConnectionsOnly);
            stringBuilder.Append(flightTypeOption.MaxConnectionTimeMinutes);
            stringBuilder.Append(flightTypeOption.MaxConnections);
            stringBuilder.Append(flightTypeOption.MinConnectionTimeMinutes);
        }

        private static void SerializeFarePreference(StringBuilder stringBuilder, FarePreference preference)
        {
            if (preference == null)
                return;

            stringBuilder.Append(preference.NoPenalty);
            stringBuilder.Append(preference.NoRestriction);
            stringBuilder.Append(preference.RefundableFares);
        }

        #endregion

        #endregion

        #region Private Members

        #endregion
    }
}
