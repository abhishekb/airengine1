﻿using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;

namespace Tavisca.TravelNxt.Flight.Avail.Core.CacheDataTransforms
{
    internal static class FareTransforms
    {
        #region public methods

        public static List<PassengerFareBreakup> GetTransformedFareBreakups(this List<PassengerFareBreakup> passengerFareBreakups, List<PassengerTypeQuantity> requiredPassengerTypeQuantities)
        {
            return (from requiredPassengerTypeQuantity in requiredPassengerTypeQuantities
                    let matchedPassengerFareBreakup =
                        passengerFareBreakups.First(
                            passengerFareBreakup =>
                            passengerFareBreakup.PassengerTypeQuantity.PassengerType ==
                            requiredPassengerTypeQuantity.PassengerType)
                    select GetTransformedFareBreakup(matchedPassengerFareBreakup, requiredPassengerTypeQuantity)).ToListBuffered();
        }

        public static void TransformFareLevelComponents(Fare source, Fare target, int sourcePaxCount, int targetPaxCount)
        {
            target.Discounts = source.Discounts.GetPaxCountAdjustedFareComponent(sourcePaxCount,
                                                                                            targetPaxCount);

            target.Fees = source.Fees.GetPaxCountAdjustedFareComponent(sourcePaxCount, targetPaxCount);

            target.Taxes = source.Taxes.GetPaxCountAdjustedFareComponent(sourcePaxCount, targetPaxCount);

            target.Penalties = source.Penalties.GetPaxCountAdjustedPenalties(sourcePaxCount, targetPaxCount);
            
            target.BaseAmount = new Money()
                                    {
                                        Amount = target.PassengerFareBreakups.Sum(
                                            passengerFareBreakup =>
                                            passengerFareBreakup.BaseAmount.Amount*
                                            passengerFareBreakup.PassengerTypeQuantity.Quantity),
                                        CurrencyCode = source.BaseAmount.CurrencyCode
                                    };
        }

        #endregion

        #region private methods

        private static List<Penalty> GetPaxCountAdjustedPenalties(this List<Penalty> penalties, int sourceCount, int targetCount)
        {
            if(penalties == null)
                return new List<Penalty>();

            var targetPenalties = penalties.GetPaxCountAdjustedFareComponent(sourceCount, targetCount);

            for(var i=0; i<penalties.Count(); i++)
            {
                targetPenalties[i].PenaltyType = penalties[i].PenaltyType;
                targetPenalties[i].AmountType = penalties[i].AmountType;
            }

            return targetPenalties;
        }

        private static List<T> GetPaxCountAdjustedFareComponent<T>(this IEnumerable<T> sourceComponents, int sourceCount, int requiredCount) where T : Data.Services.AirFareSearchV1.FareComponent, new()
        {
            if(sourceComponents == null)
                return new List<T>();

            return sourceComponents.Select(component => new T()
                                                            {
                                                                Amount = component.Amount / sourceCount * requiredCount,
                                                                Code = component.Code,
                                                                CurrencyCode = component.CurrencyCode,
                                                                Description = component.Description,
                                                                ExtensionData = component.ExtensionData,
                                                                Type = component.Type
                                                            }).ToListBuffered();
        }

        private static PassengerFareBreakup GetTransformedFareBreakup(PassengerFareBreakup sourceFareBreakup, PassengerTypeQuantity requestedPassengerTypeQuantity)
        {
            var targetFareBreakup = new PassengerFareBreakup()
                                  {
                                      FareDetails = sourceFareBreakup.FareDetails,
                                      FiledPassengerTypeCode = sourceFareBreakup.FiledPassengerTypeCode,
                                      FareRules = sourceFareBreakup.FareRules,
                                      PassengerTypeQuantity = requestedPassengerTypeQuantity,
                                      TotalAmount = sourceFareBreakup.TotalAmount,
                                      TotalDiscounts = sourceFareBreakup.TotalDiscounts,
                                      TotalEquivalentAmount = sourceFareBreakup.TotalEquivalentAmount,
                                      BaseAmount = sourceFareBreakup.BaseAmount,
                                      TotalFees = sourceFareBreakup.TotalFees,
                                      TotalTaxes = sourceFareBreakup.TotalTaxes,
                                      Discounts = sourceFareBreakup.Discounts,
                                      ExtensionData = sourceFareBreakup.ExtensionData,
                                      Fees = sourceFareBreakup.Fees,
                                      Taxes = sourceFareBreakup.Taxes,
                                      Penalties = sourceFareBreakup.Penalties
                                  };
            
            return targetFareBreakup;
        }

        #endregion
    }
}
