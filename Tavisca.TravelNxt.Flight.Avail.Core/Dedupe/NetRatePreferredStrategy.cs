﻿using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Dedupe
{
    public class NetRatePreferredStrategy : DefaultDedupeStrategy
    {
        protected override RecommendationToKeep GetComparisonResult(FlightRecommendation newRecommendation, FlightRecommendation recommendation)
        {
            if (newRecommendation.Fare.FareType == FareType.Negotiated && recommendation.Fare.FareType != FareType.Negotiated)
                return RecommendationToKeep.New;

            if (newRecommendation.Fare.FareType != FareType.Negotiated && recommendation.Fare.FareType == FareType.Negotiated)
                return RecommendationToKeep.Old;

            return base.GetComparisonResult(newRecommendation, recommendation);
        }
    }
}
