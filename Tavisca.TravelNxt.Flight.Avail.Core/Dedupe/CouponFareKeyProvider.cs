﻿using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Dedupe
{
    public class CouponFareKeyProvider : IFlightKeyProvider
    {
        public string GetKey(FlightRecommendation flightRecommendation)
        {
            if (flightRecommendation.Fare.FareAttributes != null && flightRecommendation.Fare.FareAttributes.Contains(FareAttribute.CouponFare))
                return flightRecommendation.Key + FareAttribute.CouponFare;

            return flightRecommendation.Key;
        }
    }
}
