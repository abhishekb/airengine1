﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Dedupe
{
    public class DefaultDedupeStrategy : IFlightDedupeStrategy
    {
        public IFlightKeyProvider FLightKeyProvider { get; set; }

        public IDictionary<string, int> ProviderFamilyPreferenceMapping { get; set; }

        public virtual List<FlightRecommendation> Dedupe(
            List<FlightRecommendation> uniqueRecommendations, FlightRecommendation newRecommendation)
        {
            if (uniqueRecommendations == null)
                uniqueRecommendations = new List<FlightRecommendation>();

            var newRecommendationKey = FLightKeyProvider.GetKey(newRecommendation);

            var sameKeyRecommendations =
                uniqueRecommendations.Where(x => FLightKeyProvider.GetKey(x).Equals(newRecommendationKey)).ToListBuffered();
            if (sameKeyRecommendations.Count > 0)
                sameKeyRecommendations.ForEach(
                    recommendation =>
                    {
                        var result = GetComparisonResult(newRecommendation, recommendation);
                        switch (result)
                        {
                            case RecommendationToKeep.Both:
                                uniqueRecommendations.Add(newRecommendation);
                                break;
                            case RecommendationToKeep.New:
                                newRecommendation.RefId = recommendation.RefId;
                                uniqueRecommendations.Remove(recommendation);
                                uniqueRecommendations.Add(newRecommendation);
                                break;
                            case RecommendationToKeep.Old:
                                break;
                        }
                        AddSavings(newRecommendation.Fare, recommendation.Fare);
                    });
            else
                uniqueRecommendations.Add(newRecommendation);

            return uniqueRecommendations;
        }

        protected virtual RecommendationToKeep GetComparisonResult(FlightRecommendation newRecommendation,
            FlightRecommendation recommendation)
        {
            if (!IsRecommendationValid(newRecommendation))
                return RecommendationToKeep.Old;

            var contractBasedSelection = GetPreferredContractBasedRecommendation(newRecommendation, recommendation);
            if (contractBasedSelection.HasValue)
                return contractBasedSelection.Value;

            return GetBestFarePreferredRecommendation(newRecommendation, recommendation);
        }

        protected RecommendationToKeep? GetPreferredContractBasedRecommendation(FlightRecommendation newRecommendation, FlightRecommendation oldRecommendation)
        {
            if (ContainsContracts(oldRecommendation) && !ContainsContracts(newRecommendation))
                return RecommendationToKeep.Old;
            if (ContainsContracts(newRecommendation) && !ContainsContracts(oldRecommendation))
                return RecommendationToKeep.New;

            return null;
        }

        protected bool IsRecommendationValid(FlightRecommendation flightRecommendation)
        {
            var firstSegment = flightRecommendation.Legs.First().Segments.First();
            var timeToCheck = firstSegment.DepartureDateTime;
            var selectedTime =
                RuntimeContext.Resolver.Resolve<ITimeConverter>().GetLocalTimeAtLocation(
                    firstSegment.DepartureAirport.ToTimeZoneInfo(),
                    DateTime.UtcNow.AddHours(SettingManager.AirTimeAheadInHours));
            return timeToCheck >= selectedTime;
        }

        protected void AddSavings(FlightFare first, FlightFare second)
        {
            var savings = Money.Zero;

            var fareComparisonResult = GetFareComparison(first, second);
            if (fareComparisonResult == ComparisonResult.Equal)
                return;

            FlightFare cheaperFare;
            FlightFare costlierFare;

            switch (fareComparisonResult)
            {
                case ComparisonResult.FirstCostly:
                    costlierFare = first;
                    cheaperFare = second;
                    break;
                case ComparisonResult.SecondCostly:
                    costlierFare = second;
                    cheaperFare = first;
                    break;
                default:
                    return;
            }

            if (costlierFare.Savings != null)
                savings += costlierFare.Savings;
            
            savings += ((costlierFare.BaseAmount+costlierFare.TotalTaxes) - (cheaperFare.BaseAmount + cheaperFare.TotalTaxes));
            if (cheaperFare.Savings == null || savings.BaseAmount > cheaperFare.Savings.BaseAmount)
                cheaperFare.Savings = savings;
        }

        private RecommendationToKeep GetBestFarePreferredRecommendation(FlightRecommendation newRecommendation, FlightRecommendation existingRecommendation)
        {
            switch (GetFareComparison(newRecommendation.Fare, existingRecommendation.Fare))
            {
                case ComparisonResult.FirstCostly:
                    return IsCostlyOptionValid(existingRecommendation, newRecommendation)
                               ? RecommendationToKeep.Both
                               : RecommendationToKeep.Old;
                case ComparisonResult.SecondCostly:
                    return IsCostlyOptionValid(newRecommendation, existingRecommendation)
                               ? RecommendationToKeep.Both
                               : RecommendationToKeep.New;
            }

            return GetOptionForEqualFare(newRecommendation, existingRecommendation);
        }

        private RecommendationToKeep GetOptionForEqualFare(FlightRecommendation newRecommendation, FlightRecommendation existingRecommendation)
        {
            if (newRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares) && !existingRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares))
                return RecommendationToKeep.New;
            if (!newRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares) && existingRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares))
                return RecommendationToKeep.Old;

            return GetFareSourcePreferenceBasedSelection(newRecommendation, existingRecommendation);
        }

        private RecommendationToKeep GetFareSourcePreferenceBasedSelection(FlightRecommendation newRecommendation, FlightRecommendation existingRecommendation)
        {
            var newRecommendationProviderFamily = newRecommendation.ProviderSpace.Family;
            var existingRecommendationProviderFamily = existingRecommendation.ProviderSpace.Family;

            if (!newRecommendationProviderFamily.Equals(existingRecommendationProviderFamily) && IsFamilyPreferenceMore(newRecommendationProviderFamily, existingRecommendationProviderFamily))
                return RecommendationToKeep.New;
            if (existingRecommendation.Fare.FareType == FareType.Published && newRecommendation.Fare.FareType != FareType.Published)
                return RecommendationToKeep.Old;
            return RecommendationToKeep.New;
        }

        private bool IsFamilyPreferenceMore(string familyToCompare, string familyToCompareWith)
        {
            int providerToComparePreference;
            int providerToCompareWithPreference;

            if (!ProviderFamilyPreferenceMapping.TryGetValue(familyToCompare, out providerToComparePreference))
                providerToComparePreference = -1;

            if (!ProviderFamilyPreferenceMapping.TryGetValue(familyToCompareWith, out providerToCompareWithPreference))
                providerToCompareWithPreference = -1;

            if (providerToComparePreference == -1 && providerToCompareWithPreference == -1)
                return false;

            if (providerToComparePreference != -1 && providerToCompareWithPreference == -1)
                return true;

            if (providerToComparePreference == -1 && providerToCompareWithPreference != -1)
                return false;

            return providerToComparePreference > providerToCompareWithPreference;
        }

        private static bool IsCostlyOptionValid(FlightRecommendation cheaperRecommendation, FlightRecommendation costlierRecommendation)
        {
            if (cheaperRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares))
                return false;
            if (costlierRecommendation.Fare.FareAttributes.ContainsStructValue(FareAttribute.RefundableFares))
                return true;
            return false;
        }

        private static ComparisonResult GetFareComparison(FlightFare first, FlightFare second)
        {
            if (first.TotalFare.BaseAmount > second.TotalFare.BaseAmount)
                return ComparisonResult.FirstCostly;
            if (first.TotalFare.BaseAmount < second.TotalFare.BaseAmount)
                return ComparisonResult.SecondCostly;
            return ComparisonResult.Equal;
        }

        private static bool ContainsContracts(FlightRecommendation flightRecommendation)
        {
            return (flightRecommendation.ProviderSpace.Contracts != null &&
                    flightRecommendation.ProviderSpace.Contracts.Count > 0);
        }
    }

    #region Helper enums
    public enum RecommendationToKeep
    {
        New,
        Old,
        Both
    }

    public enum ComparisonResult
    {
        FirstCostly,
        SecondCostly,
        Equal
    }
    #endregion
}
