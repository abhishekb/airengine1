﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Contracts;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Dedupe
{
    public class DefaultKeyProvider : IFlightKeyProvider
    {
        public string GetKey(Entities.FlightRecommendation flightRecommendation)
        {
            return flightRecommendation.Key;
        }
    }
}
