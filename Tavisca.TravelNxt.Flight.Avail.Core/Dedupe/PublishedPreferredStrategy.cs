﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Entities;

namespace Tavisca.TravelNxt.Flight.Avail.Core.Dedupe
{
    public class PublishedPreferredStrategy : DefaultDedupeStrategy
    {

        protected override RecommendationToKeep GetComparisonResult(FlightRecommendation newRecommendation, FlightRecommendation recommendation)
        {
            if (newRecommendation.Fare.FareType == FareType.Published && recommendation.Fare.FareType != FareType.Published)
                return RecommendationToKeep.New;

            if (newRecommendation.Fare.FareType != FareType.Published && recommendation.Fare.FareType == FareType.Published)
                return RecommendationToKeep.Old;

            return base.GetComparisonResult(newRecommendation, recommendation);
        }

    }
}
