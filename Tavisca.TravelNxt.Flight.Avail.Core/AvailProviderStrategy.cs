﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Avail.Core.Exceptions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class AvailProviderStrategy : IAvailProviderStrategy
    {
        public void Initialize(FlightAvailRq request)
        { }

        public virtual FlightAvailResult Avail(FlightAvailRq request)
        {
            Utility.LogDiagnosticEntry("avail thread started for supplier: " + request.ProviderSpace.ID);

            try
            {
                IFlightProvider provider;

                var context = Utility.GetCurrentContext();
                if (context.IsMock)
                {
                    provider = RuntimeContext.Resolver.Resolve<IFlightProvider>(
                        KeyStore.SingularityNameKeys.SearchMockDefaultProvider);
                }
                else
                {
                    var searchConfiguration =
                        request.ProviderSpace.Configurations.FirstOrDefault(
                            x => x.ConfigType == ConfigurationType.Search);

                    if (searchConfiguration == null)
                        throw new InvalidDataException("Fare source configuration not found for search, fare source: " +
                            request.ProviderSpace.ID.ToString(CultureInfo.InvariantCulture));

                    var providerLocator = string.IsNullOrWhiteSpace(searchConfiguration.ContractVersion)
                                              ? KeyStore.SingularityNameKeys.SearchDefaultProvider
                                              : searchConfiguration.ContractVersion;

                    provider = RuntimeContext.Resolver.Resolve<IFlightProvider>(providerLocator);
                }

                var result = provider.Avail(request);

                return result;
            }
            catch (FlightSupplierException)
            {
                throw;
            }
            catch (HandledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(),
                                                   KeyStore.LogCategories.ExceptionCategories.Regular);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
            finally
            {
                Utility.LogDiagnosticEntry("avail thread is about to exit for supplier: " + request.ProviderSpace.ID);
            }
        }
    }
}
