﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    internal class InternationalSearchCheckProvider
    {
        public static bool IsSearchInternational(ICollection<SearchSegment> searchSegments, IContentManager contentManager)
        {
            var airportCodes = new HashSet<string>();
            var cityCodes = new HashSet<string>();

            foreach (var searchSegment in searchSegments)
            {
                ExtractSegmentLocationCodes(searchSegment, airportCodes, cityCodes);

                if (searchSegment.ArrivalAlternateAirportInformation != null && searchSegment.ArrivalAlternateAirportInformation.AirportCodes != null)
                {
                    foreach (var airportCode in searchSegment.ArrivalAlternateAirportInformation.AirportCodes)
                        airportCodes.Add(airportCode);
                }

                if (searchSegment.DepartureAlternateAirportInformation != null && searchSegment.DepartureAlternateAirportInformation.AirportCodes != null)
                {
                    foreach (var airportCode in searchSegment.DepartureAlternateAirportInformation.AirportCodes)
                        airportCodes.Add(airportCode);
                }
            }

            return GetInternationalSearchCheckResult(airportCodes, cityCodes, contentManager);
        }

        #region private methods

        private static void ExtractSegmentLocationCodes(SearchSegment searchSegment, ISet<string> airportCodes, ISet<string> cityCodes)
        {
            if (searchSegment.IsArrivalSpecCity())
                cityCodes.Add(searchSegment.GetArrivalLocation());
            else
                airportCodes.Add(searchSegment.GetArrivalLocation());

            if (searchSegment.IsDepartureSpecCity())
                cityCodes.Add(searchSegment.GetDepartureLocation());
            else
                airportCodes.Add(searchSegment.GetDepartureLocation());
        }

        private static bool GetInternationalSearchCheckResult(IEnumerable<string> airportCodes, IEnumerable<string> cityCodes, IContentManager contentManager)
        {
            var homeCountryCode = SettingManager.AgencyHomeCountryCode;

            foreach (var cityCode in cityCodes)
            {
                var countryCode = contentManager.GetCitiesByCode(cityCode).First().CountryCode;

                if (!countryCode.Equals(homeCountryCode, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            foreach (var airportCode in airportCodes)
            {
                var airport = contentManager.GetAirportByCode(airportCode);

                if (!airport.City.CountryCode.Equals(homeCountryCode, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            return false;
        }

        #endregion
    }
}
