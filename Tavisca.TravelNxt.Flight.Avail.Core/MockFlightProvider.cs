﻿using System;
using System.Configuration;
using Tavisca.TravelNxt.Common.Exceptions;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class MockFlightProvider : FlightProvider
    {
        protected override string LogEntryTitle
        {
            get
            {
                return "Search Call to mock supplier: {0}";
            }
        }

        protected override FareSearchRS GetProviderResponse(AirFareSearchRQ airFareSearchRequest, ProviderSpace providerSpace)
        {
            try
            {
                FareSearchRS response;
                using (var client = new AirFareSearchV1Client(KeyStore.ClientEndpoints.AirFareSearchServiceBasicHttp,
                                                              GetMockUrl(airFareSearchRequest.JourneyLegs.Count)))
                {
                    Utility.LogDiagnosticEntry("sending request to supplier, provider id: " + providerSpace.ID);
                    response = client.GetFares(airFareSearchRequest);
                    Utility.LogDiagnosticEntry("recieved response from supplier, provider id: " + providerSpace.ID);
                }

                return response;
            }
            catch (Exception ex)
            {
                Utility.GetLogFactory().WriteAsync(ex.ToContextualEntry(), KeyStore.LogCategories.ExceptionCategories.Critical);
                throw new HandledException(KeyStore.ErrorMessages.ExceptionHasBeenLogged, ex);
            }
        }

        private static string GetMockUrl(int journeyLegCount)
        {
            string scenario;
            switch (journeyLegCount)
            {
                case 1 :
                    scenario = KeyStore.MockKeys.Scenario.OneWay;
                    break;

                case 2 :
                    scenario = KeyStore.MockKeys.Scenario.Roundtrip;
                    break;

                default:
                    scenario = KeyStore.MockKeys.Scenario.MultiCity;
                    break;
            }

            return string.Format("{0}/?category={1}",
                                 ConfigurationManager.AppSettings[KeyStore.MockKeys.MockUrlBase].TrimEnd('/'),
                                 KeyStore.MockKeys.MockCategory + scenario);
        }
    }
}
