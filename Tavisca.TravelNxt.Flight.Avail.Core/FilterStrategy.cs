﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class FilterStrategy : IFilterStrategy
    {
        #region IFilterStrategy Members

        public virtual bool ShouldFilter(FlightRecommendation flightRecommendation, FlightSearchCriteria flightSearchCriteria)
        {
            if (flightRecommendation == null || flightSearchCriteria == null)
                return true;


            if (flightSearchCriteria.FlightTravelPreference != null)
            {
                var airlines = new HashSet<string>(
                    flightRecommendation.Legs.SelectMany(x => x.Segments.Select(y => y.MarketingAirline.Code))
                    .ToListBuffered());

                if (!flightSearchCriteria.FlightTravelPreference.AllowMixedAirlines && IsMixed(airlines))
                    return true;

                //if (!IsEmpty(flightSearchCriteria.FlightTravelPreference.ExcludeAirlines) &&
                //    HasIntersect(airlines, flightSearchCriteria.FlightTravelPreference.ExcludeAirlines
                //    .Where(x => x.PreferenceType == PreferenceTypeOptions.Unacceptable || x.PreferenceType == PreferenceTypeOptions.Default)
                //    .Select(x => x.AirlineCode).ToListBuffered()))
                //    return true;

                //var ruledIncludeList =
                //    flightSearchCriteria.FlightTravelPreference.IncludeAirlines.Where(
                //        x => x.PreferenceType == PreferenceTypeOptions.Unacceptable).Select(x => x.AirlineCode).ToListBuffered();

                //if (!IsEmpty(ruledIncludeList) && 
                //    airlines.Any(x => ruledIncludeList.All(y => !y.Equals(x, StringComparison.OrdinalIgnoreCase))))
                //    return true;

                if (flightSearchCriteria.FlightTravelPreference.JetFlightsOnly &&
                    !IsJetRecommendation(flightRecommendation))
                    return true;

                if (flightSearchCriteria.FlightTravelPreference.Refundable.HasValue &&
                    flightSearchCriteria.FlightTravelPreference.Refundable == true && !IsFareAttributeApplicable(flightRecommendation, FareAttribute.RefundableFares))
                    return true;

                if (flightSearchCriteria.FlightTravelPreference.UnRestrictedFare &&
                    !IsFareAttributeApplicable(flightRecommendation, FareAttribute.EconomyUnrestricted))
                    return true;
            }

            return false;
        }

        public IList<FlightRecommendation> Filter(IList<FlightRecommendation> flightRecommendations, FlightSearchCriteria searchCriteria)
        {
            if (!searchCriteria.MaxPreferredResults.HasValue)
                return flightRecommendations;

            var maxResults = searchCriteria.MaxPreferredResults.Value;

            return flightRecommendations.Take(maxResults).ToListBuffered(maxResults);
        }

        #endregion

        #region Protected Members

        protected virtual bool IsMixed(IEnumerable<string> airlines)
        {
            return airlines.Count() != 1;
        }

        protected virtual bool HasIntersect(IEnumerable<string> airlines, IEnumerable<string> compareTo)
        {
            return airlines.Intersect(compareTo).Any();
        }

        protected virtual bool IsFareAttributeApplicable(FlightRecommendation flightRecommendation, FareAttribute fareAttribute)
        {
            return flightRecommendation.Fare.FareAttributes != null &&
                   flightRecommendation.Fare.FareAttributes.Contains(fareAttribute);
        }

        protected virtual bool IsJetRecommendation(FlightRecommendation recommendation)
        {
            return
                recommendation.Legs.SelectMany(x => x.Segments.Select(y => y.AircraftType))
                              .All(z => !string.IsNullOrWhiteSpace(z) && z.ToUpper().Contains("JET"));
        }

        protected static bool IsEmpty<T>(ICollection<T> list)
        {
            return list == null || list.Count == 0;
        }

        //protected bool IsNonStop(FlightRecommendation recommendation)
        //{
        //    return recommendation.Legs.Count(x => x.Segments.Count > 1) > 0 ||
        //           recommendation.Legs.SelectMany(x => x.Segments)
        //           .Count(x => x.InSegmentStops != null && x.InSegmentStops.Count > 0) > 0;
        //}

        #endregion
    }
}
