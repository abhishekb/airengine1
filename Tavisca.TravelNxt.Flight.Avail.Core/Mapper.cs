﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.Services.AirFareSearchV1;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public static class Mapper
    {
        private static readonly Dictionary<PassengerType, Entities.PassengerType>
            OPassengerTypeMapping = new Dictionary
                <PassengerType, Entities.PassengerType>
                                        {
                                            {PassengerType.Adult,Entities.PassengerType.Adult},
                                            {PassengerType.Child,Entities.PassengerType.Child},
                                            {PassengerType.Senior,Entities.PassengerType.Senior},
                                            {PassengerType.InfantInLap,Entities.PassengerType.Infant},
                                            {PassengerType.InfantWithSeat,Entities.PassengerType.Child},
                                            {PassengerType.Government,Entities.PassengerType.Adult},
                                            {PassengerType.Youth,Entities.PassengerType.Adult},
                                            {PassengerType.Mapped,Entities.PassengerType.Mapped}
                                        };

        private static readonly Dictionary<FareType, Entities.FareType>
            OFareTypeMapping = new Dictionary<FareType, Entities.FareType>
                                   {
                                       {FareType.Published,Entities.FareType.Published},
                                       {FareType.Corporate,Entities.FareType.Corporate},
                                       {FareType.Private,Entities.FareType.Negotiated},
                                       {FareType.NotSpecified,Entities.FareType.Published},
                                       {FareType.Agency,Entities.FareType.Negotiated},
                                   };

        private static readonly Dictionary<FareAttribute, Entities.FareAttribute> OFareRestrictionTypeMapping = new Dictionary<FareAttribute, Entities.FareAttribute>
                                   {
                                       {FareAttribute.NoPenalty,Entities.FareAttribute.NoPenalty},
                                       {FareAttribute.NoRestriction,Entities.FareAttribute.NoRestriction},
                                       {FareAttribute.NonRefundableFares,Entities.FareAttribute.NonRefundableFares},
                                       {FareAttribute.RefundableFares,Entities.FareAttribute.RefundableFares},
                                       {FareAttribute.CouponFares,Entities.FareAttribute.CouponFare},
                                       {FareAttribute.FrozenFares,Entities.FareAttribute.FrozenFares}
                                   };

        private static readonly Dictionary<CabinType, Entities.CabinType> CabinMapping = new Dictionary<CabinType, Entities.CabinType>
                                                                                         {
                                                                                             {CabinType.Unknown        , Entities.CabinType.Unknown  },
                                                                                             {CabinType.First          , Entities.CabinType.First          },
                                                                                             {CabinType.Business       , Entities.CabinType.Business       },
                                                                                             {CabinType.PremiumEconomy , Entities.CabinType.PremiumEconomy },
                                                                                             {CabinType.Economy        , Entities.CabinType.Economy        },
                                                                                         };
        
        public static Entities.PassengerType Cast(this PassengerType passengerType)
        {
            return OPassengerTypeMapping.ContainsKey(passengerType) ? OPassengerTypeMapping[passengerType] : Entities.PassengerType.Adult;
        }

        public static Entities.FareType Cast(this FareType fareType)
        {
            return OFareTypeMapping.ContainsKey(fareType) ? OFareTypeMapping[fareType] : Entities.FareType.Published;
        }

        public static Entities.FareAttribute Cast(this FareAttribute fareRestrictionType)
        {
            return OFareRestrictionTypeMapping.ContainsKey(fareRestrictionType) ? OFareRestrictionTypeMapping[fareRestrictionType] : Entities.FareAttribute.Unknown;
        }

        public static Entities.CabinType Cast(this CabinType cabinType)
        {
            return CabinMapping.ContainsKey(cabinType) ? CabinMapping[cabinType] : Entities.CabinType.Unknown;
        }
    }
}
