﻿using System.Collections.Generic;
using System.Linq;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Data.Services.AirConfigurationService;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;
using AirContract = Tavisca.TravelNxt.Flight.Entities.AirContract;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class ContractManager : IContractManager
    {
        #region private fields

        private ICollection<AirContract> _allContracts;

        #endregion

        public ICollection<AirContract> 
            LoadContractsForProvider(long providerSpaceId, FlightSearchCriteria searchCriteria)
        {
            var allContracts = GetAllContracts();

            return GetApplicableContracts(allContracts, providerSpaceId, searchCriteria);
        }

        protected virtual ICollection<AirContract> GetAllContracts()
        {
            if (_allContracts != null)
                return _allContracts;

            var dbContracts = GetDbContracts();

            var paperContracts = GetPaperContracts();

            _allContracts = GetMergedContracts(dbContracts, paperContracts);
            return _allContracts;
        }

        private static ICollection<AirContract> GetMergedContracts(IList<AirContract> dbContracts, IList<IPaperContract> paperContracts)
        {
            dbContracts = dbContracts ?? new List<AirContract>();
            paperContracts = paperContracts ?? new List<IPaperContract>();

            var finalSetOfContracts = new List<AirContract>(dbContracts.Count + paperContracts.Count);

            foreach (var paperContract in paperContracts)
            {
                var enclosedContract = paperContract.GetContract();
                var matchingDbContract = dbContracts.FirstOrDefault(x => x.ContractId == enclosedContract.ContractId);

                if (matchingDbContract != null)
                {
                    enclosedContract = paperContract.GetOverriddenContract(matchingDbContract);
                    dbContracts.Remove(matchingDbContract);
                }

                finalSetOfContracts.Add(enclosedContract);
            }

            finalSetOfContracts.AddRange(dbContracts);

            return finalSetOfContracts;
        }

        protected virtual IList<AirContract> GetDbContracts()
        {
            IEnumerable<Data.Services.AirConfigurationService.AirContract> response = null;

            Utility.LogDiagnosticEntry("calling AirConfigurationService.GetAirContracts");
            using (var service = new AirConfigurationServiceClient(KeyStore.ClientEndpoints.DataServicesIAirConfigurationService,
                SettingManager.GetClientEndpointAddress(KeyStore.ClientEndpoints.AddressKeys.DataServicesIAirConfigurationServiceAddr)))
            {
                response = service.GetAirContracts() ??
                           new Data.Services.AirConfigurationService.AirContract[0];
            }
            Utility.LogDiagnosticEntry("call finished for AirConfigurationService.GetAirContracts");

            return response.Select(ToEntity).ToListBuffered();
        }

        protected virtual IList<IPaperContract> GetPaperContracts()
        {
            return RuntimeContext.Resolver.ResolveAll<IPaperContract>().ToListBuffered();
        }

        private static IList<AirContract> GetApplicableContracts(IEnumerable<AirContract> allContracts, long providerSpaceId, FlightSearchCriteria searchCriteria)
        {
            return
                allContracts.Where(
                    x => x.SearchAirFareSourceId == providerSpaceId && x.IsContractApplicable(searchCriteria)).
                    ToListBuffered();
        }

        private static AirContract ToEntity(Data.Services.AirConfigurationService.AirContract airContract)
        {
            return new DbAirContract
            {
                AgencyId = airContract.AgencyId,
                Airlines =
                    airContract.Airlines == null
                        ? new List<string>()
                        : airContract.Airlines.ToListBuffered(),
                AirportPairs =
                    airContract.AirportPairs == null
                        ? new List<string>()
                        : airContract.AirportPairs.ToListBuffered(),
                ApplicableCabinTypes =
                    airContract.ApplicableCabinTypes == null
                        ? new List<string>()
                        : airContract.ApplicableCabinTypes.ToListBuffered(),
                ApplicableClassOfService =
                    airContract.ApplicableClassOfService == null
                        ? new List<string>()
                        : airContract.ApplicableClassOfService.ToListBuffered(),
                BookAirFareSourceId = airContract.BookAirFareSourceId,
                ContractId = airContract.ContractId,
                ContractName = airContract.ContractName,
                ContractRateCode = airContract.ContractRateCode,
                ContractRateCodeType = airContract.ContractRateCodeType,
                ContractType = airContract.ContractType,
                FareBasisCodes =
                    airContract.FareBasisCodes == null
                        ? new List<string>()
                        : airContract.FareBasisCodes.ToListBuffered(),
                FareType = airContract.FareType,
                Preference = airContract.Preference,
                TravelPeriod =
                    airContract.TravelPeriod == null
                        ? null
                        : new Entities.DateTimeSpan()
                        {
                            End = airContract.TravelPeriod.End,
                            Start = airContract.TravelPeriod.Start
                        },

                ValidPeriod =
                    airContract.ValidPeriod == null
                        ? null
                        : new Entities.DateTimeSpan()
                        {
                            End = airContract.ValidPeriod.End,
                            Start = airContract.ValidPeriod.Start
                        },

                TicketingPeriod =
                    airContract.TicketingPeriod == null
                        ? null
                        : new Entities.DateTimeSpan()
                        {
                            End = airContract.TicketingPeriod.End,
                            Start = airContract.TicketingPeriod.Start
                        },
                SearchAirFareSourceId = airContract.SearchAirFareSourceId,
                InterceptLevel = airContract.InterceptLevel,
                AirlinePreferenceLevel = airContract.AirlinePreferenceLevel
            };
        }
    }
}
