﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class DbAirContract : AirContract
    {
        public override bool CanSellRecommendation(FlightRecommendation flightRecommendation)
        {
            if (flightRecommendation == null || flightRecommendation.Legs == null || flightRecommendation.Legs.Count == 0)
                return false;

            var travelStart = flightRecommendation.Legs.First().Segments.First().DepartureDateTime;
            var travelEnd = flightRecommendation.Legs.Last().Segments.Last().ArrivalDateTime;

            if ((InterceptLevel.Equals(KeyStore.InterceptLevel.Post, StringComparison.OrdinalIgnoreCase) || InterceptLevel.Equals(KeyStore.InterceptLevel.Both, StringComparison.OrdinalIgnoreCase)) &&
                (IsValidPeriod() || IsTicketingPeriodValid()) && IsTravelPeriodValid(travelStart, travelEnd) && AreCabinTypesValid(flightRecommendation) && AreClassOfServiceValid(flightRecommendation) &&
                AreAirportsValid(flightRecommendation) && AreAirlinesValid(flightRecommendation) && AreFareBasisCodeValid(flightRecommendation) &&
                BookAirFareSourceId == flightRecommendation.ProviderSpace.ID)
                return true;

            return false;
        }

        public override bool IsContractApplicable(FlightSearchCriteria searchCriteria)
        {
            if (searchCriteria == null || searchCriteria.SearchSegments == null || searchCriteria.SearchSegments.Count == 0)
                return false;

            var travelStart = searchCriteria.SearchSegments.First().TravelDate.DateTime;
            var travelEnd = searchCriteria.SearchSegments.Last().TravelDate.DateTime;

            if ((InterceptLevel.Equals(KeyStore.InterceptLevel.Pre, StringComparison.OrdinalIgnoreCase) || InterceptLevel.Equals(KeyStore.InterceptLevel.Both, StringComparison.OrdinalIgnoreCase))
                && IsValidPeriod() && IsAgencyValid(AgencyId) && IsTravelPeriodValid(travelStart, travelEnd))
            {
                return true;
            }

            return false;
        }

        #region private methods

        private static bool IsAgencyValid(long agencyId)
        {
            return agencyId == 0 || agencyId == Utility.GetCurrentContext().AccountId;
        }

        private bool AreAirlinesValid(FlightRecommendation flightRecommendation)
        {
            if(Airlines == null || Airlines.Count == 0)
                return true;

            if (Airlines[0].Equals("*"))
            {
                if (AirlinePreferenceLevel.Equals(KeyStore.AirlinePreference.Exclude, StringComparison.OrdinalIgnoreCase))
                    return false;

                if (AirlinePreferenceLevel.Equals(KeyStore.AirlinePreference.Include, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            if (!Airlines[0].Equals("*") && flightRecommendation != null && flightRecommendation.Legs != null && flightRecommendation.Legs.Count > 0)
            {
                var allAirlines = GetAllAirlinesForRecommendation(flightRecommendation);

                if (AirlinePreferenceLevel.Equals(KeyStore.AirlinePreference.Include, StringComparison.OrdinalIgnoreCase) && Airlines.All(allAirlines.Contains))
                    return true;

                if (AirlinePreferenceLevel.Equals(KeyStore.AirlinePreference.Exclude, StringComparison.OrdinalIgnoreCase) && Airlines.All(x => !allAirlines.Contains(x)))
                    return false;

                if (AirlinePreferenceLevel.Equals(KeyStore.AirlinePreference.Only, StringComparison.OrdinalIgnoreCase))
                    if (allAirlines.Count == Airlines.Count && Airlines.All(allAirlines.Contains))
                        return false;
            }

            return false;
        }

        private static List<string> GetAllAirlinesForRecommendation(FlightRecommendation flightRecommendation)
        {
            return
                flightRecommendation.Legs.SelectMany(
                    leg => leg.Segments.Select(segment => segment.MarketingAirline.Code)).Distinct().ToListBuffered();

        }

        private bool IsTravelPeriodValid(DateTime start, DateTime end)
        {
            if (TravelPeriod == null ||
                ((TravelPeriod.Start == null || TravelPeriod.Start == DateTime.MinValue || start.ToUniversalTime() >= TravelPeriod.Start.Value.ToUniversalTime()) &&
                (TravelPeriod.End == null || TravelPeriod.End == DateTime.MinValue || end.ToUniversalTime() <= TravelPeriod.End.Value.ToUniversalTime())))
            {
                return true;
            }

            return false;
        }

        private bool IsTicketingPeriodValid()
        {
            var utcNow = DateTime.UtcNow;
            if (TicketingPeriod == null ||
                (
                (TicketingPeriod.Start == null || TicketingPeriod.Start == DateTime.MinValue || utcNow >= TicketingPeriod.Start.Value.AddDays(2).ToUniversalTime()) &&
                (TicketingPeriod.End == null || TicketingPeriod.End == DateTime.MinValue || utcNow <= TicketingPeriod.End.Value.AddDays(-2).ToUniversalTime())
                ))
                return true;

            return false;
        }

        private bool IsValidPeriod()
        {
            var utcNow = DateTime.UtcNow;
            if (ValidPeriod == null ||
                (ValidPeriod.Start == null || ValidPeriod.Start == DateTime.MinValue || utcNow >= ValidPeriod.Start.Value.ToUniversalTime()) &&
                (ValidPeriod.End == null || ValidPeriod.End == DateTime.MinValue || utcNow <= ValidPeriod.End.Value.ToUniversalTime()))
                return true;
            return false;
        }

        private bool AreAirportsValid(FlightRecommendation flightRecommendation)
        {
            if (AirportPairs == null || AirportPairs.Count == 0 || AirportPairs[0].Equals("*"))
                return true;

            if (flightRecommendation == null || flightRecommendation.Legs.Count == 0)
                return false;

            var allSegments = flightRecommendation.Legs.SelectMany(x => x.Segments);

            return (from segment in allSegments
                    from airportPair in AirportPairs
                    let airports = airportPair.Split('-').ToListBuffered()
                    where
                        (airports[0].Equals("*") ||
                         airports[0].Equals(segment.DepartureAirport.Code, StringComparison.OrdinalIgnoreCase)) &&
                        (airports[1].Equals("*") ||
                         airports[1].Equals(segment.ArrivalAirport.Code, StringComparison.OrdinalIgnoreCase))
                    select segment).Any();
        }

        private bool AreClassOfServiceValid(FlightRecommendation flightRecommendation)
        {
            if (ApplicableClassOfService != null && ApplicableClassOfService.Count > 0 && !ApplicableClassOfService[0].Equals("*") && flightRecommendation != null && flightRecommendation.Legs != null && flightRecommendation.Legs.Count > 0)
            {
                var allSegments = flightRecommendation.Legs.SelectMany(leg => leg.Segments);

                return

                    allSegments.SelectMany(
                        segment => segment.PaxTypeFareBasisCodes.Select(paxTypeFareBasis => paxTypeFareBasis.ClassOfService))
                        .Distinct().All(classOfService => ApplicableClassOfService.Any(applicableClassOfService => classOfService.Equals(applicableClassOfService, StringComparison.OrdinalIgnoreCase)));
            }

            return true;
        }

        private bool AreCabinTypesValid(FlightRecommendation flightRecommendation)
        {
            if (ApplicableCabinTypes != null && ApplicableCabinTypes.Count > 0 && !ApplicableCabinTypes[0].Equals("*") && flightRecommendation != null && flightRecommendation.Legs != null && flightRecommendation.Legs.Count > 0)
            {
                var allSegments = flightRecommendation.Legs.SelectMany(leg => leg.Segments);

                return
                    allSegments.SelectMany(
                        segment => segment.PaxTypeFareBasisCodes.Select(paxTypeFareBasis => paxTypeFareBasis.CabinType))
                        .Distinct().All(cabinType => ApplicableCabinTypes.Any(cabinClass => cabinType == ResolveCabinClass(cabinClass)));
            }
            return true;
        }

        private static CabinType ResolveCabinClass(string cabinClass)
        {
            switch (cabinClass.ToLower())
            {
                case "economy":
                    return CabinType.Economy;
                case "first":
                    return CabinType.First;
                case "business":
                    return CabinType.Business;
                case "premiereconomy":
                    return CabinType.PremiumEconomy;
                case "no_preference":
                    return CabinType.Unknown;
                default:
                    return CabinType.Unknown;
            }
        }

        private bool AreFareBasisCodeValid(FlightRecommendation flightRecommendation)
        {
            if (FareBasisCodes != null && FareBasisCodes.Count > 0 && !FareBasisCodes[0].Equals("*") && flightRecommendation != null && flightRecommendation.Legs != null && flightRecommendation.Legs.Count > 0)
            {
                var allSegments = flightRecommendation.Legs.SelectMany(leg => leg.Segments);

                foreach (var segment in allSegments)
                {
                    foreach (string fareBasisCode in FareBasisCodes)
                    {
                        foreach (var qualifier in segment.PaxTypeFareBasisCodes)
                        {
                            if (
                                !qualifier.FareBasisCode.Equals(fareBasisCode,
                                                                StringComparison.OrdinalIgnoreCase))
                                return false;
                        }
                    }
                }

            }
            return true;
        }

        #endregion

    }
}
