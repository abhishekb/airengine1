﻿using System.Linq;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.FareComponent;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class FlightFareDetailsExtracter
    {
        public static FlightFareDetails GetFlightFareDetails(FlightRecommendation flightRecommendation, bool jumpNegoRate)
        {
            var firstSegment = flightRecommendation.Legs.First().Segments.First();
            var lastSegment = flightRecommendation.Legs.Last().Segments.Last();
            var allSegments = flightRecommendation.Legs.SelectMany(x => x.Segments).ToListBuffered();
            return new FlightFareDetails()
            {
                DepartureTime = firstSegment.DepartureDateTime,
                ArrivalTime = lastSegment.ArrivalDateTime,
                OriginCountry = firstSegment.DepartureAirport.City.CountryCode,
                OriginAirportCode = flightRecommendation.Legs[0].Segments[0].DepartureAirport.Code,
                ItineraryType = flightRecommendation.ItineraryType,
                DestinationCountry = GetDestinationCountry(flightRecommendation),
                DestinationAirportCode = GetDestinationAirport(flightRecommendation),
                Provider = flightRecommendation.ProviderSpace,
                SegmentCount = flightRecommendation.Legs.Sum(leg => leg.Segments.Count),
                LegCount = flightRecommendation.Legs.Count,
                Fare = flightRecommendation.Fare,
                MarketingCarriers = allSegments
                    .Where(x => x.MarketingAirline != null && !string.IsNullOrEmpty(x.MarketingAirline.Code))
                    .Select(x => x.MarketingAirline.Code.ToLower()).Distinct().ToListBuffered(),
                ClassOfServices =
                    allSegments.SelectMany(
                        segment =>
                            segment.PaxTypeFareBasisCodes.Select(paxTypeFareBasis => paxTypeFareBasis.ClassOfService))
                        .Distinct()
                        .ToListBuffered(),
                FareBasisCodes =
                    allSegments.SelectMany(x => x.PaxTypeFareBasisCodes.Select(y => y.FareBasisCode))
                        .Distinct()
                        .ToListBuffered(),
                CabinTypes =
                    allSegments.SelectMany(
                        segment => segment.PaxTypeFareBasisCodes.Select(paxTypeFareBasis => paxTypeFareBasis.CabinType))
                        .Distinct()
                        .ToListBuffered(),
                JumpNegoRate = jumpNegoRate
            };
        }

        private static string GetDestinationCountry(FlightRecommendation flightRecommendation)
        {
            return flightRecommendation.ItineraryType == ItineraryTypeOptions.RoundTrip
                ? flightRecommendation.Legs.First().Segments.Last().ArrivalAirport.City.CountryCode
                : flightRecommendation.Legs.Last().Segments.Last().ArrivalAirport.City.CountryCode;
        }

        private static string GetDestinationAirport(FlightRecommendation flightRecommendation)
        {
            return flightRecommendation.ItineraryType == ItineraryTypeOptions.RoundTrip
                ? flightRecommendation.Legs.First().Segments.Last().ArrivalAirport.Code
                : flightRecommendation.Legs.Last().Segments.Last().ArrivalAirport.Code;
            ;
        }
    }
}