﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Tavisca.Frameworks.Parallel;
using Tavisca.Frameworks.Session;
using Tavisca.Singularity;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.FareComponent;
using Tavisca.TravelNxt.Flight.Avail.Core.Exceptions;
using Tavisca.TravelNxt.Flight.Contracts;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Flight.Entities.Avail;
using Tavisca.TravelNxt.Flight.ErrorSpace;
using Tavisca.TravelNxt.Flight.FareComponent;
using Tavisca.TravelNxt.Flight.Settings;
using CS = Tavisca.TravelNxt.Common.Settings;

namespace Tavisca.TravelNxt.Flight.Avail.Core
{
    public class AvailEngine : IAvailEngine
    {
        public AvailEngine()
        {
            FilterStrategy = RuntimeContext.Resolver.Resolve<IFilterStrategy>();
            RecommendationComparer = RuntimeContext.Resolver.Resolve<IFlightRecommendationComparer>();
            InterlineAgreementProvider = RuntimeContext.Resolver.Resolve<IAirlineInterlineAgreementProvider>();
            FareManager = RuntimeContext.Resolver.Resolve<IFareManager<FlightFareDetails, ICollection<ProviderSpace>>>(KeyStore.SingularityNameKeys.AirFareManager);
        }

        #region private fields

        private IFlightKeyProvider _flightKeyProvider;
        private IFlightKeyProvider GetFlightKeyProvider(Requester requester)
        {
            var nonCouponFareCustomerTypes = SettingManager.NonCouponFareCustomerTypes ?? string.Empty;
            var providerName = nonCouponFareCustomerTypes.Contains(requester.CustomerType.ToString())
                ? KeyStore.SingularityNameKeys.DefaultKeyProvider
                : KeyStore.SingularityNameKeys.CouponFareKeyProvider;
            return _flightKeyProvider ??
                   (_flightKeyProvider = RuntimeContext.Resolver.Resolve<IFlightKeyProvider>(providerName));
        }

        private IFilterStrategy FilterStrategy { get; set; }

        private IFlightRecommendationComparer RecommendationComparer { get; set; }

        private IAirlineInterlineAgreementProvider InterlineAgreementProvider { get; set; }

        private IFareManager<FlightFareDetails, ICollection<ProviderSpace>> FareManager { get; set; }

        private readonly Dictionary<string, FlightFare> _keyBasedPublishedFares = new Dictionary<string, FlightFare>();

        #endregion

        #region IAvailEngine Members

        public virtual FlightAvailResult Avail(Requester requester, FlightSearchCriteria searchCriteria, bool registerSupplierStreaming)
        {
            return AvailInternal(requester, searchCriteria, registerSupplierStreaming);
        }

        public virtual FlightAvailResult TryGetResult(DateTime? previousRequestTimeStamp)
        {
            Utility.LogDiagnosticEntry("fetching data from session");
            var result = GetFromSession();
            Utility.LogDiagnosticEntry("finished fetching data from session");

            if (previousRequestTimeStamp.HasValue && result.TimeStamp < previousRequestTimeStamp.Value)
                return new FlightAvailResult()
                {
                    Status = CallStatus.Failure,
                    TimeStamp = result.TimeStamp,
                    StatusMessage = "Invalid timestamp specified in request, expected value should be less than or equal to " + result.TimeStamp.ToString(CultureInfo.InvariantCulture)
                };

            if (IsValid(result))
                return result;

            return new FlightAvailResult() { Status = CallStatus.Failure };
        }

        #endregion

        #region Protected Members

        public void NotifySession(SessionNotificationType sessionNotificationType)
        {
            NotifySession(sessionNotificationType, (FlightAvailResult)null);
        }

        protected virtual void NotifySession(SessionNotificationType sessionNotificationType, FlightAvailResult result)
        {
            Utility.LogDiagnosticEntry(string.Format("NotifySession{0} - start", sessionNotificationType));

            var sessionId = GetSessionId();

            if (result == null)
                result = new FlightAvailResult()
                {
                    Status = CallStatus.InProgress,
                    TimeStamp = DateTime.UtcNow
                };

            var store = GetSessionStore();

            switch (sessionNotificationType)
            {
                case SessionNotificationType.Begun:
                    store.Add(KeyStore.APISessionKeys.FlightSeachCategory, sessionId, result);
                    break;
                case SessionNotificationType.Result:
                    result.TimeStamp = DateTime.UtcNow;

                    store.AddAsync(KeyStore.APISessionKeys.FlightSeachCategory, sessionId, result);
                    break;
            }

            Utility.LogDiagnosticEntry(string.Format("NotifySession{0} - end", sessionNotificationType));
        }

        protected virtual void SaveSearchCriteriaIntoSession(FlightSearchCriteria flightSearchCriteria)
        {
            var sessionId = GetSessionId();

            var store = GetSessionStore();

            store.AddAsync(KeyStore.APISessionKeys.FlightSeachCriteria, sessionId, flightSearchCriteria);
        }

        public virtual void SynchronizeAndAddSession(FlightAvailResult flightAvailResult)
        {
            var sessionId = GetSessionId();

            GetSessionStore().Add(KeyStore.APISessionKeys.FlightSeachCategory, sessionId, flightAvailResult);
        }

        private static SessionStore GetSessionStore()
        {
            return Utility.GetSessionProvider();
        }

        protected virtual FlightAvailResult GetFromSession()
        {
            return GetSessionStore()
                .Get<FlightAvailResult>(KeyStore.APISessionKeys.FlightSeachCategory, GetSessionId());
        }

        protected string GetSessionId()
        {
            var context = Utility.GetCurrentContext();

            return context.SessionId.ToString();
        }

        protected virtual bool IsValid(FlightAvailResult result)
        {
            if (result == null || result.Status == CallStatus.Failure)
            {
                return false;
            }

            return true;
        }

        #endregion

        protected FlightAvailResult AvailInternal(Requester requester, FlightSearchCriteria searchCriteria, bool registerSupplierStreaming)
        {
            SaveSearchCriteriaIntoSession(searchCriteria);

            var results = FireSupplierRequests(requester, searchCriteria);

            if (results == null)
            {
                return new FlightAvailResult()
                           {
                               Status = CallStatus.Warning,
                               StatusCode = KeyStore.ErrorCodes.NoResults,
                               StatusMessage = ErrorCodeManager.GetErrorMessage(KeyStore.ErrorCodes.NoResults)
                           };
            }
            var dedupeStrategy = GetDedupeStrategy(requester);
            RecommendationComparer.SortingOrder = searchCriteria.SortingOrder;

            var contentManager = RuntimeContext.Resolver.Resolve<IContentManagerFactory>().GetContentManager();
            var uniqueRecommendations = new List<FlightRecommendation>();
            var counter = new IntCounter();

            IList<FlightRecommendation> readyForProcessing;
            IList<FlightRecommendation> notReadyForProcessing;
            var criteriaBasedUnprocessedRecommendations = new Dictionary<FlightSearchCriteria, IList<FlightRecommendation>>();

            var allSuppliersFailed = true;
            var allFailed = true;
            var errorMessages = new List<string>();
            var didTransformChildPax = false;

            foreach (var executionResult in results)
            {
                if (!executionResult.IsSuccess)
                {
                    if (executionResult.Error.InnerException is FlightSupplierException)
                    {
                        errorMessages.Add(String.Format("Supplier Failure for Supplier (Name : {0})", executionResult.Input.ProviderSpace.Family));
                    }
                    else
                    {
                        allSuppliersFailed = false;
                        errorMessages.Add(String.Format("Internal Error for Supplier (Name : {0})", executionResult.Input.ProviderSpace.Family));
                    }

                    Utility.GetLogFactory().WriteAsync(executionResult.Error.ToContextualEntry(),
                                                       KeyStore.LogCategories.ExceptionCategories.Regular);

                    continue;
                }

                var result = executionResult.Output.Item1;

                if (result.Status == CallStatus.Failure)
                {
                    errorMessages.Add(String.Format("Supplier Failure for Supplier (Name : {0})", executionResult.Input.ProviderSpace.Family));
                    continue;
                }

                allSuppliersFailed = false;
                allFailed = false;

                var criteria = executionResult.Input.SearchCriteria;
                didTransformChildPax = didTransformChildPax ||
                                       criteria.PassengerInfoSummary.Any(x => x.ContainsConvertedChild);

                Utility.LogDiagnosticEntry("successfully recieved results from supplier family : " + executionResult.Input.ProviderSpace.Family);

                if (result.FlightRecommendations.Count > 0)
                {
                    Utility.LogDiagnosticEntry("loading static content - start");
                    contentManager.LoadFlightSegmentStaticContent(result.FlightRecommendations);
                    Utility.LogDiagnosticEntry("loading static content - end");
                    SetCabinTypeFromClassOfService(result.FlightRecommendations);
                    Utility.LogDiagnosticEntry(string.Format("content loading of results for provider-{0} complete",
                                                             executionResult.Input.ProviderSpace.Name));
                }

                Utility.LogDiagnosticEntry("Setting Net rate savings - start");

                SetNetRateSavings(result.FlightRecommendations, out readyForProcessing, out notReadyForProcessing, requester);

                Utility.LogDiagnosticEntry("Setting Net rate savings - end");

                criteriaBasedUnprocessedRecommendations[criteria] = notReadyForProcessing;

                Utility.LogDiagnosticEntry(string.Format("Recommendation processing{0} - start",
                                                         executionResult.Input.ProviderSpace.Family));
                foreach (var recommendation in readyForProcessing)
                {
                    uniqueRecommendations = ProcessRecommendation(requester, recommendation, FilterStrategy,
                                                                  criteria, counter, FareManager, uniqueRecommendations,
                                                                  dedupeStrategy, InterlineAgreementProvider);
                }
                Utility.LogDiagnosticEntry(string.Format("Recommendation processing{0} - end",
                                                         executionResult.Input.ProviderSpace.Family));

                if (!registerSupplierStreaming) continue;

                RegisterResultForStreaming(uniqueRecommendations, RecommendationComparer, FilterStrategy, searchCriteria);

                Utility.LogDiagnosticEntry(string.Format("registered results for streaming for supplier family '{0}'",
                                                         executionResult.Input.ProviderSpace.Family));
            }

            var allUnprocessedRecommendations =
                criteriaBasedUnprocessedRecommendations.Values.SelectMany(x => x).ToListBuffered();

            Utility.LogDiagnosticEntry("Setting Net rate savings for unprocessed recommendations - start");

            SetNetRateSavings(allUnprocessedRecommendations,
                              out readyForProcessing,
                              out notReadyForProcessing, requester);

            Utility.LogDiagnosticEntry("Setting Net rate savings for unprocessed recommendations - end");

            //Process all as there are no more result sets to check published fares in 
            criteriaBasedUnprocessedRecommendations.Keys.ToListBuffered().
                ForEach(criteria => criteriaBasedUnprocessedRecommendations[criteria].ForEach
                                        (recommendation =>
                                             {
                                                 uniqueRecommendations = ProcessRecommendation(requester, recommendation,
                                                                                               FilterStrategy, criteria,
                                                                                               counter, FareManager,
                                                                                               uniqueRecommendations,
                                                                                               dedupeStrategy,
                                                                                               InterlineAgreementProvider);
                                             }));

            Utility.LogDiagnosticEntry("itineraries gathered from all suppliers, count = " + uniqueRecommendations.Count);

            Utility.LogDiagnosticEntry("finalizing results - start");

            var finalizedResult = FinalizeResults(uniqueRecommendations, RecommendationComparer, FilterStrategy,
                                                    searchCriteria);

            Utility.LogDiagnosticEntry("finalizing results - end");

            var retVal = new FlightAvailResult()
                             {
                                 Status = CallStatus.Success,
                                 FlightRecommendations = finalizedResult,
                                 Messages = new List<string>()
                             };

            if (didTransformChildPax)
                retVal.Messages.Add(KeyStore.InformationMessages.ChildConversionTookPlace);

            SetResponseStatus(retVal, allFailed, allSuppliersFailed, errorMessages);

            NotifySession(SessionNotificationType.Result, retVal);

            Utility.LogDiagnosticEntry("results finalized.");

            return retVal;
        }

        private static void SetCabinTypeFromClassOfService(IList<FlightRecommendation> flightRecommendations)
        {
            if (flightRecommendations == null || flightRecommendations.Count == 0)
                return;

            var providerFamily = flightRecommendations[0].ProviderSpace.Family;

            var allSegments = flightRecommendations.SelectMany(x => x.Legs.SelectMany(y => y.Segments)).ToListBuffered();

            Utility.LogDiagnosticEntry(string.Format("Cabin type resolution{0} - start", providerFamily));
            allSegments.ForEach(
                flightSegment => flightSegment.PaxTypeFareBasisCodes.ForEach(paxTypeFareBasis =>
                                                                                 {
                                                                                     if (paxTypeFareBasis.CabinType == CabinType.Unknown && !string.IsNullOrEmpty(paxTypeFareBasis.ClassOfService))
                                                                                         paxTypeFareBasis.CabinType = GetCabinTypeFromClassOfService(paxTypeFareBasis.ClassOfService, flightSegment);
                                                                                 }));
            Utility.LogDiagnosticEntry(string.Format("Cabin type resolution{0} - end", providerFamily));
        }

        private static CabinType GetCabinTypeFromClassOfService(string classOfService, FlightSegment flightSegment)
        {
            var classOfServiceMap = RuntimeContext.Resolver.Resolve<IClassOfServiceMap>();
            return classOfServiceMap.GetClassOfService(flightSegment, classOfService);
        }

        private static void SetResponseStatus(FlightAvailResult result, bool allFailed, bool allSuppliersFailed, List<string> errorMessages)
        {
            if (allSuppliersFailed)
            {
                SetStatus(result, CallStatus.Failure, KeyStore.ErrorCodes.SupplierFailure, errorMessages);
                return;
            }

            if (allFailed)
            {
                SetStatus(result, CallStatus.Failure, KeyStore.ErrorCodes.InternalError, errorMessages);
                return;
            }

            if (errorMessages != null && errorMessages.Count > 0)
            {
                SetStatus(result, CallStatus.Warning, KeyStore.ErrorCodes.InternalErrorAndSupplierFailure, errorMessages);
                return;
            }

            if (result.FlightRecommendations == null || result.FlightRecommendations.Count == 0)
                SetStatus(result, CallStatus.Success, KeyStore.ErrorCodes.NoResults, errorMessages);

        }

        private static void SetStatus(FlightAvailResult result, CallStatus callStatus, string errorCode, List<string> errorMessages)
        {
            result.Status = callStatus;
            result.StatusCode = errorCode;
            result.StatusMessage = ErrorCodeManager.GetErrorMessage(errorCode);

            if (errorMessages != null && errorMessages.Count > 0)
                result.Messages.AddRange(errorMessages);
        }

        private IEnumerable<ExecutionResult<FlightAvailRq, Tuple<FlightAvailResult, FlightSearchCriteria>>> FireSupplierRequests(Requester requester, FlightSearchCriteria flightSearchCriteria)
        {
            var providerSpaceManager = RuntimeContext.Resolver.Resolve<IProviderSpaceManager>();

            Utility.LogDiagnosticEntry("Loading provider spaces for avail");

            var providerSpaces = providerSpaceManager.LoadProviderSpaceForAvail(requester, flightSearchCriteria);

            if (providerSpaces == null || providerSpaces.Count == 0)
            {
                var entry = Utility.GetEventEntryContextual();
                entry.CallType = KeyStore.CallTypes.FilterProviderSpaces;
                entry.Title = "Search - FilterProviderSpaces";
                entry.AddMessage("All the fare sources were filtered out. Cannot make supplier requests.");
                Utility.GetLogFactory().WriteAsync(entry, KeyStore.LogCategories.Core);
                return null;
            }

            Utility.LogDiagnosticEntry("Loading search criterion per supplier");

            var searchCriteriaAsPerProvider = providerSpaceManager.ProviderSearchCriteriaStrategy(providerSpaces, flightSearchCriteria);

            Utility.LogDiagnosticEntry("creating requests per supplier");

            var flightsAvailRequests = CreateFlightAvailRequestsPerProvider(searchCriteriaAsPerProvider);

            var availProvider = RuntimeContext.Resolver.Resolve<IAvailProviderStrategy>();

            var parallelism =
                TaskParallelismFacadeFactory.GetParallelismFacade
                    <FlightAvailRq, Tuple<FlightAvailResult, FlightSearchCriteria>>(
                        x =>
                        new Tuple<FlightAvailResult, FlightSearchCriteria>(availProvider.Avail(x), x.SearchCriteria),
                        (SettingManager.SupplierThreadTimeoutInSeconds + 3) * 1000);

            var results = parallelism.ExecuteEnumerable(flightsAvailRequests, SettingManager.SupplierSchedulerType);

            Utility.LogDiagnosticEntry(string.Format("execution of supplier results has started for {0} requests.", flightsAvailRequests.Count));

            PrecacheMarkups(providerSpaces);

            return results;
        }

        private void PrecacheMarkups(ICollection<ProviderSpace> providerSpaces)
        {
            Utility.LogDiagnosticEntry(string.Format("Precaching of markups - Start"));

            FareManager.Initialize(providerSpaces);

            Utility.LogDiagnosticEntry(string.Format("Precaching of markups - End"));
        }

        private void SetNetRateSavings(ICollection<FlightRecommendation> flightRecommendations, out IList<FlightRecommendation> canProcess, out IList<FlightRecommendation> cannotProcess, Requester requester)
        {
            canProcess = new Collection<FlightRecommendation>();
            cannotProcess = new Collection<FlightRecommendation>();

            if (flightRecommendations == null || flightRecommendations.Count == 0)
                return;

            var flightKeyProvider = GetFlightKeyProvider(requester);

            var keyBasedPublishedFareGroups =
                flightRecommendations.Where(x => x.Fare.FareType == FareType.Published)
                .GroupBy(flightKeyProvider.GetKey);

            foreach (var keyBasedPublishedFareGroup in keyBasedPublishedFareGroups)
                _keyBasedPublishedFares[keyBasedPublishedFareGroup.Key] =
                    keyBasedPublishedFareGroup.First().Fare;

            foreach (var flightRecommendation in flightRecommendations)
            {
                if (flightRecommendation.Fare.FareType != FareType.Negotiated)
                {
                    canProcess.Add(flightRecommendation);
                    continue;
                }

                var recommendationKey = flightKeyProvider.GetKey(flightRecommendation);

                FlightFare publishedFareAmount;

                if (!_keyBasedPublishedFares.TryGetValue(recommendationKey, out publishedFareAmount))
                {
                    cannotProcess.Add(flightRecommendation);
                    continue;
                }

                var savings = CalculateNetRateSavings(publishedFareAmount, flightRecommendation.Fare);
                if (savings.BaseAmount > 0 && (flightRecommendation.Fare.Savings == null || savings.BaseAmount > flightRecommendation.Fare.Savings.BaseAmount))
                    flightRecommendation.Fare.Savings = savings;
                canProcess.Add(flightRecommendation);
            }
        }

        private Money CalculateNetRateSavings(FlightFare publishedFareAmount, FlightFare fare)
        {
            var publishedFare = publishedFareAmount.BaseAmount + publishedFareAmount.TotalTaxes;
            var negotiatedFare = fare.BaseAmount + fare.TotalTaxes;
            return publishedFare - negotiatedFare;
        }

        protected void RegisterResultForStreaming(List<FlightRecommendation> uniqueRecommendations,
            IFlightRecommendationComparer sortComparer, IFilterStrategy filterStrategy, FlightSearchCriteria searchCriteria)
        {
            var result = FinalizeResults(uniqueRecommendations, sortComparer, filterStrategy, searchCriteria);

            var availResult = new FlightAvailResult
                {
                    Status = CallStatus.InProgress,
                    FlightRecommendations = result,
                    TimeStamp = DateTime.UtcNow
                };

            SynchronizeAndAddSession(availResult);
        }

        protected virtual IList<FlightRecommendation> FinalizeResults(
            List<FlightRecommendation> uniqueRecommendations,
            IFlightRecommendationComparer sortComparer,
            IFilterStrategy filterStrategy, FlightSearchCriteria searchCriteria)
        {
            var result = new List<FlightRecommendation>(uniqueRecommendations);

            result.Sort(sortComparer);

            Utility.LogDiagnosticEntry("sorting of results complete");

            result = new List<FlightRecommendation>(filterStrategy.Filter(result, searchCriteria));

            Utility.LogDiagnosticEntry("filtering of results complete, count = " + result.Count);

            return result;
        }

        protected virtual List<FlightRecommendation> ProcessRecommendation(Requester requester,
            FlightRecommendation recommendation, IFilterStrategy filterStrategy, FlightSearchCriteria criteria,
            IntCounter counter, IFareManager<FlightFareDetails, ICollection<ProviderSpace>> markupStrategy,
            List<FlightRecommendation> uniqueRecommendations,
            IFlightDedupeStrategy dedupeStrategy, IAirlineInterlineAgreementProvider airlineInterlineAgreementProvider)
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            if (CanSellItinerary(airlineInterlineAgreementProvider, recommendation, requester)
                && !filterStrategy.ShouldFilter(recommendation, criteria))
            {
                var sellabilityCheckTime = stopwatch.Elapsed.TotalMilliseconds;
                SetCurrentApplicableContract(recommendation);
                var settingContractTime = stopwatch.Elapsed.TotalMilliseconds - sellabilityCheckTime;

                recommendation.RefId = counter.Increment();

                markupStrategy.ApplyFareComponents(new PosInfo()
                {
                    PosId = requester.POS.ID,
                    RequesterDK =
                        requester.ID.ToString(CultureInfo.InvariantCulture)
                }, GetFlightFareDetails(recommendation, requester));

                var markupTime = stopwatch.Elapsed.TotalMilliseconds - settingContractTime;
                uniqueRecommendations = dedupeStrategy.Dedupe(uniqueRecommendations, recommendation);
                var dedupTime = stopwatch.Elapsed.TotalMilliseconds - markupTime;
                stopwatch.Stop();

                Utility.LogDiagnosticEntry(string.Format("Processing recommendation, sellability: {0}ms, db contract: {1}ms, markup:{2}ms, dedup:{3}ms",
                    sellabilityCheckTime, settingContractTime, markupTime, dedupTime));
            }

            return uniqueRecommendations;
        }

        protected virtual IList<FlightAvailRq> CreateFlightAvailRequestsPerProvider(IDictionary<ProviderSpace, FlightSearchCriteria> searchCriteriaAsPerProvider)
        {
            return searchCriteriaAsPerProvider.Select(searchCriteriaProviderPair =>
                new FlightAvailRq(searchCriteriaProviderPair.Key, searchCriteriaProviderPair.Value))
                .ToListBuffered(searchCriteriaAsPerProvider.Count);
        }

        protected virtual bool CanSellItinerary(IAirlineInterlineAgreementProvider airlineInterlineAgreementProvider,
            FlightRecommendation recommendation, Requester requester)
        {
            //TODO: RV Check Rate Code Contract check

            return airlineInterlineAgreementProvider.IsRecommendationTicketable(recommendation, requester);
        }

        private static void SetCurrentApplicableContract(FlightRecommendation recommendation)
        {
            var matchedContracts = GetMatchingContractsForRecommendation(recommendation);

            matchedContracts.Sort((contract1, contract2) => contract1.Preference.CompareTo(contract2.Preference));

            recommendation.Contract = matchedContracts.FirstOrDefault();
        }

        private static List<AirContract> GetMatchingContractsForRecommendation(FlightRecommendation recommendation)
        {
            if (recommendation.ProviderSpace.Contracts == null || recommendation.ProviderSpace.Contracts.Count == 0)
                return new List<AirContract>();

            if (recommendation.Fare.FareType == FareType.Negotiated)
                return recommendation.ProviderSpace.Contracts.Where(
                    x =>
                    !string.IsNullOrEmpty(x.ContractRateCode) &&
                    x.ContractRateCode.Equals(recommendation.RateCode, StringComparison.OrdinalIgnoreCase) &&
                    x.CanSellRecommendation(recommendation)).ToListBuffered();

            if (recommendation.Fare.FareType == FareType.Published)
                return recommendation.ProviderSpace.Contracts.Where(
                    x =>
                    !string.IsNullOrEmpty(x.FareType) &&
                    !x.FareType.Equals(KeyStore.FareType.Negotiated, StringComparison.OrdinalIgnoreCase) &&
                    x.CanSellRecommendation(recommendation)).ToListBuffered();

            return new List<AirContract>();
        }

        private IFlightDedupeStrategy GetDedupeStrategy(Requester requester)
        {
            var dedupeStrategy = RuntimeContext.Resolver.Resolve<IFlightDedupeStrategy>();
            dedupeStrategy.FLightKeyProvider = GetFlightKeyProvider(requester);
            dedupeStrategy.ProviderFamilyPreferenceMapping = ParseProviderFamilyPreference();

            return dedupeStrategy;
        }

        private IDictionary<string, int> ParseProviderFamilyPreference()
        {
            var preferenceMapping = new Dictionary<string, int>();

            var providerFamilyPreference = ProviderFamilyPreference ?? string.Empty;
            if (providerFamilyPreference == string.Empty)
                return preferenceMapping;

            providerFamilyPreference.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ForEach(
                preference =>
                {
                    var values = preference.Split('|');
                    int preferenceLevel;
                    if (values.Count() != 2 || !int.TryParse(values[1], out preferenceLevel))
                        return;
                    preferenceMapping[values[0]] = preferenceLevel;
                });

            return preferenceMapping;
        }

        private FlightFareDetails GetFlightFareDetails(FlightRecommendation flightRecommendation, Requester requester)
        {
            return FlightFareDetailsExtracter.GetFlightFareDetails(flightRecommendation, JumpNegoRate);
        }

        private string ProviderFamilyPreference
        {
            get { return CS.SettingManager.GetKeyedSetting(CS.KeyStore.BackOffice.Keys.ProviderFamilyPreference); }
        }

        private bool? _jumpNegoRate;
        private bool JumpNegoRate
        {
            get
            {
                if (!_jumpNegoRate.HasValue)
                {
                    var contextRetrievedValue =
                        CS.SettingManager.GetKeyedSetting(CS.KeyStore.BackOffice.Keys.JumpNegoRate);
                    _jumpNegoRate = contextRetrievedValue != null && contextRetrievedValue.ToLower().Equals(true.ToString());
                }
                return _jumpNegoRate.Value;
            }
        }
    }
}
