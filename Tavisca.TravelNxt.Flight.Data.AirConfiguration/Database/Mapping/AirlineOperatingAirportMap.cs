using System.Data.Entity.ModelConfiguration;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirlineOperatingAirportMap : EntityTypeConfiguration<AirlineOperatingAirport>
    {
        public AirlineOperatingAirportMap()
        {
            // Primary Key
            this.HasKey(t => new { t.AirlineCode, t.AirportCode });

            // Properties
            this.Property(t => t.AirlineCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(64);

            this.Property(t => t.AirportCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("AirlineOperatingAirport");
            this.Property(t => t.AirlineCode).HasColumnName("AirlineCode");
            this.Property(t => t.AirportCode).HasColumnName("AirportCode");
        }
    }
}
