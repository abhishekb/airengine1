using System.Data.Entity.ModelConfiguration;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirlineCosZoneCountryMap : EntityTypeConfiguration<AirlineCosZoneCountry>
    {
        public AirlineCosZoneCountryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.AirlineCode, t.ZoneName, t.CountryCode });

            // Properties
            this.Property(t => t.AirlineCode)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ZoneName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CountryCode)
                .IsRequired()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("AirlineCosZoneCountries");
            this.Property(t => t.AirlineCode).HasColumnName("AirlineCode");
            this.Property(t => t.ZoneName).HasColumnName("ZoneName");
            this.Property(t => t.CountryCode).HasColumnName("CountryCode");
        }
    }
}
