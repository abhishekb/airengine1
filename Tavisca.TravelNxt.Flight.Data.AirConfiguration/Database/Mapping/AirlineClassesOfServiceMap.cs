using System.Data.Entity.ModelConfiguration;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirlineClassesOfServiceMap : EntityTypeConfiguration<AirlineClassesOfService>
    {
        public AirlineClassesOfServiceMap()
        {
            // Primary Key
            this.HasKey(t => new { t.AirlineCode, t.ClassOfService, t.ZoneName });

            // Properties
            this.Property(t => t.AirlineCode)
                .IsRequired()
                .HasMaxLength(64);

            this.Property(t => t.ClassOfService)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.ZoneName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CabinType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AirlineClassesOfService");
            this.Property(t => t.AirlineCode).HasColumnName("AirlineCode");
            this.Property(t => t.ClassOfService).HasColumnName("ClassOfService");
            this.Property(t => t.ZoneName).HasColumnName("ZoneName");
            this.Property(t => t.CabinType).HasColumnName("CabinType");
        }
    }
}
