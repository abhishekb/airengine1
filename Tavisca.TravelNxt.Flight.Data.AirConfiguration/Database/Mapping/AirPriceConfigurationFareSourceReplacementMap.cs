﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirPriceConfigurationFareSourceReplacementMap : EntityTypeConfiguration<AirPriceConfigurationFareSourceReplacement>
    {
        public AirPriceConfigurationFareSourceReplacementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AirPriceConfigurationFareSourceReplacement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AirPriceConfigurationId).HasColumnName("AirPriceConfigurationId");
            this.Property(t => t.FareSourceId).HasColumnName("FareSourceId");
            this.Property(t => t.ReplacementFaresourceId).HasColumnName("ReplacementFaresourceId");
            this.Property(t => t.RuleId).HasColumnName("RuleId");

            // Relationships
            this.HasRequired(t => t.AirPriceConfiguration)
                .WithMany(t => t.AirPriceConfigurationFareSourceReplacements)
                .HasForeignKey(t => t.AirPriceConfigurationId);

            this.HasOptional(t => t.AirRule)
                .WithMany(t => t.AirPriceConfigurationFareSourceReplacements)
                .HasForeignKey(t => t.RuleId);
        }
    }
}
