using System.Data.Entity.ModelConfiguration;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class PosFareSourceStrategySettingMap : EntityTypeConfiguration<PosFareSourceStrategySetting>
    {
        public PosFareSourceStrategySettingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.AirLineCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(64);

            this.Property(t => t.RateCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.MarketType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("PosFareSourceStrategySetting");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PosID).HasColumnName("PosID");
            this.Property(t => t.FareSourceID).HasColumnName("FareSourceID");
            this.Property(t => t.AirLineCode).HasColumnName("AirLineCode");
            this.Property(t => t.RateCode).HasColumnName("RateCode");
            this.Property(t => t.CheckMarket).HasColumnName("CheckMarket");
            this.Property(t => t.MarketType).HasColumnName("MarketType");
        }
    }
}
