﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirPriceConfigurationMap : EntityTypeConfiguration<AirPriceConfiguration>
    {
        public AirPriceConfigurationMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AccountId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AirPriceConfiguration");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.PosId).HasColumnName("PosId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.IsFailoverEnabled).HasColumnName("IsFailoverEnabled");
        }
    }
}
