﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirRuleMap : EntityTypeConfiguration<AirRule>
    {
        public AirRuleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RuleSet)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("AirRules");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RuleSet).HasColumnName("RuleSet");
        }
    }
}
