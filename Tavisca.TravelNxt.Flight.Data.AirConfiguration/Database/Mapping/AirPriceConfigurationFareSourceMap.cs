﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping
{
    public class AirPriceConfigurationFareSourceMap : EntityTypeConfiguration<AirPriceConfigurationFareSource>
    {
        public AirPriceConfigurationFareSourceMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("AirPriceConfigurationFareSource");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AirPriceConfigurationId).HasColumnName("AirPriceConfigurationId");
            this.Property(t => t.FareSourceId).HasColumnName("FareSourceId");
            this.Property(t => t.PreferenceOrder).HasColumnName("PreferenceOrder");

            // Relationships
            this.HasRequired(t => t.AirPriceConfiguration)
                .WithMany(t => t.AirPriceConfigurationFareSources)
                .HasForeignKey(t => t.AirPriceConfigurationId);
        }
    }
}
