using System.Data.Entity;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database.Mapping;
using Tavisca.TravelNxt.Flight.Data.AirConfiguration.Models;
using Tavisca.TravelNxt.Flight.Settings;

namespace Tavisca.TravelNxt.Flight.Data.AirConfiguration.Database
{
    public class AirConfigurationContext : DbContext
    {
        static AirConfigurationContext()
        {
            System.Data.Entity.Database.SetInitializer<AirConfigurationContext>(null);
        }

        public AirConfigurationContext() : base(SettingManager.AirConfigurationConnectionString) { }

        public DbSet<AirlineClassesOfService> AirlineClassesOfServices { get; set; }
        public DbSet<AirlineCosZoneCountry> AirlineCosZoneCountries { get; set; }
        public DbSet<AirlineOperatingAirport> AirlineOperatingAirports { get; set; }
        public DbSet<PosFareSourceStrategySetting> PosFareSourceStrategySettings { get; set; }
        public DbSet<AirPriceConfiguration> AirPriceConfigurations { get; set; }
        public DbSet<AirPriceConfigurationFareSource> AirPriceConfigurationFareSources { get; set; }
        public DbSet<AirRule> AirRules { get; set; }
        public DbSet<AirPriceConfigurationFareSourceReplacement> AirPriceConfigurationFareSourceReplacements { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AirlineClassesOfServiceMap());
            modelBuilder.Configurations.Add(new AirlineCosZoneCountryMap());
            modelBuilder.Configurations.Add(new AirlineOperatingAirportMap());
            modelBuilder.Configurations.Add(new PosFareSourceStrategySettingMap());
            modelBuilder.Configurations.Add(new AirPriceConfigurationMap());
            modelBuilder.Configurations.Add(new AirPriceConfigurationFareSourceMap());
            modelBuilder.Configurations.Add(new AirRuleMap());
            modelBuilder.Configurations.Add(new AirPriceConfigurationFareSourceReplacementMap());
        }
    }
}
