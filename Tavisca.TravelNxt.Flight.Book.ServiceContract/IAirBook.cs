﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Tavisca.TravelNxt.Flight.Book.DataContract;

namespace Tavisca.TravelNxt.Flight.Book.ServiceContract
{
    [ServiceContract(Namespace = "http://tavisca.com")]
    public interface IAirBook
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "save", BodyStyle = WebMessageBodyStyle.Wrapped)]
        FlightSaveRS Save(FlightSaveRQ request);

        [OperationContract]
        [WebInvoke(UriTemplate = "retrieve", BodyStyle = WebMessageBodyStyle.Wrapped)]
        FlightRetrieveRS Retrieve(FlightRetrieveRQ request);
    }
}
