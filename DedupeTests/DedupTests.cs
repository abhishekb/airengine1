﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tavisca.Frameworks.Parallel.Ambience;
using Tavisca.TravelNxt.Common.Settings;
using Tavisca.TravelNxt.Flight.Avail.Core.Dedupe;
using Tavisca.TravelNxt.Flight.Entities;
using Tavisca.TravelNxt.Common.Extensions;

namespace DedupeTests
{
    [TestClass]
    public class DedupTests
    {
        [TestMethod, TestCategory("Dedupe")]
        public void TestKeepBothRecommendations()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var costlyRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, true);
                var cheaperRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, false);

                var dedupeStrategy = new DefaultDedupeStrategy()
                                         {
                                             FLightKeyProvider = new DefaultKeyProvider(),
                                             ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                                         };

                var uniqueRecommendations = new List<FlightRecommendation>() {cheaperRecommendation};

                costlyRecommendation.Fare.FareAttributes = new List<FareAttribute>() { FareAttribute.RefundableFares };

                dedupeStrategy.Dedupe(uniqueRecommendations, costlyRecommendation);


                Assert.IsNotNull(cheaperRecommendation.Fare.Savings);
                Assert.AreEqual(cheaperRecommendation.Fare.Savings.NativeAmount, 50);

                Assert.IsNull(costlyRecommendation.Fare.Savings);
                Assert.IsTrue(uniqueRecommendations.Contains(costlyRecommendation));
                Assert.IsTrue(uniqueRecommendations.Contains(cheaperRecommendation));
            }
        }

        [TestMethod, TestCategory("Dedupe")]
        public void TestKeepNewRecommendation()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var costlyRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, true);
                costlyRecommendation.RefId = 1;
                var cheaperRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, false);
                cheaperRecommendation.RefId = 2;

                var dedupeStrategy = new DefaultDedupeStrategy()
                {
                    FLightKeyProvider = new DefaultKeyProvider(),
                    ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                };

                var uniqueRecommendations = new List<FlightRecommendation>() { costlyRecommendation };

                dedupeStrategy.Dedupe(uniqueRecommendations, cheaperRecommendation);


                Assert.IsNotNull(cheaperRecommendation.Fare.Savings);
                Assert.AreEqual(cheaperRecommendation.Fare.Savings.NativeAmount, 50);

                Assert.IsNull(costlyRecommendation.Fare.Savings);
                Assert.IsTrue(uniqueRecommendations.Contains(cheaperRecommendation));
                Assert.AreEqual(cheaperRecommendation.RefId, 1);
            }
        }

        [TestMethod, TestCategory("Dedupe")]
        public void TestSavings()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var costlyRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, true);
                var cheaperRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, false);
                var cheapestRecommendation = RecommendationProvider.GetRecommendation(FareType.Published, false, true);

                var uniqueRecommendations = new List<FlightRecommendation>() {costlyRecommendation};

                var dedupeStrategy = new DefaultDedupeStrategy()
                {
                    FLightKeyProvider = new DefaultKeyProvider(),
                    ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                };

                dedupeStrategy.Dedupe(uniqueRecommendations, cheaperRecommendation);
                dedupeStrategy.Dedupe(uniqueRecommendations, cheapestRecommendation);

                Assert.IsTrue(uniqueRecommendations.Count == 1);

                Assert.IsNull(costlyRecommendation.Fare.Savings);
                Assert.IsNotNull(cheaperRecommendation.Fare.Savings);
                Assert.IsNotNull(cheapestRecommendation.Fare.Savings);

                Assert.AreEqual(cheaperRecommendation.Fare.Savings.NativeAmount, 50);
                Assert.AreEqual(cheapestRecommendation.Fare.Savings.NativeAmount, 100);
            }
        }

        [TestMethod, TestCategory("Dedupe")]
        public void TestProviderFamilyPreference()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var highPreference = RecommendationProvider.GetRecommendation(FareType.Published, false, false, "high");
                var lowPreference = RecommendationProvider.GetRecommendation(FareType.Published, false, false, "low");

                var dedupeStrategy = new DefaultDedupeStrategy()
                                         {
                                             FLightKeyProvider = new DefaultKeyProvider(),
                                             ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                                         };

                dedupeStrategy.ProviderFamilyPreferenceMapping.Add("high", 5);
                dedupeStrategy.ProviderFamilyPreferenceMapping.Add("low", 2);

                var uniqueRecommendations = new List<FlightRecommendation>() {lowPreference};
                dedupeStrategy.Dedupe(uniqueRecommendations, highPreference);

                Assert.IsTrue(uniqueRecommendations.Count == 1);
                Assert.IsTrue(uniqueRecommendations[0].ProviderSpace.Family == "high");
            }
        }

        [TestMethod, TestCategory("Dedupe")]
        public void TestNegoPreferredStrategy()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var dedupeStrategy = new NetRatePreferredStrategy()
                                         {
                                             FLightKeyProvider = new DefaultKeyProvider(),
                                             ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                                         };

                var nonNego = RecommendationProvider.GetRecommendation(FareType.Corporate, true);
                var nego = RecommendationProvider.GetRecommendation(FareType.Negotiated, false);

                var uniqueRecommendations = new List<FlightRecommendation>() {nonNego};

                dedupeStrategy.Dedupe(uniqueRecommendations, nego);

                Assert.IsTrue(uniqueRecommendations.Count == 1);
                Assert.IsTrue(uniqueRecommendations.Contains(nego));
            }
        }

        [TestMethod, TestCategory("Dedupe")]
        public void TestPublishedPreferredStrategy()
        {
            using (new AmbientContextScope(GetCallContext()))
            {
                var dedupeStrategy = new PublishedPreferredStrategy()
                {
                    FLightKeyProvider = new DefaultKeyProvider(),
                    ProviderFamilyPreferenceMapping = new Dictionary<string, int>()
                };

                var published = RecommendationProvider.GetRecommendation(FareType.Published, true);
                var nonPublished = RecommendationProvider.GetRecommendation(FareType.Negotiated, false);

                var uniqueRecommendations = new List<FlightRecommendation>() { nonPublished };

                dedupeStrategy.Dedupe(uniqueRecommendations, published);

                Assert.IsTrue(uniqueRecommendations.Count == 1);
                Assert.IsTrue(uniqueRecommendations.Contains(published));
            }
        }

        private static CallContext GetCallContext()
        {
            return new CallContext("en-us", Guid.NewGuid().ToString(), "1", StaticSettings.GetRootAccountId().ToString(CultureInfo.InvariantCulture), "AUD", "", true);
        }
    }
}
