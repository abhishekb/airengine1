﻿using System;
using System.Collections.Generic;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Flight.Entities;

namespace DedupeTests
{
    class RecommendationProvider
    {
        public static FlightRecommendation GetRecommendation(FareType fareType, bool costly, bool cheapest = false, string family = "")
        {
            var ret = new FlightRecommendation()
                          {
                              Fare = costly ? GetCostlierFlightFare() : GetCheaperFlightFare(cheapest),
                              Legs = GetFlightLegCollection(),
                              ProviderSpace = GetProviderSpace(family)
                          };

            ret.Fare.FareType = fareType;

            return ret;
        }

        private static FlightFare GetCheaperFlightFare(bool cheapest)
        {
            var fare = new FlightFare() {FareType = FareType.Published};

            fare.PassengerFares.Add(new PassengerFare()
                                        {
                                            Quantity = 1,
                                            PassengerType = PassengerType.Adult,
                                            BaseAmount = cheapest ? new Money(50, "AUD") : new Money(100, "AUD")
                                        });

            return fare;
        }

        private static FlightFare GetCostlierFlightFare()
        {
            var fare = new FlightFare() { FareType = FareType.Published };

            fare.PassengerFares.Add(new PassengerFare()
            {
                Quantity = 1,
                PassengerType = PassengerType.Adult,
                BaseAmount = new Money(150, "AUD")
            });

            return fare;
        }

        private static FlightLegCollection GetFlightLegCollection()
        {
            var legCollection = new FlightLegCollection {GetFlightLeg()};

            return legCollection;
        }

        private static FlightLeg GetFlightLeg()
        {
            var leg = new FlightLeg();

            leg.Segments = new FlightSegmentCollection();
            var flightSegment = new FlightSegment("departure", "arrival", DateTime.Now.AddDays(1),
                                                  DateTime.Now.AddDays(2),
                                                  "123", "marketing", 0)
                                    {
                                        PaxTypeFareBasisCodes = new List<PaxTypeFareBasis>() {new PaxTypeFareBasis() {ClassOfService = "S"}}
                                    };
            flightSegment.DepartureAirport.City = new City() { WindowsTimeZone = "Eastern Standard Time" };

            leg.Segments.Add(flightSegment);
            return leg;
        }

        private static ProviderSpace GetProviderSpace(string family)
        {
            return new ProviderSpace(1, "test", new Requester(1, 1, CustomerType.Agent), family, null, null, ProviderSpaceType.Domestic,
                                     new HashSet<ProviderSpaceConfiguration>(), new AdditionalInfoDictionary());
        }
    }
}
