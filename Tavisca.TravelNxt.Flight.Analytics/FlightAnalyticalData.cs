﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tavisca.Frameworks.Serialization.Specialized;
using Tavisca.TravelNxt.Common.Analytics;
using Tavisca.TravelNxt.Common.Extensions;
using Tavisca.TravelNxt.Common.Service.Inspectors.Header;

namespace Tavisca.TravelNxt.Flight.Analytics
{
    public class FlightTimeAwareAnalyticalData : IAnalyticData, ITimeAware
    {
        #region Properties

        public string SessionId { get; set; }
        public string ApplicationId { get; set; }
        public string Title { get; set; }
        public DateTime Timestamp { get; set; }
        public string Tag { get; set; }
        public long AccountId { get; set; }
        public string MachineName { get; set; }
        public CallContext Context { get; set; }
        public double TimeTakenMs { get; set; }
        public bool WasSuccessful { get; set; }
        public int NumberOfResults { get; set; }
        public string Message { get; set; }

        #endregion

        #region Contructors

        public FlightTimeAwareAnalyticalData(long accountId, string sessionId, string applicationId, string title, string tag,
            int numberOfResults, bool wasSuccessful, string message, TimeSpan timeTaken)
            : this(accountId, sessionId, applicationId, title, tag, numberOfResults, wasSuccessful, message, timeTaken, DateTime.UtcNow)
        { }

        public FlightTimeAwareAnalyticalData(long accountId, string sessionId, string applicationId, string title, string tag,
            int numberOfResults, bool wasSuccessful, string message, TimeSpan timeTaken, DateTime timestamp)
        {
            AccountId = accountId;
            SessionId = sessionId;
            ApplicationId = applicationId;
            Title = title;
            Tag = tag;
            NumberOfResults = numberOfResults;
            WasSuccessful = wasSuccessful;
            Message = message;
            TimeTakenMs = timeTaken.TotalMilliseconds;
            Timestamp = timestamp;

            MachineName = Utility.GetCurrentMachineName();
        }

        #endregion

        #region IInfluxDbData Members

        public virtual ICollection<InfluxDbTable> GetTables()
        {
            var table = new InfluxDbTable();

            table.Name = Title;
            table.Columns = new List<string>()
                {
                    "sessionId",
                    "applicationId",
                    "title",
                    "time",
                    "tag",
                    "accountId",
                    "machineName",
                    "timeTakenMs",
                    "numberOfResults",
                    "wasSuccessful",
                    "message"
                };

            table.Rows = new List<InfluxDbTableRow>()
                {
                    new InfluxDbTableRow()
                        {
                            Items = new List<InfluxDbTableRowItem>()
                                {
                                    SessionId, 
                                    ApplicationId, 
                                    Title, 
                                    Timestamp, 
                                    Tag, 
                                    AccountId, 
                                    MachineName, 
                                    TimeTakenMs,
                                    NumberOfResults,
                                    WasSuccessful,
                                    Message
                                }
                        }
                };

            return new List<InfluxDbTable>() { table };
        }

        #endregion
    }
}