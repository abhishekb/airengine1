using System;
using System.Collections.Generic;
using System.Linq;
using Tavisca.Frameworks.Serialization.Specialized;
using Tavisca.TravelNxt.Common.Analytics;

namespace Tavisca.TravelNxt.Flight.Analytics
{
    public class FlightTimeNSupplierAwareAnalyticalData : FlightTimeAwareAnalyticalData, ISupplierAware
    {
        #region Properties

        public string SupplierFamily { get; set; }
        public string SupplierId { get; set; }
        
        #endregion

        #region Contructors

        public FlightTimeNSupplierAwareAnalyticalData(long accountId, string sessionId, string applicationId, string title,
            string tag, int numberOfResults, bool wasSuccessful, string message, string supplierFamily, string supplierId, 
            TimeSpan timeTaken)
            : this(accountId, sessionId, applicationId, title, tag, numberOfResults, wasSuccessful, message, supplierFamily,
            supplierId, timeTaken, DateTime.UtcNow)
        { }

        public FlightTimeNSupplierAwareAnalyticalData(long accountId, string sessionId, string applicationId, string title, 
            string tag, int numberOfResults, bool wasSuccessful, string message,
            string supplierFamily, string supplierId, TimeSpan timeTaken, DateTime timestamp)
            : base(accountId, sessionId, applicationId, title, tag, numberOfResults, wasSuccessful, message,
            timeTaken, timestamp)
        {
            SupplierFamily = supplierFamily;
            SupplierId = supplierId;
        }

        #endregion

        #region FlightTimeAwareAnalyticalData Members

        public override ICollection<InfluxDbTable> GetTables()
        {
            var baseTable = base.GetTables().First();

            baseTable.Columns.Add("supplierFamily");
            baseTable.Columns.Add("supplierId");

            var row = baseTable.Rows.First();

            row.Items.Add(SupplierFamily);
            row.Items.Add(SupplierId);

            return new List<InfluxDbTable>() { baseTable };
        }

        #endregion
    }
}